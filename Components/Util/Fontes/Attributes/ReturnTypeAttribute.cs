﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ReturnTypeAttribute : Attribute
    {
        #region [ Fields & Properties ]

        private readonly Type _type;

        public Type Type
        {
            get { return _type; }
        }

        #endregion [ Fields & Properties ]

        #region [ Constructors ]

        public ReturnTypeAttribute(Type type)
        {
            _type = type;
        }

        #endregion [ Constructors ]

        #region [ Methods ]

        public static ReturnTypeAttribute GetReturnTypeAttribute(Type type)
        {
            ReturnTypeAttribute attribute = null;

            var customAttributes = (ReturnTypeAttribute[]) GetCustomAttributes(type, typeof (ReturnTypeAttribute));

            if ((customAttributes != null) && (customAttributes.Length > 0))
                attribute = customAttributes[0];

            return attribute;
        }

        #endregion [ Methods ]
    }
}