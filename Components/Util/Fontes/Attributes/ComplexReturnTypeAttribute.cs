﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ComplexReturnTypeAttribute : Attribute
    {
        #region [ Fields & Properties ]

        private readonly string _propertyName;
        private readonly Type _type;

        public string PropertyName
        {
            get { return _propertyName; }
        }

        public Type Type
        {
            get { return _type; }
        }

        #endregion [ Fields & Properties ]

        #region [ Constructors ]

        public ComplexReturnTypeAttribute(string propertyName, Type type)
        {
            _propertyName = propertyName;
            _type = type;
        }

        #endregion [ Constructors ]

        #region [ Methods ]

        public static ComplexReturnTypeAttribute GetReturnTypeAttribute(Type type)
        {
            ComplexReturnTypeAttribute attribute = null;

            var customAttributes = (ComplexReturnTypeAttribute[]) GetCustomAttributes(type, typeof (ComplexReturnTypeAttribute));

            if ((customAttributes != null) && (customAttributes.Length > 0))
                attribute = customAttributes[0];

            return attribute;
        }

        #endregion [ Methods ]
    }
}