﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class StringValueAttribute : Attribute
    {
        private readonly string _description;
        private readonly string _value;

        public StringValueAttribute(string value)
            : this(value, null)
        {
        }

        public StringValueAttribute(string value, string description)
        {
            _value = value;
            _description = description;
        }

        public string Description
        {
            get { return _description; }
        }

        public string Value
        {
            get { return _value; }
        }
    }
}