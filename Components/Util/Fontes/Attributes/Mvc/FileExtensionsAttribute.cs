﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lalubema.Utilitarios.Attributes.Mvc
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter,
        AllowMultiple = false)]
    public sealed class FileExtensionsAttribute : DataTypeAttribute
    {
        public FileExtensionsAttribute() : base("FileExtensions")
        {
        }

        public string[] Extensions { get; set; }

        public FileType FileType { get; set; }
    }
}