﻿using System;

namespace Lalubema.Utilitarios.Attributes.Mvc
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TypeDisplayAttribute : Attribute
    {
        private readonly string _name;

        public string Name
        {
            get { return _name; }
        }

        public TypeDisplayAttribute(string name)
        {
            _name = name;
        }
    }
}