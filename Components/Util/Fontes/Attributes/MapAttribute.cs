﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class MapAttribute : Attribute
    {
        public MapAttribute()
        {
        }

        public MapAttribute(string property)
        {
            PropertyName = property;
        }

        public string PropertyName { get; set; }

        public string Type { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Class)]
    public sealed class NoMapAttribute : Attribute
    {
        public NoMapAttribute()
        {
        }

        public NoMapAttribute(string noMapValue)
        {
            NoMapValue = noMapValue;
        }

        public string NoMapValue { get; set; }
    }
}