﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FilterAttribute : Attribute, IFlagAttribute
    {
        private readonly FilterTypeFlags _filterTypeFlags;

        public FilterAttribute(FilterTypeFlags filterTypeAttr)
        {
            _filterTypeFlags = filterTypeAttr;
        }

        public FilterTypeFlags FilterTypeFlags
        {
            get { return _filterTypeFlags; }
        }

        public bool HasFlag(Enum value)
        {
            return FilterTypeFlags.HasFlag(value);
        }
    }

    public interface IFlagAttribute
    {
        bool HasFlag(Enum value);
    }
}