﻿using System;

namespace Lalubema.Utilitarios.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ValidateCryptographyKey : Attribute
    {
        private readonly int _codigoAplicacao;
        private readonly bool _userKeyRequired;

        public ValidateCryptographyKey(bool userKeyRequired, int codigoAplicacao)
        {
            _userKeyRequired = userKeyRequired;
            _codigoAplicacao = codigoAplicacao;
        }

        public int CodigoAplicacao
        {
            get { return _codigoAplicacao; }
        }

        public bool UserKeyRequired
        {
            get { return _userKeyRequired; }
        }
    }
}