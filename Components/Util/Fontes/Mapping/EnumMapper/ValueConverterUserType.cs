﻿using System;
using System.ComponentModel;
using System.Data;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Mapping.EnumMapper
{
    public abstract class ValueConverterUserType<T> : IUserType where T : struct
    {
        protected TypeConverter Converter = TypeDescriptor.GetConverter(typeof (T));

        #region IUserType Members

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public new bool Equals(object x, object y)
        {
            if (x == null || y == null)
                return (x == null && y == null);

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public bool IsMutable
        {
            get { return false; }
        }

        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var value = (string) NHibernateUtil.String.NullSafeGet(rs, names);

            return value.ConvertoToEnum<T>();
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            var nativeValue = (value as Enum).ConvertToString();

            NHibernateUtil.String.NullSafeSet(cmd, nativeValue, index);
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public Type ReturnedType
        {
            get { return typeof (T); }
        }

        public abstract SqlType[] SqlTypes { get; }

        #endregion
    }
}