﻿using NHibernate.SqlTypes;

namespace Lalubema.Utilitarios.Mapping.EnumMapper
{
    public class TypeUserType<T> : ValueConverterUserType<T> where T : struct
    {
        #region Overrides of ValueConverterUserType<T>

        public override SqlType[] SqlTypes
        {
            get { return new SqlType[] { SqlTypeFactory.GetString(1) }; }
        }

        #endregion
    }
}