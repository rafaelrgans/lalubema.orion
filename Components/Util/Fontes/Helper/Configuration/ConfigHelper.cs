﻿using System.Configuration;

namespace Lalubema.Utilitarios.Helper.Configuration
{
    public class ConfigHelper
    {
        #region [ Fields & Properties ]

        private static readonly ConfigHelper ConfigHelperInstance = new ConfigHelper();

        public static ConfigHelper Instance
        {
            get
            {
                return ConfigHelperInstance;
            }
        }

        public string this[string parameter]
        {
            get { return GetParameterValue(parameter); }
        }

        #endregion

        #region [ Constructor ]

        private ConfigHelper()
        {
        }

        #endregion [ Constructor ]

        #region [ Methods ]

        private static string GetParameterValue(string parameter)
        {
            return ConfigurationManager.AppSettings[parameter] ?? string.Empty;
        }

        #endregion [ Methods ]
    }
}