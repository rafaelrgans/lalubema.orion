﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Xml.Linq;

namespace Lalubema.Utilitarios.Helper.Serialization
{
    public class JsonHelper
    {
        public static string XmlToJson(XmlNode xmlNode, bool ommitRootObject)
        {
            var json = new StringBuilder();

            json.AppendLine("{");

            var xDocument = XElement.Parse(xmlNode.InnerXml);

            XmlNodeToJson(json, xDocument, ommitRootObject);

            json.Remove(json.Length - 2, 2);

            json.AppendLine("}");

            return json.ToString();
        }

        private static void XmlNodeToJson(StringBuilder json, XElement xDocument, bool ommitRootObject)
        {
            var nodes = ommitRootObject ? xDocument.Elements() : new List<XElement> {xDocument};

            foreach (var node in nodes)
            {
                json.AppendFormat("\"{0}\": ", SafeJson(node.Name.LocalName));

                var itens = node.Elements("item").ToList();
                if(node.Elements().Count(t => "object".Equals(t.Attribute("type").Value, StringComparison.InvariantCultureIgnoreCase)) > 1 || itens.Any())
                {
                    json.AppendLine("[");

                    if (!itens.Any())
                        itens = node.Elements().ToList();

                    foreach (var childNode in itens)
                    {
                        json.AppendLine("{");

                        XmlNodeToJson(json, childNode, true);

                        json.AppendLine("}, ");
                    }

                    json.Remove(json.Length - 4, 1);

                    json.AppendLine("], ");
                }
                else if (node.Elements().Count() == 1)
                {
                    json.AppendLine("{");

                    XmlNodeToJson(json, node.Elements().FirstOrDefault(), false);

                    json.AppendLine("}, ");
                }
                else if(node.Elements().Count() > 1)
                {
                    json.AppendLine("{");

                    foreach (var element in node.Elements())
                    {
                        XmlNodeToJson(json, element, false);

                        json.AppendLine(", ");
                    }

                    json.Remove(json.Length - 4, 1);

                    json.AppendLine("}, ");
                }
                else
                {
                    json.AppendLine(string.Format("\"{0}\", ", SafeJson(node.Value)));
                }
            }

            json.Remove(json.Length - 4, 1);
        }

        // Make a string safe for JSON
        private static string SafeJson(string sIn)
        {
            var sbOut = new StringBuilder(sIn.Length);
            foreach (char ch in sIn)
            {
                if (Char.IsControl(ch) || ch == '\'')
                {
                    int ich = ch;
                    sbOut.Append(@"\u" + ich.ToString("x4"));
                    continue;
                }
                if (ch == '\"' || ch == '\\' || ch == '/')
                {
                    sbOut.Append('\\');
                }
                sbOut.Append(ch);
            }
            return sbOut.ToString();
        }
    }
}