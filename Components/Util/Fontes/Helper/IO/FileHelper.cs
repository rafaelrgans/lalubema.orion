﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using Lalubema.Utilitarios.Exceptions.IO;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Helper.IO
{
    public class FileHelper : IDisposable
    {
        private Bitmap _imageFile;

        protected Bitmap ImageFile
        {
            get { return _imageFile ?? (_imageFile = new Bitmap(new MemoryStream(File))); }
            private set
            {
                using(var stream = new MemoryStream())
                {
                    value.Save(stream, ImageFile.RawFormat);

                    File = stream.ToArray();
                }

                if (_imageFile != null)
                    _imageFile.Dispose();

                _imageFile = null;
            }
        }

        public SupportedFileFormat SupportedFileFormat { get; set; }

        public Int64 MaxLength { get; set; }

        public byte[] File { get; protected set; }

        public float ResolutionDpi { get; set; }

        public Size ResizeTo { get; set; }

        public bool IncludeFrameBorder { get; set; }

        public bool IsValid { get; private set; }

        public string Extension
        {
            get
            {
                var type = typeof(ImageFormat);
                var imageFormatList = type.GetProperties(BindingFlags.Static | BindingFlags.Public);

                for (var i = 0; i != imageFormatList.Length; i++)
                {
                    var formatClass = (ImageFormat)imageFormatList[i].GetValue(null, null);

                    if (formatClass.Guid.Equals(ImageFile.RawFormat.Guid))
                        return imageFormatList[i].Name.ToLower();
                }

                return "*.png";
            }
        }

        public FileHelper(SupportedFileFormat supportedFileFormat, byte[] file, Int64 maxLength,
                          int resizeToWidth = 512, int resizeToHeight = 512, bool includeFrameBorder = false, float resolutionDpi = 72)
        {
            if (file == null || file.Length == 0)
                throw new ArgumentNullException("file");

            SupportedFileFormat = supportedFileFormat;
            MaxLength = maxLength;
            File = file;
            ResolutionDpi = resolutionDpi;
            ResizeTo = new Size(resizeToWidth, resizeToHeight);
            IncludeFrameBorder = includeFrameBorder;
        }

        public FileHelper ResizeImage()
        {
            if (ResizeTo.Height <= 0 || ResizeTo.Width <= 0)
                return this;

            return CropImage()
                .ChangeDimensions(GetMinorProporcionalSize());
        }

        public FileHelper ChangeResolution()
        {
            if (ResolutionDpi >= 72)
            {
                ImageFile.SetResolution(ResolutionDpi, ResolutionDpi);
                ImageFile = ImageFile;
            }

            return this;
        }

        public FileHelper Validate(bool supressException = false)
        {
            try
            {
                CheckFileSize()
                    .CheckType()
                    .CheckSize();

                IsValid = true;
            }
            catch (Exception)
            {
                if (supressException)
                    IsValid = false;
                else
                    throw;
            }

            return this;
        }

        public FileHelper CheckType()
        {
            if (SupportedFileFormat.HasFlag(SupportedFileFormat.Any))
                return this;

            IsJpegFile(ImageFile).IsPngFile(ImageFile).IsBmpFile(ImageFile).IsGifFile(ImageFile);

            if (!IsValid)
                throw new NotSupportedFileFormatException(ImageFile.RawFormat.ToString());

            return this;
        }

        public FileHelper CheckFileSize()
        {
            if (MaxLength == -1)
                return this;

            if (File.Length > MaxLength)
                throw new MaxFileLengthExceededException(string.Format("{0} MB", MaxLength/ComputerUnit.Mb.ToLong()));

            return this;
        }

        public FileHelper CheckSize()
        {
            if (!ResizeTo.Width.Equals(ImageFile.Width) ||
                !ResizeTo.Height.Equals(ImageFile.Height))
                throw new InvalidImageResolutionException(ImageFile.Width, ImageFile.Height,
                                                          ResizeTo.Width, ResizeTo.Height);

            return this;
        }

        protected FileHelper IsGifFile(Image imageFile)
        {
            if (ImageFormat.Gif.Equals(imageFile.RawFormat))
            {
                if (!SupportedFileFormat.HasFlag(SupportedFileFormat.Gif))
                    throw new NotSupportedFileFormatException(imageFile.RawFormat.ToString());

                IsValid = true;
            }

            return this;
        }

        protected FileHelper IsBmpFile(Image imageFile)
        {
            if (ImageFormat.Bmp.Equals(imageFile.RawFormat))
            {
                if (!SupportedFileFormat.HasFlag(SupportedFileFormat.Bmp))
                    throw new NotSupportedFileFormatException(imageFile.RawFormat.ToString());

                IsValid = true;
            }

            return this;
        }

        protected FileHelper IsPngFile(Image imageFile)
        {
            if (ImageFormat.Png.Equals(imageFile.RawFormat))
            {
                if (!SupportedFileFormat.HasFlag(SupportedFileFormat.Png))
                    throw new NotSupportedFileFormatException(imageFile.RawFormat.ToString());

                IsValid = true;
            }

            return this;
        }

        protected FileHelper IsJpegFile(Image imageFile)
        {
            if (ImageFormat.Jpeg.Equals(imageFile.RawFormat))
            {
                if (!SupportedFileFormat.HasFlag(SupportedFileFormat.Jpeg))
                    throw new NotSupportedFileFormatException(imageFile.RawFormat.ToString());

                IsValid = true;
            }

            return this;
        }

        private Bitmap FrameWithBlackBorder(Image image)
        {
            var finalImage = new Bitmap(ResizeTo.Width, ResizeTo.Height);

            using(var graphicImage = Graphics.FromImage(finalImage))
            {
                graphicImage.FillRectangle(new SolidBrush(IncludeFrameBorder ? Color.Black : Color.Transparent), 0, 0,
                                           finalImage.Width, finalImage.Height);

                var rc = new Rectangle((finalImage.Width - image.Width) / 2, (finalImage.Height - image.Height) / 2, image.Width,
                                       image.Height);

                graphicImage.DrawImage(image, rc);

                image.Dispose();
            }

            return finalImage;
        }

        private FileHelper CropImage()
        {
            if (ImageFile.Height == ImageFile.Width)
                return this;

            if (ImageFile.Height < ResizeTo.Height || ImageFile.Width < ResizeTo.Width)
                ChangeDimensions(GetMajorProporcionalSize());

            var cropArea = new Rectangle((ImageFile.Width - ResizeTo.Width) / 2, (ImageFile.Height - ResizeTo.Height) / 2, ResizeTo.Width,
                                          ResizeTo.Height);

            using (var bmpImage = new Bitmap(ImageFile))
            {
                var bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
                ImageFile = bmpCrop;
            }

            return this;
        }

        private FileHelper ChangeDimensions(Size finalSize)
        {
            using (var finalImage = new Bitmap(finalSize.Width, finalSize.Height))
            {
                var graphicImage = Graphics.FromImage(finalImage);
                graphicImage.InterpolationMode = InterpolationMode.HighQualityBicubic;

                graphicImage.DrawImage(ImageFile, 0, 0, finalSize.Width, finalSize.Height);
                graphicImage.Dispose();

                ImageFile = FrameWithBlackBorder(finalImage);
            }

            return this;
        }

        private Size GetMinorProporcionalSize()
        {
            var finalSize = new Size();

            var sourceWidth = ImageFile.Width;
            var sourceHeight = ImageFile.Height;

            var nPercentW = (float)ResizeTo.Width / sourceWidth;
            var nPercentH = (float)ResizeTo.Height / sourceHeight;

            var nPercent = nPercentH < nPercentW ? nPercentH : nPercentW;

            finalSize.Width = (int)(sourceWidth * nPercent);
            finalSize.Height = (int)(sourceHeight * nPercent);

            return finalSize;
        }

        private Size GetMajorProporcionalSize()
        {
            var finalSize = new Size();

            var sourceWidth = ImageFile.Width;
            var sourceHeight = ImageFile.Height;

            var nPercentW = (float)ResizeTo.Width / sourceWidth;
            var nPercentH = (float)ResizeTo.Height / sourceHeight;

            var nPercent = nPercentH > nPercentW ? nPercentH : nPercentW;

            finalSize.Width = (int)(sourceWidth * nPercent);
            finalSize.Height = (int)(sourceHeight * nPercent);

            return finalSize;
        }

        public void Dispose()
        {
            File = null;

            if (_imageFile != null)
                _imageFile.Dispose();

            _imageFile = null;
        }
    }
}