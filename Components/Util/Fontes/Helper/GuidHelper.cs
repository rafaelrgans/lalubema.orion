﻿using System;

namespace Lalubema.Utilitarios.Helper
{
    public class GuidHelper
    {
        public static string GetGuidBase64String()
        {
            var guid = Guid.NewGuid();

            var guidBase64String = Convert.ToBase64String(guid.ToByteArray());

            guidBase64String = guidBase64String
                .Replace("=", string.Empty)
                .Replace("+", string.Empty)
                .Replace("/", string.Empty)
                .Replace("\\", string.Empty)
                .Replace(".", string.Empty);

            return guidBase64String;
        }

        public static string GetGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}