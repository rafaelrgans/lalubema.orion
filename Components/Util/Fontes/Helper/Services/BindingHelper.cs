﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Lalubema.Utilitarios.Services.Messages;

namespace Lalubema.Utilitarios.Helper.Services
{
    public static class BindingHelper
    {
        public static Binding GetBinding(string bindingType, string bindingName)
        {
            if (string.IsNullOrEmpty(bindingType))
                return new WSHttpBinding(bindingName);

            switch (bindingType)
            {
                case "customBinding":
                    return new CustomBinding(bindingName);
                case "wsHttpBinding":
                    return new WSHttpBinding(bindingName);
                case "webHttpBinding":
                    return new WebHttpBinding(bindingName);
                case "netTcpBinding":
                    return new NetTcpBinding(bindingName);
                case "basicHttpBinding":
                    return new BasicHttpBinding(bindingName);
                default:
                    return new WSHttpBinding(bindingName);
            }
        }

        public static Binding GetBindingFromConfig(string configSection)
        {
            ServiceConfig config = ServiceConfigHelper.GetConfig(configSection);

            string bindingName = config != null ? config.Binding : null;

            return !string.IsNullOrEmpty(bindingName) ? GetBinding(config.BindingType, bindingName) : null;
        }
    }
}