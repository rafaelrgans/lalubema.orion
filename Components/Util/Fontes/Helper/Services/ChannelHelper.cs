﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Lalubema.Utilitarios.Services;
using Lalubema.Utilitarios.Services.Messages;

namespace Lalubema.Utilitarios.Helper.Services
{
    public static class ChannelHelper
    {
        private const string ErrorMessage =
            "Não foi possível abrir o canal para o serviço {0}, verifique as configurações no arquivo de configurações.";


        public static T CreateChannel<T>(string configSection) where T : class
        {
            return CreateChannel<T>(configSection, false);
        }

        public static T CreateChannel<T>(string configSection, bool pingMode) where T : class
        {
            Binding binding = BindingHelper.GetBindingFromConfig(configSection);

            if (binding == null)
                throw new Exception(string.Format(ErrorMessage, configSection));

            if (pingMode)
                ConfigurePingMode(binding);


            ServiceConfig config = ServiceConfigHelper.GetConfig(configSection);
            if (config == null)
                throw new Exception(string.Format(ErrorMessage, configSection));

            var endPoint = new EndpointAddress(config.Address);
            new ChannelFactory<T>(binding, endPoint);

            string address = config.Address;
            int maxItemsInObjectGraph;
            int.TryParse(config.MaxItemsInObjectGraph, out maxItemsInObjectGraph);
            string username = null;
            string password = null;

            UserNameAuthenticationConfig authConfig = UserNameAuthenticationConfig.GetConfig(configSection);

            if (authConfig != null)
            {
                username = authConfig.UserName;
                password = authConfig.Password;
            }

            return CreateChannel<T>(binding, address, username, password, maxItemsInObjectGraph, pingMode);
        }

        public static T CreateChannel<T>(string bindingType, string bindingName, string address, bool pingMode) where T : class
        {
            Binding binding = BindingHelper.GetBinding(bindingType, bindingName);

            if (binding == null)
                throw new Exception("Não foi possível criar o binding.");

            return CreateChannel<T>(binding, address, null, null, 0, pingMode);
        }

        public static T CreateChannel<T>(Binding binding, string address, string username, string password,
                                         int maxItemsInObjectGraph, bool pingMode) where T : class
        {
            if (pingMode)
                ConfigurePingMode(binding);

            var endPoint = new EndpointAddress(address);

            var channel = new ChannelFactory<T>(binding, endPoint);

            if (binding is WebHttpBinding)
                channel.Endpoint.Behaviors.Add(new WebHttpBehavior());

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                if (channel.Credentials != null)
                {
                    channel.Credentials.UserName.UserName = username;
                    channel.Credentials.UserName.Password = password;
                }
            }

            if (maxItemsInObjectGraph != 0)
            {
                foreach (OperationDescription item in channel.Endpoint.Contract.Operations)
                {
                    var behavior =
                        item.Behaviors[typeof (DataContractSerializerOperationBehavior)] as
                        DataContractSerializerOperationBehavior;
                    if (behavior != null)
                        behavior.MaxItemsInObjectGraph = maxItemsInObjectGraph;
                }
                if (binding is WSHttpBinding)
                    ((WSHttpBinding) binding).ReaderQuotas.MaxArrayLength = maxItemsInObjectGraph;
                if (binding is BasicHttpBinding)
                    ((BasicHttpBinding) binding).ReaderQuotas.MaxArrayLength = maxItemsInObjectGraph;
                if (binding is WebHttpBinding)
                    ((WebHttpBinding) binding).ReaderQuotas.MaxArrayLength = maxItemsInObjectGraph;
            }
            return channel.CreateChannel();
        }

        private static void ConfigurePingMode(Binding binding)
        {
            binding.OpenTimeout = new TimeSpan(0, 0, 0, 10);
            binding.ReceiveTimeout = new TimeSpan(0, 0, 0, 10);
            binding.SendTimeout = new TimeSpan(0, 0, 0, 10);
            binding.CloseTimeout = new TimeSpan(0, 0, 0, 10);
        }
    }
}