﻿using System.Collections.Specialized;
using System.Configuration;
using Lalubema.Utilitarios.Services.Messages;

namespace Lalubema.Utilitarios.Helper.Services
{
    public static class ServiceConfigHelper
    {
        public static ServiceConfig GetConfig(string configSection)
        {
            var serviceConfig = new ServiceConfig();
            NameValueCollection configMachine = null;

            var config = ConfigurationManager.GetSection(configSection) as NameValueCollection;
            if (config == null) return null;

            serviceConfig.Binding = config["Binding"];
            serviceConfig.BindingType = config["BindingType"];
            serviceConfig.Address = config["Address"];
            serviceConfig.ValidateReference = config["ValidateReference"];
            serviceConfig.MaxItemsInObjectGraph = config["MaxItemsInObjectGraph"];

            if (!string.IsNullOrEmpty(config["machineKey"]))
                configMachine = ConfigurationManager.GetSection(config["machineKey"]) as NameValueCollection;

            if (configMachine != null)
            {
                if (string.IsNullOrEmpty(serviceConfig.Binding)) serviceConfig.Binding = configMachine["Binding"];
                if (string.IsNullOrEmpty(serviceConfig.BindingType))
                    serviceConfig.BindingType = configMachine["BindingType"];
                if (string.IsNullOrEmpty(serviceConfig.Address)) serviceConfig.Address = configMachine["Address"];
                if (string.IsNullOrEmpty(serviceConfig.ValidateReference))
                    serviceConfig.ValidateReference = configMachine["ValidateReference"];
                if (string.IsNullOrEmpty(serviceConfig.MaxItemsInObjectGraph))
                    serviceConfig.MaxItemsInObjectGraph = configMachine["MaxItemsInObjectGraph"];
            }
            return serviceConfig;
        }
    }
}