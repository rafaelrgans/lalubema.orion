﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Helper
{
    public class FlagsHelper
    {
        public static IList<Type> GetTypes<TFlagType>(object value, Enum flag) where TFlagType : IFlagAttribute
        {
            var properties = value.GetType().GetProperties();

            return (from property in properties
                    where property.IsMarkedWith<TFlagType>()
                    where property.GetAttribute<TFlagType>().HasFlag(flag)
                    select property.PropertyType).ToList();
        }

        public static IList<Object> GetValues<TFlagType>(object value, Enum flag) where TFlagType : IFlagAttribute
        {
            var properties = value.GetType().GetProperties();

            return (from property in properties
                    where property.IsMarkedWith<TFlagType>()
                    where property.GetAttribute<TFlagType>().HasFlag(flag)
                    select property.GetValue(value, null)).ToList();
        }
    }
}