﻿using Common.Logging;

namespace Lalubema.Utilitarios.Helper.Log
{
    public class LogHelper
    {
        #region [ Fields & Properties ]

        private static readonly LogHelper Instancia = new LogHelper();

        private ILog _logger;

        protected ILog Logger
        {
            get { return (_logger ?? (_logger = LogManager.GetLogger("InfoSistema"))); }
        }

        public static LogHelper Instance { get { return Instancia; } }

        #endregion [ Fields & Properties ]

        #region [ Public Methods ]

        public void Logar(string mensagem, LogLevel logLevel = LogLevel.Info)
        {
            switch (logLevel)
            {
                case LogLevel.All:
                case LogLevel.Trace:
                    Logger.Trace(mensagem);
                    break;
                case LogLevel.Debug:
                    Logger.Debug(mensagem);
                    break;
                case LogLevel.Info:
                    Logger.Info(mensagem);
                    break;
                case LogLevel.Warn:
                    Logger.Warn(mensagem);
                    break;
                case LogLevel.Error:
                    Logger.Error(mensagem);
                    break;
                case LogLevel.Fatal:
                    Logger.Fatal(mensagem);
                    break;
                case LogLevel.Off:
                    Logger.Error(mensagem);
                    break;
            }
        }

        #endregion [ Public Methods ]
    }
}