﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Lalubema.Utilitarios.Helper.Communication
{
    public class EmailHelper
    {
        public static void EnviarEmail(EmailSettings emailSettings, string assunto, string mensagem)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = emailSettings.UseSSL;
                smtpClient.Host = emailSettings.ServerName;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(emailSettings.Username, emailSettings.Password);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtpClient.Port = emailSettings.ServerPort > 0 ? emailSettings.ServerPort : 587;

                using (var mailMessage = new MailMessage())
                {
                    mailMessage.Subject = assunto;
                    mailMessage.Priority = MailPriority.Normal;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = mensagem;
                    mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                    mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mailMessage.From = new MailAddress(emailSettings.From);

                    foreach (var email in ObterEnderecosEmail(emailSettings.To))
                        mailMessage.To.Add(email);

                    if (!string.IsNullOrWhiteSpace(emailSettings.Cc))
                        foreach (var email in ObterEnderecosEmail(emailSettings.Cc))
                            mailMessage.CC.Add(email);

                    if (!string.IsNullOrWhiteSpace(emailSettings.Bcc))
                        foreach (var email in ObterEnderecosEmail(emailSettings.Bcc))
                            mailMessage.Bcc.Add(email);

                    smtpClient.Send(mailMessage);
                }
            }
        }

        private static IEnumerable<string> ObterEnderecosEmail(string enderecos)
        {
            return enderecos.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}