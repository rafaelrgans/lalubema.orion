﻿namespace Lalubema.Utilitarios.Helper.Communication
{
    public class EmailSettings
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public string ServerName { get; set; }

        public int ServerPort { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public bool UseSSL { get; set; }
    }
}