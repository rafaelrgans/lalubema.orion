﻿using System;
using Lalubema.Utilitarios.Exceptions.Base;

namespace Lalubema.Utilitarios.Helper.Types
{
    public class DateTimeRange
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public DateTimeRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;

            if (Start > End)
                throw new InvalidDateRangeException();
        }

        public bool Intersects(DateTimeRange test)
        {
            if (Start > End || test.Start > test.End)
                throw new InvalidDateRangeException();

            if (Start == End || test.Start == test.End)
                return false; // No actual date range

            if (Start == test.Start || End == test.End)
                return true; // If any set is the same time, then by default there must be some overlap. 

            if (Start < test.Start)
            {
                if (End > test.Start && End < test.End)
                    return true; // Condition 1

                if (End > test.End)
                    return true; // Condition 3
            }
            else
            {
                if (test.End > Start && test.End < End)
                    return true; // Condition 2

                if (test.End > End)
                    return true; // Condition 4
            }

            return false;
        }
    }
}