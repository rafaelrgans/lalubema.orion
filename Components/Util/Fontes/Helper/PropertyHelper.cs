﻿using System.Collections;
using System.Reflection;
using System.Text;

namespace Lalubema.Utilitarios.Helper
{
    public class PropertyHelper
    {
        public static Property GetProperty(object instance, string sufix)
        {
            Property propertyReturn = null;

            if (!string.IsNullOrWhiteSpace(sufix))
            {
                var properties = sufix.Split('.');

                if (properties.Length > 0)
                {
                    var property = properties[0];

                    var op = "";
                    var subSufix = new StringBuilder();

                    for (var i = 0; i < properties.Length; i++)
                    {
                        if (i > 0)
                        {
                            subSufix.Append(op + properties[i]);

                            if (op.Length == 0)
                                op = ".";
                        }
                    }

                    var list = instance as IList;

                    if (list != null && list.Count > 0)
                        instance = list[0];

                    var type = instance.GetType();

                    PropertyInfo propertyInfo = null;

                    try
                    {
                        propertyInfo = type.GetProperty(property) ??
                                       type.GetProperty(property, BindingFlags.Instance | BindingFlags.NonPublic);
                    }
                    catch
                    {
                        try
                        {
                            if (type.BaseType != null)
                                propertyInfo = type.BaseType.GetProperty(property);
                        }
                        catch
                        {
                            propertyInfo = null;
                        }
                    }

                    if (propertyInfo != null)
                    {
                        var propertyValue = propertyInfo.GetValue(instance, null);

                        if (subSufix.Length > 0 && propertyValue != null)
                            propertyReturn = GetProperty(propertyValue, subSufix.ToString());
                        else
                            propertyReturn = new Property
                                                 {PropertyInfo = propertyInfo, Source = instance, Value = propertyValue};
                    }
                }
            }

            return propertyReturn;
        }

        public static object GetPropertyValue(object instance, string sufix)
        {
            object value = null;

            var property = GetProperty(instance, sufix);

            if (property != null)
                value = property.Value;

            return value;
        }
    }
}