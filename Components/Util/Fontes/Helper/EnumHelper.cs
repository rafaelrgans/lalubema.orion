﻿using System;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Helper
{
    public class EnumHelper
    {
        public static object Parse(Type enumType, string value)
        {
            if (!enumType.IsEnum)
                throw new InvalidOperationException("Invalid operation for the type.");

            var values = Enum.GetValues(enumType);

            foreach (var enumValue in values)
            {
                if (!((Enum) enumValue).IsMarkedWith<StringValueAttribute>())
                    continue;

                var valueAttribute = ((Enum) enumValue).GetAttribute<StringValueAttribute>();

                if (valueAttribute.Value.Equals(value, StringComparison.InvariantCultureIgnoreCase))
                    return enumValue;
            }

            return Enum.GetValues(enumType).GetValue(0);
        }
    }
}