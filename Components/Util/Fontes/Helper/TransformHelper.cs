﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Utilitarios.Helper
{
    internal static class TransformHelper
    {
        internal static Property GetProperty(object instance, string propertyName)
        {
            return PropertyHelper.GetProperty(instance, propertyName);
        }

        internal static Property GetProperty(object instance, MapAttribute[] attributes)
        {
            Property property = null;

            foreach (var map in attributes)
            {
                property = PropertyHelper.GetProperty(instance, map.PropertyName);

                if (property != null)
                    break;
            }

            return property;
        }

        internal static Type GetType(MapAttribute[] attributes)
        {
            Type type = null;

            foreach (var map in attributes)
            {
                var typeMapped = map.Type.Split(',');

                if (typeMapped.Length == 2)
                {
                    var assembly = Assembly.Load(typeMapped[1].Trim());

                    if (assembly != null)
                        type = assembly.GetType(typeMapped[0].Trim());
                }

                if (type != null)
                    break;
            }

            return type;
        }

        internal static object GetCachedObject(object source, Type destiny,
                                               Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (cache.ContainsKey(source))
            {
                var cache2 = cache[source];
                if (cache2.ContainsKey(destiny))
                    return cache2[destiny];
            }

            return null;
        }

        internal static void AddObjectToCache(object source, Type destiny, object createdObject,
                                              Dictionary<object, Dictionary<Type, object>> cache)
        {
            Dictionary<Type, object> cache2;

            if (!cache.ContainsKey(source))
            {
                cache2 = new Dictionary<Type, object>();
                cache[source] = cache2;
            }
            else
            {
                cache2 = cache[source];
            }

            cache2[destiny] = createdObject;
        }

        internal static object[] TransformListToArray(IList list, Type objectType, bool useMapAttribute,
                                                      string noMapValue,
                                                      Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (cache == null)
                cache = new Dictionary<object, Dictionary<Type, object>>();

            var arrayType = objectType.MakeArrayType();
            var returnObject = (object[]) Activator.CreateInstance(arrayType, new object[] {list.Count});

            for (var i = 0; i < list.Count; i++)
            {
                if (objectType.Namespace != null && (!objectType.IsPrimitive && !objectType.Namespace.Equals("System")))
                    returnObject[i] = Transform(list[i], arrayType.GetElementType(), null, useMapAttribute, noMapValue,
                                                cache);
                else
                    returnObject[i] = list[i];
            }

            return returnObject;
        }

        internal static object Transform(object source, Type destiny, object target, bool useMapAttribute,
                                         string noMapValue, Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (source == null)
                return null;

            if (cache == null)
            {
                cache = new Dictionary<object, Dictionary<Type, object>>();
            }
            else
            {
                var cachedObject = GetCachedObject(source, destiny, cache);
                if (cachedObject != null)
                    return cachedObject;
            }

            MapAttribute[] classMaps = null;

            if (useMapAttribute)
                classMaps = (MapAttribute[]) Attribute.GetCustomAttributes(destiny, typeof (MapAttribute));

            if (classMaps != null && classMaps.Length > 0)
                destiny = GetType(classMaps);

            if (destiny.IsInterface)
                destiny = ObjectFactory.GetType(destiny, false);

            var instanceReturn = target ?? Activator.CreateInstance(destiny);

            if (destiny.IsEnum)
                instanceReturn = source;

            //Cache objects
            AddObjectToCache(source, destiny, instanceReturn, cache);

            var destinyProperties = destiny.GetProperties();

            foreach (PropertyInfo destinyProp in destinyProperties)
            {
                try
                {
                    Property property = null;

                    if (useMapAttribute)
                    {
                        var noMapAttributes =
                            Attribute.GetCustomAttributes(destinyProp, typeof (NoMapAttribute)) as NoMapAttribute[];

                        var nomap = false;

                        if (noMapAttributes != null && noMapAttributes.Length > 0)
                            nomap =
                                noMapAttributes.Any(
                                    n => string.IsNullOrEmpty(n.NoMapValue) || n.NoMapValue == noMapValue);

                        if (nomap)
                            continue;

                        var maps = (MapAttribute[]) Attribute.GetCustomAttributes(destinyProp, typeof (MapAttribute));


                        if (maps != null && maps.Length > 0)
                            property = GetProperty(source, maps);
                    }


                    if (property == null)
                        property = GetProperty(source, destinyProp.Name);

                    if (property != null)
                    {
                        var sourcePropertyInfo = property.PropertyInfo;
                        var sourcePropertyValue = property.Value;

                        if (sourcePropertyValue != null)
                        {
                            var namespaces = sourcePropertyInfo.PropertyType.Namespace.Split('.');

                            var sourceRootNamespace = string.Empty;

                            if (namespaces.Length > 0)
                                sourceRootNamespace = namespaces[0];

                            var genericTypes = destinyProp.PropertyType.GetGenericArguments();

                            if (sourcePropertyInfo.PropertyType.IsGenericType && genericTypes.Length > 0)
                                if (destinyProp.PropertyType.GetGenericTypeDefinition() == typeof (Nullable<>))
                                {
                                    destinyProp.SetValue(instanceReturn, sourcePropertyValue, null);
                                }
                                else
                                {
                                    destinyProp.SetValue(instanceReturn,
                                                         TransformList(sourcePropertyValue as IList, genericTypes[0],
                                                                       useMapAttribute, noMapValue, cache), null);
                                }
                            else if (sourcePropertyInfo.PropertyType.IsArray && destinyProp.PropertyType.IsGenericType)
                                destinyProp.SetValue(instanceReturn,
                                                     TransformArrayToGenericList((object[]) sourcePropertyValue,
                                                                                 destinyProp.PropertyType.GetElementType
                                                                                     (), useMapAttribute, noMapValue,
                                                                                 cache), null);
                            else if (destinyProp.PropertyType.IsArray && sourcePropertyInfo.PropertyType.IsGenericType)
                                destinyProp.SetValue(instanceReturn,
                                                     TransformListToArray(sourcePropertyValue as IList,
                                                                          destinyProp.PropertyType.GetElementType(),
                                                                          useMapAttribute, noMapValue, cache), null);
                            else if (destinyProp.PropertyType.IsArray && sourcePropertyInfo.PropertyType.IsArray &&
                                     sourcePropertyValue is Int32[])
                                destinyProp.SetValue(instanceReturn,
                                                     ((Int32[]) sourcePropertyValue).TransformInt32ArrayToInt32Array(),
                                                     null);
                            else if (destinyProp.PropertyType.IsArray &&
                                     destinyProp.PropertyType.GetElementType() == typeof (byte) &&
                                     sourcePropertyInfo.PropertyType.IsArray && sourcePropertyValue is byte[])
                                destinyProp.SetValue(instanceReturn, sourcePropertyValue, null);
                            else if (destinyProp.PropertyType.IsArray && sourcePropertyInfo.PropertyType.IsArray)
                                destinyProp.SetValue(instanceReturn,
                                                     TransformArrayToArray((object[]) sourcePropertyValue,
                                                                           destinyProp.PropertyType.
                                                                               GetElementType(),
                                                                           useMapAttribute, noMapValue,
                                                                           cache), null);
                            else if (!sourcePropertyInfo.PropertyType.IsPrimitive &&
                                     !sourceRootNamespace.Equals("System") &&
                                     !sourcePropertyInfo.PropertyType.IsEnum)
                                destinyProp.SetValue(instanceReturn,
                                                     Transform(sourcePropertyValue,
                                                               destinyProp.PropertyType, null,
                                                               useMapAttribute, noMapValue, cache), null);
                            else if (!(sourcePropertyInfo.PropertyType == typeof (string)) &&
                                     destinyProp.PropertyType == typeof (string))
                                destinyProp.SetValue(instanceReturn, sourcePropertyValue.ToString(),
                                                     null);
                            else
                                destinyProp.SetValue(instanceReturn, sourcePropertyValue, null);
                        }
                        else
                            destinyProp.SetValue(instanceReturn, sourcePropertyValue, null);
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }

            return instanceReturn;
        }

        internal static object[] TransformArrayToArray(IList list, Type objectType, bool useMapAttribute,
                                                       string noMapValue,
                                                       Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (cache == null)
                cache = new Dictionary<object, Dictionary<Type, object>>();

            var arrayType = objectType.MakeArrayType();
            var returnObject = (object[]) Activator.CreateInstance(arrayType, new object[] {list.Count});

            for (var i = 0; i < list.Count; i++)
            {
                if (list[i] != null)
                    returnObject[i] = Transform(list[i], objectType, null, useMapAttribute, noMapValue, cache);
            }


            return returnObject;
        }

        internal static IList TransformList(IEnumerable sourceList, Type destinyType, bool useMapAttribute,
                                            string noMapValue, Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (cache == null)
                cache = new Dictionary<object, Dictionary<Type, object>>();

            var genericType = typeof (List<>).MakeGenericType(destinyType);

            var list = (IList) Activator.CreateInstance(genericType);

            foreach (var source in sourceList)
            {
                if (destinyType.Namespace != null)
                {
                    var namespaces = destinyType.Namespace.Split('.');

                    var rootNamespace = string.Empty;

                    if (namespaces.Length > 0)
                        rootNamespace = namespaces[0];

                    if (!destinyType.IsPrimitive && rootNamespace != "System")
                        list.Add(Transform(source, destinyType, null, useMapAttribute, noMapValue, cache));
                    else
                        list.Add(source);
                }
            }

            return list;
        }

        internal static IList TransformArrayToGenericList(object[] arrayList, Type genericListType, bool useMapAttribute,
                                                          string noMapValue,
                                                          Dictionary<object, Dictionary<Type, object>> cache)
        {
            if (cache == null)
                cache = new Dictionary<object, Dictionary<Type, object>>();

            var arrayType = genericListType.MakeArrayType();
            var arrayObject = (object[]) Activator.CreateInstance(arrayType, new object[] {arrayList.Length});

            for (var i = 0; i < arrayList.Length; i++)
                arrayObject[i] = Transform(arrayList[i], genericListType, null, useMapAttribute, noMapValue, cache);

            var genericType = genericListType.MakeGenericType(genericListType);
            var returnObject = (IList) Activator.CreateInstance(genericType, arrayObject);

            return returnObject;
        }

        internal static object Transform(object source, Type destiny)
        {
            return Transform(source, destiny, true);
        }

        internal static object Transform(object source, Type destiny, bool useMapAttribute)
        {
            return Transform(source, destiny, null, useMapAttribute, null);
        }

        internal static object Transform(object source, Type destiny, object target, bool useMapAttribute,
                                         string noMapValue)
        {
            return Transform(source, destiny, target, useMapAttribute, noMapValue, null);
        }

        internal static object[] TransformListToArray(IList list, Type objectType, bool useMapAttribute,
                                                      string noMapValue)
        {
            return TransformListToArray(list, objectType, useMapAttribute, noMapValue, null);
        }

        internal static object[] TransformArrayToArray(IList list, Type objectType, bool useMapAttribute,
                                                       string noMapValue)
        {
            return TransformArrayToArray(list, objectType, useMapAttribute, noMapValue, null);
        }

        internal static IList TransformList(IEnumerable sourceList, Type destinyType, string noMapValue)
        {
            return TransformList(sourceList, destinyType, true, noMapValue);
        }

        internal static IList TransformList(IEnumerable sourceList, Type destinyType, bool useMapAttribute,
                                            string noMapValue)
        {
            return TransformList(sourceList, destinyType, useMapAttribute, noMapValue, null);
        }

        internal static IList TransformArrayToGenericList(object[] arrayList, Type genericListType, bool useMapAttribute,
                                                          string noMapValue)
        {
            return TransformArrayToGenericList(arrayList, genericListType, useMapAttribute, noMapValue, null);
        }
    }
}