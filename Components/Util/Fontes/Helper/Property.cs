﻿using System.Reflection;

namespace Lalubema.Utilitarios.Helper
{
    public class Property
    {
        public PropertyInfo PropertyInfo { get; set; }

        public object Source { get; set; }

        public object Value { get; set; }
    }
}