﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lalubema.Utilitarios.Attributes;
using System.Collections;
using Spring.Aop.Framework;
using Spring.Aop.Framework.DynamicProxy;

namespace Lalubema.Utilitarios.Spring
{
    public static class ObjectFactory
    {
        #region [ Fields & Properties ]

        private static DependencyContainerEngine _dependencyContainerEngine;

        private static DependencyContainerEngine DependencyContainerEngine
        {
            get { return _dependencyContainerEngine ?? (_dependencyContainerEngine = new DependencyContainerEngine()); }
        }

        #endregion [ Fields & Properties ]

        #region [ Public Methods ]

        public static T ConfigureObject<T>(object target, string name)
        {
            return (T)DependencyContainerEngine.ConfigureObject(target, name);
        }

        public static void Finalize()
        {
            if (DependencyContainerEngine != null)
            {
                DependencyContainerEngine.Dispose();
            }
        }

        public static string GetMessage(string name)
        {
            return DependencyContainerEngine.GetMessage(name);
        }

        public static T GetObject<T>()
        {
            return GetObject<T>(false);
        }

        public static T GetObject<T>(bool isSingleton)
        {
            return GetObject<T>(isSingleton, null);
        }

        public static T GetObject<T>(object[] ctorArgs)
        {
            return GetObject<T>(false, ctorArgs);
        }

        public static T GetObject<T>(string objectId)
        {
            return (T)DependencyContainerEngine.GetObject(objectId);
        }

        public static object GetObject(string objectId)
        {
            return DependencyContainerEngine.GetObject(objectId);
        }

        public static T GetObject<T>(bool isSingleton, object[] ctorArgs)
        {
            T local = default(T);
            Type element = typeof(T);
            var customAttributes =
                (ObjectMapAttribute[])Attribute.GetCustomAttributes(element, typeof(ObjectMapAttribute));
            foreach (ObjectMapAttribute attribute in customAttributes)
            {
                if (attribute.IsSingleton == isSingleton)
                {
                    return (((ctorArgs != null) && (ctorArgs.Length > 0))
                                ? GetObject<T>(attribute.Name, ctorArgs)
                                : GetObject<T>(attribute.Name));
                }
            }
            return local;
        }

        public static T GetObject<T>(string objectId, object[] ctorArgs)
        {
            return (T)DependencyContainerEngine.GetObject(objectId, ctorArgs);
        }

        public static T GetSingleton<T>()
        {
            return GetObject<T>(true);
        }

        public static T GetTarget<T>()
        {
            object proxy = GetObject<T>();
            object target = null;
            if (proxy != null)
            {
                target = GetTarget<T>(proxy);
            }
            return (T)target;
        }

        public static object GetTarget(object proxy)
        {
            object target;

            var advised = proxy as IAdvised;

            if ((advised != null) && (advised.TargetSource != null))
            {
                target = advised.TargetSource.GetTarget();
            }
            else
            {
                target = proxy;
            }

            if (target is AdvisedProxy)
            {
                target = GetTarget(target);
            }

            return target;
        }

        public static T GetTarget<T>(object proxy)
        {
            return (T)GetTarget(proxy);
        }

        public static T GetTarget<T>(string objectId)
        {
            object proxy = GetObject(objectId);
            object target = null;
            if (proxy != null)
            {
                target = GetTarget<T>(proxy);
            }
            return (T)target;
        }

        public static Type GetType<T>(bool isSingleton)
        {
            return GetType(typeof(T), isSingleton);
        }

        public static Type GetType(Type type, bool isSingleton)
        {
            var customAttributes =
                (ObjectMapAttribute[])Attribute.GetCustomAttributes(type, typeof(ObjectMapAttribute));

            return (from attribute in customAttributes
                    where attribute.IsSingleton == isSingleton
                    select DependencyContainerEngine.GetType(attribute.Name)).FirstOrDefault();
        }

        public static void Initialize()
        {
            var dependencyContainerEngine = DependencyContainerEngine;
        }

        public static bool ObjectExists(string objectId)
        {
            return DependencyContainerEngine.ObjectExists(objectId);
        }

        public static void ResolveDependencies(IList targetList)
        {
            foreach (object obj2 in targetList)
            {
                ResolveDependencies(obj2);
            }
        }

        public static void ResolveDependencies(object target)
        {
            DependencyContainerEngine.ResolveDependencies(target);
        }

        #endregion [ Public Methods ]
    }
}
