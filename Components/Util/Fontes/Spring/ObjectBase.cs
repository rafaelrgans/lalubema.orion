﻿using System;
using Common.Logging;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Spring
{
    [Serializable]
    public class ObjectBase
    {
        #region [ Fields & Properties ]

        [NonSerialized]
        private ILog _logger;

        protected ILog Logger
        {
            get { return (_logger ?? (_logger = LogManager.GetLogger(GetType()))); }
        }

        #endregion [ Fields & Properties ]

        #region [ Public Methods ]

        protected T ConfigureObject<T>(object target, string objectId)
        {
            return ObjectFactory.ConfigureObject<T>(target, objectId);
        }

        protected int GetMessageAsInt(string name)
        {
            return GetMessageAsString(name).ToInt32();
        }

        protected short GetMessageAsShort(string resourceID)
        {
            return GetMessageAsString(resourceID).ToShort();
        }

        protected string GetMessageAsString(string name)
        {
            return ObjectFactory.GetMessage(name);
        }

        protected string GetMessageAsString(string name, params object[] parameters)
        {
            return string.Format(GetMessageAsString(name), parameters);
        }

        protected T GetObject<T>()
        {
            return ObjectFactory.GetObject<T>();
        }

        protected T GetObject<T>(object[] ctorArgs)
        {
            return ObjectFactory.GetObject<T>(ctorArgs);
        }

        protected T GetObject<T>(string objectId)
        {
            return ObjectFactory.GetObject<T>(objectId);
        }

        protected T GetObject<T>(string objectId, object[] ctorArgs)
        {
            return ObjectFactory.GetObject<T>(objectId, ctorArgs);
        }

        protected T GetSingleton<T>()
        {
            return ObjectFactory.GetSingleton<T>();
        }

        protected T GetTarget<T>()
        {
            return ObjectFactory.GetTarget<T>();
        }

        protected T GetTarget<T>(object proxy)
        {
            return ObjectFactory.GetTarget<T>(proxy);
        }

        protected T GetTarget<T>(string objectId)
        {
            return ObjectFactory.GetTarget<T>(objectId);
        }

        #endregion [ Public Methods ]
    }
}