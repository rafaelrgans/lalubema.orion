﻿using System;
using Spring.Context;
using Spring.Context.Support;
using Spring.Objects.Factory.Config;
using Spring.Objects.Factory.Support;

namespace Lalubema.Utilitarios.Spring
{
    internal class DependencyContainerEngine : IDisposable
    {
        #region [ Fields & Properties ]

        private IApplicationContext _applicationContext;
        private DefaultListableObjectFactory _dlObjectFactory;

        #endregion [ Fields & Properties ]

        #region [ Constructors ]

        public DependencyContainerEngine()
        {
            LoadConfigurationFile();
        }

        #endregion [ Constructors ]

        #region [ Public Methods ]

        public object ConfigureObject(object target, string name)
        {
            return _dlObjectFactory.ConfigureObject(target, name);
        }

        public string GetMessage(string name)
        {
            try
            {
                return _applicationContext.GetMessage(name);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public object GetObject(string objectId)
        {
            return !ObjectExists(objectId) ? null : _applicationContext.GetObject(objectId);
        }

        public object GetObject(string objectId, object[] ctorArgs)
        {
            return !ObjectExists(objectId) ? null : _applicationContext.GetObject(objectId, ctorArgs);
        }

        public Type GetType(string objectId)
        {
            return _applicationContext.GetType(objectId);
        }

        public bool ObjectExists(string objectId)
        {
            return ((_applicationContext != null) && _applicationContext.ContainsObject(objectId));
        }

        public void ResolveDependencies(object target)
        {
            _dlObjectFactory.AutowireObjectProperties(target, AutoWiringMode.ByType, false);
        }

        #endregion [ Public Methods ]

        #region [ Private Methods ]

        private void LoadConfigurationFile()
        {
            _applicationContext = ContextRegistry.GetContext();
            _dlObjectFactory = new DefaultListableObjectFactory(_applicationContext);
        }

        #endregion [ Private Methods ]

        #region [ Interface Implements ]

        #region IDisposable Members

        public void Dispose()
        {
            if (_dlObjectFactory != null)
            {
                _dlObjectFactory.Dispose();
            }
            if (_applicationContext != null)
            {
                _applicationContext.Dispose();
            }
        }

        #endregion

        #endregion [ Interface Implements ]
    }
}