﻿using System;

namespace Lalubema.Utilitarios.Spring
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class ObjectMapAttribute : Attribute
    {
        #region [ Fields & Properties ]

        public bool IsSingleton { get; set; }

        public string Name { get; set; }

        #endregion [ Fields & Properties ]

        #region [ Constructors ]

        public ObjectMapAttribute(string name) : this(name, false) { }

        public ObjectMapAttribute(string name, bool isSingleton)
        {
            Name = name;
            IsSingleton = isSingleton;
        }

        #endregion [ Constructors ]

        #region [ Methods ]

        public static ObjectMapAttribute GetObjectMapAttribute(Type type)
        {
            ObjectMapAttribute attribute = null;

            var customAttributes = (ObjectMapAttribute[])GetCustomAttributes(type, typeof(ObjectMapAttribute));

            if ((customAttributes != null) && (customAttributes.Length > 0))
                attribute = customAttributes[0];

            return attribute;
        }

        #endregion [ Methods ]
    }
}