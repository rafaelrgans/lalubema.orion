﻿using System;

namespace Lalubema.Utilitarios.Repositorios
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class EntityIdentificatorAttribute : Attribute
    {
        #region [ Constructor ]

        public EntityIdentificatorAttribute(string fieldName)
        {
            FieldName = fieldName;
        }

        #endregion [ Constructor ]

        #region [ Fields & Properties ]

        public string FieldName { get; set; }

        #endregion [ Fields & Properties ]
    }
}