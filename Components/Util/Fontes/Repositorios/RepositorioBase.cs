﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Common.Logging;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Helper.Log;
using Lalubema.Utilitarios.Spring;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Proxy;
using NHibernate.Transform;
using NHibernate.Type;
using Spring.Aop;
using Spring.Aop.Framework;
using Spring.Dao;
using Spring.Data.NHibernate.Generic.Support;

namespace Lalubema.Utilitarios.Repositorios
{
    public class RepositorioBase<T> : HibernateDaoSupport, IRepositorioBase<T> where T : class
    {
        #region IRepositoryBase<T> Members

        public int Count()
        {
            return
                HibernateTemplate.Execute(
                    session =>
                    session.CreateCriteria(typeof(T)).SetProjection(Projections.RowCount()).UniqueResult<int>());
        }

        public object GetEntity(object id)
        {
            return Get(id);
        }

        public T Get(object id)
        {
            var implType = ObjectFactory.GetType<T>(false) ?? typeof(T);

            return Get(implType, id);
        }

        public T Get(Type entityType, object id)
        {
            return HibernateTemplate.Execute(session => (T)session.Get(entityType, id));
        }

        public Dictionary<string, object> ListIdentifiers(object obj)
        {
            var retorno = new Dictionary<string, object>();

            NHibernateUtil.Initialize(obj);

            if (ListIdentifiersAttribute(obj, ref retorno))
                return retorno;

            var entry = GetEntryEntity(ref obj);

            if (string.IsNullOrEmpty(entry.Persister.IdentifierPropertyName) &&
                entry.Persister.IdentifierType is EmbeddedComponentType)
            {
                var fields = ((EmbeddedComponentType)entry.Persister.IdentifierType).PropertyNames;

                foreach (string t in fields)
                    AddIdentifier(ref retorno, t, obj);
            }
            else
            {
                AddIdentifier(ref retorno, entry.Persister.IdentifierPropertyName, obj);
            }

            return retorno;
        }

        public void Update(T domain)
        {
            HibernateTemplate.Update(domain);
        }

        public void Save(T domain, bool flush)
        {
            try
            {
                HibernateTemplate.SaveOrUpdate(domain);

                if (flush)
                    HibernateTemplate.Flush();
            }
            catch (NonUniqueObjectException)
            {
                throw new DuplicateKeyException();
            }
            catch (UncategorizedDataAccessException e)
            {
                var sqlException = e.InnerException as SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2601:
                            throw new DuplicateKeyException();
                        default:
                            LogHelper.Instance.Logar(string.Format("SQL ERROR - {0}: {1}", sqlException.Number, sqlException.Message), LogLevel.Error);
                            break;
                    }
                }

                throw;
            }
        }

        public void Evict(T entity)
        {
            Session.Evict(entity);
        }

        public void Delete(T domain)
        {
            HibernateTemplate.Delete(domain);
        }

        public virtual IList<T> ListAll()
        {
            return HibernateTemplate.LoadAll<T>();
        }

        public IList<T> ListAll(Dictionary<string, object> filter, Dictionary<string, string> sort)
        {
            var criteria = Session.CreateCriteria(typeof (T), "o");
            var alias = new Dictionary<string, Type>();

            criteria = SetFilter(filter, criteria, alias);

            criteria = SetSort(criteria, alias, sort);

            return ExecuteListAll(criteria);
        }

        protected virtual IList<T> ExecuteListAll(ICriteria criteria)
        {
            return criteria.List<T>();
        }

        private static ICriteria SetFilter(Dictionary<string, object> filter, ICriteria criteria, IDictionary<string, Type> alias)
        {
            if(filter != null)
            {
                foreach (var f in filter)
                {
                    var propertyType = typeof(T);

                    var propertyParts = f.Key.Split('.');

                    if(propertyParts.Length == 1)
                    {
                        var propertyInfo = propertyType.GetProperty(f.Key,
                                                                    BindingFlags.IgnoreCase | BindingFlags.Public |
                                                                    BindingFlags.Instance);

                        propertyType = propertyInfo.PropertyType;

                        if(propertyType.IsEnum)
                        {
                            int checkInt;
                            if(int.TryParse(f.Value.ToString(), out checkInt))
                            {
                                criteria = criteria.Add(Restrictions.Eq(string.Format("o.{0}", propertyInfo.Name), Enum.ToObject(propertyType, f.Value.ToInt32())));
                            }
                            else
                            {
                                var enumValue = EnumHelper.Parse(propertyType, f.Value.ToString());

                                criteria = criteria.Add(Restrictions.Eq(string.Format("o.{0}", propertyInfo.Name), enumValue));
                            }
                        }
                        else
                        {
                            criteria = criteria.Add(Restrictions.Eq(string.Format("o.{0}", propertyInfo.Name), Convert.ChangeType(f.Value, propertyType)));
                        }
                    }
                    else
                    {
                        var aliasName = "o";

                        for (var i = 0; i < propertyParts.Length; i++)
                        {
                            var propertyPart = propertyParts[i];

                            if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(IList<>))
                                propertyType = propertyType.GetGenericArguments()[0];

                            var propertyInfo = propertyType.GetProperty(propertyPart,
                                                                        BindingFlags.IgnoreCase | BindingFlags.Public |
                                                                        BindingFlags.Instance);

                            var parentType = propertyType;

                            propertyType = propertyInfo.PropertyType;

                            if (i == propertyParts.Length - 1)
                                criteria =
                                    criteria.Add(Restrictions.Eq(string.Format("{0}.{1}", aliasName, propertyInfo.Name),
                                                                 propertyType.IsEnum
                                                                     ? Enum.ToObject(propertyType, f.Value.ToInt32())
                                                                     : Convert.ChangeType(f.Value, propertyType)));
                            else
                                criteria = GetAliasName(criteria, ref aliasName, propertyPart, alias, parentType);
                        }
                    }
                }
            }
            return criteria;
        }

        private static ICriteria SetSort(ICriteria criteria, IDictionary<string, Type> alias, Dictionary<string, string> sort)
        {
            if(sort != null)
            {
                foreach (var s in sort)
                {
                    var propertyType = typeof(T);

                    var propertyParts = s.Key.Split('.');

                    if (propertyParts.Length == 1)
                    {
                        criteria = criteria.AddOrder(s.Value.ToLower().Equals("desc")
                                                         ? Order.Desc(Projections.Property(string.Format("o.{0}", s.Key)))
                                                         : Order.Asc(Projections.Property(string.Format("o.{0}", s.Key))));
                    }
                    else
                    {
                        var aliasName = "o";

                        for (var i = 0; i < propertyParts.Length; i++)
                        {
                            var propertyPart = propertyParts[i];

                            if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(IList<>))
                                propertyType = propertyType.GetGenericArguments()[0];

                            var propertyInfo = propertyType.GetProperty(propertyPart,
                                                                        BindingFlags.IgnoreCase | BindingFlags.Public |
                                                                        BindingFlags.Instance);

                            var parentType = propertyType;

                            propertyType = propertyInfo.PropertyType;

                            if (i == propertyParts.Length - 1)
                            {
                                criteria = criteria.AddOrder(s.Value.ToLower().Equals("desc")
                                                                 ? Order.Desc(Projections.Property(string.Format("{0}.{1}", aliasName, propertyPart)))
                                                                 : Order.Asc(Projections.Property(string.Format("{0}.{1}", aliasName, propertyPart))));
                            }
                            else
                            {
                                criteria = GetAliasName(criteria, ref aliasName, propertyPart, alias, parentType);
                            }
                        }
                    }
                }
            }

            return criteria;
        }

        private static ICriteria GetAliasName(ICriteria criteria, ref string aliasName, string propertyPart, IDictionary<string, Type> alias, Type parentType)
        {
            var propertyInfo = parentType.GetProperty(propertyPart,
                                                        BindingFlags.IgnoreCase | BindingFlags.Public |
                                                        BindingFlags.Instance);

            var propertyType = propertyInfo.PropertyType;

            if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(IList<>))
                propertyType = propertyType.GetGenericArguments()[0];

            var newAliasName = string.Format("{0}", propertyType.Name.Substring(0, 3).ToLower());

            if (!alias.ContainsKey(newAliasName))
            {
                criteria = criteria.CreateAlias(string.Format("{0}.{1}", aliasName, propertyInfo.Name),
                                                newAliasName);

                alias.Add(newAliasName, propertyType);

                aliasName = newAliasName;
            }
            return criteria;
        }

        public virtual void Load(T entity, object id)
        {
            EvictIfInSession(entity, id);

            HibernateTemplate.Load(entity, id);
        }

        private void EvictIfInSession(T value, object id)
        {
            try
            {
                var pers = Session.GetSessionImplementation()
                    .GetEntityPersister(value.GetType().ToString(), this);

                var key = new EntityKey(id, pers, EntityMode.Poco);

                object entity = Session.GetSessionImplementation().PersistenceContext.GetEntity(key);

                if (entity != null)
                {
                    Session.Evict(entity);
                }
            } catch
            {
                log.Info(string.Format("Object {0}#{1} not in session.", value.GetType().FullName, id));
            }
        }

        #endregion

        public int Contar(DetachedCriteria criteria)
        {
            return Contar(criteria, "Code");
        }

        public int Contar(DetachedCriteria criteria, string field)
        {
            return
                HibernateTemplate.Execute(
                    session =>
                    criteria.GetExecutableCriteria(session).SetProjection(Projections.CountDistinct(field)).UniqueResult
                        <int>());
        }

        public T Get(DetachedCriteria criteria)
        {
            return Get(criteria, null);
        }

        public T Get(DetachedCriteria criteria, int maxResults)
        {
            return Get(criteria, null, maxResults);
        }

        public T Get(DetachedCriteria criteria, IResultTransformer resultTransformer)
        {
            return Get(criteria, resultTransformer, 0);
        }


        public T Get(DetachedCriteria criteria, IResultTransformer resultTransformer, int maxResults)
        {
            return HibernateTemplate.Execute(
                delegate(ISession session)
                {
                    var executableCriteria = criteria.GetExecutableCriteria(session);

                    if (resultTransformer != null)
                        executableCriteria.SetResultTransformer(resultTransformer);

                    if (maxResults != 0)
                        executableCriteria.SetMaxResults(maxResults);

                    return executableCriteria.UniqueResult<T>();
                });
        }

        public K Get<K>(DetachedCriteria criteria)
        {
            return HibernateTemplate.Execute(
                delegate(ISession session)
                {
                    var executableCriteria = criteria.GetExecutableCriteria(session);
                    return executableCriteria.UniqueResult<K>();
                }
                );
        }

        public object Execute(DetachedCriteria criteria)
        {
            return Execute<object>(criteria);
        }

        public K Execute<K>(DetachedCriteria criteria)
        {
            return HibernateTemplate.Execute(session => criteria.GetExecutableCriteria(session).UniqueResult<K>());
        }

        public IList<K> ToList<K>(DetachedCriteria criteria)
        {
            return HibernateTemplate.Execute(session => criteria.GetExecutableCriteria(session).List<K>());
        }

        private bool ListIdentifiersAttribute(object obj, ref Dictionary<string, object> list)
        {
            var identifiers = obj.GetType().GetCustomAttributes(typeof(EntityIdentificatorAttribute), true);

            if (identifiers.Length > 0)
            {
                foreach (var item in identifiers)
                    AddIdentifier(ref list, ((EntityIdentificatorAttribute)item).FieldName, obj);
            }

            return (list.Count > 0);
        }

        private void AddIdentifier(ref Dictionary<string, object> list, string fieldName, object obj)
        {
            var valueField = GetFieldValue(obj.GetType(), fieldName, obj);
            foreach (var itemField in valueField)
                list.Add(itemField.Key, itemField.Value);
        }

        private Dictionary<string, object> GetFieldValue(Type type, string fieldName, object obj)
        {
            var retorno = new Dictionary<string, object>();
            var field = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic);

            if (field != null)
            {
                var value = field.GetValue(obj);

                if (value == null || value.GetType().IsPrimitive)
                {
                    retorno.Add(fieldName, value);

                    return retorno;
                }

                value.GetType().GetProperties()[0].GetValue(value, null);

                var result = ListIdentifiers(value);
                var newResult = new Dictionary<string, object>();

                if (result != null)
                {
                    foreach (var o in result)
                    {
                        newResult.Add(string.Format("{0}.{1}", fieldName, o.Key), o.Value);
                    }
                }

                return newResult;
            }

            return type.BaseType != null ? GetFieldValue(type.BaseType, fieldName, obj) : null;
        }

        private EntityEntry GetEntryEntity(ref object obj)
        {
            EntityEntry entry = null;

            var context = ObjectFactory.GetTarget<SessionImpl>(Session).PersistenceContext;

            if (!AopUtils.IsAopProxy(obj))
            {
                entry = context.GetEntry(obj);
            }
            else if (obj is IAdvised)
            {
                obj = ObjectFactory.GetTarget(obj);
                entry = context.GetEntry(obj);
            }
            else
            {
                var proxy = obj as INHibernateProxy;
                if (proxy != null && proxy.HibernateLazyInitializer != null &&
                    proxy.HibernateLazyInitializer is ITargetSource)
                {
                    obj = ((ITargetSource)proxy.HibernateLazyInitializer).GetTarget();
                    entry = context.GetEntry(obj);
                }
            }

            if (entry == null || entry.Persister == null)
                throw new Exception("NHibernate not initialized");

            return entry;
        }

        public IList<T> ToList(DetachedCriteria criteria)
        {
            return ToList(criteria, null, null);
        }

        public IList<T> ToList(DetachedCriteria criteria, Dictionary<string, LockMode> locks)
        {
            return ToList(criteria, null, locks);
        }


        public IList<T> ToList(DetachedCriteria criteria, IResultTransformer resultTransformer)
        {
            return ToList(criteria, resultTransformer, null);
        }

        public IList<T> ToList(DetachedCriteria criteria, IResultTransformer resultTransformer,
                               Dictionary<string, LockMode> locks)
        {
            return HibernateTemplate.ExecuteFind(
                delegate(ISession session)
                {
                    var executableCriteria = criteria.GetExecutableCriteria(session);

                    if (resultTransformer != null)
                        executableCriteria.SetResultTransformer(resultTransformer);

                    if (locks != null && locks.Count > 0)
                    {
                        foreach (var key in locks)
                            executableCriteria.SetLockMode(key.Key, key.Value);
                    }

                    return executableCriteria.List<T>();
                }
                );
        }


        public IList<T> ToList(string property, object key)
        {
            return ToList<T>(property, key);
        }

        public IList<TEntity> ToList<TEntity>(string property, object key)
        {
            var criteria = DetachedCriteria.For<T>();
            CreateAlias(ref property, criteria);
            return ToList<TEntity>(criteria.Add(Restrictions.Eq(property, key)));
        }

        private static void CreateAlias(ref string pathField, DetachedCriteria criteria)
        {
            if (pathField.Count(i => i == '.') <= 1) return;

            var indiceAlias = 0;
            var aliasName = string.Format("A{0}", indiceAlias);
            var alias = pathField.Split('.');

            foreach (var alia in alias)
            {
                var fieldName = indiceAlias > 0 ? string.Format("{0}.{1}", aliasName, alia) : alia;
                aliasName = string.Format("A{0}", ++indiceAlias);
                criteria.CreateAlias(fieldName, aliasName);
                pathField = pathField.Replace(fieldName, aliasName);
            }
        }

        public IList ToList(DetachedCriteria criteria, int atualPage, int entitiesPerPage, int numberEntities)
        {
            var primeiraEntidade = (atualPage - 1) * entitiesPerPage;

            if (numberEntities > 0)
                entitiesPerPage = numberEntities - primeiraEntidade < entitiesPerPage
                                         ? numberEntities - primeiraEntidade
                                         : entitiesPerPage;

            return
                criteria.GetExecutableCriteria(Session).SetMaxResults(entitiesPerPage).SetFirstResult(
                    primeiraEntidade).List();
        }


        protected void Delete(string query)
        {
            HibernateTemplate.Delete(query);
        }

        protected IQuery CreateQuery(string sql)
        {
            IQuery query = Session.CreateQuery(sql);

            return query;
        }
    }
}