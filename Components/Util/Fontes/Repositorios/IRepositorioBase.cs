﻿using System;
using System.Collections.Generic;

namespace Lalubema.Utilitarios.Repositorios
{
    public interface IRepositorioBase
    {
        Dictionary<string, object> ListIdentifiers(object obj);
        object GetEntity(object id);
    }

    public interface IRepositorioBase<T> : IRepositorioBase
    {
        void Update(T domain);

        void Load(T entity, object id);

        int Count();

        void Delete(T domain);

        IList<T> ListAll();

        IList<T> ListAll(Dictionary<string, object> filter, Dictionary<string, string> sort);

        T Get(object id);

        T Get(Type entityType, object id);

        void Save(T domain, bool flush = false);

        void Evict(T entity);
    }
}
