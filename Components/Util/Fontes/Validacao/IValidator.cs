﻿using Lalubema.Utilitarios.Domain;

namespace Lalubema.Utilitarios.Validacao
{
    public interface IValidator<in TElement>
    {
        void Validate(TElement element, NotificationCollection notifications);
    }
}