﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using ValidationException = Lalubema.Utilitarios.Exceptions.ValidationException;

namespace Lalubema.Utilitarios.Validacao
{
    public class Validator<TElement> : IValidator<TElement>
    {
        #region [ Properties & Fields ]

        private static readonly IEnumerable<PropertyValidator> Validators;

        #endregion [ Properties & Fields ]

        #region [ Constructors ]

        static Validator()
        {
            var properties = typeof (TElement).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            Validators = from property in properties
                         where property.IsMarkedWith<ValidationAttribute>()
                         select new PropertyValidator(property);
        }

        #endregion [ Constructors ]

        #region IValidator<TElement> Members

        public void Validate(TElement element, NotificationCollection notifications)
        {
            Validators.Each(entry => entry.Validate(element, notifications));

            if (notifications.Count > 0)
                throw new ValidationException(notifications);
        }

        #endregion

        #region Nested type: PropertyValidator

        private class PropertyValidator
        {
            private readonly string _displayName;
            private readonly PropertyInfo _property;
            private readonly IEnumerable<ValidationAttribute> _propertyValidators;

            public PropertyValidator(PropertyInfo property)
            {
                _property = property;
                _displayName = GetDisplayName(_property);
                _propertyValidators = GetValidators(_property);
            }

            private static string GetDisplayName(PropertyInfo property)
            {
                return property.IsMarkedWith<DisplayNameAttribute>()
                           ? property.GetAttribute<DisplayNameAttribute>().DisplayName
                           : property.Name;
            }

            private static IEnumerable<ValidationAttribute> GetValidators(PropertyInfo property)
            {
                return property.GetAttributes<ValidationAttribute>();
            }

            public void Validate(TElement element, NotificationCollection notifications)
            {
                var value = _property.GetValue(element, new object[] {});

                _propertyValidators.Each(validator => IsValid(validator, value, notifications));
            }

            private void IsValid(ValidationAttribute validator, object value, ICollection<Notification> notifications)
            {
                if (validator.IsValid(value)) return;

                var formattedErrorMessage = validator.FormatErrorMessage(_displayName);

                notifications.Add(new Notification
                                      {
                                          Message = formattedErrorMessage,
                                          Source = _property.Name,
                                          NotificationType = NotificationType.Error
                                      });
            }
        }

        #endregion
    }
}