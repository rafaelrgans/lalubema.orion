﻿namespace Lalubema.Utilitarios.Services.Messages
{
    public class ServiceConfig
    {
        public string Binding { get; set; }

        public string BindingType { get; set; }

        public string Address { get; set; }

        public string MaxItemsInObjectGraph { get; set; }

        public string ValidateReference { get; set; }
    }
}