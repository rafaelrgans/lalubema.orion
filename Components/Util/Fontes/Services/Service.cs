﻿using System.ServiceModel;

namespace Lalubema.Utilitarios.Services
{
    public class Service
    {
        public Service(ICommunicationObject communicationObject, OperationContextScope scope)
        {
            CommunicationObject = communicationObject;
            Scope = scope;
        }

        public ICommunicationObject CommunicationObject { get; private set; }
        public OperationContextScope Scope { get; private set; }
    }
}