﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Lalubema.Utilitarios.Helper.Services;

namespace Lalubema.Utilitarios.Services
{
    public class ServiceLocator : IDisposable
    {
        private readonly Dictionary<string, Service> _services = new Dictionary<string, Service>();

        #region IDisposable Members

        public void Dispose()
        {
            CloseChannels();
        }

        #endregion

        public static void CloseChannel(Service service)
        {
            try
            {
                if (service.CommunicationObject != null)
                {
                    switch (service.CommunicationObject.State)
                    {
                        case CommunicationState.Created:
                        case CommunicationState.Faulted:
                            service.CommunicationObject.Abort();
                            break;
                        case CommunicationState.Opened:
                        case CommunicationState.Opening:
                            try
                            {
                                service.CommunicationObject.Close();
                            }
                            catch
                            {
                                service.CommunicationObject.Abort();
                            }

                            break;
                    }

                    if (service.Scope != null)
                        service.Scope.Dispose();
                }
            }
            catch
            {
            }
        }

        public void CloseChannels()
        {
            foreach (var service in _services)
                CloseChannel(service.Value);
        }

        public ServiceType GetService<ServiceType>(string section) where ServiceType : class
        {
            return GetService<ServiceType>(section, null);
        }

        public ServiceType GetPingModeService<ServiceType>(string section) where ServiceType : class
        {
            return GetService<ServiceType>(section, true, null);
        }

        private ServiceType GetNonFaultedService<ServiceType>(string section, bool pingMode,
                                                              Dictionary<string, object> namedParameters) where ServiceType : class
        {
            ServiceType typedService = default(ServiceType);
            Service service = null;

            if (_services.ContainsKey(section))
                service = _services[section];

            if (service != null)
            {
                if (service.CommunicationObject.State == CommunicationState.Faulted)
                {
                    CloseChannel(service);
                    _services.Remove(section);
                    typedService = GetService<ServiceType>(section, pingMode, namedParameters);
                }
                else
                {
                    typedService = (ServiceType) service.CommunicationObject;
                }
            }

            return typedService;
        }

        public ServiceType GetService<ServiceType>(string section, Dictionary<string, object> namedParameters) where ServiceType : class
        {
            return GetService<ServiceType>(section, false, namedParameters);
        }

        public ServiceType GetService<ServiceType>(string section, bool pingMode,
                                                   Dictionary<string, object> namedParameters) where ServiceType : class
        {
            ServiceType service;

            OperationContextScope scope;
            if (_services.ContainsKey(section))
            {
                service = GetNonFaultedService<ServiceType>(section, pingMode, namedParameters);
                CreateOperationContextScope(namedParameters, service as IContextChannel, out scope);
            }
            else
            {
                service = ChannelHelper.CreateChannel<ServiceType>(section, pingMode);
                CreateOperationContextScope(namedParameters, service as IContextChannel, out scope);
                _services.Add(section, new Service((ICommunicationObject) service, scope));
            }

            return service;
        }

        private static void CreateOperationContextScope(Dictionary<string, object> namedParameters,
                                                        IContextChannel service,
                                                        out OperationContextScope scope)
        {
            scope = new OperationContextScope(service);
            if (namedParameters != null)
            {
                foreach (var parameter in namedParameters)
                {
                    if (
                        OperationContext.Current.OutgoingMessageHeaders.Count(
                            i => i.Name == parameter.Key && i.Namespace == "lalubema") > 0)
                        OperationContext.Current.OutgoingMessageHeaders.RemoveAll(parameter.Key, "lalubema");
                    MessageHeader message = MessageHeader.CreateHeader(parameter.Key, "lalubema", parameter.Value);
                    OperationContext.Current.OutgoingMessageHeaders.Add(message);
                }
            }
        }
    }
}