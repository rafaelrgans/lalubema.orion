﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Lalubema.Utilitarios.Services
{
    public class UserNameAuthenticationConfig
    {
        private UserNameAuthenticationConfig()
        {
        }

        public string UserName { get; set; }
        public string Password { get; set; }


        public static UserNameAuthenticationConfig GetConfig(string configSection)
        {
            string configString;

            string[] arrConfig = configSection.Split('/');

            if (arrConfig.Length == 2)
                configString = string.Format("{0}/{1}", arrConfig[0], "auth");
            else
                throw new Exception("Formato de configuração de seção inválido.");

            var config = ConfigurationManager.GetSection(configString) as NameValueCollection;

            if (config == null || string.IsNullOrEmpty(config["UserName"]) || string.IsNullOrEmpty(config["Password"]))
                return null;

            return new UserNameAuthenticationConfig
                       {
                           UserName = config["UserName"],
                           Password = config["Password"]
                       };
        }
    }
}