﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Lalubema.Utilitarios.Services
{
    public class KnownTypeProvider
    {
        private static HashSet<Type> _knownTypes;

        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            if (_knownTypes == null || _knownTypes.Count == 0)
                SetKnownTypes();

            return _knownTypes;
        }

        private static void SetKnownTypes()
        {
            _knownTypes = new HashSet<Type>();

            string knownTypeAssemblies = ConfigurationManager.AppSettings["KnownTypeAssemblies"];

            if (string.IsNullOrEmpty(knownTypeAssemblies)) return;

            string[] assemblyNames = knownTypeAssemblies.Split(',');

            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(p => assemblyNames.Contains(p.GetName().Name))
                .ToArray();

            IEnumerable<Type> publicTypes = assemblies.SelectMany(p => p.GetExportedTypes());

            Type[] dataContracts = publicTypes
                .Where(type => type.GetCustomAttributes(typeof (DataContractAttribute), true).Length > 0)
                .Where(type => !type.IsGenericType)
                .ToArray();

            foreach (Type d in dataContracts)
                _knownTypes.Add(d);

            Type listType = typeof (List<>);
            Type[] listsOfContracts = dataContracts.Select(p => listType.MakeGenericType(new[] {p})).ToArray();

            foreach (Type d in listsOfContracts)
                _knownTypes.Add(d);
        }
    }
}