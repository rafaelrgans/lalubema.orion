﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lalubema.Utilitarios.Extensions
{
    public static class TypeExtension
    {
        public static bool IsMarkedWith<TAttribute>(this Type type)
        {
            return type
                       .GetCustomAttributes(typeof(TAttribute), true)
                       .FirstOrDefault() != null;
        }

        public static TAttribute GetAttribute<TAttribute>(this Type type)
        {
            return (TAttribute)type
                                    .GetCustomAttributes(typeof(TAttribute), true)
                                    .FirstOrDefault();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this Type type)
        {
            return type
                       .GetCustomAttributes(typeof(TAttribute), true) as IEnumerable<TAttribute>;
        }

        public static FieldInfo[] GetFields(this Type type, BindingFlags bindingFlags, bool inheritance)
        {
            var fields = type.GetFields(bindingFlags).ToList();
            
            if (inheritance && type.BaseType != null)
                fields.AddRange(GetFields(type.BaseType, bindingFlags, true));

            return fields.ToArray();
        }

        public static bool IsTypeOf(this Type type, Type c)
        {
            return c.IsAssignableFrom(type.IsGenericType ? type.GetGenericArguments()[0] : type);
        }

        public static Array GetValues(this Type type)
        {
            var values = new Dictionary<int, string>();

            if (type.IsTypeOf(typeof (Enum)))
                values = Enum.GetValues(type).Cast<Enum>().ToDictionary(value => value.ToInt32(),
                                                                        value => value.Description());

            return values.ToArray();
        }

        public static IList<PropertyInfo> GetProperties<TAttribute>(this Type type)
        {
            return type.GetProperties().Where(p => p.IsMarkedWith<TAttribute>()).ToList();
        }
    }
}