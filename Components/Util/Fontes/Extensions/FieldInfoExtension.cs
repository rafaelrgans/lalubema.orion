﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lalubema.Utilitarios.Extensions
{
    public static class FieldInfoExtension
    {
        public static bool IsMarkedWith<TAttribute>(this FieldInfo fieldInfo)
        {
            return fieldInfo.GetCustomAttributes(typeof (TAttribute), true).FirstOrDefault() != null;
        }

        public static TAttribute GetAttribute<TAttribute>(this FieldInfo fieldInfo)
        {
            return (TAttribute) fieldInfo.GetCustomAttributes(typeof (TAttribute), true).FirstOrDefault();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this FieldInfo fieldInfo)
        {
            return fieldInfo.GetCustomAttributes(typeof (TAttribute), true) as IEnumerable<TAttribute>;
        }
    }
}