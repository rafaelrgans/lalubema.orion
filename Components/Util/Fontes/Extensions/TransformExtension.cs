﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lalubema.Utilitarios.Helper;

namespace Lalubema.Utilitarios.Extensions
{
    public static class TransformExtension
    {
        public static T Transform<T>(this object source)
        {
            return (T) TransformHelper.Transform(source, typeof (T), false);
        }

        public static T Transform<T>(this object source, bool useMapAttribute)
        {
            return (T)TransformHelper.Transform(source, typeof(T), useMapAttribute);
        }

        public static T Transform<T>(this object source, string noMapValue)
        {
            return (T) TransformHelper.Transform(source, typeof (T), null, true, noMapValue);
        }

        public static T TransformNoMap<T>(this object source)
        {
            return (T) TransformHelper.Transform(source, typeof (T), false);
        }

        public static T Transform<T>(this object source, T target)
        {
            return (T)TransformHelper.Transform(source, target.GetType(), target, true, null);
        }

        public static object Transform(this object source, object target)
        {
            return TransformHelper.Transform(source, target.GetType(), target, true, null);
        }

        public static T[] TransformListToArray<T>(this IList list, bool useMapAttribute, string noMapValue)
        {
            return (TransformHelper.TransformListToArray(list, typeof (T), useMapAttribute, noMapValue) as T[]);
        }

        public static T[] TransformArrayToArray<T>(this object[] array, bool useMapAttribute, string noMapValue)
        {
            return ((TransformHelper.TransformArrayToArray(array, typeof (T), useMapAttribute, noMapValue)) as T[]);
        }

        internal static object TransformInt32ArrayToInt32Array(this Int32[] array)
        {
            var arrayReturn = new Int32[array.Length];

            for (var i = 0; i < array.Length; i++)
                arrayReturn[i] = array[i];

            return arrayReturn;
        }

        public static List<T> TransformArrayToList<T>(this object[] array, bool useMapAttribute, string noMapValue)
        {
            return
                (List<T>) (TransformHelper.TransformArrayToGenericList(array, typeof (T), useMapAttribute, noMapValue));
        }

        public static IList<T> TransformList<T>(this IEnumerable sourceList)
        {
            return (IList<T>) TransformHelper.TransformList(sourceList, typeof (T), true, null);
        }

        public static IList<T> TransformList<T>(this IEnumerable sourceList, bool useMapAttribute, string noMapValue)
        {
            return (IList<T>) TransformHelper.TransformList(sourceList, typeof (T), useMapAttribute, noMapValue);
        }

        public static IList<T> TransformListNoMap<T>(this IEnumerable sourceList)
        {
            return (IList<T>) TransformHelper.TransformList(sourceList, typeof (T), false, null);
        }

        public static IList<T> TransformListNoMap<T>(this IEnumerable sourceList, string noMapValue)
        {
            return (IList<T>) TransformHelper.TransformList(sourceList, typeof (T), false, noMapValue);
        }
    }
}