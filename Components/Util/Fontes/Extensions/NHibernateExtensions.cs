﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;

namespace Lalubema.Utilitarios.Extensions
{
    public static class NHibernateExtensions
    {
        public static ProjectionList GroupByObject<T>(this ISessionFactory sessionFactory, string alias)
        {
            var type = typeof (T);

            var classMetadata = sessionFactory.GetClassMetadata(type);

            var projectionList = Projections.ProjectionList();

            foreach (var propertyInfo in type.GetProperties())
            {
                if ((propertyInfo.PropertyType != typeof(IList<>) && !propertyInfo.PropertyType.IsGenericType) && (classMetadata.PropertyNames.Contains(propertyInfo.Name) || propertyInfo.Name == classMetadata.IdentifierPropertyName))
                {
                    projectionList.Add(
                                       Projections.Alias(
                                                         Projections.GroupProperty(string.Format("{0}.{1}", alias,
                                                                                                 propertyInfo.Name)),
                                                         propertyInfo.Name), propertyInfo.Name);
                }
            }

            return projectionList;
        }
    }
}