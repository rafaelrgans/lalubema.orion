﻿using System;
using System.Collections;
using System.Reflection;

namespace Lalubema.Utilitarios.Extensions
{
    public static class ObjectExtension
    {
        public static Int32 ToInt32(this object value)
        {
            return Convert.ToInt32(value);
        }

        public static short ToShort(this object value)
        {
            return Convert.ToInt16(value);
        }

        public static byte ToByte(this object value)
        {
            return Convert.ToByte(value);
        }

        public static long ToLong(this object value)
        {
            return Convert.ToInt64(value);
        }

        public static int? ToNullableInt32(this object value)
        {
            return value == null || string.IsNullOrEmpty(value.ToString()) ? null : Convert.ToInt32(value) as int?;
        }

        public static decimal? ToNullableDecimal(this string value)
        {
            return value == null || string.IsNullOrEmpty(value) ? null : decimal.Parse(value) as decimal?;
        }

        public static DateTime ToDateTime(this object value)
        {
            return Convert.ToDateTime(value);
        }

        public static DateTime? TryParseDateTime(this object value)
        {
            DateTime valor;
            return value != null && DateTime.TryParse(value.ToString(), out valor) ? (DateTime?) valor : null;
        }

        public static DateTime ToDateTime(this object value, IFormatProvider provider)
        {
            return Convert.ToDateTime(value, provider);
        }

        public static DateTime? ToNullableDateTime(this object value)
        {
            return value == null || string.IsNullOrEmpty(value.ToString())
                       ? null
                       : Convert.ToDateTime(value) as DateTime?;
        }

        public static bool Between (this IComparable obj, IComparable startValue, IComparable endValue)
        {
            return obj.CompareTo(startValue) >= 0 && obj.CompareTo(endValue) <= 0 && startValue.CompareTo(endValue) <= 0;
        }

        public static Boolean ToBoolean(this object value)
        {
            return Convert.ToBoolean(value);
        }

        public static T ToGeneric<T>(this object instance)
        {
            return (T) Convert.ChangeType(instance, typeof (T));
        }

        public static void SetValue(this object instance, string destinyPropertyName, object value, bool transform, bool isList)
        {
            var property = instance.GetType().GetProperty(destinyPropertyName);

            var definedValue = value;

            if (transform)
            {
                MethodInfo transformMethod;

                if (isList)
                {
                    transformMethod =
                        typeof (ObjectExtension).GetMethod("TransformList",
                                                           BindingFlags.Default | BindingFlags.NonPublic |
                                                           BindingFlags.Static).MakeGenericMethod(property.PropertyType.GetGenericArguments()[0]);
                }
                else
                {
                    transformMethod =
                        typeof(ObjectExtension).GetMethod("Transform",
                                                           BindingFlags.Default | BindingFlags.NonPublic |
                                                           BindingFlags.Static).MakeGenericMethod(property.PropertyType);
                }

                definedValue = transformMethod.Invoke(value, new[] { value });
            }

            property.SetValue(instance, definedValue, null);
        }

// ReSharper disable UnusedMember.Local
        private static object TransformList<T>(IList value)
        {
            return value.TransformList<T>();
        }

        private static object Transform<T>(object value)
        {
            return value.Transform<T>();
        }
// ReSharper restore UnusedMember.Local
    }
}