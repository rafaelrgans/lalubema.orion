﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lalubema.Utilitarios.Extensions
{
    public static class PropertyInfoExtension
    {
        public static bool IsMarkedWith<TAttribute>(this PropertyInfo propertyInfo)
        {
            return propertyInfo
                       .GetCustomAttributes(typeof (TAttribute), true)
                       .FirstOrDefault() != null;
        }

        public static TAttribute GetAttribute<TAttribute>(this PropertyInfo propertyInfo)
        {
            return (TAttribute) propertyInfo
                                    .GetCustomAttributes(typeof (TAttribute), true)
                                    .FirstOrDefault();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this PropertyInfo propertyInfo)
        {
            return propertyInfo
                       .GetCustomAttributes(typeof (TAttribute), true) as IEnumerable<TAttribute>;
        }
    }
}