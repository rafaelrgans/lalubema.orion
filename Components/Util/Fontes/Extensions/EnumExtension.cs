﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Utilitarios.Extensions
{
    public static class EnumExtension
    {
        public static bool IsMarkedWith<TAttribute>(this Enum value)
        {
            if (value == null)
                return false;

            return value.GetType().GetField(value.ToString()).IsMarkedWith<TAttribute>();
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : class 
        {
            if (value == null)
                return null;

            return
                (TAttribute)
                value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (TAttribute), true).FirstOrDefault
                    ();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this Enum value)
        {
            return
                value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof (TAttribute), true) as
                IEnumerable<TAttribute>;
        }

        public static string ConvertToString(this Enum value)
        {
            if (!value.IsMarkedWith<StringValueAttribute>())
                throw new NotSupportedException("Enum must be a StringValueAttribute");

            var stringValue = value.GetAttribute<StringValueAttribute>();

            return stringValue.Value;
        }

        public static string Description(this Enum value)
        {
            if (!value.IsMarkedWith<StringValueAttribute>())
                throw new NotSupportedException("Enum must be a StringValueAttribute");

            var stringValue = value.GetAttribute<StringValueAttribute>();

            return stringValue.Description;
        }
    }
}