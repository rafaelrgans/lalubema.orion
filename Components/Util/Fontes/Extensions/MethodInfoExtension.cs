﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lalubema.Utilitarios.Extensions
{
    public static class MethodInfoExtension
    {
        public static bool IsMarkedWith<TAttribute>(this MethodInfo methodInfo)
        {
            return methodInfo
                       .GetCustomAttributes(typeof (TAttribute), true)
                       .FirstOrDefault() != null;
        }

        public static TAttribute GetAttribute<TAttribute>(this MethodInfo methodInfo)
        {
            return (TAttribute) methodInfo
                                    .GetCustomAttributes(typeof (TAttribute), true)
                                    .FirstOrDefault();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this MethodInfo methodInfo)
        {
            return methodInfo
                       .GetCustomAttributes(typeof (TAttribute), true) as IEnumerable<TAttribute>;
        }
    }
}