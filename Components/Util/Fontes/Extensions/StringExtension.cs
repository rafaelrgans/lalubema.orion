﻿using System;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Utilitarios.Extensions
{
    public static class StringExtension
    {
        public static TResult ConvertoToEnum<TResult>(this string value) where TResult : struct
        {
            if (!typeof (TResult).IsEnum)
                throw new NotSupportedException("TResult must be an enum.");

            var enumValues = Enum.GetValues(typeof (TResult));

            foreach (var enumValue in enumValues)
            {
                var enumType = enumValue as Enum;

                if (!enumType.IsMarkedWith<StringValueAttribute>())
                    continue;

                var stringValue = enumType.GetAttribute<StringValueAttribute>();

                if (stringValue.Value.Equals(value) || stringValue.Description.Equals(value))
                    return (TResult) enumValue;
            }

            return default(TResult);
        }

        public static string GetValue(this string value, int indice, string separator)
        {
            var values = value.Split(new[] {separator}, StringSplitOptions.RemoveEmptyEntries);

            return values[indice];
        }

        public static bool IsNumeric(this string value)
        {
            double retNum;

            return Double.TryParse(value, System.Globalization.NumberStyles.Any,
                                   System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        }
    }
}