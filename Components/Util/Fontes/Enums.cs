﻿using System;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Utilitarios
{
    public enum NotificationType
    {
        Alert,
        Error,
        Information
    }

    public enum SexType
    {
        [StringValue("U", "Não selecionado")] Undefined = 2,
        [StringValue("F", "Feminino")] Female = 1,
        [StringValue("M", "Masculino")] Male = 0
    }

    public enum CryptographyType
    {
        [StringValue("U", "Undefined")] Undefined,
        [StringValue("S", "System")] System,
        [StringValue("A", "User")] User
    }

    [Flags]
    public enum FilterTypeFlags
    {
        Delete = 2,
        Read = 4,
        ReadAll = 8,
        Save = 16
    }

    public enum FileType
    {
        [StringValue("F", "Arquivo")] File,
        [StringValue("M", "Música")] Music,
        [StringValue("P", "Foto")] Photo,
        [StringValue("V", "Vídeo")] Video
    }

    [Flags]
    public enum SupportedFileFormat
    {
        Any = 2,
        Jpeg = 4,
        Png = 8,
        Bmp = 16,
        Gif = 32
    }

    public enum PlatformType
    {
        [StringValue("A", "Android")] Android = 1,
        [StringValue("I", "Apple")] Apple = 2,
        [StringValue("U", "Undefined")] Undefined = 0
    }

    public enum ComputerUnit : long
    {
        Bit = 1,
        Byte = Bit * 8,
        Kb = Byte * 1024,
        Mb = Kb * 1024,
        Tb = Mb * 1024
    }
}