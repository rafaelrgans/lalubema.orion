﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Lalubema.Utilitarios.Web.Scaffold.Extensions
{
    public static class PasswordForExtensions
    {
        public static MvcHtmlString PasswordFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                                Expression<Func<TModel, TValue>> expression,
                                                                object htmlAttributes,
                                                                bool custom)
        {
            return PasswordFor(html, expression, new RouteValueDictionary(htmlAttributes), custom);
        }

        public static MvcHtmlString PasswordFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper,
                                                                Expression<Func<TModel, TValue>> expression,
                                                                IDictionary<string, object> htmlAttributes,
                                                                bool custom)
        {
            if (htmlAttributes == null)
                htmlAttributes = new Dictionary<string, object>();

            htmlAttributes.Add("type", "password");
            return htmlHelper.TextBoxFor(expression, htmlAttributes, custom);
        }
    }
}