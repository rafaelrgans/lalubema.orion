﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Web.Scaffold.Extensions
{
    public static class TextBoxExtensions
    {
        public static MvcHtmlString TextBoxFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               object htmlAttributes,
                                                               bool custom)
        {
            return TextBoxFor(html, expression, new RouteValueDictionary(htmlAttributes), custom);
        }

        public static MvcHtmlString TextBoxFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               IDictionary<string, object> htmlAttributes,
                                                               bool custom)
        {
            if (!custom)
                return htmlHelper.TextBoxFor(expression, htmlAttributes);

            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            var fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName);
            var textName = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            var description = metadata.Description ?? textName;

            if (String.IsNullOrEmpty(textName))
                return MvcHtmlString.Empty;

            var propertyInfo = htmlHelper.ViewData.ModelMetadata.ModelType.GetProperty(fullName);

            TagBuilder tagBuilder;
            var isTextArea = false;

            if(propertyInfo.IsMarkedWith<DataTypeAttribute>())
            {
                var dataTypeAttribute = propertyInfo.GetAttribute<DataTypeAttribute>();

                switch (dataTypeAttribute.DataType)
                {
                    case DataType.Custom:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "text");
                        break;
                    case DataType.DateTime:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "datetime");
                        break;
                    case DataType.Date:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "date");
                        break;
                    case DataType.Time:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "time");
                        break;
                    case DataType.Duration:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "time");
                        break;
                    case DataType.PhoneNumber:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "tel");
                        break;
                    case DataType.Currency:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "text");
                        break;
                    case DataType.Text:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "text");
                        break;
                    case DataType.Html:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "text");
                        break;
                    case DataType.MultilineText:
                        tagBuilder = new TagBuilder("textarea");
                        isTextArea = true;
                        break;
                    case DataType.EmailAddress:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "email");
                        break;
                    case DataType.Password:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "password");
                        break;
                    case DataType.Url:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "url");
                        break;
                    case DataType.ImageUrl:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "url");
                        break;
                    default:
                        tagBuilder = new TagBuilder("input");
                        tagBuilder.MergeAttribute("type", "text");
                        break;
                }

                tagBuilder.GenerateId(fullName);
            }
            else
            {
                tagBuilder = new TagBuilder("input");
                tagBuilder.GenerateId(fullName);

                tagBuilder.MergeAttribute("type", "text");
            }
            
            tagBuilder.MergeAttributes(htmlAttributes, true);
            tagBuilder.MergeAttribute("name", fullName, true);
            tagBuilder.MergeAttribute("placeholder", description, true);

            // If there are any errors for a named field, we add the CSS attribute.
            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(fullName, out modelState) && modelState.Errors.Count > 0)
                tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);

            tagBuilder.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(htmlFieldName));

            var value = GetValue(htmlHelper, htmlFieldName, modelState);

            value = FormatValue(propertyInfo, value);

            if (isTextArea)
                tagBuilder.SetInnerText(value);
            else
                tagBuilder.MergeAttribute("value", value);

            var divTagBuilder = new TagBuilder("div");
            divTagBuilder.MergeAttribute("class", "controls");

            divTagBuilder.InnerHtml = tagBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(divTagBuilder.ToString(TagRenderMode.Normal));
        }

        private static string FormatValue(PropertyInfo propertyInfo, string value)
        {
            var propertyType = propertyInfo.PropertyType.IsGenericType
                                    ? propertyInfo.PropertyType.GetGenericArguments()[0]
                                    : propertyInfo.PropertyType;

            if (propertyType == typeof (DateTime))
            {
                if (!string.IsNullOrWhiteSpace(value))
                    value = ((DateTime) Convert.ChangeType(value, propertyType)).ToString("yyyy-MM-dd");
            }
            else if (propertyInfo.IsMarkedWith<DisplayFormatAttribute>())
            {
                var displayFormatAttribute = propertyInfo.GetAttribute<DisplayFormatAttribute>();

                value = string.Format(displayFormatAttribute.DataFormatString,
                                      Convert.ChangeType(value, propertyType));
            }

            return value;
        }

        private static string GetValue<TModel>(HtmlHelper<TModel> htmlHelper, string htmlFieldName, ModelState modelState)
        {
            string value;
            if (htmlHelper.ViewData.TemplateInfo != null && !string.IsNullOrWhiteSpace(htmlHelper.ViewData.TemplateInfo.FormattedModelValue.ToString()))
                value = htmlHelper.ViewData.TemplateInfo.FormattedModelValue.ToString();
            else if (modelState != null && modelState.Value != null && !string.IsNullOrWhiteSpace(modelState.Value.AttemptedValue))
                value = modelState.Value.AttemptedValue;
            else
            {
                if (htmlHelper.ViewData.ModelMetadata.Model != null)
                {
                    var propertyValue =
                        htmlHelper.ViewData.Model.GetType().GetProperty(htmlFieldName).GetValue(
                                                                                                htmlHelper.ViewData.
                                                                                                    Model, null);
                    value = propertyValue == null ? string.Empty : propertyValue.ToString();
                }
                else
                {
                    value = String.Empty;
                }
            }
            return value;
        }
    }
}