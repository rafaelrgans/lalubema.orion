﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Lalubema.Utilitarios.Web.Scaffold.Extensions
{
    public static class CheckBoxForExtensions
    {
        public static MvcHtmlString CheckBoxFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               object htmlAttributes,
                                                               bool custom)
        {
            return CheckBoxFor(html, expression, new RouteValueDictionary(htmlAttributes), custom);
        }

        public static MvcHtmlString CheckBoxFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               IDictionary<string, object> htmlAttributes,
                                                               bool custom)
        {
            if (!custom)
                return htmlHelper.TextBoxFor(expression, htmlAttributes);

            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            var fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName);
            var textName = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            var description = metadata.Description ?? textName;

            if (String.IsNullOrEmpty(textName))
                return MvcHtmlString.Empty;

            var tagBuilder = new TagBuilder("input");
            tagBuilder.GenerateId(fullName);

            tagBuilder.MergeAttribute("type", "checkbox");
            tagBuilder.MergeAttributes(htmlAttributes, true);
            tagBuilder.MergeAttribute("name", fullName, true);

            // If there are any errors for a named field, we add the CSS attribute.
            ModelState modelState;
            if (htmlHelper.ViewData.ModelState.TryGetValue(fullName, out modelState) && modelState.Errors.Count > 0)
                tagBuilder.AddCssClass(HtmlHelper.ValidationInputCssClassName);

            tagBuilder.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(htmlFieldName));

            string value;
            if (htmlHelper.ViewData.TemplateInfo != null)
                value = htmlHelper.ViewData.TemplateInfo.FormattedModelValue.ToString();
            else if (modelState != null && modelState.Value != null)
                value = modelState.Value.AttemptedValue;
            else
                value = htmlHelper.ViewData.ModelMetadata.Model != null
                            ? htmlHelper.ViewData.ModelMetadata.Model.ToString()
                            : String.Empty;

            if (bool.TrueString.Equals(value, StringComparison.InvariantCultureIgnoreCase))
                tagBuilder.MergeAttribute("checked", "checked");

            var labelTagBuilder = new TagBuilder("label");
            labelTagBuilder.MergeAttribute("class", "checkbox inline");

            labelTagBuilder.InnerHtml = tagBuilder.ToString(TagRenderMode.Normal);
            labelTagBuilder.InnerHtml += description;

            return MvcHtmlString.Create(labelTagBuilder.ToString(TagRenderMode.Normal));
        }
    }
}