﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Utilitarios.Web.Scaffold.Extensions
{
    public static class ScaffoldExtension
    {
        public static int GetPrimaryKey(this object @object)
        {
            var primarykey =
                @object.GetType().GetProperties().FirstOrDefault(p => p.IsMarkedWith<KeyAttribute>());

            if (primarykey == null)
                return 0;

            return (int) primarykey.GetValue(@object, null);
        }
    }
}