﻿using System;
using System.Collections.Generic;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Utilitarios.Domain
{
    [Serializable]
    public abstract class DomainBaseGeneric<T, R, Key> : ObjectBase
        where R : class, IRepositorioBase<T>
    {
        [NonSerialized]
        private R _repository;

        public virtual R Repository
        {
            get { return _repository ?? (_repository = GetSingleton<R>()); }
            set { _repository = value; }
        }

        public static R GetRepository()
        {
            return ObjectFactory.GetSingleton<R>();
        }


        public virtual void Load(Key codigo)
        {
            Repository.Load((T)(object)this, codigo);
        }

        public virtual T Get(Key codigo)
        {
            var returnedValue = Repository.Get(GetType(), codigo);

            if (returnedValue == null)
                throw new ItemNotFoundException(codigo, typeof (T).Name);

            return returnedValue;
        }

        public virtual void Save()
        {
            Repository.Save((T)(object)this);
        }

        public virtual void Save(bool flush)
        {
            Repository.Save((T)(object)this, flush);
        }

        public virtual void Delete()
        {
            Repository.Delete((T)(object)this);
        }

        public virtual IList<T> ListAll()
        {
            return Repository.ListAll();
        }
    }
}