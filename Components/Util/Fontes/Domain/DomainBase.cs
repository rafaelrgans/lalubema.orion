﻿using System;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Utilitarios.Domain
{
    [Serializable]
    public abstract class DomainBase<T, R> : DomainBaseGeneric<T, R, int> where R : class, IRepositorioBase<T>
    {
    }
}