﻿using System;

namespace Lalubema.Utilitarios.Domain
{
    [Serializable]
    public class Notification
    {
        public string Source { get; set; }

        public string Message { get; set; }

        public NotificationType NotificationType { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Source, Message);
        }
    }
}