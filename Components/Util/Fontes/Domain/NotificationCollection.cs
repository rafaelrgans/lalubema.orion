﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lalubema.Utilitarios.Domain
{
    [Serializable]
    public class NotificationCollection : List<Notification>
    {
        public bool HasError()
        {
            return Exists(notification => notification.NotificationType == NotificationType.Error);
        }

        public List<Notification> Errors()
        {
            return this
                .Where(notification => notification.NotificationType == NotificationType.Error)
                .ToList();
        }

        public bool HasAlert()
        {
            return Exists(notification => notification.NotificationType == NotificationType.Alert);
        }

        public List<Notification> Alerts()
        {
            return this
                .Where(notification => notification.NotificationType == NotificationType.Alert)
                .ToList();
        }

        public bool HasInformation()
        {
            return Exists(notification => notification.NotificationType == NotificationType.Alert);
        }

        public List<Notification> Informations()
        {
            return this
                .Where(notification => notification.NotificationType == NotificationType.Alert)
                .ToList();
        }

        public override string ToString()
        {
            var toString = new StringBuilder();

            ForEach(notification => toString.AppendLine(notification.Message));

            return toString.ToString();
        }
    }
}