﻿namespace Lalubema.Utilitarios.Exceptions.Base
{
    public class UriFormatException : DomainLayerException
    {
        public UriFormatException()
            : base(102005, Messages.Exceptions.EX_102005)
        {
        }
    }
}