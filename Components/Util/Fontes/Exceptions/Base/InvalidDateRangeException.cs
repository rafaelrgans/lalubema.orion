﻿namespace Lalubema.Utilitarios.Exceptions.Base
{
    public class InvalidDateRangeException : DomainLayerException
    {
        public InvalidDateRangeException()
            : base(102003, Messages.Exceptions.EX_102003)
        {
        }
    }
}