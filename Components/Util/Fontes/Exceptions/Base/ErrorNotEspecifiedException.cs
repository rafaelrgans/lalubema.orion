﻿using System;

namespace Lalubema.Utilitarios.Exceptions.Base
{
    public class ErrorNotEspecifiedException : DomainLayerException
    {
        public ErrorNotEspecifiedException(string message, string stackTrace, Exception innerException)
            : base(999001, string.Format(Messages.Exceptions.EX_999001, message, stackTrace), innerException)
        {
        }
    }
}