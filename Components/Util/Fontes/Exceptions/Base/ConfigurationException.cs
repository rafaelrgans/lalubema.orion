﻿namespace Lalubema.Utilitarios.Exceptions.Base
{
    public class ConfigurationException : DomainLayerException
    {
        public ConfigurationException(string configurationName)
            : base(102004, string.Format(Messages.Exceptions.EX_102003, configurationName))
        {
        }
    }
}