﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Utilitarios.Exceptions
{
    public class ServiceLayerException : BaseException
    {
        public ServiceLayerException()
        {
        }

        public ServiceLayerException(string message) : base(message)
        {
        }

        public ServiceLayerException(Exception innerException) : base(innerException.Message, innerException)
        {
        }

        protected ServiceLayerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ServiceLayerException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}