﻿using System;
using System.Runtime.Serialization;
using Common.Logging;
using Lalubema.Utilitarios.Helper.Log;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Utilitarios.Exceptions
{
    public abstract class BaseException : Exception
    {
        #region [ Properties & Fields ]

        protected object[] Parameters { get; set; }

        public override string Message
        {
            get
            {
                string message = null;

                try
                {
                    var messageKey = GetType().Name;
                    message = ObjectFactory.GetMessage(messageKey);
                }
                catch
                {
                    if (!String.IsNullOrEmpty(base.Message))
                        message = base.Message;
                }

                if (string.IsNullOrEmpty(message))
                    message = base.Message;

                if (Parameters != null && Parameters.Length > 0)
                    message = string.Format(message, Parameters);

                return message;
            }
        }

        #endregion [ Properties & Fields ]

        #region [ Constructors ]

        protected BaseException()
        {
            LogException(null);
        }

        protected BaseException(string message)
            : base(message)
        {
            LogException(null);
        }

        protected BaseException(string message, params object[] parameters)
            : this(message)
        {
            Parameters = parameters;
        }

        protected BaseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            LogException(null);
        }

        protected BaseException(string message, Exception innerException)
            : base(message, innerException)
        {
            LogException(innerException);
        }

        protected BaseException(Exception innerException)
            : base(string.Empty, innerException)
        {
            LogException(innerException);
        }

        #endregion [ Constructors ]

        #region [ Methods ]

        private void LogException(Exception innerException)
        {
            var message = string.Format("{0}\r\nStack Trace: {1}", Message, innerException == null ? StackTrace : innerException.StackTrace);

            LogHelper.Instance.Logar(message, LogLevel.Error);
        }

        #endregion [ Methods ]
    }
}