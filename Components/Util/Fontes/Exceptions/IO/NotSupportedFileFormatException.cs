﻿namespace Lalubema.Utilitarios.Exceptions.IO
{
    public class NotSupportedFileFormatException : DomainLayerException
    {
        public NotSupportedFileFormatException(string unsuportedFileFormat)
            : base(101006, string.Format(Messages.Exceptions.EX_101006, unsuportedFileFormat))
        {
        }
    }
}