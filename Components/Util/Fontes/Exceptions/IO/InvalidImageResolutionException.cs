﻿namespace Lalubema.Utilitarios.Exceptions.IO
{
    public class InvalidImageResolutionException : DomainLayerException
    {
        public InvalidImageResolutionException(float horizontalResolution, float verticalResolution,
                                               float expectedHorizontalResolution, float expectedVerticalResolution)
            : base(
                101007,
                string.Format(Messages.Exceptions.EX_101007, horizontalResolution, verticalResolution,
                              expectedHorizontalResolution, expectedVerticalResolution))
        {
        }
    }
}