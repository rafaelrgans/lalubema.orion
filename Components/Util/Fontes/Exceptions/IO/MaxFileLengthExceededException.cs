﻿namespace Lalubema.Utilitarios.Exceptions.IO
{
    public class MaxFileLengthExceededException : DomainLayerException
    {
        public MaxFileLengthExceededException(string maxSize)
            : base(101005, string.Format(Messages.Exceptions.EX_101005, maxSize))
        {
        }
    }
}