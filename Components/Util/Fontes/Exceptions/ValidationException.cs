﻿using Lalubema.Utilitarios.Domain;

namespace Lalubema.Utilitarios.Exceptions
{
    public class ValidationException : DomainLayerException
    {
        #region [ Properties & Fields ]

        public NotificationCollection Notifications { get; private set; }

        #endregion [ Properties & Fields ]

        #region [ Constructors ]

        public ValidationException(NotificationCollection notifications)
            : base(notifications.ToString())
        {
            Notifications = notifications;
        }

        #endregion [ Constructors ]
    }
}