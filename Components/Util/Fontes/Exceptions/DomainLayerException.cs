﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Utilitarios.Exceptions
{
    public abstract class DomainLayerException : BaseException
    {
        protected DomainLayerException(int messageCode, string message)
            : base(message)
        {
            MessageCode = messageCode;
        }
        protected DomainLayerException(int messageCode, string message, Exception innerException)
            : base(message, innerException)
        {
            MessageCode = messageCode;
        }

        protected DomainLayerException(int messageCode, string message, params object[] parameters)
            : base(message, parameters)
        {
            MessageCode = messageCode;
        }

        protected DomainLayerException(string message, params object[] parameters)
            : base(message, parameters)
        {
        }

        protected DomainLayerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected DomainLayerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DomainLayerException(Exception innerException) : base(innerException)
        {
        }

        public int MessageCode { get; protected set; }
    }
}