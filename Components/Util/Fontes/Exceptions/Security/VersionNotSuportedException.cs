﻿using System;

namespace Lalubema.Utilitarios.Exceptions.Security
{
    public class VersionNotSuportedException : DomainLayerException
    {
        public VersionNotSuportedException()
            : base(301003, Messages.Exceptions.EX_301003)
        {
        }
    }
}