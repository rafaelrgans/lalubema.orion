﻿using System;

namespace Lalubema.Utilitarios.Exceptions.Security
{
    public class CryptographyErrorException : DomainLayerException
    {
        public CryptographyErrorException(Exception innerException)
            : base("Error in the cryptography value.", innerException)
        {
        }

        public CryptographyErrorException()
            : base("Error in the cryptography value.")
        {
        }
    }
}