﻿namespace Lalubema.Utilitarios.Exceptions.Security
{
    public class ExpiredCryptographyKeyException : DomainLayerException
    {
        public ExpiredCryptographyKeyException(string keyAuthentication)
            : base(201003, Messages.Exceptions.EX_201003)
        {
            base.Data.Add("KeyAuthentication", keyAuthentication);
        }
    }
}