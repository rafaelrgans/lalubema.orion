﻿namespace Lalubema.Utilitarios.Exceptions.Security
{
    public class UnauthorizedAccessException : DomainLayerException
    {
        public UnauthorizedAccessException()
            : base(201008, Messages.Exceptions.EX_201008)
        {
        }
    }
}