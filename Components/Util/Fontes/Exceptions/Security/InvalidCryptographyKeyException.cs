﻿namespace Lalubema.Utilitarios.Exceptions.Security
{
    public class InvalidCryptographyKeyException : DomainLayerException
    {
        public InvalidCryptographyKeyException(string keyAuthentication)
            : base(201003, Messages.Exceptions.EX_201003)
        {
            base.Data.Add("KeyAuthentication", keyAuthentication);
        }
    }
}