﻿namespace Lalubema.Utilitarios.Exceptions.Integration
{
    public class ItemNotFoundException : DomainLayerException
    {
        public ItemNotFoundException(object id, string entityName)
            : base(101002, string.Format(Messages.Exceptions.EX_101002, entityName, id))
        {
        }
    }
}