﻿namespace Lalubema.Utilitarios.Exceptions.Integration
{
    public class DuplicateKeyException : DomainLayerException
    {
        public DuplicateKeyException()
            : base(901002, Messages.Exceptions.EX_901002)
        {
        }
    }
}