﻿using System.ServiceModel;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Utilitarios.Wcf
{
    [ServiceContract]
    public interface IServiceBase
    {
        [OperationContract]
        bool Ping();
    }

    public class ServiceBase : ObjectBase, IServiceBase
    {
        public bool Ping()
        {
            return true;
        }
    }
}