﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Lalubema.Orion.Publisher.Models.ActivateAccount>" %>
<%@ Import Namespace="Lalubema.Orion.Publisher.Models" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" ng-app="ConfirmacaoApp">
<head runat="server">
    <title>Ativar conta</title>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
	<link rel="stylesheet" href="../../Content/css/app.css">
	<meta name="viewport" content="initial-scale=1" />
	<meta charset="UTF-8" />
    </style>
</head>
    <body layout="column">
		<md-toolbar layout="row">
		  <h1 class="md-toolbar-tools" layout-align-gt-sm="left">
		  	<img src="https://s3.amazonaws.com/connectt/img/email/logo_connectt.png" />
		  </h1>
		</md-toolbar>

		<div layout="column" layout layout-align="center center" ng-controller="AppCtrl">
			<md-whiteframe class="md-whiteframe-z1" id="blocoFormulario" flex> 
                <% if (!String.IsNullOrWhiteSpace(Model.inviter))
                    { %>
                    <h3>Você foi convidado por <%= Model.inviter %> para utilizar o <span class="green">CONNECTT</span></h3>
                <%} else { %>
                    <h3>Você foi convidado para utilizar o <span class="green">CONNECTT</span></h3>
                <%} %>

				<div layout="column" flex class="camposFormulario">
                    <% using (Html.BeginForm("ActivateAccount", "Account", new { token = this.Request.QueryString["token"] }, FormMethod.Post, new { id = "frm-salvar" })) {%>
                    <input type="hidden" name="isActive" value="<%= Model.isActive %>" />
                    <span>Para continuar complete seu cadastro:</span>
                        <%if (Model.isActive)
                               {%>
                               <label for="name"><%= Model.name %></label>
                            <%}
                               else
                               {%>
					            <md-input-container flex>
                                    <label for="name">Nome</label>
                                    <input type="text" ng-model="usuario.nome" name="name" id="name" placeholder="Digite aqui seu nome"/>
			                    </md-input-container>
			                    <md-input-container flex>
                                    <label for="password">Senha</label>
                                    <input type="password" name="password" placeholder="Digite aqui sua senha" id="password"/>
			                    </md-input-container>
			                    <md-input-container flex>
                                    <label for="password1">Confirmar Senha</label>
                                    <input type="password" name="password1" placeholder="Digite novamente aqui a sua senha" id="password1"/>
			                    </md-input-container>
                            <%} %>

                            <div layout="column" flex class="listaPerguntas">
                                <% if (Model.residence != null) {
                                       int i = 0;
                                       foreach (Residence c in Model.residence)
                                       {%>
						        <div layout="row" flex>
                                    <div layout="column" flex>
                                        <span>Você <%= c.category == "S" ? "é sindico" : c.category == "F" ? "é funcionário" : "mora"%> no condomínio <%= c.name %><%= !String.IsNullOrWhiteSpace(c.number) ? ", na unidade " + c.number : "" %>?</span>
                                    </div>
						        </div>
                                <div layout="row" flex>
                                    <md-button type="button" class="md-raised md-primary" ng-class="{'md-primary': visivel[<%= i %>] != null && !visivel[<%= i %>]}" ng-click="add(<%= i %>); visivel[<%= i %>]=false">Sim</md-button>
                                    <md-button type="button" class="md-raised" ng-class="{'md-primary': visivel[<%= i %>]}" ng-click="remove(<%= i %>); visivel[<%= i %>]=true">Não</md-button>
                                </div>
                                <div class="justificativa" ng-show="visivel[<%= i %>]"> 
                                     <md-input-container flex>
                                        <label>Justifique</label>
                                        <textarea ng-model="justificativa[<%= i %>]" columns="1" md-maxlength="150" maxlength="150" name="residence[<%= i %>].comment" id="comment<%= c.idCondominio %>"></textarea>
                                     </md-input-container>
                                 </div>
                                 <input type="hidden" name="residence[<%= i %>].idCondominio" value="<%= c.idCondominio %>" />
                                 <input type="hidden" name="residence[<%= i %>].idUnidade" value="<%= c.idUnidade %>" />
                                 <input type="hidden" name="residence[<%= i %>].number" value="<%= c.number %>" />
                                 <input type="hidden" name="residence[<%= i %>].activate" id="activate<%= i %>" value="true" />
                                 <input type="hidden" name="residence[<%= i %>].name" value="<%= c.name %>" />
                                       <% i++;
                                       }
                                   } %>
					        </div>
                            <br />
                            <div layout="row" flex>
			                    <md-button class="md-raised md-primary" id="submit">Confirmar</md-button>
                            </div>
                    <% } %>
				</div>

			</md-whiteframe>
		</div>

		<!-- Angular Material Dependencies -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script>
            var app = angular.module('ConfirmacaoApp', ['ngMaterial'])
            .config(function($mdThemingProvider) {
              $mdThemingProvider.theme('default')
                .primaryPalette('green')
                .accentPalette('orange');
            });

            app.controller('AppCtrl', ['$scope', function ($scope) {

                $scope.usuario = {
                    nome: "<%= Model.name %>"
                };
                $scope.add = function (index) {
                    $("#activate" + index).val(true);
                };
                $scope.remove = function (index) {
                    $("#activate" + index).val(false);
                };
            }]);

		    $(document).ready(function () {
		        $("#frm-salvar").submit(function (event) {
		            var data = {
		                password: $("#password").val(),
		                password1: $("#password1").val()
		            };

		            if (data.password == '') {
		                alert("Favor informar sua senha de acesso!");
		                return false;
		            }
		            if (data.password != data.password1) {
		                alert("A confirmacao da senha deve ser igual a senha!");
		                return false;
		            }

		            var r = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
		            if (!r.test(data.password)) {
		                alert("A senha deve conter apenas letras, numeros, caracteres especiais e deve ter entre 6 e 16 caracteres!");
		                return false;
		            }

		            return true;
		        });
		    });
        </script>
    </body>
</html>
