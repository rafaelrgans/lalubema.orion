﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" ng-app="ConfirmacaoApp">
<head id="Head1" runat="server">
    <title>ActivateAccount</title>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
	<link rel="stylesheet" href="../../Content/css/app.css">
	<meta name="viewport" content="initial-scale=1" />
	<meta charset="UTF-8" />
    </style>
</head>
    <body layout="column">
		<md-toolbar layout="row">
		  <h1 class="md-toolbar-tools" layout-align-gt-sm="left">
		  	<img src="https://s3.amazonaws.com/connectt/img/email/logo_connectt.png" />
		  </h1>
		</md-toolbar>

		<div layout="column" layout layout-align="center center" ng-controller="AppCtrl">
			<md-whiteframe class="md-whiteframe-z1" id="blocoFormulario" flex> 
                <h3><%= Model.Message %></h3>
			</md-whiteframe>
		</div>

		<!-- Angular Material Dependencies -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script>
		    var app = angular.module('ConfirmacaoApp', ['ngMaterial'])
            .config(function ($mdThemingProvider) {
                $mdThemingProvider.theme('default')
                .primaryPalette('green')
                .accentPalette('orange');
            });

		    app.controller('AppCtrl', ['$scope', function ($scope) {
		    } ]);
        </script>
    </body>
</html>