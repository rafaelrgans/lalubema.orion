﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Lalubema.Orion.Publisher.Models.RecoveryPassword>" %>
<%@ Import Namespace="Lalubema.Orion.Publisher.Models" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" ng-app="ConfirmacaoApp">
<head id="Head1" runat="server">
    <title>Recuperar senha</title>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
	<link rel="stylesheet" href="../../Content/css/app.css">
	<meta name="viewport" content="initial-scale=1" />
	<meta charset="UTF-8" />
    </style>
</head>
    <body layout="column">
		<md-toolbar layout="row">
		  <h1 class="md-toolbar-tools" layout-align-gt-sm="left">
		  	<img src="https://s3.amazonaws.com/connectt/img/email/logo_connectt.png" />
		  </h1>
		</md-toolbar>

		<div layout="column" layout layout-align="center center" ng-controller="AppCtrl">
			<md-whiteframe class="md-whiteframe-z1" id="blocoFormulario" flex> 
				<div layout="column" flex class="camposFormulario">
                    <% using (Html.BeginForm("RecoveryPassword", "Account", new { token = this.Request.QueryString["token"] }, FormMethod.Post, new { id = "frm-salvar" }))
                       {%>
                    <p style="font-weight:bold;font-size:22px;color:black;">Prezado <%= Model.name%>,</p>
                    <span>Informe sua nova senha e clique em confirmar:</span>
			            <md-input-container flex>
                            <label for="password">Senha</label>
                            <input type="password" name="password" placeholder="Digite aqui sua senha" id="password"/>
			            </md-input-container>
			            <md-input-container flex>
                            <label for="password1">Confirmar Senha</label>
                            <input type="password" name="password1" placeholder="Digite novamente aqui a sua senha" id="password1"/>
			            </md-input-container>
                        <br />
                        <div layout="row" flex>
			                <md-button class="md-raised md-primary" id="submit">Confirmar</md-button>
                        </div>
                    <% } %>
				</div>

			</md-whiteframe>
		</div>

		<!-- Angular Material Dependencies -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script>
		    var app = angular.module('ConfirmacaoApp', ['ngMaterial'])
            .config(function ($mdThemingProvider) {
                $mdThemingProvider.theme('default')
                .primaryPalette('green')
                .accentPalette('orange');
            });

		    app.controller('AppCtrl', ['$scope', function ($scope) {
		    } ]);

		    $(document).ready(function () {
		        $("#frm-salvar").submit(function (event) {
		            var data = {
		                password: $("#password").val(),
		                password1: $("#password1").val()
		            };

		            if (data.password == '') {
		                alert("Favor informar sua senha de acesso!");
		                return false;
		            }
		            if (data.password != data.password1) {
		                alert("A confirmacao da senha deve ser igual a senha!");
		                return false;
		            }

		            var r = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
		            if (!r.test(data.password)) {
		                alert("A senha deve conter apenas letras, numeros, caracteres especiais e deve ter entre 6 e 16 caracteres!");
		                return false;
		            }

		            return true;
		        });
		    });
        </script>
    </body>
</html>
