﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Dynamic;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Publisher.Models;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Common;
using Lalubema.Utilitarios.Helper.Communication;
using System.Net;

namespace Lalubema.Orion.Publisher.Controllers
{
    [HandleError]
    public class AccountController : Controller
    {
        public ActionResult ActivateAccount(string token) {
            var security = new ChaveCriptografadaHelper(token);

            var corporativo = security.CodigoCorporativo.HasValue ? security.CodigoCorporativo.Value : 1;
            var c = corporativo != 1 ? corporativo.ToString() + "/" : "";

            //if(security.isExpired(TimeSpan.FromDays(1)))
            //    return View(c + "AccountError", new Exception("O token informado já está expirado!"));

            ActivateAccount model = new ActivateAccount();
            using(DBCoronaEntities db = new DBCoronaEntities()){
                Pessoa pessoa = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                if (pessoa == null)
                    return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                model.isActive = pessoa.Usuario != null && pessoa.Usuario.DataAtivacao.HasValue;
                model.name = security.EmailUsuario;
                model.inviter = null; //usuario.Pessoa2 != null ? usuario.Pessoa2.Nome : null;
                model.residence = new List<Models.Residence>();
                model.residence.AddRange(pessoa.Funcionarios.Where(w => 
                    w.DataAtivacao.HasValue == false && 
                    ((w.Condominio != null && w.Condominio.Licencas.Count(i => i.IDCondominio == w.IDCondominio && i.IDCorporativo == corporativo) > 0) ||
                    w.UnidadeHabitacional != null && w.UnidadeHabitacional.Edificio.Condominio.Licencas.Count(i => i.IDCondominio == w.IDCondominio && i.IDCorporativo == corporativo) > 0)
                ).Select(j => new Residence() {
                    idCondominio = j.IDCondominio.HasValue ? j.IDCondominio.Value : 0,
                    name = j.IDCondominio.HasValue ? j.Condominio.Nome : "",
                    category = j.Condominio.Funcionarios.FirstOrDefault(w => w.IDPessoa == pessoa.ID).Categoria_tmp
                }).ToList());
                model.residence.AddRange(pessoa.Moradors.Where(w => w.DataAtivacao.HasValue == false && w.UnidadeHabitacional.Edificio.Condominio.Licencas.Count(i => i.IDCondominio == w.UnidadeHabitacional.Edificio.IDCondominio && i.IDCorporativo == corporativo) > 0).Select(i => new Residence()
                {
                    idCondominio = i.UnidadeHabitacional.Edificio.IDCondominio,
                    name = i.UnidadeHabitacional.Edificio.Condominio.Nome,
                    idUnidade = i.IDUnidadeHabitacional,
                    number = i.UnidadeHabitacional.Numero,
                    category = "M"
                }).ToList());
            }

            if (model.isActive && model.residence.Count <= 0)
                return View(c + "AccountError", new Exception("Seu cadastro já está ativo!"));

            return View(c + "ActivateAccount", model);
        }

        [HttpPost]
        public ActionResult ActivateAccount(string token, ActivateAccount formData)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            try
            {
                if (!formData.isActive)
                {
                    if ((String.IsNullOrWhiteSpace(formData.password)) || !formData.password.Equals(formData.password1))
                        return View(c + "AccountError", new Exception("A senha e a confirmação devem ser iguais"));
                }

                //if (security.isExpired(TimeSpan.FromDays(1)))
                //    return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

                using (DBCoronaEntities db = new DBCoronaEntities())
                {
                    Pessoa pessoa = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                    if (pessoa == null)
                        return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                    if (!formData.isActive)
                    {
                        pessoa.Usuario = new Usuario() {
                            Nome = formData.name,
                            Senha = SecurityHelper.EncryptToSHA1(formData.password),
                            DataAtivacao = DateTime.UtcNow,
                            CreatedAt = DateTime.UtcNow,
                            UpdatedAt = DateTime.UtcNow,
                            Status = 1,
                            FalhasAcesso = 0,
                            ImpedirAcesso = true
                        };
                    }

                    if (formData.residence != null)
                    {
                        foreach (Residence residence in formData.residence)
                        {
                            var convite = pessoa.PessoaConvites.FirstOrDefault(w => w.IDCondominio == residence.idCondominio && w.IDUnidadeHabitacional == residence.idUnidade);

                            if (residence.activate)
                            {
                                if (!residence.idUnidade.HasValue)
                                {
                                    var funcionario = pessoa.Funcionarios.FirstOrDefault(w => w.IDCondominio == residence.idCondominio);
                                    funcionario.DataAtivacao = DateTime.UtcNow;
                                    funcionario.Status = 1;
                                    var grupo = funcionario.Categoria_tmp.Equals("S", StringComparison.InvariantCultureIgnoreCase) ? "SINDICO" : (funcionario.Categoria_tmp.Equals("F", StringComparison.InvariantCultureIgnoreCase) ? "FUNCIONARIO" : "PORTEIRO");
                                    pessoa.Usuario.UsuarioGrupoes.Add(new UsuarioGrupo()
                                    {
                                        Condominio = funcionario.Condominio,
                                        UnidadeHabitacional = funcionario.UnidadeHabitacional,
                                        Edificio = funcionario.Edificio,
                                        Grupo = db.Grupoes.FirstOrDefault(w => w.Codigo.Equals(grupo, StringComparison.InvariantCultureIgnoreCase)),
                                        Usuario = pessoa.Usuario,
                                    });
                                }
                                else
                                {
                                    var morador = pessoa.Moradors.FirstOrDefault(w => w.IDUnidadeHabitacional == residence.idUnidade);
                                    morador.DataAtivacao = DateTime.UtcNow;
                                    morador.Status = 1;
                                    var grupo = morador.Responsavel ? "MORADOR_RESPONSAVEL" : "MORADOR";
                                    pessoa.Usuario.UsuarioGrupoes.Add(new UsuarioGrupo()
                                    {
                                        Condominio = morador.UnidadeHabitacional.Edificio.Condominio,
                                        Grupo = db.Grupoes.FirstOrDefault(w => w.Codigo.Equals(grupo, StringComparison.InvariantCultureIgnoreCase)),
                                        Usuario = pessoa.Usuario,
                                        UnidadeHabitacional = morador.UnidadeHabitacional
                                    });

                                    if (convite != null && convite.Permissao != null)
                                    {
                                        var permissoes = convite.Permissao.Split('|');
                                        foreach (var pid in permissoes)
                                        {
                                            var permission = db.Permissaos.FirstOrDefault(w => w.Codigo.Equals(pid, StringComparison.InvariantCultureIgnoreCase));
                                            if (permission != null && !pessoa.Usuario.UsuarioPermissaos.Any(w => w.IDCondominio == morador.UnidadeHabitacional.Edificio.IDCondominio &&
                                                w.IDUnidadeHabitacional == morador.IDUnidadeHabitacional && w.Permissao.ID == permission.ID))
                                            {
                                                pessoa.Usuario.UsuarioPermissaos.Add(new UsuarioPermissao()
                                                {
                                                    Condominio = morador.UnidadeHabitacional.Edificio.Condominio,
                                                    UnidadeHabitacional = morador.UnidadeHabitacional,
                                                    Usuario = pessoa.Usuario,
                                                    Permissao = permission
                                                });
                                            }
                                        }
                                        convite.Status = 1;
                                    }
                                }
                            }
                            else
                            {
                                if(convite != null)
                                    convite.Status = 2;

                                var d = security.CodigoCorporativo.HasValue ? security.CodigoCorporativo.Value : 1;

                                var sindico = (from p in pessoa.Funcionarios
                                              where p.IDCondominio == residence.idCondominio &&
                                              p.Pessoa.Usuario.UsuarioGrupoes.Any(w => w.IDCondominio == residence.idCondominio && w.Grupo.Codigo.Equals("SINDICO", StringComparison.InvariantCultureIgnoreCase))
                                              select p.Pessoa).FirstOrDefault();

                                if (sindico != null)
                                {
                                    var assunto = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_CADASTRO_NAOACEITO, d);
                                    var template = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_CADASTRO_NAOACEITO, d);

                                    var email = sindico.Usuario.Nome;
                                    var nome = sindico.Nome;
                                    var nomecondominio = residence.name;

                                    if (!string.IsNullOrWhiteSpace(residence.number))
                                        nomecondominio += ", na unidade " + residence.number;

                                    EmailHelper.EnviarEmail(GetEmailSettings(email, d), assunto, string.Format(template, nome, pessoa.Nome, nomecondominio, residence.comment));
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                }
                dynamic m = new ExpandoObject();
                m.Message = "Seja bem vindo(a). Seu cadastro foi ativado com sucesso!";
                return View(c + "AccountSuccess", m);
            }
            catch (Exception ex)
            {
                return View(c + "AccountError", ex);
            }
        }

        private static EmailSettings GetEmailSettings(string sendTo, int corporativo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente, corporativo),
                ServerName = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer, corporativo),
                ServerPort = Convert.ToInt32(Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort, corporativo)),
                UseSSL = Convert.ToBoolean(Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL, corporativo)),
                Username = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp, corporativo),
                Password = Lalubema.Orion.Domain.Orion.Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp, corporativo)
            };
        }

        public ActionResult RecoveryPassword(string token)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            if (security.isExpired(TimeSpan.FromDays(1)))
                return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

            RecoveryPassword model = new RecoveryPassword();
            using (DBCoronaEntities db = new DBCoronaEntities())
            {
                Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                if (usuario == null)
                    return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                if (!usuario.Usuario.DataAtivacao.HasValue)
                    return View(c + "AccountError", new Exception("Sua conta ainda nao esta ativada. Por favor ative a conta."));

                model.name = usuario.Nome;
            }

            return View(c + "RecoveryPassword", model);
        }

        [HttpPost]
        public ActionResult RecoveryPassword(string token, RecoveryPassword formData)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            try
            {
                if ((String.IsNullOrWhiteSpace(formData.password)) || !formData.password.Equals(formData.password1))
                    return View(c + "AccountError", new Exception("A senha e confirmacao devem ser iguais"));

                if (security.isExpired(TimeSpan.FromDays(1)))
                    return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

                using (DBCoronaEntities db = new DBCoronaEntities())
                {
                    Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                    if (usuario == null)
                        return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                    usuario.Usuario.Senha = SecurityHelper.EncryptToSHA1(formData.password);

                    db.SaveChanges();
                }
                dynamic model = new ExpandoObject();
                model.Message = "Sua senha foi alterada com sucesso!";
                return View(c + "AccountSuccess", model);
            }
            catch (Exception ex)
            {
                return View(c + "AccountError", ex);
            }
        }
    }
}
