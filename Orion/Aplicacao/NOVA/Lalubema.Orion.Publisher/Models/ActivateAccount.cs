﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lalubema.Orion.Publisher.Models
{
    public class ActivateAccount
    {
        public bool isActive { get; set; }
        public string name { get; set; }
        public List<Residence> residence { get; set; }
        public string inviter { get; set; }
        public string password { get; set; }
        public string password1 { get; set; }
    }

    public class Residence
    {
        public string name { get; set; }
        public string number { get; set; }
        public string comment { get; set; }
        public bool activate { get; set; }
        public int idCondominio { get; set; }
        public int? idUnidade { get; set; }
        public string category { get; set; }
    }
}