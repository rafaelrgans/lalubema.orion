﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Vanadium;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Vanadium
{
	[TestClass]
	public class RelatorioServiceTest :TestBase
	{
		[ClassInitialize]
		public static void Initialize(TestContext testContext)
		{
			ObjectFactory.Initialize();
			NotificationManager.StartServices();
		}

		[ClassCleanup]
		public static void Cleanup()
		{
			NotificationManager.StopServices();
			ObjectFactory.Finalize();
		}

		[TestMethod]
		public void GerarRelatorio()
		{
			// Arrange
			var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();
			var dtInicio = DateTime.Now.Date.AddMonths(2);
			var dtFim = DateTime.Now.Date.AddMonths(-2);

			var relatorioEntrada = new DTORelatorioEntrada
			{
				ChaveCriptografada = chaveCriptografadaGarcom,
				CodEstabelecimento = 2,
				DtInicio = dtInicio,
				DtFim = dtFim,
				ReportType = Common.Tipos.TipoRelatorio.Garcom
			};										   

			// Action
			var result = ObterPedidoService().GerarRelatorio(relatorioEntrada);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
			Assert.IsNotNull(result.OcorreuErro);

			Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.Relatorio.Count > 0"));
			Assert.IsTrue(result.Relatorio.Count > 0);
		}


		private string GetChaveCriptografadaGarcom()
		{
			var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

			var ticketUsuario = new DTOTicketPorUsuario
			{
				ChaveCriptografada = chaveCriptogradaSistema,
				Senha = "a",
				Usuario = "a@a.com",
				Versao = "1.0"
			};

			var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

			var chaveCriptografada = autenticacao.ChaveCriptografada;

			return chaveCriptografada;
		}

		private string GetChaveCriptografadaCliente()
		{
			var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

			var ticketUsuario = new DTOTicketPorUsuario
			{
				ChaveCriptografada = chaveCriptogradaSistema,
				Senha = "ninja",
				Usuario = "bmarcall@gmail.com",
				Versao = "1.0"
			};

			var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

			var chaveCriptografada = autenticacao.ChaveCriptografada;

			return chaveCriptografada;
		}

		private string GetChaveCriptogradaSistema()
		{
			const string sistema = "VANADIUM";
			const string senha = "0?cg@I2h";

			var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

			var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

			return chaveCriptogradaSistema;
		}

		private IRelatorioService ObterPedidoService()
		{
			return ObjectFactory.GetObject<IRelatorioService>(false);
		}

	}
}
