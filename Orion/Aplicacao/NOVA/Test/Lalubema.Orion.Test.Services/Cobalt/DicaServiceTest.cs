﻿using System;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;

namespace Lalubema.Orion.Test.Services.Cobalt
{
    [TestClass]
    public class DicaServiceTest : TestBase
    {
        public const string NomeUsuario = "marcal@lalubema.com";
        public const string Senha = "ninja";

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        // Dica
        [TestMethod]
        public void SalvarDica()
        {
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            // Arrange
            var dtdica = new DTODicaEntrada
            {
                ChaveCriptografada = chaveCriptografada,
                Codigo = 0,
                Titulo = "Título da dica",
                Descricao = "Descrição da dica",
                Ativo = true
            };

            // Action
            DTORetorno dicas = ObterDicaService().SalvarDica(dtdica);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "retorno"));
            Assert.IsNotNull(dtdica);

            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "dicas"));
            Assert.IsNotNull(dtdica.ReturnSaved);
        }

        [TestMethod]
        public void ListarDicas()
        {
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            DTODicasRetorno dt = ObterDicaService().ListarDicas(chaveCriptografada);

            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "lista dicas"));
            Assert.IsNotNull(dt.OcorreuErro);
        }

		[TestMethod]
		public void ListarDica()
		{
			var chaveCriptografada = GetChaveCriptografadaUsuario();

			var dica = new DTODicaEntrada
			{
				ChaveCriptografada = chaveCriptografada,
				Codigo = 1
			};

			DTODicasRetorno dt = ObterDicaService().ListarDica(dica);

			Console.WriteLine(string.Format(AssertMessages.IsNotNull, "lista dicas"));
			Assert.IsNotNull(dt.OcorreuErro);
		}

		[TestMethod]
		public void DeleteDica()
		{
			var chaveCriptografada = GetChaveCriptografadaUsuario();

			var dica = new DTODicaEntrada
			{
				ChaveCriptografada = chaveCriptografada,
				Codigo = 1
			};

			DTORetorno dt = ObterDicaService().ExcluirDica(dica);

			Console.WriteLine(string.Format(AssertMessages.IsNotNull, "delete dicas"));
			Assert.IsNotNull(dt.OcorreuErro);
		}

		private string GetChaveCriptogradaSistema()
        {
            const string sistema = "COBALT";
            const string senha = "31=66MzJ";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private string GetChaveCriptografadaUsuario()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
            {
                ChaveCriptografada = chaveCriptogradaSistema,
                Usuario = NomeUsuario,
                Senha = Senha,
                Versao = "1.0"
            };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }


        private IDicaService ObterDicaService()
        {
            return ObjectFactory.GetObject<IDicaService>(false);
        }

    }
}
