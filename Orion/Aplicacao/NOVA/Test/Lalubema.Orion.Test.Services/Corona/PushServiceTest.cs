﻿using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Test.Services.Corona
{
    
    
    /// <summary>
    ///This is a test class for PushServiceTest and is intended
    ///to contain all PushServiceTest Unit Tests
    ///</summary>
    [TestClass]
    public class PushServiceTest : TestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        #endregion


        /// <summary>
        ///A test for PushChegadaSegura
        ///</summary>
        [TestMethod]
        public void PushChegadaSeguraTest()
        {
            // Arrange
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            var pushChegadaSegura = new DTOSendPush
                                        {
                                            ChaveCriptografada = chaveCriptografada,
                                            Message = "Teste Bruno Marçal",
                                            Origem = 6,
                                            TipoMensagem = Common.Tipos.TipoNotificacao.Delivery
                                        };

            // Action
            var target = ObterPushService();

            var actual = target.SendPush(pushChegadaSegura);

            // Assert
            Assert.AreEqual(GetRetornoOK(), actual.CodigoRetorno);
        }

        private string GetChaveCriptografadaUsuario()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
                                    {
                                        ChaveCriptografada = chaveCriptogradaSistema,
                                        Senha = "a",
                                        Usuario = "teste@lalubema.com",
                                        Versao = "1.0"
                                    };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "CORONA";
            const string senha = "5!z[B%J[";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private DTORetorno GetRetornoOK()
        {
            var expected = new DTORetorno
            {
                DescricaoRetorno = "OK"
            };
            return expected;
        }

        [TestMethod]
        public void GetPushTest()
        {
            // Credencial de Segurança
            var chaveCriptografadaUsuario = GetChaveCriptografadaUsuario();

            // Action
            var target = ObterPushService();
            var actual = target.GetPush(chaveCriptografadaUsuario, 1);

            // Assert
            Assert.AreEqual(false, actual.OcorreuErro);
        }

    }
}
