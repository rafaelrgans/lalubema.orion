﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_DisponibilidadeLocalBemRepositorio", true)]
    public interface IDisponibilidadeLocalBemRepositorio<T> : IRepositorioBase<T>
    {
    }
}