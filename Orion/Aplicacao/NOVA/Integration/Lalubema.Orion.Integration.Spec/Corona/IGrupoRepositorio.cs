﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_GrupoRepositorio", true)]
    public interface IGrupoRepositorio<T> : IRepositorioBase<T>
    {
        T Get(string code);
    }
}