﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_VisitanteRepositorio", true)]
    public interface IVisitanteRepositorio<T> : IRepositorioBase<T>
    {
    }
}