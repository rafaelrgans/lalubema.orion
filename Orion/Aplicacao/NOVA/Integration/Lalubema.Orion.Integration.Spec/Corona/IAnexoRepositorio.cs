﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_AnexoRepositorio", true)]
    public interface IAnexoRepositorio<T> : IRepositorioBase<T>
    {
    }
}