﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_VeiculoRepositorio", true)]
    public interface IVeiculoRepositorio<T> : IRepositorioBase<T>
    {
    }
}