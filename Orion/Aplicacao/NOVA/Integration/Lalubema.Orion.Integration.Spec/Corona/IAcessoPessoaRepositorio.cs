﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_AcessoPessoaRepositorio", true)]
    public interface IAcessoPessoaRepositorio<T> : IRepositorioBase<T>
    {
    }
}