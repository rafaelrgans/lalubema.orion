﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_AutorizacaoEntradaEntregaRepositorio", true)]
    public interface IAutorizacaoEntradaEntregaRepositorio<T> : IRepositorioBase<T>
    {
    }
}