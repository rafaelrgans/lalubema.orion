﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_LocalBemRepositorio", true)]
    public interface ILocalBemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoEdificio);
    }
}