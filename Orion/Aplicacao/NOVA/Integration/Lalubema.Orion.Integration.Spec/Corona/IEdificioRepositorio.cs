﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_EdificioRepositorio", true)]
    public interface IEdificioRepositorio<T> : IRepositorioBase<T>
    {
    }
}