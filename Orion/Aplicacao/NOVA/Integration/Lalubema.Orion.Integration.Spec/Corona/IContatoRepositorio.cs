﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ContatoRepositorio", true)]
    public interface IContatoRepositorio<T> : IRepositorioBase<T>
    {
    }
}