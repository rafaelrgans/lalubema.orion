﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_EdificioContatoRepositorio", true)]
    public interface IEdificioContatoRepositorio<T> : IRepositorioBase<T>
    {
    }
}