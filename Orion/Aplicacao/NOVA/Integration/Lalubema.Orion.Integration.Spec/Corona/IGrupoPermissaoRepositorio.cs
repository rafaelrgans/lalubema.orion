﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_GrupoPermissaoRepositorio", true)]
    public interface IGrupoPermissaoRepositorio<T> : IRepositorioBase<T>
    {
    }
}