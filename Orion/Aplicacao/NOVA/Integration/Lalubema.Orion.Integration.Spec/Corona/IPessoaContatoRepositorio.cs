﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PessoaContatoRepositorio", true)]
    public interface IPessoaContatoRepositorio<T> : IRepositorioBase<T>
    {
    }
}