﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_UsuarioPermissaoRepositorio", true)]
    public interface IUsuarioPermissaoRepositorio<T> : IRepositorioBase<T>
    {
    }
}