﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PessoaConviteRepositorio", true)]
    public interface IPessoaConviteRepositorio<T> : IRepositorioBase<T>
    {
    }
}