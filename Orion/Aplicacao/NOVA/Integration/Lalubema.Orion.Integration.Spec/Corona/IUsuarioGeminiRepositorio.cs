﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_UsuarioRepositorio", true)]
    public interface IUsuarioGeminiRepositorio<T> : IRepositorioBase<T>
    {
    }
}