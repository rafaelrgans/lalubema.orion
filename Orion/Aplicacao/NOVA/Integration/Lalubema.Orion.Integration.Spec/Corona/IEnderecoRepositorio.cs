﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_EnderecoRepositorio", true)]
    public interface IEnderecoRepositorio<T> : IRepositorioBase<T>
    {
    }
}