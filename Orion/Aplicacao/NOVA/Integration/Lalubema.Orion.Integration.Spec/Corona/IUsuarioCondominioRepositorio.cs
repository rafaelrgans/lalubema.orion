﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_UsuarioCondominioRepositorio", true)]
    public interface IUsuarioCondominioRepositorio<T> : IRepositorioBase<T>
    {
    }
}