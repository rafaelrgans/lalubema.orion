﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PessoaRepositorio", true)]
    public interface IPessoaRepositorio<T> : IRepositorioBase<T>
    {
        int? Corporativo { get; set; }

        T Get(string username);

        T Get(string usuario, string senha);

        T Get(string username, int nonCodigo);
    }
}