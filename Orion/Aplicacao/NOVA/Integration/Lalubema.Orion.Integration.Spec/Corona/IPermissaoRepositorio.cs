﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PermissaoRepositorio", true)]
    public interface IPermissaoRepositorio<T> : IRepositorioBase<T>
    {
        T Get(string code);
    }
}