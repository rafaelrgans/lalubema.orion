﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_PaisRepositorio", true)]
    public interface IPaisRepositorio<T> : IRepositorioBase<T>
    {
    }
}