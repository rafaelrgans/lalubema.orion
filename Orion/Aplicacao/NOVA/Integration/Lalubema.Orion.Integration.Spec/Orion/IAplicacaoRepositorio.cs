﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_AplicacaoRepositorio", true)]
    public interface IAplicacaoRepositorio<T> : IRepositorioBase<T>
    {
        T Buscar(string nomeAplicacao);
    }
}