﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_CidadeRepositorio", true)]
    public interface ICidadeRepositorio<T> : IRepositorioBase<T>
    {
    }
}