﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_ParametroRepositorio", true)]
    public interface IParametroRepositorio<T> : IRepositorioBase<T>
    {
        T BuscarPorChave(string nomeParametro, int corporativo);

        T Get(string nomeParametro, int corporativo);
    }
}