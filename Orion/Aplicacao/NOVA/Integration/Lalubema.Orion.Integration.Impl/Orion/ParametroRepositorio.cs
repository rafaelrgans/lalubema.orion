﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace Lalubema.Orion.Integration.Impl.Orion
{
    public class ParametroRepositorio : RepositorioBase<Parametro>, IParametroRepositorio<Parametro>
    {
        public Parametro BuscarPorChave(string nomeParametro, int corporativo)
        {
            var filtro = nomeParametro.Split('.');

            var criteria = DetachedCriteria.For<Parametro>("p")
                .Add(Restrictions.Eq("p.Nome", filtro[filtro.Length - 1]))
                .Add(Restrictions.Eq("p.Corporativo.Codigo", corporativo));

            var parametros = ToList(criteria);

            foreach (var parametro in parametros)
            {
                var parametroAtual = parametro;

                for (var i = filtro.Length - 1; i >= 0; i--)
                {
                    if (!parametroAtual.Nome.Equals(filtro[i], StringComparison.InvariantCultureIgnoreCase))
                        break;

                    if (i == 0 && IsRoot(parametroAtual.Pai))
                        return parametro;

                    if (parametro.Pai != null)
                        parametroAtual = parametroAtual.Pai;
                }
            }

            return null;
        }

        private static bool IsRoot(Parametro parametro)
        {
            return parametro != null && parametro.Pai == null;
        }

        public Parametro Get(string nomeParametro, int corporativo)
        {
            const string sql = @"WITH Result(ID, Codigo, IDParametroPai, Conteudo, IDCorporativo) AS 
	                            (
		                            SELECT
			                            ID, Codigo, IDParametroPai, Conteudo, IDCorporativo
		                            FROM
			                            Parametro
		                            WHERE
			                            Nome Like :nomeParametro AND IDCorporativo = :corporativo
		
		                            UNION ALL
		
		                            SELECT
			                            p.ID, p.Codigo, p.IDParametroPai, p.Conteudo, p.IDCorporativo
		                            FROM
			                            Parametro as p, Result
		                            WHERE
			                            Result.IDParametroPai = p.ID
	                            )
                            SELECT DISTINCT
	                            ID, Codigo, Conteudo, IDCorporativo
                            FROM
	                            Result
                            ORDER BY
	                            Result.ID";

            var filtro = nomeParametro.Split('.');

            var parametros =
                Session.CreateSQLQuery(sql)
                .SetParameter("nomeParametro", filtro[filtro.Length - 1])
                .SetParameter("corporativo", corporativo)
                .SetResultTransformer(new AliasToBeanResultTransformer(typeof (Parametro)))
                .List<Parametro>();

            foreach (var parametro in parametros)
            {
                var parametroAtual = parametro;

                for (var i = filtro.Length - 1; i >= 0; i--)
                {
                    if(!parametroAtual.Nome.Equals(filtro[i], StringComparison.InvariantCultureIgnoreCase))
                        break;

                    if (i == 0)
                        return parametro;

                    if(parametro.Pai != null)
                        parametroAtual = parametroAtual.Pai;
                }
            }

            return null;
        }

    }
}