﻿using System;
using System.Collections.Generic;
using Log = Common.Logging;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Integration.Spec.Orion.Notification;
using Lalubema.Utilitarios.Helper.Log;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System.IO;
using Newtonsoft.Json;

namespace Lalubema.Orion.Integration.Impl.Orion.Notification
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly Dictionary<string, PushBroker> _pushServices = new Dictionary<string, PushBroker>();

        public void SendPush(DTOPushMessage pushMessage)
        {
            if (pushMessage == null)
                throw new Exception();

            var pushService = _pushServices[pushMessage.Bundle];

            switch (pushMessage.PlatformType)
            {
                case Utilitarios.PlatformType.Apple:
                    QueueAppleNotification(pushMessage, pushService);
                    break;
                case Utilitarios.PlatformType.Android:
                    QueueAndroidNotification(pushMessage, pushService);
                    break;
            }
        }

        public void StartServices(DTONotificationSettings settings)
        {
            PushBroker pushService = null;

            if (_pushServices.ContainsKey(settings.Bundle))
                pushService = _pushServices[settings.Bundle];

            if (pushService == null)
            {
                pushService = new PushBroker();

                pushService.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                pushService.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                pushService.OnChannelException += ChannelException;
                pushService.OnNotificationFailed += NotificationFailed;

                _pushServices.Add(settings.Bundle, pushService);
            }

            if (!string.IsNullOrWhiteSpace(settings.AndroidAccessKey))
            {
                pushService.StopAllServices(false);

                pushService.RegisterGcmService(new GcmPushChannelSettings(settings.AndroidSenderId, settings.AndroidAccessKey, settings.Bundle));
            }

            if (!string.IsNullOrWhiteSpace(settings.AppleCertificate))
            {
                pushService.StopAllServices(false);

                try
                {
                    pushService.RegisterAppleService(new ApplePushChannelSettings(true, settings.AppleCertificate, settings.AppleCertificatePassword, true));
                }
                catch { }
            }
        }

        public void StopServices()
        {
            foreach (var pushService in _pushServices)
                pushService.Value.StopAllServices();
        }

        private static void QueueAndroidNotification(DTOPushMessage pushMessage, PushBroker push)
        {
            if (pushMessage == null) return;

            var args = JsonConvert.SerializeObject(pushMessage.LocalizedArgs);
            var notification =
                new GcmNotification()
                                   .ForDeviceRegistrationId(pushMessage.DeviceToken)
                                   .WithCollapseKey("NONE")
                                   .WithJson("{\"alert\":\"" + pushMessage.Message + "\",\"badge\":\"7\",\"localizedKey\":\"" +
                                   pushMessage.LocalizedKey + "\",\"localizedArgs\":" + args + ", \"sound\":\"singlebeel.wav\"}");

            push.QueueNotification(notification);
        }

        private static void QueueAppleNotification(DTOPushMessage pushMessage, PushBroker push)
        {
            if (pushMessage == null) return;

            var notification =
                new AppleNotification()
                                   .ForDeviceToken(pushMessage.DeviceToken)
                                   .WithPayload(new AppleNotificationPayload
                                                    {
                                                        Badge = pushMessage.Badge,
                                                        Sound = pushMessage.Sound,
                                                        Alert = new AppleNotificationAlert
                                                                    {
                                                                        Body = pushMessage.Message,
                                                                        LocalizedKey = pushMessage.LocalizedKey,
                                                                        LocalizedArgs = pushMessage.LocalizedArgs
                                                                    }
                                                    });

            push.QueueNotification(notification);

            LogHelper.Instance.Logar(string.Format("NotificationSended\r\n{0}\r\n{1} - {2}", notification, pushMessage.DeviceToken, DateTime.Now.ToString()), Log.LogLevel.Error);
        }

        private void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            var exception = notificationFailureException as NotificationFailureException;

            if(exception == null)
            {
                LogHelper.Instance.Logar(
                    string.Format("NotificationFailed\r\n{0}\r\n{1} - {2}", notification,
                                  notificationFailureException.Message, notificationFailureException.StackTrace),
                    Log.LogLevel.Error);
            }
            else
            {
                LogHelper.Instance.Logar(
                    string.Format(
                        "NotificationFailed\r\nError Status Code: {0}\r\nError Status Description: {1}\r\nDevice Token: {2}\r\nPayload: {3}",
                        exception.ErrorStatusCode, exception.ErrorStatusDescription, exception.Notification.DeviceToken,
                        notification), Log.LogLevel.Error);
            }
        }

        private void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            LogHelper.Instance.Logar(string.Format("ChannelException\r\n{0}\r\n{1}", exception.Message, exception.StackTrace), Log.LogLevel.Error);
        }

        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            try
            {
                var device = new Device().Get(oldSubscriptionId);

                if(device != null)
                    device.ChangeToken(newSubscriptionId);
            }
            catch (Exception exception)
            {
                LogHelper.Instance.Logar(
                    string.Format("Device Subscription ID Changed: Old Device Info: {0}\tNew Device Info: {1}\r\n{2}",
                                  oldSubscriptionId, newSubscriptionId, exception.StackTrace), Log.LogLevel.Error);
            }
        }

        private void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            try
            {
                var device = new Device().Get(expiredDeviceSubscriptionId);

                if (device != null)
                    device.Delete();
            }
            catch (Exception exception)
            {
                LogHelper.Instance.Logar(
                    string.Format("Device Subscription Expired: Device Info: {0}\r\n{1}", expiredDeviceSubscriptionId, exception.StackTrace), Log.LogLevel.Error);
            }
        }
    }
}
