﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace Lalubema.Orion.Integration.Impl.Orion
{
    public class AplicacaoRepositorio : RepositorioBase<Aplicacao>, IAplicacaoRepositorio<Aplicacao>
    {
        private static Aplicacao app = new Aplicacao() { Codigo = 5, Nome = "Corona", Versao = "1.0", Senha = "5!z[B%J[" };

        public Aplicacao Buscar(string nomeAplicacao, string versao)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Aplicacao>("a").Add(Restrictions.Eq("a.Nome", nomeAplicacao)).Add(Restrictions.Eq("a.Versao", versao));

            //return Execute<Aplicacao>(criteria);

            app.NotificationSettings = app.NotificationSettings = NotificationSettings.GetRepository().ListAll();

            return app;
        }

        public Aplicacao Buscar(string nomeAplicacao)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Aplicacao>("a").Add(Restrictions.Eq("a.Nome", nomeAplicacao));

            //return Execute<Aplicacao>(criteria);

            app.NotificationSettings = app.NotificationSettings = NotificationSettings.GetRepository().ListAll();

            return app;
        }

        public override IList<Aplicacao> ListAll()
        {
            app.NotificationSettings = app.NotificationSettings = NotificationSettings.GetRepository().ListAll();

            List<Aplicacao> a = new List<Aplicacao>();
            a.Add(app);

            return a;
        }
    }
}