﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class PessoaRepositorio : RepositorioBase<Pessoa>, IPessoaRepositorio<Pessoa>
    {
        private int? corporativo;
        public int? Corporativo
        {
            get { return corporativo; }
            set
            {
                corporativo = value;
                if (corporativo.HasValue)
                    Session.EnableFilter("corporativo").SetParameter("IDCorporativo", corporativo.Value);
                else
                    Session.DisableFilter("corporativo");
            }
        }

        private int? unidade;
        public int? Unidade
        {
            get { return unidade; }
            set
            {
                unidade = value;
                if (unidade.HasValue)
                {
                    Session.EnableFilter("porUnidade").SetParameter("IDUnidadeHabitacional", unidade.Value);
                    Session.DisableFilter("funcionarios");
                    Session.DisableFilter("corporativo");
                }
                else
                {
                    Session.EnableFilter("funcionarios");
                    if (corporativo.HasValue) Session.EnableFilter("corporativo").SetParameter("IDCorporativo", corporativo.Value);
                    Session.DisableFilter("porUnidade");
                }
            }
        }

        public Pessoa Get(string username)
        {
            var criteria = DetachedCriteria.For<Pessoa>("p")
                .CreateAlias("p.Usuario", "u", JoinType.InnerJoin)
                .Add(Restrictions.Eq("u.Nome", username));

            return Execute<Pessoa>(criteria);
        }

        public Pessoa Get(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Pessoa>("p")
                .CreateAlias("p.Usuario", "u", JoinType.InnerJoin)
                .Add(Restrictions.Eq("u.Nome", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Pessoa>(criteria);
        }

        public Pessoa Get(string username, int nonCodigo)
        {
            var criteria = DetachedCriteria.For<Pessoa>("p")
                .CreateAlias("p.Usuario", "u", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                .CreateAlias("p.Convites", "c", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                .Add(Restrictions.Or(Restrictions.Eq("u.Nome", username), Restrictions.And(Restrictions.Eq("c.Email", username), Restrictions.Eq("c.Status", byte.Parse("0")))))
                .Add(Restrictions.Not(Restrictions.Eq("p.Codigo", nonCodigo))).SetMaxResults(1);

            return Execute<Pessoa>(criteria);
        }

        protected override System.Collections.Generic.IList<Pessoa> ExecuteListAll(NHibernate.ICriteria criteria)
        {
            if (unidade.HasValue)
                criteria
                    .CreateAlias("o.PessoaCondominios", "f", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                    .CreateAlias("o.UnidadesHabitacional", "m", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                    .CreateAlias("o.Visitantes", "v", NHibernate.SqlCommand.JoinType.LeftOuterJoin)
                    .Add(Restrictions.Or(Restrictions.Or(Restrictions.Eq("f.UnidadeHabitacional.Codigo", unidade.Value), Restrictions.Eq("m.UnidadeHabitacional.Codigo", unidade.Value)), Restrictions.Eq("v.UnidadeHabitacional.Codigo", unidade.Value)));

            //Session.EnableFilter("funcionarios");
            return base.ExecuteListAll(criteria);
        }
    }
}