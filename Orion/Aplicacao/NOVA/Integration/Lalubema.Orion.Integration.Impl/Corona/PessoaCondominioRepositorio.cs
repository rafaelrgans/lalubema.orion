﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class PessoaCondominioRepositorio : RepositorioBase<PessoaCondominio>, IPessoaCondominioRepositorio<PessoaCondominio>
    {
        private int? corporativo;
        public int? Corporativo
        {
            get { return corporativo; }
            set
            {
                corporativo = value;
                if (corporativo.HasValue)
                    Session.EnableFilter("corporativo").SetParameter("IDCorporativo", corporativo.Value);
                else
                    Session.DisableFilter("corporativo");
            }
        }

        public override System.Collections.Generic.IList<PessoaCondominio> ListAll()
        {
            //Nao pode retornar a lista toda sem filtro
            return new List<PessoaCondominio>();
        }
    }
}