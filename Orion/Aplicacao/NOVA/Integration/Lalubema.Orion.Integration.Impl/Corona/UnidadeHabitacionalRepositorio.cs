﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate;
using System.Linq;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class UnidadeHabitacionalRepositorio : RepositorioBase<UnidadeHabitacional>, IUnidadeHabitacionalRepositorio<UnidadeHabitacional>
    {
        private int? corporativo;
        public int? Corporativo
        {
            get { return corporativo; }
            set
            {
                corporativo = value;
                if (corporativo.HasValue)
                    Session.EnableFilter("corporativo").SetParameter("IDCorporativo", corporativo.Value);
                else
                    Session.DisableFilter("corporativo");
            }
        }

        public override IList<UnidadeHabitacional> ListAll()
        {
            var result = base.ListAll();

            return result.OrderBy(o => o.Numero.PadLeft(6, '0')).ToList();
        }

        protected override IList<UnidadeHabitacional> ExecuteListAll(ICriteria criteria)
        {
            var result = base.ExecuteListAll(criteria);

            return result.OrderBy(o => o.Numero.PadLeft(6, '0')).ToList();
        }

        public IList<UnidadeHabitacional> ListarUnidadeHabitacionalPorCondominio(int codigoCondominio)
        {
            var criteria = DetachedCriteria.For<UnidadeHabitacional>("o")
                                           .CreateAlias("o.Edificio", "e")
                                           .CreateAlias("e.Condominio", "c")
                                           .Add(Restrictions.Eq("c.Codigo", codigoCondominio));

            return ToList(criteria);
        }
    }
}