﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class EnderecoRepositorio : RepositorioBase<Endereco>, IEnderecoRepositorio<Endereco>
    {
    }
}