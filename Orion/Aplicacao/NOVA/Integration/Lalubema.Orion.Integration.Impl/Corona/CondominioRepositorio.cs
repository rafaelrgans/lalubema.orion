﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class CondominioRepositorio : RepositorioBase<Condominio>, ICondominioRepositorio<Condominio>
    {
        private int? corporativo;
        public int? Corporativo
        {
            get { return corporativo; }
            set
            {
                corporativo = value;
                if (corporativo.HasValue)
                    Session.EnableFilter("corporativo").SetParameter("IDCorporativo", corporativo.Value);
                else
                    Session.DisableFilter("corporativo");
            }
        }
    }
}
