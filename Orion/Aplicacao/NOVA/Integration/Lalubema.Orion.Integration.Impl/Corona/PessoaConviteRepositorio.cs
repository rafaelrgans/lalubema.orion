﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class PessoaConviteRepositorio : RepositorioBase<PessoaConvite>, IPessoaConviteRepositorio<PessoaConvite>
    {
    }
}