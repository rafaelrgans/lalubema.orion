﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class VeiculoRepositorio : RepositorioBase<Veiculo>, IVeiculoRepositorio<Veiculo>
    {
        protected override System.Collections.Generic.IList<Veiculo> ExecuteListAll(NHibernate.ICriteria criteria)
        {
            criteria
                .Add(Restrictions.Eq("Status", (byte)1));

            return base.ExecuteListAll(criteria);
        }
    }
}