﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.IO;
using Lalubema.Orion.Common.Tipos;
using System.Net;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class PermissaoRepositorio : RepositorioBase<Permissao>, IPermissaoRepositorio<Permissao>
    {
        public Permissao Get(string code)
        {
            var criteria = DetachedCriteria.For<Permissao>("p")
                .Add(Restrictions.Eq("p.Codigo", code));

            return Execute<Permissao>(criteria);
        }
    }
}