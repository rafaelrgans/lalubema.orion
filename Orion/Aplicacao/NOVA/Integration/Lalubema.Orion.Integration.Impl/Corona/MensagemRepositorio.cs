﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class MensagemRepositorio : RepositorioBase<Mensagem>, IMensagemRepositorio<Mensagem>
    {
        public IList<Mensagem> ListarMensagensPublicadas()
        {
            var criteria = DetachedCriteria.For<Mensagem>("o")
                                           .Add(Restrictions.Le("o.DataPublicacao", DateTime.UtcNow))
                                           .Add(Restrictions.Ge("o.DataExpiracao", DateTime.UtcNow))
                                           .Add(Restrictions.Eq("o.Status", StatusMensagem.Publicada))
                                           .AddOrder(Order.Desc("o.DataPublicacao"))
                                           .AddOrder(Order.Desc("o.Codigo"));

            return ToList<Mensagem>(criteria);
        }
    }
}