﻿using System;
using NHibernate.Event;

namespace Lalubema.Orion.Common.Spring.Repository.Listener
{
    public class PostLoadEventListener : IPostLoadEventListener
    {
        public void OnPostLoad(PostLoadEvent @event)
        {
            if (@event == null || @event.Entity == null)
                return;

            var entity = @event.Entity;
            var propertyInfos = @event.Entity.GetType().GetProperties();

            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.PropertyType == typeof (DateTime))
                {
                    var currentDate = (DateTime) propertyInfo.GetValue(entity, null);
                    var resultDate = DateTime.SpecifyKind(currentDate, DateTimeKind.Utc);

                    propertyInfo.SetValue(entity, resultDate, null);
                }
            }
        }
    }
}