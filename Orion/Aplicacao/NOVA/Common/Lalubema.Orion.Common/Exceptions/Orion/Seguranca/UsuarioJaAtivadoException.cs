﻿namespace Lalubema.Orion.Common.Exceptions.Orion.Seguranca
{
    public class UsuarioJaAtivadoException : BaseException
    {
        public UsuarioJaAtivadoException(string username)
            : base(201005, string.Format(Utilitarios.Messages.Exceptions.EX_201005, username))
        {
            base.Data.Add("Username", username);
        }
    }
}