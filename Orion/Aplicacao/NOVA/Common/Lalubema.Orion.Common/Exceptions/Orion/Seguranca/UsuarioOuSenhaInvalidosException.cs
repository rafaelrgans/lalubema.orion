﻿namespace Lalubema.Orion.Common.Exceptions.Orion.Seguranca
{
    public class UsuarioOuSenhaInvalidosException : BaseException
    {
        public UsuarioOuSenhaInvalidosException()
            : base(201001, Utilitarios.Messages.Exceptions.EX_201001)
        {
        }
    }
}