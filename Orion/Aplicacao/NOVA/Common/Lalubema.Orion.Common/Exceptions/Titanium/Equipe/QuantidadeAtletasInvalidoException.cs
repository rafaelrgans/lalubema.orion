﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Equipe
{
    public class QuantidadeAtletasInvalidoException : BaseException
    {
        public QuantidadeAtletasInvalidoException(string torneio, int quantidadePorEquipe)
            : base(
                202001,
                string.Format("Somente é permitido {0} atletas por equipe para o torneio {1}.", quantidadePorEquipe,
                              torneio))
        {
        }
    }
}