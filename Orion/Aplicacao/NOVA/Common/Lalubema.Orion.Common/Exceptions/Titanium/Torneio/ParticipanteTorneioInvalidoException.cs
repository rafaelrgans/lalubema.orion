﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Torneio
{
    public class ParticipanteTorneioInvalidoException : BaseException
    {
        public ParticipanteTorneioInvalidoException()
            : base(
                203004,
                "Foi definido um participante para o torneio inválido. Não foi definido o responsável pelo convite.")
        {
        }
    }
}