﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Torneio
{
    public class TorneioInvalidoException : BaseException
    {
        public TorneioInvalidoException()
            : base(203004, "Torneio informado inválido.")
        {
        }
    }
}