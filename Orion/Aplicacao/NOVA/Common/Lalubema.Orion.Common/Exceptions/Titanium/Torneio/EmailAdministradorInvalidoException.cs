﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Torneio
{
    public class EmailAdministradorInvalidoException : BaseException
    {
        public EmailAdministradorInvalidoException()
            : base(203003, "Não existe usuário com o email informado para ser administrador do torneio.")
        {
        }
    }
}