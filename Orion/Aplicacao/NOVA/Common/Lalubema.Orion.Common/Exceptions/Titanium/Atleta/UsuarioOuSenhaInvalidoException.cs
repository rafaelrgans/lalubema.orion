﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Titanium.Atleta
{
    public class UsuarioOuSenhaInvalidoException : BaseException
    {
        public UsuarioOuSenhaInvalidoException()
            : base(201003, "Usuário ou senha inválidos.")
        {
        }

        public UsuarioOuSenhaInvalidoException(Exception innerException)
            : base(201003, "Usuário ou senha inválidos.", innerException)
        {
        }
    }
}