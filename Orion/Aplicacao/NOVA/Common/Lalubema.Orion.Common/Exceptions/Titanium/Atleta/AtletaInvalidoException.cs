﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Titanium.Atleta
{
    public class AtletaInvalidoException1 : BaseException
    {
        public AtletaInvalidoException1()
            : base(201002, "Não foi possível identificar o atleta.")
        {
        }

        public AtletaInvalidoException1(Exception innerException)
            : base(201002, "Não foi possível identificar o atleta.", innerException)
        {
        }

        public AtletaInvalidoException1(string emailAtleta)
            : base(201002, string.Format("Não foi possível identificar o atleta {0}", emailAtleta))
        {
        }
    }
}