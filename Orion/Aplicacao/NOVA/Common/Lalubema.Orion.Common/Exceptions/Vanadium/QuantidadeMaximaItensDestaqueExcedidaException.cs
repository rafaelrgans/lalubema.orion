﻿namespace Lalubema.Orion.Common.Exceptions.Vanadium
{
    public class QuantidadeMaximaItensDestaqueExcedidaException : BaseException
    {
        public QuantidadeMaximaItensDestaqueExcedidaException()
            : base(701001, Utilitarios.Messages.Exceptions.EX_701001)
        {
        }
    }
}