﻿namespace Lalubema.Orion.Common.Tipos
{
    public class LocalizedKey
    {
        public class Corona
        {
            public const string ChegadaSegura = "CHEGADA_SEGURA";
            public const string ChegadaSeguraStatus = "CHEGADA_SEGURA_STATUS";
            public const string ChegadaSeguraStatusDelivered = "CHEGADA_SEGURA_STATUS_DELIVERED";
            public const string ChegadaSeguraStatusVisualized = "CHEGADA_SEGURA_STATUS_VISUALIZED";
            public const string ChegadaSeguraStatusConfirmed = "CHEGADA_SEGURA_STATUS_CONFIRMED";
            public const string ChegadaSeguraStatusCanceled = "CHEGADA_SEGURA_STATUS_CANCELED";

            public const string ChegadaSeguraPe = "CHEGADA_SEGURA_PE";
            public const string ChegadaSeguraTaxi = "CHEGADA_SEGURA_TAXI";

            public const string NotificarPortaria = "NOTIFICACAO_PORTARIA";

            public const string NotificarSindico = "NOTIFICACAO_SINDICO";

            public const string NotificarResposta = "NOTIFICACAO_RESPOSTA";
        }
    }
}
