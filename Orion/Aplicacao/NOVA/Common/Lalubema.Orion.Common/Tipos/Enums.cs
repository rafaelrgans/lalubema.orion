﻿using Lalubema.Utilitarios.Attributes;
using System;

namespace Lalubema.Orion.Common.NovosTipos
{
    public enum ApartmentType
    {
        Apartment = 0,
        Roofing = 1,
        PrivateArea = 2,
        House = 0,
    }

    public enum VehicleType
    {
        Car = 1,
        Motorcycle = 1,
        Pickup = 2,
        Other = 3,
    }

    public enum ContactType
    {
        Email = 0,
        HomePhone = 1,
        WorkPhone = 2,
        CellPhone = 3,
        WebSite = 4
    }

    public enum UserStatus
    {
        Active = 1,
        Inactive = 0
    }

    public enum AnnexType
    {
        Image = 0,
        Html = 1,
        Text = 2,
    }

    public enum AnnexEntity
    {
        Picture = 0,
    }

    public enum AnnexStatus
    {
        Active = 1,
        Inactive = 0,
    }

    public enum MessageStatus
    {
        Idle = 0,
        Published = 1,
        Canceled = 2

    }

    public enum MessageType
    {
        Warning = 0,
        Communication = 1,
        Chat = 2

    }

    public enum PersonGender
    {
        Male = 1,
        Female = 2,
        Other = 0

    }

    public enum ReservationStatus
    {
        Pending = 0,
        Confirmed = 1,
        WaitingList = 2,
        Canceled = 3,
    }

    [Flags]
    public enum ReservationRuleTarget
    {
        People = 0x0,
        Apartment = 0x1,
        Everybody = 0x2,
    }

    public enum ReservationRuleStatus
    {
        Active = 1,
        Inactive = 0,
    }

    public enum DevicePlatform
    {
        Android = 0,
        iPhone = 1,
        iPad = 2,
    }

    public enum DeviceStatus
    {
        [StringValue("A", "Ativo")] Active = 1,
        [StringValue("I", "Inativo")] Inactive = 0
    }

    public static class TypeConverter
    {
        public static NotificationType1 Convert(NotificationType2 type)
        {
            switch (type)
            {
                case NotificationType2.Undefined:
                    return NotificationType1.Undefined;
                case NotificationType2.SafeArrival:
                    return NotificationType1.SafeArrival;
                case NotificationType2.TaxiSafeArrival:
                    return NotificationType1.TaxiSafeArrival;
                case NotificationType2.FootSafeArrival:
                    return NotificationType1.FootSafeArrival;
                case NotificationType2.Service:
                    return NotificationType1.Service;
                case NotificationType2.Visit:
                    return NotificationType1.Visit;
                case NotificationType2.Delivery:
                    return NotificationType1.Delivery;
                case NotificationType2.SindicoPorMorador:
                    return NotificationType1.SindicoPorMorador;
                case NotificationType2.SindicoPorFuncionario:
                    return NotificationType1.SindicoPorFuncionario;
                case NotificationType2.Answer:
                    return NotificationType1.Answer;
                case NotificationType2.Other:
                    return NotificationType1.Other;
                default:
                    return NotificationType1.Other;
            }
        }

        public static NotificationType2 Convert(NotificationType1 type)
        {
            switch (type)
            {
                case NotificationType1.Undefined:
                    return NotificationType2.Undefined;
                case NotificationType1.SafeArrival:
                    return NotificationType2.SafeArrival;
                case NotificationType1.TaxiSafeArrival:
                    return NotificationType2.TaxiSafeArrival;
                case NotificationType1.FootSafeArrival:
                    return NotificationType2.FootSafeArrival;
                case NotificationType1.Service:
                    return NotificationType2.Service;
                case NotificationType1.Visit:
                    return NotificationType2.Visit;
                case NotificationType1.Delivery:
                    return NotificationType2.Delivery;
                case NotificationType1.SindicoPorMorador:
                    return NotificationType2.SindicoPorMorador;
                case NotificationType1.SindicoPorFuncionario:
                    return NotificationType2.SindicoPorFuncionario;
                case NotificationType1.Answer:
                    return NotificationType2.Answer;
                case NotificationType1.Other:
                    return NotificationType2.Other;
                default:
                    return NotificationType2.Other;
            }
        }
    }

    public enum NotificationType2
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("C", "Chegada de carro registrado")] SafeArrival = 1,
        [StringValue("N", "Chegada de carro nao registrado")] TaxiSafeArrival = 2,
        [StringValue("P", "Chegada Segura a Pe")] FootSafeArrival = 3,
        [StringValue("S", "Serviço")] Service = 4,
        [StringValue("V", "Visitante")] Visit = 5,
        [StringValue("D", "Delivery")] Delivery = 6,
        [StringValue("M", "Síndico")] SindicoPorMorador = 7,
        [StringValue("F", "Síndico")] SindicoPorFuncionario = 8,
        [StringValue("R", "Resposta")] Answer = 9,
        [StringValue("G", "Convidado Evento")] EventGuestArrival = 11,
        [StringValue("O", "Outros")] Other = 10,
    }

    public enum NotificationType1
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("C", "Chegada de carro registrado")] SafeArrival = 100,
        [StringValue("N", "Chegada de carro nao registrado")]
        TaxiSafeArrival = 101,
        [StringValue("P", "Chegada Segura a Pe")]
        FootSafeArrival = 102,
        [StringValue("S", "Serviço")]
        Service = 200,
        [StringValue("V", "Visitante")]
        Visit = 201,
        [StringValue("D", "Delivery")]
        Delivery = 202,
        [StringValue("M", "Síndico")]
        SindicoPorMorador = 300,
        [StringValue("F", "Síndico")]
        SindicoPorFuncionario = 301,
        [StringValue("R", "Resposta")]
        Answer = 900,
        [StringValue("O", "Outros")]
        Other = 999,
    }

    public enum NotificationStatus
    {
        [StringValue("A", "Aguardando")]
        New = 2,
        [StringValue("E", "Entregue")]
        Delivered = 1,
        [StringValue("V", "Visualizado")]
        Seen = 6,
        [StringValue("C", "Confirmado")]
        Confirmed = 3,
        [StringValue("X", "Cancelado")]
        Canceled = 5,
        [StringValue("R", "Respondido")]
        Answered = 4,
        [StringValue("F", "Finalizado")]
        Finalized = 7,
    }
}

namespace Lalubema.Orion.Common.Tipos
{
    public enum Sexo
    {
        [StringValue("M", "Masculino")] M = 1,
        [StringValue("F", "Feminino")] F = 2,
        [StringValue("O", "Outro")] O = 0
    }

    public enum MetodoAutenticacao
    {
        NaoSelecionado = 0,
        PorSistema,
        PorUsuario
    }

    public enum Aplicacao
    {
        [StringValue("Pegasus", "Pegasus")] Pegasus = 1,
        [StringValue("Crux", "Crux")] Crux = 2,
        [StringValue("Aquila", "Aquila")] Aquila = 3,
        [StringValue("Columba", "Columba")] Columba = 4,
        [StringValue("Corona", "Corona")] Corona = 5,
        [StringValue("Titanium", "Titanium")] Titanium = 6,
        [StringValue("Vanadium", "Vanadium")] Vanadium = 7,
        [StringValue("Iron", "Iron")] Iron = 8,
        [StringValue("MaisCrianca", "Mais Criança")] MaisCrianca = 9,
        [StringValue("Cobalt", "Cartório Fácil")] Cobalt = 10,
        [StringValue("Corona", "Corona 1.1")] CoronaV1_1 = 13,
        [StringValue("Any", "Any")]
        Any = 999
    }

    public enum StatusUsuarioTorneio
    {
        [StringValue("S", "Não Convidado")] SemConvite,
        [StringValue("C", "Convidado")] Convidado,
        [StringValue("P", "Participante")] Participante
    }

    public enum StatusPartida
    {
        [StringValue("A", "Confirmada")] Aprovada,
        [StringValue("P", "Pendente")] Pendente,
        [StringValue("C", "Cancelada")] Cancelada
    }

    public enum TipoOcorrencia
    {
        [StringValue("U", "Único")] Unico,
        [StringValue("D", "Diário")] Diario,
        [StringValue("S", "Semanal")] Semanal
    }

    public enum ModoCompra
    {
        [StringValue("S", "Store")] Store = 1,
        [StringValue("A", "App Purchase")] AppPurchase = 2
    }

    public enum TipoConteudo
    {
        [StringValue("C", "Apresentação")] Apresentacao = 1,
        [StringValue("A", "Audiência")] Audiencia,
        [StringValue("G", "Oportunidades")] Grafico
    }

    public enum PerfilPrograma
    {
        [StringValue("A", "Eventos")] Evento = 1,
        [StringValue("E", "Esporte")] Esporte = 2,
        [StringValue("J", "Jornalismo")] Jornalismo = 3,
        [StringValue("V", "Variedades")] Variedades = 4,

		// Alterosa Interior
		[StringValue("P", "Programação")] Programacao = 5,
		[StringValue("D", "Projeto Video")] ProjetoVideo = 6,
		[StringValue("N", "Eventos")] EventoInterior = 7,
		[StringValue("S", "Novidade SBT")] Novidade = 8
	}

    public enum TipoAlbum
    {
        [StringValue("M", "Música")] Musica = 1,
        [StringValue("F", "Foto")] Foto,
        [StringValue("V", "Vídeo")] Video
    }

    public enum TipoVeiculo
    {
        Carro = 1,
        Motocicleta = 2,
        Pickup = 3,
        Outros= 4
    }

    public enum TipoLocalBem
    {
        [StringValue("L", "Local")] Local = 1,
        [StringValue("B", "Bem")] Bem
    }

    public enum TipoAutorizacao
    {
        [StringValue("E", "Entrega")] Entrega = 1,
        [StringValue("N", "Entrada")] Entrada
    }

    public enum TipoMensagem
    {
        [StringValue("I", "Interno")] Interno = 1,
        [StringValue("O", "Oficial")] Oficial = 2,
        [StringValue("Q", "Quadro de Avisos")] QuadroAvisos = 3,
        [StringValue("S", "Sindico")] Sindico = 4
    }

    public enum StatusMensagem
    {
        [StringValue("P", "Publicada")] Publicada = 1,
        [StringValue("A", "Aguardando Autorização")] AguardandoAutorizacao = 2,
        [StringValue("R", "Rejeitada")] Rejeitada = 3
    }

    public enum StatusReserva
    {
        [StringValue("C", "Confirmada")] Confirmada = 1,
        [StringValue("P", "Pendente")] Pendente,
        [StringValue("E", "Lista de Espera")] ListaEspera,
        [StringValue("C", "Cancelada")] Cancelada
    }

    public enum StatusPedido
    {
		[StringValue("U", "Unknow")] Unknow = 0,
        [StringValue("A", "Active")] Active = 1,
        [StringValue("D", "Deleted")] Deleted = 2,
        [StringValue("C", "Closed")] Closed = 3,
        [StringValue("P", "Pending")] Pending = 4
    }

    public enum LicenseType
    {
        [StringValue("F", "Free")] Free = 1,
        [StringValue("P", "MensalPro")] MensalPro,
        [StringValue("I", "MensalFull")] MensalFull,
    }

    public enum TransactionStatus
    {
        [StringValue("A", "Active")] Active = 1,
        [StringValue("N", "NotActive")] NotActive,
        [StringValue("E", "Expired")] Expired
    }

    public enum TextStatus
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("A", "Aprovado")] Approved = 1,
        [StringValue("D", "Reprovado")] Disapproved,
        [StringValue("P", "Pendente")] Pending
    }

    public enum TipoChegada
    {
        [StringValue("C", "Chegada de carro registrado")] VeiculoRegistrado = 0,
        [StringValue("N", "Chegada de carro nao registrado")] VeiculoNaoRegistrado = 1,
        [StringValue("P", "Chegada a pe")] SemVeiculo = 2
    }

    public enum TipoNotificacao
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("C", "Chegada de carro registrado")] ChegadaSegura = 100,
        [StringValue("N", "Chegada de carro nao registrado")] ChegadaSeguraTaxi = 101,
        [StringValue("P", "Chegada Segura a Pe")] ChegadaSeguraPe = 102,
        [StringValue("S", "Serviço")] Servico = 200,
        [StringValue("V", "Visitante")] Visitante = 201,
        [StringValue("D", "Delivery")] Delivery = 202,
        [StringValue("M", "Síndico")] SindicoPorMorador = 300,
        [StringValue("F", "Síndico")] SindicoPorFuncionario = 301,
        [StringValue("R", "Resposta")] Resposta = 900,
        [StringValue("O", "Outros")] Outros = 999
    }

    public enum StatusNotificacao
    {
        [StringValue("E", "Entregue")] Entregue = 1,
        [StringValue("A", "Aguardando")] Aguardando = 2,
        [StringValue("X", "Cancelado")] Cancelado = 5,
        [StringValue("V", "Visualizado")] Visualizado = 6,
        [StringValue("C", "Confirmado")] Confirmado = 3,
        [StringValue("R", "Respondido")] Respondido = 4
    }

	public enum TipoRelatorio
	{
		[StringValue("U", "Unknow")] Unknow = 0,
		[StringValue("G", "Garcom")] Garcom = 1,
		[StringValue("M", "Mesa")] Mesa = 2,
		[StringValue("I", "Item")] Item = 3
	}

    public enum DeviceStatus
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("A", "Ativo")] Active = 1,
        [StringValue("I", "Inativo")] Inactive
    }
}