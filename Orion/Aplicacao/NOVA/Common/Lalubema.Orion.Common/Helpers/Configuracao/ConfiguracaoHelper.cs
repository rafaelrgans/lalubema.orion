﻿namespace Lalubema.Orion.Common.Helpers.Configuracao
{
    public class ConfiguracaoHelper
    {
        private static readonly ConfiguracaoHelper ConfiguracaoHelperInstance = new ConfiguracaoHelper();

        private ConfiguracaoHelper()
        {
        }

        public static ConfiguracaoHelper Instance
        {
            get { return ConfiguracaoHelperInstance; }
        }

        public string this[string key]
        {
            get { return string.Empty; }
        }
    }
}