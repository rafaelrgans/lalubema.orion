﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Lalubema.Wcf.Suporte.Serialization
{
    public class JsonDataContractFormatAttribute : Attribute, IOperationBehavior
    {
        public void Validate(OperationDescription operationDescription)
        {
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            ReplaceDataContractSerializerOperationBehavior(operationDescription);
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
            ReplaceDataContractSerializerOperationBehavior(operationDescription);
        }

        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        private static void ReplaceDataContractSerializerOperationBehavior(OperationDescription operationDescription)
        {
            var dcs = operationDescription.Behaviors.Find<DataContractSerializerOperationBehavior>();

            if (dcs != null)
                operationDescription.Behaviors.Remove(dcs);

            var sfb = operationDescription.Behaviors.Find<SerializerFormatterBehavior>();

            if (sfb != null)
                return;

            operationDescription.Behaviors.Add(new SerializerFormatterBehavior());
        } 
    }
}