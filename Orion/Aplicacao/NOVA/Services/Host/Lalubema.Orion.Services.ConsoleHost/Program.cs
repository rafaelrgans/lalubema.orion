﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Utilitarios.Spring;
using System.Net;
using Lalubema.Orion.Domain.Corona;

namespace Lalubema.Orion.Services.ConsoleHost
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando serviços WCF...");
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
            Console.WriteLine("Serviços WCF iniciados...");
            Console.WriteLine("Pressione <ENTER> para encerrar os serviços...");
            Console.ReadLine();
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }
    }
}