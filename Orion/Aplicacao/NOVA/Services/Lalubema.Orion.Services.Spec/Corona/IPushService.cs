﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaPushService", false)]
    public interface IPushService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/SendPush")]
        DTOSendPushRetorno SendPush(DTOSendPush sendPush);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "/GetPush?ChaveCriptografada={chaveCriptografada}&codigoCondominio={codigoCondominio}")]
        DTOGetPush GetPush(string chaveCriptografada, int codigoCondominio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/SendReplyPush")]
        DTORetorno SendReplyPush(DTOSendPush sendPush);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "/GetReplyPush?ChaveCriptografada={chaveCriptografada}")]
        DTOGetPush GetReplyPush(string chaveCriptografada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/ChegadaSegura")]
        DTOPushChegadaSeguraRetorno ChegadaSegura(DTOPushChegadaSegura sendPush);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "/GetPushRemetente?ChaveCriptografada={chaveCriptografada}")]
        DTOGetPush GetPushRemetente(string chaveCriptografada);

        /*
        [Obsolete("SendPush: Este serviço foi substituído pelo SendPush que passara o tipo de Notificação")]
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/PushChegadaSegura")]
        DTORetorno PushChegadaSegura(DTOPushChegadaSegura pushChegadaSegura);

        [Obsolete("SendPush: Este serviço foi substituído pelo SendPush que passara o tipo de Notificação")]
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/EnviarNotificacaoPortaria")]
        DTORetorno EnviarNotificacaoPortaria(DTONotificarPortaria notificacao);
        */
    }
}