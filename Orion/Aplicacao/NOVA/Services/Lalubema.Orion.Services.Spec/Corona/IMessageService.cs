﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaMessageService", false)]
    public interface IMessageService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/ListarMuralMensagem?ChaveCriptografada={chaveCriptografada}&UnidadeHabitacional={unidadeHabitacional}"
            )]
        DTOMuralRetorno ListarMuralMensagem(string chaveCriptografada, int unidadeHabitacional);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/ListarMensagens?ChaveCriptografada={chaveCriptografada}&UnidadeHabitacional={unidadeHabitacional}"
            )]
        DTOMensagensRetorno ListarMensagens(string chaveCriptografada, int unidadeHabitacional);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "/ListarDadosCondominio?ChaveCriptografada={chaveCriptografada}&Condominio={condominio}")]
        DTOCondominioRetorno ListarDadosCondominio(string chaveCriptografada, int condominio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/Send")]
        DTORetorno Send(DTOSendMessage sendMessage);
    }
}