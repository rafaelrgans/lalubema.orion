﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaCondominioService", false)]
    public interface ICondominioService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "CheckEmail?c={c}")]
        Stream CheckEmail(string c);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/RegisterDevice")]
        DTORetorno RegisterDevice(DTORegisterDevice registerDevice);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/UnregisterDevice")]
        DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice);
    }
}