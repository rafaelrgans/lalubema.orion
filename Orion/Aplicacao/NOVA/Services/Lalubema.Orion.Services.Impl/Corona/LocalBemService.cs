﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class LocalBemService : ServiceBase, ILocalBemService
    {
        public DTOLocalBemsRetorno ListarLocalBem(string chaveCriptografada, int codigoEdificio)
        {
            return CoronaLocalBemDomainService.ListarLocalBem(chaveCriptografada, codigoEdificio);
        }

        public DTOReservasRetorno ListarReserva(string chaveCriptografada, int codigoLocalBem)
        {
            return CoronaLocalBemDomainService.ListarReserva(chaveCriptografada, codigoLocalBem);
        }

        public DTOReservaLocalBemsRetorno ListarReservaUsuario(string chaveCriptografada, int codigoCondominio)
        {
            return CoronaLocalBemDomainService.ListarReservaUsuario(chaveCriptografada, codigoCondominio);
        }

        public DTORetorno Reservar(DTOReservaLocalBemEntrada reservaLocalBem)
        {
            return CoronaLocalBemDomainService.Reservar(reservaLocalBem);
        }
    }
}