﻿using System.IO;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class UnidadeHabitacionalService : ServiceBase, IUnidadeHabitacionalService
    {
        public Stream CheckEmail(string c)
        {
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html;charset=utf-8";

            return CoronaUnidadeHabitacionalDomainService.CheckEmail(c);
        }

        public DTOUnidadesHabitacionaisRetorno ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            return CoronaUnidadeHabitacionalDomainService.ListarUnidadeHabitacionalPorCondominio(chaveCriptografada, codigoCondominio);
        }
    }
}