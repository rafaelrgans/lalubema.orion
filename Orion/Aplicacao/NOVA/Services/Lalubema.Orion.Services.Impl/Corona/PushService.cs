﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class PushService : ServiceBase, IPushService
    {
        public DTOSendPushRetorno SendPush(DTOSendPush sendPush)
        {
            return CoronaPushDomainService.SendPush(sendPush);
        }

        public DTOGetPush GetPush(string chaveCriptografada, int codigoCondominio)
        {
            return CoronaPushDomainService.GetPush(chaveCriptografada, codigoCondominio);
        }

        public DTORetorno SendReplyPush(DTOSendPush sendPush)
        {
            sendPush.TipoMensagem = Common.NovosTipos.NotificationType1.Answer;
            return CoronaPushDomainService.SendPush(sendPush);
        }

        public DTOGetPush GetReplyPush(string chaveCriptografada)
        {
            return CoronaPushDomainService.GetReplyPush(chaveCriptografada);
        }

        public DTOPushChegadaSeguraRetorno ChegadaSegura(DTOPushChegadaSegura sendPush)
        {
            return CoronaPushDomainService.ChegadaSegura(sendPush);
        }

        public DTOGetPush GetPushRemetente(string chaveCriptografada)
        {
            return CoronaPushDomainService.GetPushRemetente(chaveCriptografada);
        }

        /*
        [Obsolete("SendPush: Este serviço foi substituído pelo SendPush que passara o tipo de Notificação")]
        public DTORetorno PushChegadaSegura(DTOPushChegadaSegura pushChegadaSegura)
        {
            DTOSendPush input = new DTOSendPush()
            {
                Origem = pushChegadaSegura.CodigoVeiculo,
                TipoMensagem = Common.Tipos.TipoNotificacao.ChegadaSegura,
                ChaveCriptografada = pushChegadaSegura.ChaveCriptografada
            };

            return this.SendPush(input);
        }

        [Obsolete("SendPush: Este serviço foi substituído pelo SendPush que passara o tipo de Notificação")]
        public DTORetorno EnviarNotificacaoPortaria(DTONotificarPortaria notificacao)
        {
            DTOSendPush input = new DTOSendPush()
            {
                Origem = notificacao.CodigoUnidadeHabitacional,
                Message = notificacao.Message,
                TipoMensagem = Common.Tipos.TipoNotificacao.Outros,
                ChaveCriptografada = notificacao.ChaveCriptografada
            };

            return this.SendPush(input);
        }
        */
    }
}