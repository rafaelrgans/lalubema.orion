﻿using System.IO;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Orion.Services.Spec.Corona;
using System;
using System.Text;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class UsuarioService : ServiceBase, IUsuarioService
    {
        public DTORetorno RegisterDevice(DTORegisterDevice registerDevice)
        {
            return CoronaUsuarioDomainService.RegisterDevice(registerDevice);
        }

        public DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            return CoronaUsuarioDomainService.UnregisterDevice(unregisterDevice);
        }
    }
}
