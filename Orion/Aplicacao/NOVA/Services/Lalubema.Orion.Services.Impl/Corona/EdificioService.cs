﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class EdificioService : ServiceBase, IEdificioService
    {
        public DTORetorno AssociarEdificioLocalBem(DTOAssociarEdificioLocalBemEntrada edificioLocalBem)
        {
            return CoronaEdificioDomainService.AssociarEdificioLocalBem(edificioLocalBem);
        }
    }
}
