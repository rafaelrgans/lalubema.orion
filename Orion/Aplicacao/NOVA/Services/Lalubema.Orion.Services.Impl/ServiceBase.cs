﻿using System.ServiceModel;
using Lalubema.Orion.Domain.Services.Spec.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca;
using Lalubema.Utilitarios.Services;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Services.Impl.Aspects;

namespace Lalubema.Orion.Services.Impl
{
    [ServiceContract(Namespace = "http://www.lalubema.com/")]
    [ServiceKnownType("GetKnownTypes", typeof (KnownTypeProvider))]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    public class ServiceBase : Utilitarios.Wcf.ServiceBase
    {
        protected IAutenticacaoService AutenticacaoDomainService
        {
            get { return ObjectFactory.GetObject<IAutenticacaoService>(false); }
        }

        protected IAccountService AccountDomainService
        {
            get { return ObjectFactory.GetObject<IAccountService>(false); }
        }

        protected IFileSystemService FileSystemDomainService
        {
            get { return ObjectFactory.GetObject<IFileSystemService>(false); }
        }
        protected Domain.Services.Spec.Orion.ICadastroService CadastroDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Orion.ICadastroService>(false); }
        }

        #region [ Meu Condomínio ]

        protected Domain.Services.Spec.Corona.IUnidadeHabitacionalService CoronaUnidadeHabitacionalDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IUnidadeHabitacionalService>(false); }
        }

        protected Domain.Services.Spec.Corona.ICondominioService CoronaCondominioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.ICondominioService>(false); }
        }

        protected Domain.Services.Spec.Corona.IUsuarioService CoronaUsuarioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IUsuarioService>(false); }
        }

        protected Domain.Services.Spec.Corona.IMessageService CoronaMessageDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IMessageService>(false); }
        }

        protected Domain.Services.Spec.Corona.ILocalBemService CoronaLocalBemDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.ILocalBemService>(false); }
        }

        protected Domain.Services.Spec.Corona.IPushService CoronaPushDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IPushService>(false); }
        }

        protected Domain.Services.Spec.Corona.IEdificioService CoronaEdificioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IEdificioService>(false); }
        }

        #endregion [ Meu Condomínio ]
    }
}