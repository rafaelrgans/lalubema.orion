﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Services.Spec.Orion.InfraEstrutura;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Orion.InfraEstrutura
{
    public class FileSystemService : ServiceBase, IFileSystemService
    {
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        public DTOFileRetorno SaveFile(DTOFileEntrada file)
        {
            return FileSystem.SaveFile(file);
        }
    }
}