﻿using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class MessageService : ServiceBase, IMessageService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOMuralRetorno ListarMuralMensagem(string chaveCriptografada, int unidadeHabitacional)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var usuario = new Pessoa().Get(security.CodigoUsuario);
            var unidadeHabitacionalModel = new UnidadeHabitacional().Get(unidadeHabitacional);

            var classificado = unidadeHabitacionalModel.Edificio.Condominio.GetLastClassificado();
            var mensagem = unidadeHabitacionalModel.Edificio.Condominio.GetLastMensagem();

            var returnValue = new DTOMuralRetorno
                                  {
                                      Classificado = classificado == null ? null : classificado.Transform<DTOClassificado>(true),
                                      Mensagem = mensagem == null ? null : mensagem.Transform<DTOMensagem>(true),
                                      IsSindico = usuario.IsSindico(unidadeHabitacional)
                                  };

            return returnValue;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOMensagensRetorno ListarMensagens(string chaveCriptografada, int unidadeHabitacional)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var mensagens = new Mensagem().ListarMensagensPublicadas(security.CodigoUsuario);
            var isSindico = new Pessoa().Get(security.CodigoUsuario).IsSindico(unidadeHabitacional);

            return new DTOMensagensRetorno
                       {
                           IsSindico = isSindico,
                           Mensagens = mensagens.TransformList<DTOMensagem>(true, null)
                       };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOCondominioRetorno ListarDadosCondominio(string chaveCriptografada, int condominio)
        {
            var securityInfo = new ChaveCriptografadaHelper(chaveCriptografada);

            var condominioModel = new Pessoa().Get(securityInfo.CodigoUsuario).GetCondominio(condominio);

            var condominioRetorno = condominioModel.Transform<DTOCondominioRetorno>();

            foreach (var edificio in condominioRetorno.Edificios)
            {
                edificio.UnidadeHabitacional =
                    edificio.UnidadeHabitacional.OrderBy(t => t.Numero.PadLeft(6, '0')).ToList();
            }

            return condominioRetorno;
        }

        [ValidateCryptographyKey(true, (int) Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Send(DTOSendMessage sendMessage)
        {
            new Mensagem()
                .Send(sendMessage);

            return new DTORetorno();
        }
    }
}