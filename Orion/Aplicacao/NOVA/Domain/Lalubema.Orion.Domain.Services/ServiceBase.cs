﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion.Notification;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Spring;
using Lalubema.Utilitarios.Validacao;
using Lalubema.Utilitarios.Repositorios;
using System.Reflection;

namespace Lalubema.Orion.Domain.Services
{
    public class ServiceBase
    {
        protected void trySetCorporativo(IRepositorioBase repository, int corporativo)
        {
            PropertyInfo pi = repository.GetType().GetProperty("Corporativo", typeof(int?));
            if (pi != null)
                pi.SetValue(repository, corporativo, null);
        }

        protected void trySetUnidade(IRepositorioBase repository, int? unidade)
        {
            PropertyInfo pi = repository.GetType().GetProperty("Unidade", typeof(int?));
            if (pi != null)
                pi.SetValue(repository, unidade, null);
        }

        protected IAplicacaoRepositorio<Aplicacao> AplicacaoRepositorio
        {
            get { return ObjectFactory.GetSingleton<IAplicacaoRepositorio<Aplicacao>>(); }
        }

        protected INotificationRepository NotificationRepository
        {
            get { return ObjectFactory.GetSingleton<INotificationRepository>(); }
        }

        #region [ Validators ]

        private NotificationCollection _notificationCollection;

        protected NotificationCollection NotificationCollection
        {
            get { return _notificationCollection ?? (_notificationCollection = new NotificationCollection()); }
        }

        public IValidator<DTOAccountRegister> ValidadorAccountRegister
        {
            get { return ObjectFactory.GetObject<IValidator<DTOAccountRegister>>("ValidadorAccountRegister"); }
        }

        #endregion [ Validation ]
    }
}