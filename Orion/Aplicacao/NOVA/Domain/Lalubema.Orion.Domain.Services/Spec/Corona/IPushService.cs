﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaPushDomainService", false)]
    public interface IPushService
    {
        DTOSendPushRetorno SendPush(DTOSendPush sendPush);

        DTOGetPush GetPush(string chaveCriptografada, int codigoCondominio);

        DTOGetPush GetReplyPush(string chaveCriptografada);

        DTOPushChegadaSeguraRetorno ChegadaSegura(DTOPushChegadaSegura sendPush);

        DTOGetPush GetPushRemetente(string chaveCriptografada);
    }
}