﻿using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    [KnownType(typeof(DTOTicketPorUsuario))]
    public class DTOTicketPorUsuario : DTOTicket
    {
        public override MetodoAutenticacao Metodo
        {
            get { return MetodoAutenticacao.PorUsuario; }
        }

        public string Usuario { get; set; }

        public string Senha { get; set; }
    }
}