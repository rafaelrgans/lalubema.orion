﻿using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    public class DTOTicket : DTOEntrada
    {
        public virtual MetodoAutenticacao Metodo
        {
            get { return MetodoAutenticacao.NaoSelecionado; }
        }

        public string Versao { get; set; }
    }
}