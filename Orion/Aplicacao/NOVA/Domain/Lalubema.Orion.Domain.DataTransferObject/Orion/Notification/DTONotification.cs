﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Notification
{
    [DataContract(Namespace = "http://www.lalubema.com/Notification")]
    public class DTONotification : DTOEntrada
    {
        [DataMember]
        public string Message { get; set; }
    }
}