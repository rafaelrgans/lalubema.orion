﻿namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Notification
{
    public class DTONotificationSettings
    {
        public int Codigo { get; set; }

        public string Bundle { get; set; }

        public string AndroidAccessKey { get; set; }

        public string AndroidSenderId { get; set; }

        public string AppleCertificate { get; set; }

        public string AppleCertificatePassword { get; set; }
    }
}