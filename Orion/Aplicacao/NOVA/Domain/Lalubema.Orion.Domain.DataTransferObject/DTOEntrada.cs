﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject
{
    [DataContract(Namespace = "http://www.lalubema.com/")]
    public abstract class DTOEntrada : DTOBase
    {
        [DataMember]
        [ScaffoldColumn(false)]
        public string ChaveCriptografada { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public bool ReturnSaved { get; set; }
    }
}