﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOSendPush : DTONotification
    {
        [DataMember]
        public int CodigoMensagem { get; set; }

        [DataMember]
        public int? Origem { get; set; }

        [DataMember]
        public NotificationType1 TipoMensagem { get; set; }

        [DataMember]
        public NotificationStatus? StatusNotificacao { get; set; }

        [DataMember]
        public int TempoChegada { get; set; }

        [DataMember]
        public string RemetenteDeviceToken { get; set; }

        [DataMember]
        public string Mensagem { get; set; }
    }
}