﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Common.NovosTipos;
using System.Globalization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOGetPush : DTORetorno
    {
        [DataMember]
        public List<DTOGetPushChegadaArgs> Chegada = new List<DTOGetPushChegadaArgs>();

        [DataMember]
        public List<DTOGetPushPortariaArgs> Portaria = new List<DTOGetPushPortariaArgs>();

        [DataMember]
        public List<DTOGetPushSindicoArgs> Sindico = new List<DTOGetPushSindicoArgs>();

        [DataMember]
        public List<DTOGetPushRespostaArgs> Resposta = new List<DTOGetPushRespostaArgs>();
    }


    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOGetPushChegadaArgs
    {
        public DTOGetPushChegadaArgs() {
            VeiculoCor = " ";
            VeiculoMarca = " ";
            VeiculoModelo = " ";
            VeiculoPlaca = " ";
        }

        [DataMember]
        public int Codigo { get; set; }

        //[DataMember]
        public DateTime DataHoraNotificacao
        {
            get { return dataHoraNotificacao; }
            set
            {
                dataHoraNotificacao = value;
                this.DataNotificacao = dataHoraNotificacao.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                this.HoraNotificacao = dataHoraNotificacao.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
            }
        } private DateTime dataHoraNotificacao;

        [DataMember]
        public string DataNotificacao { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public string HoraNotificacao { get; set; }

        [DataMember]
        public NotificationType1 TipoNotificacao { get; set; }

        [DataMember]
        public NotificationStatus StatusNotificacao { get; set; }

        [DataMember]
        public int? Origem { get; set; }

        [DataMember]
        public string VeiculoMarca { get; set; }

        [DataMember]
        public string VeiculoModelo { get; set; }

        [DataMember]
        public string VeiculoPlaca { get; set; }

        [DataMember]
        public string VeiculoCor { get; set; }

        [DataMember]
        public string EdificioNome { get; set; }

        [DataMember]
        public string UnidadeHabitacionalNumero { get; set; }

        public List<object> ToList()
        {
            return new List<object>()
                    {
                       VeiculoMarca,
                       VeiculoModelo,
                       VeiculoPlaca,
                       VeiculoCor,
                       DataNotificacao,
                       HoraNotificacao,
                       EdificioNome,
                       UnidadeHabitacionalNumero,
                       Codigo,
                       Mensagem,
                       TipoNotificacao,
                       StatusNotificacao,
                   };
        }
    }

    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOGetPushPortariaArgs
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public NotificationType1 TipoNotificacao { get; set; }

        //[DataMember]
        public DateTime DataHoraNotificacao
        {
            get { return dataHoraNotificacao; }
            set
            {
                dataHoraNotificacao = value;
                this.DataNotificacao = dataHoraNotificacao.ToString("dd/MM/yyyy");
                this.HoraNotificacao = dataHoraNotificacao.ToString("HH:mm:ss");
            }
        } private DateTime dataHoraNotificacao;

        [DataMember]
        public string DataNotificacao { get; set; }

        [DataMember]
        public string HoraNotificacao { get; set; }

        [DataMember]
        public string UsuarioNome { get; set; }

        [DataMember]
        public string EdificioNome { get; set; }

        [DataMember]
        public string UnidadeHabitacionalNumero { get; set; }

        //[DataMember]
        public NotificationStatus StatusNotificacao
        {
            get { return statusNotificacao; }
            set
            {
                statusNotificacao = value;
                this.Status = EnumExtension.ConvertToString(value);
            }
        } private NotificationStatus statusNotificacao;

        [DataMember]
        public string Status { get; set; }

        public List<object> ToList()
        {
            return new List<object>()
                    {
                        UsuarioNome,
                        UnidadeHabitacionalNumero,
                        EdificioNome,
                        Mensagem,
                        Codigo,
                        DataNotificacao,
                        HoraNotificacao
                   };
        }
    }

    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOGetPushSindicoArgs
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public NotificationType1 TipoNotificacao { get; set; }

        //[DataMember]
        public DateTime DataHoraNotificacao
        {
            get { return dataHoraNotificacao; }
            set
            {
                dataHoraNotificacao = value;
                this.DataNotificacao = dataHoraNotificacao.ToString("dd/MM/yyyy");
                this.HoraNotificacao = dataHoraNotificacao.ToString("HH:mm:ss");
            }
        } private DateTime dataHoraNotificacao;

        [DataMember]
        public string DataNotificacao { get; set; }

        [DataMember]
        public string HoraNotificacao { get; set; }

        [DataMember]
        public string RemetenteNome { get; set; }

        /// <summary>
        /// Código de Origem, sendo:
        /// - Funcionário » Código do Condomínio
        /// - Morador » Código da Unidade Habitacional
        /// </summary>
        [DataMember]
        public int OrigemCodigo { get; set; }

        /// <summary>
        /// Nome de Origem, sendo:
        /// - Funcionário » Nome do Condomínio
        /// - Morador » Número da Unidade Habitacional
        /// </summary>
        [DataMember]
        public string OrigemDescricao { get; set; }

        public List<object> ToList()
        {
            return new List<object>()
                    {
                        RemetenteNome,
                        OrigemCodigo,
                        OrigemDescricao,
                        Mensagem,
                        Codigo,
                        TipoNotificacao,
                        DataNotificacao,
                        HoraNotificacao
                   };
        }
    }


    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOGetPushRespostaArgs
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public NotificationType1 TipoNotificacao { get; set; }

        //[DataMember]
        public DateTime DataHoraNotificacao
        {
            get { return dataHoraNotificacao; }
            set
            {
                dataHoraNotificacao = value;
                this.DataNotificacao = dataHoraNotificacao.ToString("dd/MM/yyyy");
                this.HoraNotificacao = dataHoraNotificacao.ToString("HH:mm:ss");
            }
        } private DateTime dataHoraNotificacao;

        [DataMember]
        public string DataNotificacao { get; set; }

        [DataMember]
        public string HoraNotificacao { get; set; }

        [DataMember]
        public string RemetenteNome { get; set; }

        [DataMember]
        public int OrigemCodigo { get; set; }

        public List<object> ToList()
        {
            return new List<object>()
                    {
                        RemetenteNome,
                        OrigemCodigo,
                        Mensagem,
                        Codigo,
                        TipoNotificacao,
                        DataNotificacao,
                        HoraNotificacao
                   };
        }
    }

}