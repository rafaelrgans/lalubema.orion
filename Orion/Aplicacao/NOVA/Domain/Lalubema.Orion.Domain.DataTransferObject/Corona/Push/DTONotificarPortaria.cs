﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [Obsolete("SendPush: Esta classe foi substituída pela DTOSendPush")]
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Corona/Push")]
    public class DTONotificarPortaria : DTONotification
    {
        [DataMember]
        public int CodigoUnidadeHabitacional { get; set; }
    }
}