﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOPushChegadaSeguraRetorno : DTORetorno
    {
        [DataMember]
        public NotificationStatus StatusNotificacao { get; set; }

        [DataMember]
        public int CodigoNotificacao { get; set; }

        [DataMember]
        public DateTime DataStatus { get; set; }
    }
}