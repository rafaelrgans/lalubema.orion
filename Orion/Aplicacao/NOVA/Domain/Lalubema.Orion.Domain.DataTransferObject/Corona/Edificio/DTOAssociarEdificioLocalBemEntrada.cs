﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Edificio")]
    public class DTOAssociarEdificioLocalBemEntrada : DTOEntrada
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public IList<DTOLocalBem> LocalBens { get; set; }
    }
}