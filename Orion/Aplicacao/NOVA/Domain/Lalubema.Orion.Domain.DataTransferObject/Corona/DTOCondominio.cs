﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Autenticacao/Condominio")]
    public class DTOCondominio
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Logradouro { get; set; }

        [DataMember]
        public string Bairro { get; set; }

        [DataMember]
        public string Cidade { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Cep { get; set; }

        [DataMember]
        [Map("UrlFoto")]
        public string Foto { get; set; }
    }
}