﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.PessoaCondominio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/PessoaCondominio")]
    public class DTOPessoaCondominioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Categoria { get; set; }

        [DataMember]
        public string Funcao { get; set; }

        [DataMember]
        public DateTime? DataAtivacao { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}