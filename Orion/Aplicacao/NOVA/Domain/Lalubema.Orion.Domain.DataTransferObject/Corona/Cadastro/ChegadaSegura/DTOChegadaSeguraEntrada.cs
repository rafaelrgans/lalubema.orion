﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ChegadaSegura
{
    [ObjectMap("CoronaChegadaSegura")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/ChegadaSegura")]
    public class DTOChegadaSeguraEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public int TempoChegada { get; set; }

        [DataMember]
        public DateTime DataHoraInclusao { get; set; }

        [DataMember]
        public string StatusPortaria { get; set; }

        [DataMember]
        public DateTime? DataHoraStatus { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public TipoChegada TipoChegada { get; set; }
    }
}