﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Classificado
{
    [ObjectMap("CoronaClassificado")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Classificado")]
    public class DTOClassificadoEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public DateTime? DataPublicacao { get; set; }

        [DataMember]
        public DateTime? DataExpiracao { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public string FotoBase64 { get; set; }

        [DataMember]
        public decimal Preco { get; set; }

        [DataMember]
        public string Texto { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public DTOPessoa Pessoa { get; set; }

        [DataMember]
        public DTOUnidadeHabitacional UnidadeHabitacional { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}