﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Classificado
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Classificado")]
    public class DTOUnidadeHabitacional
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public string Obs { get; set; }

        [DataMember]
        public int TipoUnidade { get; set; }
    }
}