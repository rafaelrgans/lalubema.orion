﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ReservaLocalBem
{
    [ObjectMap("CoronaReservaLocalBem")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/ReservaLocalBem")]
    public class DTOReservaLocalBemEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public DateTime DataHoraInicio { get; set; }

        [DataMember]
        public DateTime DataHoraFim { get; set; }

        [DataMember]
        public StatusReserva Status { get; set; }
    }
}