﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.AutorizacaoEntradaEntrega
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/AutorizacaoEntradaEntrega")]
    public class DTOAutorizacaoEntradaEntregaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public DateTime DataHoraInicio { get; set; }

        [DataMember]
        public DateTime DataHoraFim { get; set; }

        [DataMember]
        public TipoAutorizacao Tipo { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}