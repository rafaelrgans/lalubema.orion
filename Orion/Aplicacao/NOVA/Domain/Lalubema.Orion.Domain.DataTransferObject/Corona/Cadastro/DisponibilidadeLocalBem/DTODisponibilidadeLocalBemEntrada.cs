﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.DisponibilidadeLocalBem
{
    [ObjectMap("CoronaDisponibilidadeLocalBem")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/DisponibilidadeLocalBem")]
    public class DTODisponibilidadeLocalBemEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string DiaSemana { get; set; }

        [DataMember]
        public TimeSpan HoraInicio { get; set; }

        [DataMember]
        public TimeSpan HoraFim { get; set; }
    }
}