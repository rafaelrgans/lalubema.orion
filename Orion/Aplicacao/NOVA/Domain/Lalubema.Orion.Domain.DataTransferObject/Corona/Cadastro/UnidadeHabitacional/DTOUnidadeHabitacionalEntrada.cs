﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.UnidadeHabitacional
{
    [ObjectMap("CoronaUnidadeHabitacional")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/UnidadeHabitacional")]
    public class DTOUnidadeHabitacionalEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public string Obs { get; set; }

        [DataMember]
        public int TipoUnidade { get; set; }

        [DataMember]
        public string NomeResponsavel { get; set; }

        [DataMember]
        public string EmailResponsavel { get; set; }

        [DataMember]
        public bool EnviarConvite { get; set; }

        [DataMember]
        public DTOEdificio Edificio { get; set; }
    }
}