﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/UnidadeHabitacional")]
    public class DTOUnidadeHabitacionalRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public string Obs { get; set; }

        [DataMember]
        public int TipoUnidade { get; set; }

        [DataMember]
        public virtual IList<DTOPessoaUnidadeHabitacional> Moradores { get; set; }
    }
}