﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Pessoa")]
    public class DTOPessoaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Map("Username")]
        public string Email { get; set; }

        [DataMember]
        [Map("Usuario.DataAtivacao")]
        public DateTime? DataAtivacao { get; set; }

        [DataMember]
        public string DocIdent { get; set; }

        [DataMember]
        public DateTime? DataNascimento { get; set; }

        [DataMember]
        [Map("Sexo1")]
        public string Sexo { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        [Map("FotoAnexo.Url")]
        public string Foto { get; set; }

        [DataMember]
        public IList<DTOPessoaCondominio> PessoaCondominios { get; set; }

        [DataMember]
        [Map("UnidadesHabitacional1")]
        public IList<DTOPessoaUnidadeHabitacional> UnidadesHabitacional { get; set; }

        [DataMember]
        [Map("Acessos1")]
        public IList<DTOPessoaAcesso> Acessos { get; set; }
    }
}