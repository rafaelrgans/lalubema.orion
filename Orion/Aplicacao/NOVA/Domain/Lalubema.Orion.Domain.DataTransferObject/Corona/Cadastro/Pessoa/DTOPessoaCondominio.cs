﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Pessoa")]
    public class DTOPessoaCondominio
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Categoria { get; set; }

        [DataMember]
        public string Funcao { get; set; }

        [DataMember]
        public DateTime? DataAtivacao { get; set; }

        [DataMember]
        [Map("Condominio.Codigo")]
        public int CodigoCondominio { get; set; }
    }
}