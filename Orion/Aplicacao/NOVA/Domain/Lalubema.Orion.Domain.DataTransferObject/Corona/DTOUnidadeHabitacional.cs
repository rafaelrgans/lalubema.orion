﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Autenticacao/UnidadeHabitacional")]
    public class DTOUnidadeHabitacional
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public DTOEdificio Edificio { get; set; }
    }
}