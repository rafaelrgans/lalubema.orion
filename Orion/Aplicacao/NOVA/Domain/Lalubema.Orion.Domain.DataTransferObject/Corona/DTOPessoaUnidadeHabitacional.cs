﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Autenticacao/PessoaUnidadeHabitacional")]
    public class DTOPessoaUnidadeHabitacional
    {
        [DataMember]
        public int Administrador { get; set; }

        [DataMember]
        public int Responsavel { get; set; }

        [DataMember]
        public DTOUnidadeHabitacional UnidadeHabitacional { get; set; }
    }
}