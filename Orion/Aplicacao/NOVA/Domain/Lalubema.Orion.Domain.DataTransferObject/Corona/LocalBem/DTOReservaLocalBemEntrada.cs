﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem")]
    public class DTOReservaLocalBemEntrada : DTOEntrada
    {
        [DataMember]
        public int CodigoLocalBem { get; set; }

        [DataMember]
        public int CodigoUnidadeHabitacional { get; set; }

        /// <summary>
        /// Define o usuário solicitante da reserva, 
        /// quando não for enviado usará o usuário de segurança [contexto iphone]
        /// </summary>
        [DataMember]
        public int? CodigoPessoa { get; set; }

        [DataMember]
        public DateTime Inicio { get; set; }

        [DataMember]
        public DateTime Fim { get; set; }
    }
}