﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOClassificado
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public decimal Preco { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public string Texto { get; set; }

        [DataMember]
        public string Foto { get; set; }

        [DataMember]
        [Map("Pessoa.Nome")]
        public string Morador { get; set; }

        [DataMember]
        [Map("UnidadeHabitacional.Numero")]
        public string UnidadeHabitacional { get; set; }
    }
}