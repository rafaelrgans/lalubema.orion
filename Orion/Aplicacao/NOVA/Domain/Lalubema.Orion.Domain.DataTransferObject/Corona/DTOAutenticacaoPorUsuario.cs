﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    public class DTOAutenticacaoPorUsuario : DTOAutenticacao
    {
        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public int TipoUsuario { get; set; }

        [DataMember]
        public IList<DTOPessoaCondominio> PessoaCondominios { get; set; }

        [DataMember]
        public IList<DTOPessoaUnidadeHabitacional> UnidadesHabitacional { get; set; }
    }
}