﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOPessoa
    {
        [DataMember]
        [Map("Pessoa.Codigo")]
        public int Codigo { get; set; }

        [DataMember]
        [Map("Pessoa.Nome")]
        public string Nome { get; set; }

        [DataMember]
        [Map("Pessoa.Sexo")]
        public int Sexo { get; set; }

        [DataMember]
        [Map("Pessoa.FotoAnexo.Url")]
        public string FotoUrl { get; set; }

        [DataMember]
        public string Categoria { get; set; }

        [DataMember]
        [Map("Responsavel1")]
        public int Responsavel { get; set; }

        [DataMember]
        [Map("Pessoa.Acessos")]
        public IList<DTOPessoaAcesso> Acessos { get; set; }
    }
}