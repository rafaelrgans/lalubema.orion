﻿using System.Net;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject
{
    [DataContract(Namespace = "http://www.lalubema.com/")]
    public class DTOEntidadeRetorno : DTORetorno
    {
        public DTOEntidadeRetorno()
        {
        }

        public DTOEntidadeRetorno(int codigo)
        {
            Codigo = codigo;
        }

        public DTOEntidadeRetorno(int codigo, HttpStatusCode httpStatusCode)
            : base(httpStatusCode)
        {
            Codigo = codigo;
        }

        [DataMember]
        public int Codigo { get; set; }
    }
}