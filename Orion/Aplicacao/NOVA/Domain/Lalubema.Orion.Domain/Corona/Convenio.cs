using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class Convenio : DomainBase<Convenio, IConvenioRepositorio<Convenio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime DataHoraInclusao { get; set; }

        public virtual string Estabelecimento { get; set; }

        public virtual string UrlFotoPromocional { get; set; }

        public virtual byte[] FotoPromocional { get; set; }

        public virtual string FotoPromocionalFileName { get; set; }

        public virtual string Endereco { get; set; }

        public virtual string UrlLogomarca { get; set; }

        public virtual byte[] Logomarca { get; set; }

        public virtual string LogomarcaFileName { get; set; }

        public virtual DateTime? InicioValidade { get; set; }

        public virtual string Telefone { get; set; }

        public virtual string Url { get; set; }

        public virtual string Beneficio { get; set; }

        public virtual DateTime? FimValidade { get; set; }

        public virtual string Obs { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual Usuario Pessoa { get; set; }

        public virtual CategoriaConvenio Categoria { get; set; }

        public override void Save()
        {
            base.Save();

            var oldFotoPromocional = UrlFotoPromocional;
            var oldLogomarca = UrlLogomarca;

            try
            {
                if (FotoPromocional != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, FotoPromocionalFileName, FotoPromocional);

                    UrlFotoPromocional = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldFotoPromocional))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldFotoPromocional);
                }

                if (Logomarca != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, LogomarcaFileName, Logomarca);

                    UrlLogomarca = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldLogomarca))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldLogomarca);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Convenio", Estabelecimento, exception);
            }

            base.Save();
        }
    }
}