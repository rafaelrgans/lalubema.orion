using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class Visitante : DomainBase<Visitante, IVisitanteRepositorio<Visitante>>, IDomainModel
    {
        public Visitante()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }
        public virtual string Relacao { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }
        public virtual Condominio Condominio { get; set; }
        public virtual Pessoa Pessoa { get; set; }

        private void OnCreated()
        {
        }

        public override void Save()
        {
            try
            {
                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Visitante", Pessoa.Nome, exception);
            }

            base.Save();
        }
    }
}