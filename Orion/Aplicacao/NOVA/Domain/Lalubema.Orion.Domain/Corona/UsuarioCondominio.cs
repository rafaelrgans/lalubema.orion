using System;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.Corona
{
    public class UsuarioCondominio : DomainBase<UsuarioCondominio, IUsuarioCondominioRepositorio<UsuarioCondominio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Grupo Grupo { get; set; }

        public virtual Condominio Condominio { get; set; }
        public virtual UnidadeHabitacional Unidade { get; set; }

        public virtual bool IsSindico()
        {
            return Grupo.Codigo == "SINDICO";
        }
    }
}