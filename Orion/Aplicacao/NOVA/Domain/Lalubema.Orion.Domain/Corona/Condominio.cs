using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using ConfigurationException = Lalubema.Utilitarios.Exceptions.Base.ConfigurationException;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;
using Lalubema.Orion.Common.NovosTipos;
using System.IO;

namespace Lalubema.Orion.Domain.Corona
{
    public class Condominio : DomainBase<Condominio, ICondominioRepositorio<Condominio>>, IDomainModel
    {
        private IList<Device> _devices;
        private IList<Convenio> _convenios;
        private IList<Edificio> _edificios;
        private IList<Licenca> _licencas;
        private IList<PessoaCondominio> _funcionarios;
        private IList<Classificado> _classificados;
        private IList<MensagemDestino> _mensagens;

        public Condominio()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            _convenios = new List<Convenio>();
            _edificios = new List<Edificio>();
            _licencas = new List<Licenca>();
            _funcionarios = new List<PessoaCondominio>();
            _classificados = new List<Classificado>();
            _mensagens = new List<MensagemDestino>();
            _devices = new List<Device>();
        }

        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual int NumEdificios { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual int CodigoSindico { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual Anexo FotoAnexo { get; set; }
        public virtual IList<Device> Devices
        {
            get { return _devices; }
            set { _devices = value; }
        }
        public virtual IList<Convenio> Convenios
        {
            get { return _convenios; }
            set { _convenios = value; }
        }
        public virtual IList<Edificio> Edificios
        {
            get { return _edificios; }
            set { _edificios = value; }
        }
        public virtual IList<Licenca> Licencas
        {
            get { return _licencas; }
            set { _licencas = value; }
        }
        public virtual IList<PessoaCondominio> Funcionarios
        {
            get { return _funcionarios; }
            set { _funcionarios = value; }
        }
        public virtual IList<Classificado> Classificados
        {
            get { return _classificados; }
            set { _classificados = value; }
        }
        public virtual IList<MensagemDestino> Mensagens
        {
            get { return _mensagens; }
            set { _mensagens = value; }
        }

        public virtual string Foto { get; set; }
        private string _logradouro;
        private string _bairro;
        private string _cidade;
        private string _estado;
        private string _cep;
        public virtual string Logradouro { get { return Endereco != null ? Endereco.Logradouro : string.Empty; } set { _logradouro = value; } }
        public virtual string Bairro { get { return Endereco != null ? Endereco.Bairro : string.Empty; } set { _bairro = value; } }
        public virtual string Cidade { get { return Endereco != null ? Endereco.Cidade : string.Empty; } set { _cidade = value; } }
        public virtual string Estado { get { return Endereco != null ? Endereco.Estado : string.Empty; } set { _estado = value; } }
        public virtual string Cep { get { return Endereco != null ? Endereco.CEP : string.Empty; } set { _cep = value; } }

        public override void Save()
        {
            try
            {
                SalvarFoto()
                .AdicionarSindico();

                int corporativo = Repository.Corporativo.HasValue ? Repository.Corporativo.Value : 1;
                var corp = new Corporativo().Get(corporativo);
                if(Licencas.Count(w => w.Corporativo.Codigo == corporativo) <= 0)
                    Licencas.Add(new Licenca() { Condominio = this, Corporativo = corp, Status = 1, DataAtivacao = DateTime.Now });

                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;

                if (!string.IsNullOrWhiteSpace(_logradouro) || !string.IsNullOrWhiteSpace(_bairro) ||
                    !string.IsNullOrWhiteSpace(_cidade) || !string.IsNullOrWhiteSpace(_estado) || !string.IsNullOrWhiteSpace(_cep))
                {
                    if (Endereco == null)
                        Endereco = new Endereco() { CreatedAt = DateTime.UtcNow, Pais = "Brasil" };

                    Endereco.Logradouro = _logradouro;
                    Endereco.Bairro = _bairro;
                    Endereco.Cidade = _cidade;
                    Endereco.CEP = _cep;
                    Endereco.Estado = _estado;
                    Endereco.UpdatedAt = DateTime.UtcNow;
                }
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Condominio", Nome, exception);
            }

            base.Save();
        }

        private void AdicionarSindico()
        {
            var pessoaCondominio = Funcionarios.FirstOrDefault(o => o.Pessoa.Codigo == CodigoSindico);

            if (pessoaCondominio == null)
            {
                var sindico = Pessoa.GetRepository().Get(CodigoSindico);

                if (sindico == null)
                    throw new DadosInvalidosException("Sindico informado � inv�lido.");

                pessoaCondominio = new PessoaCondominio
                                       {
                                           Pessoa = sindico,
                                           Condominio = this,
                                           DataAtivacao = DateTime.UtcNow,
                                           CreatedAt = DateTime.UtcNow,
                                           UpdatedAt = DateTime.UtcNow
                                       };

                Funcionarios.Add(pessoaCondominio);
            }

            pessoaCondominio.Funcao = "S�ndico";
            pessoaCondominio.Categoria = "S";
        }

        private Condominio SalvarFoto()
        {
            var oldUrl = FotoAnexo != null ? FotoAnexo.Url : string.Empty;
            string retornoPath = null;

            try
            {
                if (Foto != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(Foto),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, extension, fileHelper.File);

                        if (FotoAnexo == null)
                            FotoAnexo = new Anexo();

                        FotoAnexo.CreatedAt = DateTime.Now;
                        FotoAnexo.UpdatedAt = DateTime.Now;
                        FotoAnexo.Extensao = extension;
                        FotoAnexo.Nome = Path.GetFileName(retorno.Path);
                        FotoAnexo.Status = (byte)AnnexStatus.Active;
                        FotoAnexo.Tamanho = fileHelper.File.Length;
                        FotoAnexo.Url = retorno.Path;
                        FotoAnexo.Entidade = (byte)AnnexEntity.Picture;
                        FotoAnexo.Tipo = (byte)AnnexType.Image;

                        retornoPath = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldUrl);
                }
            }
            catch (DomainLayerException)
            {
                if (!string.IsNullOrWhiteSpace(retornoPath))
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, retornoPath);

                throw;
            }
            catch (Exception exception)
            {
                if (!string.IsNullOrWhiteSpace(retornoPath))
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, retornoPath);

                throw new FalhaAoSalvarException("Condominio", Nome, exception);
            }

            return this;
        }

        public virtual Classificado GetLastClassificado()
        {
            return Classificados.OrderByDescending(t => t.DataPublicacao).FirstOrDefault();
        }

        public virtual Mensagem GetLastMensagem()
        {
            if (Mensagens.Count > 0)
            {
                var mensagem =
                    Mensagens.Where(t => t.Mensagem != null).OrderByDescending(t => t.Mensagem.DataPublicacao).FirstOrDefault();

                if (mensagem != null)
                    return mensagem.Mensagem;
            }

            return null;
        }

        public virtual bool IsSindico(int codigoUsuario)
        {
            return Funcionarios.Any(t => t.Pessoa.Codigo == codigoUsuario && t.IsSindico());
        }

        private DTORegisterDevice RegisterDeviceData { get; set; }

		public virtual Condominio RegisterDevice(DTORegisterDevice registerDevice)
		{
			RegisterDeviceData = registerDevice;
			Device device;

            if (registerDevice.Bundle == "com.lalubema.MCSindico")
            {
                AuthorizeRequestByUser(registerDevice.ChaveCriptografada).LoadDevice(out device);

                device.APIVersion = registerDevice.APIVersion;

                return SetNotificationSettings(device).SaveDevice(device);
            }
            else
            {
                LoadDevice(out device);

                device.APIVersion = registerDevice.APIVersion;

                return SetNotificationSettings(device).SaveDevice(device);
            }
		}

        public virtual Condominio UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            // Valida o acesso
            AuthorizeRequestByUser(unregisterDevice.ChaveCriptografada);

            // seleciona o Device
            Device device = new Device().Repository.Get(unregisterDevice.DeviceToken);
            if (device == null)
                throw new ItemNotFoundException(unregisterDevice.DeviceToken, "Device");

            // Remove o Device
            device.Status = (byte)DeviceStatus.Inactive;
            device.Save();

            // Retona o resultado
            return this;
        }

        private Condominio SaveDevice(Device device)
        {
            Devices.Add(device);
            base.Save();
            return this;
        }

        private Condominio LoadDevice(out Device device)
        {
            device = new Device().Repository.Get(RegisterDeviceData.DeviceToken) ?? new Device
                                                                                        {
                                                                                            Nome = RegisterDeviceData.Name,
                                                                                            Token = RegisterDeviceData.DeviceToken,
                                                                                            Plataforma = (byte)RegisterDeviceData.PlatformType,
                                                                                            CreatedAt = DateTime.UtcNow
                                                                                        };

            device.Condominio = this;
            device.Pessoa = null;
            device.Plataforma = (byte)RegisterDeviceData.PlatformType;
            device.Status = (byte)DeviceStatus.Active;
            device.UpdatedAt = DateTime.UtcNow;

            return this;
        }

        private Condominio SetNotificationSettings(Device device)
        {
            var notificationSettings =
                Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString()).NotificationSettings ??
                new List<NotificationSettings>();

            var setting = notificationSettings.FirstOrDefault(t => t.Bundle.Equals(RegisterDeviceData.Bundle));

            if (setting == null)
                throw new ConfigurationException("NotificationSettings");

            device.NotificationSettings = setting;

            return this;
        }

        private Condominio AuthorizeRequestByUser(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            if (
                !Funcionarios.Any(
                    t =>
                    t.Pessoa.Codigo == security.CodigoUsuario))
                throw new UnauthorizedAccessException();

            return this;
        }
    }
}