using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class AcessoPessoa : DomainBase<AcessoPessoa, IAcessoPessoaRepositorio<AcessoPessoa>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual int CodigoPessoa { get; set; }
        public virtual Pessoa Pessoa { get; set; }

        public virtual int CodigoUnidadeHabitacional { get; set; }
        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }
        public virtual int CodigoCondominio { get; set; }
        public virtual Condominio Condominio { get; set; }
        public virtual byte Status { get; set; }
        public virtual byte Tipo { get; set; }
        public virtual byte DiaSemana { get; set; }
        public virtual TimeSpan HoraInicio { get; set; }
        public virtual TimeSpan HoraFim { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public virtual string GetHour()
        {
            var manha = new { Inicio = new TimeSpan(6, 0, 0), Fim = new TimeSpan(11, 59, 59) };
            var tarde = new { Inicio = new TimeSpan(12, 0, 0), Fim = new TimeSpan(17, 59, 59) };
            var noite = new { Inicio = new TimeSpan(18, 0, 0), Fim = new TimeSpan(23, 59, 59) };

            if (HoraInicio >= manha.Inicio && HoraFim <= manha.Fim)
                return "M";
            else if (HoraInicio >= tarde.Inicio && HoraFim <= tarde.Fim)
                return "T";
            else if (HoraInicio >= noite.Inicio && HoraFim <= noite.Fim)
                return "N";
            else
                return null;
        }

        public virtual string Domingo { get; set; }
        public virtual string Segunda { get; set; }
        public virtual string Terca { get; set; }
        public virtual string Quarta { get; set; }
        public virtual string Quinta { get; set; }
        public virtual string Sexta { get; set; }
        public virtual string Sabado { get; set; }

        public override void Save()
        {
            // Pessoa (Usu�rio)
            if ((Pessoa == null || Pessoa.Codigo != CodigoPessoa) && CodigoPessoa > 0)
            {
                Pessoa = new Pessoa();
                Pessoa = Pessoa.Get(CodigoPessoa);
            }

            // Unidade Habitacional
            if ((UnidadeHabitacional == null || UnidadeHabitacional.Codigo != CodigoUnidadeHabitacional) && CodigoUnidadeHabitacional > 0)
            {
                UnidadeHabitacional = new UnidadeHabitacional();
                UnidadeHabitacional = UnidadeHabitacional.Get(CodigoUnidadeHabitacional);
            }

            // Condominio
            if ((Condominio == null || Condominio.Codigo != CodigoCondominio) && CodigoCondominio > 0)
            {
                Condominio = new Condominio();
                Condominio = Condominio.Get(CodigoCondominio);
            }

            base.Save();
        }
    }
}