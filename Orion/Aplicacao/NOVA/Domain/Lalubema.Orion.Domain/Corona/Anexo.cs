﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.NovosTipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class Anexo : DomainBase<Anexo, IAnexoRepositorio<Anexo>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual byte Status { get; set; }

        public virtual string Url { get; set; }

        public virtual int Tamanho { get; set; }

        public virtual string Titulo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual byte Tipo { get; set; }

        public virtual string Extensao { get; set; }

        public virtual byte Entidade { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
    }
}