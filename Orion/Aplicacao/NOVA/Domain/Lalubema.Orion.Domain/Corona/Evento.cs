using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Security;
using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class Evento : DomainBase<Evento, IEdificioRepositorio<Evento>>, IDomainModel
    {
        public Evento()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Observacao { get; set; }
        public virtual string Descricao { get; set; }
        public virtual bool AvisarChegada { get; set; }
        public virtual byte Status { get; set; }
        public virtual DateTime DataHoraInicio { get; set; }
        public virtual DateTime? DataHoraFim { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual Condominio Condominio { get; set; }
        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }
        public virtual LocalBem Local { get; set; }

        private void OnCreated()
        {
        }

        public override void Save()
        {
            if (Codigo == 0)
            {
                CreatedAt = DateTime.Now;
                Status = 1;
            }
            UpdatedAt = DateTime.Now;

            base.Save();
        }
    }
}