﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class UsuarioPermissao : DomainBase<UsuarioPermissao, IUsuarioPermissaoRepositorio<UsuarioPermissao>>, IDomainModel
    {
        public virtual int ID { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Permissao Permissao { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }
    }
}