﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class EdificioContato : DomainBase<EdificioContato, IEdificioContatoRepositorio<EdificioContato>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual Edificio Edificio { get; set; }

        public virtual Contato Contato { get; set; }
    }
}