﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class MensagemDestino : DomainBase<MensagemDestino, IMensagemDestinoRepositorio<MensagemDestino>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual Mensagem Mensagem { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual Edificio Edificio { get; set; }

        public virtual Condominio Condominio { get; set; }
    }
}