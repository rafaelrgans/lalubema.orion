﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.Corona
{
    public class GrupoPermissao : DomainBase<GrupoPermissao, IGrupoPermissaoRepositorio<GrupoPermissao>>, IDomainModel
    {
        public virtual int ID { get; set; }

        public virtual Grupo Grupo { get; set; }

        public virtual Permissao Permissao { get; set; }

        public virtual Condominio Condominio { get; set; }
    }
}