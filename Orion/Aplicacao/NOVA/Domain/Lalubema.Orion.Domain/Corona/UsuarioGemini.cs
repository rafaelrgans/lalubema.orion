
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.NovosTipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Exceptions.Base;

namespace Lalubema.Orion.Domain.Corona
{
    public class UsuarioGemini : DomainBase<UsuarioGemini, IUsuarioGeminiRepositorio<UsuarioGemini>>
    {
        public UsuarioGemini()
        {
        }

        public virtual int Codigo { get; set; }
        public virtual int IDUsuario { get; set; }
        public virtual byte Status { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Email { get; set; }
        public virtual string SegredoSenha { get; set; }
        public virtual string Senha { get; set; }
    }
}