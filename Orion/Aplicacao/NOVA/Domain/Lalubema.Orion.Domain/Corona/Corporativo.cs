using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class Corporativo : DomainBase<Corporativo, ICorporativoRepositorio<Corporativo>>, IDomainModel
    {
        public Corporativo()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        private void OnCreated()
        {
        }

        public override void Save()
        {
            try
            {
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Corporativo", Nome, exception);
            }

            base.Save();
        }
    }
}