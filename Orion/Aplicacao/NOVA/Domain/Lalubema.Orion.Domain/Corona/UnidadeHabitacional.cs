using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.NovosTipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;

namespace Lalubema.Orion.Domain.Corona
{
    public class UnidadeHabitacional : DomainBase<UnidadeHabitacional, IUnidadeHabitacionalRepositorio<UnidadeHabitacional>>, IDomainModel
    {
        #region Extensibility Method Definitions

        private void OnCreated()
        {
            Acessos = new List<AcessoPessoa>();
            Autorizacoes = new List<AutorizacaoEntradaEntrega>();
            Moradores = new List<PessoaUnidadeHabitacional>();
            ReservasLocalBem = new List<ReservaLocalBem>();
            Veiculos = new List<Veiculo>();
            Funcionarios = new List<PessoaCondominio>();
            Visitantes = new List<Visitante>();
        }

        #endregion

        public UnidadeHabitacional()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }
        public virtual string Numero { get; set; }
        public virtual string Obs { get; set; }
        public virtual byte TipoUnidade { get; set; }

        public virtual IList<AcessoPessoa> Acessos { get; set; }
        public virtual IList<AutorizacaoEntradaEntrega> Autorizacoes { get; set; }
        public virtual IList<PessoaUnidadeHabitacional> Moradores { get; set; }
        public virtual IList<PessoaCondominio> Funcionarios { get; set; }
        public virtual IList<Visitante> Visitantes { get; set; }
        public virtual IList<ReservaLocalBem> ReservasLocalBem { get; set; }
        public virtual Edificio Edificio { get; set; }
        public virtual IList<Veiculo> Veiculos { get; set; }

        public virtual string NomeResponsavel { get; set; }

        public virtual string EmailResponsavel { get; set; }

        public virtual bool EnviarConvite { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public override void Save()
        {
            if (Codigo == 0)
                CreatedAt = DateTime.UtcNow;

            UpdatedAt = DateTime.UtcNow;

            base.Save();

            if (string.IsNullOrWhiteSpace(EmailResponsavel))
                return;

            var responsavel = Pessoa.GetRepository().Get(EmailResponsavel);
            if (responsavel == null)
            {
                responsavel = new Pessoa
                {
                    Nome = NomeResponsavel,
                    Sexo = (byte)PersonGender.Other
                };
                responsavel.Contatos.Add(new PessoaContato() { Pessoa = responsavel, Contato = new Contato() {
                    Tipo = (byte)ContactType.Email,
                    Valor = EmailResponsavel,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow
                } });
            }

            var pessoaUnidadeHabitacional = new DTOPessoaUnidadeHabitacional
                                                {
                                                    Administrador = 1,
                                                    Responsavel = 1,
                                                    PermissaoAvisos = 2,
                                                    PermissaoChegadaSegura = 2,
                                                    PermissaoPessoas = 2,
                                                    PermissaoReserva = 2,
                                                    PermissaoVeiculos = 2,
                                                    PermissaoClassificados = 0,
                                                    PermissaoNotificacaoPortaria = 0,
                                                    Categoria = "M",
                                                    CodigoUnidadeHabitacional = Codigo,
                                                    DataAtivacao = DateTime.UtcNow
                                                };

            responsavel.Save(new List<DTOPessoaUnidadeHabitacional> { pessoaUnidadeHabitacional }, null, null);
        }

        public virtual IList<UnidadeHabitacional> ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            var unidadesHabitacionais =
                AuthorizeRequestPorCondominio(chaveCriptografada, codigoCondominio)
                    .Repository.ListarUnidadeHabitacionalPorCondominio(codigoCondominio);

            return unidadesHabitacionais.OrderBy(t => t.Edificio.Codigo).ThenBy(t => t.Numero.PadLeft(6, '0')).ToList();
        }

        private UnidadeHabitacional AuthorizeRequestPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var isFuncionario = new Condominio().Get(codigoCondominio).Funcionarios.Any(t => t.Pessoa.Codigo == security.CodigoUsuario);

            if (!isFuncionario)
                throw new UnauthorizedAccessException();

            return this;
        }
    }
}