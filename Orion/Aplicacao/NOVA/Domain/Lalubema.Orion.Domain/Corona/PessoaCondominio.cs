using System;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Utilitarios.Attributes;
using System.Collections.Generic;
using System.Text;

namespace Lalubema.Orion.Domain.Corona
{
    public class PessoaCondominio : DomainBase<PessoaCondominio, IPessoaCondominioRepositorio<PessoaCondominio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual byte Status { get; set; }

        public virtual string Categoria { get; set; }

        public virtual string Funcao { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual Edificio Edificio { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual DateTime UpdatedAt { get; set; }

        public virtual bool IsSindico()
        {
            return String.Equals(this.Categoria, "S", StringComparison.InvariantCultureIgnoreCase);
        }

        public static char ObterCategoria(string funcao)
        {
            if(string.IsNullOrWhiteSpace(funcao))
                return 'F';

            var sindico = new[] { "sindico", "sindica" };

            if(sindico.Contains(funcao, new FuncaoComparer()))
                return 'S';
            else
                return 'F';
        }
    }

    public class FuncaoComparer : IEqualityComparer<string>
    {

        private string RemoverAcentos(string texto)
        {
            string s = texto.Normalize(NormalizationForm.FormD);

            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(s[k]);
                }
            }
            return sb.ToString();
        }

        public bool Equals(string x, string y)
        {
            x = RemoverAcentos(x).ToLower().Trim();
            y = RemoverAcentos(y).ToLower().Trim();

            return y.Contains(x);
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }
}