﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.NovosTipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class Device : DomainBase<Device, IDeviceRepository<Device>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual byte Status { get; set; }

        public virtual string Token { get; set; }

        public virtual byte Plataforma { get; set; }

        public virtual NotificationSettings NotificationSettings { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public virtual float? APIVersion { get; set; }

        public virtual Device Get(string deviceToken)
        {
            var device = Repository.Get(deviceToken);

            if (device == null)
                throw new ItemNotFoundException(deviceToken, "Device");

            return device;
        }

        public virtual void ChangeToken(string newTokenDevice)
        {
            Token = newTokenDevice;

            base.Save();
        }

        public override void Delete()
        {
            Status = 2;

            base.Save();
        }
    }
}