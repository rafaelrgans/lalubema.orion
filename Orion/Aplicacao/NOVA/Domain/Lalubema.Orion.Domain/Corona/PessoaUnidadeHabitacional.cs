using System;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.Corona
{
    public class PessoaUnidadeHabitacional : DomainBase<PessoaUnidadeHabitacional, IPessoaUnidadeHabitacionalRepositorio<PessoaUnidadeHabitacional>>, IDomainModel
    {
        public virtual int Codigo { get; set; }
        public virtual byte Status { get; set; }
        public virtual string Categoria { get; set; }
        public virtual string RelacaoFuncao { get; set; }
        public virtual bool Responsavel { get; set; }
        public virtual int Responsavel1 { get { return Responsavel ? 1 : 0; } }
        public virtual int Administrador { get; set; }
        private int _permissaoPessoas = -1;
        public virtual int PermissaoPessoas { get {
            if(_permissaoPessoas == -1)
                _permissaoPessoas = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_PESSOAS) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_PESSOAS) ? 1 : 0;
            return _permissaoPessoas;
        }
            set { _permissaoPessoas = value; }
        }
        private int _permissaoVeiculos = -1;
        public virtual int PermissaoVeiculos
        {
            get
            {
                if (_permissaoVeiculos == -1)
                    _permissaoVeiculos = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_VEICULOS) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_VEICULOS) ? 1 : 0;
                return _permissaoVeiculos;
            }
            set { _permissaoVeiculos = value; }
        }
        private int _permissaoAvisos = -1;
        public virtual int PermissaoAvisos
        {
            get
            {
                if (_permissaoAvisos == -1)
                    _permissaoAvisos = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_AVISOS) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_AVISOS) ? 1 : 0;
                return _permissaoAvisos;
            }
            set { _permissaoAvisos = value; }
        }
        private int _permissaoChegadaSegura = -1;
        public virtual int PermissaoChegadaSegura
        {
            get
            {
                if (_permissaoChegadaSegura == -1)
                    _permissaoChegadaSegura = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_CHEGADASEGURA) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_CHEGADASEGURA) ? 1 : 0;
                return _permissaoChegadaSegura;
            }
            set { _permissaoChegadaSegura = value; }
        }
        private int _permissaoReserva = -1;
        public virtual int PermissaoReserva
        {
            get
            {
                if (_permissaoReserva == -1)
                    _permissaoReserva = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_RESERVAS) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_RESERVAS) ? 1 : 0;
                return _permissaoReserva;
            }
            set { _permissaoReserva = value; }
        }
        private int _permissaoClassificados = -1;
        public virtual int PermissaoClassificados
        {
            get
            {
                if (_permissaoClassificados == -1)
                    _permissaoClassificados = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_CLASSIFICADOS) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_CLASSIFICADOS) ? 1 : 0;
                return _permissaoClassificados;
            }
            set { _permissaoClassificados = value; }
        }
        private int _permissaoNotificacaoPortaria = -1;
        public virtual int PermissaoNotificacaoPortaria
        {
            get
            {
                if (_permissaoNotificacaoPortaria == -1)
                    _permissaoNotificacaoPortaria = HasPermissao(Constantes.Corona.Usuario.Permissao.TOTAL_NOTIFICACAO) ? 2 : HasPermissao(Constantes.Corona.Usuario.Permissao.VISUALIZAR_NOTIFICACAO) ? 1 : 0;
                return _permissaoNotificacaoPortaria;
            }
            set { _permissaoNotificacaoPortaria = value; }
        }

        public virtual DateTime? DataAtivacao { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual int CodigoUnidadeHabitacional { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        private bool HasPermissao(string cd)
        {
            if (Pessoa.Usuario == null)
                return false;

            var ua = UnidadeHabitacional != null ? UnidadeHabitacional : new UnidadeHabitacional().Get(CodigoUnidadeHabitacional);
            return Pessoa.Usuario.Permissoes.Any(w => w.Condominio.Codigo == ua.Edificio.Condominio.Codigo && w.Permissao.Codigo.Equals(cd, StringComparison.InvariantCultureIgnoreCase));
        }

        public PessoaUnidadeHabitacional()
        {
            OnCreated();
        }

        public override void Save(bool flush)
        {
            base.Save(flush);
        }

        public override void Save()
        {
            base.Save();
        }

        private void OnCreated()
        {
            Categoria = "M";
        }
    }
}