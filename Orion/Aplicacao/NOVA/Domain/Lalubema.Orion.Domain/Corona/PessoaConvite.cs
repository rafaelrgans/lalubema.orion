using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class PessoaConvite : DomainBase<PessoaConvite, IPessoaConviteRepositorio<PessoaConvite>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual Pessoa Pessoa { get; set; }
        public virtual Pessoa Solicitante { get; set; }
        public virtual UnidadeHabitacional Unidade { get; set; }
        public virtual Condominio Condominio { get; set; }
        public virtual byte Status { get; set; }
        public virtual int Envio { get; set; }
        public virtual string Email { get; set; }
        public virtual string Token { get; set; }
        public virtual string Comentario { get; set; }
        public virtual string Grupo { get; set; }
        public virtual string Permissao { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public override void Save()
        {
            if (Codigo == 0)
                CreatedAt = DateTime.UtcNow;

            UpdatedAt = DateTime.UtcNow;

            base.Save();
        }
    }
}