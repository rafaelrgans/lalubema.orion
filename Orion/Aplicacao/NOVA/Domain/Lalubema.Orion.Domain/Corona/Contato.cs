﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.NovosTipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class Contato : DomainBase<Contato, IContatoRepositorio<Contato>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Valor { get; set; }

        public virtual byte Tipo { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
    }
}