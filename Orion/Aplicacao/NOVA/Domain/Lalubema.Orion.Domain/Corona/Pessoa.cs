
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.NovosTipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Exceptions.Base;
using System.Threading;
using System.IO;
using Lalubema.Utilitarios.Helper.Log;
using Common.Logging;
using System.Threading.Tasks;

namespace Lalubema.Orion.Domain.Corona
{
    public class Pessoa : DomainBase<Pessoa, IPessoaRepositorio<Pessoa>>, IUsuario
    {
        private IList<PessoaContato> _contatos;
        private IList<Device> _devices;
        private IList<AcessoPessoa> _acessos;
        private IList<AutorizacaoEntradaEntrega> _autorizacoes;
        private IList<ChegadaSegura> _chegadasSeguraMorador;
        private IList<ChegadaSegura> _chegadasSeguraPorteiro;
        private IList<Classificado> _classificados;
        private IList<Convenio> _convenios;
        private IList<Mensagem> _mensagensEnviadas;
        private IList<ReservaLocalBem> _reservaLocalBems;
        private IList<ReservaLocalBem> _reservasAreaComum;
        private IList<PessoaUnidadeHabitacional> _unidadesHabitacional;
        private IList<PessoaCondominio> _pessoaCondominios;
        private IList<Visitante> _visitantes;
        private IList<Mensagem> _mensagensRecebidas;
        private IList<PessoaConvite> _convites;

        public Pessoa()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual DateTime? DataNascimento { get; set; }
        public virtual string DocIdent { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual Anexo FotoAnexo { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual int Sexo { get; set; }
        public virtual Lalubema.Orion.Common.Tipos.Sexo Sexo1 { get { return (Lalubema.Orion.Common.Tipos.Sexo)Sexo; } set { } }
        public virtual Aplicacao Aplicacao { get { return new Aplicacao().Repository.Buscar("Corona"); } set { } }

        private string _telefone;
        public virtual string Telefone
        {
            get
            {
                var ct = Contatos.FirstOrDefault(w => w.Contato.Tipo == (byte)ContactType.HomePhone);
                return ct != null ? ct.Contato.Valor : string.Empty;
            }
            set { _telefone = value; }
        }
        public virtual string Foto { get; set; }
        private bool FotoAlterada { get; set; }
        private string _email;
        [Map("Email")]
        public virtual string Username { get {
            if (string.IsNullOrWhiteSpace(_email))
                _email = Convites.Select(w => w.Email).FirstOrDefault();

            return _email;
        } set { _email = value; } }
        public virtual string Senha { get; set; }
        public virtual int TipoUsuario { get; set; }

        public virtual IList<PessoaContato> Contatos
        {
            get { return _contatos; }
            set { _contatos = value; }
        }
        public virtual IList<Device> Devices
        {
            get { return _devices; }
            set { _devices = value; }
        }
        [NoMap]
        public virtual IList<AcessoPessoa> Acessos
        {
            get { return _acessos; }
            set { _acessos = value; }
        }
        [NoMap]
        public virtual IList<AcessoPessoa> Acessos1
        {
            get {
                List<AcessoPessoa> novoAcessos = new List<AcessoPessoa>();
                foreach (var acesso in _acessos)
                {
                    var horario = acesso.GetHour();
                    var acessoSemana = novoAcessos.FirstOrDefault(w => w.Condominio.Codigo == acesso.Condominio.Codigo && 
                        (w.UnidadeHabitacional == null || acesso.UnidadeHabitacional == null || w.UnidadeHabitacional.Codigo == acesso.UnidadeHabitacional.Codigo));
                    if (acessoSemana == null)
                    {
                        acessoSemana = acesso;
                        novoAcessos.Add(acesso);
                    }

                    switch (acesso.DiaSemana)
                    {
                        case (byte)DayOfWeek.Sunday:
                            acessoSemana.Domingo += horario;
                            break;
                        case (byte)DayOfWeek.Monday:
                            acessoSemana.Segunda += horario;
                            break;
                        case (byte)DayOfWeek.Tuesday:
                            acessoSemana.Terca += horario;
                            break;
                        case (byte)DayOfWeek.Wednesday:
                            acessoSemana.Quarta += horario;
                            break;
                        case (byte)DayOfWeek.Thursday:
                            acessoSemana.Quinta += horario;
                            break;
                        case (byte)DayOfWeek.Friday:
                            acessoSemana.Sexta += horario;
                            break;
                        case (byte)DayOfWeek.Saturday:
                            acessoSemana.Sabado += horario;
                            break;
                    }
                }

                return novoAcessos;
            }
            set { _acessos = value; }
        }
        public virtual IList<AutorizacaoEntradaEntrega> Autorizacoes
        {
            get { return _autorizacoes; }
            set { _autorizacoes = value; }
        }
        public virtual IList<ChegadaSegura> ChegadasSeguraMorador
        {
            get { return _chegadasSeguraMorador; }
            set { _chegadasSeguraMorador = value; }
        }
        public virtual IList<ChegadaSegura> ChegadasSeguraPorteiro
        {
            get { return _chegadasSeguraPorteiro; }
            set { _chegadasSeguraPorteiro = value; }
        }
        public virtual IList<Classificado> Classificados
        {
            get { return _classificados; }
            set { _classificados = value; }
        }
        public virtual IList<Convenio> Convenios
        {
            get { return _convenios; }
            set { _convenios = value; }
        }
        public virtual IList<Mensagem> MensagensEnviadas
        {
            get { return _mensagensEnviadas; }
            set { _mensagensEnviadas = value; }
        }
        private IList<PessoaUnidadeHabitacional> _unidadesHabitacional1;
        public virtual IList<PessoaUnidadeHabitacional> UnidadesHabitacional1
        {
            get
            {
                _unidadesHabitacional1 = new List<PessoaUnidadeHabitacional>(_unidadesHabitacional.ToList());

                foreach (var f in _pessoaCondominios.Where(w => w.UnidadeHabitacional != null))
                    _unidadesHabitacional1.Add(new PessoaUnidadeHabitacional()
                    {
                        Administrador = 0,
                        Categoria = "F",
                        Codigo = Convert.ToInt32("-1" + f.Codigo),
                        CodigoUnidadeHabitacional = f.UnidadeHabitacional.Codigo,
                        CreatedAt = f.CreatedAt,
                        DataAtivacao = f.DataAtivacao,
                        Pessoa = this,
                        RelacaoFuncao = f.Funcao,
                        Responsavel = false,
                        Status = f.Status,
                        UnidadeHabitacional = f.UnidadeHabitacional,
                        UpdatedAt = f.UpdatedAt
                    });

                foreach (var f in _visitantes.Where(w => w.UnidadeHabitacional != null))
                    _unidadesHabitacional1.Add(new PessoaUnidadeHabitacional()
                    {
                        Administrador = 0,
                        Categoria = "V",
                        Codigo = Convert.ToInt32("-2" + f.Codigo),
                        CodigoUnidadeHabitacional = f.UnidadeHabitacional.Codigo,
                        CreatedAt = f.CreatedAt,
                        Pessoa = this,
                        RelacaoFuncao = f.Relacao,
                        Responsavel = false,
                        Status = 0,
                        UnidadeHabitacional = f.UnidadeHabitacional,
                        UpdatedAt = f.UpdatedAt
                    });

                return _unidadesHabitacional1;
            }
            set { _unidadesHabitacional = value; }
        }
        [NoMap]
        public virtual IList<PessoaUnidadeHabitacional> UnidadesHabitacional
        {
            get {
                return _unidadesHabitacional;
            }
            set { _unidadesHabitacional = value; }
        }
        [NoMap]
        public virtual IList<PessoaCondominio> PessoaCondominios
        {
            get { return _pessoaCondominios; }
            set { _pessoaCondominios = value; }
        }
        [NoMap]
        public virtual IList<Visitante> Visitantes
        {
            get { return _visitantes; }
            set { _visitantes = value; }
        }
        public virtual IList<ReservaLocalBem> ReservasAreaComum
        {
            get { return _reservasAreaComum; }
            set { _reservasAreaComum = value; }
        }
        public virtual IList<ReservaLocalBem> ReservaLocalBems
        {
            get { return _reservaLocalBems; }
            set { _reservaLocalBems = value; }
        }
        public virtual IList<Mensagem> MensagensRecebidas
        {
            get { return _mensagensRecebidas; }
            set { _mensagensRecebidas = value; }
        }
        public virtual IList<PessoaConvite> Convites
        {
            get { return _convites; }
            set { _convites = value; }
        }

        private IList<int> UnidadesAdicionadas { get; set; }
        private IList<int> CondominiosAdicionados { get; set; }

        private readonly TimeSpan DurationPush = new TimeSpan(1, 0, 0, 0);

        private void OnCreated()
        {
            _devices = new List<Device>();
            _acessos = new List<AcessoPessoa>();
            _autorizacoes = new List<AutorizacaoEntradaEntrega>();
            _chegadasSeguraMorador = new List<ChegadaSegura>();
            _chegadasSeguraPorteiro = new List<ChegadaSegura>();
            _classificados = new List<Classificado>();
            _convenios = new List<Convenio>();
            _mensagensEnviadas = new List<Mensagem>();
            _contatos = new List<PessoaContato>();
            _unidadesHabitacional = new List<PessoaUnidadeHabitacional>();
            _reservasAreaComum = new List<ReservaLocalBem>();
            _reservaLocalBems = new List<ReservaLocalBem>();
            _mensagensRecebidas = new List<Mensagem>();
            _pessoaCondominios = new List<PessoaCondominio>();
            _visitantes = new List<Visitante>();
            _convites = new List<PessoaConvite>();
        }

        private static EmailSettings GetEmailSettings(string sendTo, int corporativo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente, corporativo),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer, corporativo),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort, corporativo)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL, corporativo)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp, corporativo),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp, corporativo)
            };
        }

        private Pessoa AdicionarContatos()
        {
            if (!string.IsNullOrWhiteSpace(_telefone))
            {
                var ct = Contatos.FirstOrDefault(w => w.Contato.Tipo == (byte)ContactType.HomePhone);
                if (ct == null)
                {
                    ct = new PessoaContato()
                    {
                        Pessoa = this,
                        Contato = new Contato()
                        {
                            CreatedAt = DateTime.UtcNow,
                            UpdatedAt = DateTime.UtcNow,
                            Tipo = (byte)ContactType.HomePhone,
                            Valor = _telefone
                        }
                    };
                    Contatos.Add(ct);
                }
                ct.Contato.Valor = _telefone;
            }

            if (!string.IsNullOrWhiteSpace(Username))
            {
                var em = Contatos.FirstOrDefault(w => w.Contato.Tipo == (byte)ContactType.Email);
                if (em == null)
                {
                    em = new PessoaContato()
                    {
                        Pessoa = this,
                        Contato = new Contato()
                        {
                            CreatedAt = DateTime.UtcNow,
                            UpdatedAt = DateTime.UtcNow,
                            Tipo = (byte)ContactType.Email,
                            Valor = Username
                        }
                    };
                    Contatos.Add(em);
                }
                em.Contato.Valor = Username;
            }

            return this;
        }

        private Pessoa AdicionarUsuario(IList<DTOPessoaCondominio> pessoaCondominios)
        {
            // Procura o usu�rio por e-mail (username)
            var pessoa = String.IsNullOrEmpty(Username) ? null : Repository.Get(Username, Codigo);

            // N�o possui c�digo informado
            if (Codigo == 0)
            {
                // Pesquisa por e-mail (username) para edi��o
                if (pessoa != null)
                {
                    Load(pessoa.Codigo);
                }
            }
            else
            {
                // Valida se existe alguma colis�o de e-mail com outro usu�rio
                if (pessoa != null && pessoa.Codigo != Codigo)
                    throw new UsuarioJaCadastradoException(Username);
            }

            if (!string.IsNullOrWhiteSpace(Senha))
                if (!SecurityHelper.isValidSHA1(Senha))
                    Senha = SecurityHelper.EncryptToSHA1(Senha);

            if (Usuario == null && !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Senha))
            {
                Usuario = new Usuario()
                {
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    Senha = Senha,
                    Status = (byte)UserStatus.Active,
                    DataAtivacao = DateTime.Now,
                    FalhasAcesso = 0,
                    ImpedirAcesso = false,
                    Nome = Username,
                };
            }

            if (Usuario != null && !string.IsNullOrWhiteSpace(Username) && !Usuario.Nome.Equals(Username, StringComparison.InvariantCultureIgnoreCase))
            {
                Usuario.Nome = Username;
                //Usuario.DataAtivacao = null;
            }

            if (Usuario != null && !string.IsNullOrWhiteSpace(Senha))
                Usuario.Senha = Senha;

            return this;
        }

        private bool HasPermissao(string cd, UnidadeHabitacional ua)
        {
            if (Usuario == null)
                return false;

            return Usuario.Permissoes.Any(w => w.Condominio.Codigo == ua.Edificio.Condominio.Codigo && w.Permissao.Codigo.Equals(cd, StringComparison.InvariantCultureIgnoreCase));
        }

        private void SalvarPermissao(Condominio condominio, UnidadeHabitacional unidade, string categoria, List<string> permissoes)
        {
            var grupo = "";
            switch (categoria)
            {
                case "M":
                    grupo = "MORADOR";
                    break;
                case "R":
                    grupo = "MORADOR_RESPONSAVEL";
                    break;
                case "F":
                    grupo = "FUNCIONARIO";
                    break;
                case "V":
                    grupo = "VISITANTE";
                    break;
                case "P":
                    grupo = "PORTEIRO";
                    break;
                case "S":
                    grupo = "SINDICO";
                    break;
            }

            if (Usuario != null)
            {
                if (permissoes != null && unidade != null)
                {
                    var pm = Usuario.Permissoes.Where(w => w.Condominio.Codigo.Equals(unidade.Edificio.Condominio.Codigo) && !permissoes.Any(e => e.Equals(w.Permissao.Codigo, StringComparison.InvariantCultureIgnoreCase))).ToList();
                    foreach (var m in pm)
                    {
                        m.Delete();
                        Usuario.Permissoes.Remove(m);
                    }

                    foreach (var code in permissoes.Where(w => !HasPermissao(w, unidade)).ToList())
                        Usuario.Permissoes.Add(new UsuarioPermissao() { UnidadeHabitacional = unidade, Condominio = unidade.Edificio.Condominio, Usuario = Usuario, Permissao = new Permissao().Repository.Get(code) });
                }

                var grupos = Usuario.UsuarioCondominios.Where(w => w.Condominio.Codigo == condominio.Codigo && ((unidade == null && w.Unidade == null) || (w.Unidade != null && unidade != null && w.Unidade.Codigo == unidade.Codigo)) && !w.Grupo.Codigo.Equals(grupo, StringComparison.InvariantCultureIgnoreCase)).ToList();
                foreach (var p in grupos)
                {
                    p.Delete();
                    Usuario.UsuarioCondominios.Remove(p);
                }

                if (!Usuario.UsuarioCondominios.Any(w => w.Condominio.Codigo == condominio.Codigo && ((unidade == null && w.Unidade == null) || (w.Unidade != null && unidade != null && w.Unidade.Codigo == unidade.Codigo)) && w.Grupo.Codigo.Equals(grupo, StringComparison.InvariantCultureIgnoreCase)))
                    Usuario.UsuarioCondominios.Add(new UsuarioCondominio() { Condominio = condominio, Unidade = unidade, Grupo = new Grupo().Repository.Get(grupo), Usuario = Usuario });
            }
            else if (!string.IsNullOrWhiteSpace(Username))
            {
                var convite = Convites.FirstOrDefault(w => w.Condominio.Codigo == condominio.Codigo && ((unidade == null && w.Unidade == null) || (w.Unidade != null && unidade != null && w.Unidade.Codigo == unidade.Codigo)));

                if (convite == null)
                {
                    convite = new PessoaConvite()
                    {
                        Condominio = condominio,
                        Email = Username,
                        Pessoa = this,
                        Status = 0,
                        Unidade = unidade,
                        Envio = 1,
                        Token = "API Antiga",
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };

                    this.Convites.Add(convite);
                }

                convite.Grupo = grupo;

                foreach (var c in Convites)
                {
                    if (!c.Email.Equals(Username, StringComparison.InvariantCultureIgnoreCase))
                    {
                        c.Email = Username;
                        c.UpdatedAt = DateTime.Now;
                    }
                }

                if (permissoes != null)
                {
                    convite.Permissao = "";
                    foreach (var m in permissoes)
                    {
                        var permissao = new Permissao().Repository.Get(m);
                        if (string.IsNullOrWhiteSpace(convite.Permissao))
                            convite.Permissao = permissao.Codigo.ToString();
                        else
                            convite.Permissao += "|" + permissao.Codigo.ToString();
                    }
                }
            }
        }

        private Pessoa AdicionarUnidadesHabitacionais(ICollection<DTOPessoaUnidadeHabitacional> novasUnidades)
        {
            if (novasUnidades == null || novasUnidades.Count == 0)
                return this;

            UnidadesAdicionadas = new List<int>();

            foreach (var unidade in novasUnidades)
            {
                var u = new UnidadeHabitacional().Get(unidade.CodigoUnidadeHabitacional);

                var pessoaUnidadeHabitacional = _unidadesHabitacional.FirstOrDefault(t => t.UnidadeHabitacional != null && t.Categoria == "M" &&
                        (t.UnidadeHabitacional.Codigo == unidade.CodigoUnidadeHabitacional || t.Codigo == unidade.Codigo));

                var funcionario = _pessoaCondominios.FirstOrDefault(t => t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == unidade.CodigoUnidadeHabitacional || t.Codigo == unidade.Codigo));

                var visitante = _visitantes.FirstOrDefault(t => t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == unidade.CodigoUnidadeHabitacional || t.Codigo == unidade.Codigo));

                bool enviarConvite = true;

                #region Morador
                if (unidade.Categoria.Equals("M"))
                {
                    if (funcionario != null)
                    {
                        enviarConvite = false;
                        funcionario.Delete();
                        _pessoaCondominios.Remove(funcionario);
                    }
                    if (visitante != null)
                    {
                        enviarConvite = false;
                        visitante.Delete();
                        _visitantes.Remove(visitante);
                    }

                    if (pessoaUnidadeHabitacional == null)
                    {
                        pessoaUnidadeHabitacional = new PessoaUnidadeHabitacional
                        {
                            UnidadeHabitacional = u,
                            Pessoa = this,
                            CreatedAt = DateTime.UtcNow,
                            UpdatedAt = DateTime.UtcNow,
                            Status = enviarConvite ? (byte)0 : (byte)1,
                            DataAtivacao = enviarConvite ? (DateTime?)null : DateTime.Now
                        };

                        UnidadesHabitacional.Add(pessoaUnidadeHabitacional);
                        if (enviarConvite) UnidadesAdicionadas.Add(unidade.CodigoUnidadeHabitacional);
                    }

                    pessoaUnidadeHabitacional.Administrador = unidade.Administrador;
                    pessoaUnidadeHabitacional.Categoria = unidade.Categoria;
                    pessoaUnidadeHabitacional.RelacaoFuncao = unidade.RelacaoFuncao;
                    pessoaUnidadeHabitacional.Responsavel = Convert.ToBoolean(unidade.Responsavel);
                    pessoaUnidadeHabitacional.UpdatedAt = DateTime.Now;
                }
                #endregion

                #region Funcionario
                if (unidade.Categoria.Equals("F"))
                {
                    if (pessoaUnidadeHabitacional != null)
                    {
                        enviarConvite = false;
                        pessoaUnidadeHabitacional.Delete();
                        _unidadesHabitacional.Remove(pessoaUnidadeHabitacional);
                    }
                    if (visitante != null)
                    {
                        enviarConvite = false;
                        visitante.Delete();
                        _visitantes.Remove(visitante);
                    }

                    if (funcionario == null)
                    {
                        funcionario = new PessoaCondominio
                        {
                            UnidadeHabitacional = u,
                            Condominio = u.Edificio.Condominio,
                            Pessoa = this,
                            CreatedAt = DateTime.UtcNow,
                            Status = enviarConvite ? (byte)0 : (byte)1,
                            DataAtivacao = enviarConvite ? (DateTime?)null : DateTime.Now
                        };

                        PessoaCondominios.Add(funcionario);
                        if (enviarConvite) UnidadesAdicionadas.Add(unidade.CodigoUnidadeHabitacional);
                    }

                    funcionario.Categoria = unidade.Categoria;
                    funcionario.Funcao = unidade.RelacaoFuncao;
                    funcionario.UpdatedAt = DateTime.UtcNow;
                }
                #endregion

                #region Visitante
                if (unidade.Categoria.Equals("V"))
                {
                    if (pessoaUnidadeHabitacional != null)
                    {
                        enviarConvite = false;
                        pessoaUnidadeHabitacional.Delete();
                        _unidadesHabitacional.Remove(pessoaUnidadeHabitacional);
                    }
                    if (funcionario != null)
                    {
                        enviarConvite = false;
                        funcionario.Delete();
                        _pessoaCondominios.Remove(funcionario);
                    }

                    if (visitante == null)
                    {
                        visitante = new Visitante
                        {
                            UnidadeHabitacional = u,
                            Condominio = u.Edificio.Condominio,
                            Pessoa = this,
                            CreatedAt = DateTime.UtcNow
                        };

                        Visitantes.Add(visitante);
                        if (enviarConvite) UnidadesAdicionadas.Add(unidade.CodigoUnidadeHabitacional);
                    }

                    visitante.Relacao = unidade.RelacaoFuncao;
                    visitante.UpdatedAt = DateTime.UtcNow;
                }
                #endregion

                var prms = new List<string>();
                if (unidade.PermissaoAvisos != 0)
                    prms.Add(unidade.PermissaoAvisos == 2 ? Constantes.Corona.Usuario.Permissao.TOTAL_AVISOS : Constantes.Corona.Usuario.Permissao.VISUALIZAR_AVISOS);
                if (unidade.PermissaoPessoas != 0)
                    prms.Add(unidade.PermissaoPessoas == 2 ? Constantes.Corona.Usuario.Permissao.TOTAL_PESSOAS : Constantes.Corona.Usuario.Permissao.VISUALIZAR_PESSOAS);
                if (unidade.PermissaoVeiculos != 0)
                    prms.Add(unidade.PermissaoVeiculos == 2 ? Constantes.Corona.Usuario.Permissao.TOTAL_VEICULOS : Constantes.Corona.Usuario.Permissao.VISUALIZAR_VEICULOS);
                if (unidade.PermissaoReserva != 0)
                    prms.Add(unidade.PermissaoReserva == 2 ? Constantes.Corona.Usuario.Permissao.TOTAL_RESERVAS : Constantes.Corona.Usuario.Permissao.VISUALIZAR_RESERVAS);
                if (unidade.PermissaoChegadaSegura != 0)
                    prms.Add(unidade.PermissaoChegadaSegura == 1 ? Constantes.Corona.Usuario.Permissao.TOTAL_CHEGADASEGURA : Constantes.Corona.Usuario.Permissao.VISUALIZAR_CHEGADASEGURA);
                if (unidade.PermissaoNotificacaoPortaria != 0)
                    prms.Add(unidade.PermissaoNotificacaoPortaria == 1 ? Constantes.Corona.Usuario.Permissao.TOTAL_NOTIFICACAO : Constantes.Corona.Usuario.Permissao.VISUALIZAR_NOTIFICACAO);
                if (unidade.PermissaoClassificados != 0)
                    prms.Add(unidade.PermissaoClassificados == 2 ? Constantes.Corona.Usuario.Permissao.TOTAL_CLASSIFICADOS : Constantes.Corona.Usuario.Permissao.VISUALIZAR_CLASSIFICADOS);

                var ctg = unidade.Categoria;
                if (ctg.Equals("M") && unidade.Responsavel == 1)
                    ctg = "R";

                SalvarPermissao(u.Edificio.Condominio, u, ctg, prms);
            }

            return this;
        }

        private Pessoa AdicionarCondominios(ICollection<DTOPessoaCondominio> novosCondominios)
        {
            if (novosCondominios == null || novosCondominios.Count == 0)
                return this;

            CondominiosAdicionados = new List<int>();

            foreach (var condominio in novosCondominios)
            {
                var pessoaCondominio =
                    PessoaCondominios.FirstOrDefault(
                        t =>
                        t.Condominio != null &&
                        (t.Condominio.Codigo == condominio.CodigoCondominio || t.Codigo == condominio.Codigo));

                if (pessoaCondominio == null)
                {
                    pessoaCondominio = new PessoaCondominio
                    {
                        Condominio = new Condominio().Get(condominio.CodigoCondominio),
                        Pessoa = this,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        Status = 0
                    };

                    PessoaCondominios.Add(pessoaCondominio);
                    CondominiosAdicionados.Add(condominio.CodigoCondominio);
                }

                pessoaCondominio.Categoria = condominio.Categoria;
                //pessoaCondominio.Funcao = condominio.Categoria.Equals("S", StringComparison.InvariantCultureIgnoreCase) ? "Sindico" : "Porteiro";
                pessoaCondominio.Funcao = condominio.Funcao;

                SalvarPermissao(pessoaCondominio.Condominio, null, condominio.Categoria, null);

                //Valida se o usuario esta sendo cadastrado como funcionario normal em um condominio em que ele ja seja sindico
                if (pessoaCondominio.IsSindico() == false && PessoaCondominios.Where(e => e.IsSindico() && e.Condominio.Codigo == pessoaCondominio.Condominio.Codigo).Count() > 0)
                    throw new UsuarioJaCadastradoException(Username);
            }

            return this;
        }

        private Pessoa AdicionarAcessos(ICollection<DTOPessoaAcesso> acessos)
        {
            if (acessos == null || acessos.Count == 0)
                return this;

            // Percorre os itens de acesso
            foreach (var acesso in acessos)
            {
                var unidade = new UnidadeHabitacional().Get(acesso.CodigoUnidadeHabitacional);
                var condominio = unidade.Edificio.Condominio;

                // Localiza o item a ser inclu�do/editado
                var pessoaAcesso = Acessos.Where(t => t.Condominio.Codigo == condominio.Codigo && t.UnidadeHabitacional.Codigo == unidade.Codigo).ToList();

                var acessoSemana = new[] { 
                    new { DiaSemana = DayOfWeek.Sunday, Horarios = string.IsNullOrWhiteSpace(acesso.Domingo) ? new char[]{} : acesso.Domingo.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Monday, Horarios = string.IsNullOrWhiteSpace(acesso.Segunda) ? new char[]{} : acesso.Segunda.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Tuesday, Horarios = string.IsNullOrWhiteSpace(acesso.Terca) ? new char[]{} : acesso.Terca.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Wednesday, Horarios = string.IsNullOrWhiteSpace(acesso.Quarta) ? new char[]{} : acesso.Quarta.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Thursday, Horarios = string.IsNullOrWhiteSpace(acesso.Quinta) ? new char[]{} : acesso.Quinta.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Friday, Horarios = string.IsNullOrWhiteSpace(acesso.Sexta) ? new char[]{} : acesso.Sexta.ToArray() }, 
                    new { DiaSemana = DayOfWeek.Saturday, Horarios = string.IsNullOrWhiteSpace(acesso.Sabado) ? new char[]{} : acesso.Sabado.ToArray() }, 
                };

                var horarioManha = new { Inicio = new TimeSpan(6, 0, 0), Fim = new TimeSpan(11, 59, 59) };
                var horarioTarde = new { Inicio = new TimeSpan(12, 0, 0), Fim = new TimeSpan(17, 59, 59) };
                var horarioNoite = new { Inicio = new TimeSpan(18, 0, 0), Fim = new TimeSpan(23, 59, 59) };

                foreach (var semana in acessoSemana)
                {
                    var acessoPessoaSemana = pessoaAcesso.Where(w => w.DiaSemana == (byte)semana.DiaSemana);

                    var acessoHorario = acessoPessoaSemana.Where(a => a.HoraInicio >= horarioManha.Inicio && a.HoraFim <= horarioManha.Fim).ToList();
                    if (semana.Horarios.Contains('M'))
                    {
                        if (acessoHorario.Count <= 0)
                            Acessos.Add(new AcessoPessoa() { DiaSemana = (byte)semana.DiaSemana, HoraInicio = horarioManha.Inicio, HoraFim = horarioManha.Fim, Condominio = condominio, Pessoa = this, UnidadeHabitacional = unidade, Status = 1, Tipo = 0, CreatedAt = DateTime.UtcNow, UpdatedAt = DateTime.UtcNow });
                    }
                    else
                    {
                        acessoHorario.Each((AcessoPessoa a) => { a.Delete(); Acessos.Remove(a); });
                    }

                    acessoHorario = acessoPessoaSemana.Where(a => a.HoraInicio >= horarioTarde.Inicio && a.HoraFim <= horarioTarde.Fim).ToList();
                    if (semana.Horarios.Contains('T'))
                    {
                        if (acessoHorario.Count <= 0)
                            Acessos.Add(new AcessoPessoa() { DiaSemana = (byte)semana.DiaSemana, HoraInicio = horarioTarde.Inicio, HoraFim = horarioTarde.Fim, Condominio = condominio, Pessoa = this, UnidadeHabitacional = unidade, Status = 1, Tipo = 0, CreatedAt = DateTime.UtcNow, UpdatedAt = DateTime.UtcNow });
                    }
                    else
                    {
                        acessoHorario.Each((AcessoPessoa a) => { a.Delete(); Acessos.Remove(a); });
                    }

                    acessoHorario = acessoPessoaSemana.Where(a => a.HoraInicio >= horarioNoite.Inicio && a.HoraFim <= horarioNoite.Fim).ToList();
                    if (semana.Horarios.Contains('N'))
                    {
                        if (acessoHorario.Count <= 0)
                            Acessos.Add(new AcessoPessoa() { DiaSemana = (byte)semana.DiaSemana, HoraInicio = horarioNoite.Inicio, HoraFim = horarioNoite.Fim, Condominio = condominio, Pessoa = this, UnidadeHabitacional = unidade, Status = 1, Tipo = 0, CreatedAt = DateTime.UtcNow, UpdatedAt = DateTime.UtcNow });
                    }
                    else
                    {
                        acessoHorario.Each((AcessoPessoa a) => { a.Delete(); Acessos.Remove(a); });
                    }
                }
            }

            // Retorna o pr�prio objeto
            return this;
        }

        private Pessoa SalvarFoto()
        {
            var oldUrl = FotoAnexo != null ? FotoAnexo.Url : string.Empty;
            string retornoPath = null;
            FotoAlterada = false;

            try
            {
                if (Foto != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(Foto),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, extension, fileHelper.File);

                        if (FotoAnexo == null)
                            FotoAnexo = new Anexo();

                        FotoAnexo.CreatedAt = DateTime.Now;
                        FotoAnexo.UpdatedAt = DateTime.Now;
                        FotoAnexo.Extensao = extension;
                        FotoAnexo.Nome = Path.GetFileName(retorno.Path);
                        FotoAnexo.Status = (byte)AnnexStatus.Active;
                        FotoAnexo.Tamanho = fileHelper.File.Length;
                        FotoAnexo.Url = retorno.Path;
                        FotoAnexo.Entidade = (byte)AnnexEntity.Picture;
                        FotoAnexo.Tipo = (byte)AnnexType.Image;

                        retornoPath = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldUrl);

                    FotoAlterada = true;
                }
            }
            catch (Exception exception)
            {
                if (!string.IsNullOrWhiteSpace(retornoPath))
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, retornoPath);

                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }

            return this;
        }

        private Pessoa EnviarConviteAtivacao()
        {
            if (string.IsNullOrWhiteSpace(Username) ||
                (
                    (Usuario != null && Usuario.DataAtivacao.HasValue) &&
                    (CondominiosAdicionados == null || CondominiosAdicionados.Count <= 0) &&
                    (UnidadesAdicionadas == null || UnidadesAdicionadas.Count <= 0))
                )
                return this;

            var corporativo = Repository.Corporativo.HasValue ? Repository.Corporativo.Value : 1;

            List<String> prms = new List<string>();
            prms.Add(CryptographyType.User.ConvertToString());
            prms.Add(DateTime.Now.ToString("o"));
            prms.Add(Codigo.ToString());
            prms.Add(Username);
            prms.Add("1.0");
            prms.Add(((int)Common.Tipos.Aplicacao.Corona).ToString(CultureInfo.InvariantCulture));
            prms.Add(corporativo.ToString(CultureInfo.InvariantCulture));

            var chaveConvite = SecurityHelper.CreateCryptographyKey(prms.ToArray());

            var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LINK_EMAIL_ATIVACAO, corporativo) + chaveConvite;
            var assuntoConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_EMAIL_ATIVACAO, corporativo);
            var templateConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_EMAIL_ATIVACAO, corporativo);
            var urlAppleStore = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.ItunesUrl, corporativo);
            var urlGooglePlay = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.GooglePlayUrl, corporativo);

            var condominio = PessoaCondominios.Where(w => w.DataAtivacao.HasValue == false).Select(w => w.Condominio).FirstOrDefault();
            if (condominio == null)
                condominio = UnidadesHabitacional1.Where(w => w.DataAtivacao.HasValue == false).Select(w => w.UnidadeHabitacional.Edificio.Condominio).FirstOrDefault();

            string local = "";
            if (condominio != null)
                local = string.Format(", no " + condominio.Nome + ",{0}{1}",
                    string.IsNullOrWhiteSpace(condominio.Cidade) ? "" : " " + condominio.Cidade + ",",
                    string.IsNullOrWhiteSpace(condominio.Estado) ? "" : " " + condominio.Cidade + ","
                    );

            EmailHelper.EnviarEmail(GetEmailSettings(Username, corporativo), assuntoConvite, string.Format(templateConvite, Nome, linkConvite, urlAppleStore, urlGooglePlay, local));

            return this;
        }

        public virtual void Save(IList<DTOPessoaUnidadeHabitacional> unidadeHabitacionals,
                                 IList<DTOPessoaCondominio> pessoaCondominios,
                                 IList<DTOPessoaAcesso> acessos)
        {
            try
            {
                AdicionarUsuario(pessoaCondominios)
                .AdicionarContatos()
                .AdicionarUnidadesHabitacionais(unidadeHabitacionals)
                .AdicionarCondominios(pessoaCondominios)
                .AdicionarAcessos(acessos)
                .SalvarFoto();

                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;

                base.Save(true);
                
                EnviarConviteAtivacao();
            }
            catch (BaseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }
        }

        public virtual void Validar(string usuario, string senha)
        {
            var pessoa = Repository.Get(usuario);
            if (pessoa == null)
                throw new UsuarioOuSenhaInvalidosException();

            var user = pessoa.Usuario;

            if (!SecurityHelper.isValidSHA1(senha))
                senha = SecurityHelper.EncryptToSHA1(senha);

            if (!user.Senha.Equals(senha, StringComparison.InvariantCultureIgnoreCase))
            {
                var geminiuser = user.UsuarioGemini.FirstOrDefault();
                if (geminiuser == null)
                    throw new UsuarioOuSenhaInvalidosException();

                senha = SecurityHelper.EncryptToSHA1(geminiuser.SegredoSenha + senha);
                if (!user.Senha.Equals(senha, StringComparison.InvariantCultureIgnoreCase))
                    throw new UsuarioOuSenhaInvalidosException();
            }

            if (!user.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(usuario);

            Repository.Evict(pessoa);

            Load(pessoa.Codigo);
        }

        public virtual void RecoveryPassword(string username)
        {
            if (Aplicacao == null)
                Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString());

            var pessoa = Repository.Get(username);

            if (pessoa == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (pessoa.Usuario == null || !pessoa.Usuario.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(username);

            var corporativo = Repository.Corporativo.HasValue ? Repository.Corporativo.Value : 1;

            List<String> prms = new List<string>();
            prms.Add(CryptographyType.User.ConvertToString());
            prms.Add(DateTime.Now.ToBrasiliaTime().ToString("o"));
            prms.Add(pessoa.Codigo.ToString());
            prms.Add(pessoa.Username);
            prms.Add(pessoa.Aplicacao.Versao);
            prms.Add(((int)Common.Tipos.Aplicacao.Corona).ToString(CultureInfo.InvariantCulture));
            prms.Add(corporativo.ToString(CultureInfo.InvariantCulture));

            var chaveConvite = SecurityHelper.CreateCryptographyKey(prms.ToArray());
            var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LINK_EMAIL_RECUPERAR_SENHA, corporativo) + chaveConvite;
            var assuntoRecuperacaoSenha = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_EMAIL_RECUPERAR_SENHA, corporativo);
            var templateRecuperacaoSenha = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_EMAIL_RECUPERAR_SENHA, corporativo);

            EmailHelper.EnviarEmail(GetEmailSettings(username, corporativo), assuntoRecuperacaoSenha, string.Format(templateRecuperacaoSenha, pessoa.Nome, linkConvite));
        }

        public virtual Condominio GetCondominio(int condominio)
        {
            if (Usuario == null)
                return null;

            var pessoaCondominio = this.PessoaCondominios.FirstOrDefault(t => t.Condominio.Codigo == condominio);

            if (pessoaCondominio == null)
                throw new ItemNotFoundException(condominio, "Condominio");

            return pessoaCondominio.Condominio;
        }

        public virtual bool IsSindico(int unidadeHabitacional)
        {
            if (Usuario == null)
                return false;

            var u = new UnidadeHabitacional().Get(unidadeHabitacional);

            var codigoCondominio = u.Edificio.Condominio.Codigo;

            return Usuario.UsuarioCondominios.Any(t => t.Condominio.Codigo == codigoCondominio && t.IsSindico());
        }

        public virtual DTOGetPush ListarNotificacao(int codigoCondominio)
        {
            // Condom�nio
            var condominio = new Condominio().Get(codigoCondominio);
            IEnumerable<UnidadeHabitacional> unidadesHabitacionais = null;

            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.Now.ToBrasiliaTime().Subtract(this.DurationPush), DateTime.MaxValue).OrderByDescending(w => w.DataNotificacao);

            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao notificacao in notificacoes)
            {
                // Define o formato da notifica��o
                switch (GetFormatoNotificacao((NotificationType2)notificacao.TipoMensagem))
                {
                    case FormatoNotificacaoEnum.Chegada:
                        {
                            var unidades = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);
                            var veiculos = unidades.SelectMany(x => x.Veiculos);
                            if (unidades.Any(e => notificacao.Origem.HasValue && e.Codigo == notificacao.Origem.Value &&
                                (((NotificationType2)notificacao.TipoMensagem) == NotificationType2.FootSafeArrival || 
                                ((NotificationType2)notificacao.TipoMensagem) == NotificationType2.TaxiSafeArrival)
                                ) ||
                                veiculos.Any(e => notificacao.Origem.HasValue && e.Codigo == notificacao.Origem.Value && 
                                    ((NotificationType2)notificacao.TipoMensagem) == NotificationType2.SafeArrival))
                            {
                                var chegada = GetNotificacaoChegadaArgs(notificacao);
                                result.Chegada.Add(chegada);
                            }

                            break;
                        }
                    case FormatoNotificacaoEnum.Portaria:
                        {
                            // Lazzy Pattern
                            if (unidadesHabitacionais == null)
                                unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                            // Filtro as Unidades Habitacionais do Condom�nio
                            if (unidadesHabitacionais.Any(e => notificacao.Origem.HasValue && e.Codigo == notificacao.Origem.Value))
                            {
                                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                                result.Portaria.Add(portaria);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Sindico:
                        {
                            if ((NotificationType1)notificacao.TipoMensagem == NotificationType1.SindicoPorMorador)
                            {
                                // Lazzy Pattern
                                if (unidadesHabitacionais == null)
                                    unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                                // Filtro as Unidades Habitacionais do Condom�nio
                                if (unidadesHabitacionais.Any(e => notificacao.Origem.HasValue && e.Codigo == notificacao.Origem.Value))
                                {
                                    var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            else
                            {
                                // Filtro as notifica��es do Condom�nio
                                if (notificacao.Origem.HasValue && notificacao.Origem.Value == condominio.Codigo)
                                {
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Resposta:
                        {
                            // Ser� retornado no GetReplyPush (filtro por usu�rio)
                            break;
                        }
                }
            }
            return result;
        }

        public virtual DTOGetPush ListarNotificacaoResposta()
        {
            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.UtcNow.Subtract(this.DurationPush), DateTime.MaxValue);

            // Percorre as notifica��o de Resposta (Reply)
            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao reply in notificacoes.Where(e => e.Origem.HasValue && (NotificationType1)e.TipoMensagem == NotificationType1.Answer))
            {
                var origem = new Notificacao().Get(reply.Origem.Value);

                // Filtro as notifica��es do Usu�rio
                if (origem.Remetente.Codigo == this.Codigo)
                {
                    var notificacaoResposta = GetNotificacaoRespostaArgs(origem, reply);
                    result.Resposta.Add(notificacaoResposta);
                }
            }

            return result;
        }

        public virtual DTOGetPush ListarNotificacao()
        {
            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRangeByRemetente(Codigo, DateTime.Now.ToBrasiliaTime().Subtract(this.DurationPush), DateTime.MaxValue).OrderByDescending(w => w.DataNotificacao);

            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao notificacao in notificacoes.Where(w => w.Origem.HasValue))
            {
                // Define o formato da notifica��o
                switch (GetFormatoNotificacao((NotificationType2)notificacao.TipoMensagem))
                {
                    case FormatoNotificacaoEnum.Chegada:
                        {
                            var chegada = GetNotificacaoChegadaArgs(notificacao);
                            result.Chegada.Add(chegada);

                            break;
                        }
                    case FormatoNotificacaoEnum.Portaria:
                        {
                            var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                            var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                            result.Portaria.Add(portaria);

                            break;
                        }
                    case FormatoNotificacaoEnum.Sindico:
                        {
                            if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.SindicoPorMorador)
                            {
                                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                                result.Sindico.Add(notificacaoSindico);
                            }
                            else
                            {
                                var condominio = new Condominio().Get(notificacao.Origem.Value);
                                var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                                result.Sindico.Add(notificacaoSindico);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Resposta:
                        {
                            // Ser� retornado no GetReplyPush (filtro por usu�rio)
                            break;
                        }
                }
            }
            return result;
        }

        #region Notificar

        public virtual Notificacao Notificar(DTOSendPush sendPush)
        {
            switch (GetFormatoNotificacao(TypeConverter.Convert(sendPush.TipoMensagem)))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        return this.NotificarChegada(sendPush);
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        return this.NotificarPortaria(sendPush);
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        return this.NotificarSindico(sendPush);
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        return this.NotificarResposta(sendPush);
                    }
                default:
                    return null;
            }
        }

        private enum FormatoNotificacaoEnum { Chegada, Portaria, Sindico, Resposta, Evento }
        private FormatoNotificacaoEnum GetFormatoNotificacao(NotificationType2 tipoNotificacao)
        {
            switch (tipoNotificacao)
            {
                case NotificationType2.Undefined:
                    throw new TipoMensagemInvalidaException();
                case NotificationType2.SafeArrival:
                case NotificationType2.TaxiSafeArrival:
                case NotificationType2.FootSafeArrival:
                    return FormatoNotificacaoEnum.Chegada;
                case NotificationType2.Service:
                case NotificationType2.Visit:
                case NotificationType2.Delivery:
                case NotificationType2.Other:
                    return FormatoNotificacaoEnum.Portaria;
                case NotificationType2.SindicoPorFuncionario:
                case NotificationType2.SindicoPorMorador:
                    return FormatoNotificacaoEnum.Sindico;
                case NotificationType2.Answer:
                    return FormatoNotificacaoEnum.Resposta;
                case NotificationType2.EventGuestArrival:
                    return FormatoNotificacaoEnum.Evento;
                default:
                    throw new NotImplementedException();
            }
        }

        private Device GetDevice(string deviceToken)
        {
            if (!String.IsNullOrEmpty(deviceToken))
            {
                Device device = new Device().Get(deviceToken);
                return device;
            }
            else
                return null;
        }

        private Notificacao NotificarChegada(DTOSendPush sendPush)
        {
            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Mensagem,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = (byte)NotificationStatus.New,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = (byte)TypeConverter.Convert(sendPush.TipoMensagem),
                DataNotificacao = DateTime.Now.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os devices do Condom�nio
            IEnumerable<Device> devices = null;
            Veiculo veiculo = null;
            if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.SafeArrival)
            {
                veiculo = new Veiculo().Get(sendPush.Origem.Value);
                devices = veiculo.UnidadeHabitacional.Edificio.Condominio.Devices.Where(e => e.Pessoa == null && e.Status == (byte)DeviceStatus.Active);
            }
            else if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.FootSafeArrival || (NotificationType2)notificacao.TipoMensagem == NotificationType2.TaxiSafeArrival)
            {
                var uh = new UnidadeHabitacional().Get(sendPush.Origem.Value);
                devices = uh.Edificio.Condominio.Devices.Where(e => e.Pessoa == null && e.Status == (byte)DeviceStatus.Active);
            }

            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "bell-ringing.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSegura,
                    LocalizedArgs = GetNotificacaoChegadaArgs(notificacao).ToList(),
                    PlatformType = (PlatformType)device.Plataforma
                };

                if (device.APIVersion.HasValue && device.APIVersion.Value >= 1f)
                    pushMessage.LocalizedKey = (NotificationType2)notificacao.TipoMensagem == NotificationType2.SafeArrival ? 
                        Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSegura : ((NotificationType2)notificacao.TipoMensagem == NotificationType2.FootSafeArrival) ? 
                        Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraPe : Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraTaxi;

                try
                {
                    NotificationManager.SendPushMessage(pushMessage);

                    if (device.APIVersion.HasValue && device.APIVersion.Value >= 2f)
                    {
                        Timer t = new Timer(ReenviarChegadaSegura, pushMessage, new TimeSpan(0, 1, 0), new TimeSpan(0, 1, 0));
                        ChegadaSeguraTimers.Add(notificacao.Codigo.ToString() + "|" + device.Token, t);
                    }
                }
                catch (Exception)
                {
                }
            }

            return notificacao;
        }

        private DTOGetPushChegadaArgs GetNotificacaoChegadaArgs(Notificacao notificacao)
        {
            DTOGetPushChegadaArgs result = new DTOGetPushChegadaArgs()
            {
                Codigo = notificacao.Codigo,
                DataHoraNotificacao = notificacao.DataNotificacao,
                TipoNotificacao = (NotificationType1)TypeConverter.Convert((NotificationType2)notificacao.TipoMensagem),
                Mensagem = notificacao.Mensagem,
                StatusNotificacao = (NotificationStatus)notificacao.Status,
                Origem = notificacao.Origem,
            };

            if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.SafeArrival)
            {
                Veiculo veiculo = new Veiculo().Get(notificacao.Origem.Value);
                result.EdificioNome = veiculo.UnidadeHabitacional.Edificio.Nome;
                result.UnidadeHabitacionalNumero = veiculo.UnidadeHabitacional.Numero;
                result.VeiculoMarca = veiculo.Marca;
                result.VeiculoModelo = veiculo.Modelo;
                result.VeiculoPlaca = veiculo.Placa;
                result.VeiculoCor = veiculo.Cor;
            }
            else
            {
                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                result.EdificioNome = unidadeHabilitacional.Edificio.Nome;
                result.UnidadeHabitacionalNumero = unidadeHabilitacional.Numero;
            }

            return result;
        }

        private Notificacao NotificarPortaria(DTOSendPush sendPush)
        {
            var unidadeHabitacional =
                UnidadesHabitacional.FirstOrDefault(t => t.UnidadeHabitacional.Codigo == sendPush.Origem);

            if (unidadeHabitacional == null)
                throw new ItemNotFoundException(sendPush.Origem, "UnidadeHabitacional");

            var condominio = unidadeHabitacional.UnidadeHabitacional.Edificio.Condominio;

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = (byte)NotificationStatus.Confirmed,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = (byte)TypeConverter.Convert(sendPush.TipoMensagem),
                DataNotificacao = DateTime.Now.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os devices do Condom�nio
            var devices = condominio.Devices.Where(e => e.Pessoa == null && e.Status == (byte)DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "doorbell.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.NotificarPortaria,
                    LocalizedArgs = GetNotificacaoPortariaArgs(unidadeHabitacional.UnidadeHabitacional, notificacao).ToList(),
                    PlatformType = (PlatformType)device.Plataforma
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushPortariaArgs GetNotificacaoPortariaArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            DTOGetPushPortariaArgs result = new DTOGetPushPortariaArgs()
            {
                Codigo = notificacao.Codigo,
                UsuarioNome = notificacao.Remetente.Nome,
                EdificioNome = unidadeHabitacional.Edificio.Nome,
                UnidadeHabitacionalNumero = unidadeHabitacional.Numero,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = (NotificationType1)TypeConverter.Convert((NotificationType2)notificacao.TipoMensagem),
                DataHoraNotificacao = notificacao.DataNotificacao,
                StatusNotificacao = (NotificationStatus)notificacao.Status
            };
            return result;
        }

        private Notificacao NotificarSindico(DTOSendPush sendPush)
        {
            // Notifica��o do S�ndico oriundo Morador ou Funcion�rio
            Condominio condominio = null;
            UnidadeHabitacional unidadeHabitacional = null;
            if (sendPush.TipoMensagem == NotificationType1.SindicoPorMorador)
            {
                unidadeHabitacional = new UnidadeHabitacional().Get(sendPush.Origem.Value);
                if (unidadeHabitacional == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Unidade Habitacional");
                condominio = unidadeHabitacional.Edificio.Condominio;
            }
            else
            {
                condominio = new Condominio().Get(sendPush.Origem.Value);
                if (condominio == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Condom�nio");
            }

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = (byte)NotificationStatus.Confirmed,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = (byte)TypeConverter.Convert(sendPush.TipoMensagem),
                DataNotificacao = DateTime.Now.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os Devices dos S�ndicos
            var sindicos = condominio.Funcionarios.Where(o => o.IsSindico());
            var devices = sindicos.SelectMany(x => x.Pessoa.Devices).Where(e => e.Status == (byte)DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "bell-ringing.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.NotificarSindico,
                    LocalizedArgs = unidadeHabitacional != null ?
                                    GetNotificacaoSindicoArgs(unidadeHabitacional, notificacao).ToList() :
                                    GetNotificacaoSindicoArgs(condominio, notificacao).ToList(),
                    PlatformType = (PlatformType)device.Plataforma
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(unidadeHabitacional.Codigo, unidadeHabitacional.Numero, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(Condominio condominio, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(condominio.Codigo, condominio.Nome, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(int codigo, string descricao, Notificacao notificacao)
        {
            DTOGetPushSindicoArgs result = new DTOGetPushSindicoArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = codigo,
                OrigemDescricao = descricao,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = (NotificationType1)TypeConverter.Convert((NotificationType2)notificacao.TipoMensagem),
                DataHoraNotificacao = notificacao.DataNotificacao
            };
            return result;
        }

        private Notificacao NotificarResposta(DTOSendPush sendPush)
        {
            var origem = new Notificacao().Get(sendPush.Origem.Value);
            if (origem == null)
                throw new ItemNotFoundException(sendPush.Origem, "Notifica��o");

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = (byte)NotificationStatus.Confirmed,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = (byte)TypeConverter.Convert(sendPush.TipoMensagem),
                DataNotificacao = DateTime.Now.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Altera o Status da Notifica��o de Origem
            origem.Status = (byte)NotificationStatus.Answered;
            origem.Save();

            // Envia para o Device da Notifica��o ou Devices do Remetente
            IEnumerable<Device> devices = origem.RemetenteDevice != null && origem.RemetenteDevice.Status == (byte)DeviceStatus.Active ?
                                          new List<Device>() { origem.RemetenteDevice } :
                                          origem.Remetente.Devices.Where(e => e.Status == (byte)DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "double-bell.aiff",
                    DeviceToken = device.Token,
                    Message = device.Plataforma == (byte)PlatformType.Android ? sendPush.Message : "",
                    LocalizedKey = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.NotificarResposta,
                    LocalizedArgs = GetNotificacaoRespostaArgs(origem, notificacao).ToList(),
                    PlatformType = (PlatformType)device.Plataforma
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushRespostaArgs GetNotificacaoRespostaArgs(Notificacao origem, Notificacao notificacao)
        {
            DTOGetPushRespostaArgs result = new DTOGetPushRespostaArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = origem.Codigo,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = (NotificationType1)TypeConverter.Convert((NotificationType2)notificacao.TipoMensagem),
                DataHoraNotificacao = notificacao.DataNotificacao
            };
            return result;
        }

        private List<object> GetNotificacaoArgs(Notificacao notificacao)
        {
            // Define o formato da notifica��o
            switch (GetFormatoNotificacao((NotificationType2)notificacao.TipoMensagem))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        var chegada = GetNotificacaoChegadaArgs(notificacao);
                        return chegada.ToList();
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                        var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                        return portaria.ToList();
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        if ((NotificationType1)notificacao.TipoMensagem == NotificationType1.SindicoPorMorador)
                        {
                            var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                            return notificacaoSindico.ToList();
                        }
                        else
                        {
                            var condominio = new Condominio().Get(notificacao.Origem.Value);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                            return notificacaoSindico.ToList();
                        }
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        var origem = new Notificacao().Get(notificacao.Origem.Value);
                        var resposta = GetNotificacaoRespostaArgs(origem, notificacao);
                        return resposta.ToList();
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        #region RegisterDevice

        private DTORegisterDevice RegisterDeviceData { get; set; }

        public virtual Pessoa RegisterDevice(DTORegisterDevice registerDevice)
        {
            RegisterDeviceData = registerDevice;
            Device device;

            AuthorizeRequestByUser(registerDevice.ChaveCriptografada, registerDevice.Condominio)
                .LoadDevice(out device);

            device.APIVersion = registerDevice.APIVersion;

            return SetNotificationSettings(device)
                .SaveDevice(device);
        }

        public virtual Pessoa UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            // Valida o acesso
            AuthorizeRequestByUser(unregisterDevice.ChaveCriptografada, unregisterDevice.Condominio);

            // seleciona o Device
            Device device = new Device().Repository.Get(unregisterDevice.DeviceToken);
            if (device == null)
                throw new ItemNotFoundException(unregisterDevice.DeviceToken, "Device");

            // Remove o Device
            device.Status = (byte)DeviceStatus.Inactive;
            device.Save();

            // Retona o resultado
            return this;
        }

        private Pessoa SaveDevice(Device device)
        {
            Devices.Add(device);
            base.Save();
            return this;
        }

        private Pessoa LoadDevice(out Device device)
        {
            device = new Device().Repository.Get(RegisterDeviceData.DeviceToken) ?? new Device
            {
                Nome = RegisterDeviceData.Name,
                Token = RegisterDeviceData.DeviceToken,
                Plataforma = (byte)RegisterDeviceData.PlatformType,
                CreatedAt = DateTime.Now,
            };

            var condominio = new Condominio().Get(RegisterDeviceData.Condominio);
            device.Condominio = condominio;
            device.Pessoa = this;
            device.Plataforma = (byte)RegisterDeviceData.PlatformType;
            device.Status = (byte)DeviceStatus.Active;
            device.UpdatedAt = DateTime.Now;

            return this;
        }

        private Pessoa SetNotificationSettings(Device device)
        {
            var notificationSettings =
                Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString()).NotificationSettings ??
                new List<NotificationSettings>();

            var setting = notificationSettings.FirstOrDefault(t => t.Bundle.Equals(RegisterDeviceData.Bundle));

            if (setting == null)
                throw new Lalubema.Utilitarios.Exceptions.Base.ConfigurationException("NotificationSettings");

            device.NotificationSettings = setting;

            return this;
        }

        private Pessoa AuthorizeRequestByUser(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            bool hasPermission;

            // Usu�rio de seguran�a pertence ao Condom�nio
            var usuario = new Pessoa().Get(security.CodigoUsuario);
            hasPermission = usuario.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();

            // Usu�rio do Device pertence ao Condom�nio
            hasPermission = this.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();

            return this;
        }

        #endregion

        public virtual ChegadaSegura ChegadaSegura(DTOPushChegadaSegura entrada)
        {
            var notificacao = new Notificacao().Get(entrada.CodigoNotificacao);
            if (notificacao == null)
                throw new ItemNotFoundException(entrada.CodigoNotificacao, "Notifica��o");

            if ((NotificationStatus)notificacao.Status == NotificationStatus.Canceled || (NotificationStatus)notificacao.Status == entrada.StatusNotificacao)
            {
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Notificacao.Codigo", notificacao.Codigo);
                Dictionary<string, string> order = new Dictionary<string, string>();
                order.Add("CreatedAt", "desc");
                return new ChegadaSegura().Repository.ListAll(filter, order).FirstOrDefault();
            }

            var chegada = new ChegadaSegura()
            {
                CreatedAt = DateTime.Now.ToBrasiliaTime(),
                Status = (byte)entrada.StatusNotificacao,
                Solicitante = this,
                Notificacao = notificacao,
                Device = GetDevice(entrada.DeviceToken),
            };
            chegada.Save();
             
            var statusOrder = new[] { NotificationStatus.New, NotificationStatus.Delivered, NotificationStatus.Seen, NotificationStatus.Confirmed, NotificationStatus.Canceled, NotificationStatus.Answered }.ToList();
            if (statusOrder.FindIndex(w => w == (NotificationStatus)notificacao.Status) < statusOrder.FindIndex(w => w == (NotificationStatus)chegada.Status))
            {
                notificacao.Status = (byte)chegada.Status;
                notificacao.Save();
            }

            if (chegada.Status != (byte)NotificationStatus.New)
            {
                var timerkey = notificacao.Codigo.ToString() + "|" + entrada.DeviceToken;
                Timer t = ChegadaSeguraTimers.FirstOrDefault(w => w.Key.Equals(timerkey, StringComparison.InvariantCultureIgnoreCase)).Value;
                if (t != null)
                {
                    ChegadaSeguraTimers.Remove(timerkey);
                    t.Dispose();
                }
            }

            // Envia para os devices do Condom�nio
            IList<Device> devices = null;
            if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.SafeArrival)
            {
                Veiculo veiculo = new Veiculo().Get(notificacao.Origem.Value);
                devices = veiculo.UnidadeHabitacional.Edificio.Condominio.Devices;
            }
            else if ((NotificationType2)notificacao.TipoMensagem == NotificationType2.FootSafeArrival || (NotificationType2)notificacao.TipoMensagem == NotificationType2.TaxiSafeArrival)
            {
                var uh = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                devices = uh.Edificio.Condominio.Devices;
            }

            if (devices == null)
                throw new ItemNotFoundException(0, "Devices");

            Pessoa destinatario = chegada.Status != (byte)NotificationStatus.Canceled ? notificacao.Remetente : null;
            Device d = notificacao.Remetente.Username == "demonstracao@lalubema.com" ? notificacao.RemetenteDevice : null;
            if (chegada.Status == (byte)NotificationStatus.Confirmed || chegada.Status == (byte)NotificationStatus.Seen)
                devices = devices.Where(w => !w.Token.Equals(chegada.Device.Token) && w.Status == (byte)DeviceStatus.Active && 
                    (w.Pessoa == null || w.Pessoa.Codigo == notificacao.Remetente.Codigo) && (d == null || w.Codigo == d.Codigo)).ToList();
            else
                devices = devices.Where(w => w.Status == (byte)DeviceStatus.Active && (d == null || w.Codigo == d.Codigo) && 
                    ((destinatario == null && w.Pessoa == null) || 
                    (destinatario != null && w.Pessoa != null && w.Pessoa.Codigo == destinatario.Codigo))).ToList();

            string key;
            switch (chegada.Status)
            {
                case (byte)NotificationStatus.Seen:
                    key = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraStatusVisualized;
                    break;
                case (byte)NotificationStatus.Confirmed:
                    key = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraStatusConfirmed;
                    break;
                case (byte)NotificationStatus.Delivered:
                    key = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraStatusDelivered;
                    break;
                case (byte)NotificationStatus.Canceled:
                    key = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraStatusCanceled;
                    break;
                default:
                    key = Lalubema.Orion.Common.Tipos.LocalizedKey.Corona.ChegadaSeguraStatus;
                    break;
            }

            foreach (var device in devices)
            {
                if (device.APIVersion.HasValue && device.APIVersion.Value >= 2f)
                {
                    var pushMessage = new DTOPushMessage
                    {
                        Bundle = device.NotificationSettings.Bundle,
                        Sound = "bell-ringing.aiff",
                        DeviceToken = device.Token,
                        LocalizedKey = key,
                        LocalizedArgs = GetNotificacaoChegadaArgs(notificacao).ToList(),
                        PlatformType = (PlatformType)device.Plataforma
                    };

                    try
                    {
                        NotificationManager.SendPushMessage(pushMessage);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return chegada;
        }

        private static void ReenviarChegadaSegura(object objState)
        {
            DTOPushMessage push = (DTOPushMessage)objState;
            int codigoNotificacao = Convert.ToInt32(push.LocalizedArgs[8]);
            var notificacao = new Notificacao().Get(codigoNotificacao);
            var timerkey = notificacao.Codigo + "|" + push.DeviceToken;
            Timer t = ChegadaSeguraTimers.FirstOrDefault(w => w.Key.Equals(timerkey, StringComparison.InvariantCultureIgnoreCase)).Value;
            if (t == null)
                return;

            try
            {
                var device = new Device().Get(push.DeviceToken);
                if (device.Status != (byte)DeviceStatus.Active)
                    return;
            }
            catch (Exception)
            {
                ChegadaSeguraTimers.Remove(timerkey);
                t.Dispose();
                return;
            }

            if (notificacao.Status == (byte)NotificationStatus.Canceled || notificacao.Status == (byte)NotificationStatus.Confirmed || notificacao.Status == (byte)NotificationStatus.Finalized)
            {
                foreach (var timer in ChegadaSeguraTimers.Where(w => w.Key.StartsWith(codigoNotificacao.ToString(), StringComparison.InvariantCultureIgnoreCase)).ToList())
                {
                    timer.Value.Dispose();
                    ChegadaSeguraTimers.Remove(timer.Key);
                }
                return;
            }

            var d = DateTime.Parse(push.LocalizedArgs[4].ToString() + " " + push.LocalizedArgs[5].ToString(), new CultureInfo("pt-BR").DateTimeFormat);
            if (DateTime.Now.ToBrasiliaTime().AddDays(-1) >= d)
            {
                ChegadaSeguraTimers.Remove(timerkey);
                t.Dispose();
                return;
            }

            NotificationManager.SendPushMessage((DTOPushMessage)objState);
        }

        private static Dictionary<string, Timer> ChegadaSeguraTimers = new Dictionary<string, Timer>();
    }
}