using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Security;
using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class Edificio : DomainBase<Edificio, IEdificioRepositorio<Edificio>>, IDomainModel
    {
        public Edificio()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }
        public virtual string Nome { get; set; }
        public virtual int Unidades { get; set; }
        public virtual int Andares { get; set; }
        public virtual int UnidadesPorAndar { get; set; }
        public virtual string FormNumUnidades { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual IList<EdificioContato> Contatos { get; set; }
        public virtual Condominio Condominio { get; set; }
        public virtual IList<UnidadeHabitacional> UnidadeHabitacional { get; set; }
        public virtual IList<LocalBem> AreasComum { get; set; }

        private string _telefone;
        public virtual string Telefone { get { 
            var ct = Contatos.FirstOrDefault(w => w.Contato.Tipo == (byte)ContactType.HomePhone);
            return ct != null ? ct.Contato.Valor : string.Empty;
        }
            set { _telefone = value; }
        }
        private string _logradouro;
        private string _bairro;
        private string _cidade;
        private string _estado;
        private string _cep;
        public virtual string Logradouro { get { return Endereco != null ? Endereco.Logradouro : string.Empty; } set { _logradouro = value; } }
        public virtual string Bairro { get { return Endereco != null ? Endereco.Bairro : string.Empty; } set { _bairro = value; } }
        public virtual string Cidade { get { return Endereco != null ? Endereco.Cidade : string.Empty; } set { _cidade = value; } }
        public virtual string Estado { get { return Endereco != null ? Endereco.Estado : string.Empty; } set { _estado = value; } }
        public virtual string Cep { get { return Endereco != null ? Endereco.CEP : string.Empty; } set { _cep = value; } }

        private void OnCreated()
        {
            Contatos = new List<EdificioContato>();
            UnidadeHabitacional = new List<UnidadeHabitacional>();
            AreasComum = new List<LocalBem>();
        }

        public override void Save()
        {
            try
            {
                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;

                if (!string.IsNullOrWhiteSpace(_telefone))
                {
                    var ct = Contatos.FirstOrDefault(w => w.Contato.Tipo == (byte)ContactType.HomePhone);
                    if (ct == null)
                    {
                        ct = new EdificioContato()
                        {
                            Edificio = this,
                            Contato = new Contato()
                            {
                                CreatedAt = DateTime.UtcNow,
                                UpdatedAt = DateTime.UtcNow,
                                Tipo = (byte)ContactType.HomePhone,
                                Valor = _telefone
                            }
                        };
                        Contatos.Add(ct);
                    }
                    ct.Contato.Valor = _telefone;
                }

                if (!string.IsNullOrWhiteSpace(_logradouro) || !string.IsNullOrWhiteSpace(_bairro) ||
                    !string.IsNullOrWhiteSpace(_cidade) || !string.IsNullOrWhiteSpace(_estado) || !string.IsNullOrWhiteSpace(_cep))
                {
                    if (Endereco == null)
                        Endereco = new Endereco() { CreatedAt = DateTime.UtcNow, Pais = "Brasil" };

                    Endereco.Logradouro = _logradouro;
                    Endereco.Bairro = _bairro;
                    Endereco.Cidade = _cidade;
                    Endereco.CEP = _cep;
                    Endereco.Estado = _estado;
                    Endereco.UpdatedAt = DateTime.UtcNow;
                }
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Edificio", Nome, exception);
            }

            base.Save();
        }

        public virtual void AssociarLocalBens(DTOAssociarEdificioLocalBemEntrada edificioLocalBem)
        {
            AuthorizeRequestPorCondominio(edificioLocalBem);

            foreach (var localBem in edificioLocalBem.LocalBens)
            {
                if (localBem.Ativo)
                {
                    var localBemModel = new LocalBem().Get(localBem.Codigo);

                    if (localBemModel.Condominio.Codigo == Condominio.Codigo)
                    {
                        AreasComum.Add(localBemModel);

                        localBemModel.Edificios.Add(this);
                    }
                }
                else
                {
                    var localBemModel = AreasComum.FirstOrDefault(t => t.Codigo == localBem.Codigo);

                    if (localBemModel != null)
                    {
                        AreasComum.Remove(localBemModel);

                        localBemModel.Edificios.Remove(this);
                    }
                }
            }

            base.Save();
        }

        private void AuthorizeRequestPorCondominio(DTOEntrada edificioLocalBem)
        {
            var security = new ChaveCriptografadaHelper(edificioLocalBem.ChaveCriptografada);

            var isSindico = Condominio.IsSindico(security.CodigoUsuario);

            if (!isSindico)
                throw new Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException();
        }
    }
}