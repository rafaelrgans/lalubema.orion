﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.Corona
{
    public class Grupo : DomainBase<Grupo, IGrupoRepositorio<Grupo>>, IDomainModel
    {
        IList<GrupoPermissao> permissoes;

        public virtual int ID { get; set; }

        public virtual string Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual IList<GrupoPermissao> Permissoes
        {
            get { return permissoes; }
            set { permissoes = value; }
        }
    }
}