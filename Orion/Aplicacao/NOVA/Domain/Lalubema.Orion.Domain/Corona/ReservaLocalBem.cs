using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Types;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Orion.Common.Exceptions.Orion;

namespace Lalubema.Orion.Domain.Corona
{
    public class ReservaLocalBem : DomainBase<ReservaLocalBem, IReservaLocalBemRepositorio<ReservaLocalBem>>, IDomainModel
    {
        public ReservaLocalBem()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual DateTime DataHoraInicio { get; set; }

        public virtual DateTime DataHoraFim { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual StatusReserva Status { get; set; }

        public virtual string Obs { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual LocalBem LocalBem { get; set; }

        public virtual Usuario Pessoa { get; set; }

        public virtual IList<Usuario> Convidados { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public virtual bool IsTodayOrAfter()
        {
            if (LocalBem.AntecedenciaMaxReserva == 0)
                return DataHoraFim.CompareTo(DateTime.UtcNow) >= 0;

            var endDate = DateTime.UtcNow.AddDays(LocalBem.AntecedenciaMaxReserva);

            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0, 0);

            var periodoReserva = new DateTimeRange(DateTime.UtcNow, endDate);

            return periodoReserva.Intersects(new DateTimeRange(DataHoraInicio, DataHoraFim));
        }

        private void OnCreated()
        {
            Convidados = new List<Usuario>();
        }

        public override void Save()
        {
            try
            {
                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("ReservaLocalBem", LocalBem.Nome, exception);
            }

            base.Save();
        }

        public virtual IList<ReservaLocalBem> GetReservas(int codigoCondominio, int codigoUsuario)
        {
            var reservas = Repository.ListAll(codigoCondominio, codigoUsuario);

            return reservas.Where(t => t.IsTodayOrAfter()).ToList();
        }
    }
}