
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.NovosTipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Exceptions.Base;

namespace Lalubema.Orion.Domain.Corona
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>
    {
        public Usuario()
        {
            OnCreated();
        }

        IList<Pessoa> pessoas;
        IList<UsuarioPermissao> permissoes;
        IList<UsuarioCondominio> usuarioCondominios;

        public virtual int Codigo { get; set; }
        public virtual byte Status { get; set; }
        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Senha { get; set; }
        public virtual int FalhasAcesso { get; set; }
        public virtual bool ImpedirAcesso { get; set; }
        public virtual DateTime? DataAtivacao { get; set; }
        public virtual Aplicacao Aplicacao { get; set; }
        public virtual IList<UsuarioGemini> UsuarioGemini { get; set; }
        public virtual IList<Pessoa> Pessoas
        {
            get { return pessoas; }
            set { pessoas = value; }
        }
        public virtual IList<UsuarioPermissao> Permissoes
        {
            get { return permissoes; }
            set { permissoes = value; }
        }
        public virtual IList<UsuarioCondominio> UsuarioCondominios
        {
            get { return usuarioCondominios; }
            set { usuarioCondominios = value; }
        }

        private void OnCreated()
        {
            pessoas = new List<Pessoa>();
            usuarioCondominios = new List<UsuarioCondominio>();
            UsuarioGemini = new List<UsuarioGemini>();
        }

        public override void Save()
        {
            FalhasAcesso = 0;
            ImpedirAcesso = true;

            base.Save();
        }
    }
}