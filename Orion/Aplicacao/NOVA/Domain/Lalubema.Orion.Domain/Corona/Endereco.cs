﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class Endereco : DomainBase<Endereco, IEnderecoRepositorio<Endereco>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Logradouro { get; set; }
        public virtual string Bairro { get; set; }
        public virtual string Cidade { get; set; }
        public virtual string Estado { get; set; }
        public virtual string Pais { get; set; }
        public virtual string CEP { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
    }
}