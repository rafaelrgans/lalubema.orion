using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class Licenca : DomainBase<Licenca, ILicencaRepositorio<Licenca>>, IDomainModel
    {
        public Licenca()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual int? NumMaxEdificios { get; set; }

        public virtual int? NumMaxUnidadesHabitacionais { get; set; }

        public virtual byte Status { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual DateTime? DataInicioValidade { get; set; }

        public virtual DateTime? DataFinalValidade { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual Corporativo Corporativo { get; set; }

        public virtual bool isValid {
            get { 
                return DataAtivacao.HasValue && (DateTime.UtcNow.Between(DataInicioValidade.Value, DataFinalValidade.Value));
            }
        }

        private void OnCreated()
        {
        }

        public override void Save()
        {
            try
            {
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Licenca", Codigo.ToString(), exception);
            }

            base.Save();
        }
    }
}