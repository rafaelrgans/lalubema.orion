using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class ChegadaSegura : DomainBase<ChegadaSegura, IChegadaSeguraRepositorio<ChegadaSegura>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual byte Status { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual Pessoa Solicitante { get; set; }

        public virtual Device Device { get; set; }

        public virtual Notificacao Notificacao { get; set; }
    }
}