using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;
using System.IO;
using Lalubema.Orion.Common.NovosTipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class Veiculo : DomainBase<Veiculo, IVeiculoRepositorio<Veiculo>>, IDomainModel
    {
        public Veiculo()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual byte Tipo { get; set; }

        public virtual byte Status { get; set; }

        public virtual string Marca { get; set; }

        public virtual string Modelo { get; set; }

        public virtual string Cor { get; set; }

        public virtual string Placa { get; set; }

        public virtual DateTime CreatedAt { get; set; }
        public virtual DateTime UpdatedAt { get; set; }

        public virtual Anexo FotoFrenteAnexo { get; set; }
        public virtual Anexo FotoTraseiraAnexo { get; set; }

        public virtual string UrlFotoFrente { get { return FotoFrenteAnexo != null ? FotoFrenteAnexo.Url : null; } set { } }
        public virtual string UrlFotoTraseira { get { return FotoTraseiraAnexo != null ? FotoTraseiraAnexo.Url : null; } set { } }

        public virtual string FotoFrente { get; set; }
        public virtual string FotoTraseira { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual int CodigoUnidadeHabitacional { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual int CodigoCondominio { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual int CodigoPessoa { get; set; }

        private void OnCreated()
        {
        }

        public override void Save()
        {
            Status = 1;
            if ((UnidadeHabitacional == null || UnidadeHabitacional.Codigo != CodigoUnidadeHabitacional) && CodigoUnidadeHabitacional > 0)
            {
                UnidadeHabitacional = new UnidadeHabitacional();
                UnidadeHabitacional = UnidadeHabitacional.Get(CodigoUnidadeHabitacional);
            }

            try
            {
                if (Codigo == 0)
                    CreatedAt = DateTime.UtcNow;

                UpdatedAt = DateTime.UtcNow;

                var oldUrlFrente = FotoFrenteAnexo != null ? FotoFrenteAnexo.Url : string.Empty;
                if (FotoFrente != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(FotoFrente),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Aplicacao.Corona, extension, fileHelper.File);

                        if (FotoFrenteAnexo == null)
                            FotoFrenteAnexo = new Anexo();

                        FotoFrenteAnexo.CreatedAt = DateTime.Now;
                        FotoFrenteAnexo.UpdatedAt = DateTime.Now;
                        FotoFrenteAnexo.Extensao = extension;
                        FotoFrenteAnexo.Nome = Path.GetFileName(retorno.Path);
                        FotoFrenteAnexo.Status = (byte)AnnexStatus.Active;
                        FotoFrenteAnexo.Tamanho = fileHelper.File.Length;
                        FotoFrenteAnexo.Url = retorno.Path;
                        FotoFrenteAnexo.Entidade = (byte)AnnexEntity.Picture;
                        FotoFrenteAnexo.Tipo = (byte)AnnexType.Image;

                        UrlFotoFrente = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrlFrente))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlFrente);
                }

                var oldUrlTraseira = FotoTraseiraAnexo != null ? FotoTraseiraAnexo.Url : string.Empty;
                if (FotoTraseira != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                       Convert.FromBase64String(FotoTraseira),
                                       ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Aplicacao.Corona, extension, fileHelper.File);

                        if (FotoTraseiraAnexo == null)
                            FotoTraseiraAnexo = new Anexo();

                        FotoTraseiraAnexo.CreatedAt = DateTime.Now;
                        FotoTraseiraAnexo.UpdatedAt = DateTime.Now;
                        FotoTraseiraAnexo.Extensao = extension;
                        FotoTraseiraAnexo.Nome = Path.GetFileName(retorno.Path);
                        FotoTraseiraAnexo.Status = (byte)AnnexStatus.Active;
                        FotoTraseiraAnexo.Tamanho = fileHelper.File.Length;
                        FotoTraseiraAnexo.Url = retorno.Path;
                        FotoTraseiraAnexo.Entidade = (byte)AnnexEntity.Picture;
                        FotoTraseiraAnexo.Tipo = (byte)AnnexType.Image;

                        UrlFotoTraseira = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrlTraseira))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlTraseira);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Ve�culo", Placa, exception);
            }

            base.Save();
        }
    }
}