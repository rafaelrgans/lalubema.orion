﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;
using System;

namespace Lalubema.Orion.Domain.Corona
{
    public class Permissao : DomainBase<Permissao, IPermissaoRepositorio<Permissao>>, IDomainModel
    {
        public virtual int ID { get; set; }

        public virtual string Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }
    }
}