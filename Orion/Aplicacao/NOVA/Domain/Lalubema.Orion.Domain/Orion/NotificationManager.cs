﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Integration.Spec.Orion.Notification;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Orion
{
    public class NotificationManager
    {
        public static void StartServices()
        {
            var applications = new Aplicacao().ListAll();

            foreach (var application in applications)
            {
                foreach (var settings in application.NotificationSettings)
                {
                    NotificationRepository.StartServices(settings.Transform<DTONotificationSettings>());
                }
            }
        }

        public static void StopServices()
        {
            NotificationRepository.StopServices();
        }

        public static void SendPushMessage(DTOPushMessage pushMessage)
        {
            NotificationRepository.SendPush(pushMessage);
        }

        protected static INotificationRepository NotificationRepository
        {
            get { return ObjectFactory.GetSingleton<INotificationRepository>(); }
        }
    }
}