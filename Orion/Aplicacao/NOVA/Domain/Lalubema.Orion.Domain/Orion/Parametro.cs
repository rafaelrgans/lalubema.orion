﻿using System.Collections.Generic;
using System.Configuration;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;
using Lalubema.Orion.Domain.Corona;

namespace Lalubema.Orion.Domain.Orion
{
    public class Parametro : DomainBase<Parametro, IParametroRepositorio<Parametro>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Conteudo { get; set; }

        public virtual Parametro Pai { get; set; }

        public virtual Corporativo Corporativo { get; set; }

        public virtual IList<Parametro> Filhos { get; set; }

        private static string _ambiente;

        private static string Ambiente
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_ambiente))
                {
                    _ambiente = ConfigurationManager.AppSettings["ambiente"];
                }
                
                return _ambiente;
            }
        }

        public static string BuscarPorChave(string chave, int corporativo)
        {
            var parametro = GetRepository().BuscarPorChave(chave, corporativo);

            if (parametro == null)
                return null;

            return parametro.Conteudo;
        }
    }
}