﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Orion
{
    public class Aplicacao : DomainBase<Aplicacao, IAplicacaoRepositorio<Aplicacao>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Senha { get; set; }

        public virtual string Versao { get; set; }

        public virtual string ChaveCriptografada { get; set; }

        public virtual IList<NotificationSettings> NotificationSettings { get; set; }

        public virtual string Autenticar(string senha)
        {
            return Autenticar(senha, 1);
        }

        public virtual string Autenticar(string senha, int? corporativo)
        {
            if (!Senha.Equals(senha, StringComparison.InvariantCulture))
                throw new UsuarioOuSenhaInvalidosException();

            List<String> prs = new List<String>();
            
            prs.Add(CryptographyType.System.ConvertToString());
            prs.Add(DateTime.UtcNow.ToString("o"));
            prs.Add(Nome);
            prs.Add(Codigo.ToString(CultureInfo.InvariantCulture));
            prs.Add(Versao);
            prs.Add(CryptographyType.System.ConvertToString());
            if (corporativo.HasValue)
                prs.Add(corporativo.Value.ToString(CultureInfo.InvariantCulture));

            ChaveCriptografada = SecurityHelper.CreateCryptographyKey(prs.ToArray());

            return ChaveCriptografada;
        }

        public static Aplicacao Buscar(string sistema)
        {
            return GetRepository().Buscar(sistema);
        }
    }
}