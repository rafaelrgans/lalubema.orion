﻿using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Orion
{
    public class NotificationSettings :
        DomainBase<NotificationSettings, INotificationSettingsRepository<NotificationSettings>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Bundle { get; set; }

        public virtual string AndroidAccessKey { get; set; }

        public virtual string AndroidSenderId { get; set; }

        public virtual string AppleCertificate { get; set; }

        public virtual string AppleCertificatePassword { get; set; }
    }
}