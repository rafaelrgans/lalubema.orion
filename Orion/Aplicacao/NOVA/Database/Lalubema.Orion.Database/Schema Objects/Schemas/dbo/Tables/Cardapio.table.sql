﻿CREATE TABLE [dbo].[Cardapio] (
    [Codigo]            INT            IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (80)   NOT NULL,
    [Descricao]         VARCHAR (4000) NOT NULL,
    [CdEstabelecimento] INT            NOT NULL
);

