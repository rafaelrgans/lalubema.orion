﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILE (NAME = [DBVanadium], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', FILEGROWTH = 1024 KB) TO FILEGROUP [PRIMARY];

