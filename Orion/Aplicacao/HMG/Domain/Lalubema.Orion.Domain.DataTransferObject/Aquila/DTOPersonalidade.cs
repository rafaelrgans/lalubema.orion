﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    [KnownType(typeof (DTOPersonalidade))]
    [KnownType(typeof (DTOAlbum))]
    [KnownType(typeof (DTOItem))]
    [KnownType(typeof (DTOAgenda))]
    [KnownType(typeof(DTONoticia))]
    public class DTOPersonalidade : DTORetorno
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string Resumo { get; set; }

        public DateTime Data { get; set; }

        public int Mes { get { return Data.Month; } }

        public IList<DTOAlbum> Albuns { get; set; }

        public IList<DTOAgenda> Agendas { get; set; }

        public IList<DTONoticia> Noticias { get; set; }
    }
}