﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    public class DTONoticia
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Mensagem { get; set; }

        [DataMember]
        public DateTime Data { get; set; }

        [DataMember]
        public string UrlFoto { get; set; }
    }
}