﻿using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    public class DTOAlbum
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public int Tipo { get; set; }

        public string UrlCapa { get; set; }

        public DateTime Data { get; set; }

        public int Mes { get { return Data.Month; } set { throw new NotImplementedException(); } }

        public IList<DTOItem> Itens { get; set; }
    }
}