﻿using System;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    public class DTOAgenda
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public DateTime Data { get; set; }

        public string Local { get; set; }
    }
}