﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Personalidade
{
    [ObjectMap("AquilaPersonalidade")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Personalidade")]
    public class DTOPersonalidadeEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Nome da personalidade",
            GroupName = "Dados Básico", Name = "Nome", Order = 1, Prompt = "Digite o nome da personalidade",
            ShortName = "Nome")]
        [DataMember]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Resumo da personalidade",
            GroupName = "Dados Básico", Name = "Resumo", Order = 2, Prompt = "Digite o resumo da personalidade",
            ShortName = "Resumo")]
        [DataMember]
        public string Resumo { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.Read | FilterTypeFlags.ReadAll)]
        public string Username { get; set; }
    }
}