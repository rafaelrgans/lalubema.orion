﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Album
{
    [DisplayColumn("Nome")]
    [ObjectMap("AquilaAlbum")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Album")]
    public class DTOAlbumEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Nome do álbum",
            GroupName = "Dados Básico", Name = "Nome", Order = 1, Prompt = "Digite o nome do álbum", ShortName = "Nome")
        ]
        [DataMember]
        public string Nome { get; set; }

        [Required]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Tipo do álbum",
            GroupName = "Dados Básico", Name = "Tipo", Order = 2, Prompt = "Digite o tipo do álbum", ShortName = "Tipo")
        ]
        [DataMember]
        public int Tipo { get; set; }

        [DataMember]
        public byte[] Capa { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data do álbum",
            GroupName = "Dados Básico", Name = "Data", Order = 4, Prompt = "Digite a data do álbum", ShortName = "Data")
        ]
        [DataMember]
        public DateTime Data { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        [Filter(FilterTypeFlags.ReadAll)]
        public int CodigoPersonalidade { get; set; }
    }
}