﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Item
{
    [DisplayColumn("Nome")]
    [ObjectMap("AquilaItem")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Item")]
    public class DTOItemEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Nome do item",
            GroupName = "Dados Básico", Name = "Nome", Order = 1, Prompt = "Digite o nome do item", ShortName = "Nome")
        ]
        [DataMember]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = false, Description = "Descrição do item",
            GroupName = "Dados Básico", Name = "Descrição", Order = 2, Prompt = "Informe uma breve descrição do item",
            ShortName = "Descrição")]
        [DataMember]
        public string Descricao { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Conteúdo do item",
            GroupName = "Dados Básico", Name = "Conteúdo do item", Order = 3, Prompt = "Conteúdo do item", ShortName = "Conteúdo")
        ]
        [DataMember]
        public string Conteudo { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public byte[] Media { get; set; }

        [DataMember]
        public DTOAlbum Album { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        [Filter(FilterTypeFlags.ReadAll)]
        public int CodigoAlbum { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Delete | FilterTypeFlags.Save)]
        public int CodigoPersonalidade { get; set; }
    }
}