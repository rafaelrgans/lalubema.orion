﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Noticia
{
    [DisplayColumn("Mensagem")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Noticia")]
    public class DTONoticiaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Texto da notícia",
            GroupName = "Dados Básico", Name = "Texto", Order = 1, Prompt = "Digite o texto da notícia",
            ShortName = "Texto")]
        [DataMember]
        public string Mensagem { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data da notícia",
            GroupName = "Dados Básico", Name = "Data", Order = 4, Prompt = "Digite a data da notícia",
            ShortName = "Data")]
        [DataMember]
        public DateTime Data { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Foto da notícia",
            GroupName = "Dados Básico", Name = "Foto da notícia", Order = 3, Prompt = "Foto da notícia",
            ShortName = "Foto da notícia")]
        [DataMember]
        [Map("UrlFoto")]
        public string Foto { get; set; }
    }
}