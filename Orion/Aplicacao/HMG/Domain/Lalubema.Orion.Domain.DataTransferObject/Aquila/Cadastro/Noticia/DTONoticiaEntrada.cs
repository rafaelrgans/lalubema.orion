﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Noticia
{
    [DisplayColumn("Mensagem")]
    [ObjectMap("AquilaNoticia")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Noticia")]
    public class DTONoticiaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Texto",
            GroupName = "Dados Básico", Name = "Mensagem", Order = 1, Prompt = "Digite o texto da notícia",
            ShortName = "Texto")]
        [DataMember]
        public string Mensagem { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data da notícia",
            GroupName = "Dados Básico", Name = "Data", Order = 4, Prompt = "Digite a data da notícia",
            ShortName = "Data")]
        [DataMember]
        public DateTime Data { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        [Filter(FilterTypeFlags.ReadAll)]
        public int CodigoPersonalidade { get; set; }
    }
}