﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    [KnownType(typeof (DTOModifiedAlbum))]
    [KnownType(typeof(DTOItem))]
    public class DTOModifiedAlbum : DTOEntrada
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public int Tipo { get; set; }

        public string UrlCapa { get; set; }

        public DateTime Data { get; set; }

        public IList<DTOItem> Itens { get; set; }
    }
}