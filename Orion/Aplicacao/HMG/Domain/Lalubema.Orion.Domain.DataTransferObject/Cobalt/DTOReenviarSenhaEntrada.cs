﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Cobalt")]
    public class DTOReenviarSenhaEntrada : DTOEntrada
    {
        [DataMember]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataMember]
        [Required]
        public string Senha { get; set; }
    }
}