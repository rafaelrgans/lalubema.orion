﻿using System;
using System.Runtime.Serialization;
namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica
{
    [DataContract(Namespace = "http://www.lalubema.com/Cobalt/Cadastro/Dica")]
	[Serializable]
    public class DTODica
    {
		[DataMember]
		public int Codigo { get; set; }

        [DataMember]
        public string Titulo { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public bool Ativo { get; set; }
    }
}