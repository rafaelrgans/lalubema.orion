﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes.Mvc;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica
{
	[TypeDisplay("Dica")]
	[DisplayColumn("Titulo")]
	[DataContract(Namespace = "http://www.lalubema.com/Cobalt/Cadastro/Dica")]
    public class DTODicasRetorno : DTORetorno
    {
		[Key]
		[DataMember]
		public int Codigo { get; set; }

		[DataMember]
		public string Titulo { get; set; }

		[DataMember]
		public string Descricao { get; set; }

		[DataMember]
		[Required(AllowEmptyStrings = true)]
		[Display(Name = "Ativo", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Se a dica está ativa ou não", GroupName = "Dados Básicos", Order = 1)]
		public bool Ativo { get; set; }

        [DataMember]
        public IList<DTODica> Dicas { get; set; }
    }
}