﻿using System.Net;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Lalubema.Orion.Domain.DataTransferObject
{
    [DataContract(Namespace = "http://www.lalubema.com/")]
    public class DTORetorno : DTOBase
    {
        public DTORetorno(HttpStatusCode httpStatusCode)
        {
            SetHttpStatusCode(httpStatusCode);
        }

        public DTORetorno()
        {
        }

        [DataMember]
        [ScaffoldColumn(false)]
        public int CodigoRetorno { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string DescricaoRetorno { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public bool OcorreuErro { get; set; }
    }
}