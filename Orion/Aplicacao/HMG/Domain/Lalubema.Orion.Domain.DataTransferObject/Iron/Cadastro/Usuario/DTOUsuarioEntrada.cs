﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Usuario
{
    [ObjectMap("IronUsuario")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Usuario")]
    public class DTOUsuarioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Senha { get; set; }

        [DataMember]
        [Required]
        public bool IsAdministrador { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}