﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Usuario
{
    [DisplayColumn("Username")]
    [TypeDisplay("Usuário")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Usuario")]
    public class DTOUsuarioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Nome do Usuário", GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "E-mail", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "E-mail do usuário", GroupName = "Dados Básicos", Order = 2)]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }

        [DataMember]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Senha", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Senha do Usuário", GroupName = "Dados Básicos", Order = 3)]
        public string Senha { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Administrador", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Administrador", GroupName = "Dados Básicos", Order = 4)]
        public bool IsAdministrador { get; set; }
    }
}