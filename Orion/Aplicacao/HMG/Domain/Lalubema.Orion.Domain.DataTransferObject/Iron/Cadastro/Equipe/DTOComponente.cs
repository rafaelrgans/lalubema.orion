﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Equipe
{
    [Serializable]
    [TypeDisplay("Componente")]
    [DisplayColumn("Nome")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Equipe")]
    public class DTOComponente
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do componente",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Mini currículo", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Mini currículo do componente", GroupName = "Dados Básicos", Order = 2)]
        public string MiniCurriculo { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto do componente",
            GroupName = "Dados Básicos", Order = 3)]
        [Map("UrlFoto")]
        public string Foto { get; set; }
    }
}