﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Equipe
{
    [ObjectMap("IronEquipe")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Equipe")]
    public class DTOEquipeEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public byte[] FotoAltaResolucao { get; set; }

        [DataMember]
        public string FileNameAltaResolucao { get; set; }

        [DataMember]
        public DTOPrograma Programa { get; set; }

        [DataMember]
        public IList<DTOComponente> Componentes { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}