﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Categoria
{
    [ObjectMap("IronCategoria")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Categoria")]
    public class DTOCategoriaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descricao da categoria", GroupName = "Dados Básicos", Order = 1)]
        public string Descricao { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}