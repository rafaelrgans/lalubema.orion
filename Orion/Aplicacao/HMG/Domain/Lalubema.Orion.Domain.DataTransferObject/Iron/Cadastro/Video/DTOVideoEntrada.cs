﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Categoria;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Video
{
    [ObjectMap("IronVideo")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Video")]
    public class DTOVideoEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Título", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Titulo do vídeo",
            GroupName = "Dados Básicos", Order = 1)]
        public string Titulo { get; set; }

        [DataMember]
        [DataType(DataType.Url)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "URL", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Url do vídeo",
            GroupName = "Dados Básicos", Order = 2)]
        public string UrlVideo { get; set; }

        [DataMember]
        public DTOCategoriaRetorno Categoria { get; set; }

        [DataMember]
        public DTOPrograma Programa { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}