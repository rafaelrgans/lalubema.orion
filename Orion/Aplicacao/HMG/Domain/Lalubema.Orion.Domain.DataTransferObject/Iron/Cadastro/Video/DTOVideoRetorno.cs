﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Categoria;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Video
{
    [TypeDisplay("Vídeo")]
    [DisplayColumn("Titulo")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Video")]
    public class DTOVideoRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Título", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Titulo do vídeo",
            GroupName = "Dados Básicos", Order = 1)]
        public string Titulo { get; set; }

        [DataMember]
        [DataType(DataType.Url)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "URL", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Url do vídeo",
            GroupName = "Dados Básicos", Order = 2)]
        public string UrlVideo { get; set; }

        [DataMember]
        public DTOCategoriaRetorno Categoria { get; set; }

        [DataMember]
        public DTOPrograma Programa { get; set; }
    }
}