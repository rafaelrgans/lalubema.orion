﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Componente
{
    [ObjectMap("IronComponente")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Componente")]
    public class DTOComponenteEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do componente",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Mini currículo", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Mini currículo do componente", GroupName = "Dados Básicos", Order = 2)]
        public string MiniCurriculo { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public DTOEquipe Equipe { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}