﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Emissora
{
    [TypeDisplay("Emissora")]
    [DisplayColumn("Nome")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Emissora")]
    public class DTOEmissoraRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Nomde da Emissora", GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }
    }
}