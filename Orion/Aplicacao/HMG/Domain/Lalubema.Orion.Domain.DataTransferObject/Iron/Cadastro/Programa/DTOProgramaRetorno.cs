﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Programa
{
    [DisplayColumn("Nome")]
    [TypeDisplay("Programa")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Programa")]
    public class DTOProgramaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do programa",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto do programa",
            GroupName = "Dados Básicos", Order = 2)]
        [Map("UrlFoto")]
        public string Foto { get; set; }

        [DataMember]
        [Display(Name = "Acessos", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Quantidade de acessos do programa", GroupName = "Dados Básicos", Order = 3)]
        public int QuantidadeAcesso { get; set; }

        [DataMember]
        [Display(Name = "Perfil", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Perfil do programa", GroupName = "Dados Básicos", Order = 4)]
        public virtual PerfilPrograma Perfil { get; set; }

        [DataMember]
        public IList<DTOConteudo> Conteudos { get; set; }

        [DataMember]
        public IList<DTOVideo> Videos { get; set; }
    }
}