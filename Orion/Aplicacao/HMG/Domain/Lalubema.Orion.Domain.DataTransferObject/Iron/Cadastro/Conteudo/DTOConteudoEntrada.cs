﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Conteudo
{
    [ObjectMap("IronConteudo")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Conteudo")]
    public class DTOConteudoEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Ordem", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem do conteúdo",
            GroupName = "Dados Básicos", Order = 1)]
        public int Ordem { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Tipo", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Tipo do conteúdo",
            GroupName = "Dados Básicos", Order = 2)]
        public int Tipo { get; set; }

        [DataMember]
        public byte[] Content { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public DTOPrograma Programa { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}