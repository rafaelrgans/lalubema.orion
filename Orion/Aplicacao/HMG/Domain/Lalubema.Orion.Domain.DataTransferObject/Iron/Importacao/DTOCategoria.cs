using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOCategoria
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}