using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOComponente
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string MiniCurriculo { get; set; }

        [DataMember]
        public string UrlFoto { get; set; }

        [DataMember]
        [Map("Equipe.Codigo")]
        public int EquipeID { get; set; }
    }
}