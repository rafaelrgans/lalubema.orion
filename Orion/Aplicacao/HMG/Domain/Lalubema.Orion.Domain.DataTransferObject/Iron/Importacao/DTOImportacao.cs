﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    public class DTOImportacao : DTORetorno
    {
        [DataMember]
        public IList<DTOPrograma> Programas { get; set; }

        [DataMember]
        public IList<DTOUsuario> Usuarios { get; set; }

        [DataMember]
        public IList<DTOCategoria> Categorias { get; set; }

        [DataMember]
        public IList<DTOConteudo> Conteudos { get; set; }

        [DataMember]
        public IList<DTOComponente> Componentes { get; set; }

        [DataMember]
        public IList<DTOEquipe> Equipes { get; set; }

        [DataMember]
        public IList<DTOVideo> Videos { get; set; }
    }
}