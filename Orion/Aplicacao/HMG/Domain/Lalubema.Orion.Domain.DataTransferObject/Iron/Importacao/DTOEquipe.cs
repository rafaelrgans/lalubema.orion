using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOEquipe
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string UrlFoto { get; set; }

        [DataMember]
        public string UrlFotoAltaResolucao { get; set; }

        [DataMember]
        [Map("Programa.Codigo")]
        public int ProgramaID { get; set; }
    }
}