﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    public class DTOExportarAcesso : DTOEntrada
    {
        [DataMember]
        public IList<DTOPrograma> Programas { get; set; }
    }
}