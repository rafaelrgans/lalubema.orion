﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem
{
    public class DTOFileRetorno : DTORetorno
    {
        public DTOFileRetorno(string filePath)
        {
            Path = filePath;
        }

        [DataMember]
        public string Path { get; private set; }
    }
}