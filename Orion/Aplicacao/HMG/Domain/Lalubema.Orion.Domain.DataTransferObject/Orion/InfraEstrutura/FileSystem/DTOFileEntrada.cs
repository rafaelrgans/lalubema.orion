﻿using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem
{
    public class DTOFileEntrada : DTOEntrada
    {
        public Aplicacao Aplicacao { get; set; }

        public string FileName { get; set; }

        public byte[] File { get; set; }
    }
}