﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Paging
{
    [DataContract(Namespace = "http://www.lalubema.com/Paging")]
    public class DTOPagingFilter : DTOEntrada
    {
        [DataMember]
        public int PageNumber { get; set; }

        [DataMember]
        public int LastItemCode { get; set; }
    }
}