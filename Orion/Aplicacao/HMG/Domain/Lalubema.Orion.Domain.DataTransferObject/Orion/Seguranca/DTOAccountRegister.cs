﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Seguranca")]
    public class DTOAccountRegister : DTOEntrada
    {
        [Required]
        [DataMember]
        public string Nome { get; set; }

        [Required]
        [DataMember]
        public string Email { get; set; }

        [Required]
        [DataMember]
        public string Senha { get; set; }

        [DataMember]
        public DateTime DataNascimento { get; set; }

        [DataMember]
        public int DDD { get; set; }

        [DataMember]
        public string Celular { get; set; }

        [DataMember]
        public string Cep { get; set; }
    }
}