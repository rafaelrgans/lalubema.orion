﻿using System.Collections.Generic;
using Lalubema.Utilitarios;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Notification
{
    public class DTOPushMessage
    {
        public string Bundle { get; set; }

        public string DeviceToken { get; set; }

        public PlatformType PlatformType { get; set; }

        public string Message { get; set; }

        public string LocalizedKey { get; set; }

        public List<object> LocalizedArgs { get; set; }

        public int Badge { get; set; }

        public string Sound { get; set; }
    }
}