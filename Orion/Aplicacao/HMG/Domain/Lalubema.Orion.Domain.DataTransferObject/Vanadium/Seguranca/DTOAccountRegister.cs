﻿using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Seguranca
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Vanadium/Seguranca")]
    public class DTOAccountRegister : Orion.Seguranca.DTOAccountRegister
    {
        [DataMember]
        public string TelefoneFixo { get; set; }

        public TipoUsuario UserType
        {
            get { return TipoUsuario.Cliente; }
        }
    }
}