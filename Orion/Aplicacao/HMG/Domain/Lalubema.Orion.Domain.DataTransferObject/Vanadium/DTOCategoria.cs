﻿using System.ComponentModel.DataAnnotations;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium
{
    [ObjectMap("VanadiumCategoria")]
    [ReturnType("Categorias", typeof (DTOCategorias), true)]
    public class DTOCategoria : DTOEntrada
    {
        [Key]
        public int Codigo { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }
    }
}