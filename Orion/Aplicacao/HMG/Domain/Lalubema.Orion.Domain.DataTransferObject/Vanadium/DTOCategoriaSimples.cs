﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium
{
    [Serializable]
    [DisplayColumn("Nome")]
    public class DTOCategoriaSimples
    {
        [DataMember]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da categoria",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }
    }
}