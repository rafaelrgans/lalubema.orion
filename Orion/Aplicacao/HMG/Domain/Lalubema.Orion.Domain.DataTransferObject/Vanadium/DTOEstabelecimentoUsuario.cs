﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium
{
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/")]
    [Serializable]
    public class DTOEstabelecimentoUsuario
    {
        [DataMember]
        public DTOEstabelecimento Estabelecimento { get; set; }

        [DataMember]
        public int TipoUsuario { get; set; }
    }
}