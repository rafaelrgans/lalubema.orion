﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
    public class DTOMesasAbertasRetorno : DTORetorno
    {
        public DTOMesasAbertasRetorno()
        {
            Mesas = new List<int>();
        }

        [DataMember]
        public IList<int> Mesas { get; set; }
    }
}