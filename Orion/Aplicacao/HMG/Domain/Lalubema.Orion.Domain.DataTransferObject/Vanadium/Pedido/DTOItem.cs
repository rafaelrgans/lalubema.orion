﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
    public class DTOItem
    {
        [DataMember]
        [Map("Item.Codigo")]
        public int Codigo { get; set; }

        [DataMember]
        public DateTime? Created { get; set; }

        [DataMember]
        public int Quantidade { get; set; }

        [DataMember]
        public int Status { get; set; }

		[DataMember]
		[Map("Item.Preco")]
		public decimal Preco { get; set; }

		[DataMember]
        public string Observacao { get; set; }
    }
}