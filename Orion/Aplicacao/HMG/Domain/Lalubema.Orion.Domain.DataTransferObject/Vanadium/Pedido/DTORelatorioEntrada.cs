﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
	public class DTORelatorioEntrada : DTOEntrada
	{
		[DataMember]
		public int CodEstabelecimento { get; set; }

		[DataMember]
		public int CodigoUsuario { get; set; }

		[DataMember]
		public int CodigoItem { get; set; }

		[DataMember]
		public int NumeroMesa { get; set; }

		[DataMember]
		public string NomeEstabelecimento { get; set; }

		[DataMember]
		public string NomeCliente { get; set; }

		[DataMember]
		public DateTime DtInicio { get; set; }

		[DataMember]
		public DateTime DtFim { get; set; }

		[DataMember]
		public TipoRelatorio ReportType { get; set; }
	}
}
