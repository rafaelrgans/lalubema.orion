﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
	public class DTORelatorioRetorno : DTORetorno
	{
		[DataMember]
		public TipoRelatorio TipoRelatorio { get; set; }

		[DataMember]
		public IList<DTORelatorio> Relatorio { get; set; }
	}
}
