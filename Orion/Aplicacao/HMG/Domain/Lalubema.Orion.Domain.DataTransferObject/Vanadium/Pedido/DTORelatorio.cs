﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/OrionServices/Vanadium/Pedido")]
	public class DTORelatorio
	{
		[DataMember]
		public int Codigo { get; set; }

		[DataMember]
		[Map("StatusPedido")]
		public StatusPedido Status { get; set; }

		[DataMember]
		[Map("CodigoMesa")]
		public int Mesa { get; set; }

		[DataMember]
		public decimal TotalVendido { get; set; }

		[DataMember]
		[Map("Estabelecimento.Nome")]
		public string NomeEstabelecimento { get; set; }

		[DataMember]
		[Map("Cliente.Codigo")]
		public int CodigoCliente { get; set; }

		[DataMember]
		[Map("Cliente.Nome")]
		public string NomeCliente { get; set; }

		[DataMember]
		[Map("Responsavel.Codigo")]
		public int CodigoGarcom { get; set; }

		[DataMember]
		[Map("Responsavel.Username")]
		public string NomeGarcom { get; set; }
	}
}
