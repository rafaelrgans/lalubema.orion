﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium
{
    public class DTOAutenticacaoPorUsuario : DTOAutenticacao
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string UltimoTipoTransacao { get; set; }

        [DataMember]
        public IList<DTOEstabelecimentoUsuario> Estabelecimentos { get; set; }

        [DataMember]
        [Map("UserType")]
        public TipoUsuario TipoUsuario { get; set; }
    }
}