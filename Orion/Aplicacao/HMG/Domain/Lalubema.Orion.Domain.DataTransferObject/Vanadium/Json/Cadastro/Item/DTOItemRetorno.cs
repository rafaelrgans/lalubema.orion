﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item
{
    [DisplayColumn("Nome")]
    public class DTOItemRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do item",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descrição do item", GroupName = "Dados Básicos", Order = 2)]
        public string Descricao { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Preço", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Preço do item", GroupName = "Dados Básicos", Order = 3)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DataType(DataType.Currency)]
        public decimal Preco { get; set; }

        [DataMember]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto do item",
            GroupName = "Dados Básicos", Order = 4)]
        [Map("UrlFoto")]
        public string Foto { get; set; }

        [DataMember]
        [Display(Name = "Destaque", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Item é destaque?",
            GroupName = "Dados Básicos", Order = 5)]
        public bool Destaque { get; set; }

        [DataMember]
        public DTOCategoria Categoria { get; set; }

        [DataMember]
        public DTOCardapio Cardapio { get; set; }
    }
}