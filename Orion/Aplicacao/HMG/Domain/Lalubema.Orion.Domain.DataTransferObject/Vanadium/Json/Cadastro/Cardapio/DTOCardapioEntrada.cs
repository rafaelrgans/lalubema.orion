﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio
{
    [ObjectMap("VanadiumCardapio")]
    [ReturnType(typeof (DTOCardapioRetorno))]
    [ComplexReturnType("Cardapios", typeof (DTOCardapios))]
    public class DTOCardapioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Descricao { get; set; }

        [DataMember]
        public IList<DTOItem> Itens { get; set; }
    }
}