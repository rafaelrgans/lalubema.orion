﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    [ObjectMap("VanadiumCategoria")]
    [ReturnType(typeof(DTOCategoriaRetorno))]
    [ComplexReturnType("Categorias", typeof(DTOCategorias))]
    public class DTOCategoriaEntrada : DTOEntrada
    {
        [DataMember]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataMember]
        [Required]
        public int Ordem { get; set; }
    }
}
