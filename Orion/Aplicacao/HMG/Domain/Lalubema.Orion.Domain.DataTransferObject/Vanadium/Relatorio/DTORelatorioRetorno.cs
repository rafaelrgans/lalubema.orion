﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Relatorio")]
	public class DTORelatorioRetorno : DTORetorno
	{
		[DataMember]
		public IList<DTORelatorio> Relatorio { get; set; }
	}
}
