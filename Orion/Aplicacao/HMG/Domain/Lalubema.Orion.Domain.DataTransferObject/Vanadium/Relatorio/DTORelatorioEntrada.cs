﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Relatorio")]
	public class DTORelatorioEntrada : DTOEntrada
	{
		[DataMember]
		public int CodEstabelecimento { get; set; }

		[DataMember]
		public DateTime DtInicio { get; set; }

		[DataMember]
		public DateTime DtFim { get; set; }

		[DataMember]
		public TipoRelatorio ReportType { get; set; }

	}
}
