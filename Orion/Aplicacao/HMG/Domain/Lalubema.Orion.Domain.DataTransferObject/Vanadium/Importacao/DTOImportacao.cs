﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Importacao")]
    public class DTOImportacao : DTORetorno
    {
        [DataMember]
        public DTOEstabelecimento Estabelecimento { get; set; }

        [DataMember]
        public IList<DTOCardapio> Cardapios { get; set; }

        [DataMember]
        public IList<DTOCategoria> Categorias { get; set; }

        [DataMember]
        public IList<DTOItem> Itens { get; set; }

        [DataMember]
        public IList<DTOUsuario> Usuarios { get; set; }
    }
}
