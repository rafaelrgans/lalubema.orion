﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Usuario
{
    [KnownType(typeof (DTOUsuario))]
    public class DTOUsuarios : DTORetorno
    {
        [DataMember]
        public IList<DTOUsuario> Usuarios { get; set; }
    }
}