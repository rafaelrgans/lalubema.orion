﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Usuario
{
    [ObjectMap("VanadiumUsuario")]
    [ReturnType(typeof (DTOUsuarioRetorno))]
    [ComplexReturnType("Usuarios", typeof (DTOUsuarios))]
    public class DTOUsuarioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento do usuário.")]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.Save)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento da senha.")]
        public string Senha { get; set; }

        [DataMember]
        public bool IsAdministrador { get; set; }

        [DataMember]
        public DateTime? DataAtivacao { get; set; }
    }
}