﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item
{
    [ObjectMap("VanadiumItem")]
    [ReturnType(typeof (DTOItemRetorno))]
    [ComplexReturnType("Itens", typeof (DTOItens))]
    public class DTOItemEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento do nome prato/bebida.")]
        [Display(Name = "Nome Prato/Bebida", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do prato/bebida",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento da descrição prato/bebida.")]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descrição do prato/bebida", GroupName = "Dados Básicos", Order = 2)]
        public string Descricao { get; set; }

        [DataMember]
        [Required(ErrorMessage = "É obrigatório o preenchimento do valor do prato/bebida.")]
        [Range(0, 999999, ErrorMessage = "Valor do prato/bebida inválido.")]
        [Display(Name = "Preço", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Preço do prato/bebida", GroupName = "Dados Básicos", Order = 3)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DataType(DataType.Currency)]
        public decimal Preco { get; set; }

        [DataMember]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto prato/bebida",
            GroupName = "Dados Básicos", Order = 4)]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        [Display(Name = "Destaque", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Item é destaque?",
            GroupName = "Dados Básicos", Order = 5)]
        public bool Destaque { get; set; }

        [DataMember]
        public DTOCategoria Categoria { get; set; }

        [DataMember]
        public DTOCardapio Cardapio { get; set; }
    }
}