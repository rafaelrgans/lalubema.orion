﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento
{
    [KnownType(typeof(DTOEstabelecimento))]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/")]
    public class DTOEstabelecimentos : DTORetorno
    {
        [DataMember]
        public IList<DTOEstabelecimento> Estabelecimentos { get; set; }
    }
}