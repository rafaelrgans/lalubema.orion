﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    [DisplayColumn("Nome")]
    public class DTOCategoriaRetorno : DTORetorno
    {
        [DataMember]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento do nome da seção no cardápio.")]
        [Display(Name = "Nome da seção no cardápio", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da seção no cardápio (Entradas, Pratos Principais, Bebidas, ...)",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento da ordem de apresentação da seção no cardápio.")]
        [Display(Name = "Ordem de apresentação da seção no cardápio", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem de apresentação da seção no cardápio (1, 2, 3, 4, ...)",
            GroupName = "Dados Básicos", Order = 2)]
        public int Ordem { get; set; }

        [ScaffoldColumn(false)]
        public IList<DTOItem> Itens { get; set; }
    }
}
