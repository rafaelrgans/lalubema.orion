﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    public class DTOCategorias : DTORetorno
    {
        [DataMember]
        public IList<DTOCategoria> Categorias { get; set; }
    }
}
