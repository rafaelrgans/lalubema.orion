﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOEquipe
    {
        [DataMember]
        public int EquipeID { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        [Map("Clube")]
        public int ClubeID { get; set; }
    }
}