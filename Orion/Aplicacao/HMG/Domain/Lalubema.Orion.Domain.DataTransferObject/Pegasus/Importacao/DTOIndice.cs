﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOIndice
    {
        [DataMember]
        public int IndiceID { get; set; }

        [DataMember]
        [Map("Prova")]
        public int ProvaID { get; set; }

        [DataMember]
        public string Tempo { get; set; }

        [DataMember]
        public string RotuloTempo { get; set; }
    }
}