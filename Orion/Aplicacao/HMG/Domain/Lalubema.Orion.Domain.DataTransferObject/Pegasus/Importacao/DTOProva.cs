﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOProva
    {
        [DataMember]
        public int ProvaID { get; set; }

        [DataMember]
        [Map("Agenda")]
        public int AgendaID { get; set; }

        [DataMember]
        [Map("Estilo")]
        public int EstiloID { get; set; }

        [DataMember]
        public int Serie { get; set; }

        [DataMember]
        public int Raia { get; set; }

        [DataMember]
        public int Distancia { get; set; }

        [DataMember]
        public int? Piscina { get; set; }

        [DataMember]
        public string Tempo { get; set; }

        [DataMember]
        public string Balizamento { get; set; }

        [DataMember]
        public int? NumeroProva { get; set; }

        [DataMember]
        public int? Classificacao { get; set; }
    }
}
