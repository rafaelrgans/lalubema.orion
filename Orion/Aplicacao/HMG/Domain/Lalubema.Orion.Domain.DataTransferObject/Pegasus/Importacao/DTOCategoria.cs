﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOCategoria
    {
        [DataMember]
        public int CategoriaID { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}