﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOAgenda
    {
        [DataMember]
        public int AgendaID { get; set; }

        [DataMember]
        public int UsuarioID { get; set; }

        [DataMember]
        public string Competicao { get; set; }

        [DataMember]
        public string Local { get; set; }

        [DataMember]
        [Map("DataInicio")]
        public DateTime DtAgenda { get; set; }

        [DataMember]
        [Map("DataFinal")]
        public DateTime DtFinalEvento { get; set; }

        [DataMember]
        public int? Piscina { get; set; }

        [DataMember]
        public bool Repetir { get; set; }
    }
}