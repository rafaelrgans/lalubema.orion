﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOEstilo
    {
        [DataMember]
        public int EstiloID { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}