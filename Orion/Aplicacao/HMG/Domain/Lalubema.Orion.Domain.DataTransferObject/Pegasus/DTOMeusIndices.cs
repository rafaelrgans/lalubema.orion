﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus
{
    [KnownType(typeof (DTOExportacao))]
    public abstract class DTOMeusIndices : DTOBase
    {
    }
}