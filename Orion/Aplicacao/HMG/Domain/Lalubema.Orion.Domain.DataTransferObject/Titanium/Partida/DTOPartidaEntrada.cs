﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Partida
{
    [ObjectMap("TitaniumPartida")]
    [DataContract(Namespace = "http://www.lalubema.com/Titanium/Partida")]
    public class DTOPartidaEntrada : DTOEntrada
    {
        [Filter(FilterTypeFlags.ReadAll)]
        public int CodigoTorneio { get; set; }

        [Filter(FilterTypeFlags.ReadAll)]
        public string EmailUsuario { get; set; }
    }
}