﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Partida
{
    [DataContract(Namespace = "http://www.lalubema.com/Titanium/Partida")]
    public class DTOPartidaRetorno : DTORetorno
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public DateTime Data { get; set; }

        [DataMember]
        public int  Status { get; set; }

        [DataMember]
        [Map("Rodada.NumeroRodada")]
        public int Rodada { get; set; }

        [DataMember]
        [Map("Quadra.Nome")]
        public string Quadra { get; set; }

        [DataMember]
        [Map("Quadra.Codigo")]
        public string CodigoQuadra { get; set; }

        [DataMember]
        public IList<DTOEquipe> Equipes { get; set; }
    }
}