﻿namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOParticipanteTorneio
    {
        public string Email { get; set; }

        public string Nome { get; set; }
    }
}