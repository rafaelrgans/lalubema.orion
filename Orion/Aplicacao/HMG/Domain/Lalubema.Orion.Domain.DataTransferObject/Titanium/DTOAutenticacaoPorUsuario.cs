﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOAutenticacaoPorUsuario : DTOAutenticacao
    {
        [DataMember]
        [Map("Username")]
        public string Email { get; set; }

        [DataMember]
        public string Nome { get; set; }
    }
}