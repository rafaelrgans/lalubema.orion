﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    [KnownType(typeof (DTOParticipanteTorneio))]
    [KnownType(typeof (DTOParticipanteTorneio))]
    public class DTOCadastroTorneio : DTOEntrada
    {
        public int Codigo { get; set; }

        [Required(ErrorMessage = @"O nome do torneio é obrigatório para realizar o cadastro.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = @"A data de início do torneio é obrigatório para realizar o cadastro.")]
        public DateTime Inicio { get; set; }

        [Required(ErrorMessage = @"A data de fim do torneio é obrigatório para realizar o cadastro.")]
        public DateTime Fim { get; set; }

        [Required(ErrorMessage = @"A duração da partida do torneio é obrigatório para realizar o cadastro.")]
        public DateTime DuracaoPartida { get; set; }

        [Required(ErrorMessage = @"A quantidade de jogador por equipe no torneio é obrigatório para realizar o cadastro.")]
        public int QuantidadeJogadorPorEquipe { get; set; }

        public IList<DTOParticipanteTorneio> Usuarios { get; set; }
    }
}