﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOConvite
    {
        [Key]
        public DTOTorneioRetorno Torneio { get; set; }

        [Required]
        public DTOAtleta Atleta { get; set; }

        [Required]
        [FilterUIHint("ForeignKey")]
        [Display(Order = 6, Name = "Atletas", GroupName = "Atletas", AutoGenerateFilter = true)]
        public IList<DTOAtleta> Atletas { get; set; }
    }
}