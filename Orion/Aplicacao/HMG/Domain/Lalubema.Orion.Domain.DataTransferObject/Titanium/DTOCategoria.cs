﻿using System.ComponentModel.DataAnnotations;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOCategoria
    {
        [Key]
        public int Codigo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
    }
}