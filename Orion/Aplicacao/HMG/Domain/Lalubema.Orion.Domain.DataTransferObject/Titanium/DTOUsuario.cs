﻿using System;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOUsuario
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        [Map("Username")]
        public string Email { get; set; }

        public int DDD { get; set; }

        public string Celular { get; set; }

        public string Cep { get; set; }

        public DateTime? DataNascimento { get; set; }
    }
}