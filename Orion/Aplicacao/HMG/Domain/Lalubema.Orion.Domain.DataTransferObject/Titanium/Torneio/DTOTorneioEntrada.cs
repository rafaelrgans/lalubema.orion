﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio
{
    [ObjectMap("TitaniumTorneio")]
    [DataContract(Namespace = "http://www.lalubema.com/Titanium/Torneio")]
    public class DTOTorneioEntrada : DTOEntrada
    {
        [Filter(FilterTypeFlags.ReadAll)]
        public string EmailUsuario { get; set; }
    }
}