﻿using System.Collections.Generic;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio
{
    public class DTOTorneioPorStatus : DTORetorno
    {
        public IList<DTOStatusTorneio> EmAndamento { get; set; }

        public IList<DTOStatusTorneio> Encerrado { get; set; }

        public IList<DTOStatusTorneio> Convidado { get; set; }

        public IList<DTOPartida> ProximasPartidas { get; set; }
    }
}