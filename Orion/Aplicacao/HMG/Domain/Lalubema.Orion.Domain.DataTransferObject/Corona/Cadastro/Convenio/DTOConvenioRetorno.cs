﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Convenio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Convenio")]
    public class DTOConvenioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [DataType(DataType.Date)]
        [Display(Name = "Início Validade", Description = "Data inicial de validade da promoção.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? InicioValidade { get; set; }

        [DataMember]
        [DataType(DataType.Date)]
        [Display(Name = "Fim Validade", Description = "Data final de validade da promoção.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? FimValidade { get; set; }

        [DataMember]
        [Required]
        [StringLength(150)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Observação", Description = "Descreva os procedimentos necessários para o benefício.")]
        public string Obs { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Telefone", Description = "Telefone do estabelecimento.")]
        public string Telefone { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.Url)]
        [Display(Name = "Site", Description = "Site do estabelecimento.")]
        public string Url { get; set; }

        [DataMember]
        [Required]
        [StringLength(150)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Benefício", Description = "Descreva os benefícios disponíveis.")]
        public string Beneficio { get; set; }

        [DataMember]
        [Display(Name = "Condomínio", Description = "Nome do condomínio.")]
        [Map("Condominio.Nome")]
        public string Condominio { get; set; }

        [DataMember]
        [Display(Name = "Foto Promocional", Description = "Foto da Promoção.")]
        [Map("UrlFotoPromocional")]
        public string FotoPromocional { get; set; }

        [DataMember]
        [Display(Name = "Logomarca", Description = "Logomarca do estabelecimento")]
        [Map("UrlLogomarca")]
        public string Logomarca { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string Estabelecimento { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string Endereco { get; set; }
    }
}