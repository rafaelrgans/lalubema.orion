﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.PessoaUnidadeHabitacional
{
    [ObjectMap("CoronaPessoaUnidadeHabitacional")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/PessoaUnidadeHabitacional")]
    public class DTOPessoaUnidadeHabitacionalEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }
    }
}