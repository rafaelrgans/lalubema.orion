﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.PessoaCondominio
{
    [ObjectMap("CoronaPessoaCondominio")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/PessoaCondominio")]
    public class DTOPessoaCondominioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Categoria { get; set; }

        [DataMember]
        public string Funcao { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}