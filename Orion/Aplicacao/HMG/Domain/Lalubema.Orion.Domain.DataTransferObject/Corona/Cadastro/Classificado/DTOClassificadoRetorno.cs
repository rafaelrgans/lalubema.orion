﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Classificado
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Classificado")]
    public class DTOClassificadoRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data de publicação",
            GroupName = "Dados Básico", Name = "Publicação", Order = 1, Prompt = "Informe a data de publicação",
            ShortName = "Dt. Pub.")]
        [DataMember]
        public DateTime? DataPublicacao { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data de expiração",
            GroupName = "Dados Básico", Name = "Expiração", Order = 2, Prompt = "Informe a data de expiração",
            ShortName = "Dt. Exp.")]
        [DataMember]
        public DateTime? DataExpiracao { get; set; }

        [DataMember]
        public string Item { get; set; }

        [FileExtensions(Extensions = new[] { "*.png", "*.jpg", "*.jpeg", "*.gif"})]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Foto do classificado",
            GroupName = "Dados Básico", Name = "Foto", Order = 2, Prompt = "Selecione a foto para upload.",
            ShortName = "Foto")]
        [DataMember]
        public string Foto { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string FotoFileName { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [Display(Name = "Preço", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Preço", GroupName = "Dados Básicos", Order = 4)]
        [DataMember]
        public decimal Preco { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Texto",
            GroupName = "Dados Básico", Name = "Texto", Order = 5, Prompt = "Informe o texto",
            ShortName = "Texto")]
        [DataMember]
        public string Texto { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public DTOPessoa Pessoa { get; set; }

        [DataMember]
        public DTOUnidadeHabitacional UnidadeHabitacional { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}