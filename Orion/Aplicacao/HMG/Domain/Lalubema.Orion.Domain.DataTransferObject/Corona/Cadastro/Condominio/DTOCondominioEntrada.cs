﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Condominio
{
    [ObjectMap("CoronaCondominio")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Condominio")]
    public class DTOCondominioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public int NumEdificios { get; set; }

        [DataMember]
        public string Foto { get; set; }

        [DataMember]
        public string Logradouro { get; set; }

        [DataMember]
        public string Bairro { get; set; }

        [DataMember]
        public string Cidade { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Cep { get; set; }

        [DataMember]
        public int CodigoSindico { get; set; }
    }
}
