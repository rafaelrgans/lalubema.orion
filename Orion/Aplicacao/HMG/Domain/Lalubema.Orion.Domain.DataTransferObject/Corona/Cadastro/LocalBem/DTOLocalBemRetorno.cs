﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/LocalBem")]
    public class DTOLocalBemRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public TipoLocalBem Tipo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public int AntecedenciaMaxReserva { get; set; }

        [DataMember]
        public int PrazoConfirmacao { get; set; }

        [DataMember]
        public int TempoPreparoUtilizacao { get; set; }

        [DataMember]
        public int? TempoMaximoUtilizacao { get; set; }

        [DataMember]
        public string Situacao { get; set; }

        [DataMember]
        public string Obs { get; set; }

        [DataMember]
        [Map("Foto")]
        public string FotoUrl { get; set; }

        [DataMember]
        [Map("Condominio.Codigo")]
        public int CodigoCondominio { get; set; }

        [DataMember]
        public IList<DTOEdificio> Edificios { get; set; }
    }
}