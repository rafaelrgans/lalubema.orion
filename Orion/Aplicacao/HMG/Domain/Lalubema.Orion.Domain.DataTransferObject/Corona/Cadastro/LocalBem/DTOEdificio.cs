﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/LocalBem")]
    public class DTOEdificio
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Endereco { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public int Unidades { get; set; }

        [DataMember]
        public int Andares { get; set; }

        [DataMember]
        public int UnidadesPorAndar { get; set; }

        [DataMember]
        public string FormNumUnidades { get; set; }

        [DataMember]
        public string Logradouro { get; set; }

        [DataMember]
        public string Bairro { get; set; }

        [DataMember]
        public string Cidade { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Cep { get; set; }
    }
}