﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Pessoa")]
    public class DTOPessoaAcesso
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Map("Pessoa.Codigo")]
        public int CodigoPessoa { get; set; }

        [DataMember]
        [Map("UnidadeHabitacional.Codigo")]
        public int CodigoUnidadeHabitacional { get; set; }

        [DataMember]
        public string Domingo { get; set; }

        [DataMember]
        public string Segunda { get; set; }

        [DataMember]
        public string Terca { get; set; }

        [DataMember]
        public string Quarta { get; set; }

        [DataMember]
        public string Quinta { get; set; }

        [DataMember]
        public string Sexta { get; set; }

        [DataMember]
        public string Sabado { get; set; }
    }
}
