﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa
{
    [ObjectMap("CoronaPessoa")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Pessoa")]
    public class DTOPessoaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string DocIdent { get; set; }

        [DataMember]
        public DateTime? DataNascimento { get; set; }

        [DataMember]
        public int Sexo { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public string Foto { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Senha { get; set; }

        [DataMember]
        public int TipoUsuario { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.Save)]
        public IList<DTOPessoaUnidadeHabitacional> UnidadesHabitacional { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.Save)]
        public IList<DTOPessoaCondominio> PessoaCondominios { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.Save)]
        public IList<DTOPessoaAcesso> Acessos { get; set; }

        [DataMember]
        public bool EnviarConvite { get; set; }
    }
}