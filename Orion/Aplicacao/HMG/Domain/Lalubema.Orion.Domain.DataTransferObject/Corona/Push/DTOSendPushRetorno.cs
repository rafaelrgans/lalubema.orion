﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOSendPushRetorno : DTORetorno
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public int? Origem { get; set; }

        [DataMember]
        public TipoNotificacao TipoNotificacao { get; set; }

        [DataMember]
        public StatusNotificacao? StatusNotificacao { get; set; }

        [DataMember]
        public int TempoChegada { get; set; }

        [DataMember]
        public string RemetenteDeviceToken { get; set; }

        [DataMember]
        public string Mensagem { get; set; }
    }
}