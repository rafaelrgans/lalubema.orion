﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOPushChegadaSegura : DTOEntrada
    {
        [DataMember]
        public StatusNotificacao StatusNotificacao { get; set; }

        [DataMember]
        public int CodigoNotificacao { get; set; }

        [DataMember]
        public string DeviceToken { get; set; }
    }
}