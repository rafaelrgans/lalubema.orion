﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Usuario")]
    public class DTOUnregisterDevice : DTOEntrada
    {
        [DataMember]
        public string DeviceToken { get; set; }

        [DataMember]
        public int Condominio { get; set; }

        [DataMember]
        public int Usuario { get; set; }
    }
}
