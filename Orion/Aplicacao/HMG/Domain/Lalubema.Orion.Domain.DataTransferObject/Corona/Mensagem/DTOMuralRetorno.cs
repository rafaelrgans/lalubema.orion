﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOMuralRetorno : DTORetorno
    {
        [DataMember]
        public DTOMensagem Mensagem { get; set; }

        [DataMember]
        public DTOClassificado Classificado { get; set; }

        [DataMember]
        public bool IsSindico { get; set; }
    }
}