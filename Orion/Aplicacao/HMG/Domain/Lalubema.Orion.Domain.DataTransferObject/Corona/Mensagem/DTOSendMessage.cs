﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOSendMessage : DTOEntrada
    {
        [DataMember]
        public string Texto { get; set; }

        [DataMember]
        public DateTime DataExpiracao { get; set; }

        [DataMember]
        public int UnidadeHabitacional { get; set; }

        [DataMember]
        public int Condominio { get; set; }

        [DataMember]
        public int[] Destinatarios { get; set; }

        [DataMember]
        public int[] Edificios { get; set; }

        [DataMember]
        public int[] UnidadesHabitacionais { get; set; }

        [DataMember]
        public int TipoMensagem { get; set; }

        [DataMember]
        public DateTime DataPublicacao { get; set; }
    }
}