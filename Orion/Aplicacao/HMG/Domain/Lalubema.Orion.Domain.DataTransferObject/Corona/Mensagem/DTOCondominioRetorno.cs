﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOCondominioRetorno : DTORetorno
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public IList<DTOEdificio> Edificios { get; set; }
    }
}