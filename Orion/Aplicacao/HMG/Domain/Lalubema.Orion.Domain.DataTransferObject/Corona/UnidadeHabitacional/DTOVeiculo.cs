﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOVeiculo
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public virtual int Tipo { get; set; }

        [DataMember]
        public virtual string Marca { get; set; }

        [DataMember]
        public virtual string Modelo { get; set; }

        [DataMember]
        public virtual string Cor { get; set; }

        [DataMember]
        public virtual string Placa { get; set; }

        [DataMember]
        public virtual string UrlFotoFrente { get; set; }

        [DataMember]
        public virtual string FotoFrente { get; set; }

        [DataMember]
        public virtual string UrlFotoTraseira { get; set; }

        [DataMember]
        public string FotoTraseira { get; set; }
    }
}