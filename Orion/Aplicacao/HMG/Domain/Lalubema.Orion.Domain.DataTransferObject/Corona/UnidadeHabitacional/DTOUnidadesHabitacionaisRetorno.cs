﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOUnidadesHabitacionaisRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTOUnidadeHabitacional> UnidadesHabitacionais { get; set; }
    }
}