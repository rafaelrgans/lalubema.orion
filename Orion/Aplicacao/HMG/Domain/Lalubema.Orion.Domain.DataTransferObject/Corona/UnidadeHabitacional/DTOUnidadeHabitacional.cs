﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOUnidadeHabitacional
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        public string Obs { get; set; }

        [DataMember]
        public int TipoUnidade { get; set; }

        [DataMember]
        public DTOEdificio Edificio { get; set; }

        [DataMember]
        public IList<DTOPessoa> Moradores { get; set; }

        [DataMember]
        public IList<DTOVeiculo> Veiculos { get; set; }
    }
}