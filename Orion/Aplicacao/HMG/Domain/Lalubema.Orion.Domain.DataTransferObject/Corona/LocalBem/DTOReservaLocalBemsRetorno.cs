﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem")]
    public class DTOReservaLocalBemsRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTOReservaLocalBem> ReservaLocalBems { get; set; }
    }
}