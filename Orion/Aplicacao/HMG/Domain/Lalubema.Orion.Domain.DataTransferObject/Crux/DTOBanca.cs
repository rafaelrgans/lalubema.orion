﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Crux
{
    [KnownType(typeof (DTOBanca))]
    public class DTOBanca : DTORetorno
    {
        public int Codigo { get; set; }

        public string Descricao { get; set; }

        public IList<DTOLivro> Livros { get; set; }
    }
}