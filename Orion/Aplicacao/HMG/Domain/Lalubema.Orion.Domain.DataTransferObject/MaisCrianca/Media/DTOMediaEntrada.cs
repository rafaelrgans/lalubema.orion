﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Media")]
    public class DTOMediaEntrada : DTOEntrada
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Legend { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public byte[] File { get; set; }

        [DataMember]
        public int MediaType { get; set; }
    }
}