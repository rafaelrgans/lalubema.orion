﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Media")]
    public class DTOPagingFilter : Orion.Paging.DTOPagingFilter
    {
        [DataMember]
        public int Type { get; set; }
    }
}