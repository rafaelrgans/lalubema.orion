﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Category")]
    public class DTOCategoryEntrada : DTOEntrada
    {
        [DataMember]
        public virtual int Codigo { get; set; }

        [DataMember]
        public virtual string Description { get; set; }
    }
}