﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Category")]
    public class DTOCategoryRetorno : DTORetorno
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}