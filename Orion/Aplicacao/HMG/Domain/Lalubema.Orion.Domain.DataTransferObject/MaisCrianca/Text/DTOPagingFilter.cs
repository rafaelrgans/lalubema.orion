﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Text")]
    public class DTOPagingFilter : Orion.Paging.DTOPagingFilter
    {
        [DataMember]
        public int Category { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int TeiaVida { get; set; }
    }
}