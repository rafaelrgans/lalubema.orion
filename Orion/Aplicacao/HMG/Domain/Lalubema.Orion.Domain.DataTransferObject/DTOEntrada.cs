﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;

namespace Lalubema.Orion.Domain.DataTransferObject
{
    [DataContract(Namespace = "http://www.lalubema.com/")]
    [KnownType(typeof (DTOCategoria))]
    public abstract class DTOEntrada : DTOBase
    {
        [DataMember]
        [ScaffoldColumn(false)]
        public string ChaveCriptografada { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public bool ReturnSaved { get; set; }
    }
}