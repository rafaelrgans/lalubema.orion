﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Album : DomainBase<Album, IAlbumRepositorio<Album>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual TipoAlbum Tipo { get; set; }

        public virtual string UrlCapa { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Capa { get; set; }

        public virtual DateTime Data { get; set; }

        public virtual Personalidade Personalidade { get; set; }

        public virtual int CodigoPersonalidade { get; set; }

        public virtual IList<Item> Itens { get; set; }

        public override void Save()
        {
            if (Personalidade == null || Personalidade.Codigo != CodigoPersonalidade)
                Personalidade = Personalidade.GetRepository().Get(CodigoPersonalidade);

            var oldUrl = UrlCapa;

            try
            {
                if (Capa != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Aquila, FileName, Capa);

                    UrlCapa = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, oldUrl);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Álbum", Nome, exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlCapa))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, UrlCapa);

            base.Delete();
        }
    }
}