﻿using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Item : DomainBase<Item, IItemRepositorio<Item>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        [Map("Conteudo")]
        public virtual string Url { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Media { get; set; }

        public virtual Album Album { get; set; }

        public virtual void Save(int codigoPersonalidade)
        {
            if (Album != null && Album.Codigo > 0)
                Album = Album.Get(Album.Codigo);
            else
                Album = null;

            if (Album == null)
                throw new FalhaAoSalvarException(Messages.AlbumInvalido);

            if(Album.Personalidade.Codigo != codigoPersonalidade)
                throw new FalhaAoSalvarException(Messages.PersonalidadeInvalida);

            var oldUrl = Url;

            try
            {
                if (Media != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Aquila, FileName, Media);

                    Url = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, oldUrl);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Item do Álbum", Nome, exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            Album.Itens.Remove(this);

            base.Save();
        }
    }
}