﻿using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Noticia : DomainBase<Noticia, INoticiaRepositorio<Noticia>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Mensagem { get; set; }

        public virtual DateTime Data { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Foto { get; set; }

        public virtual Personalidade Personalidade { get; set; }

        public virtual int CodigoPersonalidade { get; set; }

        public override void Save()
        {
            if (Personalidade == null || Personalidade.Codigo != CodigoPersonalidade)
                Personalidade = Personalidade.GetRepository().Get(CodigoPersonalidade);

            var oldUrl = UrlFoto;

            try
            {
                if (Foto != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Aquila, FileName, Foto);

                    UrlFoto = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, oldUrl);
                }

                if (FileName == "DELETE")
                {
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, oldUrl);

                    UrlFoto = null;
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Notícia", string.Empty, exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlFoto))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Aquila, UrlFoto);

            base.Delete();
        }
    }
}