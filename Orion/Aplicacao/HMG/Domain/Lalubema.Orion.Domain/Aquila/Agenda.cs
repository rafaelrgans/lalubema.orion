﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Agenda : DomainBase<Agenda, IAgendaRepositorio<Agenda>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual DateTime Data { get; set; }

        public virtual string Local { get; set; }

        public virtual Personalidade Personalidade { get; set; }

        public virtual int CodigoPersonalidade { get; set; }

        public override void Save()
        {
            if (Personalidade == null || Personalidade.Codigo != CodigoPersonalidade)
                Personalidade = Personalidade.GetRepository().Get(CodigoPersonalidade);

            base.Save();
        }
    }
}