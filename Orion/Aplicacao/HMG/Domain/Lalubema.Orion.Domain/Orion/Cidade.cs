﻿using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Orion
{
    public class Cidade : DomainBase<Cidade, ICidadeRepositorio<Cidade>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual Estado Estado { get; set; }
    }
}