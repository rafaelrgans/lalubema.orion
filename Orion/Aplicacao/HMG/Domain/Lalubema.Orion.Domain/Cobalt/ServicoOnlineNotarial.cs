﻿using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Utilitarios.Helper.Communication;
using System;

namespace Lalubema.Orion.Domain.Cobalt
{
    public class ServicoOnlineNotarial
    {
        public ServicoOnlineNotarial Solicitar(DTOServicoOnlineNotarialEntrada solicitacaoServicoNotarial)
        {
            var templateEmail = Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.ServicoOnlineNotarial.Template);
            var destinatario = Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.ServicoOnlineNotarial.Destinatario);

            var emailSettings = GetEmailSettings(destinatario, solicitacaoServicoNotarial.Email,
                                                           solicitacaoServicoNotarial.Nome);

            var assuntoEmail = string.Format(Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.ServicoOnlineNotarial.Assunto),
                                                solicitacaoServicoNotarial.TipoEscritura,
                                                solicitacaoServicoNotarial.TipoDocumento);

            EmailHelper.EnviarEmail(emailSettings, assuntoEmail,
                                    string.Format(templateEmail, solicitacaoServicoNotarial.Nome,
                                                  solicitacaoServicoNotarial.Telefone, solicitacaoServicoNotarial.Email,
                                                  solicitacaoServicoNotarial.TipoEscritura,
                                                  solicitacaoServicoNotarial.TipoDocumento));

            return this;
        }

        private static EmailSettings GetEmailSettings(string sendTo, string remetente, string nome)
        {
            return new EmailSettings
                       {
                           To = sendTo,
                           From = string.Format("{0} <{1}>", nome, remetente),
                           ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                           ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                           UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                           Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                           Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
                       };
        }
    }
}