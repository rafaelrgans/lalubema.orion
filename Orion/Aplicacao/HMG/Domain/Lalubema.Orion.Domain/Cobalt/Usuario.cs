﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Cobalt;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Exceptions.Orion;

namespace Lalubema.Orion.Domain.Cobalt
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public virtual int Codigo { get; set; }

        public virtual string Name { get; set; }

        public virtual string Password { get; set; }

        public virtual string Username { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual IList<Dica> Dicas { get; set; }

        public virtual Usuario ReenviarSenha(DTOReenviarSenhaEntrada reenviarSenha)
        {
            var templateEmail = Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.Usuario.Template);
            var assuntoEmail = Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.Usuario.Assunto);

            var emailSettings = GetEmailSettings(reenviarSenha.Email);

            EmailHelper.EnviarEmail(emailSettings, assuntoEmail,
                                    string.Format(templateEmail,
                                                  DateTime.UtcNow.ToBrasiliaTime().ToString("dd/MM/yyyy"),
                                                  DateTime.UtcNow.ToBrasiliaTime().ToString("HH:mm:ss"),
                                                  reenviarSenha.Email, reenviarSenha.Senha));

            return this;
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
                {
                    To = sendTo,
                    From = Parametro.BuscarPorChave(Constantes.Cobalt.Comunicacao.Remetente),
                    ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                    ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                    UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                    Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                    Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
                };
        }


        public virtual void Validar(string usuario, string senha, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Buscar(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            throw new NotImplementedException();
        }

        public virtual void RecoveryPassword(string username)
        {
            throw new NotImplementedException();
        }
    }
}