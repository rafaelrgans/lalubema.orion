﻿using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Categoria : DomainBase<Categoria, ICategoriaRepositorio<Categoria>>
    {
        protected Categoria(){}

        public Categoria(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Categoria(string nome)
        {
            Nome = nome;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public static Categoria Buscar(int codigo)
        {
            return GetRepository().Get(codigo);
        }
    }
}