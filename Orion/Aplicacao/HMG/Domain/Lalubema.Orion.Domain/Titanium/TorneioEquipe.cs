﻿using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class TorneioEquipe : DomainBase<TorneioEquipe, ITorneioEquipeRepositorio<TorneioEquipe>>
    {
        public virtual Torneio Torneio { get; set; }

        public virtual Equipe Equipe { get; set; }

        public virtual int Ordem { get; set; }

        #region [ Equality ]

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (TorneioEquipe)) return false;
            return Equals((TorneioEquipe) obj);
        }

        public virtual bool Equals(TorneioEquipe other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Torneio, Torneio) && Equals(other.Equipe, Equipe) && other.Ordem == Ordem;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Torneio != null ? Torneio.GetHashCode() : 0);
                result = (result * 397) ^ (Equipe != null ? Equipe.GetHashCode() : 0);
                result = (result * 397) ^ Ordem;
                return result;
            }
        }

        public static bool operator ==(TorneioEquipe left, TorneioEquipe right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TorneioEquipe left, TorneioEquipe right)
        {
            return !Equals(left, right);
        }

        #endregion [ Equality ]
    }
}