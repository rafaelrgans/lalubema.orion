﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public Usuario()
        {
        }

        public Usuario(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Usuario(string nome, string email)
        {
            Nome = nome;
            Username = email;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        [Map("Email")]
        public virtual string Username { get; set; }

        public virtual string Senha { get; set; }

        public virtual DateTime? DataNascimento { get; set; }

        public virtual int DDD { get; set; }

        public virtual string Celular { get; set; }

        public virtual string Cep { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual IList<TorneioUsuario> Torneios { get; set; }

        public virtual IList<Compromisso> Compromissos { get; set; }

        public virtual IList<Equipe> Equipes { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public override void Save()
        {
            if (Aplicacao == null)
                Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Titanium.ConvertToString());

            var usuarioNovo = Codigo == 0;

            if (usuarioNovo)
            {
                var usuario = Repository.BuscarUsuarioPorEmail(Username);

                if(usuario != null)
                    throw new UsuarioJaCadastradoException(Username);
            }

            if (usuarioNovo)
            {
                GerarSenha();

                Created = DateTime.Now;
            }

            base.Save();

            //if (usuarioNovo)
            //{
            //    EnviarConviteTorneios();

            //    if (!_conviteTorneio && !DataAtivacao.HasValue)
            //        EnviarEmailConfirmacao();
            //}
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            var u = Repository.Consultar(usuario, 6);

            if (u == null || !senha.Equals(u.Senha, StringComparison.InvariantCulture))
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            if (!u.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(usuario);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            var usuario = Repository.BuscarUsuarioPorEmail(userName);

            if (usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (usuario.DataAtivacao.HasValue)
                throw new UsuarioJaAtivadoException(userName);

            usuario.DataAtivacao = DateTime.Now;

            usuario.Save();

            return "<p>Usu&aacute;rio ativado com sucesso! Acesse a App e desfrute de todos os recursos dispon&iacute;veis.</p>";
        }

        public virtual void RecoveryPassword(string username)
        {
            throw new NotImplementedException();
        }

        private void EnviarConviteTorneios()
        {
            if (Torneios != null)
            {
                foreach (var torneio in
                    Torneios.Where(torneio => torneio.Status == Common.Tipos.StatusUsuarioTorneio.SemConvite))
                {
                    var linkDownloadAplicacao = Parametro.BuscarPorChave(Constantes.Titanium.Configuracao.ItunesUrl);
                    var assuntoConviteTorneio = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.AssuntoConviteTorneio);
                    var templateConvite = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.TemplateEmailConvite);

                    var emailSettings = GetEmailSettings(Username);

                    EmailHelper.EnviarEmail(emailSettings, String.Format(assuntoConviteTorneio, torneio.ResponsavelConvite.Nome, torneio.Torneio.Nome),
                                            string.Format(templateConvite, Nome, torneio.ResponsavelConvite.Nome, linkDownloadAplicacao));

                    torneio.DataValidadeToken = DateTime.Now.AddDays(3);
                    torneio.Status = Common.Tipos.StatusUsuarioTorneio.Convidado;
                }

                base.Save();
            }
        }

        private void EnviarEmailConfirmacao()
        {
            var chaveConfirmacao = SecurityHelper.CreateCryptographyKey(Username, Nome,
                                                                        DateTime.Now.ToString(
                                                                            CultureInfo.InvariantCulture),
                                                                        ((int) Common.Tipos.Aplicacao.Titanium).ToString
                                                                            (CultureInfo.InvariantCulture),
                                                                        Aplicacao.Versao);

            var linkConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.LinkConfirmacao);
            var assuntoConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.AssuntoEmailConfirmacao);
            var templateConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.TemplateEmailConfirmacao);

            var emailSettings = GetEmailSettings(Username);

            EmailHelper.EnviarEmail(emailSettings, assuntoConfirmacao, string.Format(templateConfirmacao, Nome, linkConfirmacao, chaveConfirmacao));
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Titanium.Comunicacao.Email.Remetente),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
            };
        }

        public virtual void ConvidarParaTorneio(Torneio torneio, Usuario responsavelConvite)
        {
            if (Torneios == null)
                Torneios = new List<TorneioUsuario>();

            if (Torneios.FirstOrDefault(torneioAtleta => torneioAtleta.Torneio.Codigo == torneio.Codigo) == null)
                Torneios.Add(new TorneioUsuario(torneio, this, responsavelConvite));

            Save();
        }

        private string GerarSenha()
        {
            string novaSenha = null;

            if (!DataAtivacao.HasValue)
            {
                novaSenha = SecurityHelper.CreateNumericPassword();

                Senha = SecurityHelper.EncryptToSHA1(novaSenha);

                DataAtivacao = null;

                base.Save();
            }

            return novaSenha;
        }
    }
}