﻿using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class PartidaEquipe : DomainBase<PartidaEquipe, IPartidaEquipeRepositorio<PartidaEquipe>>
    {
        public virtual Equipe Equipe { get; set; }

        public virtual Partida Partida { get; set; }

        public virtual int Resultado { get; set; }

        public virtual int Set { get; set; }

        #region [ Equality ]

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (PartidaEquipe)) return false;
            return Equals((PartidaEquipe) obj);
        }

        public virtual bool Equals(PartidaEquipe other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Equipe, Equipe) && Equals(other.Partida, Partida) && other.Resultado == Resultado && other.Set == Set;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Equipe != null ? Equipe.GetHashCode() : 0);
                result = (result*397) ^ (Partida != null ? Partida.GetHashCode() : 0);
                result = (result*397) ^ Resultado;
                result = (result*397) ^ Set;
                return result;
            }
        }

        public static bool operator ==(PartidaEquipe left, PartidaEquipe right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PartidaEquipe left, PartidaEquipe right)
        {
            return !Equals(left, right);
        }

        #endregion [ Equality ]
    }
}