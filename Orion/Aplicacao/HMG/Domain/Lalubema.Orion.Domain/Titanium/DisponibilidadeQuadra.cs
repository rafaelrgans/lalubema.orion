﻿using System;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class DisponibilidadeQuadra :
        DomainBase<DisponibilidadeQuadra, IDisponibilidadeQuadraRepositorio<DisponibilidadeQuadra>>
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime DataInicial { get; set; }

        public virtual DateTime DataFinal { get; set; }

        public virtual Common.Tipos.TipoOcorrencia TipoOcorrencia { get; set; }

        public virtual Quadra Quadra { get; set; }
    }
}