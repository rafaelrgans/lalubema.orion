﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Partida : DomainBase<Partida, IPartidaRepositorio<Partida>>
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime? Data { get; set; }

        public virtual Common.Tipos.StatusPartida Status { get; set; }

        public virtual Rodada Rodada { get; set; }

        public virtual Quadra Quadra { get; set; }

        public virtual IList<PartidaEquipe> Equipes { get; set; }
    }
}