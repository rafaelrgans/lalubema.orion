﻿using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Endereco : DomainBase<Endereco, IEnderecoRepositorio<Endereco>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Logradouro { get; set; }

        public virtual int Numero { get; set; }

        public virtual string Cep { get; set; }

        public virtual string Complemento { get; set; }

        public virtual string Bairro { get; set; }

        public virtual string Cidade { get; set; }

        public virtual string Estado { get; set; }

        public virtual string Pais { get; set; }
    }
}