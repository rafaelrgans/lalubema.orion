using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Security;

namespace Lalubema.Orion.Domain.Corona
{
    public class Edificio : DomainBase<Edificio, IEdificioRepositorio<Edificio>>, IDomainModel
    {
        public Edificio()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Telefone { get; set; }

        public virtual int Unidades { get; set; }

        public virtual int Andares { get; set; }

        public virtual int UnidadesPorAndar { get; set; }

        public virtual string FormNumUnidades { get; set; }

        public virtual string Logradouro { get; set; }

        public virtual string Bairro { get; set; }

        public virtual string Cidade { get; set; }

        public virtual string Estado { get; set; }

        public virtual string Cep { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual IList<UnidadeHabitacional> UnidadeHabitacional { get; set; }

        public virtual IList<LocalBem> AreasComum { get; set; }

        private void OnCreated()
        {
            UnidadeHabitacional = new List<UnidadeHabitacional>();
            AreasComum = new List<LocalBem>();
        }

        public virtual void AssociarLocalBens(DTOAssociarEdificioLocalBemEntrada edificioLocalBem)
        {
            AuthorizeRequestPorCondominio(edificioLocalBem);

            foreach (var localBem in edificioLocalBem.LocalBens)
            {
                if (localBem.Ativo)
                {
                    var localBemModel = new LocalBem().Get(localBem.Codigo);

                    if (localBemModel.Condominio.Codigo == Condominio.Codigo)
                    {
                        AreasComum.Add(localBemModel);

                        localBemModel.Edificios.Add(this);
                    }
                }
                else
                {
                    var localBemModel = AreasComum.FirstOrDefault(t => t.Codigo == localBem.Codigo);

                    if (localBemModel != null)
                    {
                        AreasComum.Remove(localBemModel);

                        localBemModel.Edificios.Remove(this);
                    }
                }
            }

            base.Save();
        }

        private void AuthorizeRequestPorCondominio(DTOEntrada edificioLocalBem)
        {
            var security = new ChaveCriptografadaHelper(edificioLocalBem.ChaveCriptografada);

            var isSindico = Condominio.IsSindico(security.CodigoUsuario);

            if (!isSindico)
                throw new UnauthorizedAccessException();
        }
    }
}