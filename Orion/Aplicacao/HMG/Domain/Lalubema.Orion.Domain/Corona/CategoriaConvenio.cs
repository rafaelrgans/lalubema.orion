﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class CategoriaConvenio : DomainBase<CategoriaConvenio, ICategoriaConvenioRepositorio<CategoriaConvenio>>, IDomainModel
    {
        private IList<Convenio> _convenios;

        public CategoriaConvenio()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual IList<Convenio> Convenios
        {
            get { return _convenios; }
            set { _convenios = value; }
        }

        private void OnCreated()
        {
            _convenios = new List<Convenio>();
        }
    }
}