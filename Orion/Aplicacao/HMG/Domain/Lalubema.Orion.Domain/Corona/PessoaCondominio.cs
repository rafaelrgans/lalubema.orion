using System;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Corona
{
    public class PessoaCondominio : DomainBase<PessoaCondominio, IPessoaCondominioRepositorio<PessoaCondominio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Categoria { get; set; }

        public virtual string Funcao { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual Usuario Pessoa { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual bool IsSindico()
        {
            return String.Equals(this.Categoria, "S", StringComparison.InvariantCultureIgnoreCase);
        }

        public override void Delete()
        {
            var protectedUsers = new string[] { "demonstracao@lalubema.com", "contato@lalubema.com" };
            if (protectedUsers.Any(w => w.Equals(Pessoa.Username, StringComparison.InvariantCultureIgnoreCase)))
                throw new InvalidOperationException();

            base.Delete();
        }

        /*
        public virtual PessoaCondominio EnviarConvite()
        {
            if (DataAtivacao.HasValue)
                return this;

            var senha = Pessoa.SenhaDescriptografada ?? "********";

            var assuntoConvite = Parametro.BuscarPorChave(Constantes.Corona.Condominio.AssuntoEmailConvite);
            var templateConvite = Parametro.BuscarPorChave(Constantes.Corona.Condominio.TemplateEmailConvite);

            var linkConfirmacao = Parametro.BuscarPorChave(Constantes.Corona.Condominio.LinkConfirmacao);

            var activationUrl = SecurityHelper.CreateCryptographyKey(Pessoa.Username, Pessoa.Nome,
                                                                        DateTime.UtcNow.ToString("o"),
                                                                        ((int)Common.Tipos.Aplicacao.Corona).ToString(
                                                                            CultureInfo.InvariantCulture),
                                                                        Pessoa.Aplicacao.Versao,
                                                                        Condominio.Codigo.ToString(CultureInfo.InvariantCulture));

            activationUrl = string.Format(linkConfirmacao, activationUrl);

            var googlePlayUrl = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.GooglePlayUrl);
            var itunesUrl = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.ItunesUrl);

            var condominio = Condominio.Nome;
            var sindico = Condominio.Funcionarios.FirstOrDefault(o => o.IsSindico());
            var nomeSindico = string.Empty;

            if (sindico != null)
                nomeSindico = sindico.Pessoa.Nome;

            EmailHelper.EnviarEmail(GetEmailSettings(Pessoa.Username), assuntoConvite,
                                    string.Format(templateConvite, Pessoa.Nome, nomeSindico, condominio, string.Empty,
                                                  string.Empty, Pessoa.Username, senha, googlePlayUrl, itunesUrl,
                                                  activationUrl));

            return this;
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
            };
        }
        */
    }
}