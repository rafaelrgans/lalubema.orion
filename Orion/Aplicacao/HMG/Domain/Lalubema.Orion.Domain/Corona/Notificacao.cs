﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class Notificacao : DomainBase<Notificacao, INotificacaoRepositorio<Notificacao>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Mensagem { get; set; }

        public virtual TipoNotificacao TipoMensagem { get; set; }

        public virtual Usuario Remetente { get; set; }

        public virtual Device RemetenteDevice { get; set; }

        public virtual int? Origem { get; set; }

        public virtual StatusNotificacao Status { get; set; }

        public virtual int TempoChegada { get; set; }

        public virtual DateTime DataNotificacao { get; set; }

        public virtual IList<Notificacao> ListRange(DateTime begin, DateTime end)
        {
            return Repository.ListAll(begin, end);
        }

        public virtual IList<Notificacao> ListRangeByRemetente(int remetente, DateTime begin, DateTime end)
        {
            return Repository.ListAllByRemetente(remetente, begin, end);
        }
    }
}
