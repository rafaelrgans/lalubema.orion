
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Exceptions.Base;
using System.Threading.Tasks;
using System.Threading;

namespace Lalubema.Orion.Domain.Corona
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        private IList<Device> _devices;

        private IList<AcessoPessoa> _acessos;

        private IList<AutorizacaoEntradaEntrega> _autorizacoes;

        private IList<ChegadaSegura> _chegadasSeguraMorador;

        private IList<ChegadaSegura> _chegadasSeguraPorteiro;

        private IList<Classificado> _classificados;

        private IList<Convenio> _convenios;

        private IList<Mensagem> _mensagensEnviadas;

        private IList<PessoaCondominio> _pessoaCondominios;

        private IList<ReservaLocalBem> _reservaLocalBems;

        private IList<ReservaLocalBem> _reservasAreaComum;

        private IList<PessoaUnidadeHabitacional> _unidadesHabitacional;

        private IList<Mensagem> _mensagensRecebidas;

        public Usuario()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        [Map("Email")]
        public virtual string Username { get; set; }

        [NoMap]
        public virtual string Senha { get; set; }

        [Map("Senha")]
        public virtual string NovaSenha { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual TipoUsuario TipoUsuario { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual string DocIdent { get; set; }

        public virtual DateTime? DataNascimento { get; set; }

        public virtual DateTime? Created { get; set; }

        public virtual Sexo Sexo { get; set; }

        public virtual string Telefone { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string Foto { get; set; }

        public virtual Usuario PessoaCadastro { get; set; }

        public virtual IList<Device> Devices
        {
            get { return _devices; }
            set { _devices = value; }
        }
            
        [NoMap]
        public virtual IList<AcessoPessoa> Acessos
        {
            get { return _acessos; }
            set { _acessos = value; }
        }

        public virtual IList<AutorizacaoEntradaEntrega> Autorizacoes
        {
            get { return _autorizacoes; }
            set { _autorizacoes = value; }
        }

        public virtual IList<ChegadaSegura> ChegadasSeguraMorador
        {
            get { return _chegadasSeguraMorador; }
            set { _chegadasSeguraMorador = value; }
        }

        public virtual IList<ChegadaSegura> ChegadasSeguraPorteiro
        {
            get { return _chegadasSeguraPorteiro; }
            set { _chegadasSeguraPorteiro = value; }
        }

        public virtual IList<Classificado> Classificados
        {
            get { return _classificados; }
            set { _classificados = value; }
        }

        public virtual IList<Convenio> Convenios
        {
            get { return _convenios; }
            set { _convenios = value; }
        }

        public virtual IList<Mensagem> MensagensEnviadas
        {
            get { return _mensagensEnviadas; }
            set { _mensagensEnviadas = value; }
        }

        [NoMap]
        public virtual IList<PessoaCondominio> PessoaCondominios
        {
            get { return _pessoaCondominios; }
            set { _pessoaCondominios = value; }
        }

        [NoMap]
        public virtual IList<PessoaUnidadeHabitacional> UnidadesHabitacional
        {
            get { return _unidadesHabitacional; }
            set { _unidadesHabitacional = value; }
        }

        public virtual IList<ReservaLocalBem> ReservasAreaComum
        {
            get { return _reservasAreaComum; }
            set { _reservasAreaComum = value; }
        }

        public virtual IList<ReservaLocalBem> ReservaLocalBems
        {
            get { return _reservaLocalBems; }
            set { _reservaLocalBems = value; }
        }

        public virtual IList<Mensagem> MensagensRecebidas
        {
            get { return _mensagensRecebidas; }
            set { _mensagensRecebidas = value; }
        }

        public virtual bool EnviarConvite { get; set; }

        private Usuario EnviarConviteAtivacao() {

            if (string.IsNullOrWhiteSpace(Username) || 
                (
                    (DataAtivacao.HasValue) && 
                    (CondominiosAdicionados == null || CondominiosAdicionados.Count <= 0) && 
                    (UnidadesAdicionadas == null || UnidadesAdicionadas.Count <= 0))
                )
                return this;

            var corporativo = Repository.Corporativo.HasValue ? Repository.Corporativo.Value : 1;

            List<String> prms = new List<string>();
            prms.Add(CryptographyType.User.ConvertToString());
            prms.Add(DateTime.Now.ToBrasiliaTime().ToString("o"));
            prms.Add(Codigo.ToString());
            prms.Add(Username);
            prms.Add(Aplicacao.Versao);
            prms.Add(((int)Common.Tipos.Aplicacao.Corona).ToString(CultureInfo.InvariantCulture));
            prms.Add(corporativo.ToString(CultureInfo.InvariantCulture));

            var chaveConvite = SecurityHelper.CreateCryptographyKey(prms.ToArray());

            var c = (corporativo == 1 ? "" : corporativo.ToString());

            var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LINK_EMAIL_ATIVACAO) + chaveConvite;
            var assuntoConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_EMAIL_ATIVACAO + c);
            var templateConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_EMAIL_ATIVACAO + c);
            var urlAppleStore = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.ItunesUrl + c);
            var urlGooglePlay = Parametro.BuscarPorChave(Constantes.Corona.Configuracao.GooglePlayUrl + c);

            var condominio = PessoaCondominios.Where(w => w.DataAtivacao.HasValue == false).Select(w => w.Condominio).FirstOrDefault();
            if (condominio == null)
                condominio = UnidadesHabitacional.Where(w => w.DataAtivacao.HasValue == false).Select(w => w.UnidadeHabitacional.Edificio.Condominio).FirstOrDefault();

            string local = "";
            if (condominio != null)
                local = string.Format(", no " + condominio.Nome + ",{0}{1}", 
                    string.IsNullOrWhiteSpace(condominio.Cidade) ? "" : " " + condominio.Cidade + ",",
                    string.IsNullOrWhiteSpace(condominio.Estado) ? "" : " " + condominio.Cidade + ","
                    );

            EmailHelper.EnviarEmail(GetEmailSettings(Username), assuntoConvite, string.Format(templateConvite, Nome, linkConvite, urlAppleStore, urlGooglePlay, local));

            return this;
        }

        public override void Delete()
        {
            var protectedUsers = new string[] { "demonstracao@lalubema.com", "contato@lalubema.com" };
            if(protectedUsers.Any(w => w.Equals(Username, StringComparison.InvariantCultureIgnoreCase)))
                throw new InvalidOperationException();

            base.Delete();
        }

        public virtual void Save(IList<DTOPessoaUnidadeHabitacional> unidadeHabitacionals,
                                 IList<DTOPessoaCondominio> pessoaCondominios,
                                 IList<DTOPessoaAcesso> acessos)
        {
            try
            {
                ValidarDadosUsuario()
                    .AdicionarUnidadesHabitacionais(unidadeHabitacionals)
                    .AdicionarCondominios(pessoaCondominios)
                    .AdicionarAcessos(acessos)
                    //.CriarSenha()
                    .SalvarFoto()
                    .SalvarUsuario()
                    .EnviarConviteAtivacao();
                    //.EnviarConviteUnidadeHabitacional()
                    //.EnviarConviteCondominio()
                    //.EnviarEmailAtivacao();
            }
            catch (BaseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            if (!SecurityHelper.isValidSHA1(senha))
                senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Get(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            if (!u.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(usuario);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            try
            {
                var usuario = Repository.Get(userName);

                if (usuario == null)
                    throw new UsuarioOuSenhaInvalidosException();

                if (usuario.DataAtivacao.HasValue)
                    throw new UsuarioJaAtivadoException(userName);

                usuario.DataAtivacao = DateTime.UtcNow.ToBrasiliaTime();

                if (usuario.UnidadesHabitacional != null && usuario.UnidadesHabitacional.Count > 0)
                {
                    var unidadeHabitacional =
                        usuario.UnidadesHabitacional.OrderByDescending(t => t.Created).ElementAt(0);

                    unidadeHabitacional.DataAtivacao = DateTime.UtcNow.ToBrasiliaTime();
                }

                usuario.Save();

                return Parametro.BuscarPorChave(Constantes.Corona.Usuario.TemplateAtivacao);
            }
            catch (Exception exception)
            {
                return "Falha na ativa��o: " + exception.Message;
            }
        }

        public virtual void RecoveryPassword(string username)
        {
            if (Aplicacao == null)
                Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString());

            var usuario = Repository.Get(username);

            if (usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!usuario.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(username);

            var corporativo = Repository.Corporativo.HasValue ? Repository.Corporativo.Value : 1;

            List<String> prms = new List<string>();
            prms.Add(CryptographyType.User.ConvertToString());
            prms.Add(DateTime.Now.ToBrasiliaTime().ToString("o"));
            prms.Add(usuario.Codigo.ToString());
            prms.Add(usuario.Username);
            prms.Add(usuario.Aplicacao.Versao);
            prms.Add(((int)Common.Tipos.Aplicacao.Corona).ToString(CultureInfo.InvariantCulture));
            prms.Add(corporativo.ToString(CultureInfo.InvariantCulture));

            var c = (corporativo == 1 ? "" : corporativo.ToString());

            var chaveConvite = SecurityHelper.CreateCryptographyKey(prms.ToArray());
            var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LINK_EMAIL_RECUPERAR_SENHA) + chaveConvite;
            var assuntoRecuperacaoSenha = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_EMAIL_RECUPERAR_SENHA + c);
            var templateRecuperacaoSenha = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_EMAIL_RECUPERAR_SENHA + c);

            EmailHelper.EnviarEmail(GetEmailSettings(username), assuntoRecuperacaoSenha, string.Format(templateRecuperacaoSenha, usuario.Nome, linkConvite));
        }

        private void OnCreated()
        {
            _devices = new List<Device>();
            _acessos = new List<AcessoPessoa>();
            _autorizacoes = new List<AutorizacaoEntradaEntrega>();
            _chegadasSeguraMorador = new List<ChegadaSegura>();
            _chegadasSeguraPorteiro = new List<ChegadaSegura>();
            _classificados = new List<Classificado>();
            _convenios = new List<Convenio>();
            _mensagensEnviadas = new List<Mensagem>();
            _pessoaCondominios = new List<PessoaCondominio>();
            _unidadesHabitacional = new List<PessoaUnidadeHabitacional>();
            _reservasAreaComum = new List<ReservaLocalBem>();
            _reservaLocalBems = new List<ReservaLocalBem>();
            _mensagensRecebidas = new List<Mensagem>();
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
            };
        }

        public virtual Condominio GetCondominio(int condominio)
        {
            var pessoaCondominio = PessoaCondominios.FirstOrDefault(t => t.Condominio.Codigo == condominio);

            if (pessoaCondominio == null)
                throw new ItemNotFoundException(condominio, "Condominio");

            return pessoaCondominio.Condominio;
        }

        public virtual DTOGetPush ListarNotificacao(int codigoCondominio)
        {
            // Condom�nio
            var condominio = new Condominio().Get(codigoCondominio);
            IEnumerable<UnidadeHabitacional> unidadesHabitacionais = null;

            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.UtcNow.ToBrasiliaTime().Subtract(this.DurationPush), DateTime.MaxValue).
                Where(w => w.Status != StatusNotificacao.Finalizado).
                OrderByDescending(w => w.DataNotificacao);

            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao notificacao in notificacoes)
            {
                // Define o formato da notifica��o
                switch (GetFormatoNotificacao(notificacao.TipoMensagem))
                {
                    case FormatoNotificacaoEnum.Chegada:
                        {
                            var unidades = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);
                            var veiculos = unidades.SelectMany(x => x.Veiculos);
                            if (unidades.Any(e => e.Codigo == notificacao.Origem.Value) || veiculos.Any(e => e.Codigo == notificacao.Origem.Value))
                            {
                                var chegada = GetNotificacaoChegadaArgs(notificacao);
                                result.Chegada.Add(chegada);
                            }

                            break;
                        }
                    case FormatoNotificacaoEnum.Portaria:
                        {
                            // Lazzy Pattern
                            if (unidadesHabitacionais == null)
                                unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                            // Filtro as Unidades Habitacionais do Condom�nio
                            if (unidadesHabitacionais.Any(e => e.Codigo == notificacao.Origem.Value))
                            {
                                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                                result.Portaria.Add(portaria);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Sindico:
                        {
                            if (notificacao.TipoMensagem == TipoNotificacao.SindicoPorMorador)
                            {
                                // Lazzy Pattern
                                if (unidadesHabitacionais == null)
                                    unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                                // Filtro as Unidades Habitacionais do Condom�nio
                                if (unidadesHabitacionais.Any(e => e.Codigo == notificacao.Origem.Value))
                                {
                                    var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            else
                            {
                                // Filtro as notifica��es do Condom�nio
                                if (notificacao.Origem.Value == condominio.Codigo)
                                {
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Resposta:
                        {
                            // Ser� retornado no GetReplyPush (filtro por usu�rio)
                            break;
                        }
                }
            }
            return result;
        }

        public virtual DTOGetPush ListarNotificacao()
        {
            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRangeByRemetente(Codigo, DateTime.UtcNow.ToBrasiliaTime().Subtract(this.DurationPush), DateTime.MaxValue).OrderByDescending(w => w.DataNotificacao);

            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao notificacao in notificacoes)
            {
                // Define o formato da notifica��o
                switch (GetFormatoNotificacao(notificacao.TipoMensagem))
                {
                    case FormatoNotificacaoEnum.Chegada:
                        {
                            var chegada = GetNotificacaoChegadaArgs(notificacao);
                            result.Chegada.Add(chegada);

                            break;
                        }
                    case FormatoNotificacaoEnum.Portaria:
                        {
                            var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                            var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                            result.Portaria.Add(portaria);

                            break;
                        }
                    case FormatoNotificacaoEnum.Sindico:
                        {
                            if (notificacao.TipoMensagem == TipoNotificacao.SindicoPorMorador)
                            {
                                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                                var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                                result.Sindico.Add(notificacaoSindico);
                            }
                            else
                            {
                                var condominio = new Condominio().Get(notificacao.Origem.Value);
                                var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                                result.Sindico.Add(notificacaoSindico);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Resposta:
                        {
                            // Ser� retornado no GetReplyPush (filtro por usu�rio)
                            break;
                        }
                }
            }
            return result;
        }

        public virtual DTOGetPush ListarNotificacaoResposta()
        {
            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.UtcNow.ToBrasiliaTime().Subtract(this.DurationPush), DateTime.MaxValue);

            // Percorre as notifica��o de Resposta (Reply)
            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao reply in notificacoes.Where(e => e.TipoMensagem == TipoNotificacao.Resposta))
            {
                var origem = new Notificacao().Get(reply.Origem.Value);

                // Filtro as notifica��es do Usu�rio
                if (origem.Remetente.Codigo == this.Codigo)
                {
                    var notificacaoResposta = GetNotificacaoRespostaArgs(origem, reply);
                    result.Resposta.Add(notificacaoResposta);
                }
            }

            return result;
        }

        /// <summary>
        /// Define o tempo que ser� considerado para busca das notifica��o (chegada/portaria) no servi�o GetPush
        /// </summary>
        private readonly TimeSpan DurationPush = new TimeSpan(1, 0, 0, 0);

        #region Notificar

        public virtual Notificacao RemoverNotificacao(int codigoNotificacao)
        {
            var notificacao = new Notificacao().Get(codigoNotificacao);

            if (notificacao == null)
                throw new ItemNotFoundException(codigoNotificacao, "Notificacao");

            // Salvar Notifica��o
            notificacao.Status = StatusNotificacao.Finalizado;
            notificacao.Save();

            return notificacao;
        }

        public virtual Notificacao Notificar(DTOSendPush sendPush)
        {
            switch (GetFormatoNotificacao(sendPush.TipoMensagem))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        return this.NotificarChegada(sendPush);
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        return this.NotificarPortaria(sendPush);
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        return this.NotificarSindico(sendPush);
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        return this.NotificarResposta(sendPush);
                    }
                default:
                    return null;
            }
        }

        private enum FormatoNotificacaoEnum { Chegada, Portaria, Sindico, Resposta }
        private FormatoNotificacaoEnum GetFormatoNotificacao(TipoNotificacao tipoNotificacao)
        {
            switch (tipoNotificacao)
            {
                case TipoNotificacao.Undefined:
                    throw new TipoMensagemInvalidaException();
                case TipoNotificacao.ChegadaSegura:
                case TipoNotificacao.ChegadaSeguraTaxi:
                case TipoNotificacao.ChegadaSeguraPe:
                    return FormatoNotificacaoEnum.Chegada;
                case TipoNotificacao.Servico:
                case TipoNotificacao.Visitante:
                case TipoNotificacao.Delivery:
                case TipoNotificacao.Outros:
                    return FormatoNotificacaoEnum.Portaria;
                case TipoNotificacao.SindicoPorFuncionario:
                case TipoNotificacao.SindicoPorMorador:
                    return FormatoNotificacaoEnum.Sindico;
                case TipoNotificacao.Resposta:
                    return FormatoNotificacaoEnum.Resposta;
                default:
                    throw new NotImplementedException();
            }
        }

        private Device GetDevice(string deviceToken)
        {
            if (!String.IsNullOrEmpty(deviceToken))
            {
                Device device = new Device().Get(deviceToken);
                return device;
            }
            else
                return null;
        }

        private Notificacao NotificarChegada(DTOSendPush sendPush)
        {
            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Mensagem,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Aguardando,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };

            // Envia para os devices do Condom�nio
            IEnumerable<Device> devices = null;
            Veiculo veiculo = null;
            if (notificacao.TipoMensagem == TipoNotificacao.ChegadaSegura)
            {
                veiculo = new Veiculo().Get(sendPush.Origem.Value);
                devices = veiculo.UnidadeHabitacional.Edificio.Condominio.Devices.Where(e => e.Usuario == null && e.Status == DeviceStatus.Active);
            }
            else if (notificacao.TipoMensagem == TipoNotificacao.ChegadaSeguraPe || notificacao.TipoMensagem == TipoNotificacao.ChegadaSeguraTaxi)
            {
                var uh = new UnidadeHabitacional().Get(sendPush.Origem.Value);
                devices = uh.Edificio.Condominio.Devices.Where(e => e.Usuario == null && e.Status == DeviceStatus.Active);
            }

            notificacao.Save();

            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "bell-ringing.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = LocalizedKey.Corona.ChegadaSegura,
                    LocalizedArgs = GetNotificacaoChegadaArgs(notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                if (device.APIVersion.HasValue && device.APIVersion.Value >= 1f)
                    pushMessage.LocalizedKey = notificacao.TipoMensagem == TipoNotificacao.ChegadaSegura ? LocalizedKey.Corona.ChegadaSegura : (notificacao.TipoMensagem == TipoNotificacao.ChegadaSeguraPe) ? LocalizedKey.Corona.ChegadaSeguraPe : LocalizedKey.Corona.ChegadaSeguraTaxi;

                try
                {
                    NotificationManager.SendPushMessage(pushMessage);

                    if (device.APIVersion.HasValue && device.APIVersion.Value >= 2f)
                        ChegadaSeguraTimers.Add(notificacao.Codigo.ToString() + "|" + device.Token, 
                            new Timer(x => ReenviarChegadaSegura(x), pushMessage, new TimeSpan(0, 1, 0), new TimeSpan(0, 1, 0)));
                }
                catch (Exception)
                {
                }
            }

            return notificacao;
        }

        private DTOGetPushChegadaArgs GetNotificacaoChegadaArgs(Notificacao notificacao)
        {
            DTOGetPushChegadaArgs result = new DTOGetPushChegadaArgs()
            {
                Codigo = notificacao.Codigo,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime(),
                TipoNotificacao = notificacao.TipoMensagem,
                Mensagem = notificacao.Mensagem,
                StatusNotificacao = notificacao.Status,
                Origem = notificacao.Origem,
            };

            if (notificacao.TipoMensagem == TipoNotificacao.ChegadaSegura)
            {
                try
                {
                    Veiculo veiculo = new Veiculo().Get(notificacao.Origem.Value);
                    result.EdificioNome = veiculo.UnidadeHabitacional.Edificio.Nome;
                    result.UnidadeHabitacionalNumero = veiculo.UnidadeHabitacional.Numero;
                    result.VeiculoMarca = veiculo.Marca;
                    result.VeiculoModelo = veiculo.Modelo;
                    result.VeiculoPlaca = veiculo.Placa;
                    result.VeiculoCor = veiculo.Cor;
                }
                catch (Exception)
                {
                }
            }
            else
            {
                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                result.EdificioNome = unidadeHabilitacional.Edificio.Nome;
                result.UnidadeHabitacionalNumero = unidadeHabilitacional.Numero;
            }

            return result;
        }

        private Notificacao NotificarPortaria(DTOSendPush sendPush)
        {
            var unidadeHabitacional =
                UnidadesHabitacional.FirstOrDefault(t => t.UnidadeHabitacional.Codigo == sendPush.Origem);

            if (unidadeHabitacional == null)
                throw new ItemNotFoundException(sendPush.Origem, "UnidadeHabitacional");

            var condominio = unidadeHabitacional.UnidadeHabitacional.Edificio.Condominio;

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Confirmado,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os devices do Condom�nio
            var devices = condominio.Devices.Where(e => e.Usuario == null && e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "doorbell.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = LocalizedKey.Corona.NotificarPortaria,
                    LocalizedArgs = GetNotificacaoPortariaArgs(unidadeHabitacional.UnidadeHabitacional, notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushPortariaArgs GetNotificacaoPortariaArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            DTOGetPushPortariaArgs result = new DTOGetPushPortariaArgs()
            {
                Codigo = notificacao.Codigo,
                UsuarioNome = notificacao.Remetente.Nome,
                EdificioNome = unidadeHabitacional.Edificio.Nome,
                UnidadeHabitacionalNumero = unidadeHabitacional.Numero,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime(),
                StatusNotificacao = notificacao.Status
            };
            return result;
        }

        private Notificacao NotificarSindico(DTOSendPush sendPush)
        {
            // Notifica��o do S�ndico oriundo Morador ou Funcion�rio
            Condominio condominio = null;
            UnidadeHabitacional unidadeHabitacional = null;
            if (sendPush.TipoMensagem == TipoNotificacao.SindicoPorMorador)
            {
                unidadeHabitacional = new UnidadeHabitacional().Get(sendPush.Origem.Value);
                if (unidadeHabitacional == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Unidade Habitacional");
                condominio = unidadeHabitacional.Edificio.Condominio;
            }
            else
            {
                condominio = new Condominio().Get(sendPush.Origem.Value);
                if (condominio == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Condom�nio");
            }

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Confirmado,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os Devices dos S�ndicos
            var sindicos = condominio.Funcionarios.Where(o => o.IsSindico());
            var devices = sindicos.SelectMany(x => x.Pessoa.Devices).Where(e => e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "bell-ringing.aiff",
                    DeviceToken = device.Token,
                    LocalizedKey = LocalizedKey.Corona.NotificarSindico,
                    LocalizedArgs = unidadeHabitacional != null ?
                                    GetNotificacaoSindicoArgs(unidadeHabitacional, notificacao).ToList() :
                                    GetNotificacaoSindicoArgs(condominio, notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(unidadeHabitacional.Codigo, unidadeHabitacional.Numero, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(Condominio condominio, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(condominio.Codigo, condominio.Nome, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(int codigo, string descricao, Notificacao notificacao)
        {
            DTOGetPushSindicoArgs result = new DTOGetPushSindicoArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = codigo,
                OrigemDescricao = descricao,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime()
            };
            return result;
        }

        private Notificacao NotificarResposta(DTOSendPush sendPush)
        {
            var origem = new Notificacao().Get(sendPush.Origem.Value);
            if (origem == null)
                throw new ItemNotFoundException(sendPush.Origem, "Notifica��o");

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Confirmado,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow.ToBrasiliaTime(),
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Altera o Status da Notifica��o de Origem
            origem.Status = StatusNotificacao.Respondido;
            origem.Save();

            // Envia para o Device da Notifica��o ou Devices do Remetente
            IEnumerable<Device> devices = origem.RemetenteDevice != null && origem.RemetenteDevice.Status == DeviceStatus.Active ?
                                          new List<Device>() { origem.RemetenteDevice } :
                                          origem.Remetente.Devices.Where(e => e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    Sound = "double-bell.aiff",
                    DeviceToken = device.Token,
                    Message = device.PlatformType == PlatformType.Android ? sendPush.Message : "",
                    LocalizedKey = LocalizedKey.Corona.NotificarResposta,
                    LocalizedArgs = GetNotificacaoRespostaArgs(origem, notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }

            return notificacao;
        }

        private DTOGetPushRespostaArgs GetNotificacaoRespostaArgs(Notificacao origem, Notificacao notificacao)
        {
            DTOGetPushRespostaArgs result = new DTOGetPushRespostaArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = origem.Codigo,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime()
            };
            return result;
        }

        private List<object> GetNotificacaoArgs(Notificacao notificacao)
        {
            // Define o formato da notifica��o
            switch (GetFormatoNotificacao(notificacao.TipoMensagem))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        var chegada = GetNotificacaoChegadaArgs(notificacao);
                        return chegada.ToList();
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                        var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                        return portaria.ToList();
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        if (notificacao.TipoMensagem == TipoNotificacao.SindicoPorMorador)
                        {
                            var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                            return notificacaoSindico.ToList();
                        }
                        else
                        {
                            var condominio = new Condominio().Get(notificacao.Origem.Value);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                            return notificacaoSindico.ToList();
                        }
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        var origem = new Notificacao().Get(notificacao.Origem.Value);
                        var resposta = GetNotificacaoRespostaArgs(origem, notificacao);
                        return resposta.ToList();
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        public virtual bool IsSindico(int unidadeHabitacional)
        {
            var u = new UnidadeHabitacional().Get(unidadeHabitacional);

            var codigoCondominio = u.Edificio.Condominio.Codigo;

            return PessoaCondominios.Any(t => t.Condominio.Codigo == codigoCondominio && t.IsSindico());
        }

        public virtual Usuario Get(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            return Get(security.CodigoUsuario);
        }

        #region [ Fluent ]

        public virtual string SenhaDescriptografada { get; set; }

        public virtual bool UsuarioNovo { get { return Codigo == 0; } }

        private bool FotoAlterada { get; set; }

        private IList<int> UnidadesAdicionadas { get; set; }

        private IList<int> CondominiosAdicionados { get; set; }

        /// <summary>
        /// Valida��o da unicidade do Usu�rio atrav�s das propriedades UserName (e-mail) e C�digo
        /// </summary>
        /// <remarks>
        /// O usu�rio ser� localizado para edi��o por C�digo (ID), quando n�o for informado por e-mail (username)
        /// Na edi��o, se houver altera��o de e-mail, validar colis�o com outros usu�rios existentes na base de dados
        /// </remarks>
        private Usuario ValidarDadosUsuario()
        {
            // Procura o usu�rio por e-mail (username)
            var usuario = String.IsNullOrEmpty(Username) ? null : Repository.Get(Username, Codigo);

            // N�o possui c�digo informado
            if (Codigo == 0)
            {
                // Pesquisa por e-mail (username) para edi��o
                if (usuario != null)
                {
                    Load(usuario.Codigo);
                }
            }
            else
            {
                // Valida se existe alguma colis�o de e-mail com outro usu�rio
                if (usuario != null && usuario.Codigo != Codigo)
                    throw new UsuarioJaCadastradoException(Username);
            }

            // Retorna a pr�pria inst�ncia
            return this;
        }
        /*
        private Usuario CriarSenha()
        {
            if (!DataAtivacao.HasValue && !string.IsNullOrWhiteSpace(Username))
            {
                SenhaDescriptografada = SecurityHelper.CreateNumericPassword();

                Senha = SecurityHelper.EncryptToSHA1(SenhaDescriptografada);
            }

            return this;
        }
        */
        private Usuario SalvarFoto()
        {
            var oldUrl = UrlFoto;
            string retornoPath = null;
            FotoAlterada = false;

            try
            {
                if (Foto != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(Foto),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, extension, fileHelper.File);

                        UrlFoto = retornoPath = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldUrl);

                    FotoAlterada = true;
                }
            }
            catch (Exception exception)
            {
                if (!string.IsNullOrWhiteSpace(retornoPath))
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, retornoPath);

                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }

            return this;
        }

        private Usuario AdicionarUnidadesHabitacionais(ICollection<DTOPessoaUnidadeHabitacional> unidadeHabitacionals)
        {
            if (unidadeHabitacionals == null || unidadeHabitacionals.Count == 0)
                return this;

            UnidadesAdicionadas = new List<int>();

            foreach (var unidadeHabitacional in unidadeHabitacionals)
            {
                var pessoaUnidadeHabitacional =
                    UnidadesHabitacional.FirstOrDefault(
                        t =>
                        t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == unidadeHabitacional.CodigoUnidadeHabitacional ||
                         t.Codigo == unidadeHabitacional.Codigo));

                if (pessoaUnidadeHabitacional == null)
                {
                    pessoaUnidadeHabitacional = new PessoaUnidadeHabitacional
                    {
                        UnidadeHabitacional =
                            new UnidadeHabitacional().Get(
                                unidadeHabitacional.CodigoUnidadeHabitacional),
                        Pessoa = this
                    };

                    UnidadesHabitacional.Add(pessoaUnidadeHabitacional);

                    UnidadesAdicionadas.Add(unidadeHabitacional.CodigoUnidadeHabitacional);
                }

                pessoaUnidadeHabitacional.Administrador = unidadeHabitacional.Administrador;
                pessoaUnidadeHabitacional.Categoria = unidadeHabitacional.Categoria;
                pessoaUnidadeHabitacional.PermissaoAvisos = unidadeHabitacional.PermissaoAvisos;
                pessoaUnidadeHabitacional.PermissaoChegadaSegura = unidadeHabitacional.PermissaoChegadaSegura;
                pessoaUnidadeHabitacional.PermissaoPessoas = unidadeHabitacional.PermissaoPessoas;
                pessoaUnidadeHabitacional.PermissaoReserva = unidadeHabitacional.PermissaoReserva;
                pessoaUnidadeHabitacional.PermissaoVeiculos = unidadeHabitacional.PermissaoVeiculos;
                pessoaUnidadeHabitacional.PermissaoClassificados = unidadeHabitacional.PermissaoClassificados;
                pessoaUnidadeHabitacional.PermissaoNotificacaoPortaria = unidadeHabitacional.PermissaoNotificacaoPortaria;
                pessoaUnidadeHabitacional.RelacaoFuncao = unidadeHabitacional.RelacaoFuncao;
                pessoaUnidadeHabitacional.Responsavel = unidadeHabitacional.Responsavel;
            }

            return this;
        }

        private Usuario AdicionarCondominios(ICollection<DTOPessoaCondominio> pessoaCondominios)
        {
            if (pessoaCondominios == null || pessoaCondominios.Count == 0)
                return this;

            CondominiosAdicionados = new List<int>();

            foreach (var condominio in pessoaCondominios)
            {
                var pessoaCondominio =
                    PessoaCondominios.FirstOrDefault(
                        t =>
                        t.Condominio != null &&
                        (t.Condominio.Codigo == condominio.CodigoCondominio || t.Codigo == condominio.Codigo));

                if (pessoaCondominio == null)
                {
                    pessoaCondominio = new PessoaCondominio
                    {
                        Condominio = new Condominio().Get(condominio.CodigoCondominio),
                        Pessoa = this
                    };

                    PessoaCondominios.Add(pessoaCondominio);

                    CondominiosAdicionados.Add(condominio.CodigoCondominio);
                }

                pessoaCondominio.Categoria = condominio.Categoria;
                pessoaCondominio.Funcao = condominio.Funcao;

                //Valida se o usuario esta sendo cadastrado como funcionario normal em um condominio em que ele ja seja sindico
                if(pessoaCondominio.IsSindico() == false && PessoaCondominios.Where(e => e.IsSindico() && e.Condominio.Codigo == pessoaCondominio.Condominio.Codigo).Count() > 0){
                    throw new UsuarioJaCadastradoException(Username);
                }
            }

            return this;
        }

        private Usuario AdicionarAcessos(ICollection<DTOPessoaAcesso> acessos)
        {
            if (acessos == null || acessos.Count == 0)
                return this;

            // Percorre os itens de acesso
            foreach (var acesso in acessos)
            {
                // Localiza o item a ser inclu�do/editado
                var pessoaAcesso =
                    Acessos.FirstOrDefault(
                        t =>
                        t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == acesso.CodigoUnidadeHabitacional || t.Codigo == acesso.Codigo));

                // Inclui novo acesso, se necess�rio
                if (pessoaAcesso == null)
                {
                    pessoaAcesso = new AcessoPessoa
                    {
                        UnidadeHabitacional = new UnidadeHabitacional().Get(acesso.CodigoUnidadeHabitacional),
                        Pessoa = this
                    };
                    Acessos.Add(pessoaAcesso);
                }

                // Atualiza os dados do Acesso
                pessoaAcesso.Domingo = acesso.Domingo;
                pessoaAcesso.Segunda = acesso.Segunda;
                pessoaAcesso.Terca = acesso.Terca;
                pessoaAcesso.Quarta = acesso.Quarta;
                pessoaAcesso.Quinta = acesso.Quinta;
                pessoaAcesso.Sexta = acesso.Sexta;
                pessoaAcesso.Sabado = acesso.Sabado;
            }

            // Retorna o pr�prio objeto
            return this;
        }

        private Usuario SalvarUsuario()
        {
            try
            {
                if (Aplicacao == null)
                    Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString());

                if (!string.IsNullOrWhiteSpace(NovaSenha))
                    if (!SecurityHelper.isValidSHA1(NovaSenha))
                        Senha = SecurityHelper.EncryptToSHA1(NovaSenha);
                    else
                        Senha = NovaSenha;

                base.Save(true);
            }
            catch (BaseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                if (FotoAlterada)
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, UrlFoto);

                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }

            return this;
        }

        /*
        private Usuario EnviarConviteUnidadeHabitacional()
        {
            if (string.IsNullOrWhiteSpace(Username))
                return this;

            if (UnidadesAdicionadas != null && UnidadesAdicionadas.Count > 0)
            {
                foreach (var codigoUnidadeHabitacional in UnidadesAdicionadas)
                {
                    var unidadeHabitacional =
                        UnidadesHabitacional.FirstOrDefault(
                            t => t.UnidadeHabitacional.Codigo == codigoUnidadeHabitacional);

                    if (unidadeHabitacional != null)
                        unidadeHabitacional.EnviarConvite();
                }
            }

            return this;
        }

        private Usuario EnviarConviteCondominio()
        {
            if (string.IsNullOrWhiteSpace(Username))
                return this;

            if (CondominiosAdicionados != null && CondominiosAdicionados.Count > 0)
            {
                foreach (var codigoCondominio in CondominiosAdicionados)
                {
                    var condominio =
                        PessoaCondominios.FirstOrDefault(
                            t => t.Condominio.Codigo == codigoCondominio);

                    if (condominio != null)
                        condominio.EnviarConvite();
                }
            }

            return this;
        }

        private void EnviarEmailAtivacao()
        {
            if (string.IsNullOrWhiteSpace(Username) || this.DataAtivacao.HasValue)
                return;

            var chaveConvite = SecurityHelper.CreateCryptographyKey(Username, Nome, DateTime.Now.ToString("o"), ((int)Common.Tipos.Aplicacao.Corona).ToString(CultureInfo.InvariantCulture), Aplicacao.Versao);

            var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LinkConfirmacao);
            var assuntoConvite = String.Empty;
            var templateConvite = String.Empty;

            if (this.TipoUsuario == Common.Tipos.TipoUsuario.Administrator)
            {
                assuntoConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.AssuntoEmailConvite);
                templateConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TemplateEmailConvite);
                EmailHelper.EnviarEmail(GetEmailSettings(Username), assuntoConvite, string.Format(templateConvite, SenhaDescriptografada, linkConvite, chaveConvite));
            }
            else if (this.TipoUsuario == Common.Tipos.TipoUsuario.User)
            {
                assuntoConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.AssuntoEmailConviteCondomino);
                templateConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TemplateEmailConviteCondomino);

                EmailHelper.EnviarEmail(GetEmailSettings(Username), assuntoConvite,
                                        string.Format(templateConvite, Nome, nomeSindico, condominio, edificio,
                                                      unidade.Numero, Username, SenhaDescriptografada, linkConvite, chaveConvite));
            }
        }
        */

        #endregion [ Fluent ]

        #region RegisterDevice

        private DTORegisterDevice RegisterDeviceData { get; set; }

        public virtual Usuario RegisterDevice(DTORegisterDevice registerDevice)
        {
            RegisterDeviceData = registerDevice;
            Device device;

            AuthorizeRequestByUser(registerDevice.ChaveCriptografada, registerDevice.Condominio)
                .LoadDevice(out device);

            device.APIVersion = registerDevice.APIVersion;

            return SetNotificationSettings(device)
                .SaveDevice(device);
        }

        public virtual Usuario UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            // Valida o acesso
            AuthorizeRequestByUser(unregisterDevice.ChaveCriptografada, unregisterDevice.Condominio);

            // seleciona o Device
            Device device = new Device().Repository.Get(unregisterDevice.DeviceToken);
            if (device == null)
                throw new ItemNotFoundException(unregisterDevice.DeviceToken, "Device");

            // Remove o Device
            device.Status = DeviceStatus.Inactive;
            device.Save();

            // Retona o resultado
            return this;
        }

        private Usuario SaveDevice(Device device)
        {
            Devices.Add(device);
            base.Save();
            return this;
        }

        private Usuario LoadDevice(out Device device)
        {
            device = new Device().Repository.Get(RegisterDeviceData.DeviceToken) ?? new Device
            {
                Name = RegisterDeviceData.Name,
                Token = RegisterDeviceData.DeviceToken,
                PlatformType = RegisterDeviceData.PlatformType
            };

            var condominio = new Condominio().Get(RegisterDeviceData.Condominio);
            device.Condominio = condominio;
            device.Usuario = this;
            device.Status = DeviceStatus.Active;

            return this;
        }

        private Usuario SetNotificationSettings(Device device)
        {
            var notificationSettings =
                Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString()).NotificationSettings ??
                new List<NotificationSettings>();

            var setting = notificationSettings.FirstOrDefault(t => t.Bundle.Equals(RegisterDeviceData.Bundle));

            if (setting == null)
                throw new ConfigurationException("NotificationSettings");

            device.NotificationSettings = setting;

            return this;
        }

        private Usuario AuthorizeRequestByUser(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            bool hasPermission;

            // Usu�rio de seguran�a pertence ao Condom�nio
            var usuario = new Usuario().Get(security.CodigoUsuario);
            hasPermission = usuario.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();

            // Usu�rio do Device pertence ao Condom�nio
            hasPermission = this.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();

            return this;
        }

        #endregion

        public virtual ChegadaSegura ChegadaSegura(DTOPushChegadaSegura entrada)
        {
            var notificacao = new Notificacao().Get(entrada.CodigoNotificacao);
            if (notificacao == null)
                throw new ItemNotFoundException(entrada.CodigoNotificacao, "Notifica��o");

            if (notificacao.Status == StatusNotificacao.Cancelado)
            {
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Notificacao.Codigo", notificacao.Codigo);
                Dictionary<string, string> order = new Dictionary<string, string>();
                order.Add("CreatedAt", "desc");
                return new ChegadaSegura().Repository.ListAll(filter, order).FirstOrDefault();
            }

            var chegada = new ChegadaSegura() {
                CreatedAt = DateTime.UtcNow.ToBrasiliaTime(),
                Status = entrada.StatusNotificacao,
                Solicitante = this,
                Notificacao = notificacao,
                Device = GetDevice(entrada.DeviceToken)
            };
            chegada.Save();

            var statusOrder = new[] { StatusNotificacao.Aguardando, StatusNotificacao.Entregue, StatusNotificacao.Visualizado, StatusNotificacao.Confirmado, StatusNotificacao.Cancelado, StatusNotificacao.Respondido }.ToList();
            if (statusOrder.FindIndex(w => w == notificacao.Status) < statusOrder.FindIndex(w => w == chegada.Status))
            {
                notificacao.Status = chegada.Status;
                notificacao.Save();
            }

            if (chegada.Status != StatusNotificacao.Aguardando)
            {
                var timerkey = notificacao.Codigo.ToString() + "|" + entrada.DeviceToken;
                Timer t = ChegadaSeguraTimers.FirstOrDefault(w => w.Key.Equals(timerkey, StringComparison.InvariantCultureIgnoreCase)).Value;
                if (t != null)
                {
                    ChegadaSeguraTimers.Remove(timerkey);
                    t.Dispose();
                }
            }

            // Envia para os devices do Condom�nio
            IList<Device> devices = null;
            if (notificacao.TipoMensagem == TipoNotificacao.ChegadaSegura)
            {
                Veiculo veiculo = new Veiculo().Get(notificacao.Origem.Value);
                devices = veiculo.UnidadeHabitacional.Edificio.Condominio.Devices;
            }
            else if (notificacao.TipoMensagem == TipoNotificacao.ChegadaSeguraPe || notificacao.TipoMensagem == TipoNotificacao.ChegadaSeguraTaxi)
            {
                var uh = new UnidadeHabitacional().Get(notificacao.Origem.Value);
                devices = uh.Edificio.Condominio.Devices;
            }

            if (devices == null)
                throw new ItemNotFoundException(0, "Devices");

            Usuario destinatario = chegada.Status != StatusNotificacao.Cancelado ? notificacao.Remetente : null;
            if (chegada.Status == StatusNotificacao.Confirmado || chegada.Status == StatusNotificacao.Visualizado)
                devices = devices.Where(w => !w.Token.Equals(chegada.Device.Token) && w.Status == DeviceStatus.Active && (w.Usuario == null || w.Usuario == notificacao.Remetente)).ToList();
            else
                devices = devices.Where(w => w.Status == DeviceStatus.Active && w.Usuario == destinatario).ToList();

            string key;
            switch(chegada.Status){
                case StatusNotificacao.Visualizado:
                    key = LocalizedKey.Corona.ChegadaSeguraStatusVisualized;
                    break;
                case StatusNotificacao.Confirmado:
                    key = LocalizedKey.Corona.ChegadaSeguraStatusConfirmed;
                    break;
                case StatusNotificacao.Entregue:
                    key = LocalizedKey.Corona.ChegadaSeguraStatusDelivered;
                    break;
                case StatusNotificacao.Cancelado:
                    key = LocalizedKey.Corona.ChegadaSeguraStatusCanceled;
                    break;
                default:
                    key = LocalizedKey.Corona.ChegadaSeguraStatus;
                    break;
            }

            foreach (var device in devices)
            {
                if (device.APIVersion.HasValue && device.APIVersion.Value >= 2f)
                {
                    var pushMessage = new DTOPushMessage
                    {
                        Bundle = device.NotificationSettings.Bundle,
                        Sound = "bell-ringing.aiff",
                        DeviceToken = device.Token,
                        LocalizedKey = key,
                        LocalizedArgs = GetNotificacaoChegadaArgs(notificacao).ToList(),
                        PlatformType = device.PlatformType
                    };

                    try
                    {
                        NotificationManager.SendPushMessage(pushMessage);
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return chegada;
        }

        private static void ReenviarChegadaSegura(object objState)
        {
            DTOPushMessage push = (DTOPushMessage)objState;
            int codigoNotificacao = Convert.ToInt32(push.LocalizedArgs[8]);
            var timerkey = codigoNotificacao + "|" + push.DeviceToken;
            Timer t = ChegadaSeguraTimers.FirstOrDefault(w => w.Key.Equals(timerkey, StringComparison.InvariantCultureIgnoreCase)).Value;
            if (t == null)
                return;

            try
            {
                var notificacao = new Notificacao().Get(codigoNotificacao);
                var device = new Device().Get(push.DeviceToken);
                if (device.Status != DeviceStatus.Active)
                    return;

                if (notificacao.Status == StatusNotificacao.Cancelado || notificacao.Status == StatusNotificacao.Finalizado || notificacao.Status == StatusNotificacao.Confirmado)
                {
                    foreach (var timer in ChegadaSeguraTimers.Where(w => w.Key.StartsWith(codigoNotificacao.ToString(), StringComparison.InvariantCultureIgnoreCase)).ToList())
                    {
                        timer.Value.Dispose();
                        ChegadaSeguraTimers.Remove(timer.Key);
                    }
                    return;
                }

                var d = DateTime.Parse(push.LocalizedArgs[4].ToString() + " " + push.LocalizedArgs[5].ToString(), new CultureInfo("pt-BR").DateTimeFormat);
                if (DateTime.UtcNow.AddDays(-1) >= d)
                {
                    ChegadaSeguraTimers.Remove(timerkey);
                    t.Dispose();
                    return;
                }

                NotificationManager.SendPushMessage((DTOPushMessage)objState);
            }
            catch (Exception ex)
            {
                ChegadaSeguraTimers.Remove(timerkey);
                t.Dispose();
                return;
            }
        }

        private static Dictionary<string, Timer> ChegadaSeguraTimers = new Dictionary<string, Timer>();
    }
}