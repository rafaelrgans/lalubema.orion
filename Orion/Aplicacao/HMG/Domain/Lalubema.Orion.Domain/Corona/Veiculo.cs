using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class Veiculo : DomainBase<Veiculo, IVeiculoRepositorio<Veiculo>>, IDomainModel
    {
        private IList<ChegadaSegura> _chegadasSegura;

        public Veiculo()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual TipoVeiculo Tipo { get; set; }

        public virtual string Marca { get; set; }

        public virtual string Modelo { get; set; }

        public virtual string Cor { get; set; }

        public virtual string Placa { get; set; }

        public virtual string UrlFotoFrente { get; set; }

        public virtual string FotoFrente { get; set; }

        public virtual string UrlFotoTraseira { get; set; }

        public virtual string FotoTraseira { get; set; }

        public virtual IList<ChegadaSegura> ChegadasSegura
        {
            get { return _chegadasSegura; }
            set { _chegadasSegura = value; }
        }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual int CodigoUnidadeHabitacional { get; set; }

        private void OnCreated()
        {
            _chegadasSegura = new List<ChegadaSegura>();
        }

        public override void Save()
        {
            if ((UnidadeHabitacional == null || UnidadeHabitacional.Codigo != CodigoUnidadeHabitacional) && CodigoUnidadeHabitacional > 0)
            {
                UnidadeHabitacional = new UnidadeHabitacional();
                UnidadeHabitacional = UnidadeHabitacional.Get(CodigoUnidadeHabitacional);
            }

            var oldUrlFrente = UrlFotoFrente;
            var oldUrlTraseira = UrlFotoTraseira;

            try
            {
                if (FotoFrente != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(FotoFrente),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Aplicacao.Corona, extension, fileHelper.File);

                        UrlFotoFrente = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrlFrente))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlFrente);
                }

                if (FotoTraseira != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                       Convert.FromBase64String(FotoTraseira),
                                       ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Aplicacao.Corona, extension, fileHelper.File);

                        UrlFotoTraseira = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrlTraseira))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlTraseira);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Ve�culo", Placa, exception);
            }

            base.Save();
        }
    }
}