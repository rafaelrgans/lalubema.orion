using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class AcessoPessoa : DomainBase<AcessoPessoa, IAcessoPessoaRepositorio<AcessoPessoa>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual int CodigoPessoa { get; set; }
        public virtual Usuario Pessoa { get; set; }

        public virtual int CodigoUnidadeHabitacional { get; set; }
        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual string Domingo { get; set; }
        public virtual string Segunda { get; set; }
        public virtual string Terca { get; set; }
        public virtual string Quarta { get; set; }
        public virtual string Quinta { get; set; }
        public virtual string Sexta { get; set; }
        public virtual string Sabado { get; set; }

        public override void Save()
        {
            // Pessoa (Usu�rio)
            if ((Pessoa == null || Pessoa.Codigo != CodigoPessoa) && CodigoPessoa > 0)
            {
                Pessoa = new Usuario();
                Pessoa = Pessoa.Get(CodigoPessoa);
            }

            // Unidade Habitacional
            if ((UnidadeHabitacional == null || UnidadeHabitacional.Codigo != CodigoUnidadeHabitacional) && CodigoUnidadeHabitacional > 0)
            {
                UnidadeHabitacional = new UnidadeHabitacional();
                UnidadeHabitacional = UnidadeHabitacional.Get(CodigoUnidadeHabitacional);
            }

            base.Save();
        }
    }
}