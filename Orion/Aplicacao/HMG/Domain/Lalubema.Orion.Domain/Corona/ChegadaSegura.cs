using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class ChegadaSegura : DomainBase<ChegadaSegura, IChegadaSeguraRepositorio<ChegadaSegura>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual StatusNotificacao Status { get; set; }

        public virtual DateTime CreatedAt { get; set; }

        public virtual Usuario Solicitante { get; set; }

        public virtual Device Device { get; set; }

        public virtual Notificacao Notificacao { get; set; }
    }
}