﻿using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class VeiculoCondominio : DomainBase<VeiculoCondominio, IVeiculoCondominioRepositorio<VeiculoCondominio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual TipoVeiculo Tipo { get; set; }

        public virtual string Marca { get; set; }

        public virtual string Modelo { get; set; }

        public virtual string Cor { get; set; }

        public virtual string Placa { get; set; }

        public virtual string UrlFotoFrente { get; set; }

        public virtual string FotoFrente { get; set; }

        public virtual string UrlFotoTraseira { get; set; }

        public virtual string FotoTraseira { get; set; }

        public virtual Condominio Condominio { get; set; }

        public override void Save()
        {
            var oldUrlFrente = UrlFotoFrente;
            var oldUrlTraseira = UrlFotoTraseira;

            try
            {
                if (FotoFrente != null)
                {
                    var retornoFrente = FileSystem.SaveFile(Aplicacao.Corona, "foto.jpg",
                                                            Convert.FromBase64String(FotoFrente));

                    UrlFotoFrente = retornoFrente.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrlFrente))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlFrente);
                }

                if (FotoTraseira != null)
                {
                    var retornoTraseira = FileSystem.SaveFile(Aplicacao.Corona, "foto.jpg",
                                                              Convert.FromBase64String(FotoTraseira));

                    UrlFotoTraseira = retornoTraseira.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrlTraseira))
                        FileSystem.DeleteFile(Aplicacao.Corona, oldUrlTraseira);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Veículo Condomínio", Placa, exception);
            }

            base.Save();
        }
    }
}