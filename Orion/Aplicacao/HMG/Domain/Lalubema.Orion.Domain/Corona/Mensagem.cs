using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class Mensagem : DomainBase<Mensagem, IMensagemRepositorio<Mensagem>>, IDomainModel
    {
        public Mensagem()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Destinos = new List<MensagemDestino>();
        }

        public virtual int Codigo { get; set; }

        public virtual TipoMensagem TipoMensagem { get; set; }

        public virtual DateTime DataPublicacao { get; set; }

        public virtual DateTime DataExpiracao { get; set; }

        public virtual string Texto { get; set; }

        public virtual StatusMensagem Status { get; set; }

        public virtual Usuario Remetente { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual IList<MensagemDestino> Destinos { get; set; }

        public virtual IList<Mensagem> ListarMensagensPublicadas(int codigoUsuario)
        {
            var mensagens = Repository.ListarMensagensPublicadas();
            var usuario = new Usuario().Get(codigoUsuario);

            return
                mensagens.Where(
                    mensagem =>
                    mensagem.Destinos.Any(
                        mensagemDestino =>
                        (mensagemDestino.Pessoa != null && mensagemDestino.Pessoa.Codigo == usuario.Codigo) ||
                        usuario.UnidadesHabitacional.Any(
                            unidadeHabitacional =>
                            (mensagemDestino.Condominio != null &&
                             unidadeHabitacional.UnidadeHabitacional.Edificio.Condominio.Codigo ==
                             mensagemDestino.Condominio.Codigo) ||
                            (mensagemDestino.Edificio != null &&
                             unidadeHabitacional.UnidadeHabitacional.Edificio.Codigo == mensagemDestino.Edificio.Codigo) ||
                            (mensagemDestino.UnidadeHabitacional != null &&
                             unidadeHabitacional.UnidadeHabitacional.Codigo ==
                             mensagemDestino.UnidadeHabitacional.Codigo)))).ToList();
        }

        public virtual Mensagem Send(DTOSendMessage sendMessage)
        {
            var security = new ChaveCriptografadaHelper(sendMessage.ChaveCriptografada);

            TipoMensagem = (TipoMensagem) sendMessage.TipoMensagem;
            DataPublicacao = sendMessage.DataPublicacao;
            DataExpiracao = sendMessage.DataExpiracao;
            Texto = sendMessage.Texto;
            Status = StatusMensagem.Publicada;
            
            Remetente = new Usuario().Get(security.CodigoUsuario);

            UnidadeHabitacional = sendMessage.UnidadeHabitacional > 0 ? new UnidadeHabitacional().Get(sendMessage.UnidadeHabitacional) : null;

            AdicionarPessoasDestinatarias(sendMessage)
                .AdicionarUnidadesHabitacionaisDestino(sendMessage)
                .AdicionarEdificiosDestino(sendMessage)
                .AdicionarCondominioDestino(sendMessage)
                .Save();

            return this;
        }

        private Mensagem AdicionarPessoasDestinatarias(DTOSendMessage sendMessage)
        {
            if (sendMessage.Destinatarios != null)
            {
                foreach (var destinatario in sendMessage.Destinatarios)
                {
                    Destinos.Add(new MensagemDestino
                    {
                        Mensagem = this,
                        Pessoa = new Usuario().Get(destinatario)
                    });
                }
            }

            return this;
        }

        private Mensagem AdicionarCondominioDestino(DTOSendMessage sendMessage)
        {
            if (sendMessage.Condominio > 0)
            {
                Destinos.Add(new MensagemDestino
                                 {
                                     Mensagem = this,
                                     Condominio = new Condominio().Get(sendMessage.Condominio)
                                 });
            }

            return this;
        }

        private Mensagem AdicionarEdificiosDestino(DTOSendMessage sendMessage)
        {
            if (sendMessage.Edificios != null)
            {
                foreach (var edificio in sendMessage.Edificios)
                {
                    Destinos.Add(new MensagemDestino
                                     {
                                         Mensagem = this,
                                         Edificio = new Edificio().Get(edificio)
                                     });
                }
            }

            return this;
        }

        private Mensagem AdicionarUnidadesHabitacionaisDestino(DTOSendMessage sendMessage)
        {
            if (sendMessage.UnidadesHabitacionais != null)
            {
                foreach (var unidadeHabitacional in sendMessage.UnidadesHabitacionais)
                {
                    Destinos.Add(new MensagemDestino
                                     {
                                         Mensagem = this,
                                         UnidadeHabitacional = new UnidadeHabitacional().Get(unidadeHabitacional)
                                     });
                }
            }

            return this;
        }
    }
}