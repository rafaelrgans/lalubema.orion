﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Vanadium;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;

namespace Lalubema.Orion.Domain.Vanadium
{
	public class Relatorio : DomainBase<Relatorio, IRelatorioRepositorio<Relatorio>>
	{
		private static readonly object SyncLock = new object();

		public virtual int CodEstabelecimento { get; set; }
		public virtual DateTime DtInicio { get; set; }
		public virtual DateTime DtFim { get; set; }
		public virtual TipoRelatorio ReportType { get; set; }

		public virtual DTORelatorioRetorno GerarRelatorio(string chaveCriptografada, int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, TipoRelatorio ReportType)
		{

			var relatorioModel = Relatorio.GetRepository().GerarRelatorio(codigoEstabelecimento, dtInicio, dtFim, ReportType);

			return new DTORelatorioRetorno
			{
				Relatorio = relatorioModel.TransformList<DTORelatorio>(true, null)
			};
		}


	}
}
