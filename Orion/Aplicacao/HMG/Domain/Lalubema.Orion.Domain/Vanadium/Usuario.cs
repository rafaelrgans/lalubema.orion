﻿using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Exceptions.Titanium.Atleta;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Base;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Types;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public Usuario()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Estabelecimentos = new List<UsuarioEstabelecimento>();
            Transactions = new List<Transaction>();
        }

        public virtual int Codigo { get; set; }

        [Map("Email")]
        public virtual string Username { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Senha { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual TipoUsuario UserType { get; set; }

        public virtual string Celular { get; set; }

        public virtual string TelefoneFixo { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual IList<UsuarioEstabelecimento> Estabelecimentos { get; set; }

        public virtual IList<Transaction> Transactions { get; set; }

        public virtual IList<Pedido> Pedidos { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public virtual bool IsAdministrador { get; set; }

        public virtual string UltimoTipoTransacao
        {
            get
            {
                if (Transactions != null)
                {
                    var transaction = Transactions.MaxBy(t => t.EndDate);

                    if(transaction != null)
                        return transaction.LicenseType.Description();
                }

                return LicenseType.Free.Description();
            }
        }

        public virtual LicenseType LicencaAtiva
        {
            get
            {
                if (Transactions != null)
                {
                    var transaction = Transactions.FirstOrDefault(t => t.Status == TransactionStatus.Active);

                    if (transaction != null)
                        return transaction.LicenseType;
                }

                return LicenseType.Free;
            }
        }

        public virtual void Save(string senha)
        {
            var usuarioNovo = Codigo == 0;

            if (usuarioNovo)
            {
                ValidarUsuarioJaCadastrado(Username);

                Transactions = new List<Transaction>
                                   {
                                       new Transaction
                                           {
                                               Created = DateTime.UtcNow,
                                               LicenseType = LicenseType.Free,
                                               OperationSystem = PlatformType.Undefined,
                                               OrderID = GuidHelper.GetGuidBase64String(),
                                               StartDate = DateTime.UtcNow,
                                               Responsavel = this
                                           }
                                   };
            }

            SetAplicacao()
                .SetSenha(senha, UserType)
                .AddEstabelecimento(IsAdministrador);

            base.Save();

            if (usuarioNovo && !DataAtivacao.HasValue)
                EnviarEmailConfirmacao();
        }

        public virtual void Register(DTOAccountRegister account)
        {
            ValidarUsuarioJaCadastrado(account.Email)
                .SetSenha(account.Senha, account.UserType)
                .SetAplicacao()
                .SetDadosUsuario(account)
                .Save();

            EnviarEmailConfirmacao();
        }

        private Usuario SetDadosUsuario(DTOAccountRegister account)
        {
            Nome = account.Nome;
            Username = account.Email;
            Celular = account.Celular;
            TelefoneFixo = account.TelefoneFixo;

            return this;
        }

        private Usuario ValidarUsuarioJaCadastrado(string email)
        {
            var usuario = Repository.BuscarUsuarioPorEmail(email);

            if (usuario != null)
                throw new UsuarioJaCadastradoException(email);

            return this;
        }

        private Usuario SetSenha(string senha, TipoUsuario userType)
        {
            if (string.IsNullOrWhiteSpace(senha) && string.IsNullOrWhiteSpace(Senha))
                throw new DadosInvalidosException("Senha");

            if (string.IsNullOrWhiteSpace(senha)) return this;

            switch (userType)
            {
                case TipoUsuario.Undefined:
                    break;
                case TipoUsuario.Administrator:
                    break;
                case TipoUsuario.User:
                    if (senha.Length != 4 || !senha.IsNumeric())
                        throw new DadosInvalidosException("Senha");

                    break;
                case TipoUsuario.Demo:
                    break;
                case TipoUsuario.Cliente:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Senha = SecurityHelper.EncryptToSHA1(senha);

            return this;
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Buscar(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            if (!u.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(usuario);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            var usuario = Repository.BuscarUsuarioPorEmail(userName);

            if (usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (usuario.DataAtivacao.HasValue)
                throw new UsuarioJaAtivadoException(userName);

            usuario.DataAtivacao = DateTime.Now;

            usuario.Save();

            return "true";
        }

        public virtual void RecoveryPassword(string username)
        {
            var usuario = Repository.BuscarUsuarioPorEmail(username);

            if(usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!usuario.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(username);

            var novaSenha = SecurityHelper.CreateNumericPassword();

            usuario.SetSenha(novaSenha, UserType).Save();

            var assuntoRecuperacaoSenha =
                Parametro.BuscarPorChave(Constantes.Vanadium.Usuario.Comunicacao.AssuntoEmailRecuperacaoSenha);
            var templateRecuperacaoSenha =
                Parametro.BuscarPorChave(Constantes.Vanadium.Usuario.Comunicacao.TemplateEmailRecuperacaoSenha);

            EmailHelper.EnviarEmail(GetEmailSettings(username), assuntoRecuperacaoSenha,
                                    string.Format(templateRecuperacaoSenha, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                                                  novaSenha));
        }

        public virtual void ChangePassword(string username, string senhaAtual, string novaSenha)
        {
            senhaAtual = SecurityHelper.EncryptToSHA1(senhaAtual);

            if (!senhaAtual.Equals(Senha))
                throw new UsuarioOuSenhaInvalidoException();

            Senha = SecurityHelper.EncryptToSHA1(novaSenha);

            Save();
        }

        public virtual void AddEstabelecimento(bool isAdmin)
        {
            if(Estabelecimento == null || UserType == TipoUsuario.Cliente)
                return;

            if (Estabelecimentos == null)
                Estabelecimentos = new List<UsuarioEstabelecimento>();

            var usuarioEstabelecimento = Estabelecimentos.FirstOrDefault(e => e.Estabelecimento.Codigo == Estabelecimento.Codigo);

            if(usuarioEstabelecimento == null)
            {
                usuarioEstabelecimento = new UsuarioEstabelecimento
                                             {
                                                 Usuario = this,
                                                 Estabelecimento = Estabelecimento
                                             };

                Estabelecimentos.Add(usuarioEstabelecimento);
            }

            usuarioEstabelecimento.TipoUsuario = isAdmin ? TipoUsuario.Administrator : TipoUsuario.User;
        }

        private void EnviarEmailConfirmacao()
        {
            var chaveConfirmacao = SecurityHelper.CreateCryptographyKey(Username, Nome,
                                                                        DateTime.UtcNow.ToString("o"),
                                                                        ((int) Common.Tipos.Aplicacao.Vanadium).ToString
                                                                            (CultureInfo.InvariantCulture),
                                                                        Aplicacao.Versao);

            var linkConfirmacao = Parametro.BuscarPorChave(Constantes.Vanadium.Usuario.Comunicacao.LinkConfirmacao);
            var assuntoConfirmacao = Parametro.BuscarPorChave(Constantes.Vanadium.Usuario.Comunicacao.AssuntoEmailConfirmacao);
            var templateConfirmacao = UserType == TipoUsuario.Cliente
                                          ? Parametro.BuscarPorChave(
                                              Constantes.Vanadium.Usuario.Comunicacao.TemplateEmailConfirmacaoCliente)
                                          : Parametro.BuscarPorChave(
                                              Constantes.Vanadium.Usuario.Comunicacao.TemplateEmailConfirmacao);

            EmailHelper.EnviarEmail(GetEmailSettings(Username), assuntoConfirmacao,
                                    string.Format(templateConfirmacao, linkConfirmacao, chaveConfirmacao));
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Vanadium.Comunicacao.Email.Remetente),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
            };
        }

        private Usuario SetAplicacao()
        {
            if (Aplicacao == null)
                Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Vanadium.ConvertToString());

            return this;
        }

        public virtual DTOHistoricoPedidos ListarHistoricoPedidos(int codigoEstabelecimento, DateTime dataInicio,
                                                                  DateTime dataFim)
        {
            ValidarIntervaloConsulta(codigoEstabelecimento, dataInicio, dataFim);

            var pedidosUsuario = Pedido.GetRepository()
                                       .ListarHistorioPedidosUsuario(Codigo, codigoEstabelecimento, dataInicio, dataFim);

            return new DTOHistoricoPedidos
                       {
                           Pedidos = pedidosUsuario.TransformList<DTOPedido>(true, null)
                       };
        }

        private static void ValidarIntervaloConsulta(int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim)
        {
            var intervaloConsulta = new DateTimeRange(dataInicio, dataFim);
            int maximoDiasHistoricoPedido;

            if (codigoEstabelecimento == 0)
            {
                int.TryParse(
                    Parametro.BuscarPorChave(Constantes.Vanadium.Pedido.SemEstabelecimentoMaximoDiasHistorico),
                    out maximoDiasHistoricoPedido);
            }
            else
            {
                int.TryParse(
                    Parametro.BuscarPorChave(Constantes.Vanadium.Pedido.ComEstabelecimentoMaximoDiasHistorico),
                    out maximoDiasHistoricoPedido);
            }

            var quantidadeDiasPeriodo = intervaloConsulta.End.Subtract(intervaloConsulta.Start).Days;

            if (quantidadeDiasPeriodo > maximoDiasHistoricoPedido)
                throw new InvalidDateRangeException();
        }
    }
}