﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Vanadium;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Pedido : DomainBase<Pedido, IPedidoRepositorio<Pedido>>
    {
        private static readonly object SyncLock = new object();

        public Pedido()
        {
            OnCreated();
        }

        public Pedido(int codigoMesa, int codigoEstabelecimento, int codigoUsuario)
        {
            OnCreated(codigoMesa, codigoEstabelecimento, codigoUsuario);
        }

        private void OnCreated()
        {
            StatusPedido = StatusPedido.Active;
            Itens = new List<ItemPedido>();
        }

        private void OnCreated(int codigoMesa, int codigoEstabelecimento, int codigoUsuario)
        {
            OnCreated();

            CodigoMesa = codigoMesa;

            if (codigoEstabelecimento <= 0)
                throw new ItemNotFoundException(codigoEstabelecimento, "Estabelecimento");

            Estabelecimento = new Estabelecimento().Get(codigoEstabelecimento);
			var UsuarioResponsavel = new Usuario().Get(codigoUsuario);
			if (UsuarioResponsavel.UserType == TipoUsuario.Cliente)
				Cliente = UsuarioResponsavel;
			else
				Responsavel = UsuarioResponsavel;
        }

        public virtual int Codigo { get; set; }

        public virtual int CodigoMesa { get; protected set; }

        public virtual StatusPedido StatusPedido { get; set; }

        public virtual DateTime? DataEncerramento { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public virtual Usuario Cliente { get; set; }

        public virtual Usuario Responsavel { get; set; }

        public virtual IList<ItemPedido> Itens { get; set; }

		public virtual decimal TotalVendido { get; set; }

        public virtual Pedido GetPedidoAberto(int codigoMesa, int codigoEstabelecimento)
        {
            Pedido pedido;

            lock (SyncLock)
                pedido = Repository.GetPedidoAberto(codigoMesa, codigoEstabelecimento);

            return pedido;
        }

		/// <summary>
		/// Retorna somente pedidos do cliente que fez a requisição (para ser usado com Meu Menu)
		/// </summary>
		/// <param name="codigoMesa"></param>
		/// <param name="codigoEstabelecimento"></param>
		/// <param name="CodCliente"></param>
		/// <returns></returns>
		public virtual Pedido GetPedidoAberto(int codigoMesa, int codigoEstabelecimento, int CodCliente)
		{
			Pedido pedido;

			lock (SyncLock)
				pedido = Repository.GetPedidoAberto(codigoMesa, codigoEstabelecimento, CodCliente);

			return pedido;
		}


		public virtual Pedido GetTransferirMesaPedido(int CodigoPedido, int codigoEstabelecimento)
		{
			Pedido pedido;

			lock (SyncLock)
				pedido = Repository.GetTransferirMesaPedido(CodigoPedido, codigoEstabelecimento);

			return pedido;
		}

		public virtual DTOHistoricoPedidos ListarPedidosAbertosMenu(int codigoMesa, int codigoEstabelecimento)
		{
			var pedidosUsuario = Pedido.GetRepository().ListarPedidosAbertosMenu(codigoMesa, codigoEstabelecimento);

			return new DTOHistoricoPedidos
			{
				Pedidos = pedidosUsuario.TransformList<DTOPedido>(true, null)
			};
		}

        public virtual Pedido AdicionarItens(DTOItem[] itens, bool hasClosing, int codigoUsuario)
        {
            if (itens == null || itens.Length == 0)
            {
                if (hasClosing)
                    return this;

                throw new DadosInvalidosException(string.Empty);
            }

            var usuario = new Usuario().Get(codigoUsuario);
			
            lock (SyncLock)
            {
				//var created = DateTime.UtcNow;
				var status = usuario.UserType == TipoUsuario.Cliente ? StatusPedido.Pending : StatusPedido.Active;
                var responsavel = usuario.UserType == TipoUsuario.Cliente ? null : usuario;

                foreach (var item in itens)
                {
					//created = created.AddMilliseconds(1);

                    var selectedItem = new Item().Get(item.Codigo);

                    if (selectedItem.Cardapio.Estabelecimento.Codigo != Estabelecimento.Codigo)
                        throw new ItemNotFoundException(item.Codigo, "ItemPedido");

					var dbItem = this.Itens.FirstOrDefault(e => e.Item.Codigo == item.Codigo && e.Created == item.Created);
					if (dbItem != null)
					{
						// Item já existe, altera o status se necessário
						dbItem.Quantidade = item.Quantidade;
						dbItem.Observacao = item.Observacao;
						if (dbItem.Status == StatusPedido.Pending)
							dbItem.Status = status;
					}
					else
					{
						// Adiciona novo Item com o Status correto
						this.Itens.Add(new ItemPedido
										  {
											  Item = selectedItem,
											  Quantidade = item.Quantidade,
											  Observacao = item.Observacao,
											  Pedido = this,
											  Created = (DateTime)item.Created,
											  Status = status,
											  CriadoPor = usuario,
											  Responsavel = responsavel
										  });
					}
                }

				if (usuario.UserType != TipoUsuario.Cliente)
					ComandarItens(itens, codigoUsuario);

                base.Save(true);
            }

            return this;
        }

        public virtual Pedido RemoverItens (DTOItem[] itens)
        {
            if (itens == null || itens.Length == 0)
                throw new DadosInvalidosException("Itens not selecteds");

            lock (SyncLock)
            {
                foreach (
                    var item in
                        itens.Select(
                            item =>
                            Itens.FirstOrDefault(t => t.Item.Codigo == item.Codigo && t.Created.Equals(item.Created)))
                             .Where(item => item != null))
                {
                    item.Status = StatusPedido.Deleted;
                }

                base.Save(true);
            }

            return this;
        }

        public virtual Pedido Fechar()
        {
            lock (SyncLock)
            {
                StatusPedido = StatusPedido.Closed;
                DataEncerramento = DateTime.UtcNow;

                base.Save(true);
            }

            return this;
        }

        public IList<Pedido> ListarPedidosAbertos(int codigoEstabelecimento)
        {
            return Repository.ListPedidosAbertos(codigoEstabelecimento);
        }

		public IList<Pedido> ListarPedidosFechadosRelatorio(int codigoEstabelecimento, int codigoUsuario, DateTime DtIncio, DateTime DtFim)
		{
			return Repository.ListPedidosFechadosRelatorio(codigoEstabelecimento, codigoUsuario, DtIncio, DtFim);
		}

        public virtual Pedido TrocarMesa(int novaMesa)
        {
            lock (SyncLock)
            {
                VerificarMesa(novaMesa);

                CodigoMesa = novaMesa;

                base.Save(true);
            }

            return this;
        }

        private void VerificarMesa(int novaMesa)
        {
            var pedido = GetPedidoAberto(novaMesa, Estabelecimento.Codigo);

            if(pedido != null)
                throw new MesaJaAbertaException(novaMesa);
        }

		public void BaixarItens(DTOItem[] itens, int codigoUsuario)
		{
			var usuario = new Usuario().Get(codigoUsuario);


			if (usuario.UserType == TipoUsuario.Cliente)
				throw new UnauthorizedAccessException();

			foreach (var itemPedido in from itemPedido in Itens
									   where itemPedido.Status != StatusPedido.Deleted
									   from item in
										   itens.Where(
											   item => item.Created.Equals(itemPedido.Created))
									   select itemPedido)
			{
				itemPedido.Status = StatusPedido.Closed;
			}
		}

        public void ComandarItens(DTOItem[] itens, int codigoUsuario)
        {
            var usuario = new Usuario().Get(codigoUsuario);


            if (usuario.UserType == TipoUsuario.Cliente)
                throw new UnauthorizedAccessException();

            foreach (var itemPedido in from itemPedido in Itens
                                              where itemPedido.Status == StatusPedido.Pending
                                              from item in
                                                  itens.Where(item => item.Created.Equals(itemPedido.Created) && item.Codigo == itemPedido.Item.Codigo)
                                              select itemPedido)
            {
                itemPedido.Status = StatusPedido.Active;
                itemPedido.Responsavel = usuario;
            }
        }

        public virtual IList<ItemPedido> ListarItensPendentes()
        {
            return Itens.Where(t => t.Status == StatusPedido.Pending).ToList();
        }

		//public virtual IList<ItemPedido> ListarItensPendentes()
		//{
		//    return Pedido.GetRepository().ListPedidosFechadosRelatorio();
		//}

		//public virtual DTORelatorioRetorno GerarRelatorio(string chaveCriptografada, int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, TipoRelatorio ReportType)
		//{
		//    IList<Pedido> relatorioModel;
		//    if (ReportType == TipoRelatorio.Garcom)
		//        relatorioModel = Pedido.GetRepository().GerarRelatorio(codigoEstabelecimento, dtInicio, dtFim, ReportType);
		//    else if(ReportType == TipoRelatorio.Mesa)
		//        relatorioModel = Pedido.GetRepository().GerarRelatorio(codigoEstabelecimento, dtInicio, dtFim, ReportType);
		//    else
		//        relatorioModel = Pedido.GetRepository().GerarRelatorio(codigoEstabelecimento, dtInicio, dtFim, ReportType);

		//    return new DTORelatorioRetorno
		//    {
		//        Relatorio = relatorioModel.TransformList<DTORelatorio>(true, null)
		//    };
			
		//}

    }
}