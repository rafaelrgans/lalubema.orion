﻿using System;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Titanium.Atleta;
using Lalubema.Orion.Common.Exceptions.Vanadium;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class TransactionManager
    {
        private static ITransactionRepositorio<Transaction> TransactionRepositorio
        {
            get { return ObjectFactory.GetSingleton<ITransactionRepositorio<Transaction>>(); }
        }

        public static DTORetorno DirectDebit(int codigoResponsavel, DTODirectDebitRequest directDebitRequest)
        {
            var responsavel = Usuario.GetRepository().Get(codigoResponsavel);

            if (responsavel == null)
                throw new UsuarioOuSenhaInvalidoException();

            var licenseType = directDebitRequest.LicenseType.ConvertoToEnum<LicenseType>();

            var transactions = TransactionRepositorio.ListAll(codigoResponsavel);

            if (transactions.FirstOrDefault(t => t.LicenseType == LicenseType.MensalFull) != null && licenseType != LicenseType.MensalFull)
                throw new InvalidLicenseTypeException();

            var startDate = DateTime.UtcNow.Date;

            if(transactions.Count != 0)
            {
                var lastTransaction = transactions.MaxBy(t => t.EndDate);

                if (lastTransaction.LicenseType == LicenseType.Free)
                {
                    lastTransaction.EndDate = DateTime.UtcNow.Date.AddDays(-1);
                    lastTransaction.Save();

                    if (lastTransaction.EndDate.HasValue)
                        startDate = lastTransaction.EndDate.Value.AddMinutes(1).Date;
                }
                else
                {
                    if (lastTransaction.EndDate.HasValue)
                        startDate = lastTransaction.EndDate.Value.AddDays(1).Date;
                }
            }

            var endDate = startDate.AddDays(DateTime.DaysInMonth(startDate.Year, startDate.Month)).Date;

            var createdTime = DateTime.SpecifyKind(new DateTime(1970, 1, 1), DateTimeKind.Utc);
            createdTime = createdTime.AddMilliseconds(directDebitRequest.PurchaseTime);

            var transaction = new Transaction
                                  {
                                      OrderID = directDebitRequest.OrderID,
                                      Created = createdTime,
                                      OperationSystem = (PlatformType)directDebitRequest.OperationSystem,
                                      Responsavel = responsavel,
                                      LicenseType = directDebitRequest.LicenseType.ConvertoToEnum<LicenseType>(),
                                      StartDate = startDate,
                                      EndDate = endDate
                                  };

            transaction.Save();

            return new DTORetorno();
        }

        public static LicenseType GetLicense(Estabelecimento estabelecimento)
        {
            if (estabelecimento == null)
                throw new ArgumentNullException("estabelecimento");

            var retorno = LicenseType.Free;

            foreach (
                var usuarioEstabelecimento in
                    estabelecimento.Usuarios.Where(
                                                   usuarioEstabelecimento =>
                                                   usuarioEstabelecimento.TipoUsuario == TipoUsuario.Administrator).
                        Where(
                              usuarioEstabelecimento =>
                              usuarioEstabelecimento.Usuario.LicencaAtiva.ToInt32() > retorno.ToInt32()))
            {
                retorno = usuarioEstabelecimento.Usuario.LicencaAtiva;
            }

            return retorno;
        }
    }
}