﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Cardapio : DomainBase<Cardapio, ICardapioRepositorio<Cardapio>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual IList<Item> Itens { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public override void Save()
        {
            Estabelecimento.LastUpdate = DateTime.UtcNow;

            base.Save();
        }
    }
}