﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Media : DomainBase<Media, IMediaRepositorio<Media>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Legend { get; set; }

        public virtual string Url { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual TipoAlbum MediaType { get; set; }

        public virtual Usuario Author { get; set; }

        public static IList<Media> ListMediaFeed(DTOPagingFilter pagingFilter)
        {
            var maxRecordsPerPageValue =
                Parametro.BuscarPorChave(Constantes.MaisCrianca.Configuration.MaxMediaRecordsPerPage);

            var maxRecordsPerPage = string.IsNullOrWhiteSpace(maxRecordsPerPageValue) ? 9999 : maxRecordsPerPageValue.ToInt32();

            IList<Media> listMediaFeed;

            try
            {
                var mediaType = (TipoAlbum) pagingFilter.Type;

                listMediaFeed = GetRepository().ListMediaFeed(pagingFilter.PageNumber, maxRecordsPerPage, mediaType);
            }
            catch (Exception)
            {
                listMediaFeed = new List<Media>();
            }

            return listMediaFeed;
        }
    }
}