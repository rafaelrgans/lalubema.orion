﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Text : DomainBase<Text, ITextRepositorio<Text>>, IDomainModel
    {
        private IList<Activity> _activities;
        private IList<Comment> _comments;
        private IList<Content> _contents;

        public Text()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Title { get; set; }

        public virtual string Content { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual TextStatus Status { get; set; }

        public virtual Usuario Author { get; protected set; }

        public virtual Category Category { get; set; }

        public virtual IList<Activity> Activities
        {
            get { return _activities; }
            set { _activities = value; }
        }

        public virtual IList<Comment> Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        [NoMap]
        public virtual IList<Content> Contents
        {
            get { return _contents; }
            set { _contents = value; }
        }

        public virtual int Likes
        {
            get { return Activities.Count(o => o.Like); }
        }

        private void OnCreated()
        {
            _activities = new List<Activity>();
            _comments = new List<Comment>();
            _contents = new List<Content>();
        }

        public virtual void Save(IList<DTOContent> contents)
        {
            if (Codigo == 0)
            {
                if (Status == TextStatus.Undefined)
                    Status = TextStatus.Pending;

                DefinirAutor();
            }

            AdicionarConteudos(contents);

            base.Save();
        }

        private void DefinirAutor()
        {
            if (Author == null) return;

            if (Author.Codigo != 0)
                Author = Usuario.GetRepository().Get(Author.Codigo);
            else
            {
                if (Author.UserType == TipoUsuario.Undefined)
                    Author.UserType = TipoUsuario.User;

                Author.Save();
            }
        }

        private void AdicionarConteudos(IEnumerable<DTOContent> contents)
        {
            foreach (var content in contents)
            {
                var contentModel = new Content
                    {
                        ContentType = content.ContentType,
                        Description = content.Description,
                        Text = this,
                        Url =
                            content.ContentType != TipoAlbum.Video
                                ? SalvarFoto(content.Content)
                                : GetYoutubeVideoUrl(content.Url)
                    };

                Contents.Add(contentModel);
            }
        }

        private static string GetYoutubeVideoUrl(string url)
        {
            var youtubeMatch = Regex.Match(url,
                                           @"(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^""&?\/ ]{11})");

            if (youtubeMatch.Success)
                return string.Format("http://www.youtube.com/embed/{0}", youtubeMatch.Groups[1].Value);

            throw new Utilitarios.Exceptions.Base.UriFormatException();
        }

        private string SalvarFoto(byte[] buffer)
        {
            string pathUrl;
            
            using (
                var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg, buffer,
                                                ComputerUnit.Mb.ToLong()))
            {
                fileHelper.ResizeImage()
                          .ChangeResolution()
                          .Validate();

                buffer = fileHelper.File;
                var extension = string.Format("*.{0}", fileHelper.Extension);

                var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.MaisCrianca, extension, buffer);
                pathUrl = retorno.Path;

                if (retorno.OcorreuErro)
                    throw new FalhaAoSalvarException("Texto", Title);
            }

            return pathUrl;
        }

        public virtual void Like()
        {
            if (Activities == null)
                Activities = new List<Activity>();

            Activities.Add(new Activity {Like = true, Text = this});

            base.Save();
        }

        public virtual DTOCommentRetorno Comment(DTOCommentEntrada comment)
        {
            if (Comments == null)
                Comments = new List<Comment>();

            var commentAuthor = Usuario.GetRepository().Get(comment.Author.Codigo) ??
                                    comment.Author.Transform<Usuario>();

            if (commentAuthor.UserType.ToInt32() == 0)
            {
                commentAuthor.UserType = TipoUsuario.User;

                commentAuthor.Save();
            }

            var commentModel = comment.Transform<Comment>();

            commentModel.Codigo = 0;
            commentModel.Author = commentAuthor;
            commentModel.Created = DateTime.UtcNow;
            commentModel.IsAdministrator = commentAuthor.UserType == TipoUsuario.Administrator;
            commentModel.Text = this;

            Comments.Add(commentModel);

            base.Save();

            return commentModel.Transform<DTOCommentRetorno>();
        }

        public static IList<Text> ListTextFeed(DTOPagingFilter pagingFilter)
        {
            var maxRecordsPerPageValue =
                Parametro.BuscarPorChave(Constantes.MaisCrianca.Configuration.MaxTextRecordsPerPage);

            var maxRecordsPerPage = string.IsNullOrWhiteSpace(maxRecordsPerPageValue)
                                        ? 9999
                                        : maxRecordsPerPageValue.ToInt32();

            SetTeiaVidaByCategory(pagingFilter);

            IList<Text> listTextFeed;

            try
            {
                listTextFeed = GetRepository()
                    .ListTextFeed(pagingFilter.PageNumber, maxRecordsPerPage, pagingFilter.Category,
                                  (TextStatus) pagingFilter.Status, pagingFilter.TeiaVida);
            }
            catch (Exception)
            {
                listTextFeed = new List<Text>();
            }

            return listTextFeed;
        }

        private static void SetTeiaVidaByCategory(DTOPagingFilter pagingFilter)
        {
            if (pagingFilter.Category != 0)
            {
                var category = new Category().Get(pagingFilter.Category);

                pagingFilter.TeiaVida = category.TeiaVida;
            }
        }

        public static IList<Text> ListTextTopFeed(DTOCategoryEntrada category)
        {
            var maxRecordsTopTextFeedValue =
                Parametro.BuscarPorChave(Constantes.MaisCrianca.Configuration.MaxRecordsTopTextFeed);

            var maxRecordsTopTextFeed = string.IsNullOrWhiteSpace(maxRecordsTopTextFeedValue)
                                            ? 5
                                            : maxRecordsTopTextFeedValue.ToInt32();

            IList<Text> listTextFeed;

            try
            {
                listTextFeed = GetRepository().ListTextTopFeed(maxRecordsTopTextFeed, category.Codigo);
            }
            catch (Exception)
            {
                listTextFeed = new List<Text>();
            }

            return listTextFeed;
        }

        public virtual void ChangeStatus(DTOTextEntrada text)
        {
            try
            {
                if (text.Status > 0 && Status != (TextStatus) text.Status)
                {
                    Status = (TextStatus) text.Status;

                    if (Status == TextStatus.Undefined)
                        throw new InvalidStatusException(text.Status.ToString(CultureInfo.InvariantCulture));
                }

                if (Status == TextStatus.Approved)
                {
                    if (text.Category > 0)
                    {
                        if (Category == null || Category.Codigo != text.Category)
                        {
                            Category = Category.GetRepository().Get(text.Category);
                        }
                    }

                    if (Category == null)
                        throw new DadosInvalidosException("Categoria informada inválida.");
                }

                if (!string.IsNullOrWhiteSpace(text.Title))
                    Title = text.Title;

                base.Save();
            }
            catch (InvalidStatusException)
            {
                throw;
            }
            catch (DadosInvalidosException)
            {
                throw;
            }
            catch
            {
                throw new DadosInvalidosException(string.Empty);
            }
        }
    }
}