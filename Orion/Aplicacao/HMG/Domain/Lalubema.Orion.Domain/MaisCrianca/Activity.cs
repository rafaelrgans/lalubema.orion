﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Activity : DomainBase<Activity, IActivityRepositorio<Activity>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual string Ip { get; set; }

        public virtual bool Like { get; set; }

        public virtual Text Text { get; set; }
    }
}