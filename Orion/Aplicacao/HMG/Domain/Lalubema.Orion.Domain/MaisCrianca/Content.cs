﻿using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Content : DomainBase<Content, IContentRepositorio<Content>>, IDomainModel
    {
        public Content()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            ContentType = TipoAlbum.Foto;
        }

        public virtual int Codigo { get; set; }

        public virtual string Url { get; set; }

        public virtual string Description { get; set; }

        public virtual TipoAlbum ContentType { get; set; }

        public virtual Text Text { get; set; }
    }
}