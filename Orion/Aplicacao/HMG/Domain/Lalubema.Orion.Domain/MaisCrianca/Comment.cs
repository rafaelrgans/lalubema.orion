﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Comment : DomainBase<Comment, ICommentRepositorio<Comment>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Description { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual bool IsAdministrator { get; set; }

        public virtual Text Text { get; set; }

        public virtual Usuario Author { get; set; }
    }
}