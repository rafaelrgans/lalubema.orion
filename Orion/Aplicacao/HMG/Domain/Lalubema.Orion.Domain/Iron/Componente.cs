using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Iron
{
    public class Componente : DomainBase<Componente, IComponenteRepositorio<Componente>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string MiniCurriculo { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Foto { get; set; }

        public virtual Equipe Equipe { get; set; }

        public virtual void Save(int codigoEmissora)
        {
            if (Equipe == null || Equipe.Codigo == 0)
                throw new DadosInvalidosException("Equipe selecionado inv�lido");

            Equipe.Load(Equipe.Codigo);

            if (Equipe.Programa == null || Equipe.Programa.Emissora == null || Equipe.Programa.Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            Save();
        }

        public override void Save()
        {
            if (Equipe != null && Equipe.Codigo > 0)
                Equipe = Equipe.Get(Equipe.Codigo);
            else
                Equipe = null;

            var oldUrl = UrlFoto;

            try
            {
                if (Foto != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Iron, FileName, Foto);

                    UrlFoto = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, oldUrl);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Componente", Nome, exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlFoto))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, UrlFoto);

            base.Delete();
        }
    }
}