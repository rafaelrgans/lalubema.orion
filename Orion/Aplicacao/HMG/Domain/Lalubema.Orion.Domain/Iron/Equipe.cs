using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Iron
{
    public class Equipe : DomainBase<Equipe, IEquipeRepositorio<Equipe>>, IDomainModel
    {
        public Equipe()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Componentes = new List<Componente>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Foto { get; set; }

        public virtual string UrlFotoAltaResolucao { get; set; }

        public virtual string FileNameAltaResolucao { get; set; }

        public virtual byte[] FotoAltaResolucao { get; set; }

        public virtual IList<Componente> Componentes { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual void Save(int codigoEmissora)
        {
            if (Programa == null || Programa.Codigo == 0)
                throw new DadosInvalidosException("Programa selecionado inv�lido");

            Programa.Load(Programa.Codigo);

            if (Programa.Emissora == null || Programa.Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            Save();
        }

        public override void Save()
        {
            Programa.Load(Programa.Codigo);

            if (Programa.Equipes != null && Programa.Equipes.Count > 0)
            {
                if (Programa.Equipes[0].Codigo != Codigo)
                    throw new QuantidadeMaximaItensExcedidaException();
            }

            Nome = Programa.Nome;

            var oldUrl = UrlFoto;
            var oldUrlAltaResolucao = UrlFotoAltaResolucao;

            try
            {
                if (Foto != null && FotoAltaResolucao != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Iron, FileName, Foto);
                    var retornoAltaResolucao = FileSystem.SaveFile(Common.Tipos.Aplicacao.Iron, FileNameAltaResolucao,
                                                                   FotoAltaResolucao);

                    UrlFoto = retorno.Path;
                    UrlFotoAltaResolucao = retornoAltaResolucao.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, oldUrl);

                    if (!string.IsNullOrWhiteSpace(oldUrlAltaResolucao))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, oldUrlAltaResolucao);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Equipe", Codigo.ToString(), exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlFoto))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, UrlFoto);

            base.Delete();
        }
    }
}