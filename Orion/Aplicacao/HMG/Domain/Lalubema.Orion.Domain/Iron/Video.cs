using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Iron
{
    public class Video : DomainBase<Video, IVideoRepositorio<Video>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string UrlVideo { get; set; }

        public virtual string Titulo { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual void Save(int codigoEmissora)
        {
            if (Programa == null || Programa.Codigo == 0)
                throw new DadosInvalidosException("Programa selecionado inv�lido");

            Programa.Load(Programa.Codigo);

            if(Programa.Emissora == null || Programa.Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            base.Save();
        }
    }
}