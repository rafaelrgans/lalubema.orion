﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Iron
{
    public class Emissora : DomainBase<Emissora, IEmissoraRepositorio<Emissora>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual IList<Programa> Programas { get; set; }

        public virtual IList<Categoria> Categorias { get; set; }

        public virtual IList<Usuario> Usuarios { get; set; }
    }
}