using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Iron
{
    public class Categoria : DomainBase<Categoria, ICategoriaRepositorio<Categoria>>, IDomainModel
    {
        public Categoria()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Videos = new List<Video>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual Emissora Emissora { get; set; }

        public virtual IList<Video> Videos { get; set; }

        public virtual void Save(int codigoEmissora)
        {
            if (Emissora == null)
                Emissora = Emissora.GetRepository().Get(codigoEmissora);
            else if (Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            base.Save();
        }
    }
}