﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Estilo : DomainBase<Estilo, IEstiloRepositorio<Estilo>>
    {
        public virtual int Codigo { get; set; }

        public virtual int EstiloID { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int Usuario { get; set; }
    }
}