﻿using System;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Agenda : DomainBase<Agenda, IAgendaRepositorio<Agenda>>
    {
        public virtual int Codigo { get; set; }

        public virtual int AgendaID { get; set; }

        public virtual string Competicao { get; set; }

        public virtual string Local { get; set; }

        [Map("DtAgenda")]
        public virtual DateTime DataInicio { get; set; }

        [Map("DtFinalEvento")]
        public virtual DateTime DataFinal { get; set; }

        public virtual bool Repetir { get; set; }

        public virtual int? Piscina { get; set; }

        public virtual int UsuarioID { get; set; }

        public virtual int Usuario { get; set; }
    }
}