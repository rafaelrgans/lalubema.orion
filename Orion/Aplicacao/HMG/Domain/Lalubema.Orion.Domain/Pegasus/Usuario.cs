﻿using System;
using Lalubema.Orion.Common.Exceptions;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>
    {
        protected Usuario()
        {
        }

        public Usuario(string email)
        {
            Email = email;
        }

        public virtual int Codigo { get; set; }

        public virtual int UsuarioID { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Senha { get; set; }

        public virtual string Email { get; set; }

        public virtual int? Registro { get; set; }

        public virtual DateTime? DataNascimento { get; set; }

        public virtual int Categoria { get; set; }

        public virtual string Clube { get; set; }

        public virtual void ValidarSenha(string senha)
        {
            if (!Senha.Equals(senha, StringComparison.InvariantCulture))
                throw new UsuarioOuSenhaInvalidosException();
        }
    }
}