﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Prova : DomainBase<Prova, IProvaRepositorio<Prova>>
    {
        public virtual int Codigo { get; set; }

        public virtual int ProvaID { get; set; }

        public virtual int Serie { get; set; }

        public virtual int Raia { get; set; }

        public virtual int Distancia { get; set; }

        public virtual int? Piscina { get; set; }

        public virtual int? NumeroProva { get; set; }

        public virtual string Tempo { get; set; }

        public virtual string Balizamento { get; set; }

        public virtual int? Classificacao { get; set; }

        [Map("EstiloID")]
        public virtual int Estilo { get; set; }

        [Map("AgendaID")]
        public virtual int Agenda { get; set; }

        public virtual int Usuario { get; set; }
    }
}