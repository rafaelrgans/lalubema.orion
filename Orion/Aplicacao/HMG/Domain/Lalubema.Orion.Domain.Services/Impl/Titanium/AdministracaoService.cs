﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Titanium;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;
using Lalubema.Orion.Domain.Services.Spec.Titanium;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Titanium
{
    public class AdministracaoService : ServiceBase, IAdministracaoService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Titanium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno CadastrarTorneio(DTOCadastroTorneio torneio)
        {
            var security = new ChaveCriptografadaHelper(torneio.ChaveCriptografada);

            var torneioModel = TitaniumTorneioRepositorio.Get(torneio.Codigo) ?? new Torneio(torneio.Nome);

            torneioModel.Nome = torneio.Nome;
            torneioModel.Inicio = torneio.Inicio;
            torneioModel.Fim = torneio.Fim;
            torneioModel.QuantidadeJogadorPorEquipe = torneio.QuantidadeJogadorPorEquipe;
            torneioModel.TempoMaximoPartida = torneio.DuracaoPartida;

            torneioModel.SetAdministrador(security.EmailUsuario);

            if (torneioModel.Usuarios == null)
                torneioModel.Usuarios = new List<Usuario>();

            torneioModel.Save();

            var responsavelConviteTorneio = TitaniumUsuarioRepositorio.BuscarUsuarioPorEmail(security.EmailUsuario);

            foreach (var participante in torneio.Usuarios)
            {
                var atletaModel = TitaniumUsuarioRepositorio.BuscarUsuarioPorEmail(participante.Email) ??
                                  new Usuario(participante.Nome, participante.Email);

                atletaModel.ConvidarParaTorneio(torneioModel, responsavelConviteTorneio);
            }

            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Titanium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOTorneioRetorno BuscarTorneio(string chaveCriptografada, int codigoTorneio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var torneio = TitaniumTorneioRepositorio.BuscarTorneioUsuario(codigoTorneio, security.EmailUsuario);

            torneio.VerificarAdministrador(security.EmailUsuario);

            return torneio.Transform<DTOTorneioRetorno>();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Titanium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOTorneioPorStatus BuscarTorneios(DTOTorneioEntrada torneioEntrada)
        {
            var security = new ChaveCriptografadaHelper(torneioEntrada.ChaveCriptografada);

            var torneios = TitaniumTorneioRepositorio.ListAll(security.EmailUsuario);

            var emAndamentoModel =
                            (from torneio in torneios
                             where torneio.Fim >= DateTime.Now.Date
                             orderby torneio.Inicio descending
                             select torneio).ToList();

            foreach (var torneio in emAndamentoModel)
                torneio.VerificarAdministrador(security.EmailUsuario);

            var encerradosModel =
                (from torneio in torneios
                 where torneio.Fim < DateTime.Now.Date
                 orderby torneio.Fim descending
                 select torneio).ToList();

            foreach (var torneio in encerradosModel)
                torneio.VerificarAdministrador(security.EmailUsuario);

            var conviteModel = (from torneio in torneios
                                from atleta in torneio.Usuarios
                                where atleta.Username.Equals(security.EmailUsuario, StringComparison.InvariantCultureIgnoreCase)
                                from torneioAtleta in atleta.Torneios
                                where torneioAtleta.Torneio.Codigo == torneio.Codigo && torneioAtleta.Status == StatusUsuarioTorneio.Convidado
                                orderby torneio.Inicio
                                select torneio).ToList();

            var proximasPartidas = (from torneio in emAndamentoModel
                                    from rodada in torneio.Rodadas.Take(1)
                                    orderby rodada.NumeroRodada descending
                                    from partida in rodada.Partidas
                                    from equipe in partida.Equipes
                                    from atleta in equipe.Equipe.Usuarios
                                    where atleta.Username.Equals(security.EmailUsuario, StringComparison.InvariantCultureIgnoreCase)
                                    orderby partida.Data
                                    select partida).ToList();

            var retorno = new DTOTorneioPorStatus
            {
                EmAndamento = emAndamentoModel.TransformList<DTOStatusTorneio>(),
                Convidado = conviteModel.TransformList<DTOStatusTorneio>(),
                Encerrado = encerradosModel.TransformList<DTOStatusTorneio>(),
                ProximasPartidas =
                    proximasPartidas.TransformList<DTOPartida>()
            };

            return retorno;
        }
    }
}