﻿using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Cobalt;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.Services.Impl.Cobalt
{
    public class ServicoOnlineService : ServiceBase, IServicoOnlineService
    {
        [ValidateCryptographyKey(false, (int) Aplicacao.Cobalt)]
        public DTORetorno SolicitarServicoOnlineNotarial(DTOServicoOnlineNotarialEntrada solicitacaoServicoNotarial)
        {
            ValidadorCobaltSolicitarServicoNotarial.Validate(solicitacaoServicoNotarial, NotificationCollection);

            if (NotificationCollection.HasError())
                throw new DadosInvalidosException(NotificationCollection.ToString());

            new ServicoOnlineNotarial().Solicitar(solicitacaoServicoNotarial);

            return new DTORetorno();
        }
    }
}