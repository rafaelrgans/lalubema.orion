﻿using System;
using System.Linq;
using System.Net;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Orion.Domain.Services.Spec.Vanadium;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;
using System.Collections;

namespace Lalubema.Orion.Domain.Services.Impl.Vanadium
{
    public class PedidoService : ServiceBase, IPedidoService
    {
        [ValidateCryptographyKey(true, (int) Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOEntidadeRetorno RegistrarPedido(DTOPedidoEntrada pedidoEntrada)
        {
            AuthorizeRequest(pedidoEntrada.ChaveCriptografada, pedidoEntrada.CodigoEstabelecimento);

            var httpStatusCode = HttpStatusCode.OK;

			var security = new ChaveCriptografadaHelper(pedidoEntrada.ChaveCriptografada);

			Pedido pedidoModel = new Pedido();
			if(pedidoEntrada.CodigoMesa != 0)
				pedidoModel = new Pedido().GetPedidoAberto(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento);
			else
				pedidoModel = new Pedido().GetPedidoAberto(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento, security.CodigoUsuario);

            if (pedidoModel == null)
            {
                pedidoModel = new Pedido(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento, security.CodigoUsuario);
                httpStatusCode = HttpStatusCode.Created;
            }

            pedidoModel.AdicionarItens(pedidoEntrada.Itens, false, security.CodigoUsuario);

            return new DTOEntidadeRetorno(pedidoModel.Codigo, httpStatusCode);
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno ComandarPedido(DTOPedidoEntrada pedidoEntrada)
        {
            AuthorizeRequest(pedidoEntrada.ChaveCriptografada, pedidoEntrada.CodigoEstabelecimento);

            var pedidoModel = new Pedido().GetPedidoAberto(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento);

            var security = new ChaveCriptografadaHelper(pedidoEntrada.ChaveCriptografada);
	
            pedidoModel.ComandarItens(pedidoEntrada.Itens, security.CodigoUsuario);

            return new DTORetorno(HttpStatusCode.OK);
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno RemoverItens(DTOPedidoEntrada pedidoEntrada)
        {
            AuthorizeRequest(pedidoEntrada.ChaveCriptografada, pedidoEntrada.CodigoEstabelecimento);

            var pedidoModel =
                VanadiumPedidoRepositorio.GetPedidoAberto(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento);

            if (pedidoModel == null)
                throw new ItemNotFoundException(
                    string.Format("Estabelecimento: {0}\r\nMesa: {1}", pedidoEntrada.CodigoEstabelecimento,
                                  pedidoEntrada.CodigoMesa), "Pedido");

            pedidoModel.RemoverItens(pedidoEntrada.Itens);

            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno FecharComanda(DTOPedidoEntrada pedidoEntrada)
        {
            AuthorizeRequest(pedidoEntrada.ChaveCriptografada, pedidoEntrada.CodigoEstabelecimento);

            var pedidoModel =
                VanadiumPedidoRepositorio.GetPedidoAberto(pedidoEntrada.CodigoMesa, pedidoEntrada.CodigoEstabelecimento);

            if (pedidoModel == null)
                throw new ItemNotFoundException(
                    string.Format("Estabelecimento: {0}\r\nMesa: {1}", pedidoEntrada.CodigoEstabelecimento,
                                  pedidoEntrada.CodigoMesa), "Pedido");

            var security = new ChaveCriptografadaHelper(pedidoEntrada.ChaveCriptografada);

			pedidoModel.BaixarItens(pedidoEntrada.Itens, security.CodigoUsuario);
			pedidoModel.Fechar();

            return new DTORetorno(HttpStatusCode.OK);
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOMesasAbertasRetorno ListarMesasAbertas(string chaveCriptografada, int codigoEstabelecimento)
        {
            AuthorizeRequest(chaveCriptografada, codigoEstabelecimento);

            var pedidos = new Pedido().ListarPedidosAbertos(codigoEstabelecimento);

            var result = new DTOMesasAbertasRetorno();

            foreach (var pedido in pedidos)
                result.Mesas.Add(pedido.CodigoMesa);

            return result;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOPedidoRetorno BuscarComanda(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa)
        {
            AuthorizeRequest(chaveCriptografada, codigoEstabelecimento);

            var pedidoModel = VanadiumPedidoRepositorio.GetPedidoAberto(codigoMesa, codigoEstabelecimento);

            if (pedidoModel == null)
                throw new ItemNotFoundException(
                    string.Format("Estabelecimento: {0}\r\nMesa: {1}", codigoEstabelecimento,
                                  codigoMesa), "Pedido");

            return pedidoModel.Transform<DTOPedidoRetorno>(true);
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOPedidoRetorno ListarItensPendentes(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa)
        {
            AuthorizeRequest(chaveCriptografada, codigoEstabelecimento);

			var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var pedidoModel = VanadiumPedidoRepositorio.GetPedidoAberto(codigoMesa, codigoEstabelecimento, security.CodigoUsuario);

            if (pedidoModel == null)
                throw new ItemNotFoundException(
                    string.Format("Estabelecimento: {0}\r\nMesa: {1}", codigoEstabelecimento,
                                  codigoMesa), "Pedido");

            var pedidoRetorno = new DTOPedidoRetorno
                                    {
                                        CodigoMesa = codigoMesa,
                                        Estabelecimento = codigoEstabelecimento,
                                        Itens = pedidoModel.ListarItensPendentes().TransformList<DTOItem>()
                                    };

            return pedidoRetorno;
        }

		[ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
		[Transaction(TransactionPropagation.Required, ReadOnly = true)]
		public DTOPedidoRetorno ListarPedidosEstabelecimento(string chaveCriptografada, int codigoEstabelecimento)
		{
			AuthorizeRequest(chaveCriptografada, codigoEstabelecimento);

			var security = new ChaveCriptografadaHelper(chaveCriptografada);

			var pedidoModel = VanadiumPedidoRepositorio.GetPedidoAberto(0, codigoEstabelecimento, security.CodigoUsuario);

			if (pedidoModel == null)
				throw new ItemNotFoundException(string.Format("Estabelecimento: {0}", codigoEstabelecimento), "Pedido");

			return pedidoModel.Transform<DTOPedidoRetorno>(true);
		}

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOHistoricoPedidos ListarHistoricoPedidos(string chaveCriptografada, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim)
        {
            var chaveCriptografadaHelper = new ChaveCriptografadaHelper(chaveCriptografada);
            var historicoPedidos = new Usuario().Get(chaveCriptografadaHelper.CodigoUsuario).ListarHistoricoPedidos(codigoEstabelecimento, dataInicio, dataFim);

            return historicoPedidos;
        }

		[ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
		[Transaction(TransactionPropagation.Required, ReadOnly = true)]
		public DTOHistoricoPedidos ListarPedidosAbertosMenu(string chaveCriptografada, int codigoMesa, int codigoEstabelecimento)
		{
			AuthorizeRequest(chaveCriptografada, codigoEstabelecimento);

			var security = new ChaveCriptografadaHelper(chaveCriptografada);

			var pedidoModel = new Pedido().ListarPedidosAbertosMenu(codigoMesa, codigoEstabelecimento);

			if (pedidoModel == null)
				throw new ItemNotFoundException(string.Format("Estabelecimento: {0}", codigoEstabelecimento), "Pedido");

			
			return pedidoModel.Transform<DTOHistoricoPedidos>(true);
		}

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno TrocarMesa(DTOAlterarMesaEntrada alterarMesa)
        {
            AuthorizeRequest(alterarMesa.ChaveCriptografada, alterarMesa.CodigoEstabelecimento);

            var pedido = new Pedido().GetPedidoAberto(alterarMesa.CodigoMesa, alterarMesa.CodigoEstabelecimento);

            if (pedido == null)
                throw new ItemNotFoundException(
                    string.Format("Mesa: {0} - Estabelecimento: {1}", alterarMesa.CodigoMesa,
                                  alterarMesa.CodigoEstabelecimento), "Pedido");

            pedido.TrocarMesa(alterarMesa.NovaMesa);

            return new DTORetorno();
        }

		[ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
		[Transaction(TransactionPropagation.Required, ReadOnly = false)]
		public DTORetorno TransferirMesaPedido(DTOTransfereMesaEntrada transferirMesa)
		{
			AuthorizeRequest(transferirMesa.ChaveCriptografada, transferirMesa.CodigoEstabelecimento);

			var usuario = new ChaveCriptografadaHelper(transferirMesa.ChaveCriptografada);
			var UsuarioResponsavel = new Usuario().Get(usuario.CodigoUsuario);
			if (usuario != null)
				transferirMesa.CodigoUsuario = usuario.CodigoUsuario;

			var pedido = new Pedido().GetTransferirMesaPedido(transferirMesa.CodigoPedido, transferirMesa.CodigoEstabelecimento);

			if (pedido == null)
				throw new ItemNotFoundException(string.Format("Mesa: {0} - Estabelecimento: {1}", transferirMesa.CodigoMesa, transferirMesa.CodigoEstabelecimento), "Pedido");

			pedido.Responsavel = UsuarioResponsavel;
			pedido.TrocarMesa(transferirMesa.NovaMesa);

			return new DTORetorno();
		}

		public DTORelatorioGarcomRetorno GerarRelatorioGarcom(DTORelatorioEntrada relatorio)
		{
			//AuthorizeRequest(relatorio.ChaveCriptografada, relatorio.CodEstabelecimento);

			var pedidos = new Pedido().ListarPedidosFechadosRelatorio(relatorio.CodEstabelecimento, relatorio.CodigoUsuario, relatorio.DtInicio, relatorio.DtFim);
			for (int i = 0; i < pedidos.Count; i++)
				pedidos.ToList()[i].TotalVendido = pedidos.ToList()[i].Itens.Sum(p => p.Item.Preco * p.Quantidade);

			return new DTORelatorioGarcomRetorno
			{
				Relatorio = pedidos.TransformList<DTORelatorio>(true, null)
			};
		}

		public DTORelatorioItemRetorno GerarRelatorioItem(DTORelatorioEntrada relatorio)
		{
			//AuthorizeRequest(relatorio.ChaveCriptografada, relatorio.CodEstabelecimento);

			var itens = new ItemPedido().ListAll().Where(p => p.Item.Codigo == relatorio.CodigoItem).Where(p => p.Pedido.DataEncerramento.HasValue).Where(p => p.Created.CompareTo(relatorio.DtInicio) > 0 && p.Created.CompareTo(relatorio.DtFim) < 0);
			for (int i = 0; i < itens.ToList().Count; i++)
				itens.ToList()[i].TotalVendido = itens.ToList().Sum(p => p.Item.Preco * p.Quantidade);

			return new DTORelatorioItemRetorno
			{
				ItemPedido = itens.TransformList<DTOItemPedido>(true, null)
			};
		}

		public DTORelatorioMesaRetorno GerarRelatorioMesa(DTORelatorioEntrada relatorio)
		{
			//AuthorizeRequest(relatorio.ChaveCriptografada, relatorio.CodEstabelecimento);

			var mesa = new ItemPedido().ListAll().Where(p => p.Pedido.CodigoMesa == relatorio.NumeroMesa).Where(p => p.Pedido.DataEncerramento.HasValue).Where(p => p.Created.CompareTo(relatorio.DtInicio) > 0 && p.Created.CompareTo(relatorio.DtFim) < 0);
			for (int i = 0; i < mesa.ToList().Count; i++)
				mesa.ToList()[i].TotalVendido = mesa.ToList().Sum(p => p.Item.Preco * p.Quantidade);

			var ret = new DTORelatorioMesaRetorno { ItemPedido = mesa.TransformList<DTOItemPedido>(true, null) };

			return ret;
		}

        private void AuthorizeRequest(string chaveCriptografada, int codigoEstabelecimento)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var usuario = new Usuario().Get(security.CodigoUsuario);

            if (usuario.UserType == TipoUsuario.Cliente)
                return;

            var estabelecimento = new Estabelecimento().Get(codigoEstabelecimento);

            if (estabelecimento.Usuarios.All(t => t.Usuario.Codigo != security.CodigoUsuario))
                throw new UnauthorizedAccessException();
        }
	}
}