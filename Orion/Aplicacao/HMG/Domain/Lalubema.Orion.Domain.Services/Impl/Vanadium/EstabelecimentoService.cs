﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao;
using Lalubema.Orion.Domain.Services.Spec.Vanadium;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using DTOCardapio = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio.DTOCardapio;
using DTOCategoria = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria.DTOCategoria;
using DTOEstabelecimento = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao.DTOEstabelecimento;
using DTOItem = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item.DTOItem;

namespace Lalubema.Orion.Domain.Services.Impl.Vanadium
{
    public class EstabelecimentoService : ServiceBase, IEstabelecimentoService
    {
        [ValidateCryptographyKey(true, (int) Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        public DTOCategorias BuscarCategoriasCardapio(string chaveCriptografada, int codigoEstabelecimento)
        {
            var categorias = VanadiumCategoriaRepositorio.ListAll(codigoEstabelecimento);

            var retorno = new DTOCategorias {Categorias = categorias.TransformList<DTOCategoria>()};

            retorno.Categorias.Insert(0, new DTOCategoria {Codigo = -1, Nome = "Destaque", Ordem = 1});

            return retorno;
        }

        [ValidateCryptographyKey(true, (int) Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOCardapios BuscarCardapios(string chaveCriptografada, int codigoEstabelecimento)
        {
            var cardapios = VanadiumCardapioRepositorio.ListAll(codigoEstabelecimento);

            var retorno = new DTOCardapios {Cardapios = cardapios.TransformList<DTOCardapio>()};

            return retorno;
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOItens BuscarDestaques(string chaveCriptografada, int codigoEstabelecimento)
        {
            var itens = VanadiumItemRepositorio.ListarDestaques(codigoEstabelecimento);

            return new DTOItens {Itens = itens.TransformList<DTOItem>()};
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOImportacao Importar(string chaveCriptografada, int codigoEstabelecimento)
        {
            var estabelecimento = VanadiumEstabelecimentoRepositorio.Get(codigoEstabelecimento);

            var retorno = new DTOImportacao
                              {
                                  Estabelecimento = estabelecimento.Transform<DTOEstabelecimento>(true),
                                  Cardapios =
                                      VanadiumCardapioRepositorio.ListAll(codigoEstabelecimento).TransformList
                                      <DataTransferObject.Vanadium.Importacao.DTOCardapio>(),
                                  Categorias =
                                      VanadiumCategoriaRepositorio.ListAll(codigoEstabelecimento).TransformList
                                      <DataTransferObject.Vanadium.Importacao.DTOCategoria>(),
                                  Itens =
                                      VanadiumItemRepositorio.ListAll(codigoEstabelecimento).TransformList
                                      <DataTransferObject.Vanadium.Importacao.DTOItem>()
                              };

            if (estabelecimento != null)
            {
                retorno.Usuarios = new List<DTOUsuario>();

                foreach (var usuario in estabelecimento.Usuarios)
                {
                    var u = usuario.Usuario.Transform<DTOUsuario>();
                    u.IsAdministrador = usuario.TipoUsuario == TipoUsuario.Administrator;
                    u.Tipo = usuario.TipoUsuario.ToInt32();

                    retorno.Usuarios.Add(u);
                }
            }

            return retorno;
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOEstabelecimentos ListarEstabelecimentos(string chaveCriptografada)
        {
            var estabelecimentos = new Estabelecimento().ListAll();

            return new DTOEstabelecimentos
                       {
                           Estabelecimentos =
                               estabelecimentos
                               .TransformList<DataTransferObject.Vanadium.Cadastro.Estabelecimento.DTOEstabelecimento>(
                                   true, null)
                       };
        }
    }
}