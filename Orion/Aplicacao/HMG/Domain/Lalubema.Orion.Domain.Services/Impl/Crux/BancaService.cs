﻿using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Crux;
using Lalubema.Orion.Domain.Services.Spec.Crux;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Crux
{
    public class BancaService : ServiceBase, IBancaService
    {
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        [ValidateCryptographyKey(false, (int)Aplicacao.Crux)]
        public DTOBanca ConsultarBanca(string chaveCriptografada, string cultureName, int codigoBanca)
        {
            var banca = CruxBancaRepositorio.ConsultarBanca(codigoBanca, cultureName);

            return banca.Transform<DTOBanca>();
        }

        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        [ValidateCryptographyKey(false, (int)Aplicacao.Crux)]
        public DTOBancas ConsultarBanca(string chaveCriptografada, string cultureName, string nomeBanca)
        {
            var bancas = CruxBancaRepositorio.ConsultarBanca(nomeBanca, cultureName);

            var retorno = new DTOBancas {Bancas = bancas.TransformList<DTOBanca>()};

            return retorno;
        }
    }
}