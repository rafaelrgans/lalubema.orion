﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class CondominioService : ServiceBase, ICondominioService
    {
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public Stream CheckEmail(string chaveConfirmacao)
        {
            var resposta = "<html><head><title>Aceitar Convite</title></head><body>{0}</body></html>";

            try
            {
                var conta = SecurityHelper.Decrypt(chaveConfirmacao);

                var aplicacao = (Aplicacao) conta[3].ToInt32();
                var codigoCondominio = conta[5].ToInt32();

                var condominio = Condominio.GetRepository().Get(codigoCondominio);

                resposta = condominio.Ativar(conta[0], conta[2].ToDateTime(), aplicacao);
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (TargetInvocationException)
            {
                throw;
            }
            catch (Exception)
            {
                resposta = string.Format(resposta,
                                         "<p>Ocorreu um erro ao ativar o usu&aacute;rio. Tente novamente mais tarde e caso o erro persista favor entrar em contato com o administrador do sistema.</p>");

                throw new Exception(resposta);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(resposta));
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno RegisterDevice(DTORegisterDevice registerDevice)
        {
            var condominio = new Condominio().Get(registerDevice.Condominio);

            condominio.RegisterDevice(registerDevice);

            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            var condominio = new Condominio().Get(unregisterDevice.Condominio);
            condominio.UnregisterDevice(unregisterDevice);
            return new DTORetorno();
        }
    }
}