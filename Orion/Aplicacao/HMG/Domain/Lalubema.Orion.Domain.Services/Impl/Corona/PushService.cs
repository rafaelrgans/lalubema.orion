﻿using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Lalubema.Orion.Common.Exceptions.Orion;
using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class PushService : ServiceBase, IPushService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOSendPushRetorno SendPush(DTOSendPush sendPush)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(sendPush.ChaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Enviaar Notificação
            Notificacao nt = usuario.Notificar(sendPush);

            // Retorno Sucesso
            return new DTOSendPushRetorno()
            {
                Codigo = nt.Codigo,
                Mensagem = nt.Mensagem,
                Origem = nt.Origem,
                RemetenteDeviceToken = nt.RemetenteDevice.Token,
                StatusNotificacao = nt.Status,
                TempoChegada = nt.TempoChegada,
                TipoNotificacao = nt.TipoMensagem
            };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOSendPushRetorno RemovePush(string chaveCriptografada, int codigoNotificacao)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Enviaar Notificação
            Notificacao nt = usuario.RemoverNotificacao(codigoNotificacao);

            // Retorno Sucesso
            return new DTOSendPushRetorno()
            {
                Codigo = nt.Codigo,
                Mensagem = nt.Mensagem,
                Origem = nt.Origem,
                RemetenteDeviceToken = nt.RemetenteDevice.Token,
                StatusNotificacao = nt.Status,
                TempoChegada = nt.TempoChegada,
                TipoNotificacao = nt.TipoMensagem
            };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOGetPush GetPush(string chaveCriptografada, int codigoCondominio)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Leitura das informações de Notificação (GetPush)
            var result = usuario.ListarNotificacao(codigoCondominio);
            return result;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOGetPush GetReplyPush(string chaveCriptografada)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Leitura das informações de Notificação (GetPush)
            var result = usuario.ListarNotificacaoResposta();
            return result;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOPushChegadaSeguraRetorno ChegadaSegura(DTOPushChegadaSegura sendPush)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(sendPush.ChaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Enviaar Notificação
            ChegadaSegura cs = usuario.ChegadaSegura(sendPush);

            // Retorno Sucesso
            return new DTOPushChegadaSeguraRetorno() {
                CodigoNotificacao = cs.Notificacao.Codigo,
                DataStatus = cs.CreatedAt,
                StatusNotificacao = cs.Status
            };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOGetPush GetPushRemetente(string chaveCriptografada)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Leitura das informações de Notificação (GetPush)
            var result = usuario.ListarNotificacao();
            return result;
        }
    }
}