﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.Orion;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class UsuarioService : ServiceBase, IUsuarioService
    {
        [ValidateCryptographyKey(true, (int)Lalubema.Orion.Common.Tipos.Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno RegisterDevice(DTORegisterDevice registerDevice)
        {
            var usuario = new Usuario().Get(registerDevice.Usuario);
            usuario.RegisterDevice(registerDevice);
            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Lalubema.Orion.Common.Tipos.Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            var usuario = new Usuario().Get(unregisterDevice.Usuario);
            usuario.UnregisterDevice(unregisterDevice);
            return new DTORetorno();
        }
    }
}