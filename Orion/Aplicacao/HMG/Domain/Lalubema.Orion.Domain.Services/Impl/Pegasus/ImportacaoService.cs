﻿using System.Net;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Domain.Services.Spec.Pegasus;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Pegasus
{
    public class ImportacaoService : ServiceBase, IImportacaoService
    {
        [Transaction(TransactionPropagation.Required)]
        [ValidateCryptographyKey(false, (int)Aplicacao.Pegasus)]
        public DTORetorno Exportar(DTOExportacao importacaoIndices)
        {
            var usuario = PegasusUsuarioRepositorio.Buscar(importacaoIndices.Usuario.Email,
                                                    importacaoIndices.Usuario.ChaveNuvem);

            var novoUsuario = usuario == null;

            if (usuario != null)
                LimparDadosUsuario(usuario.Email, importacaoIndices.Usuario.ChaveNuvem);

            IncluirDadosUsuario(importacaoIndices);

            return new DTORetorno(novoUsuario ? HttpStatusCode.Created : HttpStatusCode.OK);
        }

        private void IncluirDadosUsuario(DTOExportacao dadosUsuario)
        {
            var usuario = new Usuario(dadosUsuario.Usuario.Email)
                              {
                                  Nome = dadosUsuario.Usuario.Nome,
                                  Registro = dadosUsuario.Usuario.Registro,
                                  Clube = dadosUsuario.Usuario.Clube,
                                  DataNascimento = dadosUsuario.Usuario.DtNascimento,
                                  Categoria = dadosUsuario.Usuario.CategoriaID,
                                  Senha = dadosUsuario.Usuario.ChaveNuvem,
                                  UsuarioID = dadosUsuario.Usuario.UsuarioID
                              };

            PegasusUsuarioRepositorio.Save(usuario);

            IncluirAgendasUsuario(dadosUsuario, usuario);

            IncluirCategoriasUsuario(dadosUsuario, usuario);

            IncluirClubesUsuario(dadosUsuario, usuario);

            IncluirEquipesUsuario(dadosUsuario, usuario);

            IncluirEstilosUsuario(dadosUsuario, usuario);

            IncluirIndicesUsuario(dadosUsuario, usuario);

            IncluirProvasUsuario(dadosUsuario, usuario);

            IncluirMetasUsuario(dadosUsuario, usuario);
        }

        private void IncluirMetasUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Metas == null)
                return;

            foreach (var metaUsuario in dadosUsuario.Metas)
            {
                var meta = metaUsuario.Transform<Meta>(true);
                meta.Usuario = usuario.Codigo;

                PegasusMetaRepositorio.Save(meta);
            }
        }

        private void IncluirProvasUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Provas == null)
                return;

            foreach (var provaUsuario in dadosUsuario.Provas)
            {
                var prova = provaUsuario.Transform<Prova>(true);
                prova.Usuario = usuario.Codigo;

                PegasusProvaRepositorio.Save(prova);
            }
        }

        private void IncluirIndicesUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Indices == null)
                return;

            foreach (var indiceUsuario in dadosUsuario.Indices)
            {
                var indice = indiceUsuario.Transform<Indice>(true);
                indice.Usuario = usuario.Codigo;

                PegasusIndiceRepositorio.Save(indice);
            }
        }

        private void IncluirEstilosUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Estilos == null)
                return;

            foreach (var estiloUsuario in dadosUsuario.Estilos)
            {
                var estilo = estiloUsuario.Transform<Estilo>();
                estilo.Usuario = usuario.Codigo;

                PegasusEstiloRepositorio.Save(estilo);
            }
        }

        private void IncluirEquipesUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Equipes == null)
                return;

            foreach (var equipeUsuario in dadosUsuario.Equipes)
            {
                var equipe = equipeUsuario.Transform<Equipe>(true);
                equipe.Usuario = usuario.Codigo;

                EquipeRepositorio.Save(equipe);
            }
        }

        private void IncluirClubesUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Clubes == null)
                return;

            foreach (var clubeUsuario in dadosUsuario.Clubes)
            {
                var clube = clubeUsuario.Transform<Clube>();
                clube.Usuario = usuario.Codigo;

                PegasusClubeRepositorio.Save(clube);
            }
        }

        private void IncluirCategoriasUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if (dadosUsuario.Categorias == null)
                return;

            foreach (var categoriaUsuario in dadosUsuario.Categorias)
            {
                var categoria = categoriaUsuario.Transform<Categoria>();
                categoria.Usuario = usuario.Codigo;

                PegasusCategoriaRepositorio.Save(categoria);
            }
        }

        private void IncluirAgendasUsuario(DTOExportacao dadosUsuario, Usuario usuario)
        {
            if(dadosUsuario.Agendas == null)
                return;
            
            foreach (var agendaUsuario in dadosUsuario.Agendas)
            {
                var agenda = agendaUsuario.Transform<Agenda>(true);
                agenda.Usuario = usuario.Codigo;

                PegasusAgendaRepositorio.Save(agenda);
            }
        }

        private void LimparDadosUsuario(string email, string senha)
        {
            var usuario = PegasusUsuarioRepositorio.Buscar(email, senha);
            
            LimparAgendasUsuario(usuario);

            LimparCategoriasUsuario(usuario);

            LimparClubesUsuario(usuario);

            LimparEquipesUsuario(usuario);

            LimparEstilosUsuario(usuario);

            LimparIndicesUsuario(usuario);

            LimparProvasUsuario(usuario);

            LimparMetasUsuario(usuario);

            PegasusUsuarioRepositorio.Delete(usuario);
        }

        private void LimparMetasUsuario(Usuario usuario)
        {
            var metas = PegasusMetaRepositorio.ListAll(usuario.Codigo);

            foreach (var meta in metas)
                meta.Delete();
        }

        private void LimparProvasUsuario(Usuario usuario)
        {
            var provas = PegasusProvaRepositorio.ListAll(usuario.Codigo);

            foreach (var prova in provas)
                prova.Delete();
        }

        private void LimparIndicesUsuario(Usuario usuario)
        {
            var indices = PegasusIndiceRepositorio.ListAll(usuario.Codigo);

            foreach (var indice in indices)
                indice.Delete();
        }

        private void LimparEstilosUsuario(Usuario usuario)
        {
            var estilos = PegasusEstiloRepositorio.ListAll(usuario.Codigo);

            foreach (var estilo in estilos)
                estilo.Delete();
        }

        private void LimparEquipesUsuario(Usuario usuario)
        {
            var equipes = EquipeRepositorio.ListAll(usuario.Codigo);

            foreach (var equipe in equipes)
                equipe.Delete();
        }

        private void LimparClubesUsuario(Usuario usuario)
        {
            var clubes = PegasusClubeRepositorio.ListAll(usuario.Codigo);

            foreach (var clube in clubes)
                clube.Delete();
        }

        private void LimparCategoriasUsuario(Usuario usuario)
        {
            var categorias = PegasusCategoriaRepositorio.ListAll(usuario.Codigo);

            foreach (var categoria in categorias)
                categoria.Delete();
        }

        private void LimparAgendasUsuario(Usuario usuario)
        {
            var agendas = PegasusAgendaRepositorio.ListAll(usuario.Codigo);

            foreach (var agenda in agendas)
                agenda.Delete();
        }

        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        [ValidateCryptographyKey(false, (int)Aplicacao.Pegasus)]
        public DTOImportacao Importar(string chaveCriptografada, string email, string senha)
        {
            var usuario = PegasusUsuarioRepositorio.Buscar(email, senha);

            if (usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            usuario.ValidarSenha(senha);

            var agendas = PegasusAgendaRepositorio.ListAll(usuario.Codigo);
            var categorias = PegasusCategoriaRepositorio.ListAll(usuario.Codigo);
            var clubes = PegasusClubeRepositorio.ListAll(usuario.Codigo);
            var equipes = EquipeRepositorio.ListAll(usuario.Codigo);
            var estilos = PegasusEstiloRepositorio.ListAll(usuario.Codigo);
            var indices = PegasusIndiceRepositorio.ListAll(usuario.Codigo);
            var provas = PegasusProvaRepositorio.ListAll(usuario.Codigo);
            var metas = PegasusMetaRepositorio.ListAll(usuario.Codigo);

            var retorno = new DTOImportacao
                              {
                                  Usuario = usuario.Transform<DTOUsuario>(true),
                                  Agendas = agendas.TransformList<DTOAgenda>(true, null),
                                  Categorias = categorias.TransformList<DTOCategoria>(),
                                  Clubes = clubes.TransformList<DTOClube>(),
                                  Equipes = equipes.TransformList<DTOEquipe>(true, null),
                                  Estilos = estilos.TransformList<DTOEstilo>(),
                                  Indices = indices.TransformList<DTOIndice>(true, null),
                                  Provas = provas.TransformList<DTOProva>(true, null),
                                  Metas = metas.TransformList<DTOMeta>(true, null)
                              };

            return retorno;
        }
    }
}