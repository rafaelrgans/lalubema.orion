﻿using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao;
using Lalubema.Orion.Domain.Services.Spec.Iron;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using DTOConteudo = Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao.DTOConteudo;
using DTOPrograma = Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao.DTOPrograma;
using DTOVideo = Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao.DTOVideo;

namespace Lalubema.Orion.Domain.Services.Impl.Iron
{
    public class ImportacaoService : ServiceBase, IImportacaoService
    {
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        [ValidateCryptographyKey(false, (int) Aplicacao.Iron)]
        public DTOImportacao Importar(string chaveCriptografada, int codigoEmissora)
        {
            var retorno = new DTOImportacao();

            var emissora = IronEmissoraRepositorio.Get(codigoEmissora);

            if (emissora == null)
                throw new ItemNotFoundException(codigoEmissora, "Emissora");

            var equipes = (from programa in emissora.Programas from equipe in programa.Equipes select equipe).ToList();
            var componentes = (from equipe in equipes from componente in equipe.Componentes select componente).ToList();
            var conteudos = (from programa in emissora.Programas from conteudo in programa.Conteudos select conteudo).ToList();
            var videos = (from programa in emissora.Programas from video in programa.Videos select video).ToList();

            retorno.Categorias = emissora.Categorias.TransformList<DTOCategoria>(true, null);
            retorno.Componentes = componentes.TransformList<DTOComponente>(true, null);
            retorno.Usuarios = emissora.Usuarios.TransformList<DTOUsuario>(true, null);
            retorno.Equipes = equipes.TransformList<DTOEquipe>(true, null);
            retorno.Programas = emissora.Programas.TransformList<DTOPrograma>(true, null);
            retorno.Conteudos = conteudos.TransformList<DTOConteudo>(true, null);
            retorno.Videos = videos.TransformList<DTOVideo>(true, null);

            return retorno;
        }


        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        [ValidateCryptographyKey(false, (int)Aplicacao.Iron)]
        public DTORetorno ExportarAcessos(DTOExportarAcesso programas)
        {
            foreach (var programa in programas.Programas)
            {
                var p = IronProgramaRepositorio.Get(programa.Codigo);
                p.QuantidadeAcesso += programa.QuantidadeAcesso;

                p.Save();
            }

            return new DTORetorno();
        }
    }
}