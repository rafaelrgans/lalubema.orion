﻿using System.Collections.Generic;
using System.Net;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Orion.Domain.Services.Spec.Aquila;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Aquila
{
    public class AquilaService : ServiceBase, IAquilaService
    {
        [ValidateCryptographyKey(false, (int)Aplicacao.Aquila)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada, int idPersonalidade)
        {
            var personalidade = AquilaPersonalidadeRepositorio.Get(idPersonalidade);

            return personalidade.Transform<DTOPersonalidade>();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Aquila)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var personalidade = AquilaPersonalidadeRepositorio.Get(security.EmailUsuario);

            return personalidade.Transform<DTOPersonalidade>();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Aquila)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno SalvarAlbum(string chaveCriptografada, DTOModifiedAlbum album)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var usuario = AquilaUsuarioRepositorio.Get(security.CodigoUsuario);

            var newResource = album.Codigo == 0;

            var albumModel = newResource ? new Album() : AquilaAlbumRepositorio.Get(album.Codigo);

            albumModel.Nome = album.Nome;
            albumModel.Tipo = (TipoAlbum)album.Tipo;
            albumModel.UrlCapa = album.UrlCapa;
            albumModel.Personalidade = usuario.Personalidade;
            albumModel.Data = album.Data;

            DefinirItensAlbum(album, albumModel);

            albumModel.Save();

            
            return new DTORetorno(newResource ? HttpStatusCode.Created : HttpStatusCode.OK);
        }

        private void DefinirItensAlbum(DTOModifiedAlbum album, Album albumModel)
        {
            if (albumModel.Itens == null)
                albumModel.Itens = new List<Item>();

            albumModel.Itens.Clear();

            foreach (var item in album.Itens)
            {
                var itemModel = item.Codigo == 0 ? new Item() : AquilaItemRepositorio.Get(item.Codigo);

                itemModel.Nome = item.Nome;
                itemModel.Descricao = item.Descricao;
                itemModel.Url = item.Url;
                itemModel.Album = albumModel;

                albumModel.Itens.Add(itemModel);
            }
        }
    }
}