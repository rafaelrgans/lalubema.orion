﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Services.Spec.Orion;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Spring;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Domain.Services.Impl.Orion
{
    public class CadastroService : ServiceBase, ICadastroService
    {
        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Salvar(DTOEntrada entidade)
        {
            var repository = GetRepository(entidade);

            var security = new ChaveCriptografadaHelper(entidade.ChaveCriptografada);
            if (security.CodigoCorporativo.HasValue)
                trySetCorporativo((IRepositorioBase)repository, security.CodigoCorporativo.Value);

            var key = GetPrimaryKey(entidade);

            var entidadeModel = GetEntityModel(repository, entidade, key, FilterTypeFlags.Read);

            if (((int)key) > 0 && entidadeModel == null)
                throw new ItemNotFoundException(key, string.Empty);

            MethodInfo transformMethod;

            if (entidadeModel == null)
            {
                var domainModel = GetDomainModel(entidade);

                transformMethod = typeof (TransformExtension)
                    .GetMethod("Transform", BindingFlags.Default | BindingFlags.Public | BindingFlags.Static, null,
                               new[] {typeof (object), typeof (bool)}, null)
                    .MakeGenericMethod(domainModel.GetType());

                entidadeModel = (IDomainModel) transformMethod.Invoke(entidade, new object[] {entidade, true});
            }
            else
            {
                transformMethod = typeof(TransformExtension)
                    .GetMethod("Transform", BindingFlags.Default | BindingFlags.Public | BindingFlags.Static, null,
                               new[] {typeof (object), entidadeModel.GetType()}, null);

                entidadeModel = (IDomainModel)transformMethod.Invoke(entidade, new object[] { entidade, entidadeModel });
            }

            var types = FlagsHelper.GetTypes<FilterAttribute>(entidade, FilterTypeFlags.Save);

            if (types.Count != 0)
            {
                var values = FlagsHelper.GetValues<FilterAttribute>(entidade, FilterTypeFlags.Save);

                entidadeModel.GetType().GetMethod("Save", types.ToArray()).Invoke(entidadeModel, values.ToArray());
            }
            else
                entidadeModel.Save();

            return entidade.ReturnSaved ? GetResult(entidadeModel, entidade) : new DTORetorno();
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Excluir(DTOEntrada entidade)
        {
            var repository = GetRepository(entidade);

            var key = GetPrimaryKey(entidade);

            var entidadeModel = GetEntityModel(repository, entidade, key, FilterTypeFlags.Delete);

            if (((int)key) == 0 || entidadeModel == null)
                throw new ItemNotFoundException(key, string.Empty);

            entidadeModel.Delete();

            return new DTORetorno();
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTORetorno Listar(DTOEntrada entidade)
        {
            var repository = GetRepository(entidade);

            var security = new ChaveCriptografadaHelper(entidade.ChaveCriptografada);
            if (security.CodigoCorporativo.HasValue)
                trySetCorporativo((IRepositorioBase)repository, security.CodigoCorporativo.Value);

            var resultado = ListAll(entidade, repository, null, null);

            return resultado != null ? (DTORetorno) GetResult(resultado, entidade) : null;
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORetorno> ListAll(DTOEntrada entidade, string filter, string sort)
        {
            var repository = GetRepository(entidade);

            var security = new ChaveCriptografadaHelper(entidade.ChaveCriptografada);
            if (security.CodigoCorporativo.HasValue)
                trySetCorporativo((IRepositorioBase)repository, security.CodigoCorporativo.Value);

            var resultado = ListAll(entidade, repository, filter, sort);

            return resultado != null ? ((IList) GetResult(resultado, entidade)).Cast<DTORetorno>().ToList() : null;
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTORetorno Buscar(DTOEntrada entidade)
        {
            var repository = GetRepository(entidade);

            var security = new ChaveCriptografadaHelper(entidade.ChaveCriptografada);
            if (security.CodigoCorporativo.HasValue)
                trySetCorporativo((IRepositorioBase)repository, security.CodigoCorporativo.Value);

            var key = GetPrimaryKey(entidade);

            var resultado = GetEntityModel(repository, entidade, key, FilterTypeFlags.Read);

            if (resultado == null)
                throw new ItemNotFoundException(key, string.Empty);

            return GetResult(resultado, entidade);
        }

        private static object GetResult(IList resultado, DTOBase entidade)
        {
            if (!entidade.GetType().IsMarkedWith<ComplexReturnTypeAttribute>())
                return GetResultList(entidade, resultado);

            var returnType = entidade.GetType().GetAttribute<ComplexReturnTypeAttribute>();

            var result = (DTORetorno)Activator.CreateInstance(returnType.Type);

            result.SetValue(returnType.PropertyName, resultado, true, true);

            return result;
        }

        private static object GetResultList(DTOBase entidade, IList resultado)
        {
            var retornoType = GetRetornoType(entidade);

            if(retornoType == null)
                return new DTORetorno();

            var transformMethod = typeof(TransformExtension)
                .GetMethod("TransformList", BindingFlags.Static | BindingFlags.Public, null,
                           new[] {typeof (IEnumerable)}, null)
                .MakeGenericMethod(retornoType);

            var listResult = transformMethod.Invoke(resultado, new object[] { resultado });

            return listResult;
        }

        private static DTORetorno GetResult(object resultado, DTOBase entidade)
        {
            var returnType = entidade.GetType().IsMarkedWith<ReturnTypeAttribute>()
                                  ? entidade.GetType().GetAttribute<ReturnTypeAttribute>().Type
                                  : GetRetornoType(entidade);

            if (returnType == null)
                return new DTORetorno();

            var transformMethod = typeof(TransformExtension).GetMethod("Transform",
                                                                        BindingFlags.Default | BindingFlags.Public |
                                                                        BindingFlags.Static,
                                                                        null, new[] { typeof(object), typeof(bool) }, null).
                MakeGenericMethod(returnType);

            var result = (DTORetorno)transformMethod.Invoke(resultado, new[] { resultado, true });

            return result;
        }

        private static Type GetRetornoType(DTOBase entidade)
        {
            var fullName = entidade.GetType().FullName;

            var retornoTypeName = fullName.Replace("Entrada", "Retorno");

            var retornoType = Type.GetType(retornoTypeName + ", " + entidade.GetType().Assembly.FullName);

            return retornoType;
        }

        private static object GetRepository(DTOBase entidade)
        {
            var domain = GetDomainModel(entidade);

            return domain.GetType().GetProperty("Repository").GetValue(domain, null);
        }

        private static object GetDomainModel(DTOBase entidade)
        {
            var att = entidade.GetType().GetAttribute<ObjectMapAttribute>();

            if (att == null)
                throw new Exception(entidade.GetType().Name + ". Mapeamento do DTO não encontrado.");

            var domain = ObjectFactory.GetObject(att.Name);

            return domain;
        }

        private static object GetPrimaryKey(DTOEntrada entidade)
        {
            var primarykey = entidade.GetType().GetProperties().FirstOrDefault(p => p.IsMarkedWith<KeyAttribute>());

            if (primarykey == null)
                throw new Exception(entidade.GetType().Name + ". Chave primária não encontrada.");

            return primarykey.GetValue(entidade, null);
        }

        private static IDomainModel GetEntityModel(object repository, object entidade, object key, FilterTypeFlags filterTypeFlags)
        {
            try
            {
                var types = FlagsHelper.GetTypes<FilterAttribute>(entidade, filterTypeFlags);
                var values = FlagsHelper.GetValues<FilterAttribute>(entidade, filterTypeFlags);

                types.Insert(0, typeof(int));
                values.Insert(0, key);

                IDomainModel model = (IDomainModel)repository.GetType().GetMethod("Get", types.ToArray()).Invoke(repository, values.ToArray());

                return model;
            }
            catch
            {
                return null;
            }
        }

        private static IList ListAll(DTOEntrada entidade, object repository, string filter, string sort)
        {
            if (filter == null && sort == null)
            {
                return (IList) repository
                                   .GetType()
                                   .GetMethod("ListAll", FlagsHelper.GetTypes<FilterAttribute>(entidade, FilterTypeFlags.ReadAll)
                                                  .ToArray())
                                   .Invoke(repository, FlagsHelper.GetValues<FilterAttribute>(entidade, FilterTypeFlags.ReadAll)
                                               .ToArray());
            }

            var filters = GetFilters(filter);

            var sorts = GetSorts(sort);

            return (IList) repository
                               .GetType()
                               .GetMethod("ListAll",
                                          new[]
                                              {
                                                  typeof (Dictionary<string, object>),
                                                  typeof (Dictionary<string, string>)
                                              })
                               .Invoke(repository, new object[] {filters, sorts});
        }

        private static Dictionary<string, object > GetFilters(string filter)
        {
            if (string.IsNullOrEmpty(filter)) return null;

            var filterList = filter.Split('|');

            return filterList.Select(s => s.Split('$')).ToDictionary(filterParts => filterParts[0], filterParts => (object) filterParts[1]);
        }

        private static Dictionary<string, string> GetSorts(string sort)
        {
            if (string.IsNullOrEmpty(sort)) return null;

            var sortList = sort.Split('|');

            return sortList.Select(s => s.Split('$')).ToDictionary(sortParts => sortParts[0], sortParts => sortParts[1]);
        }
    }
}