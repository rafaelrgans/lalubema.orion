﻿using System;
using System.Globalization;
using System.Reflection;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Domain.Services.Impl.Orion.Seguranca
{
    public class AutenticacaoService : ServiceBase, IAutenticacaoService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAutenticacao Autenticar(string chaveCriptografada)
        {
            try
            {
                var security = new ChaveCriptografadaHelper(chaveCriptografada);

                var usuarioClassName = GetUsuarioClassName(security.CodigoAplicacao);

                var usuario = Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

                ((IUsuario)usuario).Load(security.CodigoUsuario);

                var chaveCriptografadaUsuario = GetChaveCriptografada(security.CodigoAplicacao, usuario, security.CodigoCorporativo);

                var retornoAutenticacao = CreateInstanceRetornoAutenticacao(security.CodigoAplicacao);

                retornoAutenticacao = usuario.Transform(retornoAutenticacao);

                ((DTOAutenticacao)retornoAutenticacao).ChaveCriptografada = chaveCriptografadaUsuario;
                ((DTOAutenticacao)retornoAutenticacao).CodigoAplicacao = security.CodigoAplicacao;

                return (DTOAutenticacao)retornoAutenticacao;
            }
            catch (TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;

                throw;
            }
        }

        [Transaction(TransactionPropagation.Supports)]
        public DTOAutenticacao AutenticarPorSistema(string usuario, string senha, int? corporativo)
        {
            var aplicacao = AplicacaoRepositorio.Buscar(usuario);

            if (aplicacao == null)
                throw new UsuarioOuSenhaInvalidosException();

            var chaveCriptografada = aplicacao.Autenticar(senha, corporativo);

            var autenticacao = new DTOAutenticacao
                                   {
                                       ChaveCriptografada = chaveCriptografada,
                                       CodigoAplicacao = aplicacao.Codigo
                                   };

            return autenticacao;
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAutenticacao Autenticar(DTOTicket ticket)
        {
            const string inicialMetodo = "Autenticar";

            try
            {
                var retorno =
                    GetType().InvokeMember(inicialMetodo + ticket.Metodo,
                                           BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public |
                                           BindingFlags.Instance, null, this, new object[] { ticket.ChaveCriptografada, ticket })
                    as DTOAutenticacao;

                return retorno;
            }
            catch (TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;

                throw;
            }
        }

        protected DTOAutenticacao AutenticarPorUsuario(string chaveCriptografada, DTOTicket ticket)
        {
            try
            {
                var ticketUsuario = ticket as DTOTicketPorUsuario;

                var security = new ChaveCriptografadaHelper(chaveCriptografada);

                if (ticketUsuario == null)
                    throw new FalhaAutenticarUsuarioException();

                var usuarioClassName = GetUsuarioClassName(security.CodigoAplicacao);

                IUsuario usuario = (IUsuario)Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

                if (usuario is Lalubema.Orion.Domain.Corona.Usuario && security.CodigoCorporativo.HasValue)
                    trySetCorporativo(((Lalubema.Orion.Domain.Corona.Usuario)usuario).Repository, security.CodigoCorporativo.Value);

                usuario.Validar(ticketUsuario.Usuario, ticketUsuario.Senha, ticketUsuario.Versao);

                var chaveCriptografadaUsuario = GetChaveCriptografada(security.CodigoAplicacao, usuario, security.CodigoCorporativo);

                var retornoAutenticacao = CreateInstanceRetornoAutenticacao(security.CodigoAplicacao);

                retornoAutenticacao = usuario.Transform(retornoAutenticacao);

                ((DTOAutenticacao) retornoAutenticacao).ChaveCriptografada = chaveCriptografadaUsuario;
                ((DTOAutenticacao) retornoAutenticacao).CodigoAplicacao = security.CodigoAplicacao;
                ((DTOAutenticacao) retornoAutenticacao).UserName = usuario.Username;
				
				//if(usuario is Lalubema.Orion.Domain.Vanadium.Usuario)
				//    ((DTOAutenticacao) retornoAutenticacao).TipoUsuario = ((Lalubema.Orion.Domain.Vanadium.Usuario)(usuario)).UserType;
				
                return (DTOAutenticacao) retornoAutenticacao;
            }
            catch(TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                
                throw;
            }
        }

        private static string GetChaveCriptografada(int codigoAplicacao, object usuario, int? corporativo)
        {
            List<string> args = new List<string>();
            args.Add(CryptographyType.User.ConvertToString());
            args.Add(DateTime.UtcNow.ToString("o"));
            args.Add(((IUsuario) usuario).Codigo.ToString(CultureInfo.InvariantCulture));
            args.Add(((IUsuario) usuario).Username);
            args.Add(((IUsuario) usuario).Aplicacao.Versao);
            args.Add(codigoAplicacao.ToString(CultureInfo.InvariantCulture));
            if(corporativo.HasValue)
                args.Add(corporativo.Value.ToString(CultureInfo.InvariantCulture));
            return SecurityHelper.CreateCryptographyKey(args.ToArray());
        }

        private static Object CreateInstanceRetornoAutenticacao(int codigoAplicacao)
        {
            var retornoClassName = string.Format("Lalubema.Orion.Domain.DataTransferObject.{0}.DTOAutenticacaoPorUsuario", ((Aplicacao)codigoAplicacao).ConvertToString());

            if (!CheckTypeExists(string.Format("{0}, Lalubema.Orion.Domain.DataTransferObject", retornoClassName)))
                retornoClassName = "Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca.DTOAutenticacao";

            return Activator.CreateInstance("Lalubema.Orion.Domain.DataTransferObject", retornoClassName).Unwrap();
        }

        private static string GetUsuarioClassName(int codigoAplicacao)
        {
            var usuarioClassName = string.Format("Lalubema.Orion.Domain.{0}.Usuario",
                                                 ((Aplicacao) codigoAplicacao).ConvertToString());

            if(!CheckTypeExists(string.Format("{0}, Lalubema.Orion.Domain", usuarioClassName)))
                throw new InvalidOperationException("Valid type not found.");

            return usuarioClassName;
        }

        private static bool CheckTypeExists(string className)
        {
            var tipo = Type.GetType(className);

            return tipo != null;
        }
    }
}