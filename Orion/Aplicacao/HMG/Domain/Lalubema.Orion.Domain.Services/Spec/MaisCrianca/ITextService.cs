﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.MaisCrianca
{
    [ObjectMap("MaisCriancaTextDomainService", false)]
    public interface ITextService
    {
        IList<DTOTextRetorno> ListTextFeed(DTOPagingFilter pagingFilter);

        IList<DTOTextRetorno> ListTextTopFeed(DTOCategoryEntrada category);

        DTORetorno SendText(DTOTextEntrada text);

        DTORetorno Like(DTOTextEntrada text);

        DTOCommentRetorno Comment(DTOCommentEntrada comment);

        DTORetorno ChangeStatus(DTOTextEntrada text);

        DTORetorno Delete(DTOTextEntrada text);
    }
}