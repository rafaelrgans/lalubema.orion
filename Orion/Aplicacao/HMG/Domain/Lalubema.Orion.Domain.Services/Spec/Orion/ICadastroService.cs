﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Orion
{
    [ObjectMap("CadastroDomainService", false)]
    public interface ICadastroService
    {
        DTORetorno Salvar(DTOEntrada entidade);

        DTORetorno Excluir(DTOEntrada entidade);

        DTORetorno Listar(DTOEntrada entidade);

        IList<DTORetorno> ListAll(DTOEntrada entidade, string filter, string sort);

        DTORetorno Buscar(DTOEntrada entidade);
    }
}