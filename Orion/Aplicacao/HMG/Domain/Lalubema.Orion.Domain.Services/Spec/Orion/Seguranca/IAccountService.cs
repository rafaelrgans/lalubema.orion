﻿using System.IO;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca
{
    [ObjectMap("AccountDomainService", false)]
    public interface IAccountService
    {
        DTORetorno Register(DTOAccountRegister account);

        Stream CheckEmail(string chaveConfirmacao);

        DTORetorno RecoveryPassword(DTORecoveryPassword recoveryPassword);

        DTORetorno DirectDebit(DTODirectDebitRequest directDebitRequest);
    }
}