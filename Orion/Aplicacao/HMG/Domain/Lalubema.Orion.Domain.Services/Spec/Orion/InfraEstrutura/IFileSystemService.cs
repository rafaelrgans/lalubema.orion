﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Orion.InfraEstrutura
{
    [ObjectMap("FileSystemDomainService", false)]
    public interface IFileSystemService
    {
        DTOFileRetorno SaveFile(DTOFileEntrada file);
    }
}