﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Vanadium
{
    [ObjectMap("VanadiumPedidoDomainService", false)]
    public interface IPedidoService
    {
        DTOEntidadeRetorno RegistrarPedido(DTOPedidoEntrada pedidoEntrada);

        DTORetorno ComandarPedido(DTOPedidoEntrada pedidoEntrada);

        DTORetorno RemoverItens(DTOPedidoEntrada pedidoEntrada);
        
        DTORetorno FecharComanda(DTOPedidoEntrada pedidoEntrada);

        DTOMesasAbertasRetorno ListarMesasAbertas(string chaveCriptografada, int codigoEstabelecimento);

        DTOPedidoRetorno BuscarComanda(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

		DTOPedidoRetorno ListarItensPendentes(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

		DTOPedidoRetorno ListarPedidosEstabelecimento(string chaveCriptografada, int codigoEstabelecimento);

		DTOHistoricoPedidos ListarPedidosAbertosMenu(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

        DTOHistoricoPedidos ListarHistoricoPedidos(string chaveCriptografada, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim);

		DTORetorno TransferirMesaPedido(DTOTransfereMesaEntrada transferirMesa);

		DTORetorno TrocarMesa(DTOAlterarMesaEntrada alterarMesa);

		//DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio);

		DTORelatorioGarcomRetorno GerarRelatorioGarcom(DTORelatorioEntrada relatorio);

		DTORelatorioItemRetorno GerarRelatorioItem(DTORelatorioEntrada relatorio);

		DTORelatorioMesaRetorno GerarRelatorioMesa(DTORelatorioEntrada relatorio);
	}
}
