﻿using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Vanadium
{
    [ObjectMap("VanadiumEstabelecimentoDomainService", false)]
    public interface IEstabelecimentoService
    {
        DTOCategorias BuscarCategoriasCardapio(string chaveCriptografada, int codigoEstabelecimento);

        DTOCardapios BuscarCardapios(string chaveCriptografada, int codigoEstabelecimento);

        DTOItens BuscarDestaques(string chaveCriptografada, int codigoEstabelecimento);

        DTOImportacao Importar(string chaveCriptografada, int codigoEstabelecimento);

        DTOEstabelecimentos ListarEstabelecimentos(string chaveCriptografada);
    }
}