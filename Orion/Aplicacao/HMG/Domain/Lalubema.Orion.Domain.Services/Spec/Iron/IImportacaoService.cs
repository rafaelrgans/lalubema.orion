﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Iron
{
    [ObjectMap("IronImportacaoDomainService", false)]
    public interface IImportacaoService
    {
        DTOImportacao Importar(string chaveCriptografada, int codigoEmissora);
        
        DTORetorno ExportarAcessos(DTOExportarAcesso programas);
    }
}