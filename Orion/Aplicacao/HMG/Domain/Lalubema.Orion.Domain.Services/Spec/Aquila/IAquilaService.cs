﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Aquila
{
    [ObjectMap("AquilaDomainService", false)]
    public interface IAquilaService
    {
        DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada, int idPersonalidade);

        DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada);
        
        DTORetorno SalvarAlbum(string chaveCriptografada, DTOModifiedAlbum album);
    }
}