﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaLocalBemDomainService", false)]
    public interface ILocalBemService
    {
        DTOLocalBemsRetorno ListarLocalBem(string chaveCriptografada, int codigoEdificio);
        
        DTOReservasRetorno ListarReserva(string chaveCriptografada, int codigoLocalBem);

        DTOReservaLocalBemsRetorno ListarReservaUsuario(string chaveCriptografada, int codigoCondominio);

        DTORetorno Reservar(DTOReservaLocalBemEntrada reservaLocalBem);
    }
}