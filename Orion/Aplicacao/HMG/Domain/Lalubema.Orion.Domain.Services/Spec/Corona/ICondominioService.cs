﻿using System.IO;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaCondominioDomainService", false)]
    public interface ICondominioService
    {
        Stream CheckEmail(string chaveConfirmacao);
        
        DTORetorno RegisterDevice(DTORegisterDevice registerDevice);

        DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice);
    }
}