﻿using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Domain.Crux;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Orion.Integration.Spec.Crux;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Orion.Integration.Spec.Orion.Notification;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Spring;
using Lalubema.Utilitarios.Validacao;
using Agenda = Lalubema.Orion.Domain.Pegasus.Agenda;
using Categoria = Lalubema.Orion.Domain.Pegasus.Categoria;
using Equipe = Lalubema.Orion.Domain.Pegasus.Equipe;
using Item = Lalubema.Orion.Domain.Aquila.Item;
using Usuario = Lalubema.Orion.Domain.Pegasus.Usuario;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Utilitarios.Repositorios;
using System.Reflection;

namespace Lalubema.Orion.Domain.Services
{
    public class ServiceBase
    {
        protected void trySetCorporativo(IRepositorioBase repository, int corporativo)
        {
            PropertyInfo pi = repository.GetType().GetProperty("Corporativo", typeof(int?));
            if (pi != null)
                pi.SetValue(repository, corporativo, null);
        }

        protected IAplicacaoRepositorio<Aplicacao> AplicacaoRepositorio
        {
            get { return ObjectFactory.GetSingleton<IAplicacaoRepositorio<Aplicacao>>(); }
        }

        protected INotificationRepository NotificationRepository
        {
            get { return ObjectFactory.GetSingleton<INotificationRepository>(); }
        }

        #region [ Pegasus ]

        protected Integration.Spec.Pegasus.IAgendaRepositorio<Agenda> PegasusAgendaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Pegasus.IAgendaRepositorio<Agenda>>(); }
        }

        protected Integration.Spec.Pegasus.ICategoriaRepositorio<Categoria> PegasusCategoriaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Pegasus.ICategoriaRepositorio<Categoria>>(); }
        }

        protected IClubeRepositorio<Clube> PegasusClubeRepositorio
        {
            get { return ObjectFactory.GetSingleton<IClubeRepositorio<Clube>>(); }
        }

        protected Integration.Spec.Pegasus.IEquipeRepositorio<Equipe> EquipeRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Pegasus.IEquipeRepositorio<Equipe>>(); }
        }

        protected IEstiloRepositorio<Estilo> PegasusEstiloRepositorio
        {
            get { return ObjectFactory.GetSingleton<IEstiloRepositorio<Estilo>>(); }
        }

        protected IIndiceRepositorio<Indice> PegasusIndiceRepositorio
        {
            get { return ObjectFactory.GetSingleton<IIndiceRepositorio<Indice>>(); }
        }

        protected IProvaRepositorio<Prova> PegasusProvaRepositorio
        {
            get { return ObjectFactory.GetSingleton<IProvaRepositorio<Prova>>(); }
        }

        protected IMetaRepositorio<Meta> PegasusMetaRepositorio
        {
            get { return ObjectFactory.GetSingleton<IMetaRepositorio<Meta>>(); }
        }

        protected Integration.Spec.Pegasus.IUsuarioRepositorio<Usuario> PegasusUsuarioRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Pegasus.IUsuarioRepositorio<Usuario>>(); }
        }

        #endregion [ Pegasus ]

        #region [ Crux ]

        protected IBancaRepositorio<Banca> CruxBancaRepositorio
        {
            get { return ObjectFactory.GetSingleton<IBancaRepositorio<Banca>>(); }
        }

        protected ILivroRepositorio<Livro> CruxLivroRepositorio
        {
            get { return ObjectFactory.GetSingleton<ILivroRepositorio<Livro>>(); }
        }

        #endregion [ Crux ]

        #region [ Aquila ]

        protected IPersonalidadeRepositorio<Personalidade> AquilaPersonalidadeRepositorio
        {
            get { return ObjectFactory.GetSingleton<IPersonalidadeRepositorio<Personalidade>>(); }
        }

        protected IAlbumRepositorio<Album> AquilaAlbumRepositorio
        {
            get { return ObjectFactory.GetSingleton<IAlbumRepositorio<Album>>(); }
        }

        protected Integration.Spec.Aquila.IAgendaRepositorio<Agenda> AquilaAgendaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Aquila.IAgendaRepositorio<Agenda>>(); }
        }

        protected Integration.Spec.Aquila.IUsuarioRepositorio<Aquila.Usuario> AquilaUsuarioRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Aquila.IUsuarioRepositorio<Aquila.Usuario>>(); }
        }

        protected Integration.Spec.Aquila.IItemRepositorio<Item> AquilaItemRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Aquila.IItemRepositorio<Item>>(); }
        }

        #endregion [ Aquila ]

        #region [ Titanium ]

        protected ITorneioRepositorio<Torneio> TitaniumTorneioRepositorio
        {
            get { return ObjectFactory.GetSingleton<ITorneioRepositorio<Torneio>>(); }
        }

        protected Integration.Spec.Titanium.IUsuarioRepositorio<Titanium.Usuario> TitaniumUsuarioRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Titanium.IUsuarioRepositorio<Titanium.Usuario>>(); }
        }

        protected Integration.Spec.Vanadium.ICategoriaRepositorio<Vanadium.Categoria> VanadiumCategoriaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Vanadium.ICategoriaRepositorio<Vanadium.Categoria>>(); }
        }

        #endregion [ Titanium ]

        #region [ Vanadium ]

        protected ICardapioRepositorio<Cardapio> VanadiumCardapioRepositorio
        {
            get { return ObjectFactory.GetSingleton<ICardapioRepositorio<Cardapio>>(); }
        }

        protected IEstabelecimentoRepositorio<Estabelecimento> VanadiumEstabelecimentoRepositorio
        {
            get { return ObjectFactory.GetSingleton<IEstabelecimentoRepositorio<Estabelecimento>>(); }
        }

        protected IPedidoRepositorio<Pedido> VanadiumPedidoRepositorio
        {
            get { return ObjectFactory.GetSingleton<IPedidoRepositorio<Pedido>>(); }
        }

        protected Integration.Spec.Vanadium.IItemRepositorio<Vanadium.Item> VanadiumItemRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Vanadium.IItemRepositorio<Vanadium.Item>>(); }
        }

        #endregion [ Vanadium ]

        #region [ Iron ]

        protected Integration.Spec.Iron.IUsuarioRepositorio<Iron.Usuario> IronUsuarioRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IUsuarioRepositorio<Iron.Usuario>>(); }
        }

        protected Integration.Spec.Iron.ICategoriaRepositorio<Iron.Categoria> IronCategoriaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.ICategoriaRepositorio<Iron.Categoria>>(); }
        }

        protected Integration.Spec.Iron.IVideoRepositorio<Iron.Video> IronVideoRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IVideoRepositorio<Iron.Video>>(); }
        }

        protected Integration.Spec.Iron.IConteudoRepositorio<Iron.Conteudo> IronConteudoRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IConteudoRepositorio<Iron.Conteudo>>(); }
        }

        protected Integration.Spec.Iron.IComponenteRepositorio<Iron.Componente> IronComponenteRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IComponenteRepositorio<Iron.Componente>>(); }
        }

        protected Integration.Spec.Iron.IEquipeRepositorio<Iron.Equipe> IronEquipeRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IEquipeRepositorio<Iron.Equipe>>(); }
        }

        protected Integration.Spec.Iron.IProgramaRepositorio<Iron.Programa> IronProgramaRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IProgramaRepositorio<Iron.Programa>>(); }
        }

        protected Integration.Spec.Iron.IEmissoraRepositorio<Iron.Emissora> IronEmissoraRepositorio
        {
            get { return ObjectFactory.GetSingleton<Integration.Spec.Iron.IEmissoraRepositorio<Iron.Emissora>>(); }
        }

        #endregion [ Iron ]

        #region [ Mais Criança ]

        protected ITextRepositorio<Text> MaisCriancaTextRepositorio
        {
            get { return ObjectFactory.GetSingleton<ITextRepositorio<Text>>(); }
        }

        #endregion [ Mais Criança ]

        #region [ Validators ]

        private NotificationCollection _notificationCollection;

        protected NotificationCollection NotificationCollection
        {
            get { return _notificationCollection ?? (_notificationCollection = new NotificationCollection()); }
        }

        public IValidator<DTOAccountRegister> ValidadorAccountRegister
        {
            get { return ObjectFactory.GetObject<IValidator<DTOAccountRegister>>("ValidadorAccountRegister"); }
        }

        #region [ Cobalt - Cartório Fácil ]

        public IValidator<DTOServicoOnlineNotarialEntrada> ValidadorCobaltSolicitarServicoNotarial
        {
            get { return ObjectFactory.GetObject<IValidator<DTOServicoOnlineNotarialEntrada>>("CobaltValidadorSolicitarServicoNotarial"); }
        }

        public IValidator<DTOReenviarSenhaEntrada> ValidadorCobaltReenviarSenha
        {
            get { return ObjectFactory.GetObject<IValidator<DTOReenviarSenhaEntrada>>("CobaltValidadorReenviarSenha"); }
        }

        #endregion [ Cobalt - Cartório Fácil ]

        #endregion [ Validation ]
    }
}