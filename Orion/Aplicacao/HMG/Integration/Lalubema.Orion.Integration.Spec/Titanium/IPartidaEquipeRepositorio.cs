﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_PartidaEquipeRepositorio", true)]
    public interface IPartidaEquipeRepositorio<T> : IRepositorioBase<T>
    {
    }
}