﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_RodadaRepositorio", true)]
    public interface IRodadaRepositorio<T> : IRepositorioBase<T>
    {
    }
}