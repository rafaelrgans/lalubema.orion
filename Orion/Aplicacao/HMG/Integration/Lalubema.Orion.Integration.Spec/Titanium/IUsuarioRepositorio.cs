﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Consultar(string usuario, int codigoAplicacao);

        IList<T> ListarUsuariosDisponiveisEquipe(int codigoTorneio, int codigoEquipe);

        IList<T> ListarUsuariosTorneio(int codigoTorneio);

        IList<T> ListarUsuariosTorneio(int codigoTorneio, bool administrador);

        T BuscarUsuarioPorEmail(string email);
    }
}