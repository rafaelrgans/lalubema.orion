﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_TorneioEquipeRepositorio")]
    public interface ITorneioEquipeRepositorio<T> : IRepositorioBase<T>
    {
    }
}