﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_AtletaRepositorio", true)]
    public interface IAtletaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListarAtletasDisponiveisEquipe(int codigoTorneio, int codigoEquipe);

        IList<T> ListarAtletasTorneio(int codigoTorneio);

        IList<T> ListarAtletasTorneio(int codigoTorneio, bool administrador);

        T BuscarAtletaPorEmail(string email);
    }
}