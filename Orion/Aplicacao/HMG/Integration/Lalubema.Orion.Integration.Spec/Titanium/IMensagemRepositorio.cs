﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_MensagemRepositorio", IsSingleton = true)]
    public interface IMensagemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListarMensagens(int codigoTorneio);
    }
}