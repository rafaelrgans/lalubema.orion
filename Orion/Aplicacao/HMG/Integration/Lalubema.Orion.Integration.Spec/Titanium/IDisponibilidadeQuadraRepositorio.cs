﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_DisponibilidadeQuadraRepositorio", true)]
    public interface IDisponibilidadeQuadraRepositorio<T> : IRepositorioBase<T>
    {
    }
}