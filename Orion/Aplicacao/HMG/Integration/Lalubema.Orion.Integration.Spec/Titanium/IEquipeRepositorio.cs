﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_EquipeRepositorio", true)]
    public interface IEquipeRepositorio<T> : IRepositorioBase<T>
    {
    }
}