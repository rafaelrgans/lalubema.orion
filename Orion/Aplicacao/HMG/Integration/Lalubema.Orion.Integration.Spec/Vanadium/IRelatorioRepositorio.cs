﻿using System;
using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
	[ObjectMap("Vanadium_RelatorioRepositorio", true)]
	public interface IRelatorioRepositorio<T> : IRepositorioBase<T>
	{
		IList<T> GerarRelatorio(int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, TipoRelatorio ReportType);
	}
}
