﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Buscar(string usuario, string senha);

        T BuscarUsuarioPorEmail(string usuario);

        IList<T> ListAll(int codigoEstabelecimento);

        T Get(int id, int codigoEstabelecimento);
    }
}