﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_CategoriaRepositorio", true)]
    public interface ICategoriaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoEstabelecimento);

        T Get(int id, int codigoEstabelecimento);
    }
}