﻿using System;
using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_PedidoRepositorio", true)]
    public interface IPedidoRepositorio<T> : IRepositorioBase<T>
    {
		T GetPedidoAberto(int codigoMesa, int codigoEstabelecimento);

		T GetPedidoAberto(int codigoMesa, int codigoEstabelecimento, int CodUsuario);

		T GetTransferirMesaPedido(int CodigoPedido, int codigoEstabelecimento);

		IList<T> ListPedidosAbertos(int codigoEstabelecimento);

		IList<T> ListPedidosFechadosRelatorio(int codigoEstabelecimento, int codigoUsuario, DateTime DtInicio, DateTime DtFim);

		IList<T> ListarPedidosAbertosMenu(int codigoMesa, int codigoEstabelecimento);

		IList<T> ListarPedidosAbertosUsuario(int codigoUsuario);

		IList<T> ListarHistorioPedidosUsuario(int codigoUsuario, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim);

		IList<T> GerarRelatorio(int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, TipoRelatorio ReportType);
	}
}