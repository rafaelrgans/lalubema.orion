﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_TransactionRepositorio", true)]
    public interface ITransactionRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoResponsavel);
    }
}