﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_AlbumRepositorio", true)]
    public interface IAlbumRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int idPersonalidade);

        T Get(int codigo, int codigoPersonalidade);
    }
}