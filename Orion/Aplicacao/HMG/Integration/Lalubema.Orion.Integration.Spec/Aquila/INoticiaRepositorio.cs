﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_NoticiaRepositorio", true)]
    public interface INoticiaRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoPersonalidade);

        IList<T> ListAll(int codigoPersonalidade);
    }
}