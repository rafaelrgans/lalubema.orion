﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_PersonalidadeRepositorio", true)]
    public interface IPersonalidadeRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int idPersonalidade);

        T Get(string emailPersonalidade);

        T Get(int idPersonalidade, string emailPersonalidade);

        IList<T> ListAll(string emailPersonalidade);
    }
}