﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ReservaLocalBemRepositorio", true)]
    public interface IReservaLocalBemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoCondominio, int codigoUsuario);
    }
}