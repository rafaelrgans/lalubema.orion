﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ModeloMarcaRepositorio", true)]
    public interface IModeloMarcaRepositorio<T> : IRepositorioBase<T>
    {
    }
}