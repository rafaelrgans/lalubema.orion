﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_CorporativoRepositorio", true)]
    public interface ICorporativoRepositorio<T> : IRepositorioBase<T>
    {
    }
}