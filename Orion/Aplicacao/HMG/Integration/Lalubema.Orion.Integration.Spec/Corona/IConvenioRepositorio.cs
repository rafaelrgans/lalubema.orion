﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ConvenioRepositorio", true)]
    public interface IConvenioRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int usuario);
    }
}