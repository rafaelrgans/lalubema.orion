﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_CategoriaConvenioRepositorio", true)]
    public interface ICategoriaConvenioRepositorio<T> : IRepositorioBase<T>
    {
    }
}