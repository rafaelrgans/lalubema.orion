﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_VeiculoCondominioRepositorio", true)]
    public interface IVeiculoCondominioRepositorio<T> : IRepositorioBase<T>
    {
    }
}