﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_LicencaRepositorio", true)]
    public interface ILicencaRepositorio<T> : IRepositorioBase<T>
    {
    }
}