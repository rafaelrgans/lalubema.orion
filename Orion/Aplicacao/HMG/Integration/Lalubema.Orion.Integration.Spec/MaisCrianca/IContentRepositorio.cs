﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_ContentRepositorio", true)]
    public interface IContentRepositorio<T> : IRepositorioBase<T>
    {
    }
}