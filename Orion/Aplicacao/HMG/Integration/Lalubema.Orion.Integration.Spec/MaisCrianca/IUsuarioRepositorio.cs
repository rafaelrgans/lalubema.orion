﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Get(string usuario, string senha);

        T Get(string username);

        T Buscar(string username, string password);
    }
}