﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_CategoryRepositorio", true)]
    public interface ICategoryRepositorio<T> : IRepositorioBase<T>
    {
    }
}