﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_MediaRepositorio", true)]
    public interface IMediaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListMediaFeed(int pageNumber, int recordsPerPage, TipoAlbum mediaType);
    }
}