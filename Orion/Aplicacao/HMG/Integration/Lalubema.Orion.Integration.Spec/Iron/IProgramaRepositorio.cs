﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Iron
{
    [ObjectMap("Iron_ProgramaRepositorio", true)]
    public interface IProgramaRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoEmissora);

        IList<T> ListAll(int codigoEmissora);
    }
}