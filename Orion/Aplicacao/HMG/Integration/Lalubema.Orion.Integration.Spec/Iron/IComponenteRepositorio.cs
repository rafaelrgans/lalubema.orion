﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Iron
{
    [ObjectMap("Iron_ComponenteRepositorio", true)]
    public interface IComponenteRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoEmissora);

        IList<T> ListAll(int codigoEmissora);
    }
}