﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Crux
{
    [ObjectMap("Crux_LivroRepositorio", true)]
    public interface ILivroRepositorio<T> : IRepositorioBase<T>
    {
    }
}