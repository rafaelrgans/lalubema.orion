﻿using System;
using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;

namespace Lalubema.Orion.Integration.Spec.Cobalt
{
    [ObjectMap("Cobalt_DicaRepositorio", true)]
    public interface IDicaRepositorio<T> : IRepositorioBase<T>
    {
		T Get(int codigo);

        IList<T> ListAll(DateTime lastSync, bool exibirOnly);

		IList<T> ListAll(bool exibirOnly);

		IList<T> ListAll(int codigo);
	}
}