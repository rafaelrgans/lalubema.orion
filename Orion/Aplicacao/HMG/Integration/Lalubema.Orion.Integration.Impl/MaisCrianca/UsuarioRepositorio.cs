﻿using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.MaisCrianca
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Get(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Usuario>(criteria);
        }

        public Usuario Get(string username)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", username));

            return Execute<Usuario>(criteria);
        }

        public Usuario Buscar(string username, string password)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", username))
                .Add(Restrictions.Eq("u.Password", password));

            return Execute<Usuario>(criteria);
        }
    }
}