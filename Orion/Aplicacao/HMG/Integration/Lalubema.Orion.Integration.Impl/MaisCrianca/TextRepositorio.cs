﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using System.Linq;
using NHibernate.Linq;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.MaisCrianca
{
    public class TextRepositorio : RepositorioBase<Text>, ITextRepositorio<Text>
    {
        public IList<Text> ListTextFeed(int pageNumber, int recordsPerPage, int category, TextStatus status, int teiaVida)
        {
            var criteria =
                QueryOver.Of<Text>()
                         .OrderBy(Projections.Property("Created"))
                         .Desc.Skip(pageNumber*recordsPerPage)
                         .Take(recordsPerPage)
                         .DetachedCriteria.CreateAlias("Category", "c", JoinType.LeftOuterJoin);

            if (teiaVida > 0)
                criteria = criteria.Add(Restrictions.Eq("c.TeiaVida", teiaVida));

            if (category > 0)
                criteria =
                    criteria.Add(Restrictions.Or(Restrictions.Eq("c.Codigo", category),
                                                 Restrictions.Eq("c.General", true)));

            if (status != TextStatus.Undefined)
                criteria = criteria.Add(Restrictions.Eq("Status", status));

            return ToList<Text>(criteria);
        }

        public IList<Text> ListTextTopFeed(int maxRecordsTopTextFeed, int category)
        {
            return Session.Query<Text>()
                .OrderByDescending(o => o.Activities.Count)
                .Take(maxRecordsTopTextFeed)
                .ToList()
                .Where(o => o.Status == TextStatus.Approved)
                .ToList();
        }
    }
}