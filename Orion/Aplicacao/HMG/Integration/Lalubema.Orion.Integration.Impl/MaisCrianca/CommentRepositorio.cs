﻿using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.MaisCrianca
{
    public class CommentRepositorio : RepositorioBase<Comment>, ICommentRepositorio<Comment>
    {
    }
}