﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class AlbumRepositorio : RepositorioBase<Album>, IAlbumRepositorio<Album>
    {
        public IList<Album> ListAll(int idPersonalidade)
        {
            var criteria = DetachedCriteria.For<Album>("a")
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("p.Codigo", idPersonalidade))
				.AddOrder(Order.Desc("a.Data"));

            return ToList<Album>(criteria);
        }

        public Album Get(int codigo, int codigoPersonalidade)
        {
            var criteria = DetachedCriteria.For<Album>("a")
                .Add(Restrictions.Eq("a.Codigo", codigo))
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
				.AddOrder(Order.Desc("a.Data"));

            return Execute<Album>(criteria);
        }
    }
}