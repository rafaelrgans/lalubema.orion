﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class AgendaRepositorio : RepositorioBase<Agenda>, IAgendaRepositorio<Agenda>
    {
        public IList<Agenda> ListAll(int codigoPersonalidade)
        {
            var criteria = DetachedCriteria.For<Agenda>("a")
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
				.AddOrder(Order.Desc("a.Data"));

            return ToList<Agenda>(criteria);
        }

        public Agenda Get(int codigo, int codigoPersonalidade)
        {
            var criteria = DetachedCriteria.For<Agenda>("a")
                .Add(Restrictions.Eq("a.Codigo", codigo))
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
				.AddOrder(Order.Desc("a.Data"));

            return Execute<Agenda>(criteria);
        }
    }
}