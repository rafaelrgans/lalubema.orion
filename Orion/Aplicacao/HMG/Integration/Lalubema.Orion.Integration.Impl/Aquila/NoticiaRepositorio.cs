﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class NoticiaRepositorio : RepositorioBase<Noticia>, INoticiaRepositorio<Noticia>
    {
		public Noticia Get(int codigo, int codigoPersonalidade)
		{
			var criteria = DetachedCriteria.For<Noticia>("o")
				.CreateAlias("o.Personalidade", "p")
				.Add(Restrictions.Eq("o.Codigo", codigo))
				.Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
				.AddOrder(Order.Desc("o.Data"));

			return Execute<Noticia>(criteria);
		}

		public IList<Noticia> ListAll(int codigoPersonalidade)
		{
			var criteria = DetachedCriteria.For<Noticia>("o")
				.CreateAlias("o.Personalidade", "p", JoinType.InnerJoin)
				.Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
				.AddOrder(Order.Desc("o.Data"));

			return ToList<Noticia>(criteria);
		}
    }
}