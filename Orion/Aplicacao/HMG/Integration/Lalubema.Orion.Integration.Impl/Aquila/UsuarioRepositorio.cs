﻿using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Buscar(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Usuario>(criteria);
        }

        public Usuario Consultar(string usuario, int codigoAplicacao)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Aplicacao.Codigo", codigoAplicacao));

            return Execute<Usuario>(criteria);
        }
    }
}