﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class CategoriaRepositorio : RepositorioBase<Categoria>, ICategoriaRepositorio<Categoria>
    {
        public Categoria Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Categoria>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Categoria>(criteria);
        }

        public IList<Categoria> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Categoria>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }
    }
}