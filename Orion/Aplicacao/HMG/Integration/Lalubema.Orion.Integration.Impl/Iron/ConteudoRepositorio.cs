﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class ConteudoRepositorio : RepositorioBase<Conteudo>, IConteudoRepositorio<Conteudo>
    {
        public Conteudo Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Conteudo>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Conteudo>(criteria);
        }

        public IList<Conteudo> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Conteudo>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora))
                .AddOrder(Order.Asc("p.Nome"))
                .AddOrder(Order.Asc("o.Tipo"))
                .AddOrder(Order.Asc("o.Ordem")); ;

            return ToList(criteria);
        }

        public IList<Conteudo> ListByTipo(int codigoPrograma, TipoConteudo tipoConteudo)
        {
            var criteria = DetachedCriteria.For<Conteudo>("c")
                .CreateAlias("c.Programa", "p")
                .Add(Restrictions.Eq("p.Codigo", codigoPrograma))
                .Add(Restrictions.Eq("c.Tipo", tipoConteudo));

            return ToList<Conteudo>(criteria);
        }

        public override IList<Conteudo> ListAll()
        {
            var criteria = DetachedCriteria.For<Conteudo>("c")
                .CreateAlias("c.Programa", "p")
                .AddOrder(Order.Asc("p.Nome"))
                .AddOrder(Order.Asc("c.Tipo"))
                .AddOrder(Order.Asc("c.Ordem"));

            return ToList<Conteudo>(criteria);
        }
    }
}