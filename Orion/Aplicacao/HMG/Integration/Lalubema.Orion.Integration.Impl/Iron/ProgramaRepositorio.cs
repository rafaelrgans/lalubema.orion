﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class ProgramaRepositorio : RepositorioBase<Programa>, IProgramaRepositorio<Programa>
    {
        public Programa Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Programa>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Programa>(criteria);
        }

        public IList<Programa> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Programa>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }
    }
}