﻿using System.Collections.Generic;
using System;
using NHibernate.Criterion;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Orion.Domain.Corona;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class NotificacaoRepositorio : RepositorioBase<Notificacao>, INotificacaoRepositorio<Notificacao>
    {
        public IList<Notificacao> ListAll(DateTime begin, DateTime end)
        {
            var criteria = DetachedCriteria.For<Notificacao>("n")
               .Add(Restrictions.Between("n.DataNotificacao", begin, end));

            return ToList<Notificacao>(criteria);
        }

        public IList<Notificacao> ListAllByRemetente(int remetente, DateTime begin, DateTime end)
        {
            begin = new DateTime(2015, 06, 28);
            var criteria = DetachedCriteria.For<Notificacao>("n")
               .Add(Restrictions.Eq("n.Remetente.Codigo", remetente))
               .Add(Restrictions.Between("n.DataNotificacao", begin, end));

            return ToList<Notificacao>(criteria);
        }
    }
}