﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class CategoriaConvenioRepositorio : RepositorioBase<CategoriaConvenio>,
                                                ICategoriaConvenioRepositorio<CategoriaConvenio>
    {
    }
}