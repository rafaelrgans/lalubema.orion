﻿using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class CompromissoRepositorio : RepositorioBase<Compromisso>, ICompromissoRepositorio<Compromisso>
    {
    }
}