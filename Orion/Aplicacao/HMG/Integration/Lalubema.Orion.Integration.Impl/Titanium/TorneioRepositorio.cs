﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class TorneioRepositorio : RepositorioBase<Torneio>, ITorneioRepositorio<Torneio>
    {
        #region ITorneioRepositorio<Torneio> Members

        public IList<Torneio> ListAll(string email)
        {
            var criteria =
                DetachedCriteria.For<Torneio>("t").CreateAlias("t.Usuarios", "a", JoinType.InnerJoin).Add(
                    Restrictions.Eq("a.Username", email));

            return ToList(criteria);
        }

        public Torneio BuscarTorneioUsuario(int codigoTorneio, string emailUsuario)
        {
            var criteria =
                DetachedCriteria.For<Torneio>("t").Add(Restrictions.Eq("t.Codigo", codigoTorneio)).CreateAlias(
                    "t.Usuarios", "a", JoinType.InnerJoin).Add(Restrictions.Eq("a.Username", emailUsuario));

            return Execute<Torneio>(criteria);
        }

        #endregion
    }
}