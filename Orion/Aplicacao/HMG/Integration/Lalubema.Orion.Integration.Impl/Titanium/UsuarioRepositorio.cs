﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Consultar(string usuario, int codigoAplicacao)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Aplicacao.Codigo", codigoAplicacao));

            return Execute<Usuario>(criteria);
        }

        public IList<Usuario> ListarUsuariosDisponiveisEquipe(int codigoTorneio, int codigoEquipe)
        {
            var criteria =
                DetachedCriteria.For<Usuario>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).CreateAlias(
                    "a.Equipes", "e", JoinType.LeftOuterJoin).Add(Restrictions.Eq("t.Torneio.Codigo", codigoTorneio)).Add(
                        Restrictions.Eq("t.Status", Common.Tipos.StatusUsuarioTorneio.Participante));

            criteria = codigoEquipe > 0
                           ? criteria.Add(Restrictions.Or(Restrictions.Eq("e.Codigo", codigoEquipe), Restrictions.IsNull("e.Codigo")))
                           : criteria.Add(Restrictions.IsNull("e.Codigo"));

            return ToList(criteria);
        }

        public IList<Usuario> ListarUsuariosTorneio(int codigoTorneio)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Usuario>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).Add(
                    Restrictions.Eq("t.Status", Common.Tipos.StatusUsuarioTorneio.Participante));

            return ToList(criteria);
        }

        public IList<Usuario> ListarUsuariosTorneio(int codigoTorneio, bool administrador)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Usuario>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).Add(
                    Restrictions.Eq("t.Administrador", administrador));

            return ToList(criteria);
        }

        public Usuario BuscarUsuarioPorEmail(string email)
        {
            var criteria = DetachedCriteria.For<Usuario>("a").Add(Restrictions.Eq("a.Username", email));

            return Execute<Usuario>(criteria);
        }
    }
}