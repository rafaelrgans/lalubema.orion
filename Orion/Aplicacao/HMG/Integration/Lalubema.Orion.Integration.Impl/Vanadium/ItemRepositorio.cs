﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class ItemRepositorio : RepositorioBase<Item>, IItemRepositorio<Item>
    {
        public IList<Item> ListAll(int codigoEstabelecimento)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Item>("i").CreateAlias("i.Cardapio", "c").Add(
                                                                                   Restrictions.Eq(
                                                                                                   "c.Estabelecimento.Codigo",
                                                                                                   codigoEstabelecimento));

            return ToList(criteria);
        }

        public Item Get(int id, int codigoEstabelecimento)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Item>("i").Add(Restrictions.Eq("i.Codigo", id)).CreateAlias("i.Cardapio", "c").Add(
                                                                                                                        Restrictions
                                                                                                                            .
                                                                                                                            Eq
                                                                                                                            ("c.Estabelecimento.Codigo",
                                                                                                                             codigoEstabelecimento));

            return Execute<Item>(criteria);
        }

        public IList<Item> ListarDestaques(int codigoEstabelecimento)
        {
            var criteria =
                DetachedCriteria.For<Item>("i").Add(Restrictions.Eq("i.Destaque", true)).CreateAlias("i.Cardapio", "c").
                    Add(Restrictions.Eq("c.Estabelecimento.Codigo", codigoEstabelecimento));

            return ToList<Item>(criteria);
        }
    }
}