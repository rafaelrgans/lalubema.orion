﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class CardapioRepositorio : RepositorioBase<Cardapio>, ICardapioRepositorio<Cardapio>
    {
        public IList<Cardapio> ListAll(int codigoEstabelecimento)
        {
            var criteria =
                DetachedCriteria.For<Cardapio>("c")
                                .Add(Restrictions.Eq("c.Estabelecimento.Codigo", codigoEstabelecimento));

            return ToList(criteria);
        }

        public Cardapio Get(int id, int codigoEstabelecimento)
        {
            var criteria =
                DetachedCriteria.For<Cardapio>("c")
                                .Add(Restrictions.Eq("c.Estabelecimento.Codigo", codigoEstabelecimento)).SetMaxResults(1);

            return Execute<Cardapio>(criteria);
        }
    }
}