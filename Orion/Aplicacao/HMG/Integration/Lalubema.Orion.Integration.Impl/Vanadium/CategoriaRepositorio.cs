﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class CategoriaRepositorio : RepositorioBase<Categoria>, ICategoriaRepositorio<Categoria>
    {
        public IList<Categoria> ListAll(int codigoEstabelecimento)
        {
            var criteria = DetachedCriteria.For<Categoria>("c")
                .Add(Restrictions.Eq("c.Estabelecimento.Codigo", codigoEstabelecimento))
                .AddOrder(new Order("c.Ordem", true));

            return ToList(criteria);
        }

        public Categoria Get(int id, int codigoEstabelecimento)
        {
            var criteria = DetachedCriteria.For<Categoria>("c")
                .Add(Restrictions.Eq("c.Codigo", id))
                .Add(Restrictions.Eq("c.Estabelecimento.Codigo", codigoEstabelecimento));

            return Execute<Categoria>(criteria);
        }
    }
}