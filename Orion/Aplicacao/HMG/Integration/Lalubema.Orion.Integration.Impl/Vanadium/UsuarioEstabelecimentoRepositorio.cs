﻿using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class UsuarioEstabelecimentoRepositorio : RepositorioBase<UsuarioEstabelecimento>,
                                                     IUsuarioEstabelecimentoRepositorio<UsuarioEstabelecimento>
    {
    }
}