﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class EstiloRepositorio : RepositorioBase<Estilo>, IEstiloRepositorio<Estilo>
    {
        public IList<Estilo> ListAll(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Estilo>("e").Add(Restrictions.Eq("e.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}