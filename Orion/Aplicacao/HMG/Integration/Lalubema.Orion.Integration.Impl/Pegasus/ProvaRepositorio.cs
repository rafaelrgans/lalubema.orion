﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class ProvaRepositorio : RepositorioBase<Prova>, IProvaRepositorio<Prova>
    {
        public IList<Prova> ListAll(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Prova>("p").Add(Restrictions.Eq("p.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}