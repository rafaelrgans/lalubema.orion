﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class EquipeRepositorio : RepositorioBase<Equipe>, IEquipeRepositorio<Equipe>
    {
        public IList<Equipe> ListAll(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Equipe>("e").Add(Restrictions.Eq("e.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}