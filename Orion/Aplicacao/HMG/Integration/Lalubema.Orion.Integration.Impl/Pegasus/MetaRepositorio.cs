﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class MetaRepositorio : RepositorioBase<Meta>, IMetaRepositorio<Meta>
    {
        public IList<Meta> ListAll(int codigoUsuario)
        {
            var criteria =
                DetachedCriteria.For<Meta>("a").Add(Restrictions.Eq("a.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}