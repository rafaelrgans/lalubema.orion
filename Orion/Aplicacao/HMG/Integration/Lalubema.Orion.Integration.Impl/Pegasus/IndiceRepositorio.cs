﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class IndiceRepositorio : RepositorioBase<Indice>, IIndiceRepositorio<Indice>
    {
        public IList<Indice> ListAll(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Indice>("i").Add(Restrictions.Eq("i.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}