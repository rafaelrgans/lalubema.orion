﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class AgendaRepositorio : RepositorioBase<Agenda>, IAgendaRepositorio<Agenda>
    {
        public IList<Agenda> ListAll(int codigoUsuario)
        {
            var criteria =
                DetachedCriteria.For<Agenda>("a").Add(Restrictions.Eq("a.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}