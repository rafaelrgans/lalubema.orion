﻿using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace Lalubema.Orion.Integration.Impl.Orion
{
    public class ParametroRepositorio : RepositorioBase<Parametro>, IParametroRepositorio<Parametro>
    {
        public Parametro BuscarPorChave(string nomeParametro)
        {
            var filtro = nomeParametro.Split('.');

            var criteria = DetachedCriteria.For<Parametro>("p").Add(Restrictions.Eq("p.Nome", filtro[filtro.Length - 1]));

            var parametros = ToList(criteria);

            foreach (var parametro in parametros)
            {
                var parametroAtual = parametro;

                for (var i = filtro.Length - 1; i >= 0; i--)
                {
                    if (!parametroAtual.Nome.Equals(filtro[i], StringComparison.InvariantCultureIgnoreCase))
                        break;

                    if (i == 0 && IsRoot(parametroAtual.Pai))
                        return parametro;

                    if (parametro.Pai != null)
                        parametroAtual = parametroAtual.Pai;
                }
            }

            return null;
        }

        private static bool IsRoot(Parametro parametro)
        {
            return parametro != null && parametro.Pai == null;
        }

        public Parametro Get(string nomeParametro)
        {
            const string sql = @"WITH Result(Codigo, Nome, ParametroPai, ConteudoPRD, ConteudoHMG, ConteudoTST, ConteudoDSV) AS 
	                            (
		                            SELECT
			                            Codigo, Nome, ParametroPai, ConteudoPRD, ConteudoHMG, ConteudoTST, ConteudoDSV
		                            FROM
			                            Parametro
		                            WHERE
			                            Nome Like :nomeParametro
		
		                            UNION ALL
		
		                            SELECT
			                            p.Codigo, p.Nome, p.ParametroPai, p.ConteudoPRD, p.ConteudoHMG, p.ConteudoTST, p.ConteudoDSV
		                            FROM
			                            Parametro as p, Result
		                            WHERE
			                            Result.ParametroPai = p.Codigo
	                            )
                            SELECT DISTINCT
	                            Codigo, Nome, ConteudoPRD, ConteudoHMG, ConteudoTST, ConteudoDSV
                            FROM
	                            Result
                            ORDER BY
	                            Result.Codigo";

            var filtro = nomeParametro.Split('.');

            var parametros =
                Session.CreateSQLQuery(sql)
                .SetParameter("nomeParametro", filtro[filtro.Length - 1])
                .SetResultTransformer(new AliasToBeanResultTransformer(typeof (Parametro)))
                .List<Parametro>();

            foreach (var parametro in parametros)
            {
                var parametroAtual = parametro;

                for (var i = filtro.Length - 1; i >= 0; i--)
                {
                    if(!parametroAtual.Nome.Equals(filtro[i], StringComparison.InvariantCultureIgnoreCase))
                        break;

                    if (i == 0)
                        return parametro;

                    if(parametro.Pai != null)
                        parametroAtual = parametroAtual.Pai;
                }
            }

            return null;
        }

    }
}