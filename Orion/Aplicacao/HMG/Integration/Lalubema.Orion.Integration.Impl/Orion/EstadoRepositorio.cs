﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Orion
{
    public class EstadoRepositorio : RepositorioBase<Estado>, IEstadoRepositorio<Estado>
    {
    }
}