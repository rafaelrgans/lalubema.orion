﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Orion
{
    public class AplicacaoRepositorio : RepositorioBase<Aplicacao>, IAplicacaoRepositorio<Aplicacao>
    {
        public Aplicacao Buscar(string nomeAplicacao, string versao)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Aplicacao>("a").Add(Restrictions.Eq("a.Nome", nomeAplicacao)).Add(Restrictions.Eq("a.Versao", versao));

            return Execute<Aplicacao>(criteria);
        }

        public Aplicacao Buscar(string nomeAplicacao)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Aplicacao>("a").Add(Restrictions.Eq("a.Nome", nomeAplicacao));

            return Execute<Aplicacao>(criteria);
        }
    }
}