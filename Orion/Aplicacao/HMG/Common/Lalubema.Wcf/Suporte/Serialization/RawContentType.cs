﻿using System.ServiceModel.Channels;

namespace Lalubema.Wcf.Suporte.Serialization
{
    public class RawContentType : WebContentTypeMapper
    {
        public override WebContentFormat GetMessageFormatForContentType(string contentType)
        {
            return WebContentFormat.Json;
        }
    }
}