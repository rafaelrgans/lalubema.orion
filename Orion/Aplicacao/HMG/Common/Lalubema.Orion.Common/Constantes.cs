﻿namespace Lalubema.Orion.Common
{
    public class Constantes
    {
        public class Orion
        {
            public class Comunicacao
            {
                public class Email
                {
                    public const string SmtpServer = "ORION.COMUNICACAO.EMAIL.SMTP_SERVER";
                    public const string SmtpUseSSL = "ORION.COMUNICACAO.EMAIL.SMTP_USESSL";
                    public const string SmtpServerPort = "ORION.COMUNICACAO.EMAIL.SMTP_SERVERPORT";
                    public const string UsuarioSmtp = "ORION.COMUNICACAO.EMAIL.USUARIO_SMTP";
                    public const string SenhaSmtp = "ORION.COMUNICACAO.EMAIL.SENHA_SMTP";
                }
            }

            public class InfraEstrutura
            {
                public class FileSystem
                {
                    public const string BasePath = "ORION.INFRA_ESTRUTURA.FILE_SYSTEM.BASE_PATH";
                    public const string BaseUrl = "ORION.INFRA_ESTRUTURA.FILE_SYSTEM.BASE_URL";
                }
            }
        }

        public class Corona
        {
            public class Configuracao
            {
                // Endereço Lojas
                public const string GooglePlayUrl = "CORONA.CONFIGURACAO.GOOGLE_PLAY_URL";
                public const string ItunesUrl = "CORONA.CONFIGURACAO.ITUNES_URL";
            }

            public class Comunicacao
            {
                public class Email
                {
                    public const string Remetente = "CORONA.COMUNICACAO.EMAIL.EMAIL_REMETENTE";
                }
            }

            public class Usuario
            {
                public class Comunicacao
                {
                    // Convite de Cadastro síndico
                    //public const string LinkConfirmacao = "CORONA.USUARIO.COMUNICACAO.LINK_CONFIRMACAO";
                    //public const string TemplateEmailConvite = "CORONA.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONVITE";
                    //public const string AssuntoEmailConvite = "CORONA.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_CONVITE";

                    // Convite de Cadastro condomino
                    //public const string TemplateEmailConviteCondomino = "CORONA.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONVITE_COND";
                    //public const string AssuntoEmailConviteCondomino = "CORONA.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_CONVITE_COND";

                    public const string LINK_EMAIL_ATIVACAO = "CORONA.USUARIO.COMUNICACAO.LINK_CONFIRMACAO";
                    public const string ASSUNTO_EMAIL_ATIVACAO = "CORONA.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_ATIVACAO";
                    public const string TEMPLATE_EMAIL_ATIVACAO = "CORONA.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_ATIVACAO";

                    public const string ASSUNTO_CADASTRO_NAOACEITO = "CORONA.USUARIO.COMUNICACAO.ASSUNTO_CADASTRO_NAOACEITO";
                    public const string TEMPLATE_CADASTRO_NAOACEITO = "CORONA.USUARIO.COMUNICACAO.TEMPLATE_CADASTRO_NAOACEITO";

                    // Recuperação de senha
                    public const string ASSUNTO_EMAIL_RECUPERAR_SENHA = "CORONA.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_RECUPERAR_SENHA";
                    public const string TEMPLATE_EMAIL_RECUPERAR_SENHA = "CORONA.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_RECUPERAR_SENHA";
                    public const string LINK_EMAIL_RECUPERAR_SENHA = "CORONA.USUARIO.COMUNICACAO.LINK_EMAIL_RECUPERAR_SENHA";
                }

                public const string TemplateAtivacao = "CORONA.USUARIO.TEMPLATE_ATIVACAO";
            }

            public class UnidadeHabitacional
            {
                //public const string AssuntoEmailConvite = "CORONA.UNIDADE_HABITACIONAL.ASSUNTO_EMAIL_CONVITE";
                //public const string TemplateEmailConvite = "CORONA.UNIDADE_HABITACIONAL.TEMPLATE_EMAIL_CONVITE";
                //public const string LinkConfirmacao = "CORONA.UNIDADE_HABITACIONAL.LINK_CONFIRMACAO";
                public const string TemplateAtivacao = "CORONA.UNIDADE_HABITACIONAL.TEMPLATE_ATIVACAO";
            }

            public class Condominio
            {
                //public const string AssuntoEmailConvite = "CORONA.CONDOMINIO.ASSUNTO_EMAIL_CONVITE";
                //public const string TemplateEmailConvite = "CORONA.CONDOMINIO.TEMPLATE_EMAIL_CONVITE";
                //public const string LinkConfirmacao = "CORONA.CONDOMINIO.LINK_CONFIRMACAO";
                public const string TemplateAtivacao = "CORONA.CONDOMINIO.TEMPLATE_ATIVACAO";
            }
        }

        public class Vanadium
        {
            public class Pedido
            {
                public const string ComEstabelecimentoMaximoDiasHistorico = "VANADIUM.PEDIDO.COM_ESTABELECIMENTO_MAX_DIAS_HISTORICO_PEDIDO";
                public const string SemEstabelecimentoMaximoDiasHistorico = "VANADIUM.PEDIDO.SEM_ESTABELECIMENTO_MAX_DIAS_HISTORICO_PEDIDO";
            }

            public class Configuracao
            {
                public const string LimiteItensEmDestaque = "VANADIUM.CONFIGURACAO.LIMITE_ITENS_DESTAQUE";
                public const string QuantidadeMesasPadrao = "VANADIUM.CONFIGURACAO.QTD_MESAS_PADRAO";
            }

            public class Comunicacao
            {
                public class Email
                {
                    public const string Remetente = "VANADIUM.COMUNICACAO.EMAIL.EMAIL_REMETENTE";
                    public const string Password = "VANADIUM.COMUNICACAO.EMAIL.SENHA_REMETENTE";
                }
            }

            public class Usuario
            {
                public class Comunicacao
                {
                    // Confirmação de Cadastro
                    public const string LinkConfirmacao = "VANADIUM.USUARIO.COMUNICACAO.LINK_CONFIRMACAO";
                    public const string TemplateEmailConfirmacao = "VANADIUM.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONFIRMACAO";
                    public const string TemplateEmailConfirmacaoCliente = "VANADIUM.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONFIRMACAO_CLIENTE";
                    public const string AssuntoEmailConfirmacao = "VANADIUM.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_CONFIRMACAO";

                    // Recuperação de Senha
                    public const string TemplateEmailRecuperacaoSenha = "VANADIUM.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_PASS_RECOVERY";
                    public const string AssuntoEmailRecuperacaoSenha = "VANADIUM.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_PASS_RECOVERY";
                }
            }
        }

        public class Titanium
        {
            public class Configuracao
            {
                // Endereço Lojas
                public const string GooglePlayUrl = "TITANIUM.CONFIGURACAO.GOOGLE_PLAY_URL";
                public const string ItunesUrl = "TITANIUM.CONFIGURACAO.ITUNES_URL";
            }

            public class Comunicacao
            {
                public class Email
                {
                    public const string Remetente = "TITANIUM.COMUNICACAO.EMAIL.EMAIL_REMETENTE";
                }
            }

            public class Usuario
            {
                public class Comunicacao
                {
                    public const string TemplateEmailConvite = "TITANIUM.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONVITE";
                    public const string AssuntoConviteTorneio = "TITANIUM.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_CONVITE";

                    public const string LinkConfirmacao = "TITANIUM.USUARIO.COMUNICACAO.LINK_CONFIRMACAO";
                    public const string TemplateEmailConfirmacao = "TITANIUM.USUARIO.COMUNICACAO.TEMPLATE_EMAIL_CONFIRMACAO";
                    public const string AssuntoEmailConfirmacao = "TITANIUM.USUARIO.COMUNICACAO.ASSUNTO_EMAIL_CONFIRMACAO";
                }
            }
        }

        public class Iron
        {
            public class Configuracao
            {
                public const string LimiteItensConteudo = "IRON.CONFIGURACAO.LIMITE_ITENS_CONTEUDO";
                public const string LimiteItensGrafico = "IRON.CONFIGURACAO.LIMITE_ITENS_GRAFICO";
                public const string LimiteItensAudiencia = "IRON.CONFIGURACAO.LIMITE_ITENS_AUDIENCIA";
            }
        }

        public class MaisCrianca
        {
            public class Configuration
            {
                public const string MaxTextRecordsPerPage = "MAIS_CRIANCA.CONFIGURATION.MAX_TEXT_RECORDS_PER_PAGE";
                public const string MaxRecordsTopTextFeed = "MAIS_CRIANCA.CONFIGURATION.MAX_TEXT_TOP_RECORDS";

                public const string MaxMediaRecordsPerPage = "MAIS_CRIANCA.CONFIGURATION.MAX_MEDIA_RECORDS_PER_PAGE";
            } 
        }

        public class Cobalt
        {
            public class Comunicacao
            {
                public const string Remetente = "COBALT.COMUNICACAO.REMETENTE";

                public class ServicoOnlineNotarial
                {
                    public const string Destinatario = "COBALT.COMUNICACAO.SERVICO_ONLINE_NOTARIAL.DESTINATARIO";
                    public const string Assunto = "COBALT.COMUNICACAO.SERVICO_ONLINE_NOTARIAL.ASSUNTO";
                    public const string Template = "COBALT.COMUNICACAO.SERVICO_ONLINE_NOTARIAL.TEMPLATE";
                }

                public class Usuario
                {
                    public const string Assunto = "COBALT.COMUNICACAO.USUARIO.ASSUNTO_EMAIL";
                    public const string Template = "COBALT.COMUNICACAO.USUARIO.TEMPLATE_EMAIL";
                }
            }
        }
    }
}
