﻿using System;
using System.Collections.Generic;
using NHibernate.Persister.Entity;

namespace Lalubema.Orion.Common.Spring.Repository.Listener
{
    internal class EntityUpdateHelper
    {
        internal static void UpdateDate(IEntityPersister persister, IList<object> values, string[] names)
        {
            for (int i = 0; i < values.Count; i++)
            {
                if (values[i] is DateTime)
                    values[i] = ((DateTime) values[i]).ToUniversalTime();
            }

            SetCreated(values, names);
            Set(persister, values, "LastUpdate", DateTime.UtcNow);
        }

        private static void Set(IEntityPersister persister, IList<object> state, string propertyName, object value)
        {
            int index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1)
                return;
            state[index] = value;
        }

        private static void SetCreated(IList<object> state, string[] names)
        {
            int index = Array.FindIndex(names, n => n == "Created");

            if (index >= 0)
            {
                if (state[index] != null)
                {
                    if (DateTime.MinValue.Equals(state[index]) || DateTime.MinValue.ToUniversalTime().Equals(state[index]))
                    {
                        state[index] = DateTime.UtcNow;
                    }
                }
                else
                {
                    state[index] = DateTime.UtcNow;
                }
            }
        }
    }
}