﻿namespace Lalubema.Orion.Common.Exceptions.Vanadium
{
    public class ExpiredLicenseException : BaseException
    {
        public ExpiredLicenseException()
            : base(701003, Utilitarios.Messages.Exceptions.EX_701003)
        {
        }
    }
}