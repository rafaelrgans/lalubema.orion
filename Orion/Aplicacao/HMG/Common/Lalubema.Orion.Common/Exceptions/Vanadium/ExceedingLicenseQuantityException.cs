﻿namespace Lalubema.Orion.Common.Exceptions.Vanadium
{
    public class ExceedingLicenseQuantityException : BaseException
    {
        public ExceedingLicenseQuantityException(int quantityItems, string nextLicense)
            : base(701004, string.Format(Utilitarios.Messages.Exceptions.EX_701004, quantityItems, nextLicense))
        {
        }
    }
}