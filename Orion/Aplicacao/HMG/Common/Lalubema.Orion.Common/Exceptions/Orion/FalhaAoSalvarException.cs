﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class FalhaAoSalvarException : BaseException
    {
        public FalhaAoSalvarException(string nome, string identificador)
            : base(101001, string.Format(Utilitarios.Messages.Exceptions.EX_101001, nome, identificador))
        {
        }

        public FalhaAoSalvarException(string nome, string identificador, Exception innerException)
            : base(101001, string.Format(Utilitarios.Messages.Exceptions.EX_101001, nome, identificador), innerException)
        {
        }

        public FalhaAoSalvarException(string message)
            : base(101001, string.Format(Utilitarios.Messages.Exceptions.EX_101001, string.Empty, message))
        {
        }
    }
}