﻿namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class VersionNotFoundException : BaseException
    {
        public VersionNotFoundException(string atualVersion)
            : base(201004, string.Format(Utilitarios.Messages.Exceptions.EX_201004, atualVersion))
        {
        }
    }
}