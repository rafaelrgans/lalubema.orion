﻿namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class DataInvalidaException : BaseException
    {
        public DataInvalidaException()
            : base(102001, Utilitarios.Messages.Exceptions.EX_102001)
        {
        }
    }
}