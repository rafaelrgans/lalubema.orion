﻿namespace Lalubema.Orion.Common.Exceptions.Orion.Seguranca
{
    public class FalhaAutenticarUsuarioException : BaseException
    {
        public FalhaAutenticarUsuarioException()
            : base(201002, Utilitarios.Messages.Exceptions.EX_201002)
        {
        }
    }
}