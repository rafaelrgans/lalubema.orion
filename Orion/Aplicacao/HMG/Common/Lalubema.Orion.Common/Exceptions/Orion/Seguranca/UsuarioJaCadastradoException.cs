﻿namespace Lalubema.Orion.Common.Exceptions.Orion.Seguranca
{
    public class UsuarioJaCadastradoException : BaseException
    {
        public UsuarioJaCadastradoException(string username)
            : base(201007, string.Format(Utilitarios.Messages.Exceptions.EX_201007, username))
        {
        }
    }
}