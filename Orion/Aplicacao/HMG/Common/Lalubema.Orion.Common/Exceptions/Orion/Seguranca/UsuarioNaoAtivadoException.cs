﻿namespace Lalubema.Orion.Common.Exceptions.Orion.Seguranca
{
    public class UsuarioNaoAtivadoException : BaseException
    {
        public UsuarioNaoAtivadoException(string username)
            : base(201006, Utilitarios.Messages.Exceptions.EX_201006)
        {
            base.Data.Add("Username", username);
        }
    }
}