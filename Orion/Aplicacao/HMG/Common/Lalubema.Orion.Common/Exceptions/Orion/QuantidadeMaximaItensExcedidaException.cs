﻿namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class QuantidadeMaximaItensExcedidaException : BaseException
    {
        public QuantidadeMaximaItensExcedidaException()
            : base(101003, Utilitarios.Messages.Exceptions.EX_101003)
        {
        }
    }
}