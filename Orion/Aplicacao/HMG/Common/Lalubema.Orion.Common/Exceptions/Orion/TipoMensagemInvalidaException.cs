﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class TipoMensagemInvalidaException : BaseException
    {
        public TipoMensagemInvalidaException()
            : base(102006, Utilitarios.Messages.Exceptions.EX_102006)
        {
        }
    }
}