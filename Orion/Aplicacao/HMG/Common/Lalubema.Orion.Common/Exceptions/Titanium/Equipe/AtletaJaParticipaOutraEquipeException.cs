﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Titanium.Equipe
{
    public class AtletaJaParticipaOutraEquipeException : BaseException
    {
        public AtletaJaParticipaOutraEquipeException(string atleta)
            : base(202002, string.Format("O atleta {0} já está associado a outra equipe.", atleta))
        {
        }

        public AtletaJaParticipaOutraEquipeException(string atleta, Exception innerException)
            : base(202002, string.Format("O atleta {0} já está associado a outra equipe.", atleta), innerException)
        {
        }
    }
}