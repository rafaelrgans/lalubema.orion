﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Torneio
{
    public class QuantidadeEquipeInvalidoException : BaseException
    {
        public QuantidadeEquipeInvalidoException()
            : base(203001, "Número de equipes para o torneio é inválido.")
        {
        }
    }
}