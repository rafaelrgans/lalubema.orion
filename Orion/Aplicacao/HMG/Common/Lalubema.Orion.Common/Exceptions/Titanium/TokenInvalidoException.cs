﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Titanium
{
    public class TokenInvalidoException : BaseException
    {
        public TokenInvalidoException()
            : base(301001, Utilitarios.Messages.Exceptions.EX_301001)
        {
        }

        public TokenInvalidoException(Exception innerException) : base(301001, innerException)
        {
        }
    }
}