﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Exceptions;

namespace Lalubema.Orion.Common.Exceptions
{
    public abstract class BaseException : DomainLayerException
    {
        protected BaseException(int messageCode, string message)
            : base(messageCode, message)
        {
        }

        protected BaseException(int messageCode, string message, params object[] parameters)
            : base(message, parameters)
        {
            MessageCode = messageCode;
        }

        protected BaseException(int messageCode, SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            MessageCode = messageCode;
        }

        protected BaseException(int messageCode, string message, Exception innerException)
            : base(message, innerException)
        {
            MessageCode = messageCode;
        }

        protected BaseException(int messageCode, Exception innerException)
            : base(innerException)
        {
            MessageCode = messageCode;
        }
    }
}