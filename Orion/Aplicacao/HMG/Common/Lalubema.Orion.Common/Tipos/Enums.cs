﻿using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Common.Tipos
{
    public enum Sexo
    {
        [StringValue("M", "Masculino")] M = 1,
        [StringValue("F", "Feminino")] F = 2,
        [StringValue("O", "Outro")] O = 0
    }

    public enum MetodoAutenticacao
    {
        NaoSelecionado = 0,
        PorSistema,
        PorUsuario
    }

    public enum Aplicacao
    {
        [StringValue("Pegasus", "Pegasus")] Pegasus = 1,
        [StringValue("Crux", "Crux")] Crux = 2,
        [StringValue("Aquila", "Aquila")] Aquila = 3,
        [StringValue("Columba", "Columba")] Columba = 4,
        [StringValue("Corona", "Corona")] Corona = 5,
        [StringValue("Titanium", "Titanium")] Titanium = 6,
        [StringValue("Vanadium", "Vanadium")] Vanadium = 7,
        [StringValue("Iron", "Iron")] Iron = 8,
        [StringValue("MaisCrianca", "Mais Criança")] MaisCrianca = 9,
        [StringValue("Cobalt", "Cartório Fácil")] Cobalt = 10,
        [StringValue("Corona", "Corona 1.1")] CoronaV1_1 = 13,
        [StringValue("Any", "Any")]
        Any = 999
    }

    public enum StatusUsuarioTorneio
    {
        [StringValue("S", "Não Convidado")] SemConvite,
        [StringValue("C", "Convidado")] Convidado,
        [StringValue("P", "Participante")] Participante
    }

    public enum StatusPartida
    {
        [StringValue("A", "Confirmada")] Aprovada,
        [StringValue("P", "Pendente")] Pendente,
        [StringValue("C", "Cancelada")] Cancelada
    }

    public enum TipoOcorrencia
    {
        [StringValue("U", "Único")] Unico,
        [StringValue("D", "Diário")] Diario,
        [StringValue("S", "Semanal")] Semanal
    }

    public enum ModoCompra
    {
        [StringValue("S", "Store")] Store = 1,
        [StringValue("A", "App Purchase")] AppPurchase = 2
    }

    public enum TipoUsuario
    {
        [StringValue("I", "Undefined")] Undefined = 0,
        [StringValue("A", "Administrator")] Administrator = 1,
        [StringValue("U", "User")] User = 2,
        [StringValue("D", "Demo")] Demo = 3,
        [StringValue("C", "Customer")] Cliente = 4
    }

    public enum TipoConteudo
    {
        [StringValue("C", "Apresentação")] Apresentacao = 1,
        [StringValue("A", "Audiência")] Audiencia,
        [StringValue("G", "Oportunidades")] Grafico
    }

    public enum PerfilPrograma
    {
        [StringValue("A", "Eventos")] Evento = 1,
        [StringValue("E", "Esporte")] Esporte = 2,
        [StringValue("J", "Jornalismo")] Jornalismo = 3,
        [StringValue("V", "Variedades")] Variedades = 4,

		// Alterosa Interior
		[StringValue("P", "Programação")] Programacao = 5,
		[StringValue("D", "Projeto Video")] ProjetoVideo = 6,
		[StringValue("N", "Eventos")] EventoInterior = 7,
		[StringValue("S", "Novidade SBT")] Novidade = 8
	}

    public enum TipoAlbum
    {
        [StringValue("M", "Música")] Musica = 1,
        [StringValue("F", "Foto")] Foto,
        [StringValue("V", "Vídeo")] Video
    }

    public enum TipoVeiculo
    {
        [StringValue("C", "Carro")] Carro = 1,
        [StringValue("M", "Motocicleta")] Motocicleta,
        [StringValue("P", "Pickup")] Pickup,
        [StringValue("O", "Outros")] Outros
    }

    public enum TipoLocalBem
    {
        [StringValue("L", "Local")] Local = 1,
        [StringValue("B", "Bem")] Bem
    }

    public enum TipoAutorizacao
    {
        [StringValue("E", "Entrega")] Entrega = 1,
        [StringValue("N", "Entrada")] Entrada
    }

    public enum TipoMensagem
    {
        [StringValue("I", "Interno")] Interno = 1,
        [StringValue("O", "Oficial")] Oficial = 2,
        [StringValue("Q", "Quadro de Avisos")] QuadroAvisos = 3,
        [StringValue("S", "Sindico")] Sindico = 4
    }

    public enum StatusMensagem
    {
        [StringValue("P", "Publicada")] Publicada = 1,
        [StringValue("A", "Aguardando Autorização")] AguardandoAutorizacao = 2,
        [StringValue("R", "Rejeitada")] Rejeitada = 3
    }

    public enum StatusReserva
    {
        [StringValue("C", "Confirmada")] Confirmada = 1,
        [StringValue("P", "Pendente")] Pendente,
        [StringValue("E", "Lista de Espera")] ListaEspera,
        [StringValue("C", "Cancelada")] Cancelada
    }

    public enum StatusPedido
    {
		[StringValue("U", "Unknow")] Unknow = 0,
        [StringValue("A", "Active")] Active = 1,
        [StringValue("D", "Deleted")] Deleted = 2,
        [StringValue("C", "Closed")] Closed = 3,
        [StringValue("P", "Pending")] Pending = 4
    }

    public enum LicenseType
    {
        [StringValue("F", "Free")] Free = 1,
        [StringValue("P", "MensalPro")] MensalPro,
        [StringValue("I", "MensalFull")] MensalFull,
    }

    public enum TransactionStatus
    {
        [StringValue("A", "Active")] Active = 1,
        [StringValue("N", "NotActive")] NotActive,
        [StringValue("E", "Expired")] Expired
    }

    public enum TextStatus
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("A", "Aprovado")] Approved = 1,
        [StringValue("D", "Reprovado")] Disapproved,
        [StringValue("P", "Pendente")] Pending
    }

    public enum TipoChegada
    {
        [StringValue("C", "Chegada de carro registrado")] VeiculoRegistrado = 0,
        [StringValue("N", "Chegada de carro nao registrado")] VeiculoNaoRegistrado = 1,
        [StringValue("P", "Chegada a pe")] SemVeiculo = 2
    }

    public enum TipoNotificacao
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("C", "Chegada de carro registrado")] ChegadaSegura = 100,
        [StringValue("N", "Chegada de carro nao registrado")] ChegadaSeguraTaxi = 101,
        [StringValue("P", "Chegada Segura a Pe")] ChegadaSeguraPe = 102,
        [StringValue("S", "Serviço")] Servico = 200,
        [StringValue("V", "Visitante")] Visitante = 201,
        [StringValue("D", "Delivery")] Delivery = 202,
        [StringValue("M", "Síndico")] SindicoPorMorador = 300,
        [StringValue("F", "Síndico")] SindicoPorFuncionario = 301,
        [StringValue("R", "Resposta")] Resposta = 900,
        [StringValue("O", "Outros")] Outros = 999
    }

    public enum StatusNotificacao
    {
        [StringValue("E", "Entregue")] Entregue = 1,
        [StringValue("A", "Aguardando")] Aguardando = 2,
        [StringValue("X", "Cancelado")] Cancelado = 5,
        [StringValue("V", "Visualizado")] Visualizado = 6,
        [StringValue("C", "Confirmado")] Confirmado = 3,
        [StringValue("R", "Respondido")] Respondido = 4,
        [StringValue("F", "Finalizado")] Finalizado = 7
    }

	public enum TipoRelatorio
	{
		[StringValue("U", "Unknow")] Unknow = 0,
		[StringValue("G", "Garcom")] Garcom = 1,
		[StringValue("M", "Mesa")] Mesa = 2,
		[StringValue("I", "Item")] Item = 3
	}

    public enum DeviceStatus
    {
        [StringValue("U", "Undefined")] Undefined = 0,
        [StringValue("A", "Ativo")] Active = 1,
        [StringValue("I", "Inativo")] Inactive
    }
}