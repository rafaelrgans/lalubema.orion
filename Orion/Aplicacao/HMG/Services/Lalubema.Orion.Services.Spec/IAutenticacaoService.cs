﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec
{
    [ServiceContract]
    [ObjectMap("AutenticacaoService", false)]
    public interface IAutenticacaoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AutenticarUsuario")]
        [ServiceKnownType(typeof(DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Vanadium.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Iron.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Titanium.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Corona.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Cobalt.DTOAutenticacaoPorUsuario))]
        DTOAutenticacao AutenticarUsuario(DTOTicketPorUsuario ticket);

        [OperationContract]
        [ServiceKnownType(typeof(DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Vanadium.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Iron.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Titanium.DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Corona.DTOAutenticacaoPorUsuario))]
		[ServiceKnownType(typeof(Domain.DataTransferObject.Cobalt.DTOAutenticacaoPorUsuario))]
		DTOAutenticacao Autenticar(DTOTicketPorUsuario ticket);

        [OperationContract]
        [ServiceKnownType(typeof(DTOAutenticacaoPorUsuario))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Vanadium.DTOAutenticacaoPorUsuario))]
        DTOAutenticacao RenovarAutenticacao(string chaveCriptografada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "AutenticarSistema?usuario={usuario}&senha={senha}&corporativo={corporativo}", ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json)]
        DTOAutenticacao AutenticarSistema(string usuario, string senha, int corporativo);
    }
}