﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaUnidadeHabitacionalService", false)]
    public interface IUnidadeHabitacionalService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "CheckEmail?c={c}")]
        Stream CheckEmail(string c);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "ListarUnidadeHabitacionalPorCondominio?chaveCriptografada={chaveCriptografada}&codigoCondominio={codigoCondominio}"
            )]
        DTOUnidadesHabitacionaisRetorno ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada,
                                                                               int codigoCondominio);
    }
}