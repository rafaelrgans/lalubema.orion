﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaLocalBemService", false)]
    public interface ILocalBemService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "/ListarLocalBem?ChaveCriptografada={chaveCriptografada}&codigoEdificio={codigoEdificio}")]
        DTOLocalBemsRetorno ListarLocalBem(string chaveCriptografada, int codigoEdificio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "/ListarReserva?ChaveCriptografada={chaveCriptografada}&codigoLocalBem={codigoLocalBem}")]
        DTOReservasRetorno ListarReserva(string chaveCriptografada, int codigoLocalBem);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "/ListarReservaUsuario?ChaveCriptografada={chaveCriptografada}&codigoCondominio={codigoCondominio}")]
        DTOReservaLocalBemsRetorno ListarReservaUsuario(string chaveCriptografada, int codigoCondominio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/Reservar")]
        DTORetorno Reservar(DTOReservaLocalBemEntrada reservaLocalBem);
    }
}