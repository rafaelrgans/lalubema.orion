﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Iron
{
    [ServiceContract]
    [ObjectMap("IronImportacaoService", false)]
    public interface IImportacaoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Importar?ChaveCriptografada={chaveCriptografada}&codigoEmissora={codigoEmissora}")]
        DTOImportacao Importar(string chaveCriptografada, int codigoEmissora);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/ExportarAcessos",
            ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        DTORetorno ExportarAcessos(DTOExportarAcesso programas);
    }
}