﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject.Crux;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Crux
{
    [ServiceContract]
    [ObjectMap("CruxBancaService", false)]
    public interface IBancaService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "ConsultarBancaPorCodigo?chaveCriptografada={chaveCriptografada}&cultureName={cultureName}&codigoBanca={codigoBanca}")]
        DTOBanca ConsultarBancaPorCodigo(string chaveCriptografada, string cultureName, int codigoBanca);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "ConsultarBancaPorNome?chaveCriptografada={chaveCriptografada}&cultureName={cultureName}&nomeBanca={nomeBanca}")]
        DTOBancas ConsultarBancaPorNome(string chaveCriptografada, string cultureName, string nomeBanca);
    }
}