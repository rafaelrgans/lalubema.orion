﻿using System.Collections.Generic;
using System.ServiceModel;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.MaisCrianca
{
    [ServiceContract]
    [ObjectMap("MaisCriancaTextService", false)]
    public interface ITextService
    {
        [OperationContract]
        IList<DTOTextRetorno> ListTextFeed(DTOPagingFilter pagingFilter);

        [OperationContract]
        IList<DTOTextRetorno> ListTextTopFeed(DTOCategoryEntrada category);

        [OperationContract]
        DTORetorno SendText(DTOTextEntrada text);

        [OperationContract]
        DTORetorno Like(DTOTextEntrada text);

        [OperationContract]
        DTORetorno Comment(DTOCommentEntrada comment);

        [OperationContract]
        DTORetorno ChangeStatus(DTOTextEntrada text);

        [OperationContract]
        DTORetorno Delete(DTOTextEntrada text);
    }
}