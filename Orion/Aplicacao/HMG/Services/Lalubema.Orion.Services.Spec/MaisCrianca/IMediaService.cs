﻿using System.Collections.Generic;
using System.ServiceModel;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.MaisCrianca
{
    [ServiceContract]
    [ObjectMap("MaisCriancaMediaService", false)]
    public interface IMediaService
    {
        [OperationContract]
        IList<DTOMediaRetorno> ListMediaFeed(DTOPagingFilter pagingFilter);

        [OperationContract]
        DTOMediaRetorno Save(DTOMediaEntrada media);

        [OperationContract]
        DTORetorno Delete(DTOMediaEntrada media);
    }
}