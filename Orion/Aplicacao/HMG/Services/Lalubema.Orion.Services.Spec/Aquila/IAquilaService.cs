﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Aquila
{
    [ServiceContract]
    [ObjectMap("AquilaService", false)]
    public interface IAquilaService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "ConsultarPersonalidadePorId?chaveCriptografada={chaveCriptografada}&idPersonalidade={idPersonalidade}")
        ]
        DTOPersonalidade ConsultarPersonalidadePorId(string chaveCriptografada, int idPersonalidade);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "ConsultarPersonalidade?chaveCriptografada={chaveCriptografada}")
        ]
        DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "SalvarAlbum?chaveCriptografada={chaveCriptografada}")
        ]
        DTORetorno SalvarAlbum(string chaveCriptografada, DTOModifiedAlbum album);
    }
}