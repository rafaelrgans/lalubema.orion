﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Vanadium
{
    [ServiceContract]
    [ObjectMap("VanadiumPedidoService", false)]
    public interface IPedidoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/RegistrarPedido")]
        DTOEntidadeRetorno RegistrarPedido(DTOPedidoEntrada pedidoEntrada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/ComandarPedido")]
        DTORetorno ComandarPedido(DTOPedidoEntrada pedidoEntrada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/RemoverItens")]
        DTORetorno RemoverItens(DTOPedidoEntrada pedidoEntrada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/FecharComanda")]
        DTORetorno FecharComanda(DTOPedidoEntrada pedidoEntrada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/ListarMesasAbertas?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}")]
        DTOMesasAbertasRetorno ListarMesasAbertas(string chaveCriptografada, int codigoEstabelecimento);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/BuscarComanda?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}&codigoMesa={codigoMesa}")]
        DTOPedidoRetorno BuscarComanda(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

		// Meu Menu
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/ListarItensPendentes?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}&codigoMesa={codigoMesa}"
            )]
        DTOPedidoRetorno ListarItensPendentes(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate =
                "/ListarHistoricoPedidos?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}&dataInicio={dataInicio}&dataFim={dataFim}"
            )]
        DTOHistoricoPedidos ListarHistoricoPedidos(string chaveCriptografada, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim);

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
			UriTemplate =
				"/ListarPedidosEstabelecimento?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}"
			)]
		DTOPedidoRetorno ListarPedidosEstabelecimento(string chaveCriptografada, int codigoEstabelecimento);

		// Meu Garcom
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
			UriTemplate =
				"/ListarPedidosAbertosMenu?chaveCriptografada={chaveCriptografada}&codigoEstabelecimento={codigoEstabelecimento}&codigoMesa={codigoMesa}"
			)]
		DTOHistoricoPedidos ListarPedidosAbertosMenu(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa);

		//[OperationContract]
		//DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio);

		[OperationContract]
		DTORelatorioGarcomRetorno GerarRelatorioGarcom(DTORelatorioEntrada relatorio);

		[OperationContract]
		DTORelatorioItemRetorno GerarRelatorioItem(DTORelatorioEntrada relatorio);
	
		[OperationContract]
		DTORelatorioMesaRetorno GerarRelatorioMesa(DTORelatorioEntrada relatorio);

		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/TransferirMesaPedido")]
		DTORetorno TransferirMesaPedido(DTOTransfereMesaEntrada transferirMesa);

		[OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/TrocarMesa")]
        DTORetorno TrocarMesa(DTOAlterarMesaEntrada alterarMesa);
    }
}