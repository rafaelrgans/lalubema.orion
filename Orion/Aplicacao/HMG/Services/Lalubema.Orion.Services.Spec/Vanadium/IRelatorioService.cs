﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Vanadium
{
	[ServiceContract]
	[ObjectMap("VanadiumRelatorioService", false)]
	public interface IRelatorioService
	{
		[OperationContract]
		DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio);
	}
}
