﻿using System.ServiceModel;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Vanadium
{
    [ServiceContract]
    [ObjectMap("VanadiumCadastroService", false)]
    public interface ICadastroService
    {
        [OperationContract]
        DTORetorno Salvar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        [OperationContract]
        DTORetorno Excluir(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        [OperationContract]
        DTORetorno Listar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        [OperationContract]
        DTORetorno Buscar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        [OperationContract]
        DTOItens BuscarDestaques(string chaveCriptografada, int codigoEstabelecimento);
    }
}