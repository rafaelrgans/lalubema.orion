﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Orion.Services.Spec.Aquila;

namespace Lalubema.Orion.Services.Impl.Aquila
{
    public class AquilaService : ServiceBase, IAquilaService
    {
        public DTOPersonalidade ConsultarPersonalidadePorId(string chaveCriptografada, int idPersonalidade)
        {
            return AquilaDomainService.ConsultarPersonalidade(chaveCriptografada, idPersonalidade);
        }

        public DTOPersonalidade ConsultarPersonalidade(string chaveCriptografada)
        {
            return AquilaDomainService.ConsultarPersonalidade(chaveCriptografada);
        }

        public DTORetorno SalvarAlbum(string chaveCriptografada, DTOModifiedAlbum album)
        {
            return AquilaDomainService.SalvarAlbum(chaveCriptografada, album);
        }
    }
}