﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Orion.Services.Spec.Vanadium;

namespace Lalubema.Orion.Services.Impl.Vanadium
{
	public class RelatorioService : ServiceBase, IRelatorioService
	{
		public DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio)
		{
			return VanadiumRelatorioDomainService.GerarRelatorio(relatorio);
		}
	}
}
