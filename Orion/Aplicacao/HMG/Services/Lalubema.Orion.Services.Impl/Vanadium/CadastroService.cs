﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Orion.Services.Spec.Vanadium;

namespace Lalubema.Orion.Services.Impl.Vanadium
{
    public class CadastroService : ServiceBase, ICadastroService
    {
        public DTORetorno Salvar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            return VanadiumCadastroDomainService.Salvar(chaveCriptografada, entidade, codigoEstabelecimento);
        }

        public DTORetorno Excluir(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            return VanadiumCadastroDomainService.Excluir(chaveCriptografada, entidade, codigoEstabelecimento);
        }

        public DTORetorno Listar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            return VanadiumCadastroDomainService.Listar(chaveCriptografada, entidade, codigoEstabelecimento);
        }

        public DTORetorno Buscar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            return VanadiumCadastroDomainService.Buscar(chaveCriptografada, entidade, codigoEstabelecimento);
        }

        public DTOItens BuscarDestaques(string chaveCriptografada, int codigoEstabelecimento)
        {
            return VanadiumEstabelecimentoDomainService.BuscarDestaques(chaveCriptografada, codigoEstabelecimento);
        }
    }
}