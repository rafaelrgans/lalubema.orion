﻿using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao;
using Lalubema.Orion.Services.Spec.Vanadium;

namespace Lalubema.Orion.Services.Impl.Vanadium
{
    public class EstabelecimentoService : ServiceBase, IEstabelecimentoService
    {
        public DTOCategorias BuscarCategoriasCardapio(string chaveCriptografada, int codigoEstabelecimento)
        {
            return VanadiumEstabelecimentoDomainService.BuscarCategoriasCardapio(chaveCriptografada,
                                                                                 codigoEstabelecimento);
        }

        public DTOCardapios BuscarCardapios(string chaveCriptografada, int codigoEstabelecimento)
        {
            return VanadiumEstabelecimentoDomainService.BuscarCardapios(chaveCriptografada, codigoEstabelecimento);
        }

        public DTOItemRetorno BuscarItem(string chaveCriptografada, int codigoEstabelecimento, int codigoItem)
        {
            return VanadiumCadastroDomainService.Buscar<DTOItemRetorno>(chaveCriptografada,
                                                                        new DTOItemEntrada {Codigo = codigoItem},
                                                                        codigoEstabelecimento);
        }

        public DTOImportacao Importar(string chaveCriptografada, int codigoEstabelecimento)
        {
            return VanadiumEstabelecimentoDomainService.Importar(chaveCriptografada, codigoEstabelecimento);
        }

        public DTOEstabelecimentos ListarEstabelecimentos(string chaveCriptografada)
        {
            return VanadiumEstabelecimentoDomainService.ListarEstabelecimentos(chaveCriptografada);
        }
    }
}