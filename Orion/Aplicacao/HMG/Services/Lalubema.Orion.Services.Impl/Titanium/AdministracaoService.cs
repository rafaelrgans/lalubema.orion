﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Titanium;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Partida;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;
using Lalubema.Orion.Services.Spec.Titanium;

namespace Lalubema.Orion.Services.Impl.Titanium
{
    public class AdministracaoService : ServiceBase, IAdministracaoService
    {
        public DTORetorno CadastrarTorneio(DTOCadastroTorneio torneio)
        {
            return TitaniumAdministracaoDomainService.CadastrarTorneio(torneio);
        }

        public DTOTorneioRetorno BuscarTorneio(string chaveCriptografada, int codigoTorneio)
        {
            return TitaniumAdministracaoDomainService.BuscarTorneio(chaveCriptografada, codigoTorneio);
        }

        public DTOTorneioPorStatus BuscarTorneios(string chaveCriptografada)
        {
            return TitaniumAdministracaoDomainService.BuscarTorneios(new DTOTorneioEntrada
                                                                         {
                                                                             ChaveCriptografada = chaveCriptografada
                                                                         });
        }

        public IList<DTOPartidaRetorno> BuscarPartidas(string chaveCriptografada, int codigoTorneio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            return CadastroDomainService.ListAll(new DTOPartidaEntrada
                                                     {
                                                         ChaveCriptografada = chaveCriptografada,
                                                         CodigoTorneio = codigoTorneio,
                                                         EmailUsuario = security.EmailUsuario
                                                     }, null, null)
                .Cast<DTOPartidaRetorno>()
                .ToList();
        }
    }
}