﻿using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;
using Lalubema.Orion.Services.Spec.Pegasus;

namespace Lalubema.Orion.Services.Impl.Pegasus
{
    public class ImportacaoService : ServiceBase, IImportacaoService
    {
        public DTORetorno Exportar(DTOExportacao importacaoIndices)
        {
            var response = PegasusImportacaoDomainService.Exportar(importacaoIndices);

            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = response.GetHttpStatusCode();

            return response;
        }

        public DTOImportacao Importar(string chaveCriptografada, string email, string senha)
        {
            return PegasusImportacaoDomainService.Importar(chaveCriptografada, email, senha);
        }
    }
}