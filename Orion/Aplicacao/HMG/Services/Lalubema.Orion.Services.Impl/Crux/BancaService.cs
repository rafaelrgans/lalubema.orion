﻿using Lalubema.Orion.Domain.DataTransferObject.Crux;
using Lalubema.Orion.Services.Spec.Crux;

namespace Lalubema.Orion.Services.Impl.Crux
{
    public class BancaService : ServiceBase, IBancaService
    {
        public DTOBanca ConsultarBancaPorCodigo(string chaveCriptografada, string cultureName, int codigoBanca)
        {
            return BancaDomainService.ConsultarBanca(chaveCriptografada, cultureName, codigoBanca);
        }

        public DTOBancas ConsultarBancaPorNome(string chaveCriptografada, string cultureName, string nomeBanca)
        {
            return BancaDomainService.ConsultarBanca(chaveCriptografada, cultureName, nomeBanca);
        }
    }
}