﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Services.Spec;

namespace Lalubema.Orion.Services.Impl
{
    public class AutenticacaoService : ServiceBase, IAutenticacaoService
    {
        public DTOAutenticacao AutenticarUsuario(DTOTicketPorUsuario ticket)
        {
            return AutenticacaoDomainService.Autenticar(ticket);
        }

        public DTOAutenticacao Autenticar(DTOTicketPorUsuario ticket)
        {
            return AutenticacaoDomainService.Autenticar(ticket);
        }

        public DTOAutenticacao RenovarAutenticacao(string chaveCriptografada)
        {
            return AutenticacaoDomainService.Autenticar(chaveCriptografada);
        }

        public DTOAutenticacao AutenticarSistema(string usuario, string senha, int corporativo)
        {
            int? c = corporativo;
            if (c == 0) c = null;

            return AutenticacaoDomainService.AutenticarPorSistema(usuario, senha, c);
        }
    }
}