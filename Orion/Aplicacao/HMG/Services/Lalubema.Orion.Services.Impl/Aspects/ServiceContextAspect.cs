﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Xml;
using AopAlliance.Intercept;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Exceptions.Security;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Lalubema.Orion.Services.Impl.Aspects
{
    public class ServiceContextAspect : IMethodInterceptor
    {
        /// <summary>
        /// Implement this method to perform extra treatments before and after
        ///             the call to the supplied <paramref name="invocation"/>.
        /// </summary>
        /// <remarks>
        /// <p>Polite implementations would certainly like to invoke
        ///             <see cref="M:AopAlliance.Intercept.IJoinpoint.Proceed"/>. 
        ///             </p>
        /// </remarks>
        /// <param name="invocation">The method invocation that is being intercepted.
        ///             </param>
        /// <returns>
        /// The result of the call to the
        ///             <see cref="M:AopAlliance.Intercept.IJoinpoint.Proceed"/> method of
        ///             the supplied <paramref name="invocation"/>; this return value may
        ///             well have been intercepted by the interceptor.
        /// </returns>
        /// <exception cref="T:System.Exception">If any of the interceptors in the chain or the target object itself
        ///             throws an exception.
        ///             </exception>
        public object Invoke(IMethodInvocation invocation)
        {
            object result = null;

            try
            {
                SetCurrentCulture()
                    .TransformRequestToLocalData(invocation);

                result = invocation.Proceed();

                var dtoResult = result as DTORetorno;
                if (dtoResult != null)
                {
                    dtoResult.CodigoRetorno = 0;
                    dtoResult.DescricaoRetorno = "OK";
                    dtoResult.OcorreuErro = false;
                }
            }
            catch (ValidationException exception)
            {
                if (invocation.Method.ReturnType == typeof (void))
                    throw;

                if (invocation.Method.ReturnType.IsTypeOf(typeof (DTORetorno)) &&
                    !invocation.Method.ReturnType.IsInterface)
                {
                    var retorno =
                        invocation.Method.ReturnType.Assembly.CreateInstance(invocation.Method.ReturnType.FullName);

                    if (retorno != null)
                        UpdateRetorno(retorno, -9001001, exception.Message, true, HttpStatusCode.PreconditionFailed);

                    result = retorno;
                }
                else if (invocation.Method.ReturnType.IsTypeOf(typeof (Stream)))
                {
                    result = new MemoryStream(Encoding.UTF8.GetBytes(exception.Message));
                }

                if (result == null)
                {
                    throw new FaultException(new FaultReason(exception.Message),
                                             new FaultCode("-9001001"));
                }
            }
            catch (ItemNotFoundException exception)
            {
                result = CreateDomainLayerExceptionResponse(invocation, exception, result, HttpStatusCode.NotFound);
            }
            catch (InvalidCryptographyKeyException exception)
            {
                result = CreateDomainLayerExceptionResponse(invocation, exception, result, HttpStatusCode.Forbidden);
            }
            catch (ExpiredCryptographyKeyException exception)
            {
                result = CreateDomainLayerExceptionResponse(invocation, exception, result, HttpStatusCode.Forbidden);
            }
            catch (UsuarioJaCadastradoException exception)
            {
                result = CreateDomainLayerExceptionResponse(invocation, exception, result, HttpStatusCode.PreconditionFailed);
            }
            catch (DomainLayerException exception)
            {
                result = CreateDomainLayerExceptionResponse(invocation, exception, result, HttpStatusCode.BadRequest);
            }
            catch (NotImplementedException exception)
            {
                result = CreateGenericExceptionResponse(invocation, exception, result, HttpStatusCode.NotImplemented);
            }
            catch (Exception exception)
            {
                result = CreateGenericExceptionResponse(invocation, exception, result, HttpStatusCode.BadRequest);
            }

            return result;
        }

        #region [ Request ]

        private ServiceContextAspect SetCurrentCulture()
        {
            try
            {
                var context = WebOperationContext.Current;

                if (context != null)
                {
                    var acceptLanguages = context.IncomingRequest.Headers.Get("Accept-Language");

                    if (acceptLanguages != null)
                    {
                        var languages = acceptLanguages.Split(',');

                        if (languages.Length > 0)
                        {
                            Thread.CurrentThread.CurrentCulture = new CultureInfo(languages[0]);
                            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                        }
                    }
                }
            }
            catch
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }

            return this;
        }

        private void TransformRequestToLocalData(IMethodInvocation invocation)
        {
            SetChaveCriptografadaToEntity(invocation)
                .TransformCadastroServiceRequest(invocation)
                .TransformAccountRegister(invocation);
        }

        private ServiceContextAspect SetChaveCriptografadaToEntity(IMethodInvocation invocation)
        {
            if (invocation.Arguments != null && invocation.Arguments.Length > 0 && invocation.Arguments[0] != null &&
                invocation.Arguments[0].GetType().IsTypeOf(typeof (DTOEntrada)))
            {
                var context = WebOperationContext.Current;

                if (context != null && context.IncomingRequest.UriTemplateMatch != null)
                {
                    var queryParameters = context.IncomingRequest.UriTemplateMatch.QueryParameters;

                    var chaveCriptografada = queryParameters["ChaveCriptografada"];

                    if (!string.IsNullOrWhiteSpace(chaveCriptografada))
                        ((DTOEntrada) invocation.Arguments[0]).ChaveCriptografada = chaveCriptografada;

                    context.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
                    context.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "POST");
                    context.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept");
                }
            }

            return this;
        }

        private ServiceContextAspect TransformCadastroServiceRequest(IMethodInvocation invocation)
        {
            if (invocation.Arguments != null && invocation.Arguments.Length == 2 && invocation.Arguments[0] != null &&
                invocation.Arguments[1] != null &&
                invocation.Method.GetParameters()[0].Name.Equals("entity",
                                                                 StringComparison.InvariantCultureIgnoreCase))
            {
                var entityName = invocation.Arguments[0].ToString();

                var entityFullName = "Lalubema.Orion.Domain.DataTransferObject.{0}.Cadastro." + entityName + ".DTO" + entityName + "Entrada, Lalubema.Orion.Domain.DataTransferObject";

                var entity = TransformRequestToEntity(entityFullName);

                if (entity != null)
                    invocation.Arguments[1] = entity;
            }

            return this;
        }

        private void TransformAccountRegister(IMethodInvocation invocation)
        {
            if (invocation.Method.DeclaringType != null &&
                invocation.Method.DeclaringType.FullName.Equals(
                    "Lalubema.Orion.Services.Impl.Orion.Seguranca.AccountService",
                    StringComparison.InvariantCultureIgnoreCase) &&
                invocation.Method.Name.Equals("Register", StringComparison.InvariantCultureIgnoreCase))
            {
                const string entityFullName = "Lalubema.Orion.Domain.DataTransferObject.{0}.Seguranca.DTOAccountRegister, Lalubema.Orion.Domain.DataTransferObject";

                var entity = TransformRequestToEntity(entityFullName);

                if (entity != null)
                    invocation.Arguments[0] = entity;
            }
        }

        private static object TransformRequestToEntity(string entityFullName)
        {
            var jsonMessage = GetJsonMessage(OperationContext.Current.RequestContext.RequestMessage);

            var aplicacao = GetAplicacao(GetChaveCriptografada(jsonMessage));

            var entityType = Type.GetType(string.Format(entityFullName, aplicacao));

            if (entityType == null) return null;

            var entidade = JsonConvert.DeserializeObject(jsonMessage, entityType);

            return entidade;
        }

        private static string GetAplicacao(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            return ((Aplicacao) security.CodigoAplicacao).ConvertToString();
        }

        private static string GetChaveCriptografada(string jsonMessage)
        {
            var jObject = JObject.Parse(jsonMessage);

            var token = jObject["ChaveCriptografada"];

            return token.Value<string>();
        }

        private static string GetJsonMessage(Message message)
        {
            string jsonMessage;

            try
            {
                var bodyReader = message.GetReaderAtBodyContents();

                var encodedMessage = bodyReader.ReadElementContentAsBase64();

                jsonMessage = Encoding.UTF8.GetString(encodedMessage);
            }
            catch (Exception)
            {
                var xmlMessage = message.ToString();

                var xmlDocument = new XmlDocument();

                if (xmlMessage != null) xmlDocument.LoadXml(xmlMessage);

                jsonMessage = JsonHelper.XmlToJson(xmlDocument, true);
            }

            return jsonMessage;
        }

        #endregion [ Request ]

        #region [ Response ]

        private static object CreateDomainLayerExceptionResponse(IMethodInvocation invocation,
                                                                 DomainLayerException exception,
                                                                 object result, HttpStatusCode httpStatusCode)
        {
            if (invocation.Method.ReturnType == typeof (void))
                throw exception;

            if (invocation.Method.ReturnType.IsTypeOf(typeof (DTORetorno)) && !invocation.Method.ReturnType.IsInterface)
            {
                var retorno =
                    invocation.Method.ReturnType.Assembly.CreateInstance(invocation.Method.ReturnType.FullName);

                if (retorno != null)
                {
                    UpdateRetorno(retorno, exception.MessageCode, exception.Message, true, httpStatusCode);
                }

                result = retorno;
            }
            else if (invocation.Method.ReturnType.IsTypeOf(typeof (Stream)))
            {
                result = new MemoryStream(Encoding.UTF8.GetBytes(exception.Message));
            }

            if (result == null)
            {
                throw new FaultException(new FaultReason(exception.Message),
                                         new FaultCode(exception.MessageCode.ToString(CultureInfo.InvariantCulture)));
            }
            return result;
        }

        private static object CreateGenericExceptionResponse(IMethodInvocation invocation, Exception exception, object result, HttpStatusCode httpStatusCode)
        {
            if (invocation.Method.ReturnType == typeof(void))
                throw exception;

            if (invocation.Method.ReturnType.IsTypeOf(typeof(DTORetorno)) && !invocation.Method.ReturnType.IsInterface)
            {
                var retorno =
                    invocation.Method.ReturnType.Assembly.CreateInstance(invocation.Method.ReturnType.FullName);

                if (retorno != null)
                    UpdateRetorno(retorno, -999, exception.Message, true, httpStatusCode);

                result = retorno;
            }
            else if (invocation.Method.ReturnType.IsTypeOf(typeof(Stream)))
            {
                result = new MemoryStream(Encoding.UTF8.GetBytes(exception.Message));
            }

            if (result == null)
            {
                throw new FaultException(new FaultReason(exception.Message),
                                         new FaultCode("-999"));
            }
            return result;
        }

        private static void UpdateRetorno(object retorno, int codigoRetorno, string descricaoRetorno, bool ocorreuErro,
                                          HttpStatusCode httpStatusCode = HttpStatusCode.OK)
        {
            if (retorno == null) return;

            ((DTORetorno) retorno).CodigoRetorno = codigoRetorno;
            ((DTORetorno) retorno).DescricaoRetorno = descricaoRetorno;
            ((DTORetorno) retorno).SetHttpStatusCode(httpStatusCode);
            ((DTORetorno) retorno).OcorreuErro = ocorreuErro;

            var context = WebOperationContext.Current;

            if (context != null)
                context.OutgoingResponse.StatusCode = httpStatusCode;
        }

        #endregion [ Response ]
    }
}