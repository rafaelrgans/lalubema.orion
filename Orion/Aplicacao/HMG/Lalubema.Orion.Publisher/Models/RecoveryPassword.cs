﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lalubema.Orion.Publisher.Models
{
    public class RecoveryPassword
    {
        public string name { get; set; }
        public string password { get; set; }
        public string password1 { get; set; }
    }
}