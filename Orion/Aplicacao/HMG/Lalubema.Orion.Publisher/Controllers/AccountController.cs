﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Dynamic;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Publisher.Models;
using Lalubema.Utilitarios.Helper.Security;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Common;
using Lalubema.Utilitarios.Helper.Communication;

namespace Lalubema.Orion.Publisher.Controllers
{
    [HandleError]
    public class AccountController : Controller
    {
        public ActionResult ActivateAccount(string token) {
            var security = new ChaveCriptografadaHelper(token);

            var corporativo = security.CodigoCorporativo.HasValue ? security.CodigoCorporativo.Value : 1;
            var c = corporativo != 1 ? corporativo.ToString() + "/" : "";

            //if(security.isExpired(TimeSpan.FromDays(1)))
            //    return View(c + "AccountError", new Exception("O token informado já está expirado!"));

            ActivateAccount model = new ActivateAccount();
            using(DBCoronaEntities db = new DBCoronaEntities()){
                Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                if (usuario == null)
                    return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                model.isActive = usuario.DataAtivacao.HasValue;
                model.name = usuario.Nome;
                model.inviter = usuario.Pessoa2 != null ? usuario.Pessoa2.Nome : null;
                model.residence = new List<Models.Residence>();
                model.residence.AddRange(usuario.PessoaCondominios.Where(w => w.DataAtivacao.HasValue == false && w.Condominio.Licencas.Count(i => i.IDCondominio == w.IDCondominio && i.IDCorporativo == corporativo) > 0).Select(i => new Residence() {
                    idCondominio = i.IDCondominio,
                    name = i.Condominio.Nome,
                    category = i.Categoria
                }).ToList());
                model.residence.AddRange(usuario.PessoaUnidadeHabitacionals.Where(w => w.DataAtivacao.HasValue == false && w.UnidadeHabitacional.Edificio.Condominio.Licencas.Count(i => i.IDCondominio == w.UnidadeHabitacional.Edificio.IDCondominio && i.IDCorporativo == corporativo) > 0).Select(i => new Residence()
                {
                    idCondominio = i.UnidadeHabitacional.Edificio.IDCondominio,
                    name = i.UnidadeHabitacional.Edificio.Condominio.Nome,
                    idUnidade = i.IDUnidadeHab,
                    number = i.UnidadeHabitacional.Numero,
                    category = "M"
                }).ToList());
            }

            if (model.isActive && model.residence.Count <= 0)
                return View(c + "AccountError", new Exception("Seu cadastro já está ativo!"));

            return View(c + "ActivateAccount", model);
        }

        [HttpPost]
        public ActionResult ActivateAccount(string token, ActivateAccount formData)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            try
            {
                if (!formData.isActive)
                {
                    if ((String.IsNullOrWhiteSpace(formData.password)) || !formData.password.Equals(formData.password1))
                        return View(c + "AccountError", new Exception("A senha e a confirmação devem ser iguais"));
                }

                //if (security.isExpired(TimeSpan.FromDays(1)))
                //    return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

                using (DBCoronaEntities db = new DBCoronaEntities())
                {
                    Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                    if (usuario == null)
                        return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                    if (!formData.isActive)
                    {
                        usuario.Nome = formData.name;
                        usuario.Senha = SecurityHelper.EncryptToSHA1(formData.password);
                        usuario.DataAtivacao = DateTime.UtcNow;
                    }

                    if (formData.residence != null)
                    {
                        foreach (Residence residence in formData.residence)
                        {
                            if (residence.activate)
                            {
                                if (!residence.idUnidade.HasValue)
                                    usuario.PessoaCondominios.FirstOrDefault(w => w.IDCondominio == residence.idCondominio).DataAtivacao = DateTime.UtcNow;
                                else
                                    usuario.PessoaUnidadeHabitacionals.FirstOrDefault(w => w.IDUnidadeHab == residence.idUnidade).DataAtivacao = DateTime.UtcNow;
                            }
                            else
                            {
                                var d = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.Value.ToString() : "";

                                var condominio = usuario.PessoaCondominios.FirstOrDefault(w => w.IDCondominio == residence.idCondominio).Condominio;

                                if (condominio.PessoaCondominios.Count(w => w.Categoria == "S") > 0)
                                {
                                    var assunto = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.ASSUNTO_CADASTRO_NAOACEITO + d);
                                    var template = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TEMPLATE_CADASTRO_NAOACEITO + d);

                                    var sindico = condominio.PessoaCondominios.FirstOrDefault(w => w.Categoria == "S").Pessoa;
                                    var email = usuario.Pessoa2 != null ? usuario.Pessoa2.Email : sindico.Email;
                                    var nome = usuario.Pessoa2 != null ? usuario.Pessoa2.Nome : sindico.Nome;
                                    var nomecondominio = residence.name;

                                    if (!string.IsNullOrWhiteSpace(residence.number))
                                    {
                                        nomecondominio += ", na unidade " + residence.number;
                                    }

                                    EmailHelper.EnviarEmail(GetEmailSettings(email), assunto, string.Format(template, nome, usuario.Nome, nomecondominio, residence.comment));
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                }
                dynamic m = new ExpandoObject();
                m.Message = "Seja bem vindo(a). Seu cadastro foi ativado com sucesso!";
                return View(c + "AccountSuccess", m);
            }
            catch (Exception ex)
            {
                return View(c + "AccountError", ex);
            }
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
            {
                To = sendTo,
                From = Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente),
                ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
            };
        }

        public ActionResult RecoveryPassword(string token)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            if (security.isExpired(TimeSpan.FromDays(1)))
                return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

            RecoveryPassword model = new RecoveryPassword();
            using (DBCoronaEntities db = new DBCoronaEntities())
            {
                Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                if (usuario == null)
                    return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                if (!usuario.DataAtivacao.HasValue)
                    return View(c + "AccountError", new Exception("Sua conta ainda nao esta ativada. Por favor ative a conta."));

                model.name = usuario.Nome;
            }

            return View(c + "RecoveryPassword", model);
        }

        [HttpPost]
        public ActionResult RecoveryPassword(string token, RecoveryPassword formData)
        {
            var security = new ChaveCriptografadaHelper(token);
            var c = security.CodigoCorporativo.HasValue && security.CodigoCorporativo.Value != 1 ? security.CodigoCorporativo.ToString() + "/" : "";

            try
            {
                if ((String.IsNullOrWhiteSpace(formData.password)) || !formData.password.Equals(formData.password1))
                    return View(c + "AccountError", new Exception("A senha e confirmacao devem ser iguais"));

                if (security.isExpired(TimeSpan.FromDays(1)))
                    return View(c + "AccountError", new Exception("O token informado ja esta expirado!"));

                using (DBCoronaEntities db = new DBCoronaEntities())
                {
                    Pessoa usuario = db.Pessoas.FirstOrDefault(w => w.ID == security.CodigoUsuario);

                    if (usuario == null)
                        return View(c + "AccountError", new Exception("Token de authenticacao inválido!"));

                    usuario.Senha = SecurityHelper.EncryptToSHA1(formData.password);

                    db.SaveChanges();
                }
                dynamic model = new ExpandoObject();
                model.Message = "Sua senha foi alterada com sucesso!";
                return View(c + "AccountSuccess", model);
            }
            catch (Exception ex)
            {
                return View(c + "AccountError", ex);
            }
        }
    }
}
