﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Orion.Seguranca
{
    /// <summary>
    /// Summary description for AutenticacaoServiceTest
    /// </summary>
    [TestClass]
    public class AutenticacaoServiceTest : TestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            ObjectFactory.Finalize();
        }

        #endregion

        [TestMethod]
        public void AutenticarUsuario()
        {
            // Arrange
            const string sistema = "MEUS_INDICES";
            const string senha = "5wKoTkhE";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var ticketUsuario = new DTOTicketPorUsuario
                                    {
                                        ChaveCriptografada = autenticarSistema.ChaveCriptografada,
                                        Senha = "123456",
                                        Usuario = "teste@teste.com.br",
                                        Versao = "1.0"
                                    };


            // Action
            var retorno = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.AreEqual(-1, retorno.CodigoRetorno);
        }

        [TestMethod]
        public void AutenticarSistemaSucesso()
        {
            // Arrange
            const string sistema = "MEUS_INDICES";
            const string senha = "5wKoTkhE";

            // Action
            var retorno = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.IsFalse(retorno.OcorreuErro);
            Assert.AreEqual(0, retorno.CodigoRetorno);
            Assert.IsNotNull(retorno.ChaveCriptografada);
        }

        [TestMethod]
        public void AutenticarSistemaUsuarioOuSenhaInvalidos()
        {
            // Arrange
            const string sistema = "MEUS_INDICES";
            const string senha = "x9ck3d0eJ";

            // Action
            var retorno = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.IsTrue(retorno.OcorreuErro);
            Assert.AreEqual(201001, retorno.CodigoRetorno);
            Assert.IsNull(retorno.ChaveCriptografada);
        }

        [TestMethod]
        public void AutenticarSistemaVersaoSucesso()
        {
            // Arrange
            //var ticket = new DTOTicketPorSistema
            //                 {
            //                     Sistema = "MEUS_INDICES",
            //                     Senha = "5wKoTkhE",
            //                     Versao = "1.0"
            //                 };

            // Action
            //var retorno = ObterAutenticacaoService().AutenticarSistema(ticket);

            // Assert
            //Assert.IsNotNull(retorno);
            //Assert.IsFalse(retorno.OcorreuErro);
            //Assert.AreEqual(0, retorno.CodigoRetorno);
            //Assert.IsNotNull(retorno.ChaveCriptografada);
        }
    }
}