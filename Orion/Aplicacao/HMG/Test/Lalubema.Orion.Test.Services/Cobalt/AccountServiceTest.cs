﻿using System;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Cobalt
{
    [TestClass]
    public class AccountServiceTest : TestBase
    {
        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void ReenviarSenhaTest()
        {
            // Arrange
            var chaveCriptografada = GetChaveCriptogradaSistema();
            var usuario = new DTOReenviarSenhaEntrada
                              {
                                  ChaveCriptografada = chaveCriptografada,
                                  Email = "marcal@lalubema.com",
                                  Senha = "123456"
                              };

            // Action
            var result = ObterAccountService().ReenviarSenha(usuario);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "COBALT";
            const string senha = "31=66MzJ";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IAccountService ObterAccountService()
        {
            return ObjectFactory.GetObject<IAccountService>(false);
        }
    }
}