﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Pegasus
{
    [TestClass]
    public class ImportacaoServiceTest : TestBase
    {
        [TestMethod]
        public void ExportarDados()
        {
            // Arrange
            var autenticacao = ObterAutenticacaoService().AutenticarSistema("MEUS_INDICES", "5wKoTkhE");

            var dados = new DTOExportacao
                            {
                                ChaveCriptografada = autenticacao.ChaveCriptografada, 
                                Categorias = new List<DTOCategoria>
                                                 {
                                                     new DTOCategoria
                                                         {
                                                             CategoriaID = 1,
                                                             Descricao = "Pré-Mirim"
                                                         },
                                                     new DTOCategoria
                                                         {
                                                             CategoriaID = 2,
                                                             Descricao = "Mirim 1"
                                                         }
                                                 },
                                Clubes = new List<DTOClube>
                                             {
                                                 new DTOClube
                                                     {
                                                         ClubeID = 1,
                                                         Descricao = "Gota D'Água"
                                                     },
                                                 new DTOClube
                                                     {
                                                         ClubeID = 2,
                                                         Descricao = "Minas"
                                                     }
                                             },
                                Usuario = new DTOUsuario
                                              {
                                                  Registro = 103703,
                                                  CategoriaID = 1,
                                                  Nome = "Daniela Katina Mascarenhas",
                                                  DtNascimento = new DateTime(1997, 01, 15),
                                                  Email = "danielakmascarenhas@hotmail.com"
                                              },
                                Estilos = new List<DTOEstilo>
                                              {
                                                  new DTOEstilo
                                                      {
                                                          EstiloID = 1,
                                                          Descricao = "Livre"
                                                      },
                                                  new DTOEstilo
                                                      {
                                                          EstiloID = 2,
                                                          Descricao = "Peito"
                                                      }
                                              }
                            };

            var agenda = new DTOAgenda
                             {
                                 AgendaID = 1,
                                 UsuarioID = 103703,
                                 Competicao = "1o Metropolitano 2011",
                                 Local = "Minas Tenis Clube",
                                 DtAgenda = new DateTime(2012, 05, 25),
                                 DtFinalEvento = new DateTime(2012, 06, 17),
                                 Piscina = 25,
                                 Repetir = false
                             };

            dados.Agendas = new List<DTOAgenda> {agenda};

            var prova = new DTOProva
                            {
                                ProvaID = 1,
                                AgendaID = 1,
                                Serie = 1,
                                Raia = 1,
                                Distancia = 100,
                                Piscina = 25,
                                Tempo = "02:42.93",
                                Balizamento = "02:43.93",
                                NumeroProva = 1,
                                Classificacao = 1
                            };

            dados.Provas = new List<DTOProva> {prova};

            var indice = new DTOIndice
                             {
                                 IndiceID = 1,
                                 ProvaID = 1,
                                 Tempo = "00:02:48.93",
                                 RotuloTempo = "Volta 1"
                             };

            dados.Indices = new List<DTOIndice> {indice};

            // Action
            var retorno = ObterImportacaoService().Exportar(dados);

            // Assert
            try
            {
                Assert.IsNotNull(retorno);
                Assert.AreEqual(0, retorno.CodigoRetorno);
            } catch(AssertFailedException)
            {
                Assert.Fail(retorno.DescricaoRetorno);
            }
        }
    }
}