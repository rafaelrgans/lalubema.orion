﻿using System;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Orion.Seguranca;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Vanadium
{
    [TestClass]
    public class AccountTest : TestBase
    {
        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void RegistrarUsuarioMenuTest()
        {
            // Arrange
            var chaveCriptografada = GetChaveCriptogradaSistema();

            var account = new DTOAccountRegister
                              {
                                  ChaveCriptografada = chaveCriptografada,
                                  Nome = "Bruno Lacerda",
                                  Email = "bmarcall@msn.com",
                                  Celular = "3188498818",
                                  Senha = "lalubema"
                              };

            // Action
            var result = ObterAccountService().Register(account);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "result.OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "VANADIUM";
            const string senha = "0?cg@I2h";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IAccountService ObterAccountService()
        {
            return ObjectFactory.GetObject<IAccountService>(false);
        }
    }
}