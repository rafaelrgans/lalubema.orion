﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Corona;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Corona
{
    [TestClass]
    public class EdificioServiceTest : TestBase
    {
        private const string UsuarioValido = "teste@lalubema.com";

        private const string SenhaValida = "a";

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void DesassociarEdificioLocalBem()
        {
            // Arrange
            const int codigoEdificio = 1;
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            var edificioLocalBens = new DTOAssociarEdificioLocalBemEntrada
                                        {
                                            ChaveCriptografada = chaveCriptografada,
                                            Codigo = codigoEdificio,
                                            LocalBens = new List<DTOLocalBem>
                                                            {
                                                                new DTOLocalBem
                                                                    {
                                                                        Ativo = false,
                                                                        Codigo = 3
                                                                    }
                                                            }
                                        };

            // Action
            DTORetorno retorno = ObterEdificioService().AssociarEdificioLocalBem(edificioLocalBens);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "retorno"));
            Assert.IsNotNull(retorno);

            Console.WriteLine(retorno.DescricaoRetorno);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(retorno.OcorreuErro);
        }

        [TestMethod]
        public void AssociarEdificioLocalBem()
        {
            // Arrange
            const int codigoEdificio = 1;
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            var edificioLocalBens = new DTOAssociarEdificioLocalBemEntrada
            {
                ChaveCriptografada = chaveCriptografada,
                Codigo = codigoEdificio,
                LocalBens = new List<DTOLocalBem>
                                                            {
                                                                new DTOLocalBem
                                                                    {
                                                                        Ativo = true,
                                                                        Codigo = 3
                                                                    }
                                                            }
            };

            // Action
            DTORetorno retorno = ObterEdificioService().AssociarEdificioLocalBem(edificioLocalBens);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "retorno"));
            Assert.IsNotNull(retorno);

            Console.WriteLine(retorno.DescricaoRetorno);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(retorno.OcorreuErro);
        }

        private string GetChaveCriptografadaUsuario()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
            {
                ChaveCriptografada = chaveCriptogradaSistema,
                Senha = SenhaValida,
                Usuario = UsuarioValido,
                Versao = "1.0"
            };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "CORONA";
            const string senha = "5!z[B%J[";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IEdificioService ObterEdificioService()
        {
            return ObjectFactory.GetObject<IEdificioService>(false);
        }
    }
}
