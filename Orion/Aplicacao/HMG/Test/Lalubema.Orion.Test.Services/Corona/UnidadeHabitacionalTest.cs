﻿using System;
using System.Linq;
using Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Corona;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Corona
{
    [TestClass]
    public class UnidadeHabitacionalTest : TestBase
    {
        private const string UsuarioValido = "teste@lalubema.com";

        private const string SenhaValida = "a";

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void ListarUnidadeHabitacionalPorCondominioOrdenadoPorEdificioEUnidadeHabitacionalTest()
        {
            // Arrange
            const int codigoCondominio = 1;
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            // Action
            DTOUnidadesHabitacionaisRetorno unidadesHabitacionais = ObterUnidadeHabitacionalService().ListarUnidadeHabitacionalPorCondominio(chaveCriptografada, codigoCondominio);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "retorno"));
            Assert.IsNotNull(unidadesHabitacionais);

            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "unidadesHabitacionais"));
            Assert.IsNotNull(unidadesHabitacionais.UnidadesHabitacionais);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(unidadesHabitacionais.OcorreuErro);

            Console.WriteLine(string.Format(AssertMessages.IsTrue, "UnidadesHabitacionais.Count > 0"));
            Assert.IsTrue(unidadesHabitacionais.UnidadesHabitacionais.Count > 0);

            Console.WriteLine(string.Format(AssertMessages.IsTrue, "UnidadesHabitacionais.OrderedUnidadeHabitacional"));
            var expectedOrderedUnidadesHabitacionais =
                unidadesHabitacionais.UnidadesHabitacionais.OrderBy(t => t.Edificio.Codigo)
                                     .ThenBy(t => t.Numero.PadLeft(6, '0'));
            Assert.IsTrue(unidadesHabitacionais.UnidadesHabitacionais.SequenceEqual(expectedOrderedUnidadesHabitacionais));;
        }

        private string GetChaveCriptografadaUsuario()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
            {
                ChaveCriptografada = chaveCriptogradaSistema,
                Senha = SenhaValida,
                Usuario = UsuarioValido,
                Versao = "1.0"
            };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "CORONA";
            const string senha = "5!z[B%J[";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IUnidadeHabitacionalService ObterUnidadeHabitacionalService()
        {
            return ObjectFactory.GetObject<IUnidadeHabitacionalService>(false);
        }
    }
}
