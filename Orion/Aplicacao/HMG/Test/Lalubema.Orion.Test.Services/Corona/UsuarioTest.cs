﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Corona
{
    [TestClass]
    public class UsuarioTest : TestBase
    {
        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void CriarNovaConta()
        {
        }

        [TestMethod]
        public void AssociarUsuarioNovoUnidadeHabitacional()
        {
        }

        [TestMethod]
        public void AssociarUsuarioUnidadeHabitacional()
        {
        }

        [TestMethod]
        public void AssociarUsuarioNovoCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioNovoComEmailCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioNovoSemEmailCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioComEmailCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioSemEmailCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioNovoUnidadeHabitacionalCondominio()
        {
        }

        [TestMethod]
        public void AssociarUsuarioUnidadeHabitacionalCondominio()
        {
        }
    }
}
