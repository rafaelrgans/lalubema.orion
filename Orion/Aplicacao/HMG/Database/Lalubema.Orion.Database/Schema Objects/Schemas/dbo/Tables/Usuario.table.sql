﻿CREATE TABLE [dbo].[Usuario] (
    [Codigo]      INT           IDENTITY (1, 1) NOT NULL,
    [Nome]        VARCHAR (120) NULL,
    [Username]    VARCHAR (255) NOT NULL,
    [Senha]       VARCHAR (64)  NOT NULL,
    [CdAplicacao] INT           NOT NULL
);

