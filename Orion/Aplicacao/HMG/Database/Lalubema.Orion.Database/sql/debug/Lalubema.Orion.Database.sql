﻿/*
Deployment script for Lalubema.Orion.Database
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "Lalubema.Orion.Database"
:setvar DefaultDataPath ""
:setvar DefaultLogPath ""

GO
:on error exit
GO
USE [master]
GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL
    AND DATABASEPROPERTYEX(N'$(DatabaseName)','Status') <> N'ONLINE')
BEGIN
    RAISERROR(N'The state of the target database, %s, is not set to ONLINE. To deploy to this database, its state must be set to ONLINE.', 16, 127,N'$(DatabaseName)') WITH NOWAIT
    RETURN
END

GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Creating $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [DBVanadium], FILENAME = '$(DefaultDataPath)$(DatabaseName).mdf', FILEGROWTH = 1024 KB)
    LOG ON (NAME = [DBVanadium_log], FILENAME = '$(DefaultLogPath)$(DatabaseName)_log.ldf', MAXSIZE = 2097152 MB, FILEGROWTH = 10 %) COLLATE SQL_Latin1_General_CP1_CI_AS
GO
EXECUTE sp_dbcmptlevel [$(DatabaseName)], 100;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL,
                RECOVERY FULL,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE,
                SUPPLEMENTAL_LOGGING OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'The database settings cannot be modified. You must be a SysAdmin to apply these settings.';
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET HONOR_BROKER_PRIORITY OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'The database settings cannot be modified. You must be a SysAdmin to apply these settings.';
    END


GO
USE [$(DatabaseName)]
GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO
/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO
PRINT N'Creating [dbo].[Cardapio]...';


GO
CREATE TABLE [dbo].[Cardapio] (
    [Codigo]            INT            IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (80)   NOT NULL,
    [Descricao]         VARCHAR (4000) NOT NULL,
    [CdEstabelecimento] INT            NOT NULL
);


GO
PRINT N'Creating PK_Cardapio...';


GO
ALTER TABLE [dbo].[Cardapio]
    ADD CONSTRAINT [PK_Cardapio] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Categoria]...';


GO
CREATE TABLE [dbo].[Categoria] (
    [Codigo]            INT           IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (100) NOT NULL,
    [Ordem]             INT           NULL,
    [CdEstabelecimento] INT           NOT NULL
);


GO
PRINT N'Creating PK_Categoria...';


GO
ALTER TABLE [dbo].[Categoria]
    ADD CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Estabelecimento]...';


GO
CREATE TABLE [dbo].[Estabelecimento] (
    [Codigo]    INT            IDENTITY (1, 1) NOT NULL,
    [Nome]      VARCHAR (60)   NOT NULL,
    [Descricao] VARCHAR (4000) NOT NULL,
    [Site]      VARCHAR (255)  NULL,
    [UrlLogo]   VARCHAR (255)  NULL,
    [Endereco]  VARCHAR (4000) NOT NULL
);


GO
PRINT N'Creating PK_Estabelecimento...';


GO
ALTER TABLE [dbo].[Estabelecimento]
    ADD CONSTRAINT [PK_Estabelecimento] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Item]...';


GO
CREATE TABLE [dbo].[Item] (
    [Codigo]      INT            IDENTITY (1, 1) NOT NULL,
    [Nome]        VARCHAR (80)   NOT NULL,
    [Descricao]   VARCHAR (4000) NOT NULL,
    [Preco]       MONEY          NOT NULL,
    [UrlFoto]     VARCHAR (255)  NULL,
    [Destaque]    BIT            NULL,
    [CdCardapio]  INT            NOT NULL,
    [CdCategoria] INT            NOT NULL
);


GO
PRINT N'Creating PK_Item...';


GO
ALTER TABLE [dbo].[Item]
    ADD CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Pagina]...';


GO
CREATE TABLE [dbo].[Pagina] (
    [Codigo]   INT          IDENTITY (1, 1) NOT NULL,
    [Nome]     VARCHAR (20) NOT NULL,
    [Conteudo] TEXT         NOT NULL
);


GO
PRINT N'Creating PK_Pagina...';


GO
ALTER TABLE [dbo].[Pagina]
    ADD CONSTRAINT [PK_Pagina] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Usuario]...';


GO
CREATE TABLE [dbo].[Usuario] (
    [Codigo]      INT           IDENTITY (1, 1) NOT NULL,
    [Nome]        VARCHAR (120) NULL,
    [Username]    VARCHAR (255) NOT NULL,
    [Senha]       VARCHAR (64)  NOT NULL,
    [CdAplicacao] INT           NOT NULL
);


GO
PRINT N'Creating PK_Usuario...';


GO
ALTER TABLE [dbo].[Usuario]
    ADD CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([Codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating IX_Usuario...';


GO
ALTER TABLE [dbo].[Usuario]
    ADD CONSTRAINT [IX_Usuario] UNIQUE NONCLUSTERED ([Username] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY];


GO
PRINT N'Creating [dbo].[Usuario_Estabelecimento]...';


GO
CREATE TABLE [dbo].[Usuario_Estabelecimento] (
    [CdEstabelecimento] INT      NOT NULL,
    [CdUsuario]         INT      NOT NULL,
    [TpUsuario]         CHAR (1) NOT NULL
);


GO
PRINT N'Creating PK_Usuario_Estabelecimento...';


GO
ALTER TABLE [dbo].[Usuario_Estabelecimento]
    ADD CONSTRAINT [PK_Usuario_Estabelecimento] PRIMARY KEY CLUSTERED ([CdEstabelecimento] ASC, [CdUsuario] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating DF_Usuario_Estabelecimento_TpUsuario...';


GO
ALTER TABLE [dbo].[Usuario_Estabelecimento]
    ADD CONSTRAINT [DF_Usuario_Estabelecimento_TpUsuario] DEFAULT ('U') FOR [TpUsuario];


GO
PRINT N'Creating FK_Cardapio_Estabelecimento...';


GO
ALTER TABLE [dbo].[Cardapio] WITH NOCHECK
    ADD CONSTRAINT [FK_Cardapio_Estabelecimento] FOREIGN KEY ([CdEstabelecimento]) REFERENCES [dbo].[Estabelecimento] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Categoria_Estabelecimento...';


GO
ALTER TABLE [dbo].[Categoria] WITH NOCHECK
    ADD CONSTRAINT [FK_Categoria_Estabelecimento] FOREIGN KEY ([CdEstabelecimento]) REFERENCES [dbo].[Estabelecimento] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Item_Cardapio...';


GO
ALTER TABLE [dbo].[Item] WITH NOCHECK
    ADD CONSTRAINT [FK_Item_Cardapio] FOREIGN KEY ([CdCardapio]) REFERENCES [dbo].[Cardapio] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Item_Categoria...';


GO
ALTER TABLE [dbo].[Item] WITH NOCHECK
    ADD CONSTRAINT [FK_Item_Categoria] FOREIGN KEY ([CdCategoria]) REFERENCES [dbo].[Categoria] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Usuario_Estabelecimento_Estabelecimento...';


GO
ALTER TABLE [dbo].[Usuario_Estabelecimento] WITH NOCHECK
    ADD CONSTRAINT [FK_Usuario_Estabelecimento_Estabelecimento] FOREIGN KEY ([CdEstabelecimento]) REFERENCES [dbo].[Estabelecimento] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Usuario_Estabelecimento_Usuario...';


GO
ALTER TABLE [dbo].[Usuario_Estabelecimento] WITH NOCHECK
    ADD CONSTRAINT [FK_Usuario_Estabelecimento_Usuario] FOREIGN KEY ([CdUsuario]) REFERENCES [dbo].[Usuario] ([Codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
-- Refactoring step to update target server with deployed transaction logs
CREATE TABLE  [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
GO
sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
GO

GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Cardapio] WITH CHECK CHECK CONSTRAINT [FK_Cardapio_Estabelecimento];

ALTER TABLE [dbo].[Categoria] WITH CHECK CHECK CONSTRAINT [FK_Categoria_Estabelecimento];

ALTER TABLE [dbo].[Item] WITH CHECK CHECK CONSTRAINT [FK_Item_Cardapio];

ALTER TABLE [dbo].[Item] WITH CHECK CHECK CONSTRAINT [FK_Item_Categoria];

ALTER TABLE [dbo].[Usuario_Estabelecimento] WITH CHECK CHECK CONSTRAINT [FK_Usuario_Estabelecimento_Estabelecimento];

ALTER TABLE [dbo].[Usuario_Estabelecimento] WITH CHECK CHECK CONSTRAINT [FK_Usuario_Estabelecimento_Usuario];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        DECLARE @VarDecimalSupported AS BIT;
        SELECT @VarDecimalSupported = 0;
        IF ((ServerProperty(N'EngineEdition') = 3)
            AND (((@@microsoftversion / power(2, 24) = 9)
                  AND (@@microsoftversion & 0xffff >= 3024))
                 OR ((@@microsoftversion / power(2, 24) = 10)
                     AND (@@microsoftversion & 0xffff >= 1600))))
            SELECT @VarDecimalSupported = 1;
        IF (@VarDecimalSupported > 0)
            BEGIN
                EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
            END
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET MULTI_USER 
    WITH ROLLBACK IMMEDIATE;


GO
