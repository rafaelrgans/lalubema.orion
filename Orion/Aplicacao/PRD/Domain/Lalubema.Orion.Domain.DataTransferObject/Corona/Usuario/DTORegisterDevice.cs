﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Usuario")]
    public class DTORegisterDevice : DTOEntrada
    {
        [DataMember]
        public string DeviceToken { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public PlatformType PlatformType { get; set; }

        [DataMember]
        public int Condominio { get; set; }

        [DataMember]
        public int Usuario { get; set; }

        [DataMember]
        public string Bundle { get; set; }
    }
}