﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem")]
    public class DTOReservasRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTOReserva> Reservas { get; set; }
    }
}