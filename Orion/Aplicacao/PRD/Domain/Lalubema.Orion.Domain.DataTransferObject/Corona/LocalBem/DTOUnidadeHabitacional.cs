﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem/")]
    public class DTOUnidadeHabitacional
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Numero { get; set; }

        [DataMember]
        [Map("Edificio.Nome")]
        public string Edificio { get; set; }
    }
}