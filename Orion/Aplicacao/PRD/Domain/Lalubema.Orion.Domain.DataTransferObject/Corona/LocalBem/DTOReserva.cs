﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem")]
    public class DTOReserva
    {
        [DataMember]
        [Map("DataHoraInicio")]
        public DateTime Inicio { get; set; }

        [DataMember]
        [Map("DataHoraFim")]
        public DateTime Fim { get; set; }

        [DataMember]
        [Map("Pessoa.Nome")]
        public string ReservadoPor { get; set; }

        [DataMember]
        public DTOUnidadeHabitacional UnidadeHabitacional { get; set; }
    }
}