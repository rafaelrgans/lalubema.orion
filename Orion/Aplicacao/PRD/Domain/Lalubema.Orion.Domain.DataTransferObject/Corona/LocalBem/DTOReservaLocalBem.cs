﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/LocalBem")]
    public class DTOReservaLocalBem
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Map("DataHoraInicio")]
        public DateTime Inicio { get; set; }

        [DataMember]
        [Map("DataHoraFim")]
        public DateTime Fim { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        [Map("LocalBem.Nome")]
        public string NomeLocalBem { get; set; }

    }
}