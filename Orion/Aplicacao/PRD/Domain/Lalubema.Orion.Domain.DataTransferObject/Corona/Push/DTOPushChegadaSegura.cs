﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Push
{
    [Obsolete("SendPush: Esta classe foi substituída pela DTOSendPush")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Push")]
    public class DTOPushChegadaSegura : DTONotification
    {
        [DataMember]
        public int CodigoVeiculo { get; set; }

        [DataMember]
        public int CodigoCondominio { get; set; }
    }
}