﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Autenticacao")]
    public class DTOPessoaCondominio
    {
        [DataMember]
        [Map("Condominio.Codigo")]
        public int Condominio { get; set; }

        [DataMember]
        public string Categoria { get; set; }
    }
}