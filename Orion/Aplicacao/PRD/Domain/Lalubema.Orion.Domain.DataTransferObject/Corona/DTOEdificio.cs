﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Autenticacao/Edificio")]
    public class DTOEdificio
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}