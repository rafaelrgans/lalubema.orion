﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOEdificio
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }
    }
}