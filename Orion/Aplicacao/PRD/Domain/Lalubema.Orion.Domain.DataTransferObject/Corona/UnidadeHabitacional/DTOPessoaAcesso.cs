﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/UnidadeHabitacional")]
    public class DTOPessoaAcesso
    {
        [DataMember]
        public int Codigo { get; set; }

        [Map("UnidadeHabitacional.Codigo")]
        public int CodigoUnidadeHabitacional { get; set; }

        [DataMember]
        public string Domingo { get; set; }

        [DataMember]
        public string Segunda { get; set; }

        [DataMember]
        public string Terca { get; set; }

        [DataMember]
        public string Quarta { get; set; }

        [DataMember]
        public string Quinta { get; set; }

        [DataMember]
        public string Sexta { get; set; }

        [DataMember]
        public string Sabado { get; set; }
    }
}
