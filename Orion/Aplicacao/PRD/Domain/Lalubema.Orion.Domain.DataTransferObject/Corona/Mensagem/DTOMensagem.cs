﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOMensagem
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Texto { get; set; }
    }
}