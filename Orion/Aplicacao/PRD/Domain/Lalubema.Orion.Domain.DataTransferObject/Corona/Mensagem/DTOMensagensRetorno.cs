﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Mensagem")]
    public class DTOMensagensRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTOMensagem> Mensagens { get; set; }

        [DataMember]
        public bool IsSindico { get; set; }
    }
}
