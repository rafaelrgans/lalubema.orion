﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Edificio")]
    public class DTOLocalBem
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public bool Ativo { get; set; }
    }
}