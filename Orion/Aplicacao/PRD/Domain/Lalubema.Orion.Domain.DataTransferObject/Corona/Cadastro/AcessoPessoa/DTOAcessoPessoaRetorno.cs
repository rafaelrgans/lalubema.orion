﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.AcessoPessoa
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/AcessoPessoa")]
    public class DTOAcessoPessoaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Map("Pessoa.Codigo")]
        [DataMember]
        public int CodigoPessoa { get; set; }

        [Map("UnidadeHabitacional.Codigo")]
        [DataMember]
        public int CodigoUnidadeHabitacional { get; set; }

        [DataMember]
        public string Domingo { get; set; }

        [DataMember]
        public string Segunda { get; set; }

        [DataMember]
        public string Terca { get; set; }

        [DataMember]
        public string Quarta { get; set; }

        [DataMember]
        public string Quinta { get; set; }

        [DataMember]
        public string Sexta { get; set; }

        [DataMember]
        public string Sabado { get; set; }
    }
}