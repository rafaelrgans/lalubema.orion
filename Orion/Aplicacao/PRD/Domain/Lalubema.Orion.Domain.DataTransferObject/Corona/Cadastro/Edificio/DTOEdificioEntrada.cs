﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Edificio
{
    [ObjectMap("CoronaEdificio")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Edificio")]
    public class DTOEdificioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public int Unidades { get; set; }

        [DataMember]
        public int Andares { get; set; }

        [DataMember]
        public int UnidadesPorAndar { get; set; }

        [DataMember]
        public string FormNumUnidades { get; set; }

        [DataMember]
        public string Logradouro { get; set; }

        [DataMember]
        public string Bairro { get; set; }

        [DataMember]
        public string Cidade { get; set; }

        [DataMember]
        public string Estado { get; set; }

        [DataMember]
        public string Cep { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}