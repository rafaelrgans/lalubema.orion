﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.VeiculoCondominio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/VeiculoCondominio")]
    public class DTOCondominio
    {
        [DataMember]
        public int Codigo { get; set; }
    }
}