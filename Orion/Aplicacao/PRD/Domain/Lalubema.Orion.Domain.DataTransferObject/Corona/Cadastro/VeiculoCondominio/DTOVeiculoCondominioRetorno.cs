﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.VeiculoCondominio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/VeiculoCondominio")]
    public class DTOVeiculoCondominioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public TipoVeiculo Tipo { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Cor { get; set; }

        [DataMember]
        public string Placa { get; set; }

        [DataMember]
        [Map("UrlFotoFrente")]
        public string FotoFrente { get; set; }

        [DataMember]
        [Map("UrlFotoTraseira")]
        public string FotoTraseira { get; set; }

        [DataMember]
        public DTOCondominio Condominio { get; set; }
    }
}