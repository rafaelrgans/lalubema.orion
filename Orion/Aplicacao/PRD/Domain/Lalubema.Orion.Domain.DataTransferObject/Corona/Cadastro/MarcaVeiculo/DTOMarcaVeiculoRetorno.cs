﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.MarcaVeiculo
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/MarcaVeiculo")]
    public class DTOMarcaVeiculoRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public TipoVeiculo Tipo { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public int Agrupador { get; set; }
    }
}