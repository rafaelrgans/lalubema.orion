﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/UnidadeHabitacional")]
    public class DTOPessoaUnidadeHabitacional
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Categoria { get; set; }

        [DataMember]
        public string RelacaoFuncao { get; set; }

        [DataMember]
        public int Responsavel { get; set; }

        [DataMember]
        public int Administrador { get; set; }

        [DataMember]
        public int PermissaoPessoas { get; set; }

        [DataMember]
        public int PermissaoVeiculos { get; set; }

        [DataMember]
        public int PermissaoAvisos { get; set; }

        [DataMember]
        public int PermissaoChegadaSegura { get; set; }

        [DataMember]
        public int PermissaoReserva { get; set; }

        [DataMember]
        public int PermissaoClassificados { get; set; }

        [DataMember]
        public int PermissaoNotificacaoPortaria { get; set; }

        [DataMember]
        public DTOPessoa Pessoa { get; set; }

        [DataMember]
        [Map("UnidadeHabitacional.Codigo")]
        public int CodigoUnidadeHabitacional { get; set; }
    }
}