﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.UnidadeHabitacional
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/UnidadeHabitacional")]
    public class DTOPessoa : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Map("Username")]
        public string Email { get; set; }

        [DataMember]
        public DateTime? DataAtivacao { get; set; }

        [DataMember]
        public string DocIdent { get; set; }

        [DataMember]
        public DateTime? DataNascimento { get; set; }

        [DataMember]
        public string Sexo { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        [Map("UrlFoto")]
        public string Foto { get; set; }
    }
}