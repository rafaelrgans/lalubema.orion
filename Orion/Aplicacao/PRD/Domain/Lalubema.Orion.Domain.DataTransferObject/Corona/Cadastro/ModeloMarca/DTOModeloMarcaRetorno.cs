﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ModeloMarca
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/ModeloMarca")]
    public class DTOModeloMarcaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Modelo { get; set; }
    }
}