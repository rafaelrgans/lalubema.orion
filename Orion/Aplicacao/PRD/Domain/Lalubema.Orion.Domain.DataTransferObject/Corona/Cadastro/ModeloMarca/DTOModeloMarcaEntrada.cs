﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ModeloMarca
{
    [ObjectMap("CoronaModeloMarca")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/ModeloMarca")]
    public class DTOModeloMarcaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Modelo { get; set; }
    }
}