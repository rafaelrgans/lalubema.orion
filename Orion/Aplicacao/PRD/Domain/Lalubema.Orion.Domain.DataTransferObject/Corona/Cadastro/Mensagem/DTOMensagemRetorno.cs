﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Mensagem
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Mensagem")]
    public class DTOMensagemRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public TipoMensagem TipoMensagem { get; set; }

        [DataMember]
        public DateTime DataPublicacao { get; set; }

        [DataMember]
        public DateTime DataExpiracao { get; set; }

        [DataMember]
        public string Texto { get; set; }

        [DataMember]
        public StatusMensagem Status { get; set; }
    }
}