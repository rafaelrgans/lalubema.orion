﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Veiculo
{
    [ObjectMap("CoronaVeiculo")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/Veiculo")]
    public class DTOVeiculoEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public TipoVeiculo Tipo { get; set; }

        [DataMember]
        public string Marca { get; set; }

        [DataMember]
        public string Modelo { get; set; }

        [DataMember]
        public string Cor { get; set; }

        [DataMember]
        public string Placa { get; set; }

        [DataMember]
        public string FotoFrente { get; set; }

        [DataMember]
        public string FotoTraseira { get; set; }

        [DataMember]
        public int CodigoUnidadeHabitacional { get; set; }
    }
}