﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.CategoriaConvenio
{
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/CategoriaConvenio")]
    public class DTOCategoriaConvenioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}