﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.CategoriaConvenio
{
    [ObjectMap("CoronaCategoriaConvenio")]
    [DataContract(Namespace = "http://www.lalubema.com/Corona/Cadastro/CategoriaConvenio")]
    public class DTOCategoriaConvenioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}