﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt.Dica
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Cobalt/Dica")]
    public class DTODicasRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTODica> Dicas { get; set; }
    }
}