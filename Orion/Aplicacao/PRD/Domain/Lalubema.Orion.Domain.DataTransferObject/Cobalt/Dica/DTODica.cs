﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt.Dica
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Cobalt/Dica")]
    public class DTODica
    {
        [DataMember]
        public string Titulo { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public bool Ativo { get; set; }
    }
}