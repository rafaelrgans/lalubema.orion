﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Emissora;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt
{
    public class DTOAutenticacaoPorUsuario : DTOAutenticacao
    {
        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public bool IsAdministrador { get; set; }
    }
}
