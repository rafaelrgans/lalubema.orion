﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica
{
	[ObjectMap("CobaltDica")]
	[DataContract(Namespace = "http://www.lalubema.com/Cobalt/Cadastro/Dica")]
    public class DTODicaEntrada : DTOEntrada
    {
		[Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Titulo { get; set; }

        [DataMember]
		public string Descricao { get; set; }

        [DataMember]
        public bool Ativo { get; set; }
    }
}