﻿using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Lalubema.Orion.Domain.DataTransferObject.Cobalt
{
	[DataContract(Namespace="http://www.lalubema.com/OrionServices/Cobalt")]
	public class DTOServicoOnlineNotarialEntrada : DTOEntrada
	{
		[DataMember]
        [Required]
		public string Nome { get; set; }

		[DataMember]
        [Required]
 		public string Telefone { get; set; }

		[DataMember]
        [Required]
        [DataType(DataType.EmailAddress)]
		public string Email { get; set; }

        [DataMember]
        [Required]
        public string TipoEscritura { get; set; }

        [DataMember]
        [Required]
        public string TipoDocumento { get; set; }
	}
}
