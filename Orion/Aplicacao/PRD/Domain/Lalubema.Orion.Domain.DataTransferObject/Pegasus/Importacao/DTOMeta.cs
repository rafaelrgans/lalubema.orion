﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOMeta
    {
        [DataMember]
        public int MetaID { get; set; }

        [DataMember]
        public int UsuarioID { get; set; }

        [DataMember]
        public int EstiloID { get; set; }

        [DataMember]
        public int Distancia { get; set; }

        [DataMember]
        public int Piscina { get; set; }

        [DataMember]
        public DateTime DtInicio { get; set; }

        [DataMember]
        public DateTime DtFim { get; set; }

        [DataMember]
        public string Meta1 { get; set; }

        [DataMember]
        public string Meta2 { get; set; }

        [DataMember]
        public string Meta3 { get; set; }

        [DataMember]
        public string Meta4 { get; set; }
    }
}