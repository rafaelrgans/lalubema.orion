﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOUsuario
    {
        [DataMember]
        public int UsuarioID { get; set; }

        [DataMember]
        [Map("Categoria")]
        public int CategoriaID { get; set; }

        [DataMember]
        public int Registro { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Map("Senha")]
        public string ChaveNuvem { get; set; }

        [DataMember]
        [Map("DataNascimento")]
        public DateTime? DtNascimento { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Clube { get; set; }
    }
}