﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao
{
    [KnownType(typeof(DTOImportacao))]
    [DataContract(Namespace = "http://www.lalubema.com/OrionService/Pegasus/Importacao")]
    public class DTOImportacao : DTORetorno
    {
        [DataMember]
        public DTOUsuario Usuario { get; set; }

        [DataMember]
        public IList<DTOAgenda> Agendas { get; set; }

        [DataMember]
        public IList<DTOCategoria> Categorias { get; set; }

        [DataMember]
        public IList<DTOClube> Clubes { get; set; }

        [DataMember]
        public IList<DTOEquipe> Equipes { get; set; }

        [DataMember]
        public IList<DTOEstilo> Estilos { get; set; }

        [DataMember]
        public IList<DTOIndice> Indices { get; set; }

        [DataMember]
        public IList<DTOProva> Provas { get; set; }

        [DataMember]
        public IList<DTOMeta> Metas { get; set; }
    }
}