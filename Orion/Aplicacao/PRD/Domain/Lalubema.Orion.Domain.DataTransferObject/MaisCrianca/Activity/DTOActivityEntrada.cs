﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Activity
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Activity")]
    public class DTOActivityEntrada : DTOEntrada
    {
        [DataMember]
        public virtual int Codigo { get; set; }

        [DataMember]
        public virtual DateTime Created { get; set; }

        [DataMember]
        public virtual string Ip { get; set; }

        [DataMember]
        public virtual bool Like { get; set; }
    }
}