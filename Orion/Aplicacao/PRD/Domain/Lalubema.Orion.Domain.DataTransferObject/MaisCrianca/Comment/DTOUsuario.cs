﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Comment")]
    public class DTOUsuario
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public int UserType { get; set; }
    }
}