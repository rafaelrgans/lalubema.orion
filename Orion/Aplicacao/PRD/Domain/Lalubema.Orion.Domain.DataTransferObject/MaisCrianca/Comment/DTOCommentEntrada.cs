﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Comment")]
    public class DTOCommentEntrada : DTOEntrada
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public bool IsAdministrator { get; set; }

        [DataMember]
        public DTOUsuario Author { get; set; }

        [DataMember]
        public int TextCode { get; set; }
    }
}