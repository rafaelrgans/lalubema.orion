﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Usuario
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Usuario")]
    public class DTOUsuarioRetorno : DTORetorno
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public int UserType { get; set; }
    }
}