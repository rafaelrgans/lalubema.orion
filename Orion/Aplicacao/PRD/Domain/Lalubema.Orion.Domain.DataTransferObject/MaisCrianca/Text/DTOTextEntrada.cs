﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Text")]
    public class DTOTextEntrada : DTOEntrada
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Category { get; set; }

        [DataMember]
        public DTOUsuario Author { get; set; }

        [DataMember]
        public IList<DTOContent> Contents { get; set; }
    }
}