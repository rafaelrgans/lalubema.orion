﻿using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text
{
    [DataContract(Namespace = "http://www.lalubema.com/MaisCrianca/Text")]
    public class DTOContent
    {
        [DataMember]
        public byte[] Content { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public TipoAlbum ContentType { get; set; }
    }
}