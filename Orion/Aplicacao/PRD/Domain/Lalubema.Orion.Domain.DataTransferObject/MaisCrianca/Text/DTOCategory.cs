﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text
{
    [DataContract(Namespace = "http://www.lalubema.com/Xuxa/MaisCrianca/Text")]
    public class DTOCategory
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}