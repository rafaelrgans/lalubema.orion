﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila.Cadastro.Agenda
{
    [DisplayColumn("Nome")]
    [ObjectMap("AquilaAgenda")]
    [DataContract(Namespace = "http://www.lalubema.com/Aquila/Cadastro/Agenda")]
    public class DTOAgendaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [Required]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Nome do evento",
            GroupName = "Dados Básico", Name = "Nome", Order = 1, Prompt = "Digite o nome do evento", ShortName = "Nome"
            )]
        [DataMember]
        public string Nome { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = false, Description = "Descrição do evento",
            GroupName = "Dados Básico", Name = "Descrição", Order = 2, Prompt = "Informe uma breve descrição do evento",
            ShortName = "Descrição")]
        [DataMember]
        public string Descricao { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(AutoGenerateField = true, AutoGenerateFilter = true, Description = "Data do evento",
            GroupName = "Dados Básico", Name = "Data do Evento", Order = 3, Prompt = "Informe a data do evento",
            ShortName = "Data")]
        [DataMember]
        public DateTime Data { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(AutoGenerateField = true, AutoGenerateFilter = false, Description = "Local do evento",
            GroupName = "Dados Básico", Name = "Local", Order = 4, Prompt = "Informe o local do evento",
            ShortName = "Local")]
        [DataMember]
        public string Local { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        [Filter(FilterTypeFlags.ReadAll)]
        public int CodigoPersonalidade { get; set; }
    }
}