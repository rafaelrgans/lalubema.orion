﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    public class DTOAutenticacaoPorUsuario : DTOAutenticacao
    {
        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Map("Username")]
        public string Email { get; set; }

        [DataMember]
        [Map("Personalidade.Codigo")]
        public int CodigoPersonalidade { get; set; }
    }
}