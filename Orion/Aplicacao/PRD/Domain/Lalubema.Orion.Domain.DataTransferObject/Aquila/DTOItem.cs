﻿namespace Lalubema.Orion.Domain.DataTransferObject.Aquila
{
    public class DTOItem
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public string Url { get; set; }
    }
}