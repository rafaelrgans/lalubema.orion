﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Crux
{
    [KnownType(typeof (DTOLivro))]
    public class DTOLivro
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }

        public string UrlCapa { get; set; }

        public string UrlItunes { get; set; }

        public string UrlGoogle { get; set; }

        public string CultureName { get; set; }

        public ModoCompra ModoCompra { get; set; }

        public string Versao { get; set; }

        public DateTime DataPublicacao { get; set; }
    }
}