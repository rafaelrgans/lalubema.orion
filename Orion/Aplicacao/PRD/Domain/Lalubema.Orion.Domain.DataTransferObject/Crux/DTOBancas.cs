﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Crux
{
    [KnownType(typeof (DTOBancas))]
    public class DTOBancas : DTORetorno
    {
        public IList<DTOBanca> Bancas { get; set; }
    }
}