﻿using System;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions
{
    public class DTODirectDebitRequest : DTOEntrada
    {
        public string OrderID { get; set; }

        public Int64 PurchaseTime { get; set; }

        public int OperationSystem { get; set; }

        public string LicenseType { get; set; }
    }
}