﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    [DataContract(Namespace = "http://www.lalubema.com/Orion/Seguranca")]
    public class DTORecoveryPassword : DTOEntrada
    {
        [DataMember]
        public string Username { get; set; }
    }
}