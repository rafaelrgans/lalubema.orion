﻿using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Aquila;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    [KnownType(typeof(DTOAutenticacaoPorUsuario))]
    public class DTOAutenticacao : DTORetorno
    {
        [DataMember]
        public int CodigoAplicacao { get; set; }

        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string ChaveCriptografada { get; set; }
    }
}