﻿using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca
{
    [KnownType(typeof(DTOTicketPorSistema))]
    public class DTOTicketPorSistema : DTOTicket
    {
        public override MetodoAutenticacao Metodo
        {
            get { return MetodoAutenticacao.PorSistema; }
        }

        public string Sistema { get; set; }

        public string Senha { get; set; }
    }
}