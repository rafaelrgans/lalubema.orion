﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;

namespace Lalubema.Orion.Domain.DataTransferObject
{
    [DataContract(Namespace = "http://www.lalubema.com/")]
    [KnownType(typeof(DTOAutenticacao))]
    [KnownType(typeof(DTORetorno))]
    [KnownType(typeof(DTOEntrada))]
    [KnownType(typeof(DTOBase))]
    [Serializable]
    public abstract class DTOBase
    {
        private HttpStatusCode _httpStatusCode = HttpStatusCode.OK;

        public void SetHttpStatusCode(HttpStatusCode httpStatusCode)
        {
            _httpStatusCode = httpStatusCode;
        }

        public HttpStatusCode GetHttpStatusCode()
        {
            return _httpStatusCode;
        }
    }
}