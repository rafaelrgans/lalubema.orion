﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Importacao")]
    public class DTOUsuario
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Senha { get; set; }

        [DataMember]
        public int Tipo { get; set; }

        [DataMember]
        public bool IsAdministrador { get; set; }
    }
}