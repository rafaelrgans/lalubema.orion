﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Importacao")]
    public class DTOItem
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public decimal Preco { get; set; }

        [DataMember]
        [Map("UrlFoto")]
        public string Foto { get; set; }

        [DataMember]
        public bool Destaque { get; set; }

        [DataMember]
        [Map("Categoria.Codigo")]
        public int CodigoCategoria { get; set; }

        [DataMember]
        [Map("Cardapio.Codigo")]
        public int CodigoCardapio { get; set; }
    }
}