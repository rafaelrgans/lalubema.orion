﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Importacao")]
    public class DTOEstabelecimento
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Descricao { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        [Map("UrlLogo")]
        public string Logo { get; set; }

        [DataMember]
        public string Endereco { get; set; }

        [DataMember]
        public int QuantidadeMesas { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        [DataMember]
        public string TipoComida { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }
    }
}