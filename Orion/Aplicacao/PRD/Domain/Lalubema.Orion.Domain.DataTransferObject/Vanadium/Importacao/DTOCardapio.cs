﻿using System;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Importacao")]
    public class DTOCardapio
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string Descricao { get; set; }
    }
}