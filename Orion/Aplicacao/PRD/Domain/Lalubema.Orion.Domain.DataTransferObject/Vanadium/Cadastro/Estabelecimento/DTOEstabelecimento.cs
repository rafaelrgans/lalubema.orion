﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento
{
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/")]
    [Serializable]
    [DisplayColumn("Nome")]
    public class DTOEstabelecimento
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Nome do estabelecimento",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descrição do estabelecimento", GroupName = "Dados Básicos", Order = 2)]
        public string Descricao { get; set; }

        [DataMember]
        [Display(Name = "Site", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Site do estabelecimento",
            GroupName = "Dados Básicos", Order = 5)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Logo", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Logo do estabelecimento",
            GroupName = "Dados Básicos", Order = 4)]
        [Map("UrlLogo")]
        public string Logo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Endereço", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Endereço do estabelecimento",
            GroupName = "Dados Básicos", Order = 6)]
        public string Endereco { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Quantidade de Mesas", AutoGenerateFilter = false,
            Description = "Quantidade de mesas disponíveis no estabelecimento", GroupName = "Dados Básicos", Order = 3)]
        public int QuantidadeMesas { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Telefone", AutoGenerateFilter = false,
            Description = "Telefone do estabelecimento", GroupName = "Dados Básicos", Order = 7)]
        public string Telefone { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Tipo de Comida", AutoGenerateFilter = false,
            Description = "Tipo de comida do estabelecimento", GroupName = "Dados Básicos", Order = 8)]
        public string TipoComida { get; set; }

        [DataMember]
        [Display(Name = "Latitude", AutoGenerateFilter = false,
            Description = "Latitude do estabelecimento", GroupName = "Dados Básicos", Order = 9)]
        public string Latitude { get; set; }

        [DataMember]
        [Display(Name = "Longitude", AutoGenerateFilter = false,
            Description = "Longitude do estabelecimento", GroupName = "Dados Básicos", Order = 10)]
        public string Longitude { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public DateTime LastUpdate { get; set; }
    }
}