﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/")]
    [Serializable]
    [DisplayColumn("Nome")]
    public class DTOCategoria
    {
        [DataMember]
        [Required(ErrorMessage = "É obrigatório o preenchimento da categoria.")]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento do nome da seção no cardápio.")]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da categoria",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento da ordem de apresentação da seção no cardápio.")]
        [Display(Name = "Ordem", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem da categoria",
            GroupName = "Dados Básicos", Order = 2)]
        public int Ordem { get; set; }
    }
}
