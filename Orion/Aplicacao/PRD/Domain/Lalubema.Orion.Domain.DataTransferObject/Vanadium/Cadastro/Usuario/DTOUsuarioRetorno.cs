﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Usuario
{
    [DisplayColumn("Username")]
    public class DTOUsuarioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento do usuário.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Usuário", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Usuário",
            GroupName = "Dados Básicos", Order = 1)]
        public string Username { get; set; }

        [NoMap]
        [DataMember]
        [Required(AllowEmptyStrings = false, ErrorMessage = "É obrigatório o preenchimento da senha.")]
        [Display(Name = "Senha", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Senha",
            GroupName = "Dados Básicos", Order = 2)]
        public string Senha { get; set; }

        [DataMember]
        [Display(Name = "Administrador do Estabelecimento", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Administrador do Estabelecimento",
            GroupName = "Dados Básicos", Order = 3)]
        public bool IsAdministrador { get; set; }

        [DataMember]
        [Display(Name = "Data de Ativação", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Data de Ativação",
            GroupName = "Dados Básicos", Order = 4)]
        public DateTime? DataAtivacao { get; set; }
    }
}