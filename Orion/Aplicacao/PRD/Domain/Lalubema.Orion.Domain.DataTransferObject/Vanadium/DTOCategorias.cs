﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium
{
    [KnownType(typeof(DTOCategorias))]
    [KnownType(typeof(DTOCategoriaSimples))]
    public class DTOCategorias : DTORetorno
    {
        [DataMember]
        public IList<DTOCategoriaSimples> Categorias { get; set; }
    }
}