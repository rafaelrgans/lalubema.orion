﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Vanadium/Pedido")]
    public class DTOPedidosUsuarioRetorno : DTORetorno
    {
        [DataMember]
        public IList<DTOPedido> Pedidos { get; set; }
    }
}