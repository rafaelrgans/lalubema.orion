﻿using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
	public class DTOItemPedido
	{
		[DataMember]
		[Map("Pedido.Codigo")]
		public int Codigo { get; set; }

		[DataMember]
		[Map("Item.Preco")]
		public decimal Preco { get; set; }

		[DataMember]
		[Map("Quantidade")]
		public int Quantidade { get; set; }

		[DataMember]
		[Map("Item.Status")]
		public StatusPedido StatusPedido { get; set; }

		[DataMember]
		[Map("Item.Codigo")]
		public int CodigoItem { get; set; }

		[DataMember]
		[Map("Item.Nome")]
		public string NomeItem { get; set; }

		[DataMember]
		public decimal TotalVendido { get; set; }
	}
}
