﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
    public class DTOAlterarMesaEntrada : DTOEntrada
    {
        [DataMember]
        public int CodigoEstabelecimento { get; set; }

        [DataMember]
        public int CodigoMesa { get; set; }

        [DataMember]
        public int NovaMesa { get; set; }
    }
}
