﻿using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
	public class DTOTransfereMesaEntrada : DTOEntrada
	{
		[DataMember]
		public int CodigoPedido { get; set; }

		[DataMember]
		public int CodigoEstabelecimento { get; set; }

		[DataMember]
		public int CodigoMesa { get; set; }

		[DataMember]
		public int CodigoUsuario { get; set; }

		[DataMember]
		public int NovaMesa { get; set; }
	}
}
