﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
	public class DTORelatorioMesaRetorno : DTORelatorioRetorno
	{
		[DataMember]
		public int NumeroMesa { get; set; }

		[DataMember]
		public decimal TotalMesa { get; set; }

		[DataMember]
		public IList<DTOItemPedido> ItemPedido { get; set; }
	}
}
