﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [DataContract(Namespace = "http://www.lalubema.com/OrionServices/Vanadium/Pedido")]
    public class DTOPedido
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
		[Map("StatusPedido")]
        public StatusPedido Status { get; set; }

        [DataMember]
        public int Mesa { get; set; }

        [DataMember]
		[Map("Estabelecimento.Nome")]
        public string NomeEstabelecimento { get; set; }

		[DataMember]
		[Map("Cliente.Codigo")]
		public int CodigoCliente { get; set; }

        [DataMember]
		[Map("Cliente.Nome")]
        public string NomeCliente { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public IList<DTOItem> Itens { get; set; }
	}
}