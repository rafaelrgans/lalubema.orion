﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido
{
    [Serializable]
    [DataContract(Namespace = "http://www.lalubema.com/Vanadium/Pedido")]
    public class DTOPedidoRetorno : DTORetorno
    {
        [DataMember]
        public int CodigoMesa { get; set; }

        [DataMember]
        [Map("Estabelecimento.Codigo")]
        public int Estabelecimento { get; set; }

        [DataMember]
        public IList<DTOItem> Itens { get; set; }
    }
}