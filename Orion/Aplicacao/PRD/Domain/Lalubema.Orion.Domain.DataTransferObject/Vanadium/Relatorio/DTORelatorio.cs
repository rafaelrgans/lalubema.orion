﻿using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio
{
	[DataContract(Namespace = "http://www.lalubema.com/Vanadium/Relatorio")]
	public class DTORelatorio
	{
		[DataMember]
		public int Codigo { get; set; }

		//[DataMember]
		//public int CodigoUsuario { get; set; }

		//[DataMember]
		//public int CodigoItem { get; set; }

		[DataMember]
		public string NomeEstabelecimento { get; set; }

		[DataMember]
		public string NomeCliente { get; set; }

		//[DataMember]
		//public StatusPedido StatusPedido { get; set; }

		//[DataMember]
		//public string Total { get; set; }
	}
}
