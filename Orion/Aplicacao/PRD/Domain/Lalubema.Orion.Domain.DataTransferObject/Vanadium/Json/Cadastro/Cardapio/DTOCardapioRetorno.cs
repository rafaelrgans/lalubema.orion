﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio
{
    [DisplayColumn("Nome")]
    public class DTOCardapioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do cardápio",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descrição do cardápio", GroupName = "Dados Básicos", Order = 1)]
        public string Descricao { get; set; }

        [DataMember]
        public IList<DTOItem> Itens { get; set; }
    }
}