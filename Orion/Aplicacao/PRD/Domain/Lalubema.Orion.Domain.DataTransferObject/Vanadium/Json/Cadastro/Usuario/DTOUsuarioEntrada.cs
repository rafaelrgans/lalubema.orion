﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Usuario
{
    [ObjectMap("VanadiumUsuario")]
    [ReturnType(typeof (DTOUsuarioRetorno))]
    [ComplexReturnType("Usuarios", typeof (DTOUsuarios))]
    public class DTOUsuarioEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Senha { get; set; }

        [DataMember]
        [Required]
        public bool IsAdministrador { get; set; }
    }
}