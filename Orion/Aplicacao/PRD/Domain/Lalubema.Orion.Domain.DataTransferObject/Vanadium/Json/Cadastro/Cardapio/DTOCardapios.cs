﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio
{
    [KnownType(typeof(DTOCardapio))]
    [KnownType(typeof(DTOItem))]
    public class DTOCardapios : DTORetorno
    {
        [DataMember]
        public IList<DTOCardapio> Cardapios { get; set; }
    }
}