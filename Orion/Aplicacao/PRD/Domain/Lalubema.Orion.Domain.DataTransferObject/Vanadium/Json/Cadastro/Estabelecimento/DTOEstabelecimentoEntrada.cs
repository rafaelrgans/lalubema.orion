﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento
{
    [ObjectMap("VanadiumEstabelecimento")]
    [ReturnType(typeof (DTOEstabelecimentoRetorno))]
    [ComplexReturnType("Estabelecimentos", typeof(DTOEstabelecimentos))]
    public class DTOEstabelecimentoEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Descricao { get; set; }

        [DataMember]
        public string Site { get; set; }

        [DataMember]
        public byte[] Logo { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Endereco { get; set; }

        [DataMember]
        [Required]
        public string UserName { get; set; }

        [DataMember]
        [Required]
        public bool IsAdmin { get; set; }
    }
}