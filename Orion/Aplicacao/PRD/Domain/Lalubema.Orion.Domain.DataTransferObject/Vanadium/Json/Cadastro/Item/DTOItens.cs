﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item
{
    [KnownType(typeof (DTOItem))]
    public class DTOItens : DTORetorno
    {
        [DataMember]
        public IList<DTOItem> Itens { get; set; }
    }
}