﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item
{
    [ObjectMap("VanadiumItem")]
    [ReturnType(typeof (DTOItemRetorno))]
    [ComplexReturnType("Itens", typeof (DTOItens))]
    public class DTOItemEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        public string Descricao { get; set; }

        [DataMember]
        [Required]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DataType(DataType.Currency)]
        public decimal Preco { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public bool Destaque { get; set; }

        [DataMember]
        public DTOCategoria Categoria { get; set; }

        [DataMember]
        public DTOCardapio Cardapio { get; set; }
    }
}