﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Usuario
{
    [DisplayColumn("Username")]
    public class DTOUsuarioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome de Usuário", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Nome de Usuário",
            GroupName = "Dados Básicos", Order = 1)]
        public string Username { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Senha do Usuário", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Senha de Usuário",
            GroupName = "Dados Básicos", Order = 2)]
        public string Senha { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Administrador do Estabelecimento", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Administrador do Estabelecimento",
            GroupName = "Dados Básicos", Order = 3)]
        public bool IsAdministrador { get; set; }
    }
}