﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento
{
    [DisplayColumn("Nome")]
    public class DTOEstabelecimentoRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Nome do estabelecimento",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descrição do estabelecimento", GroupName = "Dados Básicos", Order = 1)]
        public string Descricao { get; set; }

        [DataMember]
        [Display(Name = "Site", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Site do estabelecimento",
            GroupName = "Dados Básicos", Order = 1)]
        public string Site { get; set; }

        [DataMember]
        [Display(Name = "Logo", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Logo do estabelecimento",
            GroupName = "Dados Básicos", Order = 1)]
        [Map("UrlLogo")]
        public string Logo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Endereço", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Endereço do estabelecimento",
            GroupName = "Dados Básicos", Order = 1)]
        public string Endereco { get; set; }
    }
}