﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    [DataContract(IsReference = true)]
    [Serializable]
    [DisplayColumn("Nome")]
    public class DTOCategoria
    {
        [DataMember]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da categoria",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Ordem", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem da categoria",
            GroupName = "Dados Básicos", Order = 2)]
        public int Ordem { get; set; }
    }
}
