﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;

namespace Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria
{
    [DisplayColumn("Nome")]
    public class DTOCategoriaRetorno : DTORetorno
    {
        [DataMember]
        [Key]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da categoria",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Ordem", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem da categoria",
            GroupName = "Dados Básicos", Order = 2)]
        public int Ordem { get; set; }

        [ScaffoldColumn(false)]
        public IList<DTOItem> Itens { get; set; }
    }
}
