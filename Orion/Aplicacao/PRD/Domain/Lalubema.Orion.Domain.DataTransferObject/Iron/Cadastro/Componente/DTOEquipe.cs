﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Componente
{
    [Serializable]
    [TypeDisplay("Equipe")]
    [DisplayColumn("Nome")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Componente")]
    public class DTOEquipe
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome da equipe",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto da equipe",
            GroupName = "Dados Básicos", Order = 3)]
        [Map("UrlFoto")]
        public string Foto { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto em alta resolução", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Foto da equipe em alta resolução", GroupName = "Dados Básicos", Order = 4)]
        [Map("UrlFotoAltaResolucao")]
        public string FotoAltaResolucao { get; set; }
    }
}