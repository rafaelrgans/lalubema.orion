﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Categoria
{
    [TypeDisplay("Categoria")]
    [DisplayColumn("Descricao")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Categoria")]
    public class DTOCategoriaRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Descrição", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Descricao da categoria", GroupName = "Dados Básicos", Order = 1)]
        public string Descricao { get; set; }
    }
}