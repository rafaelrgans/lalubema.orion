﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Programa
{
    [Serializable]
    [DisplayColumn("Content")]
    [TypeDisplay("Conteúdo")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Programa")]
    public class DTOConteudo
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Ordem", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Ordem do conteúdo",
            GroupName = "Dados Básicos", Order = 1)]
        public int Ordem { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Tipo", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Tipo do conteúdo",
            GroupName = "Dados Básicos", Order = 2)]
        public int Tipo { get; set; }

        [DataMember]
        [Display(Name = "Conteúdo", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Conteúdo",
            GroupName = "Dados Básicos", Order = 3)]
        [Map("UrlConteudo")]
        public string Content { get; set; }
    }
}