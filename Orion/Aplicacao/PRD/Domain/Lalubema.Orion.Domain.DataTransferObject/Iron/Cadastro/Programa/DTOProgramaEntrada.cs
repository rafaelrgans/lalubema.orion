﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Programa
{
    [ObjectMap("IronPrograma")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Programa")]
    public class DTOProgramaEntrada : DTOEntrada
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do programa",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [Display(Name = "Acessos", AutoGenerateFilter = true, AutoGenerateField = true,
            Description = "Quantidade de acessos do programa", GroupName = "Dados Básicos", Order = 3)]
        public int QuantidadeAcesso { get; set; }

        [DataMember]
        public byte[] Foto { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public IList<DTOConteudo> Conteudos { get; set; }

        [DataMember]
        public IList<DTOVideo> Videos { get; set; }

        [DataMember]
        [Filter(FilterTypeFlags.ReadAll | FilterTypeFlags.Read | FilterTypeFlags.Save)]
        public int CodigoEmissora { get; set; }
    }
}