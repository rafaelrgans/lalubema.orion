﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Attributes.Mvc;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Cadastro.Equipe
{
    [Serializable]
    [DisplayColumn("Nome")]
    [TypeDisplay("Programa")]
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Cadastro/Equipe")]
    public class DTOPrograma
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nome", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Nome do programa",
            GroupName = "Dados Básicos", Order = 1)]
        public string Nome { get; set; }

        [DataMember]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Foto", AutoGenerateFilter = true, AutoGenerateField = true, Description = "Foto do programa",
            GroupName = "Dados Básicos", Order = 2)]
        [Map("UrlFoto")]
        public string Foto { get; set; }
    }
}