using System;
using System.Runtime.Serialization;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOPrograma
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string UrlFoto { get; set; }

        [DataMember]
        public string UrlFotoAltaResolucao { get; set; }

        [DataMember]
        public int QuantidadeAcesso { get; set; }

        [DataMember]
        public virtual PerfilPrograma Perfil { get; set; }
    }
}