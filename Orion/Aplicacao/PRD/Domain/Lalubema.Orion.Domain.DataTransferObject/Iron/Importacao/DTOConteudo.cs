using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOConteudo
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string UrlConteudo { get; set; }

        [DataMember]
        public int Ordem { get; set; }

        [DataMember]
        public int Tipo { get; set; }

        [DataMember]
        [Map("Programa.Codigo")]
        public int ProgramaID { get; set; }
    }
}