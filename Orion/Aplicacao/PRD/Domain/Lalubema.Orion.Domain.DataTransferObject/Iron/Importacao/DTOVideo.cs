using System;
using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao
{
    [DataContract(Namespace = "http://www.lalubema.com/Iron/Importacao")]
    [Serializable]
    public class DTOVideo
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string UrlVideo { get; set; }

        [DataMember]
        public string Titulo { get; set; }

        [DataMember]
        [Map("Categoria.Codigo")]
        public int CategoriaID { get; set; }

        [DataMember]
        [Map("Programa.Codigo")]
        public int ProgramaID { get; set; }
    }
}