﻿using System;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTODisponibilidade
    {
        public DateTime DataInicial { get; set; }

        public DateTime DataFinal { get; set; }

        [Map("TipoOcorrencia")]
        public String Ocorrencia { get; set; }
    }
}