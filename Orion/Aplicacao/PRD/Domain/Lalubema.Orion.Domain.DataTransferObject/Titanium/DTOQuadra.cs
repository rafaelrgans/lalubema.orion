﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOQuadra
    {
        [Key]
        [ScaffoldColumn(false)]
        public int Codigo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [StringLength(18, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 8)]
        [Display(Name = "Telefone")]
        public string Telefone { get; set; }

        [Map("Endereco.Logradouro")]
        [Required(ErrorMessage = "A rua é obrigatória")]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Rua")]
        public string Rua { get; set; }

        [Map("Endereco.Numero")]
        [Required(ErrorMessage = "O número é obrigatório")]
        [Display(Name = "Número")]
        public int? Numero { get; set; }

        [Map("Endereco.Bairro")]
        [Required(ErrorMessage = "O bairro é obrigatório")]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Bairro")]
        public string Bairro { get; set; }

        public IList<DTODisponibilidade> Disponibilidades { get; set; }
    }
}