﻿using System;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOPartida : DTORetorno
    {
        public int Codigo { get; set; }

        public DateTime Data { get; set; }
    }
}