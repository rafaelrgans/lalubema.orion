﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOAtleta
    {
        [Key]
        [ScaffoldColumn(false)]
        public int Codigo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [StringLength(150, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Categoria")]
        public DTOCategoria Categoria { get; set; }

        public IList<DTOCategoria> Categorias { get; set; }
    }
}