﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOEquipe
    {
        [Key]
        [ScaffoldColumn(false)]
        public int Codigo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 2)]
        [Display(Order = 1, Name = "Nome", GroupName = "Dados Básicos")]
        public string Nome { get; set; }

        [Required]
        [Display(Order = 2, Name = "Ordem", GroupName = "Dados Básicos")]
        public int Ordem { get; set; }

        [Required]
        [Display(Order = 3, Name = "Torneio", GroupName = "Dados Básicos", AutoGenerateFilter = true)]
        public DTOTorneioRetorno Torneio { get; set; }

        [Required]
        [Display(Order = 4, Name = "Atletas", GroupName = "Atletas", AutoGenerateFilter = true)]
        public IList<DTOAtleta> Atletas { get; set; }

        public int[] AtletasSelecionados { get; set; }

        [ScaffoldColumn(false)]
        public IList<DTOAtleta> AtletasDisponiveis { get; set; }

        [ScaffoldColumn(false)]
        public IList<DTOTorneioRetorno> TorneiosDisponiveis { get; set; }
    }
}