﻿using System;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium
{
    public class DTOCompromisso
    {
        public int Codigo { get; set; }

        public DateTime DataFim { get; set; }

        public DateTime DataInicio { get; set; }

        public string Descricao { get; set; }
    }
}