﻿using System.Runtime.Serialization;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Partida
{
    [DataContract(Namespace = "http://www.lalubema.com/Titanium/Partida")]
    public class DTOEquipe
    {
        [DataMember]
        [Map("Equipe.Codigo")]
        public int Codigo { get; set; }

        [DataMember]
        [Map("Equipe.Nome")]
        public string Nome { get; set; }

        [DataMember]
        public int Resultado { get; set; }

        [DataMember]
        public int Set { get; set; }
    }
}