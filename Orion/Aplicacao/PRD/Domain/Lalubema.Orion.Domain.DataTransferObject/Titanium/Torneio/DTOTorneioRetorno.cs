﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio
{
    [DataContract(Namespace = "http://www.lalubema.com/Titanium/Torneio")]
    public class DTOTorneioRetorno : DTORetorno
    {
        [Key]
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public DateTime Inicio { get; set; }

        [DataMember]
        public DateTime Fim { get; set; }

        [DataMember]
        public int QuantidadeJogadorPorEquipe { get; set; }

        [DataMember]
        public DateTime TempoMaximoPartida { get; set; }

        [DataMember]
        public IList<DTOUsuario> Usuarios { get; set; }

        [DataMember]
        public string EmailAdministrador { get; set; }

        [DataMember]
        public bool Iniciado { get; set; }
    }
}