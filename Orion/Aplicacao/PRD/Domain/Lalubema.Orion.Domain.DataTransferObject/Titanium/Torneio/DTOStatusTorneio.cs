﻿using System;

namespace Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio
{
    public class DTOStatusTorneio
    {
        public int Codigo { get; set; }

        public string Nome { get; set; }

        public DateTime Inicio { get; set; }

        public DateTime Fim { get; set; }

        public DateTime TempoMaximoPartida { get; set; }

        public bool IsAdministrador { get; set; }
    }
}