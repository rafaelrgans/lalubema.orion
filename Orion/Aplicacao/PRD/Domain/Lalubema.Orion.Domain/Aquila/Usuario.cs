﻿using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Username { get; set; }

        public virtual string Senha { get; set; }

        public virtual Personalidade Personalidade { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Buscar(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            throw new NotImplementedException();
        }

        public virtual void RecoveryPassword(string username)
        {
            throw new NotImplementedException();
        }

        public virtual bool Validar(string usuario, string senha, int codigoAplicacao, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Consultar(usuario, codigoAplicacao);

            if (!senha.Equals(u.Senha, StringComparison.InvariantCulture))
                return false;

            if (!Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(Aplicacao.Versao);

            Load(u.Codigo);

            return true;
        }
    }
}