﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Aquila
{
    public class Personalidade : DomainBase<Personalidade, IPersonalidadeRepositorio<Personalidade>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Resumo { get; set; }

        public virtual IList<Album> Albuns { get; set; }

        public virtual IList<Agenda> Agendas { get; set; }

        public virtual IList<Noticia> Noticias { get; set; }

        public virtual IList<Usuario> Usuarios { get; set; }
    }
}