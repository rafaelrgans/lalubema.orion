﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public Usuario()
        {
            Initialize();
        }

        private void Initialize()
        {
            UserType = TipoUsuario.User;
        }

        public virtual int Codigo { get; set; }

        public virtual string Username { get; set; }

        public virtual string Password { get; set; }

        public virtual string Name { get; set; }

        public virtual DateTime Birthday { get; set; }

        public virtual string City { get; set; }

        public virtual TipoUsuario UserType { get; set; }

        public virtual IList<Media> Medias { get; set; }

        public virtual IList<Text> Texts { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public override void Save()
        {
            Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.MaisCrianca.ConvertToString());

            base.Save();
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Buscar(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            throw new NotImplementedException();
        }

        public virtual void RecoveryPassword(string username)
        {
            throw new NotImplementedException();
        }
    }
}