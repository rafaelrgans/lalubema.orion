﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.MaisCrianca
{
    public class Category : DomainBase<Category, ICategoryRepositorio<Category>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Description { get; set; }

        public virtual bool General { get; set; }

        public virtual int TeiaVida { get; set; }

        public virtual IList<Text> Texts { get; set; }
    }
}