﻿using System;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Transaction : DomainBase<Transaction, ITransactionRepositorio<Transaction>>
    {
        public virtual int Codigo { get; set; }

        public virtual string OrderID { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual PlatformType OperationSystem { get; set; }

        public virtual LicenseType LicenseType { get; set; }

        public virtual TransactionStatus Status { get
        {
            if (LicenseType == LicenseType.Free && (!EndDate.HasValue || DateTime.UtcNow.Date.CompareTo(EndDate.Value.Date) <= 0))
                return TransactionStatus.Active;

            if (!EndDate.HasValue && DateTime.UtcNow.Date.CompareTo(StartDate) <= 0)
                return TransactionStatus.Active;

            if (DateTime.UtcNow.Date.Between(StartDate, EndDate))
                return TransactionStatus.Active;

            return DateTime.UtcNow.Date.CompareTo(EndDate) > 0 ? TransactionStatus.Expired : TransactionStatus.NotActive;
        } }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime? EndDate { get; set; }

        public virtual Usuario Responsavel { get; set; }
    }
}