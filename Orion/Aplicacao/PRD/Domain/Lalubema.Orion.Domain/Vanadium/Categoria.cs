﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Categoria : DomainBase<Categoria, ICategoriaRepositorio<Categoria>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual int Ordem { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public virtual IList<Item> Itens { get; set; }

        public override void Save()
        {
            Estabelecimento.LastUpdate = DateTime.UtcNow;

            var categorias = Repository.ListAll(Estabelecimento.Codigo);

            if (categorias.FirstOrDefault(c => c.Ordem == Ordem && c.Codigo != Codigo) != null)
                throw new FalhaAoSalvarException(Messages.OrdemCategoriaInvalida);

            base.Save();
        }
    }
}