﻿using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class UsuarioEstabelecimento :
        DomainBase<UsuarioEstabelecimento, IUsuarioEstabelecimentoRepositorio<UsuarioEstabelecimento>>
    {
        public virtual Usuario Usuario { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public virtual TipoUsuario TipoUsuario { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (UsuarioEstabelecimento)) return false;
            return Equals((UsuarioEstabelecimento) obj);
        }

        public virtual bool Equals(UsuarioEstabelecimento usuarioEstabelecimento)
        {
            if (ReferenceEquals(null, usuarioEstabelecimento)) return false;
            if (ReferenceEquals(this, usuarioEstabelecimento)) return true;
            return Equals(usuarioEstabelecimento.Usuario, Usuario) && Equals(usuarioEstabelecimento.Estabelecimento, Estabelecimento);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = Usuario.GetHashCode();
                result = (result*397) ^ Estabelecimento.GetHashCode();

                return result;
            }
        }
    }
}