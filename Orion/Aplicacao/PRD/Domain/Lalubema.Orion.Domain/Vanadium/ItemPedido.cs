﻿using System;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class ItemPedido : DomainBase<ItemPedido, IItemPedidoRepositorio<ItemPedido>>
    {
        public ItemPedido()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Created = DateTime.UtcNow;
            Status = StatusPedido.Active;
        }

        public virtual Item Item { get; set; }

        public virtual Pedido Pedido { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual int Quantidade { get; set; }

        public virtual string Observacao { get; set; }

        public virtual StatusPedido Status { get; set; }

        public virtual Usuario CriadoPor { get; set; }

        public virtual Usuario Responsavel { get; set; }

		public virtual decimal TotalVendido { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj.GetType() == typeof (ItemPedido) && Equals((ItemPedido) obj);
        }

        public virtual bool Equals(ItemPedido other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Item, Item) && Equals(other.Pedido, Pedido) && Equals(other.Created, Created);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var result = Item.GetHashCode();
                result = (result*397) ^ Pedido.GetHashCode();
                result = (result * 397) ^ Created.GetHashCode();

                return result;
            }
        }
    }
}