﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Vanadium;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Spring;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Estabelecimento : DomainBase<Estabelecimento, IEstabelecimentoRepositorio<Estabelecimento>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual string Site { get; set; }

        public virtual string UrlLogo { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Logo { get; set; }

        public virtual string Endereco { get; set; }

        public virtual int QuantidadeMesas { get; set; }

        public virtual string Telefone { get; set; }

        public virtual string TipoComida { get; set; }

        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }

        public virtual DateTime LastUpdate { get; set; }

        public virtual IList<Cardapio> Cardapios { get; set; }

        public virtual IList<UsuarioEstabelecimento> Usuarios { get; set; }

        public virtual string UserName { get; set; }

        public virtual bool IsAdmin { get; set; }

        public Estabelecimento()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Cardapios = new List<Cardapio>();
            Usuarios = new List<UsuarioEstabelecimento>();
        }

        public override void Save()
        {
            var oldUrl = UrlLogo;

            try
            {
                var usuarioRepositorio = ObjectFactory.GetSingleton<IUsuarioRepositorio<Usuario>>();

                var usuario = usuarioRepositorio.BuscarUsuarioPorEmail(UserName);

                AdicionarEstabelecimento(usuario);

                SalvarLogo(oldUrl);
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch(Exception exception)
            {
                throw new FalhaAoSalvarException("Estabelecimento", Nome, exception);
            }

            base.Save();
        }

        private void AdicionarEstabelecimento(Usuario usuario)
        {
            UsuarioEstabelecimento usuarioEstabelecimento;

            if (Codigo == 0)
            {
                usuarioEstabelecimento = new UsuarioEstabelecimento
                                             {
                                                 Estabelecimento = this,
                                                 Usuario = usuario,
                                                 TipoUsuario =
                                                     IsAdmin ? TipoUsuario.Administrator : TipoUsuario.User
                                             };
                Usuarios = new List<UsuarioEstabelecimento> { usuarioEstabelecimento };

                usuario.Estabelecimentos.Add(usuarioEstabelecimento);

                AdicionarCardapioPadrao();
            }
            else
            {
                usuarioEstabelecimento = Usuarios.FirstOrDefault(u => u.Usuario.Username == UserName);

                if (usuarioEstabelecimento == null)
                {
                    Usuarios.Add(new UsuarioEstabelecimento
                                     {
                                         Estabelecimento = this,
                                         Usuario = usuario,
                                         TipoUsuario = IsAdmin ? TipoUsuario.Administrator : TipoUsuario.User
                                     });
                }
                else
                {
                    usuarioEstabelecimento.TipoUsuario = IsAdmin ? TipoUsuario.Administrator : TipoUsuario.User;
                }
            }

            ValidateQuantityItemLicense(usuario);
        }

        private void AdicionarCardapioPadrao()
        {
            Cardapios.Add(new Cardapio
                              {
                                  Descricao = "Cardápio Padrão",
                                  Estabelecimento = this,
                                  Nome = "Cardápio Padrão"
                              });
        }

        private static void ValidateQuantityItemLicense(Usuario usuario)
        {
            switch (usuario.LicencaAtiva)
            {
                case LicenseType.Free:
                    if (usuario.Estabelecimentos.Count > 1)
                        throw new ExceedingLicenseQuantityException(1, LicenseType.MensalPro.Description());
                    break;
                case LicenseType.MensalPro:
                    if (usuario.Estabelecimentos.Count > 3)
                        throw new ExceedingLicenseQuantityException(3, LicenseType.MensalFull.Description());
                    break;
            }
        }

        private void SalvarLogo(string oldUrl)
        {
            if (Logo != null)
            {
                using (
                    var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg, Logo,
                                                    ComputerUnit.Mb.ToLong()))
                {
                    fileHelper.ChangeResolution()
                              .ResizeImage()
                              .Validate();

                    Logo = fileHelper.File;
                    var extension = string.Format("*.{0}", fileHelper.Extension);

                    var retorno = FileSystem.SaveFile(Aplicacao.Vanadium, extension, Logo);

                    UrlLogo = retorno.Path;
                }

                if (!string.IsNullOrWhiteSpace(oldUrl))
                    FileSystem.DeleteFile(Aplicacao.Vanadium, oldUrl);
            }
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlLogo))
                FileSystem.DeleteFile(Aplicacao.Vanadium, UrlLogo);

            ExcluirItens();

            ExcluirCardapios();

            ExcluirCategorias();

            base.Delete();
        }

        private void ExcluirItens()
        {
            var itemRepositorio = ObjectFactory.GetSingleton<IItemRepositorio<Item>>();

            var itens = itemRepositorio.ListAll(Codigo);

            foreach (var t in itens)
                t.Delete();
        }

        private void ExcluirCardapios()
        {
            var cardapioRepositorio = ObjectFactory.GetSingleton<ICardapioRepositorio<Cardapio>>();

            var cardapios = cardapioRepositorio.ListAll(Codigo);

            foreach (var t in cardapios)
                t.Delete();
        }

        private void ExcluirCategorias()
        {
            var categoriaRepositorio = ObjectFactory.GetSingleton<ICategoriaRepositorio<Categoria>>();

            var categorias = categoriaRepositorio.ListAll(Codigo);

            foreach (var t in categorias)
                t.Delete();
        }
    }
}