﻿using System;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Vanadium;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;

namespace Lalubema.Orion.Domain.Vanadium
{
    public class Item : DomainBase<Item, IItemRepositorio<Item>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Foto { get; set; }

        public virtual decimal Preco { get; set; }

        public virtual bool Destaque { get; set; }

        public virtual Categoria Categoria { get; set; }

		public virtual decimal TotalVendido { get; set; }

        [NoMap]
        public virtual Cardapio Cardapio { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

        public override void Save()
        {
            Estabelecimento.LastUpdate = DateTime.UtcNow;

            Cardapio = new Cardapio().Repository.Get(0, Estabelecimento.Codigo);

            ValidateQuantityItemLicense();

            var itensDestaque = (from item in Cardapio.Itens where item.Destaque select item).ToList();

            var quantidadeItensDestaque = Parametro.BuscarPorChave(Constantes.Vanadium.Configuracao.LimiteItensEmDestaque).ToInt32();

            if (itensDestaque.Count > quantidadeItensDestaque && Destaque)
                throw new QuantidadeMaximaItensDestaqueExcedidaException();

            var oldUrl = UrlFoto;

            try
            {
                if (Foto != null)
                {
                    using (
                        var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg, Foto,
                                                        ComputerUnit.Mb.ToLong()))
                    {
                        fileHelper.ResizeImage()
                                  .ChangeResolution()
                                  .Validate();

                        Foto = fileHelper.File;
                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Vanadium, extension, Foto);

                        UrlFoto = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Vanadium, oldUrl);
                }
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Item", Nome, exception);
            }

            base.Save();
        }

        private void ValidateQuantityItemLicense()
        {
            var usuarios =
                (from usuario in Estabelecimento.Usuarios
                 where usuario.TipoUsuario == TipoUsuario.Administrator
                 select usuario).ToList();

            var quantidadeItens =
                usuarios.Select(u => Repository.ListAll(u.Estabelecimento.Codigo)).Select(items => items.Count).Sum();

            switch (TransactionManager.GetLicense(Estabelecimento))
            {
                case LicenseType.Free:
                    if (quantidadeItens >= 30)
                        throw new ExceedingLicenseQuantityException(30, LicenseType.MensalPro.Description());
                    break;
                case LicenseType.MensalPro:
                    if (quantidadeItens >= 150)
                        throw new ExceedingLicenseQuantityException(150, LicenseType.MensalFull.Description());
                    break;
            }
        }

        public override void Delete()
        {
            Cardapio.Estabelecimento.LastUpdate = DateTime.UtcNow;

            if(!string.IsNullOrWhiteSpace(UrlFoto))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Vanadium, UrlFoto);

            base.Delete();
        }
    }
}