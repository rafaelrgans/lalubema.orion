using System.Collections.Generic;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Iron
{
    public class Programa : DomainBase<Programa, IProgramaRepositorio<Programa>>, IDomainModel
    {
        public Programa()
        {
            OnCreated();
        }

        private void OnCreated()
        {
            Conteudos = new List<Conteudo>();
            Equipes = new List<Equipe>();
            Videos = new List<Video>();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string UrlFotoAltaResolucao { get; set; }

        public virtual int QuantidadeAcesso { get; set; }

        public virtual PerfilPrograma Perfil { get; set; }

        public virtual Emissora Emissora { get; set; }

        public virtual IList<Conteudo> Conteudos { get; set; }

        public virtual IList<Equipe> Equipes { get; set; }

        public virtual IList<Video> Videos { get; set; }

        public override void Save()
        {
            var equipeRepositorio = ObjectFactory.GetSingleton<IEquipeRepositorio<Equipe>>();

            var equipe = equipeRepositorio.GetByProduto(Codigo);

            if (equipe != null)
            {
                equipe.Nome = Nome;

                equipe.Save();
            }

            base.Save();
        }

        public virtual void Save(int codigoEmissora)
        {
            if (Emissora == null || Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            Save();
        }
    }
}