using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;

namespace Lalubema.Orion.Domain.Iron
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Username { get; set; }

        public virtual string Senha { get; set; }

        public virtual TipoUsuario TipoUsuario { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual Emissora Emissora { get; set; }

        public virtual bool IsAdministrador
        {
            get { return TipoUsuario == TipoUsuario.Administrator; }
            set { TipoUsuario = value ? TipoUsuario.Administrator : TipoUsuario.User; }
        }

        public virtual void Save(int codigoEmissora)
        {
            if (Emissora == null || Emissora.Codigo != codigoEmissora)
                Emissora = Emissora.GetRepository().Get(codigoEmissora);

            if (Emissora == null || Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            Save();
        }

        public override void Save()
        {
            if (Codigo == 0)
            {
                var usuario = Repository.Buscar(Username);

                if (usuario != null)
                    throw new UsuarioJaCadastradoException(Username);
            }

            if (Aplicacao == null || Aplicacao.Codigo != Common.Tipos.Aplicacao.Iron.ToInt32())
                Aplicacao = Aplicacao.GetRepository().Get(Common.Tipos.Aplicacao.Iron.ToInt32());

            base.Save();
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            var u = Repository.Buscar(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            throw new NotImplementedException();
        }

        public virtual void RecoveryPassword(string username)
        {
            throw new NotImplementedException();
        }
    }
}