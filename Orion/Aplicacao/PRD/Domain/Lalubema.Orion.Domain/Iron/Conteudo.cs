using System;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Orion.Domain.Iron
{
    public class Conteudo : DomainBase<Conteudo, IConteudoRepositorio<Conteudo>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string UrlConteudo { get; set; }

        public virtual string FileName { get; set; }

        public virtual byte[] Content { get; set; }

        public virtual int Ordem { get; set; }

        public virtual TipoConteudo Tipo { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual void Save(int codigoEmissora)
        {
            if (Programa == null || Programa.Codigo == 0)
                throw new DadosInvalidosException("Programa selecionado inv�lido");

            Programa.Load(Programa.Codigo);

            if (Programa.Emissora == null || Programa.Emissora.Codigo != codigoEmissora)
                throw new DadosInvalidosException(
                    "N�o foi poss�vel identificar a emissora, por gentileza tente novamente mais tarde e caso o erro persista contacte o Administrador.");

            Save();
        }

        public override void Save()
        {
            if (Programa.Codigo > 0)
                Programa = Programa.Get(Programa.Codigo);

            var oldUrl = UrlConteudo;

            VerificarQuantidadeLimite();

            try
            {
                if (Content != null)
                {
                    var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Iron, FileName, Content);

                    UrlConteudo = retorno.Path;

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, oldUrl);
                }
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Conte�do", FileName, exception);
            }

            base.Save();
        }

        public override void Delete()
        {
            if (!string.IsNullOrWhiteSpace(UrlConteudo))
                FileSystem.DeleteFile(Common.Tipos.Aplicacao.Iron, UrlConteudo);

            base.Delete();
        }

        private void VerificarQuantidadeLimite()
        {
            int quantidadeLimite;

            switch (Tipo)
            {
                case TipoConteudo.Apresentacao:
                    quantidadeLimite = Parametro.BuscarPorChave(Constantes.Iron.Configuracao.LimiteItensConteudo).ToInt32();
                    break;
                case TipoConteudo.Audiencia:
                    quantidadeLimite = Parametro.BuscarPorChave(Constantes.Iron.Configuracao.LimiteItensAudiencia).ToInt32();
                    break;
                case TipoConteudo.Grafico:
                    quantidadeLimite = Parametro.BuscarPorChave(Constantes.Iron.Configuracao.LimiteItensGrafico).ToInt32();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var conteudos = Repository.ListByTipo(Programa.Codigo, Tipo);

            if (conteudos.Count >= quantidadeLimite)
                throw new QuantidadeMaximaItensExcedidaException();

            if (conteudos.FirstOrDefault(c => c.Ordem == Ordem && c.Codigo != Codigo) != null)
                throw new FalhaAoSalvarException(Messages.OrdemConteudoInvalido);
        }
    }
}