﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Categoria : DomainBase<Categoria, ICategoriaRepositorio<Categoria>>
    {
        public virtual int Codigo { get; set; }

        public virtual int CategoriaID { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int Usuario { get; set; }
    }
}