﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Equipe : DomainBase<Equipe, IEquipeRepositorio<Equipe>>
    {
        public virtual int Codigo { get; set; }

        public virtual int EquipeID { get; set; }

        public virtual string Descricao { get; set; }

        [Map("ClubeID")]
        public virtual int Clube { get; set; }

        public virtual int Usuario { get; set; }
    }
}