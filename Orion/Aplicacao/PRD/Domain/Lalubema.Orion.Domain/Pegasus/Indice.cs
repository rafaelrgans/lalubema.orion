﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Indice : DomainBase<Indice, IIndiceRepositorio<Indice>>
    {
        public virtual int Codigo { get; set; }

        public virtual int IndiceID { get; set; }

        public virtual string Tempo { get; set; }

        public virtual string RotuloTempo { get; set; }

        [Map("ProvaID")]
        public virtual int Prova { get; set; }

        public virtual int Usuario { get; set; }
    }
}