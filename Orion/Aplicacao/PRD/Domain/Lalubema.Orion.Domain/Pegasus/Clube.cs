﻿using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Clube : DomainBase<Clube, IClubeRepositorio<Clube>>
    {
        public virtual int Codigo { get; set; }

        public virtual int ClubeID { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int Usuario { get; set; }
    }
}