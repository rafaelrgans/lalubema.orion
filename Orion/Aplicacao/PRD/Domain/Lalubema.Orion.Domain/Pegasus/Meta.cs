﻿using System;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Pegasus
{
    public class Meta : DomainBase<Meta, IMetaRepositorio<Meta>>
    {
        public virtual int Codigo { get; set; }

        public virtual int MetaID { get; set; }

        public virtual int UsuarioID { get; set; }

        public virtual int EstiloID { get; set; }

        public virtual int Distancia { get; set; }

        public virtual int Piscina { get; set; }

        public virtual DateTime DtInicio { get; set; }

        public virtual DateTime DtFim { get; set; }

        public virtual string Meta1 { get; set; }

        public virtual string Meta2 { get; set; }

        public virtual string Meta3 { get; set; }

        public virtual string Meta4 { get; set; }

        public virtual int Usuario { get; set; }
    }
}