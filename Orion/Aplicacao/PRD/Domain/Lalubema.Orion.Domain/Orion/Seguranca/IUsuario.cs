﻿using System;

namespace Lalubema.Orion.Domain.Orion.Seguranca
{
    public interface IUsuario : IDomainModel
    {
        int Codigo { get; set; }

        string Username { get; set; }

        Aplicacao Aplicacao { get; set; }

        void Validar(string usuario, string senha, string versao);
        
        string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao);

        void RecoveryPassword(string username);
    }
}