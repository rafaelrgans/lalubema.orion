﻿using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Orion
{
    public class Pais : DomainBase<Pais, IPaisRepositorio<Pais>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Sigla { get; set; }

        public virtual IList<Estado> Estados { get; set; }
    }
}