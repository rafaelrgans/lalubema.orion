﻿using System;
using System.IO;
using Common.Logging;
using Lalubema.Orion.Common;
using Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Helper.Log;

namespace Lalubema.Orion.Domain.Orion.InfraEstrutura
{
    public class FileSystem
    {
        public static DTOFileRetorno SaveFile(DTOFileEntrada file)
        {
            return SaveFile(file.Aplicacao, file.FileName, file.File);
        }

        public static DTOFileRetorno SaveFile(Common.Tipos.Aplicacao aplicacao, string fileName, byte[] file)
        {
            fileName = GetFileName() + Path.GetExtension(fileName);
            var filePath = Parametro.BuscarPorChave(Constantes.Orion.InfraEstrutura.FileSystem.BasePath);
            var fileUrl = Parametro.BuscarPorChave(Constantes.Orion.InfraEstrutura.FileSystem.BaseUrl);

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            filePath = Path.Combine(filePath, GetApplicationFolder(aplicacao));

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);

            filePath = Path.Combine(filePath, fileName);
            fileUrl = Path.Combine(fileUrl, GetApplicationFolder(aplicacao), fileName).Replace('\\', '/');

            File.WriteAllBytes(filePath, file);

            return new DTOFileRetorno(fileUrl);
        }

        public static void DeleteFile(Common.Tipos.Aplicacao aplicacao, string fileUrl)
        {
            try
            {
                if (fileUrl == null)
                    throw new ArgumentNullException("fileUrl");

                var filePath = Parametro.BuscarPorChave(Constantes.Orion.InfraEstrutura.FileSystem.BasePath);

                filePath = Path.Combine(filePath, GetApplicationFolder(aplicacao));

                var fileName = Path.GetFileName(fileUrl);

                if(fileName == null)
                    return;

                filePath = Path.Combine(filePath, fileName);

                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            catch (Exception e)
            {
                LogHelper.Instance.Logar(e.Message, LogLevel.Error);
            }
        }

        private static string GetApplicationFolder(Common.Tipos.Aplicacao aplicacao)
        {
            switch (aplicacao)
            {
                case Common.Tipos.Aplicacao.Pegasus:
                    return "pg";
                case Common.Tipos.Aplicacao.Crux:
                    return "li";
                case Common.Tipos.Aplicacao.Aquila:
                    return "aq";
                case Common.Tipos.Aplicacao.Columba:
                    return "cl";
                case Common.Tipos.Aplicacao.Corona:
                    return "cr";
                case Common.Tipos.Aplicacao.Titanium:
                    return "tt";
                case Common.Tipos.Aplicacao.Vanadium:
                    return "vn";
                case Common.Tipos.Aplicacao.Iron:
                    return "ir";
                case Common.Tipos.Aplicacao.MaisCrianca:
                    return "xx\\mc";
                case Common.Tipos.Aplicacao.Any:
                    return "gn";
                default:
                    throw new ArgumentOutOfRangeException("aplicacao");
            }
        }

        private static string GetFileName()
        {
            return GuidHelper.GetGuidBase64String();
        }
    }
}