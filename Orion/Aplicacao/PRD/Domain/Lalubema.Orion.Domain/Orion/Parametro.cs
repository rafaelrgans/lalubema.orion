﻿using System.Collections.Generic;
using System.Configuration;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Orion
{
    public class Parametro : DomainBase<Parametro, IParametroRepositorio<Parametro>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string ConteudoPRD { get; set; }

        public virtual string ConteudoHMG { get; set; }

        public virtual string ConteudoTST { get; set; }

        public virtual string ConteudoDSV { get; set; }

        public virtual Parametro Pai { get; set; }

        public virtual IList<Parametro> Filhos { get; set; }

        private static string _ambiente;

        private static string Ambiente
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_ambiente))
                {
                    _ambiente = ConfigurationManager.AppSettings["ambiente"];
                }
                
                return _ambiente;
            }
        }

        public static string BuscarPorChave(string chave)
        {
            var parametro = GetRepository().BuscarPorChave(chave);

            if (parametro == null)
                return null;

            switch (Ambiente.ToLower())
            {
                case "prd":
                    return parametro.ConteudoPRD;
                case "hmg":
                    return parametro.ConteudoHMG;
                case "tst":
                    return parametro.ConteudoTST;
                case "dsv":
                    return parametro.ConteudoDSV;
            }

            return parametro.ConteudoPRD;
        }
    }
}