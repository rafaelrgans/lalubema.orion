﻿namespace Lalubema.Orion.Domain.Orion
{
    public interface IDomainModel
    {
        void Save();

        void Delete();

        void Load(int codigo);
    }
}