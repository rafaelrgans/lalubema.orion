﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Orion
{
    public class Aplicacao : DomainBase<Aplicacao, IAplicacaoRepositorio<Aplicacao>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; protected set; }

        public virtual string Senha { get; protected set; }

        public virtual string Versao { get; set; }

        public virtual string ChaveCriptografada { get; set; }

        public virtual IList<NotificationSettings> NotificationSettings { get; set; }

        public virtual string Autenticar(string senha)
        {
            if (!Senha.Equals(senha, StringComparison.InvariantCulture))
                throw new UsuarioOuSenhaInvalidosException();

            ChaveCriptografada = SecurityHelper.CreateCryptographyKey(CryptographyType.System.ConvertToString(),
                                                                      DateTime.UtcNow.ToString("o"), Nome,
                                                                      Codigo.ToString(CultureInfo.InvariantCulture),
                                                                      Versao);

            return ChaveCriptografada;
        }

        public static Aplicacao Buscar(string sistema)
        {
            return GetRepository().Buscar(sistema);
        }
    }
}