﻿using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Orion;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Orion
{
    public class Estado : DomainBase<Estado, IEstadoRepositorio<Estado>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Uf { get; set; }

        public virtual Pais Pais { get; set; }

        public virtual IList<Cidade> Cidades { get; set; }
    }
}