﻿using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Crux;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Crux
{
    public class Banca : DomainBase<Banca, IBancaRepositorio<Banca>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual IList<Livro> Livros { get; set; }
    }
}