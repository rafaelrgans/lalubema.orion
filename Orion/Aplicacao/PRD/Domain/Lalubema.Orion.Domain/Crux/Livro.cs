﻿using System;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Integration.Spec.Crux;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Crux
{
    public class Livro : DomainBase<Livro, ILivroRepositorio<Livro>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual string UrlCapa { get; set; }

        public virtual string UrlItunes { get; set; }

        public virtual string UrlGoogle { get; set; }

        public virtual string CultureName { get; set; }

        public virtual ModoCompra ModoCompra { get; set; }

        public virtual string Versao { get; set; }

        public virtual DateTime DataPublicacao { get; set; }
    }
}