﻿using System;
using System.Security.Principal;
using Lalubema.Orion.Common.Exceptions.Titanium.Torneio;
using Lalubema.Orion.Common.Spring.Repository.Listener;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Titanium
{
    public class TorneioUsuario : DomainBase<TorneioUsuario, ITorneioAtletaRepositorio<TorneioUsuario>>, IDateModified
    {
        protected TorneioUsuario()
        {
        }

        public TorneioUsuario(Torneio torneio, Usuario atleta)
        {
            Torneio = torneio;
            Usuario = atleta;
            Status = Common.Tipos.StatusUsuarioTorneio.SemConvite;
            Administrador = false;
            Token = GerarToken();
        }

        public TorneioUsuario(Torneio torneio, Usuario atleta, Usuario responsavelConvite)
        {
            Torneio = torneio;
            Usuario = atleta;
            ResponsavelConvite = responsavelConvite;
            Status = Common.Tipos.StatusUsuarioTorneio.SemConvite;
            Administrador = false;
            Token = GerarToken();
        }

        public virtual Usuario Usuario { get; set; }

        public virtual Torneio Torneio { get; set; }

        public virtual string Token { get; protected set; }

        public virtual DateTime? DataValidadeToken { get; set; }

        public virtual Common.Tipos.StatusUsuarioTorneio Status { get; set; }

        public virtual bool Administrador { get; set; }

        public virtual Usuario ResponsavelConvite { get; set; }

        #region IDateModified Members

        public virtual DateTime LastUpdate { get; set; }

        #endregion

        private static string GerarToken()
        {
            return SecurityHelper.CreateNumericPassword();
        }

        #region [ Equality ]

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (TorneioUsuario)) return false;
            return Equals((TorneioUsuario) obj);
        }

        public virtual bool Equals(TorneioUsuario other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Usuario, Usuario) && Equals(other.Torneio, Torneio) && Equals(other.Token, Token) && other.DataValidadeToken.Equals(DataValidadeToken) && Equals(other.Status, Status) && other.LastUpdate.Equals(LastUpdate);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (Usuario != null ? Usuario.GetHashCode() : 0);
                result = (result*397) ^ (Torneio != null ? Torneio.GetHashCode() : 0);
                result = (result*397) ^ (Token != null ? Token.GetHashCode() : 0);
                result = (result*397) ^ DataValidadeToken.GetHashCode();
                result = (result*397) ^ Status.GetHashCode();
                result = (result*397) ^ LastUpdate.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(TorneioUsuario left, TorneioUsuario right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TorneioUsuario left, TorneioUsuario right)
        {
            return !Equals(left, right);
        }

        #endregion [ Equality ]

        public override void Save()
        {
            if (!Administrador && ResponsavelConvite == null)
                throw new ParticipanteTorneioInvalidoException();

            base.Save();
        }
    }
}