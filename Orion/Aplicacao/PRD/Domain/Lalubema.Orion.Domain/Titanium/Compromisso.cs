﻿using System;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Compromisso : DomainBase<Compromisso, ICompromissoRepositorio<Compromisso>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual DateTime DataInicio { get; set; }

        public virtual DateTime DataFim { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}