﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Titanium;
using Lalubema.Orion.Common.Exceptions.Titanium.Torneio;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Torneio : DomainBase<Torneio, ITorneioRepositorio<Torneio>>
    {
        private bool _isAdministrador;

        protected Torneio()
        {
        }

        public Torneio(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Torneio(string nome)
        {
            Nome = nome;
        }

        public Torneio(int codigo, string nome, DateTime inicio, DateTime fim, int quantidadeJogadorPorEquipe,
                       DateTime tempoMaximoPartida)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);

            Nome = nome;
            Inicio = inicio;
            Fim = fim;
            QuantidadeJogadorPorEquipe = quantidadeJogadorPorEquipe;
            TempoMaximoPartida = tempoMaximoPartida;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual DateTime Inicio { get; set; }

        public virtual DateTime Fim { get; set; }

        public virtual int QuantidadeJogadorPorEquipe { get; set; }

        public virtual DateTime TempoMaximoPartida { get; set; }

        public virtual IList<TorneioEquipe> EquipesTorneio { get; set; }

        public virtual IList<Usuario> Usuarios { get; set; }

        public virtual IList<Rodada> Rodadas { get; set; }

        public virtual IList<TorneioUsuario> Administradores { get; set; }

        public virtual bool IsAdministrador
        {
            get { return _isAdministrador; }
        }

        public static Torneio Buscar(int codigo)
        {
            return GetRepository().Get(codigo);
        }

        public override void Save()
        {
            if (Inicio.CompareTo(Fim) >= 0)
                throw new DataInicialMenorOuIgualDataFinalException();

            if (Inicio.CompareTo(DateTime.Now.Date) < 0)
                throw new DataInvalidaException();

            base.Save();
        }

        public virtual void GerarTabela()
        {
            if (Rodadas != null && Rodadas.Count > 0)
                throw new Exception();

            Rodada rodada = GerarPartidasRodada();

            ProcessarDataRodada(rodada);

            Save();
        }

        private void ProcessarDataRodada(Rodada rodada)
        {
            IList<Quadra> quadrasDisponiveis = ObjectFactory.GetSingleton<IQuadraRepositorio<Quadra>>().ListAll();

            foreach (Partida partida in rodada.Partidas)
            {
                List<DisponibilidadeQuadra> disponibilidades = (from quadra in quadrasDisponiveis
                                                                from disponibilidade in quadra.Disponibilidades
                                                                orderby disponibilidade.DataInicial
                                                                select disponibilidade).ToList();

                IList<Compromisso> compromissos = (from equipe in partida.Equipes
                                                   from atleta in equipe.Equipe.Usuarios
                                                   from compromisso in atleta.Compromissos
                                                   select compromisso).ToList();

                DateTime dataInicio = DateTime.Now.Date;

                if (Inicio > dataInicio)
                    dataInicio = Inicio;


                while (dataInicio <= Fim && !ProximaDataPartida(dataInicio, disponibilidades, compromissos, partida))
                    dataInicio = dataInicio.AddDays(1);
            }
        }

        private bool ProximaDataPartida(DateTime dataInicio, IEnumerable<DisponibilidadeQuadra> disponibilidadesQuadras,
                                        IEnumerable<Compromisso> compromissosAtleta, Partida partida)
        {
            foreach (DisponibilidadeQuadra disponibilidade in disponibilidadesQuadras)
            {
                DateTime? data = null;
                DisponibilidadeQuadra disponibilidadeQuadra = disponibilidade;

                switch (disponibilidadeQuadra.TipoOcorrencia)
                {
                    case Common.Tipos.TipoOcorrencia.Unico:
                        if (disponibilidadeQuadra.DataInicial.Date == dataInicio.Date)
                            data = GetValidDate(disponibilidadeQuadra, compromissosAtleta);

                        break;
                    case Common.Tipos.TipoOcorrencia.Diario:
                        if (disponibilidadeQuadra.DataInicial.Date <= dataInicio.Date)
                            data = GetValidDate(disponibilidadeQuadra, compromissosAtleta);

                        break;
                    case Common.Tipos.TipoOcorrencia.Semanal:
                        if (disponibilidadeQuadra.DataInicial.DayOfWeek == dataInicio.DayOfWeek &&
                            disponibilidadeQuadra.DataInicial.Day == dataInicio.Day)
                            data = GetValidDate(disponibilidadeQuadra, compromissosAtleta);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (!data.HasValue || disponibilidadeQuadra.Quadra.ProximasPartida.Any(p => p.Data == data.Value))
                    continue;

                partida.Data = data.Value;
                partida.Quadra = disponibilidade.Quadra;

                return true;
            }

            return false;
        }

        private Rodada GerarPartidasRodada()
        {
            List<Equipe> equipes = (from e in EquipesTorneio orderby e.Ordem select e.Equipe).ToList();

            Rodada rodada = GerarRodada();

            if (rodada.Partidas == null)
                rodada.Partidas = new List<Partida>();

            if (equipes.Count < 2)
                throw new QuantidadeEquipeInvalidoException();

            for (int i = 0; i < equipes.Count; i += 2)
            {
                var partida = new Partida {Rodada = rodada, Status = Common.Tipos.StatusPartida.Aprovada};

                var equipe2 = new PartidaEquipe {Equipe = equipes[i + 1], Partida = partida};
                var equipe1 = new PartidaEquipe {Equipe = equipes[i], Partida = partida};

                partida.Equipes = new List<PartidaEquipe> {equipe1, equipe2};

                rodada.Partidas.Add(partida);
            }

            return rodada;
        }

        private Rodada GerarRodada()
        {
            if (Rodadas == null)
                Rodadas = new List<Rodada>();

            var rodada = new Rodada {NumeroRodada = Rodadas.Count + 1, Torneio = this};

            Rodadas.Add(rodada);

            return rodada;
        }


        private DateTime? GetValidDate(DisponibilidadeQuadra disponibilidadeQuadra,
                                       IEnumerable<Compromisso> compromissosAtleta)
        {
            List<Compromisso> compromissos = (from compromisso in compromissosAtleta
                                              where
                                                  (compromisso.DataInicio >= disponibilidadeQuadra.DataInicial &&
                                                   compromisso.DataInicio <= disponibilidadeQuadra.DataFinal) ||
                                                  (compromisso.DataFim >= disponibilidadeQuadra.DataInicial &&
                                                   compromisso.DataFim <= disponibilidadeQuadra.DataFinal)
                                              select compromisso).ToList();

            TimeSpan tempoTotalDisponibilidade = (disponibilidadeQuadra.DataFinal.TimeOfDay -
                                                  disponibilidadeQuadra.DataInicial.TimeOfDay);

            if (tempoTotalDisponibilidade > TempoMaximoPartida.TimeOfDay)
            {
                long quantidadePartidas = tempoTotalDisponibilidade.Ticks/TempoMaximoPartida.TimeOfDay.Ticks;

                DateTime dataPartida = disponibilidadeQuadra.DataInicial;

                for (int i = 0; i < quantidadePartidas; i++)
                {
                    DateTime horarioPartida = dataPartida;

                    List<Compromisso> c = (from compromisso in compromissos
                                           where
                                               (compromisso.DataInicio >= horarioPartida &&
                                                compromisso.DataInicio <=
                                                horarioPartida.AddTicks(TempoMaximoPartida.Ticks)) ||
                                               (compromisso.DataFim >= horarioPartida &&
                                                compromisso.DataFim <= horarioPartida.AddTicks(TempoMaximoPartida.Ticks))
                                           select compromisso).ToList();

                    if (c.Count == 0)
                        return dataPartida;

                    dataPartida = dataPartida.AddTicks(TempoMaximoPartida.Ticks);
                }
            }
            else
            {
                if (tempoTotalDisponibilidade < TempoMaximoPartida.TimeOfDay || compromissos.Count > 0)
                    return null;
            }

            return null;
        }

        public virtual void SetAdministrador(string emailAdministrador)
        {
            var usuarioRepositorio = ObjectFactory.GetSingleton<IUsuarioRepositorio<Usuario>>();

            Usuario usuario = usuarioRepositorio.BuscarUsuarioPorEmail(emailAdministrador);

            if (usuario == null)
                throw new EmailAdministradorInvalidoException();

            if (Administradores == null)
                Administradores = new List<TorneioUsuario>();

            TorneioUsuario torneioUsuario = usuario.Torneios.FirstOrDefault(t => t.Torneio.Codigo == Codigo);

            foreach (TorneioUsuario administrador in Administradores)
                administrador.Administrador = false;

            if (torneioUsuario == null)
                Administradores.Add(new TorneioUsuario(this, usuario) { Administrador = true });
            else
                torneioUsuario.Administrador = true;
        }

        public virtual bool VerificarAdministrador(string emailUsuario)
        {
            _isAdministrador =
                Administradores.Any(
                    a => a.Usuario.Username.Equals(emailUsuario, StringComparison.InvariantCultureIgnoreCase));

            return IsAdministrador;
        }
    }
}