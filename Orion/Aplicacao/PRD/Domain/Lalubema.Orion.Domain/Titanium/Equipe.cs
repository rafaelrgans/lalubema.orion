﻿using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Titanium.Equipe;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Equipe : DomainBase<Equipe, IEquipeRepositorio<Equipe>>
    {
        protected Equipe(){}

        public Equipe(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Equipe(string nome)
        {
            Nome = nome;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual IList<Usuario> Usuarios { get; set; }

        public virtual IList<TorneioEquipe> Torneios { get; set; }

        public static Equipe Buscar(int codigo)
        {
            return GetRepository().Get(codigo);
        }

        public override void Save()
        {
            foreach (var torneio in Torneios)
            {
                if (torneio.Torneio.QuantidadeJogadorPorEquipe != Usuarios.Count)
                    throw new QuantidadeAtletasInvalidoException(torneio.Torneio.Nome,
                                                                 torneio.Torneio.QuantidadeJogadorPorEquipe);

                var t = torneio;

                foreach (var atleta in from atleta in Usuarios
                                       from equipe in atleta.Equipes
                                       from torneioEquipe in equipe.Torneios
                                       where torneioEquipe.Torneio.Codigo == t.Torneio.Codigo
                                       where equipe.Codigo != Codigo
                                       select atleta)
                {
                    throw new AtletaJaParticipaOutraEquipeException(atleta.Nome);
                }
            }

            base.Save();
        }
    }
}