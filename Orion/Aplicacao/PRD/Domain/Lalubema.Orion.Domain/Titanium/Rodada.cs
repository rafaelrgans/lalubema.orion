﻿using System.Collections.Generic;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Rodada : DomainBase<Rodada, IRodadaRepositorio<Rodada>>
    {
        public virtual int Codigo { get; set; }

        public virtual int NumeroRodada { get; set; }

        public virtual Torneio Torneio { get; set; }

        public virtual IList<Partida> Partidas { get; set; }
    }
}