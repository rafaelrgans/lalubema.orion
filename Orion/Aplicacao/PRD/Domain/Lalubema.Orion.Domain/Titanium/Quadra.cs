﻿using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Quadra : DomainBase<Quadra, IQuadraRepositorio<Quadra>>
    {
        protected Quadra()
        {
        }

        public Quadra(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Quadra(string nome)
        {
            Nome = nome;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Telefone { get; set; }

        public virtual Endereco Endereco { get; set; }

        public virtual IList<DisponibilidadeQuadra> Disponibilidades { get; set; }

        public virtual IList<Partida> ProximasPartida { get; set; }

        public static Quadra Buscar(int codigo)
        {
            return GetRepository().Get(codigo);
        }

        public override void Save()
        {
            if (Disponibilidades != null)
            {
                if (Disponibilidades.Any(disponibilidade => disponibilidade.DataInicial >= disponibilidade.DataFinal))
                    throw new DataInicialMenorOuIgualDataFinalException();
            }

            base.Save();
        }
    }
}