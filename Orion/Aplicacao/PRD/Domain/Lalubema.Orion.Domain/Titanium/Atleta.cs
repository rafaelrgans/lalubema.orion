﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions;
using Lalubema.Orion.Common.Helpers.Comunicacao;
using Lalubema.Orion.Domain.DataTransferObject.Titanium;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Atleta : DomainBase<Atleta, IAtletaRepositorio<Atleta>>
    {
        private bool _conviteTorneio;

        protected Atleta()
        {
        }

        public Atleta(int codigo)
        {
            Codigo = codigo;

            if (codigo > 0)
                GetRepository().Load(this, codigo);
        }

        public Atleta(string nome, string email)
        {
            Nome = nome;
            Email = email;
        }

        public Atleta(string email)
        {
            Email = email;
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Email { get; set; }

        public virtual string Senha { get; set; }

        public virtual int DDD { get; set; }

        public virtual string Celular { get; set; }

        public virtual string Cep { get; set; }

        public virtual DateTime? DataNascimento { get; set; }

        public virtual Categoria Categoria { get; set; }

        public virtual DateTime? DataCadastro { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

        public virtual IList<TorneioUsuario> Torneios { get; set; }

        public virtual IList<Compromisso> Compromissos { get; set; }

        public virtual IList<Equipe> Equipes { get; set; }

        public static Atleta Buscar(int codigo)
        {
            return GetRepository().Get(codigo);
        }

        public override void Save()
        {
            var usuarioNovo = Codigo == 0;

            if (usuarioNovo)
                DataCadastro = DateTime.Now;

            base.Save();

            if (usuarioNovo)
            {
                EnviarConviteTorneios();

                if (!_conviteTorneio && !DataAtivacao.HasValue)
                    EnviarEmailConfirmacao();
            }
        }

        private void EnviarConviteTorneios()
        {
            if (Torneios != null)
            {
                foreach (TorneioUsuario torneio in
                    Torneios.Where(torneio => torneio.Status == Common.Tipos.StatusUsuarioTorneio.SemConvite))
                {
                    var linkDownloadAplicacao = Parametro.BuscarPorChave(Constantes.Titanium.Configuracao.ItunesUrl);
                    var assuntoConviteTorneio = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.AssuntoConviteTorneio);
                    var templateConvite = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.TemplateEmailConvite);

                    //EmailHelper.EnviarEmail(Email, String.Format(assuntoConviteTorneio, torneio.ResponsavelConvite.Nome, torneio.Torneio.Nome),
                    //                        string.Format(templateConvite, Nome, torneio.ResponsavelConvite.Nome, linkDownloadAplicacao));

                    torneio.DataValidadeToken = DateTime.Now.AddDays(3);
                    torneio.Status = Common.Tipos.StatusUsuarioTorneio.Convidado;
                }

                base.Save();
            }
        }

        private void EnviarEmailConfirmacao()
        {
            var chaveConfirmacao = SecurityHelper.CreateCryptographyKey(Email, Nome, DateTime.Now.ToString());

            var linkConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.LinkConfirmacao);
            var assuntoConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.AssuntoEmailConfirmacao);
            var templateConfirmacao = Parametro.BuscarPorChave(Constantes.Titanium.Usuario.Comunicacao.TemplateEmailConfirmacao);

            //EmailHelper.EnviarEmail(Email, assuntoConfirmacao, string.Format(templateConfirmacao, Nome, linkConfirmacao, chaveConfirmacao));
        }

        public virtual void SalvarCompromissos(DTOCompromisso[] compromissos)
        {
            foreach (Compromisso c in compromissos.Select(compromisso => new Compromisso
                                                                             {
                                                                                 DataFim = compromisso.DataFim,
                                                                                 DataInicio = compromisso.DataInicio,
                                                                                 Descricao = compromisso.Descricao
                                                                             }))
            {
                Compromissos.Add(c);
            }

            base.Save();
        }

        public virtual void ExcluirCompromissos(DTOCompromisso[] compromissos)
        {
            foreach (
                Compromisso compromisso in
                    compromissos.Select(c => Compromissos.FirstOrDefault(o => o.Codigo == c.Codigo)).Where(
                        compromisso => compromisso != null))
            {
                compromisso.Delete();
            }
        }

        public virtual void EnviarSenha()
        {
            var novaSenha = SecurityHelper.CreatePassword();

            Senha = SecurityHelper.EncryptToSHA1(novaSenha);

            //EmailHelper.EnviarEmail(Email, "Recuperação de Senha", "Você solicitou uma nova senha. Senha: " + novaSenha);
        }

        public virtual void AlterarSenha(string senhaAtual, string novaSenha)
        {
            senhaAtual = SecurityHelper.EncryptToSHA1(senhaAtual);

            if (!senhaAtual.Equals(Senha))
                throw new UsuarioOuSenhaInvalidosException();

            Senha = SecurityHelper.EncryptToSHA1(novaSenha);

            Save();
        }

        public virtual void Ativar(string[] conta)
        {
            // TODO: Criar exceção para quando o usuário já estiver ativado.
            if (DataAtivacao.HasValue)
                throw new Exception("Usuário já ativado"); //UsuarioJaAtivadoException(Email);

            DataAtivacao = DateTime.Now;

            base.Save();
        }

        public virtual void ConvidarParaTorneio(Torneio torneio, Atleta responsavelConvite)
        {
            if (Torneios == null)
                Torneios = new List<TorneioUsuario>();

            //if (!Torneios.Any(torneioAtleta => torneioAtleta.Torneio.Codigo == torneio.Codigo))
            //    Torneios.Add(new TorneioUsuario(torneio, this, responsavelConvite));

            _conviteTorneio = true;

            Save();
        }
    }
}