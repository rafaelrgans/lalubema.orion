﻿using System;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Titanium
{
    public class Mensagem : DomainBase<Mensagem, IMensagemRepositorio<Mensagem>>
    {
        public virtual int Codigo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual DateTime Inicio { get; set; }

        public virtual DateTime Fim { get; set; }

        public virtual DateTime Cadastro { get; set; }

        public virtual Torneio Torneio { get; set; }

        public override void Save()
        {
            if (Codigo == 0)
                Cadastro = DateTime.Now;

            base.Save();
        }
    }
}