using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.LocalBem;
using Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Base;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Types;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class LocalBem : DomainBase<LocalBem, ILocalBemRepositorio<LocalBem>>, IDomainModel
    {
        private IList<DisponibilidadeLocalBem> _disponibilidades;

        private IList<Edificio> _edificios;

        private IList<LocalBem> _preRequisitos;

        private IList<ReservaLocalBem> _reservas;

        public LocalBem()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual TipoLocalBem Tipo { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int AntecedenciaMaxReserva { get; set; }

        public virtual int PrazoConfirmacao { get; set; }

        public virtual int TempoPreparoUtilizacao { get; set; }

        public virtual int? TempoMaximoUtilizacao { get; set; }

        public virtual string Situacao { get; set; }

        public virtual string Obs { get; set; }

        public virtual string Foto { get; set; }

        public virtual string FotoBase64 { get; set; }

        public virtual IList<LocalBem> PreRequisitos
        {
            get { return _preRequisitos; }
            set { _preRequisitos = value; }
        }

        public virtual LocalBem Local { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual IList<DisponibilidadeLocalBem> Disponibilidades
        {
            get { return _disponibilidades; }
            set { _disponibilidades = value; }
        }

        public virtual IList<ReservaLocalBem> Reservas
        {
            get { return _reservas; }
            set { _reservas = value; }
        }

        [NoMap]
        public virtual IList<Edificio> Edificios
        {
            get { return _edificios; }
            set { _edificios = value; }
        }

        public virtual void Save(int codigoCondominio, IList<DTOEdificio> edificios)
        {
            var oldUrlFoto = Foto;

            try
            {
                if (codigoCondominio > 0 && (Condominio == null || Condominio.Codigo != codigoCondominio))
                    Condominio = new Condominio().Get(codigoCondominio);

                if (edificios != null)
                {
                    foreach (var e in from edificio in edificios
                                      where Edificios.All(t => t.Codigo != edificio.Codigo)
                                      select new Edificio().Get(edificio.Codigo))
                    {
                        e.AreasComum.Add(this);

                        Edificios.Add(e);
                    }
                }

                if (FotoBase64 != null)
                {
                    using (
                        var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                        Convert.FromBase64String(FotoBase64),
                                                        ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Aplicacao.Corona, extension, fileHelper.File);

                        Foto = retorno.Path;
                    }
                }

                base.Save(true);
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("LocalBem", Nome, exception);
            }
            finally
            {
                if (!string.IsNullOrWhiteSpace(oldUrlFoto))
                    FileSystem.DeleteFile(Aplicacao.Corona, oldUrlFoto);
            }
        }

        private void OnCreated()
        {
            _preRequisitos = new List<LocalBem>();
            _disponibilidades = new List<DisponibilidadeLocalBem>();
            _reservas = new List<ReservaLocalBem>();
            _edificios = new List<Edificio>();
        }

        public virtual void Reservar(DTOReservaLocalBemEntrada reservaLocalBem)
        {
            var dateRange = new DateTimeRange(reservaLocalBem.Inicio, reservaLocalBem.Fim);

            if (Reservas.Any(t => dateRange.Intersects(new DateTimeRange(t.DataHoraInicio, t.DataHoraFim))))
                throw new InvalidDateRangeException();

            if (AntecedenciaMaxReserva > 0 &&
                !new DateTimeRange(DateTime.UtcNow, DateTime.UtcNow.AddDays(AntecedenciaMaxReserva)).Intersects(
                    dateRange))
                throw new InvalidDateRangeException();

            if (TempoMaximoUtilizacao.HasValue &&
                reservaLocalBem.Fim.Subtract(reservaLocalBem.Inicio).Days > TempoMaximoUtilizacao)
                throw new InvalidDateRangeException();

            // Define o solicitante da Reserva
            var security = new ChaveCriptografadaHelper(reservaLocalBem.ChaveCriptografada);
            int codigoUsuario = reservaLocalBem.CodigoPessoa.HasValue ? reservaLocalBem.CodigoPessoa.Value : security.CodigoUsuario;
            var responsavel = new Usuario().Get(codigoUsuario);

            // Define a Unidade Habitacional
            var unidadadeHabitacional = new UnidadeHabitacional().Get(reservaLocalBem.CodigoUnidadeHabitacional);

            // Salva a Reserva
            var reserva = new ReservaLocalBem
                              {
                                  DataHoraInicio = reservaLocalBem.Inicio,
                                  DataHoraFim = reservaLocalBem.Fim,
                                  LocalBem = this,
                                  Pessoa = responsavel,
                                  UnidadeHabitacional = unidadadeHabitacional,
                                  Status = StatusReserva.Confirmada
                              };
            Reservas.Add(reserva);
            base.Save();
        }

        public virtual IList<ReservaLocalBem> GetProximasReservas()
        {
            return Reservas.Where(t => t.IsTodayOrAfter()).ToList();
        }

        public virtual IList<LocalBem> ListAll(int codigoUsuario, int codigoEdificio)
        {
            var usuario = new Usuario().Get(codigoUsuario);

            var usuarioHasPermission =
                usuario.UnidadesHabitacional.Any(t => t.UnidadeHabitacional.Edificio.Codigo == codigoEdificio);

            if (!usuarioHasPermission)
                throw new Utilitarios.Exceptions.Security.UnauthorizedAccessException();

            return Repository.ListAll(codigoEdificio);
        }
    }
}