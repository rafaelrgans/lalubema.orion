
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Communication;
using Lalubema.Utilitarios.Helper.IO;
using Lalubema.Utilitarios.Helper.Security;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Exceptions.Base;

namespace Lalubema.Orion.Domain.Corona
{
    public class Usuario : DomainBase<Usuario, IUsuarioRepositorio<Usuario>>, IUsuario
    {
        private IList<Device> _devices;

        private IList<AcessoPessoa> _acessos;

        private IList<AutorizacaoEntradaEntrega> _autorizacoes;

        private IList<ChegadaSegura> _chegadasSeguraMorador;

        private IList<ChegadaSegura> _chegadasSeguraPorteiro;

        private IList<Classificado> _classificados;

        private IList<Convenio> _convenios;

        private IList<Mensagem> _mensagensEnviadas;

        private IList<PessoaCondominio> _pessoaCondominios;

        private IList<ReservaLocalBem> _reservaLocalBems;

        private IList<ReservaLocalBem> _reservasAreaComum;

        private IList<PessoaUnidadeHabitacional> _unidadesHabitacional;

        private IList<Mensagem> _mensagensRecebidas;

        public Usuario()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Nome { get; set; }

        [Map("Email")]
        public virtual string Username { get; set; }

        [NoMap]
        public virtual string Senha { get; set; }

        [Map("Senha")]
        public virtual string NovaSenha { get; set; }

        public virtual DateTime? DataAtivacao { get; set; }

		public virtual DateTime? Created { get; set; }

        public virtual TipoUsuario TipoUsuario { get; set; }

        public virtual Aplicacao Aplicacao { get; set; }

        public virtual string DocIdent { get; set; }

        public virtual DateTime? DataNascimento { get; set; }

        public virtual Sexo Sexo { get; set; }

        public virtual string Telefone { get; set; }

        public virtual string UrlFoto { get; set; }

        public virtual string Foto { get; set; }

        public virtual IList<Device> Devices
        {
            get { return _devices; }
            set { _devices = value; }
        }

        [NoMap]
        public virtual IList<AcessoPessoa> Acessos
        {
            get { return _acessos; }
            set { _acessos = value; }
        }

        public virtual IList<AutorizacaoEntradaEntrega> Autorizacoes
        {
            get { return _autorizacoes; }
            set { _autorizacoes = value; }
        }

        public virtual IList<ChegadaSegura> ChegadasSeguraMorador
        {
            get { return _chegadasSeguraMorador; }
            set { _chegadasSeguraMorador = value; }
        }

        public virtual IList<ChegadaSegura> ChegadasSeguraPorteiro
        {
            get { return _chegadasSeguraPorteiro; }
            set { _chegadasSeguraPorteiro = value; }
        }

        public virtual IList<Classificado> Classificados
        {
            get { return _classificados; }
            set { _classificados = value; }
        }

        public virtual IList<Convenio> Convenios
        {
            get { return _convenios; }
            set { _convenios = value; }
        }

        public virtual IList<Mensagem> MensagensEnviadas
        {
            get { return _mensagensEnviadas; }
            set { _mensagensEnviadas = value; }
        }

        [NoMap]
        public virtual IList<PessoaCondominio> PessoaCondominios
        {
            get { return _pessoaCondominios; }
            set { _pessoaCondominios = value; }
        }

        [NoMap]
        public virtual IList<PessoaUnidadeHabitacional> UnidadesHabitacional
        {
            get { return _unidadesHabitacional; }
            set { _unidadesHabitacional = value; }
        }

        public virtual IList<ReservaLocalBem> ReservasAreaComum
        {
            get { return _reservasAreaComum; }
            set { _reservasAreaComum = value; }
        }

        public virtual IList<ReservaLocalBem> ReservaLocalBems
        {
            get { return _reservaLocalBems; }
            set { _reservaLocalBems = value; }
        }

        public virtual IList<Mensagem> MensagensRecebidas
        {
            get { return _mensagensRecebidas; }
            set { _mensagensRecebidas = value; }
        }

        public virtual bool EnviarConvite { get; set; }

        public virtual void Save(IList<DTOPessoaUnidadeHabitacional> unidadeHabitacionals,
                                 IList<DTOPessoaCondominio> pessoaCondominios,
                                 IList<DTOPessoaAcesso> acessos)
        {
            try
            {
                ValidarDadosUsuario()
                    .CriarSenha()
                    .AdicionarUnidadesHabitacionais(unidadeHabitacionals)
                    .AdicionarCondominios(pessoaCondominios)
                    .AdicionarAcessos(acessos)
                    .SalvarFoto()
                    .SalvarUsuario()
                    .EnviarConviteUnidadeHabitacional()
                    .EnviarConviteCondominio()
                    .EnviarEmailAtivacao();
            }
            catch (BaseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }
        }

        public virtual void Validar(string usuario, string senha, string versao)
        {
            senha = SecurityHelper.EncryptToSHA1(senha);

            var u = Repository.Get(usuario, senha);

            if (u == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!u.Aplicacao.Versao.Equals(versao, StringComparison.InvariantCultureIgnoreCase))
                throw new VersionNotFoundException(u.Aplicacao.Versao);

            if (!u.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(usuario);

            var codigo = u.Codigo;

            Repository.Evict(u);

            Load(codigo);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            try
            {
                var usuario = Repository.Get(userName);

                if (usuario == null)
                    throw new UsuarioOuSenhaInvalidosException();

                if (usuario.DataAtivacao.HasValue)
                    throw new UsuarioJaAtivadoException(userName);

                usuario.DataAtivacao = DateTime.UtcNow;

                if (usuario.UnidadesHabitacional != null && usuario.UnidadesHabitacional.Count > 0)
                {
                    var unidadeHabitacional =
                        usuario.UnidadesHabitacional.OrderByDescending(t => t.Created).ElementAt(0);

                    unidadeHabitacional.DataAtivacao = DateTime.UtcNow;
                }

                usuario.Save();

                return Parametro.BuscarPorChave(Constantes.Corona.Usuario.TemplateAtivacao);
            }
            catch (Exception exception)
            {
                return "Falha na ativa��o: " + exception.Message;
            }
        }

        public virtual void RecoveryPassword(string username)
        {
            var usuario = Repository.Get(username);

            if (usuario == null)
                throw new UsuarioOuSenhaInvalidosException();

            if (!usuario.DataAtivacao.HasValue)
                throw new UsuarioNaoAtivadoException(username);

            var novaSenha = SecurityHelper.CreateNumericPassword();

            usuario.Senha = SecurityHelper.EncryptToSHA1(novaSenha);

            usuario.Save();

            var assuntoRecuperacaoSenha =
                Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.AssuntoEmailRecuperacaoSenha);
            var templateRecuperacaoSenha =
                Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TemplateEmailRecuperacaoSenha);

            EmailHelper.EnviarEmail(GetEmailSettings(username), assuntoRecuperacaoSenha,
                                    string.Format(templateRecuperacaoSenha, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                                                  novaSenha));
        }

        private void OnCreated()
        {
            _devices = new List<Device>();
            _acessos = new List<AcessoPessoa>();
            _autorizacoes = new List<AutorizacaoEntradaEntrega>();
            _chegadasSeguraMorador = new List<ChegadaSegura>();
            _chegadasSeguraPorteiro = new List<ChegadaSegura>();
            _classificados = new List<Classificado>();
            _convenios = new List<Convenio>();
            _mensagensEnviadas = new List<Mensagem>();
            _pessoaCondominios = new List<PessoaCondominio>();
            _unidadesHabitacional = new List<PessoaUnidadeHabitacional>();
            _reservasAreaComum = new List<ReservaLocalBem>();
            _reservaLocalBems = new List<ReservaLocalBem>();
            _mensagensRecebidas = new List<Mensagem>();
        }

        private static EmailSettings GetEmailSettings(string sendTo)
        {
            return new EmailSettings
                       {
                           To = sendTo,
                           From = Parametro.BuscarPorChave(Constantes.Corona.Comunicacao.Email.Remetente),
                           ServerName = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServer),
                           ServerPort = Convert.ToInt32(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpServerPort)),
                           UseSSL = Convert.ToBoolean(Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SmtpUseSSL)),
                           Username = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.UsuarioSmtp),
                           Password = Parametro.BuscarPorChave(Constantes.Orion.Comunicacao.Email.SenhaSmtp)
                       };
        }

        public virtual Condominio GetCondominio(int condominio)
        {
            var pessoaCondominio = PessoaCondominios.FirstOrDefault(t => t.Condominio.Codigo == condominio);

            if (pessoaCondominio == null)
                throw new ItemNotFoundException(condominio, "Condominio");

            return pessoaCondominio.Condominio;
        }

        public virtual DTOGetPush ListarNotificacao(int codigoCondominio)
        {
            // Condom�nio
            var condominio = new Condominio().Get(codigoCondominio);
            IEnumerable<UnidadeHabitacional> unidadesHabitacionais = null;
            IEnumerable<Veiculo> veiculos = null;

            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.UtcNow.Subtract(this.DurationPush), DateTime.MaxValue);

            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao notificacao in notificacoes)
            {
                // Define o formato da notifica��o
                switch (GetFormatoNotificacao(notificacao.TipoMensagem))
                {
                    case FormatoNotificacaoEnum.Chegada:
                        {
                            // Lazzy Pattern
                            if (veiculos == null)
                                veiculos = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional.SelectMany(x => x.Veiculos));

                            // Filtro os ve�culos do Condom�nio
                            if (veiculos.Any(e => e.Codigo == notificacao.Origem))
                            {
                                var veiculo = new Veiculo().Get(notificacao.Origem);
                                var chegada = GetNotificacaoChegadaArgs(veiculo, notificacao);
                                result.Chegada.Add(chegada);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Portaria:
                        {
                            // Lazzy Pattern
                            if (unidadesHabitacionais == null)
                                unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                            // Filtro as Unidades Habitacionais do Condom�nio
                            if (unidadesHabitacionais.Any(e => e.Codigo == notificacao.Origem))
                            {
                                var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem);
                                var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                                result.Portaria.Add(portaria);
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Sindico:
                        {
                            if (notificacao.TipoMensagem == TipoNotificacao.SindicoPorMorador)
                            {
                                // Lazzy Pattern
                                if (unidadesHabitacionais == null)
                                    unidadesHabitacionais = condominio.Edificios.SelectMany(e => e.UnidadeHabitacional);

                                // Filtro as Unidades Habitacionais do Condom�nio
                                if (unidadesHabitacionais.Any(e => e.Codigo == notificacao.Origem))
                                {
                                    var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem);
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            else
                            {
                                // Filtro as notifica��es do Condom�nio
                                if (notificacao.Origem == condominio.Codigo)
                                {
                                    var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                                    result.Sindico.Add(notificacaoSindico);
                                }
                            }
                            break;
                        }
                    case FormatoNotificacaoEnum.Resposta:
                        {
                            // Ser� retornado no GetReplyPush (filtro por usu�rio)
                            break;
                        }
                }
            }
            return result;
        }

        public virtual DTOGetPush ListarNotificacaoResposta()
        {
            // Lista as Notifica��es dentro do per�odo 'DurationPush'
            var notificacoes = new Notificacao().ListRange(DateTime.UtcNow.Subtract(this.DurationPush), DateTime.MaxValue);

            // Percorre as notifica��o de Resposta (Reply)
            DTOGetPush result = new DTOGetPush();
            foreach (Notificacao reply in notificacoes.Where(e => e.TipoMensagem == TipoNotificacao.Resposta))
            {
                var origem = new Notificacao().Get(reply.Origem);

                // Filtro as notifica��es do Usu�rio
                if (origem.Remetente.Codigo == this.Codigo)
                {
                    var notificacaoResposta = GetNotificacaoRespostaArgs(origem, reply);
                    result.Resposta.Add(notificacaoResposta);
                }
            }

            return result;
        }

        /// <summary>
        /// Define o tempo que ser� considerado para busca das notifica��o (chegada/portaria) no servi�o GetPush
        /// </summary>
        private readonly TimeSpan DurationPush = new TimeSpan(1, 0, 0, 0);

        #region Notificar

        public virtual void Notificar(DTOSendPush sendPush)
        {
            switch (GetFormatoNotificacao(sendPush.TipoMensagem))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        this.NotificarChegada(sendPush);
                        break;
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        this.NotificarPortaria(sendPush);
                        break;
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        this.NotificarSindico(sendPush);
                        break;
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        this.NotificarResposta(sendPush);
                        break;
                    }

            }
        }

        private enum FormatoNotificacaoEnum { Chegada, Portaria, Sindico, Resposta }
        private FormatoNotificacaoEnum GetFormatoNotificacao(TipoNotificacao tipoNotificacao)
        {
            switch (tipoNotificacao)
            {
                case TipoNotificacao.Undefined:
                    throw new TipoMensagemInvalidaException();
                case TipoNotificacao.ChegadaSegura:
                    return FormatoNotificacaoEnum.Chegada;
                case TipoNotificacao.Servico:
                case TipoNotificacao.Visitante:
                case TipoNotificacao.Delivery:
                case TipoNotificacao.Outros:
                    return FormatoNotificacaoEnum.Portaria;
                case TipoNotificacao.SindicoPorFuncionario:
                case TipoNotificacao.SindicoPorMorador:
                    return FormatoNotificacaoEnum.Sindico;
                case TipoNotificacao.Resposta:
                    return FormatoNotificacaoEnum.Resposta;
                default:
                    throw new NotImplementedException();
            }
        }

        private Device GetDevice(string deviceToken)
        {
            if (!String.IsNullOrEmpty(deviceToken))
            {
                Device device = new Device().Get(deviceToken);
                return device;
            }
            else
                return null;
        }

        private void NotificarChegada(DTOSendPush sendPush)
        {
            // Salvar Notifica��o
            var veiculo = new Veiculo().Get(sendPush.Origem);
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Pendente,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow,
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os devices do Condom�nio
            var devices = veiculo.UnidadeHabitacional.Edificio.Condominio.Devices.Where(e => e.Usuario == null && e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                                      {
                                          Bundle = device.NotificationSettings.Bundle,
                                          DeviceToken = device.Token,
                                          LocalizedKey = LocalizedKey.Corona.ChegadaSegura,
                                          LocalizedArgs = GetNotificacaoChegadaArgs(veiculo, notificacao).ToList(),
                                          PlatformType = device.PlatformType
                                      };
                try
                {
                    NotificationManager.SendPushMessage(pushMessage);
                }
                catch (Exception)
                {
                }
            }
        }

        private DTOGetPushChegadaArgs GetNotificacaoChegadaArgs(Veiculo veiculo, Notificacao notificacao)
        {
            DTOGetPushChegadaArgs result = new DTOGetPushChegadaArgs()
            {
                Codigo = notificacao.Codigo,
                VeiculoMarca = veiculo.Marca,
                VeiculoModelo = veiculo.Modelo,
                VeiculoPlaca = veiculo.Placa,
                VeiculoCor = veiculo.Cor,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime(),
                TipoNotificacao = notificacao.TipoMensagem,
                EdificioNome = veiculo.UnidadeHabitacional.Edificio.Nome,
                UnidadeHabitacionalNumero = veiculo.UnidadeHabitacional.Numero
            };

            return result;
        }


        private void NotificarPortaria(DTOSendPush sendPush)
        {
            var unidadeHabitacional =
                UnidadesHabitacional.FirstOrDefault(t => t.UnidadeHabitacional.Codigo == sendPush.Origem);

            if (unidadeHabitacional == null)
                throw new ItemNotFoundException(sendPush.Origem, "UnidadeHabitacional");

            var condominio = unidadeHabitacional.UnidadeHabitacional.Edificio.Condominio;

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Pendente,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow,
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os devices do Condom�nio
            var devices = condominio.Devices.Where(e => e.Usuario == null && e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    DeviceToken = device.Token,
                    LocalizedKey = LocalizedKey.Corona.NotificarPortaria,
                    LocalizedArgs = GetNotificacaoPortariaArgs(unidadeHabitacional.UnidadeHabitacional, notificacao).ToList(), 
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }
        }

        private DTOGetPushPortariaArgs GetNotificacaoPortariaArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            DTOGetPushPortariaArgs result = new DTOGetPushPortariaArgs()
            {
                Codigo = notificacao.Codigo,
                UsuarioNome = notificacao.Remetente.Nome,
                EdificioNome = unidadeHabitacional.Edificio.Nome,
                UnidadeHabitacionalNumero = unidadeHabitacional.Numero,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime(),
                StatusNotificacao = notificacao.Status
            };
            return result;
        }

        private void NotificarSindico(DTOSendPush sendPush)
        {
            // Notifica��o do S�ndico oriundo Morador ou Funcion�rio
            Condominio condominio = null;
            UnidadeHabitacional unidadeHabitacional = null;
            if (sendPush.TipoMensagem == TipoNotificacao.SindicoPorMorador)
            {
                unidadeHabitacional = new UnidadeHabitacional().Get(sendPush.Origem);
                if (unidadeHabitacional == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Unidade Habitacional");
                condominio = unidadeHabitacional.Edificio.Condominio;
            }
            else
            {
                condominio = new Condominio().Get(sendPush.Origem);
                if (condominio == null)
                    throw new ItemNotFoundException(sendPush.Origem, "Condom�nio");
            }

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Pendente,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow,
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Envia para os Devices dos S�ndicos
            var sindicos = condominio.Funcionarios.Where(o => o.IsSindico());
            var devices = sindicos.SelectMany(x => x.Pessoa.Devices).Where(e => e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    DeviceToken = device.Token,
                    LocalizedKey = LocalizedKey.Corona.NotificarSindico,
                    LocalizedArgs = unidadeHabitacional != null ?
                                    GetNotificacaoSindicoArgs(unidadeHabitacional, notificacao).ToList() :
                                    GetNotificacaoSindicoArgs(condominio, notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(UnidadeHabitacional unidadeHabitacional, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(unidadeHabitacional.Codigo, unidadeHabitacional.Numero, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(Condominio condominio, Notificacao notificacao)
        {
            return GetNotificacaoSindicoArgs(condominio.Codigo, condominio.Nome, notificacao);
        }

        private DTOGetPushSindicoArgs GetNotificacaoSindicoArgs(int codigo, string descricao, Notificacao notificacao)
        {
            DTOGetPushSindicoArgs result = new DTOGetPushSindicoArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = codigo,
                OrigemDescricao = descricao,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime()
            };
            return result;
        }

        private void NotificarResposta(DTOSendPush sendPush)
        {
            var origem = new Notificacao().Get(sendPush.Origem);
            if (origem == null)
                throw new ItemNotFoundException(sendPush.Origem, "Notifica��o");

            // Salvar Notifica��o
            var notificacao = new Notificacao
            {
                Codigo = sendPush.CodigoMensagem,
                Mensagem = sendPush.Message,
                Origem = sendPush.Origem,
                Remetente = this,
                Status = StatusNotificacao.Pendente,
                TempoChegada = sendPush.TempoChegada,
                TipoMensagem = sendPush.TipoMensagem,
                DataNotificacao = DateTime.UtcNow,
                RemetenteDevice = GetDevice(sendPush.RemetenteDeviceToken)
            };
            notificacao.Save();

            // Altera o Status da Notifica��o de Origem
            origem.Status = StatusNotificacao.Respondido;
            origem.Save();

            // Envia para o Device da Notifica��o ou Devices do Remetente
            IEnumerable<Device> devices = origem.RemetenteDevice != null && origem.RemetenteDevice.Status == DeviceStatus.Active ?
                                          new List<Device>() { origem.RemetenteDevice } :
                                          origem.Remetente.Devices.Where(e => e.Status == DeviceStatus.Active);
            foreach (var device in devices)
            {
                var pushMessage = new DTOPushMessage
                {
                    Bundle = device.NotificationSettings.Bundle,
                    DeviceToken = device.Token,
					Message = device.PlatformType == PlatformType.Android ? sendPush.Message : "",
                    LocalizedKey = LocalizedKey.Corona.NotificarResposta,
                    LocalizedArgs = GetNotificacaoRespostaArgs(origem, notificacao).ToList(),
                    PlatformType = device.PlatformType
                };

                NotificationManager.SendPushMessage(pushMessage);
            }
        }

        private DTOGetPushRespostaArgs GetNotificacaoRespostaArgs(Notificacao origem, Notificacao notificacao)
        {
            DTOGetPushRespostaArgs result = new DTOGetPushRespostaArgs()
            {
                Codigo = notificacao.Codigo,
                RemetenteNome = notificacao.Remetente.Nome,
                OrigemCodigo = origem.Codigo,
                Mensagem = notificacao.Mensagem,
                TipoNotificacao = notificacao.TipoMensagem,
                DataHoraNotificacao = notificacao.DataNotificacao.ToBrasiliaTime()
            };
            return result;
        }

        private List<object> GetNotificacaoArgs(Notificacao notificacao)
        {
            // Define o formato da notifica��o
            switch (GetFormatoNotificacao(notificacao.TipoMensagem))
            {
                case FormatoNotificacaoEnum.Chegada:
                    {
                        var veiculo = new Veiculo().Get(notificacao.Origem);
                        var chegada = GetNotificacaoChegadaArgs(veiculo, notificacao);
                        return chegada.ToList();
                    }
                case FormatoNotificacaoEnum.Portaria:
                    {
                        var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem);
                        var portaria = GetNotificacaoPortariaArgs(unidadeHabilitacional, notificacao);
                        return portaria.ToList();
                    }
                case FormatoNotificacaoEnum.Sindico:
                    {
                        if (notificacao.TipoMensagem == TipoNotificacao.SindicoPorMorador)
                        {
                            var unidadeHabilitacional = new UnidadeHabitacional().Get(notificacao.Origem);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(unidadeHabilitacional, notificacao);
                            return notificacaoSindico.ToList();
                        }
                        else
                        {
                            var condominio = new Condominio().Get(notificacao.Origem);
                            var notificacaoSindico = GetNotificacaoSindicoArgs(condominio, notificacao);
                            return notificacaoSindico.ToList();
                        }
                    }
                case FormatoNotificacaoEnum.Resposta:
                    {
                        var origem = new Notificacao().Get(notificacao.Origem);
                        var resposta = GetNotificacaoRespostaArgs(origem, notificacao);
                        return resposta.ToList();
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        public virtual bool IsSindico(int unidadeHabitacional)
        {
            var u = new UnidadeHabitacional().Get(unidadeHabitacional);

            var codigoCondominio = u.Edificio.Condominio.Codigo;

            return PessoaCondominios.Any(t => t.Condominio.Codigo == codigoCondominio && t.IsSindico());
        }

        public virtual Usuario Get(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            return Get(security.CodigoUsuario);
        }

        #region [ Fluent ]

        public virtual string SenhaDescriptografada { get; set; }

        public virtual bool UsuarioNovo { get; set; }

        private bool FotoAlterada { get; set; }

        private IList<int> UnidadesAdicionadas { get; set; }

        private IList<int> CondominiosAdicionados { get; set; }

        /// <summary>
        /// Valida��o da unicidade do Usu�rio atrav�s das propriedades UserName (e-mail) e C�digo
        /// </summary>
        /// <remarks>
        /// O usu�rio ser� localizado para edi��o por C�digo (ID), quando n�o for informado por e-mail (username)
        /// Na edi��o, se houver altera��o de e-mail, validar colis�o com outros usu�rios existentes na base de dados
        /// </remarks>
        private Usuario ValidarDadosUsuario()
        {
            // Procura o usu�rio por e-mail (username)
            var usuario = String.IsNullOrEmpty(Username) ? null : Repository.Get(Username, Codigo);

            // N�o possui c�digo informado
            if (Codigo == 0)
            {
                // Pesquisa por e-mail (username) para edi��o
                if (usuario != null)
                    Load(usuario.Codigo);
            }
            else
            {
                // Valida se existe alguma colis�o de e-mail com outro usu�rio
                if (usuario != null && usuario.Codigo != Codigo)
                    throw new UsuarioJaCadastradoException(Username);
            }

            // Retorna a pr�pria inst�ncia
            return this;
        }

        private Usuario CriarSenha()
        {
            if (!DataAtivacao.HasValue && !string.IsNullOrWhiteSpace(Username))
            {
                SenhaDescriptografada = SecurityHelper.CreateNumericPassword();

                Senha = SecurityHelper.EncryptToSHA1(SenhaDescriptografada);
            }

            return this;
        }

        private Usuario SalvarFoto()
        {
            var oldUrl = UrlFoto;
            string retornoPath = null;
            FotoAlterada = false;

            try
            {
                if (Foto != null)
                {
                    using (var fileHelper = new FileHelper(SupportedFileFormat.Png | SupportedFileFormat.Jpeg,
                                                           Convert.FromBase64String(Foto),
                                                           ComputerUnit.Mb.ToLong()))
                    {
                        // Aten��o: Ser� responsabilidade da App gerenciar a imagem
                        //fileHelper.ResizeImage()
                        //          .ChangeResolution()
                        //          .Validate();

                        var extension = string.Format("*.{0}", fileHelper.Extension);

                        var retorno = FileSystem.SaveFile(Common.Tipos.Aplicacao.Corona, extension, fileHelper.File);

                        UrlFoto = retornoPath = retorno.Path;
                    }

                    if (!string.IsNullOrWhiteSpace(oldUrl))
                        FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, oldUrl);

                    FotoAlterada = true;
                }
            }
            catch (Exception exception)
            {
                if (!string.IsNullOrWhiteSpace(retornoPath))
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, retornoPath);

                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }

            return this;
        }

        private Usuario AdicionarUnidadesHabitacionais(ICollection<DTOPessoaUnidadeHabitacional> unidadeHabitacionals)
        {
            if (unidadeHabitacionals == null || unidadeHabitacionals.Count == 0)
                return this;

            UnidadesAdicionadas = new List<int>();

            foreach (var unidadeHabitacional in unidadeHabitacionals)
            {
                var pessoaUnidadeHabitacional =
                    UnidadesHabitacional.FirstOrDefault(
                        t =>
                        t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == unidadeHabitacional.CodigoUnidadeHabitacional ||
                         t.Codigo == unidadeHabitacional.Codigo));

                if (pessoaUnidadeHabitacional == null)
                {
                    pessoaUnidadeHabitacional = new PessoaUnidadeHabitacional
                                                    {
                                                        UnidadeHabitacional =
                                                            new UnidadeHabitacional().Get(
                                                                unidadeHabitacional.CodigoUnidadeHabitacional),
                                                        Pessoa = this
                                                    };

                    UnidadesHabitacional.Add(pessoaUnidadeHabitacional);

                    UnidadesAdicionadas.Add(unidadeHabitacional.CodigoUnidadeHabitacional);
                }

                pessoaUnidadeHabitacional.Administrador = unidadeHabitacional.Administrador;
                pessoaUnidadeHabitacional.Categoria = unidadeHabitacional.Categoria;
                pessoaUnidadeHabitacional.PermissaoAvisos = unidadeHabitacional.PermissaoAvisos;
                pessoaUnidadeHabitacional.PermissaoChegadaSegura = unidadeHabitacional.PermissaoChegadaSegura;
                pessoaUnidadeHabitacional.PermissaoPessoas = unidadeHabitacional.PermissaoPessoas;
                pessoaUnidadeHabitacional.PermissaoReserva = unidadeHabitacional.PermissaoReserva;
                pessoaUnidadeHabitacional.PermissaoVeiculos = unidadeHabitacional.PermissaoVeiculos;
                pessoaUnidadeHabitacional.PermissaoClassificados = unidadeHabitacional.PermissaoClassificados;
                pessoaUnidadeHabitacional.PermissaoNotificacaoPortaria = unidadeHabitacional.PermissaoNotificacaoPortaria;
                pessoaUnidadeHabitacional.RelacaoFuncao = unidadeHabitacional.RelacaoFuncao;
                pessoaUnidadeHabitacional.Responsavel = unidadeHabitacional.Responsavel;
            }

            return this;
        }

        private Usuario AdicionarCondominios(ICollection<DTOPessoaCondominio> pessoaCondominios)
        {
            if (pessoaCondominios == null || pessoaCondominios.Count == 0)
                return this;

            CondominiosAdicionados = new List<int>();

            foreach (var condominio in pessoaCondominios)
            {
                var pessoaCondominio =
                    PessoaCondominios.FirstOrDefault(
                        t =>
                        t.Condominio != null &&
                        (t.Condominio.Codigo == condominio.CodigoCondominio || t.Codigo == condominio.Codigo));

                if (pessoaCondominio == null)
                {
                    pessoaCondominio = new PessoaCondominio
                                           {
                                               Condominio = new Condominio().Get(condominio.CodigoCondominio),
                                               Pessoa = this
                                           };

                    PessoaCondominios.Add(pessoaCondominio);

                    CondominiosAdicionados.Add(condominio.CodigoCondominio);
                }

                pessoaCondominio.Categoria = condominio.Categoria;
                pessoaCondominio.Funcao = condominio.Funcao;
            }

            return this;
        }

        private Usuario AdicionarAcessos(ICollection<DTOPessoaAcesso> acessos)
        {
            if (acessos == null || acessos.Count == 0)
                return this;

            // Percorre os itens de acesso
            foreach (var acesso in acessos)
            {
                // Localiza o item a ser inclu�do/editado
                var pessoaAcesso =
                    Acessos.FirstOrDefault(
                        t =>
                        t.UnidadeHabitacional != null &&
                        (t.UnidadeHabitacional.Codigo == acesso.CodigoUnidadeHabitacional || t.Codigo == acesso.Codigo));

                // Inclui novo acesso, se necess�rio
                if (pessoaAcesso == null)
                {
                    pessoaAcesso = new AcessoPessoa
                    {
                        UnidadeHabitacional = new UnidadeHabitacional().Get(acesso.CodigoUnidadeHabitacional),
                        Pessoa = this
                    };
                    Acessos.Add(pessoaAcesso);
                }

                // Atualiza os dados do Acesso
                pessoaAcesso.Domingo = acesso.Domingo;
                pessoaAcesso.Segunda = acesso.Segunda;
                pessoaAcesso.Terca = acesso.Terca;
                pessoaAcesso.Quarta = acesso.Quarta;
                pessoaAcesso.Quinta = acesso.Quinta;
                pessoaAcesso.Sexta = acesso.Sexta;
                pessoaAcesso.Sabado = acesso.Sabado;
            }

            // Retorna o pr�prio objeto
            return this;
        }

        private Usuario SalvarUsuario()
        {
            try
            {
                if (Aplicacao == null)
                    Aplicacao = Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString());

                UsuarioNovo = Codigo == 0;

                if (!string.IsNullOrWhiteSpace(NovaSenha))
                    Senha = SecurityHelper.EncryptToSHA1(NovaSenha);

                base.Save(true);
            }
            catch (BaseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                if (FotoAlterada)
                    FileSystem.DeleteFile(Common.Tipos.Aplicacao.Corona, UrlFoto);

                throw new FalhaAoSalvarException("Pessoa", Nome, exception);
            }

            return this;
        }

        private Usuario EnviarConviteUnidadeHabitacional()
        {
            if (string.IsNullOrWhiteSpace(Username))
                return this;

            if (UnidadesAdicionadas != null && UnidadesAdicionadas.Count > 0)
            {
                foreach (var codigoUnidadeHabitacional in UnidadesAdicionadas)
                {
                    var unidadeHabitacional =
                        UnidadesHabitacional.FirstOrDefault(
                            t => t.UnidadeHabitacional.Codigo == codigoUnidadeHabitacional);

                    if (unidadeHabitacional != null)
                        unidadeHabitacional.EnviarConvite();
                }
            }

            return this;
        }

        private Usuario EnviarConviteCondominio()
        {
            if (string.IsNullOrWhiteSpace(Username))
                return this;

            if (CondominiosAdicionados != null && CondominiosAdicionados.Count > 0)
            {
                foreach (var codigoCondominio in CondominiosAdicionados)
                {
                    var condominio =
                        PessoaCondominios.FirstOrDefault(
                            t => t.Condominio.Codigo == codigoCondominio);

                    if (condominio != null)
                        condominio.EnviarConvite();
                }
            }

            return this;
        }

        private void EnviarEmailAtivacao()
        {
            if (string.IsNullOrWhiteSpace(Username))
                return;

            if (IsUsuarioAtivado() && 
                (CondominiosAdicionados == null || CondominiosAdicionados.Count == 0) && (UnidadesAdicionadas == null || UnidadesAdicionadas.Count == 0))
            {
                var chaveConvite = SecurityHelper.CreateCryptographyKey(Username, Nome,
                                                                           DateTime.Now.ToString("o"),
                                                                           ((int)Common.Tipos.Aplicacao.Corona)
                                                                               .ToString(
                                                                                   CultureInfo.InvariantCulture),
                                                                           Aplicacao.Versao);

                var linkConvite = Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.LinkConfirmacao);
                var assuntoConvite =
                    Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.AssuntoEmailConvite);
                var templateConvite =
                    Parametro.BuscarPorChave(Constantes.Corona.Usuario.Comunicacao.TemplateEmailConvite);

                EmailHelper.EnviarEmail(GetEmailSettings(Username), assuntoConvite,
                                        string.Format(templateConvite, SenhaDescriptografada, linkConvite,
                                                      chaveConvite));
            }
        }

        private bool IsUsuarioAtivado()
        {
            return this.UsuarioNovo || !this.DataAtivacao.HasValue;
        }


        #endregion [ Fluent ]

        #region RegisterDevice

        private DTORegisterDevice RegisterDeviceData { get; set; }

        public virtual Usuario RegisterDevice(DTORegisterDevice registerDevice)
        {
            RegisterDeviceData = registerDevice;
            Device device;

            return AuthorizeRequestByUser(registerDevice.ChaveCriptografada, registerDevice.Condominio)
                .LoadDevice(out device)
                .SetNotificationSettings(device)
                .SaveDevice(device);
        }

        public virtual Usuario UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            // Valida o acesso
            AuthorizeRequestByUser(unregisterDevice.ChaveCriptografada, unregisterDevice.Condominio);

            // seleciona o Device
            Device device = new Device().Repository.Get(unregisterDevice.DeviceToken);
            if (device == null)
                throw new ItemNotFoundException(unregisterDevice.DeviceToken, "Device");

            // Remove o Device
            device.Status = DeviceStatus.Inactive;
            device.Save();

            // Retona o resultado
            return this;
        }

        private Usuario SaveDevice(Device device)
        {
            Devices.Add(device);
            base.Save();
            return this;
        }

        private Usuario LoadDevice(out Device device)
        {
            device = new Device().Repository.Get(RegisterDeviceData.DeviceToken) ?? new Device
            {
                Name = RegisterDeviceData.Name,
                Token = RegisterDeviceData.DeviceToken,
                PlatformType = RegisterDeviceData.PlatformType
            };

            var condominio = new Condominio().Get(RegisterDeviceData.Condominio);
            device.Condominio = condominio;
            device.Usuario = this;
            device.Status = DeviceStatus.Active;

            return this;
        }

        private Usuario SetNotificationSettings(Device device)
        {
            var notificationSettings =
                Aplicacao.Buscar(Common.Tipos.Aplicacao.Corona.ConvertToString()).NotificationSettings ??
                new List<NotificationSettings>();

            var setting = notificationSettings.FirstOrDefault(t => t.Bundle.Equals(RegisterDeviceData.Bundle));

            if (setting == null)
                throw new ConfigurationException("NotificationSettings");

            device.NotificationSettings = setting;

            return this;
        }

        private Usuario AuthorizeRequestByUser(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            bool hasPermission;

            // Usu�rio de seguran�a pertence ao Condom�nio
            var usuario = new Usuario().Get(security.CodigoUsuario);
            hasPermission = usuario.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();
            
            // Usu�rio do Device pertence ao Condom�nio
            hasPermission = this.UnidadesHabitacional.Any(e => e.UnidadeHabitacional.Edificio.Condominio.Codigo == codigoCondominio);
            if (!hasPermission)
                throw new UnauthorizedAccessException();

            return this;
        }

        #endregion
    }
}