using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Common;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using UnauthorizedAccessException = Lalubema.Utilitarios.Exceptions.Security.UnauthorizedAccessException;

namespace Lalubema.Orion.Domain.Corona
{
    public class UnidadeHabitacional : DomainBase<UnidadeHabitacional, IUnidadeHabitacionalRepositorio<UnidadeHabitacional>>, IDomainModel
    {
        #region Extensibility Method Definitions

        private void OnCreated()
        {
            Acessos = new List<AcessoPessoa>();
            Autorizacoes = new List<AutorizacaoEntradaEntrega>();
            Moradores = new List<PessoaUnidadeHabitacional>();
            ReservasLocalBem = new List<ReservaLocalBem>();
            Veiculos = new List<Veiculo>();
        }

        #endregion

        public UnidadeHabitacional()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual string Numero { get; set; }

        public virtual string Obs { get; set; }

        public virtual int TipoUnidade { get; set; }

        public virtual IList<AcessoPessoa> Acessos { get; set; }

        public virtual IList<AutorizacaoEntradaEntrega> Autorizacoes { get; set; }

        public virtual IList<PessoaUnidadeHabitacional> Moradores { get; set; }

        public virtual IList<ReservaLocalBem> ReservasLocalBem { get; set; }

        public virtual Edificio Edificio { get; set; }

        public virtual IList<Veiculo> Veiculos { get; set; }

        public virtual string NomeResponsavel { get; set; }

        public virtual string EmailResponsavel { get; set; }

        public virtual bool EnviarConvite { get; set; }

        public override void Save()
        {
            base.Save();

            if (string.IsNullOrWhiteSpace(EmailResponsavel))
                return;

            var responsavel = Usuario.GetRepository().Get(EmailResponsavel) ?? new Usuario
                                                                                   {
                                                                                       Nome = NomeResponsavel,
                                                                                       Username = EmailResponsavel,
                                                                                       TipoUsuario =
                                                                                           TipoUsuario.Administrator,
                                                                                       Sexo = Sexo.Outro
                                                                                   };
            var pessoaUnidadeHabitacional = new DTOPessoaUnidadeHabitacional
                                                {
                                                    Administrador = 1,
                                                    Responsavel = 1,
                                                    PermissaoAvisos = 2,
                                                    PermissaoChegadaSegura = 2,
                                                    PermissaoPessoas = 2,
                                                    PermissaoReserva = 2,
                                                    PermissaoVeiculos = 2,
                                                    PermissaoClassificados = 0,
                                                    PermissaoNotificacaoPortaria = 0,
                                                    Categoria = "M",
                                                    CodigoUnidadeHabitacional = Codigo
                                                };

            responsavel.Save(new List<DTOPessoaUnidadeHabitacional> { pessoaUnidadeHabitacional }, null, null);
        }

        public virtual string Ativar(string userName, DateTime dataCadastro, Common.Tipos.Aplicacao aplicacao)
        {
            var morador = Moradores.FirstOrDefault(
                m => !string.IsNullOrWhiteSpace(m.Pessoa.Username) &&
                m.Pessoa.Username.Equals(userName, StringComparison.InvariantCultureIgnoreCase));

            if (morador == null)
                throw new ItemNotFoundException(userName, "Usuario");

            if (morador.DataAtivacao.HasValue && morador.Pessoa.DataAtivacao.HasValue)
                throw new UsuarioJaAtivadoException(userName);

            morador.DataAtivacao = DateTime.UtcNow;

            if (!morador.Pessoa.DataAtivacao.HasValue)
                morador.Pessoa.DataAtivacao = morador.DataAtivacao;

            return Parametro.BuscarPorChave(Constantes.Corona.UnidadeHabitacional.TemplateAtivacao) ?? "Usu&aacute; ativado com sucesso.";
        }

        public virtual IList<UnidadeHabitacional> ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            var unidadesHabitacionais =
                AuthorizeRequestPorCondominio(chaveCriptografada, codigoCondominio)
                    .Repository.ListarUnidadeHabitacionalPorCondominio(codigoCondominio);

            return unidadesHabitacionais.OrderBy(t => t.Edificio.Codigo).ThenBy(t => t.Numero.PadLeft(6, '0')).ToList();
        }

        private UnidadeHabitacional AuthorizeRequestPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var isFuncionario = new Condominio().Get(codigoCondominio).Funcionarios.Any(t => t.Pessoa.Codigo == security.CodigoUsuario);

            if (!isFuncionario)
                throw new UnauthorizedAccessException();

            return this;
        }
    }
}