using System;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Orion.InfraEstrutura;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;
using Aplicacao = Lalubema.Orion.Common.Tipos.Aplicacao;

namespace Lalubema.Orion.Domain.Corona
{
    public class Classificado : DomainBase<Classificado, IClassificadoRepositorio<Classificado>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime? DataPublicacao { get; set; }

        public virtual DateTime? DataExpiracao { get; set; }

        public virtual string Item { get; set; }

        public virtual string Foto { get; set; }

        public virtual string FotoBase64 { get; set; }

        public virtual decimal Preco { get; set; }

        public virtual string Texto { get; set; }

        public virtual string Status { get; set; }

        public virtual string Telefone { get; set; }

        public virtual Usuario Pessoa { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }

        public virtual Condominio Condominio { get; set; }

        public override void Save()
        {
            var oldFoto = Foto;

            try
            {
                if (!string.IsNullOrWhiteSpace(FotoBase64))
                {
                    var savedFile = FileSystem.SaveFile(Aplicacao.Corona, "foto.jpg",
                                                        Convert.FromBase64String(FotoBase64));

                    DataPublicacao = DateTime.UtcNow;
                    DataExpiracao = DateTime.UtcNow.AddDays(30);

                    Foto = savedFile.Path;
                }

                base.Save();
            }
            catch (Exception exception)
            {
                throw new FalhaAoSalvarException("Classificado", Item, exception);
            }

            if (!string.IsNullOrWhiteSpace(oldFoto))
                FileSystem.DeleteFile(Aplicacao.Corona, oldFoto);
        }
    }
}