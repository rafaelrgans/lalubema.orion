﻿using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Domain;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Domain.Corona
{
    public class Device : DomainBase<Device, IDeviceRepository<Device>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Name { get; set; }

        public virtual DeviceStatus Status { get; set; }

        public virtual string Token { get; set; }

        public virtual PlatformType PlatformType { get; set; }

        public virtual NotificationSettings NotificationSettings { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Condominio Condominio { get; set; }

        public virtual Device Get(string deviceToken)
        {
            var device = Repository.Get(deviceToken);

            if (device == null)
                throw new ItemNotFoundException(deviceToken, "Device");

            return device;
        }

        public virtual void ChangeToken(string newTokenDevice)
        {
            Token = newTokenDevice;

            base.Save();
        }
    }
}