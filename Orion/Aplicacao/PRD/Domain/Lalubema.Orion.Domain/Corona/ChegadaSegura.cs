using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class ChegadaSegura : DomainBase<ChegadaSegura, IChegadaSeguraRepositorio<ChegadaSegura>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual int TempoChegada { get; set; }

        public virtual DateTime DataHoraInclusao { get; set; }

        public virtual string StatusPortaria { get; set; }

        public virtual DateTime? DataHoraStatus { get; set; }

        public virtual string Mensagem { get; set; }

        public virtual Usuario Solicitante { get; set; }

        public virtual Usuario Porteiro { get; set; }

        public virtual Veiculo Veiculo { get; set; }
    }
}