using System;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class DisponibilidadeLocalBem : DomainBase<DisponibilidadeLocalBem, IDisponibilidadeLocalBemRepositorio<DisponibilidadeLocalBem>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string DiaSemana { get; set; }

        public virtual TimeSpan HoraInicio { get; set; }

        public virtual TimeSpan HoraFim { get; set; }

        public virtual LocalBem LocalBem { get; set; }
    }
}