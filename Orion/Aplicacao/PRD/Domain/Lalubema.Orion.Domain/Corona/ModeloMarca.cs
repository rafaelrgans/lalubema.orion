using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class ModeloMarca : DomainBase<ModeloMarca, IModeloMarcaRepositorio<ModeloMarca>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Modelo { get; set; }

        public virtual MarcaVeiculo MarcaVeiculo { get; set; }
    }
}