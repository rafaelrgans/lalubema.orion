using System;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class AutorizacaoEntradaEntrega : DomainBase<AutorizacaoEntradaEntrega, IAutorizacaoEntradaEntregaRepositorio<AutorizacaoEntradaEntrega>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual DateTime DataHoraInicio { get; set; }

        public virtual DateTime DataHoraFim { get; set; }

        public virtual TipoAutorizacao Tipo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual Usuario Pessoa { get; set; }

        public virtual UnidadeHabitacional UnidadeHabitacional { get; set; }
    }
}