using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Domain;

namespace Lalubema.Orion.Domain.Corona
{
    public class MarcaVeiculo : DomainBase<MarcaVeiculo, IMarcaVeiculoRepositorio<MarcaVeiculo>>, IDomainModel
    {
        private IList<ModeloMarca> _modelosMarca;

        public MarcaVeiculo()
        {
            OnCreated();
        }

        public virtual int Codigo { get; set; }

        public virtual TipoVeiculo Tipo { get; set; }

        public virtual string Marca { get; set; }

        public virtual int Agrupador { get; set; }

        public virtual IList<ModeloMarca> ModelosMarca
        {
            get { return _modelosMarca; }
            set { _modelosMarca = value; }
        }

        private void OnCreated()
        {
            _modelosMarca = new List<ModeloMarca>();
        }
    }
}