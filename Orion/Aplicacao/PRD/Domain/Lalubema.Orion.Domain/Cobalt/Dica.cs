﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Integration.Spec.Cobalt;
using Lalubema.Utilitarios.Domain;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Orion.Common.Exceptions.Orion;

namespace Lalubema.Orion.Domain.Cobalt
{
    public class Dica : DomainBase<Dica, IDicaRepositorio<Dica>>, IDomainModel
    {
        public virtual int Codigo { get; set; }

        public virtual string Titulo { get; set; }

        public virtual string Descricao { get; set; }

        public virtual bool Ativo { get; set; }

        public virtual DateTime Created { get; set; }

        public virtual DateTime LastUpdate { get; set; }

        public virtual Usuario CreatedBy { get; set; }

        public virtual IList<Dica> ListarDicasDisponiveis(DateTime? lastSync)
        {
            return lastSync.HasValue ? Repository.ListAll(lastSync.Value, true) : Repository.ListAll(true);
        }

		public virtual IList<Dica> ListarDicas()
		{
			return Repository.ListAll(false);
		}

		public virtual IList<Dica> ListarDica(DTODicaEntrada dica)
		{
			return Repository.ListAll(dica.Codigo);
		}


        public virtual void Save(int codUsuario)
        {
            CreatedBy = new Usuario().Get(codUsuario);
            Save();
        }

        public override void Save()
        {
            if (Codigo == 0)
                Ativo = true;

            base.Save();
        }

        public override void Delete()
        {
            Ativo = false;
            base.Save();
        }
    }
}