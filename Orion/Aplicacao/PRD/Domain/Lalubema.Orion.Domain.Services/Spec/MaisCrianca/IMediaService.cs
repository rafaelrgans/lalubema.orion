﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.MaisCrianca
{
    [ObjectMap("MaisCriancaMediaDomainService", false)]
    public interface IMediaService
    {
        IList<DTOMediaRetorno> ListMediaFeed(DTOPagingFilter pagingFilter);
        
        DTOMediaRetorno Save(DTOMediaEntrada media);
        
        DTORetorno Delete(DTOMediaEntrada media);
    }
}