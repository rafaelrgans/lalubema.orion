﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Titanium;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Titanium
{
    [ObjectMap("TitaniumAdministracaoDomainService", false)]
    public interface IAdministracaoService
    {
        DTORetorno CadastrarTorneio(DTOCadastroTorneio torneio);

        DTOTorneioRetorno BuscarTorneio(string chaveCriptografada, int codigoTorneio);

        DTOTorneioPorStatus BuscarTorneios(DTOTorneioEntrada torneioEntrada);
    }
}