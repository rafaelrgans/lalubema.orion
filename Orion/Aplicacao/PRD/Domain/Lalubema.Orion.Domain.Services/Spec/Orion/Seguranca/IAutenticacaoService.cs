﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca
{
    [ObjectMap("AutenticacaoDomainService", false)]
    public interface IAutenticacaoService
    {
        DTOAutenticacao Autenticar(DTOTicket ticket);

        DTOAutenticacao Autenticar(string chaveCriptografada);

        DTOAutenticacao AutenticarPorSistema(string usuario, string senha);
    }
}