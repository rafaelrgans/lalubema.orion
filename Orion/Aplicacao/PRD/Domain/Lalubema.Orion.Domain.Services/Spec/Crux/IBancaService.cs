﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject.Crux;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Crux
{
    [ObjectMap("CruxBancaDomainService", false)]
    public interface IBancaService
    {
        DTOBanca ConsultarBanca(string chaveCriptografada, string cultureName, int codigoBanca);

        DTOBancas ConsultarBanca(string chaveCriptografada, string cultureName, string nomeBanca);
    }
}