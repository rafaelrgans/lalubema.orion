﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lalubema.Utilitarios.Spring;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaUsuarioDomainService", false)]
    public interface IUsuarioService
    {
        DTORetorno RegisterDevice(DTORegisterDevice registerDevice);

        DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice);
    }
}