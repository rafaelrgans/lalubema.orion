﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaMessageDomainService", false)]
    public interface IMessageService
    {
        DTOMuralRetorno ListarMuralMensagem(string chaveCriptografada, int unidadeHabitacional);

        DTOMensagensRetorno ListarMensagens(string chaveCriptografada, int unidadeHabitacional);

        DTOCondominioRetorno ListarDadosCondominio(string chaveCriptografada, int condominio);

        DTORetorno Send(DTOSendMessage sendMessage);
    }
}