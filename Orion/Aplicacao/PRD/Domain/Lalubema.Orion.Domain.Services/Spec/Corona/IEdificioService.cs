﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaEdificioDomainService", false)]
    public interface IEdificioService
    {
        DTORetorno AssociarEdificioLocalBem(DTOAssociarEdificioLocalBemEntrada edificioLocalBem);
    }
}