﻿using System.IO;
using Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Corona
{
    [ObjectMap("CoronaUnidadeHabitacionalDomainService", false)]
    public interface IUnidadeHabitacionalService
    {
        Stream CheckEmail(string chaveConfirmacao);

        DTOUnidadesHabitacionaisRetorno ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada, int codigoCondominio);
    }
}