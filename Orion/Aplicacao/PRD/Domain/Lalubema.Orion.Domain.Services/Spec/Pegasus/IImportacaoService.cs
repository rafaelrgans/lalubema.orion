﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Pegasus
{
    [ObjectMap("PegasusImportacaoDomainService", false)]
    public interface IImportacaoService
    {
        DTORetorno Exportar(DTOExportacao importacaoIndices);

        DTOImportacao Importar(string chaveCriptografada, string email, string senha);
    }
}