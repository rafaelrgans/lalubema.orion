﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Vanadium
{
	[ObjectMap("VanadiumRelatorioDomainService", false)]
    public interface IRelatorioService
	{
		DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio);
	}
}
