﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Vanadium
{
    [ObjectMap("VanadiumCadastroDomainService", false)]
    public interface ICadastroService
    {
        DTORetorno Salvar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        DTORetorno Excluir(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        DTORetorno Listar(string chaveCriptografada, DTOBase entidade, int codigoEstabelecimento);

        DTORetorno Buscar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento);

        T Buscar<T>(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento) where T : DTORetorno;
    }
}