﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Cobalt
{
    [ObjectMap("CobaltAccountDomainService", false)]
    public interface IAccountService
    {
        DTORetorno ReenviarSenha(DTOReenviarSenhaEntrada reenviarSenha);
    }
}