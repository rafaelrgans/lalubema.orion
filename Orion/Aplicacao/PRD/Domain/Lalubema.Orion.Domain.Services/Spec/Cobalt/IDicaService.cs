﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Cobalt
{
    [ObjectMap("CobaltDicaDomainService", false)]
    public interface IDicaService
    {
        DTORetorno SalvarDica(DTODicaEntrada dica);

        DTORetorno ExcluirDica(DTODicaEntrada dica);
		
		DTODicasRetorno ListarDicas(string chaveCriptografada);

		DTODicasRetorno ListarDica(DTODicaEntrada dica);

        DTODicasRetorno ListarDicasDisponiveis(string chaveCriptografada, DateTime? lastSync);
    }
}