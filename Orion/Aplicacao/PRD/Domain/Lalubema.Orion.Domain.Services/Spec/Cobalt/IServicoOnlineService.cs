﻿using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Domain.Services.Spec.Cobalt
{
    [ObjectMap("CobaltServicoOnlineDomainService", false)]
    public interface IServicoOnlineService
    {
        DTORetorno SolicitarServicoOnlineNotarial(DTOServicoOnlineNotarialEntrada solicitacaoServicoNotarial);
    }
}
