﻿using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Cobalt;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Attributes;

namespace Lalubema.Orion.Domain.Services.Impl.Cobalt
{
    public class AccountService : ServiceBase, IAccountService
    {
        [ValidateCryptographyKey(false, (int) Aplicacao.Cobalt)]
        public DTORetorno ReenviarSenha(DTOReenviarSenhaEntrada reenviarSenha)
        {
            ValidadorCobaltReenviarSenha.Validate(reenviarSenha, NotificationCollection);

            if (NotificationCollection.HasError())
                throw new DadosInvalidosException(NotificationCollection.ToString());

            new Usuario()
                .ReenviarSenha(reenviarSenha);

            return new DTORetorno();
        }
    }
}