﻿using System;
using System.Net;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Cobalt;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Orion.Domain.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Lalubema.Orion.Common.Seguranca;


namespace Lalubema.Orion.Domain.Services.Impl.Cobalt
{
    public class DicaService : ServiceBase, IDicaService
    {
        [ValidateCryptographyKey(true, (int) Aplicacao.Cobalt)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = false)]
        public DTORetorno SalvarDica(DTODicaEntrada dica)
        {
			Dica dicaModel;
			var httpStatusCode = HttpStatusCode.OK;

			dicaModel = dica.Transform<Dica>(true);
			httpStatusCode = HttpStatusCode.Created;

			var chaveCriptografadaHelper = new ChaveCriptografadaHelper(dica.ChaveCriptografada);
			dicaModel.Save(chaveCriptografadaHelper.CodigoUsuario);

			return new DTORetorno(httpStatusCode);
        }

		[ValidateCryptographyKey(true, (int)Aplicacao.Cobalt)]
		[Transaction(TransactionPropagation.Required, ReadOnly = false)]
		public DTORetorno ExcluirDica(DTODicaEntrada dica)
		{
			var dicaModel = Dica.GetRepository().Get(dica.Codigo);

			dicaModel.Delete();

			return new DTORetorno();
		}

        [ValidateCryptographyKey(true, (int)Aplicacao.Cobalt)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = false)]
        public DTODicasRetorno ListarDicas(string chaveCriptografada)
        {
			var dicas = new Dica().ListarDicas();

            return new DTODicasRetorno
                {
                    Dicas = dicas.TransformList<DTODica>(true, null)
                };
        }

		[ValidateCryptographyKey(true, (int)Aplicacao.Cobalt)]
		[Transaction(TransactionPropagation.Supports, ReadOnly = false)]
		public DTODicasRetorno ListarDica(DTODicaEntrada dica)
		{
			var dicas = new Dica().ListarDica(dica);

			return new DTODicasRetorno
			{
				Dicas = dicas.TransformList<DTODica>(true, null)
			};
		}

        [ValidateCryptographyKey(false, (int) Aplicacao.Cobalt)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = false)]
        public DTODicasRetorno ListarDicasDisponiveis(string chaveCriptografada, DateTime? lastSync)
        {
            var dicas = new Dica().ListarDicasDisponiveis(lastSync);

            return new DTODicasRetorno
                {
                    Dicas = dicas.TransformList<DTODica>(true, null)
                };
        }
    }
}