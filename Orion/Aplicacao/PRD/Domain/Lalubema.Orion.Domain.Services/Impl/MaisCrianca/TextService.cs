﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text;
using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Domain.Services.Spec.MaisCrianca;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.MaisCrianca
{
    public class TextService : ServiceBase, ITextService
    {
        [ValidateCryptographyKey(false, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTextRetorno> ListTextFeed(DTOPagingFilter pagingFilter)
        {
            return Text.ListTextFeed(pagingFilter).TransformList<DTOTextRetorno>();
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTextRetorno> ListTextTopFeed(DTOCategoryEntrada category)
        {
            return Text.ListTextTopFeed(category).TransformList<DTOTextRetorno>();
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno SendText(DTOTextEntrada text)
        {
            var textModel = text.Transform<Text>(true);

            textModel.Save(text.Contents);

            return new DTORetorno();
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Like(DTOTextEntrada text)
        {
            var textModel = Text.GetRepository().Get(text.Codigo);

            textModel.Like();

            return new DTORetorno();
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTOCommentRetorno Comment(DTOCommentEntrada comment)
        {
            var textModel = Text.GetRepository().Get(comment.TextCode);

            var savedComment = textModel.Comment(comment);

            return savedComment;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno ChangeStatus(DTOTextEntrada text)
        {
            var textModel = Text.GetRepository().Get(text.Codigo);

            textModel.ChangeStatus(text);

            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Delete(DTOTextEntrada text)
        {
            var textModel = Text.GetRepository().Get(text.Codigo);

            textModel.Delete();

            return new DTORetorno();
        }
    }
}