﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media;
using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Domain.Services.Spec.MaisCrianca;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.MaisCrianca
{
    public class MediaService : ServiceBase, IMediaService
    {
        [ValidateCryptographyKey(false, (int) Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        public IList<DTOMediaRetorno> ListMediaFeed(DTOPagingFilter pagingFilter)
        {
            return Media.ListMediaFeed(pagingFilter).TransformList<DTOMediaRetorno>();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        public DTOMediaRetorno Save(DTOMediaEntrada media)
        {
            var mediaModel = media.Transform<Media>();

            mediaModel.Save();

            return mediaModel.Transform<DTOMediaRetorno>();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.MaisCrianca)]
        [Transaction(TransactionPropagation.Supports, ReadOnly = true)]
        public DTORetorno Delete(DTOMediaEntrada media)
        {
            var mediaModel = media.Transform<Media>();

            mediaModel.Delete();

            return new DTORetorno();
        }
    }
}