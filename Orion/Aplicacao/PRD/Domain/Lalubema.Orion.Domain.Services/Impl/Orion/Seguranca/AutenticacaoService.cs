﻿using System;
using System.Globalization;
using System.Reflection;
using Lalubema.Orion.Common.Exceptions.Orion.Seguranca;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Orion.Seguranca
{
    public class AutenticacaoService : ServiceBase, IAutenticacaoService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAutenticacao Autenticar(string chaveCriptografada)
        {
            try
            {
                var security = new ChaveCriptografadaHelper(chaveCriptografada);

                var usuarioClassName = GetUsuarioClassName(security.CodigoAplicacao);

                var usuario = Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

                ((IUsuario)usuario).Load(security.CodigoUsuario);

                var chaveCriptografadaUsuario = GetChaveCriptografada(security.CodigoAplicacao, usuario);

                var retornoAutenticacao = CreateInstanceRetornoAutenticacao(security.CodigoAplicacao);

                retornoAutenticacao = usuario.Transform(retornoAutenticacao);

                ((DTOAutenticacao)retornoAutenticacao).ChaveCriptografada = chaveCriptografadaUsuario;
                ((DTOAutenticacao)retornoAutenticacao).CodigoAplicacao = security.CodigoAplicacao;

                return (DTOAutenticacao)retornoAutenticacao;
            }
            catch (TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;

                throw;
            }
        }

        [Transaction(TransactionPropagation.Supports)]
        public DTOAutenticacao AutenticarPorSistema(string usuario, string senha)
        {
            var aplicacao = AplicacaoRepositorio.Buscar(usuario);

            if (aplicacao == null)
                throw new UsuarioOuSenhaInvalidosException();

            var chaveCriptografada = aplicacao.Autenticar(senha);

            var autenticacao = new DTOAutenticacao
                                   {
                                       ChaveCriptografada = chaveCriptografada,
                                       CodigoAplicacao = aplicacao.Codigo
                                   };

            return autenticacao;
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAutenticacao Autenticar(DTOTicket ticket)
        {
            const string inicialMetodo = "Autenticar";

            try
            {
                var retorno =
                    GetType().InvokeMember(inicialMetodo + ticket.Metodo,
                                           BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Public |
                                           BindingFlags.Instance, null, this, new object[] { ticket.ChaveCriptografada, ticket })
                    as DTOAutenticacao;

                return retorno;
            }
            catch (TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;

                throw;
            }
        }

        protected DTOAutenticacao AutenticarPorUsuario(string chaveCriptografada, DTOTicket ticket)
        {
            try
            {
                var ticketUsuario = ticket as DTOTicketPorUsuario;

                var security = new ChaveCriptografadaHelper(chaveCriptografada);

                if (ticketUsuario == null)
                    throw new FalhaAutenticarUsuarioException();

                var usuarioClassName = GetUsuarioClassName(security.CodigoAplicacao);

                var usuario = Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

                ((IUsuario)usuario).Validar(ticketUsuario.Usuario, ticketUsuario.Senha, ticketUsuario.Versao);

                var chaveCriptografadaUsuario = GetChaveCriptografada(security.CodigoAplicacao, usuario);

                var retornoAutenticacao = CreateInstanceRetornoAutenticacao(security.CodigoAplicacao);

                retornoAutenticacao = usuario.Transform(retornoAutenticacao);

                ((DTOAutenticacao) retornoAutenticacao).ChaveCriptografada = chaveCriptografadaUsuario;
                ((DTOAutenticacao) retornoAutenticacao).CodigoAplicacao = security.CodigoAplicacao;
                ((DTOAutenticacao) retornoAutenticacao).UserName = ((IUsuario) usuario).Username;
				
				//if(usuario is Lalubema.Orion.Domain.Vanadium.Usuario)
				//    ((DTOAutenticacao) retornoAutenticacao).TipoUsuario = ((Lalubema.Orion.Domain.Vanadium.Usuario)(usuario)).UserType;
				
                return (DTOAutenticacao) retornoAutenticacao;
            }
            catch(TargetInvocationException exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                
                throw;
            }
        }

        private static string GetChaveCriptografada(int codigoAplicacao, object usuario)
        {
            return SecurityHelper.CreateCryptographyKey(CryptographyType.User.ConvertToString(),
                                                        DateTime.UtcNow.ToString("o"),
                                                        ((IUsuario) usuario).Codigo.ToString(
                                                            CultureInfo.InvariantCulture),
                                                        ((IUsuario) usuario).Username,
                                                        ((IUsuario) usuario).Aplicacao.Versao,
                                                        codigoAplicacao.ToString(CultureInfo.InvariantCulture));
        }

        private static Object CreateInstanceRetornoAutenticacao(int codigoAplicacao)
        {
            var retornoClassName = string.Format("Lalubema.Orion.Domain.DataTransferObject.{0}.DTOAutenticacaoPorUsuario", ((Aplicacao)codigoAplicacao).ConvertToString());

            if (!CheckTypeExists(string.Format("{0}, Lalubema.Orion.Domain.DataTransferObject", retornoClassName)))
                retornoClassName = "Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca.DTOAutenticacao";

            return Activator.CreateInstance("Lalubema.Orion.Domain.DataTransferObject", retornoClassName).Unwrap();
        }

        private static string GetUsuarioClassName(int codigoAplicacao)
        {
            var usuarioClassName = string.Format("Lalubema.Orion.Domain.{0}.Usuario",
                                                 ((Aplicacao) codigoAplicacao).ConvertToString());

            if(!CheckTypeExists(string.Format("{0}, Lalubema.Orion.Domain", usuarioClassName)))
                throw new InvalidOperationException("Valid type not found.");

            return usuarioClassName;
        }

        private static bool CheckTypeExists(string className)
        {
            var tipo = Type.GetType(className);

            return tipo != null;
        }
    }
}