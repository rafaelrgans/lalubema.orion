﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Lalubema.Orion.Common.Exceptions.Orion;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions;
using Lalubema.Orion.Domain.Orion.Seguranca;
using Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Orion.Seguranca
{
    public class AccountService : ServiceBase, IAccountService
    {
        [ValidateCryptographyKey(false, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Register(DTOAccountRegister account)
        {
            ValidadorAccountRegister.Validate(account, NotificationCollection);

            if (NotificationCollection.HasError())
                throw new DadosInvalidosException(NotificationCollection.ToString());

            var security = new ChaveCriptografadaHelper(account.ChaveCriptografada);

            var usuarioClassName = GetUsuarioClassName(security.CodigoAplicacao);

            var usuario = (IUsuario)Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

            var usuarioDomain = account.Transform(usuario);

            usuarioDomain.Aplicacao = AplicacaoRepositorio.Get(security.CodigoAplicacao);

            var registerMethod = usuarioDomain.GetType().GetMethod("Register", new[] {account.GetType()});

            if (registerMethod == null)
                usuarioDomain.Save();
            else
                registerMethod.Invoke(usuarioDomain, new object[] {account});

            return new DTORetorno(HttpStatusCode.Created);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public Stream CheckEmail(string chaveConfirmacao)
        {
            string resposta = "<html><head><title>Ativar Conta</title></head><body>{0}</body></html>";

            try
            {
                var conta = SecurityHelper.Decrypt(chaveConfirmacao);

                var aplicacao = (Aplicacao) conta[3].ToInt32();

                var usuario =
                    (IUsuario)
                    Activator.CreateInstance("Lalubema.Orion.Domain", GetUsuarioClassName(aplicacao)).Unwrap();

				resposta = usuario.Ativar(conta[0], conta[2].ToDateTime(), aplicacao);
			}
            catch (DomainLayerException)
            {
                throw;
            }
            catch (TargetInvocationException)
            {
                throw;
            }
            catch(Exception)
            {
                resposta = string.Format(resposta, "<p>Ocorreu um erro ao ativar o usu&aacute;rio. Tente novamente mais tarde e caso o erro persista favor entrar em contato com o administrador do sistema.</p>");

                throw new Exception(resposta);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(resposta));
        }

        [ValidateCryptographyKey(false, (int)Aplicacao.Any)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno RecoveryPassword(DTORecoveryPassword recoveryPassword)
        {
            var security = new ChaveCriptografadaHelper(recoveryPassword.ChaveCriptografada);

            var usuarioClassName = GetUsuarioClassName((Aplicacao)security.CodigoAplicacao);

            var usuario = Activator.CreateInstance("Lalubema.Orion.Domain", usuarioClassName).Unwrap();

            ((IUsuario) usuario).RecoveryPassword(recoveryPassword.Username);
            
            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno DirectDebit(DTODirectDebitRequest directDebitRequest)
        {
            var security = new ChaveCriptografadaHelper(directDebitRequest.ChaveCriptografada);

            return TransactionManager.DirectDebit(security.CodigoUsuario, directDebitRequest);
        }

        private static string GetUsuarioClassName(Aplicacao aplicacao)
        {
            var usuarioClassName = string.Format("Lalubema.Orion.Domain.{0}.Usuario", (aplicacao).ConvertToString());

            if (!CheckTypeExists(string.Format("{0}, Lalubema.Orion.Domain", usuarioClassName)))
                throw new InvalidOperationException("Valid type not found.");

            return usuarioClassName;
        }

        private static string GetUsuarioClassName(int codigoAplicacao)
        {
            return GetUsuarioClassName((Aplicacao) codigoAplicacao);
        }

        private static bool CheckTypeExists(string className)
        {
            var tipo = Type.GetType(className);

            return tipo != null;
        }
    }
}