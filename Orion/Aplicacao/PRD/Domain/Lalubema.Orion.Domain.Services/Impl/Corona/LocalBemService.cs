﻿using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using DTOLocalBem = Lalubema.Orion.Domain.DataTransferObject.Corona.LocalBem.DTOLocalBem;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class LocalBemService : ServiceBase, ILocalBemService
    {
        [ValidateCryptographyKey(true, (int) Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOLocalBemsRetorno ListarLocalBem(string chaveCriptografada, int codigoEdificio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var returnedValue = new LocalBem().ListAll(security.CodigoUsuario, codigoEdificio);

            return new DTOLocalBemsRetorno {LocalBems = returnedValue.TransformList<DTOLocalBem>()};
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOReservasRetorno ListarReserva(string chaveCriptografada, int codigoLocalBem)
        {
            var localBem = new LocalBem().Get(codigoLocalBem);

            return new DTOReservasRetorno
                       {
                           Reservas =
                               localBem.GetProximasReservas().TransformList<DTOReserva>(true, null)
                       };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOReservaLocalBemsRetorno ListarReservaUsuario(string chaveCriptografada, int codigoCondominio)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            var returnedValue = new ReservaLocalBem().GetReservas(codigoCondominio, security.CodigoUsuario);

            return new DTOReservaLocalBemsRetorno
                       {
                           ReservaLocalBems =
                               returnedValue.TransformList<DTOReservaLocalBem>()
                       };
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Reservar(DTOReservaLocalBemEntrada reservaLocalBem)
        {
            var localBem = new LocalBem().Get(reservaLocalBem.CodigoLocalBem);

            localBem.Reservar(reservaLocalBem);

            return new DTORetorno();
        }
    }
}