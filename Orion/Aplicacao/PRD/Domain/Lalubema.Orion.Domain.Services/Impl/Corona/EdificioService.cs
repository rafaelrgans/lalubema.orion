﻿using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Edificio;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class EdificioService : ServiceBase, IEdificioService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno AssociarEdificioLocalBem(DTOAssociarEdificioLocalBemEntrada edificioLocalBem)
        {
            var edificio = new Edificio().Get(edificioLocalBem.Codigo);

            edificio.AssociarLocalBens(edificioLocalBem);

            return new DTORetorno();
        }
    }
}
