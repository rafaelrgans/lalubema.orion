﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject.Corona.UnidadeHabitacional;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class UnidadeHabitacionalService : ServiceBase, IUnidadeHabitacionalService
    {
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public Stream CheckEmail(string chaveConfirmacao)
        {
            string resposta = "<html><head><title>Aceitar Convite</title></head><body>{0}</body></html>";

            try
            {
                string[] conta = SecurityHelper.Decrypt(chaveConfirmacao);

                var aplicacao = (Aplicacao) conta[3].ToInt32();
                int codigoUnidade = conta[5].ToInt32();

                UnidadeHabitacional unidadeHabitacional = UnidadeHabitacional.GetRepository().Get(codigoUnidade);

                resposta = unidadeHabitacional.Ativar(conta[0], conta[2].ToDateTime(), aplicacao);
            }
            catch (DomainLayerException)
            {
                throw;
            }
            catch (TargetInvocationException)
            {
                throw;
            }
            catch (Exception)
            {
                resposta = string.Format(resposta,
                                         "<p>Ocorreu um erro ao ativar o usu&aacute;rio. Tente novamente mais tarde e caso o erro persista favor entrar em contato com o administrador do sistema.</p>");

                throw new Exception(resposta);
            }

            return new MemoryStream(Encoding.UTF8.GetBytes(resposta));
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOUnidadesHabitacionaisRetorno ListarUnidadeHabitacionalPorCondominio(string chaveCriptografada, int codigoCondominio)
        {
            var result = new DTOUnidadesHabitacionaisRetorno
                               {
                                   UnidadesHabitacionais =
                                       (new UnidadeHabitacional()).ListarUnidadeHabitacionalPorCondominio(chaveCriptografada,
                                                                                                          codigoCondominio)
                                                                  .TransformList<DTOUnidadeHabitacional>()
                               };

            // Os acessos dos Moradores são carregados para todas as Unidades existentes, logo abaixo mantém somente da Unidade Habitacional do contexto
            // Atenção: As classes do domínio (NHibernate) usam referência ao mesmo objeto em memória não permitindo filtrar a lista por Unidade Habitacional
            //          ex: uma Pessoa pode ser referenciada várias Unidades Habitacionais
            foreach (var unidadeHabitacional in result.UnidadesHabitacionais)
                foreach (var morador in unidadeHabitacional.Moradores)
                    morador.Acessos = morador.Acessos.Where(e => e.CodigoUnidadeHabitacional == unidadeHabitacional.Codigo).ToList();

            return result;
        }
    }
}