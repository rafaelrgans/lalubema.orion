﻿using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Push;
using Lalubema.Orion.Domain.Services.Spec.Corona;
using Lalubema.Utilitarios.Attributes;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Lalubema.Orion.Common.Exceptions.Orion;
using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Domain.Services.Impl.Corona
{
    public class PushService : ServiceBase, IPushService
    {
        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno SendPush(DTOSendPush sendPush)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(sendPush.ChaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Enviaar Notificação
            usuario.Notificar(sendPush);

            // Retorno Sucesso
            return new DTORetorno();
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOGetPush GetPush(string chaveCriptografada, int codigoCondominio)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Leitura das informações de Notificação (GetPush)
            var result = usuario.ListarNotificacao(codigoCondominio);
            return result;
        }

        [ValidateCryptographyKey(true, (int)Aplicacao.Corona)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOGetPush GetReplyPush(string chaveCriptografada)
        {
            // Informações de Segurança
            var security = new ChaveCriptografadaHelper(chaveCriptografada);
            var usuario = new Usuario().Get(security.CodigoUsuario);

            // Leitura das informações de Notificação (GetPush)
            var result = usuario.ListarNotificacaoResposta();
            return result;
        }

    }
}