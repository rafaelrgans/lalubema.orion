﻿using System;
using System.Linq;
using System.Net;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Relatorio;
using Lalubema.Orion.Domain.Services.Spec.Vanadium;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Spring.Transaction;
using Spring.Transaction.Interceptor;

namespace Lalubema.Orion.Domain.Services.Impl.Vanadium
{
	public class RelatorioService : ServiceBase, IRelatorioService
	{
		[ValidateCryptographyKey(true, (int)Aplicacao.Vanadium)]
		[Transaction(TransactionPropagation.Required, ReadOnly = true)]
		public DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio)
		{
			AuthorizeRequest(relatorio.ChaveCriptografada, relatorio.CodEstabelecimento);

			var relatorioModel = new Relatorio().GerarRelatorio(relatorio.ChaveCriptografada, relatorio.CodEstabelecimento, relatorio.DtInicio, relatorio.DtFim, relatorio.ReportType);

			if (relatorioModel == null)
				throw new ItemNotFoundException(string.Format("Estabelecimento: {0}", relatorio.CodEstabelecimento), "Pedido");

			return relatorioModel.Transform<DTORelatorioRetorno>(true);
		}

		private void AuthorizeRequest(string chaveCriptografada, int codigoEstabelecimento)
		{
			var security = new ChaveCriptografadaHelper(chaveCriptografada);

			var usuario = new Usuario().Get(security.CodigoUsuario);

			if (usuario.UserType == TipoUsuario.Cliente)
				return;

			var estabelecimento = new Estabelecimento().Get(codigoEstabelecimento);

			if (estabelecimento.Usuarios.All(t => t.Usuario.Codigo != security.CodigoUsuario))
				throw new UnauthorizedAccessException();
		}

	}
}
