﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Domain.Services.Spec.Vanadium;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Integration;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper;
using Lalubema.Utilitarios.Spring;
using Spring.Transaction;
using Spring.Transaction.Interceptor;
using Aplicacao = Lalubema.Orion.Domain.Orion.Aplicacao;

namespace Lalubema.Orion.Domain.Services.Impl.Vanadium
{
    public class CadastroService : ServiceBase, ICadastroService
    {
        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Salvar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            var repository = GetRepository(entidade);

            var key = GetPrimaryKey(entidade);

            var entidadeModel =
                (IDomainModel)
                repository.GetType().GetMethod("Get", new[] {typeof (int), typeof (int)}).Invoke(repository,
                                                                                                 new[]
                                                                                                     {
                                                                                                         key,
                                                                                                         codigoEstabelecimento
                                                                                                     });

            if (((int)key) > 0 && entidadeModel == null)
                throw new ItemNotFoundException(key, entidade.GetType().Name);

            var httpStatusCode = HttpStatusCode.OK;
            MethodInfo transformMethod;

            if(entidadeModel == null)
            {
                var domainModel = GetDomainModel(entidade);

                transformMethod = typeof (TransformExtension).GetMethod("Transform",
                                                                        BindingFlags.Default | BindingFlags.Public |
                                                                        BindingFlags.Static,
                                                                        null, new[] {typeof (object)}, null).
                    MakeGenericMethod(domainModel.GetType());

                entidadeModel = (IDomainModel) transformMethod.Invoke(entidade, new object[] { entidade });

                httpStatusCode = HttpStatusCode.Created;
            }
            else
            {
                transformMethod = typeof (TransformExtension).GetMethod("Transform",
                                                                        BindingFlags.Default | BindingFlags.Public |
                                                                        BindingFlags.Static,
                                                                        null, new[] { typeof(object), entidadeModel.GetType() },
                                                                        null);

                entidadeModel = (IDomainModel) transformMethod.Invoke(entidade, new object[] {entidade, entidadeModel});
            }

            SetEstabelecimento(codigoEstabelecimento, entidadeModel);

            SetAplicacao(chaveCriptografada, entidadeModel);

            var types = FlagsHelper.GetTypes<FilterAttribute>(entidade, FilterTypeFlags.Save);

            if (types.Count != 0)
            {
                var values = FlagsHelper.GetValues<FilterAttribute>(entidade, FilterTypeFlags.Save);

                entidadeModel.GetType().GetMethod("Save", types.ToArray()).Invoke(entidadeModel, values.ToArray());
            }
            else
                entidadeModel.Save();

            return new DTORetorno(httpStatusCode);
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = false)]
        public DTORetorno Excluir(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            var repository = GetRepository(entidade);

            var key = GetPrimaryKey(entidade);

            var entidadeModel =
                (IDomainModel)
                repository.GetType().GetMethod("Get", new[] { typeof(int), typeof(int) }).Invoke(repository,
                                                                                                 new[]
                                                                                                     {
                                                                                                         key,
                                                                                                         codigoEstabelecimento
                                                                                                     });

            if (((int)key) == 0 || entidadeModel == null)
                throw new ItemNotFoundException(key, entidade.GetType().Name);

            entidadeModel.Delete();

            return new DTORetorno();
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTORetorno Listar(string chaveCriptografada, DTOBase entidade, int codigoEstabelecimento)
        {
            var repository = GetRepository(entidade);

            var resultado = (IList)repository.GetType().GetMethod(
                "ListAll", new[] { typeof(int) })
                .Invoke(repository, new object[] { codigoEstabelecimento });

            GetAdministradorEstabelecimentos(resultado, codigoEstabelecimento);

            return resultado != null ? GetResult(resultado, entidade) : null;
        }

        [ValidateCryptographyKey(false, (int)Common.Tipos.Aplicacao.Vanadium)]
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTORetorno Buscar(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento)
        {
            var repository = GetRepository(entidade);

            var key = GetPrimaryKey(entidade);

            var resultado =
                repository.GetType().GetMethod("Get", new[] {typeof (int), typeof (int)}).Invoke(repository,
                                                                                                 new[]
                                                                                                     {
                                                                                                         key,
                                                                                                         codigoEstabelecimento
                                                                                                     });

            if (resultado == null)
                throw new ItemNotFoundException(key, entidade.GetType().Name);

            GetAdministradorEstabelecimentos(resultado, codigoEstabelecimento);

            return GetResult(resultado, entidade);
        }

        public T Buscar<T>(string chaveCriptografada, DTOEntrada entidade, int codigoEstabelecimento) where T : DTORetorno
        {
            return (T) Buscar(chaveCriptografada, entidade, codigoEstabelecimento);
        }

        private static DTORetorno GetResult(IList resultado, DTOBase entidade)
        {
            if (!entidade.GetType().IsMarkedWith<ComplexReturnTypeAttribute>())
                return new DTORetorno();

            var returnType = entidade.GetType().GetAttribute<ComplexReturnTypeAttribute>();

            var result = (DTORetorno)Activator.CreateInstance(returnType.Type);

            result.SetValue(returnType.PropertyName, resultado, true, true);

            return result;
        }

        private static DTORetorno GetResult(object resultado, DTOBase entidade)
        {
            if (!entidade.GetType().IsMarkedWith<ReturnTypeAttribute>())
                return new DTORetorno();

            var returnType = entidade.GetType().GetAttribute<ReturnTypeAttribute>();

            var transformMethod = typeof (TransformExtension).GetMethod("Transform",
                                                                        BindingFlags.Default | BindingFlags.Public |
                                                                        BindingFlags.Static,
                                                                        null, new[] {typeof (object), typeof(bool)}, null).
                MakeGenericMethod(returnType.Type);

            var result = (DTORetorno) transformMethod.Invoke(resultado, new[] { resultado, true });

            return result;
        }

        private static object GetRepository(DTOBase entidade)
        {
            var domain = GetDomainModel(entidade);

            return domain.GetType().GetProperty("Repository").GetValue(domain, null);
        }

        private static object GetDomainModel(DTOBase entidade)
        {
            var att = entidade.GetType().GetAttribute<ObjectMapAttribute>();

            if (att == null)
                throw new Exception(entidade.GetType().Name + ". Mapeamento do DTO não encontrado.");

            var domain = ObjectFactory.GetObject(att.Name);

            return domain;
        }

        private static object GetPrimaryKey(DTOEntrada entidade)
        {
            var primarykey = entidade.GetType().GetProperties().FirstOrDefault(p => p.IsMarkedWith<KeyAttribute>());

            if (primarykey == null)
                throw new Exception(entidade.GetType().Name + ". Chave primária não encontrada.");

            return primarykey.GetValue(entidade, null);
        }

        private void SetEstabelecimento(int codigoEstabelecimento, IDomainModel entidadeModel)
        {
            var estabelecimento = VanadiumEstabelecimentoRepositorio.Get(codigoEstabelecimento);

            var property = entidadeModel.GetType().GetProperty("Estabelecimento");

            if (estabelecimento == null || property == null)
                return;

            property.SetValue(entidadeModel, estabelecimento, null);
        }

        private static void SetAplicacao(string chaveCriptografada, IDomainModel model)
        {
            var property = model.GetType().GetProperty("Aplicacao");

            if(property == null || property.GetType() != typeof(Aplicacao))
                return;

            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            property.SetValue((Common.Tipos.Aplicacao) security.CodigoAplicacao, model, null);
        }

        private static void GetAdministradorEstabelecimentos(IList resultado, int codigoEstabelecimento)
        {
            if (resultado.GetType() == typeof(List<Usuario>))
            {
                foreach (Usuario usuario in resultado)
                {
                    var estabelecimento = usuario.Estabelecimentos.FirstOrDefault(e => e.Estabelecimento.Codigo == codigoEstabelecimento);

                    if(estabelecimento != null)
                        usuario.IsAdministrador = estabelecimento.TipoUsuario == TipoUsuario.Administrator;
                }
            }
        }

        private static void GetAdministradorEstabelecimentos(object resultado, int codigoEstabelecimento)
        {
            if (resultado.GetType().BaseType == typeof(Usuario))
            {
                var estabelecimento = ((Usuario)resultado).Estabelecimentos.FirstOrDefault(e => e.Estabelecimento.Codigo == codigoEstabelecimento);

                if (estabelecimento != null)
                    ((Usuario) resultado).IsAdministrador = estabelecimento.TipoUsuario == TipoUsuario.Administrator;
            }
        }
    }
}