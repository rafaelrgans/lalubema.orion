﻿using System;
using AopAlliance.Intercept;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Attributes;
using Lalubema.Utilitarios.Exceptions.Security;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Orion.Domain.Services.Aspects
{
    public class ServiceContextAspect : IMethodInterceptor
    {
        #region IMethodInterceptor Members

        /// <summary>
        ///     Implement this method to perform extra treatments before and after
        ///     the call to the supplied <paramref name="invocation" />.
        /// </summary>
        /// <remarks>
        ///     <p>
        ///         Polite implementations would certainly like to invoke
        ///         <see cref="M:AopAlliance.Intercept.IJoinpoint.Proceed" />.
        ///     </p>
        /// </remarks>
        /// <param name="invocation">
        ///     The method invocation that is being intercepted.
        /// </param>
        /// <returns>
        ///     The result of the call to the
        ///     <see cref="M:AopAlliance.Intercept.IJoinpoint.Proceed" /> method of
        ///     the supplied <paramref name="invocation" />; this return value may
        ///     well have been intercepted by the interceptor.
        /// </returns>
        /// <exception cref="T:System.Exception">
        ///     If any of the interceptors in the chain or the target object itself
        ///     throws an exception.
        /// </exception>
        public object Invoke(IMethodInvocation invocation)
        {
            AuthorizeRequest(invocation);

            return invocation.Proceed();
        }

        #endregion

        #region [ Security ]

        private void AuthorizeRequest(IMethodInvocation invocation)
        {
            if (invocation.Method.Name.StartsWith("Autenticar", StringComparison.InvariantCultureIgnoreCase) ||
                !invocation.Method.IsMarkedWith<ValidateCryptographyKey>())
                return;

            if (invocation.Arguments.Length == 0 || invocation.Arguments[0] == null ||
                (invocation.Arguments[0].GetType() != typeof(DTOEntrada) &&
                 string.IsNullOrWhiteSpace(invocation.Arguments[0].ToString())))
                throw new InvalidCryptographyKeyException(string.Empty);

            var validateCryptographyKeyAttribute = invocation.Method.GetAttribute<ValidateCryptographyKey>();

            var cryptographyKey = string.Empty;

            if (invocation.Arguments[0].GetType().IsTypeOf(typeof(string)))
                cryptographyKey = invocation.Arguments[0].ToString();
            else if (invocation.Arguments[0].GetType().IsTypeOf(typeof(DTOEntrada)))
                cryptographyKey = ((DTOEntrada)invocation.Arguments[0]).ChaveCriptografada;

            ValidateCryptographyKey(cryptographyKey, validateCryptographyKeyAttribute.UserKeyRequired);
        }

		private void ValidateCryptographyKey(string cryptographyKey, bool userKeyRequired)
		{
			try
			{
				var security = new ChaveCriptografadaHelper(cryptographyKey);

				if (userKeyRequired && security.CryptographyType != CryptographyType.User)
					throw new InvalidCryptographyKeyException(cryptographyKey);

                switch (security.CryptographyType)
                {
                    case CryptographyType.Undefined:
                        throw new InvalidCryptographyKeyException(cryptographyKey);
                    case CryptographyType.System:
                        {
                            TimeSpan duracao = DateTime.UtcNow.Subtract(security.DataCriacao);
                            if (duracao.TotalMinutes < 0)
                                throw new ExpiredCryptographyKeyException(cryptographyKey);
                            break;
                        }
                    case CryptographyType.User:
                        {
                            TimeSpan duracao = DateTime.UtcNow.Subtract(security.DataCriacao);
                            if (duracao.TotalMinutes < 0 || duracao.TotalMinutes > 20)
                                throw new ExpiredCryptographyKeyException(cryptographyKey);
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception)
            {
                throw new InvalidCryptographyKeyException(cryptographyKey);
            }
        }

        #endregion [ Security ]
    }
}