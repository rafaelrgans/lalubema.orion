﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Buscar(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Usuario>(criteria);
        }

        public Usuario BuscarUsuarioPorEmail(string usuario)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario));

            return Execute<Usuario>(criteria);
        }

        public IList<Usuario> ListAll(int codigoEstabelecimento)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Usuario>("c").CreateAlias("c.Estabelecimentos", "e").Add(Restrictions.Eq(
                                                                                                              "e.Estabelecimento.Codigo",
                                                                                                              codigoEstabelecimento));

            return ToList(criteria);
        }

        public Usuario Get(int id, int codigoEstabelecimento)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Usuario>("c").CreateAlias("c.Estabelecimentos", "e").Add(Restrictions.Eq(
                                                                                                              "c.Codigo",
                                                                                                              id)).Add(
                                                                                                                       Restrictions
                                                                                                                           .
                                                                                                                           Eq
                                                                                                                           ("e.Estabelecimento.Codigo",
                                                                                                                            codigoEstabelecimento));

            return Execute<Usuario>(criteria);
        }
    }
}