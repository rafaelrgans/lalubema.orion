﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
	public class RelatorioRepositorio : RepositorioBase<Relatorio>, IRelatorioRepositorio<Relatorio>
	{
		public IList<Relatorio> GerarRelatorio(int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, Common.Tipos.TipoRelatorio ReportType)
		{
			var criteria = DetachedCriteria.For<Relatorio>("p")
										   .CreateAlias("p.Pedido", "e")
										   .Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
										   //.Add(Restrictions.Between("r.Created", dtInicio, dtFim))
										   .Add(Restrictions.IsNotNull("p.DataEncerramento"));

			return ToList(criteria);
		}
	}
}
