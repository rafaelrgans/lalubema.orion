﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class TransactionRepositorio : RepositorioBase<Transaction>, ITransactionRepositorio<Transaction>
    {
        public IList<Transaction> ListAll(int codigoResponsavel)
        {
            var criteria = DetachedCriteria.For<Transaction>("o")
                .CreateAlias("o.Responsavel", "r")
                .Add(Restrictions.Eq("r.Codigo", codigoResponsavel));

            return ToList<Transaction>(criteria);
        }
    }
}