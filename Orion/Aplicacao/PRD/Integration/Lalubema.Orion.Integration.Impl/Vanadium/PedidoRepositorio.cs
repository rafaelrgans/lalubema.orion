﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using Lalubema.Orion.Common.Tipos;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class PedidoRepositorio : RepositorioBase<Pedido>, IPedidoRepositorio<Pedido>
    {
        public Pedido GetPedidoAberto(int codigoMesa, int codigoEstabelecimento)
        {
            var criteria = DetachedCriteria.For<Pedido>("p")
                .CreateAlias("p.Estabelecimento", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
                .Add(Restrictions.Eq("p.CodigoMesa", codigoMesa))
                .Add(Restrictions.IsNull("p.DataEncerramento"));

            return Execute<Pedido>(criteria);
        }

		public Pedido GetTransferirMesaPedido(int CodigoPedido, int codigoEstabelecimento)
		{
			var criteria = DetachedCriteria.For<Pedido>("p")
				.CreateAlias("p.Estabelecimento", "e")
				.Add(Restrictions.Eq("p.Codigo", CodigoPedido))
				.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
				.Add(Restrictions.IsNull("p.DataEncerramento"));

			return Execute<Pedido>(criteria);
		}

		public Pedido GetPedidoAberto(int codigoMesa, int codigoEstabelecimento, int CodUsuario)
		{
			var Usuario = new Usuario().Get(CodUsuario);
			if (Usuario.UserType == Common.Tipos.TipoUsuario.Cliente)
			{
				var criteria = DetachedCriteria.For<Pedido>("p")
					.CreateAlias("p.Estabelecimento", "e")
					.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
					.Add(Restrictions.Eq("p.Cliente", Usuario))
					.Add(Restrictions.Eq("p.CodigoMesa", codigoMesa))
					.Add(Restrictions.IsNull("p.DataEncerramento"));

				return Execute<Pedido>(criteria);
			}
			else
			{
				var criteria = DetachedCriteria.For<Pedido>("p")
					.CreateAlias("p.Estabelecimento", "e")
					.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
					.Add(Restrictions.Eq("p.Responsavel", Usuario))
					.Add(Restrictions.Eq("p.CodigoMesa", codigoMesa))
					.Add(Restrictions.IsNull("p.DataEncerramento"));

				return Execute<Pedido>(criteria);
			}
		}

		public IList<Pedido> ListPedidosAbertos(int codigoEstabelecimento)
		{
			var criteria = DetachedCriteria.For<Pedido>("p")
                                .CreateAlias("p.Estabelecimento", "e")
                                .Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
                                .Add(Restrictions.IsNull("p.DataEncerramento"));


            return ToList(criteria);
		}

		public IList<Pedido> ListPedidosFechadosRelatorio(int codigoEstabelecimento, int codigoUsuario, DateTime DtInicio, DateTime DtFim)
		{
			var criteria = DetachedCriteria.For<Pedido>("p")
										.CreateAlias("p.Estabelecimento", "e")
										.CreateAlias("p.Responsavel", "r")
										.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
										.Add(Restrictions.Eq("r.Codigo", codigoUsuario))
										.Add(Restrictions.Between("p.Created", DtInicio, DtFim))
										.Add(Restrictions.IsNotNull("p.DataEncerramento"));

			return ToList(criteria);
		}

        public IList<Pedido> ListarPedidosAbertosUsuario(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Pedido>("p")
                               .CreateAlias("p.Cliente", "c")
                               .Add(Restrictions.Eq("c.Codigo", codigoUsuario))
                               .Add(Restrictions.IsNull("p.DataEncerramento"));

            return ToList(criteria);
        }

        public IList<Pedido> ListarHistorioPedidosUsuario(int codigoUsuario, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim)
        {
            var criteria = DetachedCriteria.For<Pedido>("p")
                               .CreateAlias("p.Cliente", "c")
                               .Add(Restrictions.Eq("c.Codigo", codigoUsuario))
							   .Add(Restrictions.IsNotNull("p.DataEncerramento"))
							   .Add(Restrictions.Ge("p.DataEncerramento", dataInicio))
							   .Add(Restrictions.Le("p.DataEncerramento", dataFim));

            if (codigoEstabelecimento > 0)
            {
                criteria =
                    criteria.CreateAlias("p.Estabelecimento", "e")
                            .Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento));
            }

            return ToList(criteria);
        }

		public IList<Pedido> ListarPedidosAbertosMenu(int codigoMesa, int codigoEstabelecimento)
		{
			var criteria = DetachedCriteria.For<Pedido>("p")
				.CreateAlias("p.Estabelecimento", "e")
				.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
				.Add(Restrictions.Eq("p.CodigoMesa", codigoMesa))
				.Add(Restrictions.IsNull("p.DataEncerramento"));

			return ToList(criteria);			
		}

		public IList<Pedido> GerarRelatorio(int codigoEstabelecimento, DateTime dtInicio, DateTime dtFim, TipoRelatorio ReportType)
		{
			var responsavel = new Usuario().Get(2);
			var cliente = new Usuario().Get(1040);
			//DetachedCriteria subquery = DetachedCriteria.For(typeof(Usuario));
			var criteria = DetachedCriteria.For<Pedido>("p")
				.CreateAlias("p.Estabelecimento", "e")
				.CreateAlias("p.Responsavel", "r")
				//.CreateAlias("p.Cliente", "c")
				.Add(Restrictions.Eq("e.Codigo", codigoEstabelecimento))
				.Add(Restrictions.Eq("r.Codigo", responsavel.Codigo))
				//.Add(Subqueries.Lt(1, subquery))
				.Add(Restrictions.IsNull("p.DataEncerramento"));
				//.SetProjection(Projections.ProjectionList()
				//    .Add(Projections.Alias(Projections.Sum("i.Preco").As("Total"), "Total"))
				//    .Add(Projections.Alias(Projections.GroupProperty("e.Codigo"), "Codigo"))
				//    .Add(Projections.Alias(Projections.GroupProperty("r.Codigo"), "Responsavel"))
				//);

			return ToList(criteria);
		}





	}
}