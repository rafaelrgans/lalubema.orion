﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Vanadium;
using Lalubema.Orion.Integration.Spec.Vanadium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Vanadium
{
    public class EstabelecimentoRepositorio : RepositorioBase<Estabelecimento>,
                                              IEstabelecimentoRepositorio<Estabelecimento>
    {
        public Estabelecimento Get(int id, int codigoEstabelecimento)
        {
            return Get(id);
        }

        public IList<Estabelecimento> ListAll(int codigoEstabelecimento)
        {
            var criteria =
                DetachedCriteria.For<Estabelecimento>("o").Add(Restrictions.Eq("o.Codigo", codigoEstabelecimento));

            return ToList<Estabelecimento>(criteria);
        }
    }
}