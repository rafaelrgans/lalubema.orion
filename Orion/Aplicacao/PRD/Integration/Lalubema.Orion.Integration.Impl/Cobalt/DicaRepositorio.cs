﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Cobalt;
using Lalubema.Orion.Integration.Spec.Cobalt;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Cobalt
{
    public class DicaRepositorio : RepositorioBase<Dica>, IDicaRepositorio<Dica>
    {
		public Dica Get(int codigo)
		{
			var criteria = DetachedCriteria.For<Dica>("o")
				.Add(Restrictions.Eq("o.Codigo", codigo));

			return Execute<Dica>(criteria);
		}

        public IList<Dica> ListAll(DateTime lastSync, bool exibirOnly)
        {
            var criteria = DetachedCriteria.For<Dica>("o")
                                           .Add(Restrictions.Ge("o.LastUpdate", lastSync))
										   .AddOrder(Order.Desc("o.Ativo"));

            if (exibirOnly)
                criteria = criteria.Add(Restrictions.Eq("o.Ativo", exibirOnly));

            return ToList<Dica>(criteria);
        }

        public IList<Dica> ListAll(bool exibirOnly)
        {
			var criteria = DetachedCriteria.For<Dica>("o").AddOrder(Order.Desc("o.Ativo"));

            return ToList<Dica>(criteria);
        }

		public IList<Dica> ListAll(int Codigo)
        {
            var criteria = DetachedCriteria.For<Dica>("o")
										   .Add(Restrictions.Eq("o.Codigo", Codigo))
										   .AddOrder(Order.Desc("o.Ativo"));

            return ToList<Dica>(criteria);
        }


	}
}