﻿using Lalubema.Orion.Domain.Cobalt;
using Lalubema.Orion.Integration.Spec.Cobalt;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Cobalt
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {

        public Usuario Buscar(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Password", senha));

            return Execute<Usuario>(criteria);
        }
    }
}