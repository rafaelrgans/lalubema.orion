﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class AtletaRepositorio : RepositorioBase<Atleta>, IAtletaRepositorio<Atleta>
    {
        #region Implementation of IAtletaRepositorio<Usuario>

        public IList<Atleta> ListarAtletasDisponiveisEquipe(int codigoTorneio, int codigoEquipe)
        {
            var criteria =
                DetachedCriteria.For<Atleta>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).CreateAlias(
                    "a.Equipes", "e", JoinType.LeftOuterJoin).Add(Restrictions.Eq("t.Torneio.Codigo", codigoTorneio)).Add(
                        Restrictions.Eq("t.Status", Common.Tipos.StatusUsuarioTorneio.Participante));

            criteria = codigoEquipe > 0
                           ? criteria.Add(Restrictions.Or(Restrictions.Eq("e.Codigo", codigoEquipe), Restrictions.IsNull("e.Codigo")))
                           : criteria.Add(Restrictions.IsNull("e.Codigo"));

            return ToList(criteria);
        }

        public IList<Atleta> ListarAtletasTorneio(int codigoTorneio)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Atleta>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).Add(
                    Restrictions.Eq("t.Status", Common.Tipos.StatusUsuarioTorneio.Participante));

            return ToList(criteria);
        }

        public IList<Atleta> ListarAtletasTorneio(int codigoTorneio, bool administrador)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Atleta>("a").CreateAlias("a.Torneios", "t", JoinType.InnerJoin).Add(
                    Restrictions.Eq("t.Administrador", administrador));

            return ToList(criteria);
        }

        public Atleta BuscarAtletaPorEmail(string email)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Atleta>("a").Add(Restrictions.Eq("a.Username", email)).SetFetchMode("Torneios",
                                                                                                      FetchMode.Join);

            return Execute<Atleta>(criteria);
        }

        #endregion
    }
}