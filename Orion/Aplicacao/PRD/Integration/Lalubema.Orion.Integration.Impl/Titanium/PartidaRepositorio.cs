﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class PartidaRepositorio : RepositorioBase<Partida>, IPartidaRepositorio<Partida>
    {
        #region IPartidaRepositorio<Partida> Members

        public IList<Partida> ListarPartidas(int codigoTorneio)
        {
            var criteria = DetachedCriteria.For<Partida>("p")
                .Add(Restrictions.Eq("p.Rodada.Torneio.Codigo", codigoTorneio));

            return ToList(criteria);
        }

        public IList<Partida> ListarPartidas(int codigoTorneio, int codigoAtleta)
        {
            var criteria = DetachedCriteria.For<Partida>("p")
                .Add(Restrictions.Eq("p.Rodada.Torneio.Codigo", codigoTorneio))
                .CreateCriteria("p.Equipes", "e", JoinType.InnerJoin)
                .CreateCriteria("e.Equipe.Usuarios", "a", JoinType.InnerJoin)
                .Add(Restrictions.Eq("a.Codigo", codigoAtleta));

            return ToList(criteria);
        }

        public IList<Partida> ListAll(int codigoTorneio, string emailUsuario)
        {
            var criteria = DetachedCriteria.For<Partida>("p")
                .CreateAlias("p.Rodada", "r")
                .CreateAlias("r.Torneio", "t")
                .Add(Restrictions.Eq("t.Codigo", codigoTorneio));

            var partidas = ToList(criteria);

            var retorno = new List<Partida>();

            foreach (var partida in from partida in partidas
                                    from equipe in partida.Equipes
                                    from usuario in equipe.Equipe.Usuarios
                                    let partidaTemp = partida
                                    where usuario.Username.Equals(emailUsuario, StringComparison.CurrentCultureIgnoreCase) && retorno.FirstOrDefault(p => p.Codigo == partidaTemp.Codigo) == null
                                    select partida)
            {
                retorno.Add(partida);
            }

            return retorno;
        }

        #endregion
    }
}