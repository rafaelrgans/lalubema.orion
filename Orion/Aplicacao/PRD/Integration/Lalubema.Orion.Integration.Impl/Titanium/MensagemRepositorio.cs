﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Titanium;
using Lalubema.Orion.Integration.Spec.Titanium;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Titanium
{
    public class MensagemRepositorio : RepositorioBase<Mensagem>, IMensagemRepositorio<Mensagem>
    {
        #region IMensagemRepositorio<Mensagem> Members

        public IList<Mensagem> ListarMensagens(int codigoTorneio)
        {
            var criteria = DetachedCriteria.For<Mensagem>("m").Add(Restrictions.Eq("m.Torneio.Codigo", codigoTorneio));

            return ToList(criteria);
        }

        #endregion
    }
}