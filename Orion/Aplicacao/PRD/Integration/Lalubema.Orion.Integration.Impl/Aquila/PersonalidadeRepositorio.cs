﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class PersonalidadeRepositorio : RepositorioBase<Personalidade>, IPersonalidadeRepositorio<Personalidade>
    {
        public Personalidade Get(int idPersonalidade)
        {
			var criteria = DetachedCriteria.For<Personalidade>("p")
				.CreateAlias("p.Noticias", "n")
				.Add(Restrictions.Eq("p.Codigo", idPersonalidade))
				.AddOrder(Order.Desc("n.Data"));

            return Execute<Personalidade>(criteria);
        }

        public Personalidade Get(string emailPersonalidade)
        {
            var personalidades = ListAll(emailPersonalidade);

            if (personalidades.Count > 0)
                return personalidades[0];

            return null;
        }

        public Personalidade Get(int idPersonalidade, string emailPersonalidade)
        {
            var criteria = DetachedCriteria.For<Personalidade>("p")
                .CreateAlias("p.Usuarios", "u", JoinType.InnerJoin)
                .Add(Restrictions.Eq("p.Codigo", idPersonalidade))
                .Add(Restrictions.Eq("u.Username", emailPersonalidade));

            return Execute<Personalidade>(criteria);
        }

        public IList<Personalidade> ListAll(string emailPersonalidade)
        {
            var criteria = DetachedCriteria.For<Personalidade>("p")
                .CreateAlias("p.Usuarios", "u", JoinType.InnerJoin)
                .Add(Restrictions.Eq("u.Username", emailPersonalidade));

            return ToList<Personalidade>(criteria);
        }
    }
}