﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.Aquila;
using Lalubema.Orion.Integration.Spec.Aquila;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Aquila
{
    public class ItemRepositorio : RepositorioBase<Item>, IItemRepositorio<Item>
    {
        public IList<Item> ListAll(int codigoAlbum, int codigoPersonalidade)
        {
            var criteria = DetachedCriteria.For<Item>("o")
                .CreateAlias("o.Album", "a")
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("a.Codigo", codigoAlbum))
                .Add(Restrictions.Eq("p.Codigo", codigoPersonalidade));

            return ToList<Item>(criteria);
        }

        public Item Get(int codigo, int codigoPersonalidade)
        {
            var criteria = DetachedCriteria.For<Item>("o")
                .CreateAlias("o.Album", "a")
                .CreateAlias("a.Personalidade", "p")
                .Add(Restrictions.Eq("p.Codigo", codigoPersonalidade))
                .Add(Restrictions.Eq("o.Codigo", codigo));

            return Execute<Item>(criteria);
        }
    }
}