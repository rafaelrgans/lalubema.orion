﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class EmissoraRepositorio : RepositorioBase<Emissora>, IEmissoraRepositorio<Emissora>
    {
        public Emissora Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Emissora>("o")
                .Add(Restrictions.Eq("o.Codigo", codigo));

            return Execute<Emissora>(criteria);
        }

        public IList<Emissora> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Emissora>("o")
                .Add(Restrictions.Eq("o.Codigo", codigoEmissora));

            return ToList(criteria);
        }
    }
}