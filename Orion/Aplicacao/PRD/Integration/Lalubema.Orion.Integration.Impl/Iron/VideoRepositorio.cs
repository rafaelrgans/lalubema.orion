﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class VideoRepositorio : RepositorioBase<Video>, IVideoRepositorio<Video>
    {
        public Video Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Video>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Video>(criteria);
        }

        public IList<Video> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Video>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }
    }
}