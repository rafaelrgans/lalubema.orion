﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class EquipeRepositorio : RepositorioBase<Equipe>, IEquipeRepositorio<Equipe>
    {
        public Equipe Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Equipe>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Equipe>(criteria);
        }

        public IList<Equipe> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Equipe>("o")
                .CreateAlias("o.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }

        public Equipe GetByProduto(int codigoProduto)
        {
            var criteria = DetachedCriteria.For<Equipe>("e").Add(Restrictions.Eq("e.Programa.Codigo", codigoProduto));

            return Execute<Equipe>(criteria);
        }
    }
}