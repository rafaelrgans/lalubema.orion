﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class ComponenteRepositorio : RepositorioBase<Componente>, IComponenteRepositorio<Componente>
    {
        public Componente Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Componente>("o")
                .CreateAlias("o.Equipe", "eq")
                .CreateAlias("eq.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Componente>(criteria);
        }

        public IList<Componente> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Componente>("o")
                .CreateAlias("o.Equipe", "eq")
                .CreateAlias("eq.Programa", "p")
                .CreateAlias("p.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }
    }
}