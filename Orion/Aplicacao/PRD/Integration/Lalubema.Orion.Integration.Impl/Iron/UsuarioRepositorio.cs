﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Iron;
using Lalubema.Orion.Integration.Spec.Iron;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Iron
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Get(int codigo, int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Usuario>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("o.Codigo", codigo))
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return Execute<Usuario>(criteria);
        }

        public IList<Usuario> ListAll(int codigoEmissora)
        {
            var criteria = DetachedCriteria.For<Usuario>("o")
                .CreateAlias("o.Emissora", "e")
                .Add(Restrictions.Eq("e.Codigo", codigoEmissora));

            return ToList(criteria);
        }

        public Usuario Buscar(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Usuario>(criteria);
        }

        public Usuario Buscar(string usuario)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario));

            return Execute<Usuario>(criteria);
        }
    }
}