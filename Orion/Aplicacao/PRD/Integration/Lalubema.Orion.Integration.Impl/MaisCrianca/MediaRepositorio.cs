﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.MaisCrianca;
using Lalubema.Orion.Integration.Spec.MaisCrianca;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.MaisCrianca
{
    public class MediaRepositorio : RepositorioBase<Media>, IMediaRepositorio<Media>
    {
        public IList<Media> ListMediaFeed(int pageNumber, int recordsPerPage, TipoAlbum mediaType)
        {
            var criteria = QueryOver.Of<Media>()
                .Where(o => o.MediaType == mediaType)
                .OrderBy(Projections.Property("Created")).Desc
                .Skip(pageNumber * recordsPerPage)
                .Take(recordsPerPage)
                .DetachedCriteria;

            return ToList<Media>(criteria);
        }
    }
}