﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class ClubeRepositorio : RepositorioBase<Clube>, IClubeRepositorio<Clube>
    {
        public IList<Clube> ListAll(int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<Clube>("c").Add(Restrictions.Eq("c.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}