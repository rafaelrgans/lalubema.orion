﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class CategoriaRepositorio : RepositorioBase<Categoria>, ICategoriaRepositorio<Categoria>
    {
        public IList<Categoria> ListAll(int codigoUsuario)
        {
            var criteria =
                DetachedCriteria.For<Categoria>("c").Add(Restrictions.Eq("c.Usuario", codigoUsuario));

            return ToList(criteria);
        }
    }
}