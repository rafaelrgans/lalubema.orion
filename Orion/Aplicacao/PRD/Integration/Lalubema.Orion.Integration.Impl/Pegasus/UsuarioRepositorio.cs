﻿using Lalubema.Orion.Domain.Pegasus;
using Lalubema.Orion.Integration.Spec.Pegasus;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Pegasus
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Buscar(string email, string senha)
        {
            DetachedCriteria criteria =
                DetachedCriteria.For<Usuario>("u").Add(Restrictions.Eq("u.Email", email)).Add(Restrictions.Eq(
                                                                                                              "u.Senha",
                                                                                                              senha));

            return Execute<Usuario>(criteria);
        }
    }
}