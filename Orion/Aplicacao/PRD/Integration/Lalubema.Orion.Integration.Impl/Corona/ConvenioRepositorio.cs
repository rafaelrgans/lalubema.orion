﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class ConvenioRepositorio : RepositorioBase<Convenio>, IConvenioRepositorio<Convenio>
    {
        public Convenio Get(int codigo, int usuario)
        {
            var criteria = DetachedCriteria.For<Convenio>("o")
                .CreateAlias("o.Pessoa", "u")
                .Add(Restrictions.Eq("u.Codigo", usuario))
                .SetMaxResults(1);

            return Execute<Convenio>(criteria);
        }
    }
}