﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class LocalBemRepositorio : RepositorioBase<LocalBem>, ILocalBemRepositorio<LocalBem>
    {
        public IList<LocalBem> ListAll(int codigoEdificio)
        {
            var criteria = DetachedCriteria.For<LocalBem>("o")
                                           .CreateAlias("o.Edificios", "e", JoinType.InnerJoin)
                                           .Add(Restrictions.Eq("e.Codigo", codigoEdificio));

            return ToList<LocalBem>(criteria);
        }
    }
}