﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class UsuarioRepositorio : RepositorioBase<Usuario>, IUsuarioRepositorio<Usuario>
    {
        public Usuario Get(string usuario, string senha)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", usuario))
                .Add(Restrictions.Eq("u.Senha", senha));

            return Execute<Usuario>(criteria);
        }

        public Usuario Get(string username)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", username));

            return Execute<Usuario>(criteria);
        }

        public Usuario Get(string username, int nonCodigo)
        {
            var criteria = DetachedCriteria.For<Usuario>("u")
                .Add(Restrictions.Eq("u.Username", username))
                .Add(Restrictions.Not(Restrictions.Eq("u.Codigo", nonCodigo)));

            return Execute<Usuario>(criteria);
        }
    }
}