﻿using System.Collections.Generic;
using System;
using NHibernate.Criterion;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Orion.Domain.Corona;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class NotificacaoRepositorio : RepositorioBase<Notificacao>, INotificacaoRepositorio<Notificacao>
    {
        public IList<Notificacao> ListAll(DateTime begin, DateTime end)
        {
            var criteria = DetachedCriteria.For<Notificacao>("n")
               .Add(Restrictions.Between("n.DataNotificacao", begin, end));

            return ToList<Notificacao>(criteria);
        }
    }
}