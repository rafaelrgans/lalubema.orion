﻿using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class DeviceRepository : RepositorioBase<Device>, IDeviceRepository<Device>
    {
        public Device Get(string deviceToken)
        {
            var criteria = DetachedCriteria.For<Device>("o")
                                           .Add(Restrictions.Eq("o.Token", deviceToken));

            return Execute<Device>(criteria);
        }
    }
}