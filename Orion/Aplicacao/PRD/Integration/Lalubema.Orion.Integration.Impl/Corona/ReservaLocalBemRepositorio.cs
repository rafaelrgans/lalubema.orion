﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Corona;
using Lalubema.Orion.Integration.Spec.Corona;
using Lalubema.Utilitarios.Repositorios;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Corona
{
    public class ReservaLocalBemRepositorio : RepositorioBase<ReservaLocalBem>, IReservaLocalBemRepositorio<ReservaLocalBem>
    {
        public IList<ReservaLocalBem> ListAll(int codigoCondominio, int codigoUsuario)
        {
            var criteria = DetachedCriteria.For<ReservaLocalBem>("o")
                                           .CreateAlias("o.UnidadeHabitacional", "u", JoinType.InnerJoin)
                                           .CreateAlias("u.Moradores", "m")
                                           .CreateAlias("m.Pessoa", "p")
                                           .CreateAlias("u.Edificio", "e", JoinType.InnerJoin)
                                           .CreateAlias("e.Condominio", "c", JoinType.InnerJoin)
                                           .Add(Restrictions.Eq("p.Codigo", codigoUsuario))
                                           .Add(Restrictions.Eq("c.Codigo", codigoCondominio))
                                           .AddOrder(Order.Asc(Projections.Property("o.DataHoraInicio")));

            return ToList<ReservaLocalBem>(criteria);
        }
    }
}