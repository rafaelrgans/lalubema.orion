﻿using Lalubema.Orion.Domain.Crux;
using Lalubema.Orion.Integration.Spec.Crux;
using Lalubema.Utilitarios.Repositorios;

namespace Lalubema.Orion.Integration.Impl.Crux
{
    public class LivroRepositorio : RepositorioBase<Livro>, ILivroRepositorio<Livro>
    {
    }
}