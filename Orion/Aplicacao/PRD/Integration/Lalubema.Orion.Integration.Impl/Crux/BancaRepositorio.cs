﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.Crux;
using Lalubema.Orion.Integration.Spec.Crux;
using Lalubema.Utilitarios.Repositorios;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;

namespace Lalubema.Orion.Integration.Impl.Crux
{
    public class BancaRepositorio : RepositorioBase<Banca>, IBancaRepositorio<Banca>
    {
        public Banca ConsultarBanca(int codigoBanca, string cultureName)
        {
            var criteria = DetachedCriteria.For<Banca>("b")
                .CreateAlias("b.Livros", "l", JoinType.LeftOuterJoin)
                .Add(Restrictions.Eq("b.Codigo", codigoBanca))
                .Add(Restrictions.Eq("l.CultureName", cultureName))
                .SetFetchMode("l", FetchMode.Join);

            return Execute<Banca>(criteria);
        }

        public IList<Banca> ConsultarBanca(string nomeBanca, string cultureName)
        {
            var criteria = DetachedCriteria.For<Banca>("b")
                .Add(Restrictions.Like("b.Descricao", nomeBanca, MatchMode.Start));

            var livros = criteria
                .CreateCriteria("b.Livros", "l", JoinType.None)
                .Add(Restrictions.Eq("l.CultureName", cultureName));

            return ToList<Banca>(livros);
        }
    }
}