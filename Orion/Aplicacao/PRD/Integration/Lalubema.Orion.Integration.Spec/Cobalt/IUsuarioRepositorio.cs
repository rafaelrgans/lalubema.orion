﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Cobalt
{
    [ObjectMap("Cobalt_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Buscar(string usuario, string senha);
    }
}