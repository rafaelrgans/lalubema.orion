﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_ItemPedidoRepositorio", true)]
    public interface IItemPedidoRepositorio<T> : IRepositorioBase<T>
    {
    }
}