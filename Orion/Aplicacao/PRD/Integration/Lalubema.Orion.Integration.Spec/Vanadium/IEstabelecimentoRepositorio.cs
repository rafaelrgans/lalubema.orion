﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_EstabelecimentoRepositorio", true)]
    public interface IEstabelecimentoRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int id, int codigoEstabelecimento);

        IList<T> ListAll(int codigoEstabelecimento);
    }
}