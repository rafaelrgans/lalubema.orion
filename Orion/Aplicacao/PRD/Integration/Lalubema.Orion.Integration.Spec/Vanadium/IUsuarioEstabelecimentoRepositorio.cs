﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_UsuarioEstabelecimentoRepositorio", true)]
    public interface IUsuarioEstabelecimentoRepositorio<T> : IRepositorioBase<T>
    {
    }
}