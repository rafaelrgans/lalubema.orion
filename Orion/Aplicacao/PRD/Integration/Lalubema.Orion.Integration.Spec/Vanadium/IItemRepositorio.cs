﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Vanadium
{
    [ObjectMap("Vanadium_ItemRepositorio", true)]
    public interface IItemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoEstabelecimento);

        T Get(int id, int codigoEstabelecimento);

        IList<T> ListarDestaques(int codigoEstabelecimento);
    }
}