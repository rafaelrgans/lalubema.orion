﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Notification;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion.Notification
{
    [ObjectMap("Orion_NotificationRepositorio", true)]
    public interface INotificationRepository
    {
        void SendPush(DTOPushMessage pushMessage);
        
        void StartServices(DTONotificationSettings settings);

        void StopServices();
    }
}