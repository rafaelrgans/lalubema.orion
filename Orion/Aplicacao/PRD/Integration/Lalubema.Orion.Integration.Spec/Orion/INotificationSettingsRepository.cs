﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_NotificationSettingsRepository", true)]
    public interface INotificationSettingsRepository<T> : IRepositorioBase<T>
    {
    }
}