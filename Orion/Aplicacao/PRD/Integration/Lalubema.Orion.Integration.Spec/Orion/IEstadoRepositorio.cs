﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Orion
{
    [ObjectMap("Orion_EstadoRepositorio", true)]
    public interface IEstadoRepositorio<T> : IRepositorioBase<T>
    {
    }
}