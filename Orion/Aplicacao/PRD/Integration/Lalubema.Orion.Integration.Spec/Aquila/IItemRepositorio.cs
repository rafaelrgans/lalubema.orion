﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_ItemRepositorio", true)]
    public interface IItemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoAlbum, int codigoPersonalidade);

        T Get(int codigo, int codigoPersonalidade);
    }
}