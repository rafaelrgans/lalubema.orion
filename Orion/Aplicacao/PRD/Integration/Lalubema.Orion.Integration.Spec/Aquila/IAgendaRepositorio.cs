﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_AgendaRepositorio", true)]
    public interface IAgendaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoPersonalidade);

        T Get(int codigo, int codigoPersonalidade);
    }
}