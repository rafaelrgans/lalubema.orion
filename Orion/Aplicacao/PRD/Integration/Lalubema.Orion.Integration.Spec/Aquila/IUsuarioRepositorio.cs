﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Aquila
{
    [ObjectMap("Aquila_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Buscar(string usuario, string senha);

        T Consultar(string usuario, int codigoAplicacao);
    }
}