﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_TorneioAtletaRepositorio", true)]
    public interface ITorneioAtletaRepositorio<T> : IRepositorioBase<T>
    {
    }
}