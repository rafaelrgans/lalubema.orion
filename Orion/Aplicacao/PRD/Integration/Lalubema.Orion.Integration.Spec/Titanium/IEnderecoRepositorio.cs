﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_EnderecoRepositorio")]
    public interface IEnderecoRepositorio<T> : IRepositorioBase<T>
    {
    }
}