﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_TorneioRepositorio", true)]
    public interface ITorneioRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(string email);

        T BuscarTorneioUsuario(int codigoTorneio, string emailUsuario);
    }
}