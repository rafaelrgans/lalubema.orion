﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_QuadraRepositorio", true)]
    public interface IQuadraRepositorio<T> : IRepositorioBase<T>
    {
    }
}