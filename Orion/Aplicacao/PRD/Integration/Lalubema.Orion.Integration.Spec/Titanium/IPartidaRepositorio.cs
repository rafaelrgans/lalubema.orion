﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_PartidaRepositorio", true)]
    public interface IPartidaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListarPartidas(int codigoTorneio);

        IList<T> ListarPartidas(int codigoTorneio, int codigoAtleta);

        IList<T> ListAll(int codigoTorneio, string emailUsuario);
    }
}