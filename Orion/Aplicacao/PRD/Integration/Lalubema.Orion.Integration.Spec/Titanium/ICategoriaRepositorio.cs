﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_CategoriaRepositorio", true)]
    public interface ICategoriaRepositorio<T> : IRepositorioBase<T>
    {
    }
}