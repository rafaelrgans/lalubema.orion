﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Titanium
{
    [ObjectMap("Titanium_CompromissoRepositorio")]
    public interface ICompromissoRepositorio<T> : IRepositorioBase<T>
    {
    }
}