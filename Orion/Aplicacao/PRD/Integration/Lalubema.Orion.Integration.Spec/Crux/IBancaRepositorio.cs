﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Crux
{
    [ObjectMap("Crux_BancaRepositorio", true)]
    public interface IBancaRepositorio<T> : IRepositorioBase<T>
    {
        T ConsultarBanca(int codigoBanca, string cultureName);

        IList<T> ConsultarBanca(string nomeBanca, string cultureName);
    }
}