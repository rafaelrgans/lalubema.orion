﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PessoaCondominioRepositorio", true)]
    public interface IPessoaCondominioRepositorio<T> : IRepositorioBase<T>
    {
    }
}