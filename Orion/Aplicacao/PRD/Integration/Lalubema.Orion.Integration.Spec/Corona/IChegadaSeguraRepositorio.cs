﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ChegadaSeguraRepositorio", true)]
    public interface IChegadaSeguraRepositorio<T> : IRepositorioBase<T>
    {
    }
}