﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_UsuarioRepositorio", true)]
    public interface IUsuarioRepositorio<T> : IRepositorioBase<T>
    {
        T Get(string usuario, string senha);

        T Get(string username);

        T Get(string username, int nonCodigo);
    }
}