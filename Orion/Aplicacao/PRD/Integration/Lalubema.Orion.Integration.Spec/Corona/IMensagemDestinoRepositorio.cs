﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_MensagemDestinoRepositorio", true)]
    public interface IMensagemDestinoRepositorio<T> : IRepositorioBase<T>
    {
    }
}