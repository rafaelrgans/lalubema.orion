﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_DeviceRepository", true)]
    public interface IDeviceRepository<T> : IRepositorioBase<T>
    {
        T Get(string deviceToken);
    }
}