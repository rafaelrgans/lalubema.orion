﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_CondominioRepositorio", true)]
    public interface ICondominioRepositorio<T> : IRepositorioBase<T>
    {
    }
}