﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_UnidadeHabitacionalRepositorio", true)]
    public interface IUnidadeHabitacionalRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListarUnidadeHabitacionalPorCondominio(int codigoCondominio);
    }
}