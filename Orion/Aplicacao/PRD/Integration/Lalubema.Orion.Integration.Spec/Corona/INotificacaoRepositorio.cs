﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;
using System;
using System.Collections.Generic;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_NotificacaoRepositorio", true)]
    public interface INotificacaoRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(DateTime begin, DateTime end);
    }
}
