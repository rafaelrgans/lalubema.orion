﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_ClassificadoRepositorio", true)]
    public interface IClassificadoRepositorio<T> : IRepositorioBase<T>
    {
    }
}