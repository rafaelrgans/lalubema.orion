﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_MarcaVeiculoRepositorio", true)]
    public interface IMarcaVeiculoRepositorio<T> : IRepositorioBase<T>
    {
    }
}