﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_PessoaUnidadeHabitacionalRepositorio", true)]
    public interface IPessoaUnidadeHabitacionalRepositorio<T> : IRepositorioBase<T>
    {
    }
}