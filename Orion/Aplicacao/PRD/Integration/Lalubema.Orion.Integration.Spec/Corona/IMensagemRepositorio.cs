﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Corona
{
    [ObjectMap("Corona_MensagemRepositorio", true)]
    public interface IMensagemRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListarMensagensPublicadas();
    }
}