﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_ActivityRepositorio", true)]
    public interface IActivityRepositorio<T> : IRepositorioBase<T>
    {
    }
}