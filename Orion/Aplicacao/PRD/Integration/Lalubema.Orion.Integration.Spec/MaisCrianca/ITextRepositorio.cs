﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_TextRepositorio", true)]
    public interface ITextRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListTextFeed(int pageNumber, int recordsPerPage, int category, TextStatus status, int teiaVida);

        IList<T> ListTextTopFeed(int maxRecordsTopTextFeed, int category);
    }
}