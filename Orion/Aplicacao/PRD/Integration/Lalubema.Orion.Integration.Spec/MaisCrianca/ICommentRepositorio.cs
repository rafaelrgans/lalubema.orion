﻿using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.MaisCrianca
{
    [ObjectMap("MaisCrianca_CommentRepositorio", true)]
    public interface ICommentRepositorio<T> : IRepositorioBase<T>
    {
    }
}