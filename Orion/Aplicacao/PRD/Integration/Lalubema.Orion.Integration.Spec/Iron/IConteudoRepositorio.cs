﻿using System.Collections.Generic;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Iron
{
    [ObjectMap("Iron_ConteudoRepositorio", true)]
    public interface IConteudoRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoEmissora);

        IList<T> ListAll(int codigoEmissora);

        IList<T> ListByTipo(int codigoPrograma, TipoConteudo tipoConteudo);
    }
}