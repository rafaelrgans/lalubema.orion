﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Iron
{
    [ObjectMap("Iron_EquipeRepositorio", true)]
    public interface IEquipeRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoEmissora);

        IList<T> ListAll(int codigoEmissora);

        T GetByProduto(int codigoProduto);
    }
}