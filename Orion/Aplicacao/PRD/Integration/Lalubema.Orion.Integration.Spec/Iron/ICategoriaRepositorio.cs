﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Iron
{
    [ObjectMap("Iron_CategoriaRepositorio", true)]
    public interface ICategoriaRepositorio<T> : IRepositorioBase<T>
    {
        T Get(int codigo, int codigoEmissora);

        IList<T> ListAll(int codigoEmissora);
    }
}