﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Pegasus
{
    [ObjectMap("Pegasus_AgendaRepositorio", true)]
    public interface IAgendaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoUsuario);
    }
}