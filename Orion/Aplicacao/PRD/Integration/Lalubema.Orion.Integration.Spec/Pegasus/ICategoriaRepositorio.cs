﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Pegasus
{
    [ObjectMap("Pegasus_CategoriaRepositorio", true)]
    public interface ICategoriaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoUsuario);
    }
}