﻿using System.Collections.Generic;
using Lalubema.Utilitarios.Repositorios;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Integration.Spec.Pegasus
{
    [ObjectMap("Pegasus_MetaRepositorio", true)]
    public interface IMetaRepositorio<T> : IRepositorioBase<T>
    {
        IList<T> ListAll(int codigoUsuario);
    }
}