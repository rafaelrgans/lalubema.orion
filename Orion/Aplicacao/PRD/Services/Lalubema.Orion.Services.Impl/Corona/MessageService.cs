﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Mensagem;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class MessageService : ServiceBase, IMessageService
    {
        public DTOMuralRetorno ListarMuralMensagem(string chaveCriptografada, int unidadeHabitacional)
        {
            return CoronaMessageDomainService.ListarMuralMensagem(chaveCriptografada, unidadeHabitacional);
        }

        public DTOMensagensRetorno ListarMensagens(string chaveCriptografada, int unidadeHabitacional)
        {
            return CoronaMessageDomainService.ListarMensagens(chaveCriptografada, unidadeHabitacional);
        }

        public DTOCondominioRetorno ListarDadosCondominio(string chaveCriptografada, int condominio)
        {
            return CoronaMessageDomainService.ListarDadosCondominio(chaveCriptografada, condominio);
        }

        public DTORetorno Send(DTOSendMessage sendMessage)
        {
            return CoronaMessageDomainService.Send(sendMessage);
        }
    }
}