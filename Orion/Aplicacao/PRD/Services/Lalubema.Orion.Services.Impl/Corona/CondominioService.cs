﻿using System.IO;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Orion.Services.Spec.Corona;

namespace Lalubema.Orion.Services.Impl.Corona
{
    public class CondominioService : ServiceBase, ICondominioService
    {
        public Stream CheckEmail(string c)
        {
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html;charset=utf-8";

            return CoronaCondominioDomainService.CheckEmail(c);
        }

        public DTORetorno RegisterDevice(DTORegisterDevice registerDevice)
        {
            return CoronaCondominioDomainService.RegisterDevice(registerDevice);
        }

        public DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice)
        {
            return CoronaCondominioDomainService.UnregisterDevice(unregisterDevice);
        }
    }
}
