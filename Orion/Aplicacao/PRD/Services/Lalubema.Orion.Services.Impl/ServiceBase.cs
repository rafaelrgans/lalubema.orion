﻿using System.ServiceModel;
using Lalubema.Orion.Domain.Services.Spec.Aquila;
using Lalubema.Orion.Domain.Services.Spec.Crux;
using Lalubema.Orion.Domain.Services.Spec.Orion.InfraEstrutura;
using Lalubema.Orion.Domain.Services.Spec.Orion.Seguranca;
using Lalubema.Orion.Domain.Services.Spec.Pegasus;
using Lalubema.Orion.Domain.Services.Spec.Titanium;
using Lalubema.Orion.Domain.Services.Spec.Vanadium;
using Lalubema.Utilitarios.Services;
using Lalubema.Utilitarios.Spring;
using ICadastroService = Lalubema.Orion.Domain.Services.Spec.Vanadium.ICadastroService;

namespace Lalubema.Orion.Services.Impl
{
    [ServiceContract(Namespace = "http://www.lalubema.com/")]
    [ServiceKnownType("GetKnownTypes", typeof (KnownTypeProvider))]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    public class ServiceBase : Utilitarios.Wcf.ServiceBase
    {
        protected IAutenticacaoService AutenticacaoDomainService
        {
            get { return ObjectFactory.GetObject<IAutenticacaoService>(false); }
        }

        protected IAccountService AccountDomainService
        {
            get { return ObjectFactory.GetObject<IAccountService>(false); }
        }

        protected IFileSystemService FileSystemDomainService
        {
            get { return ObjectFactory.GetObject<IFileSystemService>(false); }
        }

        protected IImportacaoService PegasusImportacaoDomainService
        {
            get { return ObjectFactory.GetObject<IImportacaoService>(false); }
        }

        protected IBancaService BancaDomainService
        {
            get { return ObjectFactory.GetObject<IBancaService>(false); }
        }

        protected IAquilaService AquilaDomainService
        {
            get { return ObjectFactory.GetObject<IAquilaService>(false); }
        }

        protected IAdministracaoService TitaniumAdministracaoDomainService
        {
            get { return ObjectFactory.GetObject<IAdministracaoService>(false); }
        }

        #region [ Vanadium ]

        protected IEstabelecimentoService VanadiumEstabelecimentoDomainService
        {
            get { return ObjectFactory.GetObject<IEstabelecimentoService>(false); }
        }

        protected IPedidoService VanadiumPedidoDomainService
        {
            get { return ObjectFactory.GetObject<IPedidoService>(false); }
        }

        protected ICadastroService VanadiumCadastroDomainService
        {
            get { return ObjectFactory.GetObject<ICadastroService>(false); }
        }

        #endregion [ Vanadium ]


        protected Domain.Services.Spec.Iron.IImportacaoService IronImportacaoDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Iron.IImportacaoService>(false); }
        }

        protected Domain.Services.Spec.Orion.ICadastroService CadastroDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Orion.ICadastroService>(false); }
        }

        #region [ Mais Criança ]

        protected Domain.Services.Spec.MaisCrianca.ITextService MaisCriancaTextDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.MaisCrianca.ITextService>(false); }
        }

        protected Domain.Services.Spec.MaisCrianca.IMediaService MaisCriancaMediaDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.MaisCrianca.IMediaService>(false); }
        }

        #endregion [ Mais Criança ]

        #region [ Meu Condomínio ]

        protected Domain.Services.Spec.Corona.IUnidadeHabitacionalService CoronaUnidadeHabitacionalDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IUnidadeHabitacionalService>(false); }
        }

        protected Domain.Services.Spec.Corona.ICondominioService CoronaCondominioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.ICondominioService>(false); }
        }

        protected Domain.Services.Spec.Corona.IUsuarioService CoronaUsuarioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IUsuarioService>(false); }
        }

        protected Domain.Services.Spec.Corona.IMessageService CoronaMessageDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IMessageService>(false); }
        }

        protected Domain.Services.Spec.Corona.ILocalBemService CoronaLocalBemDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.ILocalBemService>(false); }
        }

        protected Domain.Services.Spec.Corona.IPushService CoronaPushDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IPushService>(false); }
        }

        protected Domain.Services.Spec.Corona.IEdificioService CoronaEdificioDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Corona.IEdificioService>(false); }
        }

        #endregion [ Meu Condomínio ]

        #region [ Cobalt - Cartório Fácil ]

        protected Domain.Services.Spec.Cobalt.IServicoOnlineService CobaltServicoOnlineDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Cobalt.IServicoOnlineService>(false); }
        }

        protected Domain.Services.Spec.Cobalt.IAccountService CobaltAccountDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Cobalt.IAccountService>(false); }
        }

        protected Domain.Services.Spec.Cobalt.IDicaService CobaltDicaDomainService
        {
            get { return ObjectFactory.GetObject<Domain.Services.Spec.Cobalt.IDicaService>(false); }
        }

        #endregion [ Cobalt - Cartório Fácil ]
    }
}