﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Services.Spec.Cobalt;

namespace Lalubema.Orion.Services.Impl.Cobalt
{
    public class AccountService : ServiceBase, IAccountService
    {
        public DTORetorno ReenviarSenha(DTOReenviarSenhaEntrada reenviarSenha)
        {
            return CobaltAccountDomainService.ReenviarSenha(reenviarSenha);
        }
    }
}