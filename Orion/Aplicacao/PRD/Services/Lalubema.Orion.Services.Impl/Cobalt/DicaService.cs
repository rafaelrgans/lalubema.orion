﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Orion.Services.Spec.Cobalt;

namespace Lalubema.Orion.Services.Impl.Cobalt
{
    public class DicaService : ServiceBase, IDicaService
    {
        public DTORetorno SalvarDica(DTODicaEntrada dica)
        {
            return CobaltDicaDomainService.SalvarDica(dica);
        }

        public DTORetorno ExcluirDica(DTODicaEntrada dica)
        {
            return CobaltDicaDomainService.ExcluirDica(dica);
        }

        public DTODicasRetorno ListarDicas(string chaveCriptografada)
        {
            return CobaltDicaDomainService.ListarDicas(chaveCriptografada);
        }

		public DTODicasRetorno ListarDica(DTODicaEntrada dica)
		{
			return CobaltDicaDomainService.ListarDica(dica);
		}
        public DTODicasRetorno ListarDicasDisponiveis(string chaveCriptografada, DateTime lastSync)
        {
            var lastSyncDate = lastSync == DateTime.MinValue ? (DateTime?) null : lastSync;

            return CobaltDicaDomainService.ListarDicasDisponiveis(chaveCriptografada, lastSyncDate);
        }



	}
}