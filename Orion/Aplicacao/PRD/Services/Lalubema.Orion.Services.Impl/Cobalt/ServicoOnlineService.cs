﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Services.Spec.Cobalt;

namespace Lalubema.Orion.Services.Impl.Cobalt
{
    public class ServicoOnlineService : ServiceBase, IServicoOnlineService
    {
        public DTORetorno SolicitarServicoOnlineNotarial(DTOServicoOnlineNotarialEntrada solicitacaoServicoNotarial)
        {
            return CobaltServicoOnlineDomainService.SolicitarServicoOnlineNotarial(solicitacaoServicoNotarial);
        }
    }
}