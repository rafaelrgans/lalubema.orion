﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Media;
using Lalubema.Orion.Services.Spec.MaisCrianca;

namespace Lalubema.Orion.Services.Impl.MaisCrianca
{
    public class MediaService : ServiceBase, IMediaService
    {
        public IList<DTOMediaRetorno> ListMediaFeed(DTOPagingFilter pagingFilter)
        {
            return MaisCriancaMediaDomainService.ListMediaFeed(pagingFilter);
        }

        public DTOMediaRetorno Save(DTOMediaEntrada media)
        {
            return MaisCriancaMediaDomainService.Save(media);
        }

        public DTORetorno Delete(DTOMediaEntrada media)
        {
            return MaisCriancaMediaDomainService.Delete(media);
        }
    }
}