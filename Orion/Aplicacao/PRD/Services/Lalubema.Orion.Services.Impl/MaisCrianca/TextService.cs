﻿using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Category;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Comment;
using Lalubema.Orion.Domain.DataTransferObject.MaisCrianca.Text;
using Lalubema.Orion.Services.Spec.MaisCrianca;

namespace Lalubema.Orion.Services.Impl.MaisCrianca
{
    public class TextService : ServiceBase, ITextService
    {
        public IList<DTOTextRetorno> ListTextFeed(DTOPagingFilter pagingFilter)
        {
            return MaisCriancaTextDomainService.ListTextFeed(pagingFilter);
        }

        public IList<DTOTextRetorno> ListTextTopFeed(DTOCategoryEntrada category)
        {
            return MaisCriancaTextDomainService.ListTextTopFeed(category);
        }

        public DTORetorno SendText(DTOTextEntrada text)
        {
            return MaisCriancaTextDomainService.SendText(text);
        }

        public DTORetorno Like(DTOTextEntrada text)
        {
            return MaisCriancaTextDomainService.Like(text);
        }

        public DTORetorno Comment(DTOCommentEntrada comment)
        {
            return MaisCriancaTextDomainService.Comment(comment);
        }

        public DTORetorno ChangeStatus(DTOTextEntrada text)
        {
            return MaisCriancaTextDomainService.ChangeStatus(text);
        }

        public DTORetorno Delete(DTOTextEntrada text)
        {
            return MaisCriancaTextDomainService.Delete(text);
        }
    }
}