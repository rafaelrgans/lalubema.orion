﻿using System.IO;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions;
using Lalubema.Orion.Services.Spec.Orion.Seguranca;

namespace Lalubema.Orion.Services.Impl.Orion.Seguranca
{
    public class AccountService : ServiceBase, IAccountService
    {
        public DTORetorno Register(DTOAccountRegister account)
        {
            return AccountDomainService.Register(account);
        }

        public DTORetorno RegisterAccount(DTOAccountRegister account)
        {
            return AccountDomainService.Register(account);
        }

        public Stream CheckEmail(string c)
        {
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html;charset=utf-8";

            return AccountDomainService.CheckEmail(c);
        }

        public DTORetorno RecoveryPassword(DTORecoveryPassword recoveryPassword)
        {
            return AccountDomainService.RecoveryPassword(recoveryPassword);
        }

        public DTORetorno DirectDebit(DTODirectDebitRequest directDebitRequest)
        {
            return AccountDomainService.DirectDebit(directDebitRequest);
        }
    }
}