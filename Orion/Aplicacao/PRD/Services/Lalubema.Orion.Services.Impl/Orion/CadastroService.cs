﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Lalubema.Orion.Common.Seguranca;
using Lalubema.Orion.Common.Tipos;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Services.Spec.Orion;
using Lalubema.Utilitarios.Extensions;

namespace Lalubema.Orion.Services.Impl.Orion
{
    public class CadastroService : ServiceBase, ICadastroService
    {
        public DTORetorno Salvar(DTOEntrada entidade)
        {
            return CadastroDomainService.Salvar(entidade);
        }

        public DTORetorno Excluir(DTOEntrada entidade)
        {
            return CadastroDomainService.Excluir(entidade);
        }

        public DTORetorno Listar(DTOEntrada entidade)
        {
            return CadastroDomainService.Listar(entidade);
        }

        [Obsolete("ListAll is deprecated, please use List instead. This method will be removed from future versions")]
        public IList<DTORetorno> ListAll(DTOEntrada entidade)
        {
            return CadastroDomainService.ListAll(entidade, null, null);
        }

        [Obsolete("Buscar is deprecated, please use Get instead. This method will be removed from future versions")]
        public DTORetorno Buscar(DTOEntrada entidade)
        {
            return CadastroDomainService.Buscar(entidade);
        }

        // ========================== JSON ===================================

        public DTORetorno Save(string entity, object entidade)
        {
            return CadastroDomainService.Salvar((DTOEntrada) entidade);
        }

        public DTORetorno Delete(string chaveCriptografada, string entity, string id)
        {
            var entidade = GetFiltro(chaveCriptografada, entity, id);

            return CadastroDomainService.Excluir(entidade);
        }

        public IList<DTORetorno> List(string chaveCriptografada, string entity, string filter, string sort)
        {
            var entidade = GetFiltro(chaveCriptografada, entity, "0");

            return CadastroDomainService.ListAll(entidade, filter, sort);
        }

        public DTORetorno Get(string chaveCriptografada, string entity, string id)
        {
            var entidade = GetFiltro(chaveCriptografada, entity, id);

            return CadastroDomainService.Buscar(entidade);
        }

        private static DTOEntrada GetFiltro(string chaveCriptografada, string entity, string id)
        {
            var aplicacao = GetAplicacao(chaveCriptografada);

            var entityType = Type.GetType(string.Format("Lalubema.Orion.Domain.DataTransferObject.{0}.Cadastro.{1}.DTO{1}Entrada, Lalubema.Orion.Domain.DataTransferObject", aplicacao, entity));

            var entidade = (DTOEntrada)Activator.CreateInstance(entityType);

            SetPrimaryKey(entidade, Convert.ToInt32(id));

            SetChaveCriptografada(entidade, chaveCriptografada);

            return entidade;
        }

        private static string GetAplicacao(string chaveCriptografada)
        {
            var security = new ChaveCriptografadaHelper(chaveCriptografada);

            return ((Aplicacao)security.CodigoAplicacao).ConvertToString();
        }

        private static void SetChaveCriptografada(DTOEntrada entidade, string chaveCriptografada)
        {
            entidade.SetValue("ChaveCriptografada", chaveCriptografada, false, false);
        }

        private static void SetPrimaryKey(DTOEntrada entidade, int id)
        {
            var primarykey = entidade.GetType().GetProperties().FirstOrDefault(p => p.IsMarkedWith<KeyAttribute>());

            if (primarykey == null)
                throw new Exception(entidade.GetType().Name + ". Chave primária não encontrada.");

            primarykey.SetValue(entidade, id, null);
        }
    }
}