﻿using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Services.Spec;

namespace Lalubema.Orion.Services.Impl
{
    public class AutenticacaoService : ServiceBase, IAutenticacaoService
    {
        public DTOAutenticacao AutenticarUsuario(DTOTicketPorUsuario ticket)
        {
            return AutenticacaoDomainService.Autenticar(ticket);
        }

        public DTOAutenticacao Autenticar(DTOTicketPorUsuario ticket)
        {
            return AutenticacaoDomainService.Autenticar(ticket);
        }

        public DTOAutenticacao RenovarAutenticacao(string chaveCriptografada)
        {
            return AutenticacaoDomainService.Autenticar(chaveCriptografada);
        }

        public DTOAutenticacao AutenticarSistema(string usuario, string senha)
        {
            return AutenticacaoDomainService.AutenticarPorSistema(usuario, senha);
        }
    }
}