﻿using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Iron.Importacao;
using Lalubema.Orion.Services.Spec.Iron;

namespace Lalubema.Orion.Services.Impl.Iron
{
    public class ImportacaoService : ServiceBase, IImportacaoService
    {
        public DTOImportacao Importar(string chaveCriptografada, int codigoEmissora)
        {
            return IronImportacaoDomainService.Importar(chaveCriptografada, codigoEmissora);
        }

        public DTORetorno ExportarAcessos(DTOExportarAcesso programas)
        {
            return IronImportacaoDomainService.ExportarAcessos(programas);
        }
    }
}