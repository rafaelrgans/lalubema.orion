﻿using System;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Orion.Services.Spec.Vanadium;

namespace Lalubema.Orion.Services.Impl.Vanadium
{
    public class PedidoService : ServiceBase, IPedidoService
    {
        public DTOEntidadeRetorno RegistrarPedido(DTOPedidoEntrada pedidoEntrada)
        {
            return VanadiumPedidoDomainService.RegistrarPedido(pedidoEntrada);
        }

        public DTORetorno ComandarPedido(DTOPedidoEntrada pedidoEntrada)
        {
            return VanadiumPedidoDomainService.ComandarPedido(pedidoEntrada);
        }

        public DTORetorno RemoverItens(DTOPedidoEntrada pedidoEntrada)
        {
            return VanadiumPedidoDomainService.RemoverItens(pedidoEntrada);
        }

        public DTORetorno FecharComanda(DTOPedidoEntrada pedidoEntrada)
        {
            return VanadiumPedidoDomainService.FecharComanda(pedidoEntrada);
        }

        public DTOMesasAbertasRetorno ListarMesasAbertas(string chaveCriptografada, int codigoEstabelecimento)
        {
            return VanadiumPedidoDomainService.ListarMesasAbertas(chaveCriptografada, codigoEstabelecimento);
        }

        public DTOPedidoRetorno BuscarComanda(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa)
        {
            return VanadiumPedidoDomainService.BuscarComanda(chaveCriptografada, codigoEstabelecimento, codigoMesa);
        }

        public DTOPedidoRetorno ListarItensPendentes(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa)
        {
            return VanadiumPedidoDomainService.ListarItensPendentes(chaveCriptografada, codigoEstabelecimento, codigoMesa);
        }

		public DTOHistoricoPedidos ListarPedidosAbertosMenu(string chaveCriptografada, int codigoEstabelecimento, int codigoMesa)
		{
			return VanadiumPedidoDomainService.ListarPedidosAbertosMenu(chaveCriptografada, codigoMesa, codigoEstabelecimento);
		}

		public DTOPedidoRetorno ListarPedidosEstabelecimento(string chaveCriptografada, int codigoEstabelecimento)
		{
			return VanadiumPedidoDomainService.ListarPedidosEstabelecimento(chaveCriptografada, codigoEstabelecimento);
		}

        public DTOHistoricoPedidos ListarHistoricoPedidos(string chaveCriptografada, int codigoEstabelecimento, DateTime dataInicio, DateTime dataFim)
        {
            return VanadiumPedidoDomainService.ListarHistoricoPedidos(chaveCriptografada, codigoEstabelecimento, dataInicio, dataFim);
        }

        public DTORetorno TrocarMesa(DTOAlterarMesaEntrada alterarMesa)
        {
            return VanadiumPedidoDomainService.TrocarMesa(alterarMesa);
        }

		public DTORetorno TransferirMesaPedido(DTOTransfereMesaEntrada transferirMesa)
		{
			return VanadiumPedidoDomainService.TransferirMesaPedido(transferirMesa);
		}



		//public DTORelatorioRetorno GerarRelatorio(DTORelatorioEntrada relatorio)
		//{
		//    return VanadiumPedidoDomainService.GerarRelatorio(relatorio);
		//}

		public DTORelatorioGarcomRetorno GerarRelatorioGarcom(DTORelatorioEntrada relatorio)
		{
			return VanadiumPedidoDomainService.GerarRelatorioGarcom(relatorio);
		}

		public DTORelatorioItemRetorno GerarRelatorioItem(DTORelatorioEntrada relatorio)
		{
			return VanadiumPedidoDomainService.GerarRelatorioItem(relatorio);
		}

		public DTORelatorioMesaRetorno GerarRelatorioMesa(DTORelatorioEntrada relatorio)
		{
			return VanadiumPedidoDomainService.GerarRelatorioMesa(relatorio);
		}
	}
}