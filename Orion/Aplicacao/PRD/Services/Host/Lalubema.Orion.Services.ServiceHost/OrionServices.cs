﻿using System;
using System.ServiceProcess;
using System.Threading;
using Common.Logging;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.ServiceHost
{
    public partial class OrionServices : ServiceBase
    {
        private readonly ILog _logger;
        private bool _cancelar;

        public OrionServices()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;

            currentDomain.UnhandledException += CurrentDomainUnhandledException;

            InitializeComponent();

            _logger = LogManager.GetLogger(GetType());

            _cancelar = false;
        }

        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            if (ex != null)
                _logger.Error(ex.Message, ex);
        }

        protected override void OnStart(string[] args)
        {
            var oThread = new Thread(InicarServico);

            oThread.Start();
        }

        protected override void OnStop()
        {
            try
            {
                _cancelar = true;

                ObjectFactory.Finalize();

                NotificationManager.StopServices();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }
        }

        protected void InicarServico()
        {
            var iniciou = false;

            while (!iniciou && !_cancelar)
            {
                try
                {
                    ObjectFactory.Initialize();

                    iniciou = true;

                    NotificationManager.StartServices();
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message, ex);

                    Thread.Sleep(60000);
                }
            }
        }
    }
}