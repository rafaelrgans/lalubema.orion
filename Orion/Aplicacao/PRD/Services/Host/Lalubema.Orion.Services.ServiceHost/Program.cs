﻿using System.ServiceProcess;

namespace Lalubema.Orion.Services.ServiceHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var servicesToRun = new ServiceBase[]
                                    {
                                        new OrionServices()
                                    };

            ServiceBase.Run(servicesToRun);
        }
    }
}