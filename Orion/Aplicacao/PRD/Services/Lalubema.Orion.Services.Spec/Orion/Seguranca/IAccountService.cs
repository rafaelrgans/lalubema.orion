﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Transactions;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Orion.Seguranca
{
    [ServiceContract]
    [ObjectMap("AccountService", false)]
    public interface IAccountService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Register")]
        [ServiceKnownType(typeof(DTOAccountRegister))]
        [ServiceKnownType(typeof(Domain.DataTransferObject.Vanadium.Seguranca.DTOAccountRegister))]
        DTORetorno Register(DTOAccountRegister account);

        [OperationContract]
        [ServiceKnownType(typeof(DTOAccountRegister))]
        DTORetorno RegisterAccount(DTOAccountRegister account);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "CheckEmail?c={c}")]
        Stream CheckEmail(string c);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/RecoveryPassword")]
        DTORetorno RecoveryPassword(DTORecoveryPassword recoveryPassword);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/DirectDebit")]
        DTORetorno DirectDebit(DTODirectDebitRequest directDebitRequest);
    }
}