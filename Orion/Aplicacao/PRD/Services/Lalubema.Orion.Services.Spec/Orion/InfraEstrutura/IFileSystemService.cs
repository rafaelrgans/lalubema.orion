﻿using System.ServiceModel;
using Lalubema.Orion.Domain.DataTransferObject.Orion.InfraEstrutura.FileSystem;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Orion.InfraEstrutura
{
    [ServiceContract]
    [ObjectMap("FileSystemService", false)]
    public interface IFileSystemService
    {
        [OperationContract]
        DTOFileRetorno SaveFile(DTOFileEntrada file);
    }
}