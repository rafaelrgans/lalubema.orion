﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.AcessoPessoa;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.LocalBem;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.AutorizacaoEntradaEntrega;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ChegadaSegura;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Classificado;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Condominio;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Convenio;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.DisponibilidadeLocalBem;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Edificio;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.MarcaVeiculo;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Mensagem;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ModeloMarca;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.PessoaCondominio;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.ReservaLocalBem;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.UnidadeHabitacional;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Veiculo;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.VeiculoCondominio;
using Lalubema.Utilitarios.Spring;
using DTOPessoaUnidadeHabitacional = Lalubema.Orion.Domain.DataTransferObject.Corona.Cadastro.Pessoa.DTOPessoaUnidadeHabitacional;

namespace Lalubema.Orion.Services.Spec.Orion
{
    [ServiceContract]
    [DataContractFormat]
    [ObjectMap("CadastroService", false)]
    public interface ICadastroService
    {
        [OperationContract]
        DTORetorno Salvar(DTOEntrada entidade);

        [OperationContract]
        DTORetorno Excluir(DTOEntrada entidade);

        [OperationContract]
        DTORetorno Listar(DTOEntrada entidade);

        [OperationContract]
        IList<DTORetorno> ListAll(DTOEntrada entidade);

        [OperationContract]
        DTORetorno Buscar(DTOEntrada entidade);

        // ========================== JSON ===================================

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", UriTemplate = "/Save/{entity}",
            ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [ServiceKnownType(typeof(DTOAcessoPessoaRetorno))]
        [ServiceKnownType(typeof(DTOLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOAutorizacaoEntradaEntregaRetorno))]
        [ServiceKnownType(typeof(DTOChegadaSeguraRetorno))]
        [ServiceKnownType(typeof(DTOClassificadoRetorno))]
        [ServiceKnownType(typeof(DTOCondominioRetorno))]
        [ServiceKnownType(typeof(DTOConvenioRetorno))]
        [ServiceKnownType(typeof(DTODisponibilidadeLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOEdificioRetorno))]
        [ServiceKnownType(typeof(DTOMarcaVeiculoRetorno))]
        [ServiceKnownType(typeof(DTOMensagemRetorno))]
        [ServiceKnownType(typeof(DTOModeloMarcaRetorno))]
        [ServiceKnownType(typeof(DTOPessoaRetorno))]
        [ServiceKnownType(typeof(DTOPessoaCondominioRetorno))]
        [ServiceKnownType(typeof(DTOPessoaUnidadeHabitacional))]
        [ServiceKnownType(typeof(DTOReservaLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOUnidadeHabitacionalRetorno))]
        [ServiceKnownType(typeof(DTOVeiculoRetorno))]
        [ServiceKnownType(typeof(DTOVeiculoCondominioRetorno))]
        DTORetorno Save(string entity, object entidade);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "DELETE", UriTemplate = "/Delete/{entity}/{id}?chaveCriptografada={chaveCriptografada}",
            ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        DTORetorno Delete(string chaveCriptografada, string entity, string id);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "/ListAll/{entity}?chaveCriptografada={chaveCriptografada}&filter={filter}&sort={sort}",
            ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        IList<DTORetorno> List(string chaveCriptografada, string entity, string filter, string sort);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "/Get/{entity}/{id}?chaveCriptografada={chaveCriptografada}",
            ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [ServiceKnownType(typeof(DTOAcessoPessoaRetorno))]
        [ServiceKnownType(typeof(DTOLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOAutorizacaoEntradaEntregaRetorno))]
        [ServiceKnownType(typeof(DTOChegadaSeguraRetorno))]
        [ServiceKnownType(typeof(DTOClassificadoRetorno))]
        [ServiceKnownType(typeof(DTOCondominioRetorno))]
        [ServiceKnownType(typeof(DTOConvenioRetorno))]
        [ServiceKnownType(typeof(DTODisponibilidadeLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOEdificioRetorno))]
        [ServiceKnownType(typeof(DTOMarcaVeiculoRetorno))]
        [ServiceKnownType(typeof(DTOMensagemRetorno))]
        [ServiceKnownType(typeof(DTOModeloMarcaRetorno))]
        [ServiceKnownType(typeof(DTOPessoaRetorno))]
        [ServiceKnownType(typeof(DTOPessoaCondominioRetorno))]
        [ServiceKnownType(typeof(DTOPessoaUnidadeHabitacional))]
        [ServiceKnownType(typeof(DTOReservaLocalBemRetorno))]
        [ServiceKnownType(typeof(DTOUnidadeHabitacionalRetorno))]
        [ServiceKnownType(typeof(DTOVeiculoRetorno))]
        [ServiceKnownType(typeof(DTOPessoaUnidadeHabitacional))]
        DTORetorno Get(string chaveCriptografada, string entity, string id);
    }
}