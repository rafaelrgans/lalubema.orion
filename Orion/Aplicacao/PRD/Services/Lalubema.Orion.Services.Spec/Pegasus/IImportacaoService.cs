﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Pegasus.Importacao;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Pegasus
{
    [ServiceContract]
    [ObjectMap("PegasusImportacaoService", false)]
    public interface IImportacaoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/Exportar")]
        DTORetorno Exportar(DTOExportacao importacaoIndices);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", UriTemplate = "Importar?ChaveCriptografada={chaveCriptografada}&email={email}&senha={senha}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        DTOImportacao Importar(string chaveCriptografada, string email, string senha);
    
    }
}