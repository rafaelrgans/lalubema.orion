﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Cobalt
{
	[ServiceContract]
	[ObjectMap("CobaltServicoOnlineService", false)]
	public interface IServicoOnlineService
	{
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/SolicitarServicoOnlineNotarial")]
		DTORetorno SolicitarServicoOnlineNotarial(DTOServicoOnlineNotarialEntrada solicitacaoServicoNotarial);
	}
}
