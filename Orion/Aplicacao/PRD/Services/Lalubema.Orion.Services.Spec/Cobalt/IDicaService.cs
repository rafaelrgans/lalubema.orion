﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt.Cadastro.Dica;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Cobalt
{
    [ServiceContract]
    [ObjectMap("CobaltDicaService", false)]
    public interface IDicaService
    {
        [OperationContract]
        DTORetorno SalvarDica(DTODicaEntrada dica);

        [OperationContract]
        DTORetorno ExcluirDica(DTODicaEntrada dica);

		[OperationContract]
		DTODicasRetorno ListarDicas(string chaveCriptografada);

		[OperationContract]
		DTODicasRetorno ListarDica(DTODicaEntrada dica);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET",
            UriTemplate = "/ListarDicasDisponiveis?chaveCriptografada={chaveCriptografada}&lastSync={lastSync}")]
        DTODicasRetorno ListarDicasDisponiveis(string chaveCriptografada, DateTime lastSync);
    }
}