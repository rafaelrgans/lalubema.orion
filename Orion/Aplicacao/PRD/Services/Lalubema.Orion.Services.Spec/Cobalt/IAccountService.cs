﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Cobalt
{
    [ServiceContract]
    [ObjectMap("CobaltAccountService", false)]
    public interface IAccountService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/ReenviarSenha")]
        DTORetorno ReenviarSenha(DTOReenviarSenhaEntrada reenviarSenha);
    }
}