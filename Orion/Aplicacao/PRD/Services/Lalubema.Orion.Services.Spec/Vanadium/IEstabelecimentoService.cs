﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Estabelecimento;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Importacao;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Utilitarios.Spring;
using DTOCardapio = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Cardapio.DTOCardapio;
using DTOCategoria = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Categoria.DTOCategoria;
using DTOItem = Lalubema.Orion.Domain.DataTransferObject.Vanadium.Cadastro.Item.DTOItem;

namespace Lalubema.Orion.Services.Spec.Vanadium
{
    [ServiceContract]
    [ObjectMap("VanadiumEstabelecimentoService", false)]
    public interface IEstabelecimentoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/BuscarCategoriasCardapio?ChaveCriptografada={chaveCriptografada}&CodigoEstabelecimento={codigoEstabelecimento}"
            )]
        [ServiceKnownType(typeof(DTOCategoria))]
        DTOCategorias BuscarCategoriasCardapio(string chaveCriptografada, int codigoEstabelecimento);


        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/BuscarCardapios?ChaveCriptografada={chaveCriptografada}&CodigoEstabelecimento={codigoEstabelecimento}"
            )]
        [ServiceKnownType(typeof(DTOCardapio))]
        [ServiceKnownType(typeof(DTOItem))]
        [ServiceKnownType(typeof(DTOCategoria))]
        DTOCardapios BuscarCardapios(string chaveCriptografada, int codigoEstabelecimento);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/BuscarItem?ChaveCriptografada={chaveCriptografada}&CodigoEstabelecimento={codigoEstabelecimento}&CodigoItem={codigoItem}"
            )]
        DTOItemRetorno BuscarItem(string chaveCriptografada, int codigoEstabelecimento, int codigoItem);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/Importar?ChaveCriptografada={chaveCriptografada}&CodigoEstabelecimento={codigoEstabelecimento}"
            )]
        DTOImportacao Importar(string chaveCriptografada, int codigoEstabelecimento);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ListarEstabelecimentos?ChaveCriptografada={chaveCriptografada}")]
        DTOEstabelecimentos ListarEstabelecimentos(string chaveCriptografada);
    }
}