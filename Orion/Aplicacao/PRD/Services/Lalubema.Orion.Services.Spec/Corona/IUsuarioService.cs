﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Usuario;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Corona
{
    [ServiceContract]
    [ObjectMap("CoronaUsuarioService", false)]
    public interface IUsuarioService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/RegisterDevice")]
        DTORetorno RegisterDevice(DTORegisterDevice registerDevice);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "POST", UriTemplate = "/UnregisterDevice")]
        DTORetorno UnregisterDevice(DTOUnregisterDevice unregisterDevice);
    }
}