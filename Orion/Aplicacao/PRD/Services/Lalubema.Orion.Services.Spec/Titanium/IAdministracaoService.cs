﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Lalubema.Orion.Domain.DataTransferObject;
using Lalubema.Orion.Domain.DataTransferObject.Titanium;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Partida;
using Lalubema.Orion.Domain.DataTransferObject.Titanium.Torneio;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Services.Spec.Titanium
{
    [ServiceContract]
    [ObjectMap("TitaniumAdministracaoService", false)]
    public interface IAdministracaoService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "PUT", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, UriTemplate = "/CadastrarTorneio")]
        [ServiceKnownType(typeof(DTOCadastroTorneio))]
        DTORetorno CadastrarTorneio(DTOCadastroTorneio torneio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/BuscarTorneio?ChaveCriptografada={chaveCriptografada}&CodigoTorneio={codigoTorneio}")]
        [ServiceKnownType(typeof (DTORetorno))]
        [ServiceKnownType(typeof (DTOTorneioRetorno))]
        DTOTorneioRetorno BuscarTorneio(string chaveCriptografada, int codigoTorneio);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/BuscarTorneios?chaveCriptografada={chaveCriptografada}")]
        DTOTorneioPorStatus BuscarTorneios(string chaveCriptografada);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare, Method = "GET", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate =
                "/BuscarPartidas?chaveCriptografada={chaveCriptografada}&codigoTorneio={codigoTorneio}"
            )]
        IList<DTOPartidaRetorno> BuscarPartidas(string chaveCriptografada, int codigoTorneio);
    }
}