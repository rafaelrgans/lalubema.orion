﻿CREATE TABLE [dbo].[Categoria] (
    [Codigo]            INT           IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (100) NOT NULL,
    [Ordem]             INT           NULL,
    [CdEstabelecimento] INT           NOT NULL
);

