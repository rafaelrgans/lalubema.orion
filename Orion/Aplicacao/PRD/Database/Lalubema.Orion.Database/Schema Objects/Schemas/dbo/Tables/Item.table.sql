﻿CREATE TABLE [dbo].[Item] (
    [Codigo]      INT            IDENTITY (1, 1) NOT NULL,
    [Nome]        VARCHAR (80)   NOT NULL,
    [Descricao]   VARCHAR (4000) NOT NULL,
    [Preco]       MONEY          NOT NULL,
    [UrlFoto]     VARCHAR (255)  NULL,
    [Destaque]    BIT            NULL,
    [CdCardapio]  INT            NOT NULL,
    [CdCategoria] INT            NOT NULL
);

