﻿CREATE TABLE [dbo].[Estabelecimento] (
    [Codigo]    INT            IDENTITY (1, 1) NOT NULL,
    [Nome]      VARCHAR (60)   NOT NULL,
    [Descricao] VARCHAR (4000) NOT NULL,
    [Site]      VARCHAR (255)  NULL,
    [UrlLogo]   VARCHAR (255)  NULL,
    [Endereco]  VARCHAR (4000) NOT NULL
);

