﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Lalubema.Wcf.Suporte.Serialization
{
    public class EndpointBehaviorMessageSerializer : IDispatchMessageFormatter
    {
        private readonly OperationDescription _operation;
        private readonly Dictionary<string, int> _parameterNames;


        public EndpointBehaviorMessageSerializer(OperationDescription operation, bool isRequest)
        {
            _operation = operation;

            if (isRequest)
            {
                var operationParameterCount = operation.Messages[0].Body.Parts.Count;
                if (operationParameterCount > 1)
                {
                    _parameterNames = new Dictionary<string, int>();

                    for (var i = 0; i < operationParameterCount; i++)
                    {
                        _parameterNames.Add(operation.Messages[0].Body.Parts[i].Name, i);
                    }
                }
            }
        }

        public void DeserializeRequest(Message message, object[] parameters)
        {
            if(parameters.Length > 0)
            {
                SetPrimiteParameters(message, parameters);

                if (!message.IsEmpty)
                {
                    SetComplexParameters(message, parameters);
                }
            }
        }

        private void SetComplexParameters(Message message, object[] parameters)
        {
            var jsonMessage = GetJsonMessage(message);

            var j = 0;

            foreach (var parametro in _operation.Messages[0].Body.Parts)
            {
                if (!parametro.Type.IsPrimitive && parametro.Type != typeof(string))
                {
                    var o = new JavaScriptSerializer().Deserialize(jsonMessage,
                                                                    _operation.Messages[0].Body.Parts[j].Type);

                    parameters[j] = o;
                }

                j++;
            }
        }

        private static string GetJsonMessage(Message message)
        {
            string jsonMessage;

            try
            {
                var bodyReader = message.GetReaderAtBodyContents();

                var encodedMessage = bodyReader.ReadElementContentAsBase64();

                jsonMessage = Encoding.UTF8.GetString(encodedMessage);
            }
            catch (XmlException)
            {
                jsonMessage = string.Empty;

                var xmlMessage = message.ToString();

                var xmlDocument = new XmlDocument();

                xmlDocument.LoadXml(xmlMessage);

                foreach (XmlNode node in xmlDocument.ChildNodes[0].ChildNodes)
                {
                    const string templateJsonValue = "\"{0}\":\"{1}\"";

                    var name = node.Name;
                    var text = node.InnerText;

                    var jsonValue = string.Format(templateJsonValue, name, text);

                    if (string.IsNullOrWhiteSpace(jsonMessage))
                    {
                        jsonMessage = jsonValue;
                    }
                    else
                    {
                        jsonMessage += ", " + jsonValue;
                    }
                }

                jsonMessage = "{" + jsonMessage + "}";
            }

            return jsonMessage;
        }

        private void SetPrimiteParameters(Message message, object[] parameters)
        {
            var valueQueryString = ((HttpRequestMessageProperty) message.Properties["httpRequest"]).QueryString.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var t in valueQueryString)
            {
                var keyValue = t.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries);

                var j = 0;

                foreach (var key in _parameterNames.Keys)
                {
                    if(key.Equals(keyValue[0], StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (_operation.Messages[0].Body.Parts[j].Type.IsPrimitive || _operation.Messages[0].Body.Parts[j].Type == typeof(string))
                        {
                            parameters[j] = Convert.ChangeType(keyValue[1], _operation.Messages[0].Body.Parts[j].Type);

                            break;
                        }
                    }

                    j++;
                }
            }
        }

        public Message SerializeReply(MessageVersion messageVersion, object[] parameters, object result)
        {
            var o = new JavaScriptSerializer().Serialize(result);

            var body = Encoding.Default.GetBytes(o);

            var replyMessage = Message.CreateMessage(messageVersion, _operation.Messages[1].Action, new RawBodyWriter(body));

            replyMessage.Properties.Add(WebBodyFormatMessageProperty.Name, new WebBodyFormatMessageProperty(WebContentFormat.Raw));

            var respProp = new HttpResponseMessageProperty();
            respProp.Headers[HttpResponseHeader.ContentType] = "application/json; charset=utf-8";
            replyMessage.Properties.Add(HttpResponseMessageProperty.Name, respProp);

            return replyMessage;
        }
    }
}