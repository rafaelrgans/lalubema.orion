﻿namespace Lalubema.Orion.Common.Tipos
{
    public class LocalizedKey
    {
        public class Corona
        {
            public const string ChegadaSegura = "CHEGADA_SEGURA";

            public const string NotificarPortaria = "NOTIFICACAO_PORTARIA";

            public const string NotificarSindico = "NOTIFICACAO_SINDICO";

            public const string NotificarResposta = "NOTIFICACAO_RESPOSTA";
        }
    }
}
