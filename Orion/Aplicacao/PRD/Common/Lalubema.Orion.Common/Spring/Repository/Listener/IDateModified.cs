﻿using System;

namespace Lalubema.Orion.Common.Spring.Repository.Listener
{
    public interface IDateModified
    {
        DateTime LastUpdate { get; set; }
    }
}