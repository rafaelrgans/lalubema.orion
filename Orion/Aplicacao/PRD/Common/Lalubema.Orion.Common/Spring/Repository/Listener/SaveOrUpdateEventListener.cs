﻿using NHibernate.Event;

namespace Lalubema.Orion.Common.Spring.Repository.Listener
{
    public class SaveOrUpdateEventListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        #region IPreInsertEventListener Members

        public bool OnPreInsert(PreInsertEvent @event)
        {
            EntityUpdateHelper.UpdateDate(@event.Persister, @event.State, @event.Persister.PropertyNames);

            return false;
        }

        #endregion

        #region IPreUpdateEventListener Members

        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            EntityUpdateHelper.UpdateDate(@event.Persister, @event.State, @event.Persister.PropertyNames);

            return false;
        }

        #endregion
    }
}