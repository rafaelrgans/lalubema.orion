﻿namespace Lalubema.Orion.Common.Exceptions.Vanadium
{
    public class MesaJaAbertaException : BaseException
    {
        public MesaJaAbertaException(int codigoMesa)
            : base(701005, string.Format(Utilitarios.Messages.Exceptions.EX_701005, codigoMesa))
        {
        }
    }
}