﻿namespace Lalubema.Orion.Common.Exceptions.Vanadium
{
    public class InvalidLicenseTypeException : BaseException
    {
        public InvalidLicenseTypeException()
            : base(701002, Utilitarios.Messages.Exceptions.EX_701002)
        {
        }
    }
}