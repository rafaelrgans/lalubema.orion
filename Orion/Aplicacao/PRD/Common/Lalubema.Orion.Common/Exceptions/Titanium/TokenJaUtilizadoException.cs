﻿using System;

namespace Lalubema.Orion.Common.Exceptions.Titanium
{
    public class TokenJaUtilizadoException : BaseException
    {
        public TokenJaUtilizadoException()
            : base(301002, Utilitarios.Messages.Exceptions.EX_301002)
        {
        }

        public TokenJaUtilizadoException(Exception innerException)
            : base(301002, innerException)
        {
        }
    }
}