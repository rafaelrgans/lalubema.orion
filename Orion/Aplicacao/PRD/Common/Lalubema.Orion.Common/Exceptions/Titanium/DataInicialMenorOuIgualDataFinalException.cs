﻿namespace Lalubema.Orion.Common.Exceptions.Titanium
{
    public class DataInicialMenorOuIgualDataFinalException : BaseException
    {
        public DataInicialMenorOuIgualDataFinalException()
            : base(102002, Utilitarios.Messages.Exceptions.EX_102002)
        {
        }
    }
}