﻿namespace Lalubema.Orion.Common.Exceptions.Titanium.Torneio
{
    public class TorneioJaIniciadoException : BaseException
    {
        public TorneioJaIniciadoException()
            : base(203002, "O torneio já iniciou e não é possível gerar a tabela novamente.")
        {
        }
    }
}