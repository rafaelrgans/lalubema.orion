﻿namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class InvalidStatusException : BaseException
    {
        public InvalidStatusException(string newStatus)
            : base(101004, string.Format(Utilitarios.Messages.Exceptions.EX_101004, newStatus))
        {
        }
    }
}