﻿namespace Lalubema.Orion.Common.Exceptions.Orion
{
    public class DadosInvalidosException : BaseException
    {
        public DadosInvalidosException(string message)
            : base(901001, string.Format(Utilitarios.Messages.Exceptions.EX_901001, message))
        {
        }
    }
}