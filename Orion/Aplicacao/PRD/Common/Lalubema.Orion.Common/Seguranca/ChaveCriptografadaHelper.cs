﻿using System;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Exceptions.Security;
using Lalubema.Utilitarios.Extensions;
using Lalubema.Utilitarios.Helper.Security;

namespace Lalubema.Orion.Common.Seguranca
{
    public class ChaveCriptografadaHelper
    {
        #region [ Properties & Fields ]

        public DateTime DataCriacao { get; private set; }

        public int CodigoAplicacao { get; private set; }

        public string Versao { get; private set; }

        public int CodigoUsuario { get; private set; }

        public string EmailUsuario { get; private set; }

        public CryptographyType CryptographyType { get; private set; }

        #endregion [ Properties & Fields ]

        #region [ Constructors ]

        public ChaveCriptografadaHelper(string chaveCriptografada)
        {
            if (string.IsNullOrWhiteSpace(chaveCriptografada))
                throw new ArgumentNullException();

            SetValues(chaveCriptografada);
        }

        #endregion [ Constructors ]

        #region [ Methods ]

        private void SetValues(string chaveCriptografada)
        {
            var values = SecurityHelper.Decrypt(chaveCriptografada);

            CryptographyType = values[0].ConvertoToEnum<CryptographyType>();
			DataCriacao = DateTime.UtcNow;
			//DataCriacao = DateTime.SpecifyKind(values[1].ToDateTime(), DateTimeKind.Utc);

            switch (CryptographyType)
            {
                case CryptographyType.Undefined:
                    throw new InvalidCryptographyKeyException(chaveCriptografada);
                case CryptographyType.System:
                    SetSystemValues(values);
                    break;
                case CryptographyType.User:
                    SetUserValues(values);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetSystemValues(string[] values)
        {
            if(values.Length >= 4)
                CodigoAplicacao = Convert.ToInt32(values[3]);

            if (values.Length >= 5)
                Versao = values[4];
        }

        private void SetUserValues(string[] values)
        {
            if (values.Length >= 3)
                CodigoUsuario = values[2].ToInt32();

            if (values.Length >= 4)
                EmailUsuario = values[3];

            if (values.Length >= 5)
                Versao = values[4];

            if (values.Length >= 6)
                CodigoAplicacao = Convert.ToInt32(values[5]);
        }

        #endregion [ Methods ]
    }
}