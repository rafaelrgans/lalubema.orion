﻿using System;
using Lalubema.Orion.Domain.DataTransferObject.Corona.Condominio;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Corona;
using Lalubema.Utilitarios;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Corona
{
    [TestClass]
    public class CondominioServiceTest : TestBase
    {
        private const string UsuarioValido = "teste@lalubema.com";

        private const string SenhaValida = "a";

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        private string GetChaveCriptografadaUsuario()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
            {
                ChaveCriptografada = chaveCriptogradaSistema,
                Senha = SenhaValida,
                Usuario = UsuarioValido,
                Versao = "1.0"
            };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "CORONA";
            const string senha = "5!z[B%J[";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private ICondominioService ObterCondominioService()
        {
            return ObjectFactory.GetObject<ICondominioService>(false);
        }

        [TestMethod]
        public void RegisterDeviceTest()
        {
            // Arrange
            var chaveCriptografada = GetChaveCriptografadaUsuario();

            var registerDevice = new DTORegisterDevice
                                     {
                                         ChaveCriptografada = chaveCriptografada,
                                         DeviceToken =
                                             "afe5b56bd028729ac32e02cb669da65dc581c6146ee64c7522cede79cd7f6557",
                                         Bundle = "com.lalubema.MC",
                                         Name = "iPad Lalubema",
                                         PlatformType = PlatformType.Apple,
                                         Condominio = 1
                                     };

            // Action
            var retorno = ObterCondominioService().RegisterDevice(registerDevice);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "retorno"));
            Assert.IsNotNull(retorno);

            Console.WriteLine(retorno.DescricaoRetorno);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(retorno.OcorreuErro);
        }
    }
}
