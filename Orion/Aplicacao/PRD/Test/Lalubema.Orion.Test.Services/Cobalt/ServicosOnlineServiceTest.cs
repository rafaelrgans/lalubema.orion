﻿using System;
using Lalubema.Orion.Domain.DataTransferObject.Cobalt;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Cobalt;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Cobalt
{
    [TestClass]
    public class ServicosOnlineServiceTest : TestBase
    {
        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void SolicitarServicoOnlineNotarial()
        {
            //Arrange
            var chaveCriptografada = GetChaveCriptogradaSistema();
            var solicitacaoNotarial = new DTOServicoOnlineNotarialEntrada
                                          {
                                              ChaveCriptografada = chaveCriptografada,
                                              Nome = "Bruno",
                                              Telefone = "(031) 88498818",
                                              Email = "bmarcall@gmail.com",
                                              TipoEscritura = "Procuração",
                                              TipoDocumento = "INSS"
                                          };

            // Action
            var result = ObterServicoOnlineService().SolicitarServicoOnlineNotarial(solicitacaoNotarial);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "COBALT";
            const string senha = "31=66MzJ";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IServicoOnlineService ObterServicoOnlineService()
        {
            return ObjectFactory.GetObject<IServicoOnlineService>(false);
        }
    }
}