﻿using System;
using System.Collections.Generic;
using Lalubema.Orion.Domain.DataTransferObject.Orion.Seguranca;
using Lalubema.Orion.Domain.DataTransferObject.Vanadium.Pedido;
using Lalubema.Orion.Domain.Orion;
using Lalubema.Orion.Services.Spec.Vanadium;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services.Vanadium
{
    [TestClass]
    public class PedidoServiceTest : TestBase
    {
        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
            NotificationManager.StartServices();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            NotificationManager.StopServices();
            ObjectFactory.Finalize();
        }

        [TestMethod]
        public void RegistrarPedidoGarcomTest()
        {
            // Arrange
            const int codigoEstabelecimento = 2;

            var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();
            var cardapios = ObterEstabelecimentoService()
                .BuscarCardapios(chaveCriptografadaGarcom, codigoEstabelecimento);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "cardapios"));
            Assert.IsFalse(cardapios.OcorreuErro);

            Console.WriteLine(string.Format(AssertMessages.IsTrue, "Cardapios.Count > 0"));
            Assert.IsTrue(cardapios.Cardapios.Count > 0);

            var cardapio = cardapios.Cardapios[0];
            var rdn = new Random(DateTime.UtcNow.Millisecond);

            var codigoMesa = rdn.Next(1, 51);

            var pedido = new DTOPedidoEntrada
                             {
                                 ChaveCriptografada = chaveCriptografadaGarcom,
                                 CodigoEstabelecimento = codigoEstabelecimento,
                                 CodigoMesa = codigoMesa
                             };

            var itens = new List<DTOItem>();

            for (var i = 0; i < 3; i++)
            {
                var itemIndex = rdn.Next(0, cardapio.Itens.Count);

                itens.Add(new DTOItem
                              {
                                  Codigo = cardapio.Itens[itemIndex].Codigo,
                                  Observacao = "Teste Unitário .Net",
                                  Quantidade = rdn.Next(1, 4)
                              });
            }

            pedido.Itens = itens.ToArray();

            // Action
            var result = ObterPedidoService().RegistrarPedido(pedido);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "result.OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);

            Console.WriteLine(string.Format("Mesa Aberta: {0}", codigoMesa));
        }

        [TestMethod]
        public void RegistrarPedidoClienteTest()
        {
            // Arrange
            const int codigoEstabelecimento = 2;

            var chaveCriptografadaCliente = GetChaveCriptografadaCliente();
            var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();

            var cardapios = ObterEstabelecimentoService().BuscarCardapios(chaveCriptografadaGarcom, codigoEstabelecimento);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "cardapios"));
            Assert.IsFalse(cardapios.OcorreuErro);

            Console.WriteLine(string.Format(AssertMessages.IsTrue, "Cardapios.Count > 0"));
            Assert.IsTrue(cardapios.Cardapios.Count > 0);

            var cardapio = cardapios.Cardapios[0];
            var rdn = new Random(DateTime.UtcNow.Millisecond);

            var pedido = new DTOPedidoEntrada
                             {
                                 ChaveCriptografada = chaveCriptografadaCliente,
                                 CodigoEstabelecimento = codigoEstabelecimento
                             };

            var itens = new List<DTOItem>();

            for (var i = 0; i < 3; i++)
            {
                var itemIndex = rdn.Next(0, cardapio.Itens.Count);

                itens.Add(new DTOItem
                              {
                                  Codigo = cardapio.Itens[itemIndex].Codigo,
                                  Observacao = "Teste Unitário .Net",
                                  Quantidade = rdn.Next(1, 4)
                              });
            }

            pedido.Itens = itens.ToArray();

            // Action
            var result = ObterPedidoService().RegistrarPedido(pedido);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "result.OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);
        }

        [TestMethod]
        public void FecharPedidoTest()
        {
            // Arrange
            const int codigoEstabelecimento = 2;

            var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();

            var mesasAbertas = ObterPedidoService().ListarMesasAbertas(chaveCriptografadaGarcom, codigoEstabelecimento);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "cardapios"));
            Assert.IsFalse(mesasAbertas.OcorreuErro);

            Console.WriteLine(string.Format(AssertMessages.IsTrue, "Cardapios.Count > 0"));
            Assert.IsTrue(mesasAbertas.Mesas.Count > 0);

            var rdn = new Random(DateTime.UtcNow.Millisecond);
            var indexMesa = rdn.Next(0, mesasAbertas.Mesas.Count);

            var codigoMesa = mesasAbertas.Mesas[indexMesa];

            var pedido = new DTOPedidoEntrada
                             {
                                 ChaveCriptografada = chaveCriptografadaGarcom,
                                 CodigoEstabelecimento = codigoEstabelecimento,
                                 CodigoMesa = codigoMesa
                             };

            // Action
            var result = ObterPedidoService().FecharComanda(pedido);

            // Assert
            Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
            Assert.IsNotNull(result);

            Console.WriteLine(string.Format(AssertMessages.IsFalse, "result.OcorreuErro"));
            Assert.IsFalse(result.OcorreuErro);

            Console.WriteLine(string.Format("Mesa Fechada: {0}", codigoMesa));
        }

		[TestMethod]
		public void ListarItensPendentes()
		{
			// Arrange
			int codigoEstabelecimento = 2;
			int codigoMesa = 0;

			var chaveCriptografadaCliente = GetChaveCriptografadaCliente();

			// Action
			var result = ObterPedidoService().ListarItensPendentes(chaveCriptografadaCliente, codigoEstabelecimento, codigoMesa);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
			Assert.IsNotNull(result.OcorreuErro);

			Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.Itens.Count > 0"));
			Assert.IsTrue(result.Itens.Count > 0);
		}

		[TestMethod]
		public void ListarHistoricoPedidos()
		{
			int codigoEstabelecimento = 2;
			var dtinicio = DateTime.Now.Date.AddMonths(-3);
			var dtfim = DateTime.Now.Date.AddMonths(2);

			var chaveCriptografadaCliente = GetChaveCriptografadaCliente();

			// Action
			var result = ObterPedidoService().ListarHistoricoPedidos(chaveCriptografadaCliente, codigoEstabelecimento, dtinicio, dtfim);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
			Assert.IsNotNull(result.OcorreuErro);

			Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.Pedidos.Count > 0"));
			Assert.IsTrue(result.Pedidos.Count > 0);
		}


		[TestMethod]
		public void ListarPedidosEstabelecimento()
		{
			int codigoEstabelecimento = 2;

			var chaveCriptografadaCliente = GetChaveCriptografadaCliente();

			// Action
			var result = ObterPedidoService().ListarPedidosEstabelecimento(chaveCriptografadaCliente, codigoEstabelecimento);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
			Assert.IsNotNull(result.OcorreuErro);

			Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.Itens.Count > 0"));
			Assert.IsTrue(result.Itens.Count > 0);
		}

		[TestMethod]
		public void ListarPedidosAbertosMenu()
		{
			int codigoEstabelecimento = 2;
			int codigoMesa = 0;

			var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();

			// Action
			var result = ObterPedidoService().ListarPedidosAbertosMenu(chaveCriptografadaGarcom, codigoEstabelecimento, codigoMesa);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
			Assert.IsNotNull(result.OcorreuErro);

			Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.Pedidos.Count > 0"));
			Assert.IsTrue(result.Pedidos.Count > 0);
		}

		[TestMethod]
		public void TrocarMesaMenuTest()
		{
			int codigoPedido = 1069;

			var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();

			var dtomesa = new DTOTransfereMesaEntrada
			{
				ChaveCriptografada = chaveCriptografadaGarcom,
				CodigoPedido = codigoPedido,
				CodigoMesa = 0,
				NovaMesa = 2,
				CodigoEstabelecimento = 2
			};

			// Action
			var result = ObterPedidoService().TransferirMesaPedido(dtomesa);

			// Assert
			Console.WriteLine(string.Format(AssertMessages.IsNotNull, "result"));
			Assert.IsNotNull(result);

			Console.WriteLine(string.Format(AssertMessages.IsFalse, "result.OcorreuErro"));
			Assert.IsFalse(result.OcorreuErro);
		}


		[TestMethod]
		public void GerarRelatorioTest()
		{
			// Arrange
			var chaveCriptografadaGarcom = GetChaveCriptografadaGarcom();
			var dtInicio = DateTime.Now.Date.AddMonths(-1);
			var dtFim = DateTime.Now.Date.AddMonths(1);

			var relatorioEntrada = new DTORelatorioEntrada
			{
				ChaveCriptografada = chaveCriptografadaGarcom,
				CodEstabelecimento = 2,
				CodigoUsuario = 2,
				CodigoItem = 82,
				NumeroMesa = 2,
				DtInicio = dtInicio,
				DtFim = dtFim,
				ReportType = Common.Tipos.TipoRelatorio.Mesa
			};


			DTORelatorioGarcomRetorno result = null;
			DTORelatorioItemRetorno item = null;
			DTORelatorioMesaRetorno mesa = null;
			if (relatorioEntrada.ReportType == Common.Tipos.TipoRelatorio.Garcom)
			{
				// Action
				result = ObterPedidoService().GerarRelatorioGarcom(relatorioEntrada);
				IList<DTORelatorio> garcom = ((DTORelatorioGarcomRetorno)(result)).Relatorio;
				result.TipoRelatorio = relatorioEntrada.ReportType;

				// Assert
				Console.WriteLine(string.Format(AssertMessages.IsFalse, "result"));
				Assert.IsFalse(result.OcorreuErro);

				Console.WriteLine(string.Format(AssertMessages.IsTrue, "garcom.Count > 0"));
				Assert.IsTrue(garcom.Count > 0);
			}
			else if (relatorioEntrada.ReportType == Common.Tipos.TipoRelatorio.Item)
			{
				item = ObterPedidoService().GerarRelatorioItem(relatorioEntrada);
				item.NomeItem = item.ItemPedido[0].NomeItem;
				item.TotalVendido = item.ItemPedido[0].TotalVendido;
				item.TipoRelatorio = relatorioEntrada.ReportType;

				Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.ItemPedido.Count > 0"));
				Assert.IsTrue(item.ItemPedido.Count > 0);
			}
			else
			{
				mesa = ObterPedidoService().GerarRelatorioMesa(relatorioEntrada);
				mesa.NumeroMesa = relatorioEntrada.NumeroMesa;
				mesa.TotalMesa = mesa.ItemPedido[0].TotalVendido;
				mesa.TipoRelatorio = relatorioEntrada.ReportType;
				
				Console.WriteLine(string.Format(AssertMessages.IsTrue, "result.ItemPedido.Count > 0"));
				Assert.IsTrue(mesa.ItemPedido.Count > 0);
			}
		}

        private string GetChaveCriptografadaGarcom()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
            {
                ChaveCriptografada = chaveCriptogradaSistema,
                Senha = "a",
                Usuario = "a@a.com",
                Versao = "1.0"
            };

            var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptografadaCliente()
        {
            var chaveCriptogradaSistema = GetChaveCriptogradaSistema();

            var ticketUsuario = new DTOTicketPorUsuario
                                    {
                                        ChaveCriptografada = chaveCriptogradaSistema,
                                        Senha = "ninja",
										Usuario = "bmarcall@msn.com",
                                        Versao = "1.0"
                                    };

			var autenticacao = ObterAutenticacaoService().AutenticarUsuario(ticketUsuario);
			//var autenticacao = ObterAutenticacaoService().Autenticar(ticketUsuario);

            var chaveCriptografada = autenticacao.ChaveCriptografada;

            return chaveCriptografada;
        }

        private string GetChaveCriptogradaSistema()
        {
            const string sistema = "VANADIUM";
            const string senha = "0?cg@I2h";

            var autenticarSistema = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            var chaveCriptogradaSistema = autenticarSistema.ChaveCriptografada;

            return chaveCriptogradaSistema;
        }

        private IEstabelecimentoService ObterEstabelecimentoService()
        {
            return ObjectFactory.GetObject<IEstabelecimentoService>(false);
        }

        private IPedidoService ObterPedidoService()
        {
            return ObjectFactory.GetObject<IPedidoService>(false);
        }
    }
}