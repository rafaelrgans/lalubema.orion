﻿using Lalubema.Orion.Services.Spec;
using Lalubema.Orion.Services.Spec.Corona;
using Lalubema.Orion.Services.Spec.Pegasus;
using Lalubema.Utilitarios.Spring;

namespace Lalubema.Orion.Test.Services
{
    public class TestBase
    {
        protected IAutenticacaoService ObterAutenticacaoService()
        {
            return ObjectFactory.GetObject<IAutenticacaoService>(false);
        }

        protected IPushService ObterPushService()
        {
            return ObjectFactory.GetObject<IPushService>(false);
        }

        protected IImportacaoService ObterImportacaoService()
        {
            return ObjectFactory.GetObject<IImportacaoService>(false);
        }
    }
}