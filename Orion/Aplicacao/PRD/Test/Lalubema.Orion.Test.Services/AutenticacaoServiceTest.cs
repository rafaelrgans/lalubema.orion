﻿using Lalubema.Orion.Domain.DataTransferObject.Autenticacao;
using Lalubema.Utilitarios.Spring;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lalubema.Orion.Test.Services
{
    /// <summary>
    /// Summary description for AutenticacaoServiceTest
    /// </summary>
    [TestClass]
    public class AutenticacaoServiceTest : TestBase
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ObjectFactory.Initialize();
        }

        [ClassCleanup]
        public static void Cleanup()
        {
            ObjectFactory.Finalize();
        }

        #endregion

        [TestMethod]
        public void Autenticar()
        {
            // Arrange
            const string usuario = "teste@teste.com.br";
            const string senha = "123456";

            // Action
            DTOAutenticacao retorno = ObterAutenticacaoService().Autenticar(usuario, senha);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.AreEqual(-1, retorno.CodigoRetorno);
        }

        [TestMethod]
        public void AutenticarSistemaSucesso()
        {
            // Arrange
            const string sistema = "MEUS_INDICES";
            const string senha = "5wKoTkhE";

            // Action
            var retorno = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.IsFalse(retorno.OcorreuErro);
            Assert.AreEqual(0, retorno.CodigoRetorno);
            Assert.IsNotNull(retorno.ChaveCriptografada);
        }

        [TestMethod]
        public void AutenticarSistemaUsuarioOuSenhaInvalidos()
        {
            // Arrange
            const string sistema = "MEUS_INDICES";
            const string senha = "x9ck3d0eJ";

            // Action
            var retorno = ObterAutenticacaoService().AutenticarSistema(sistema, senha);

            // Assert
            Assert.IsNotNull(retorno);
            Assert.IsTrue(retorno.OcorreuErro);
            Assert.AreEqual(201001, retorno.CodigoRetorno);
            Assert.IsNull(retorno.ChaveCriptografada);
        }
    }
}